import re
import logging
import readline
import client
import protocols
import utils


class ProgressiveCompleter():
    """
    Tab completion helper class.
    """
    RE_SPACE = re.compile('.*\s+$', re.M)

    def __init__(self):
        self.options = None
        self.protocol = None
        self.last_cmd = None
        self.last_cmd_result = None
        self.last_input = None
        self.user = None
        self.num_completed_words = 0
        self.reply_mode = False
        self.completing_command_next = False

    def set_options(self, list):
        self.options = list

    def complete(self, text, state):
        if text is None: text = ''
        line = readline.get_line_buffer()
        begin = readline.get_begidx()
        end = readline.get_endidx()
        being_completed = line[begin:end]
        words = line.split()
        if len(words) > 0 and words[0] == 'send':
            words = words[1:]
        elif self.reply_mode:
            if words[0] == 'reply' or words[0] == 'watch':
                words = [self.user, self.protocol.name] + words[2:]
                line += ' '
            else:
                words = [self.user, self.protocol.name] + words
        if line.endswith(' '):
            #self.RE_SPACE.match(line):
            words.append('')
        try:
          if len(words) > self.num_completed_words:
            self.num_completed_words = len(words)

            if not self.user and (begin == 0 or len(words) < 1):
                # user not fixed yet
                self.options = [u+' ' for u in self.options]
            else:
                self.options = []
                self.user = words[0]
                if not self.protocol and len(words) <= 2:
                    # protocol not fixed yet
                    self.all_ports = [p for p in client.get_ports_exposed_by_user(self.user)]
                    self.options = [p.name+' ' for p in self.all_ports]
                else:
                    port = words[1]
                    if not self.protocol:
                        for p in self.all_ports:
                            if port == p.name:
                                break
                        self.protocol = p
                    if len(words) <= 3:
                        # command not fixed yet
                        self.options = [c+' ' for c in self.protocol.get_valid_transition_commands(self.last_cmd, None)]
                        self.completing_command_next = True
                    else:
                        # find the last command and input in the line
                        self.last_cmd = None
                        self.last_input = None
                        if self.completing_command_next:
                            self.last_cmd = words[-2]
                            if len(words) > 4:
                                self.last_input = words[-3]
                        else:
                            self.last_input = words[-2]
                            self.last_cmd = words[-3]

                        self.completing_command_next = not self.completing_command_next

                        if self.last_cmd_result is None or \
                           self.last_cmd_result != protocols.CommandFireResult.FireDone:
                            # command not done firing, fire again
                            input_choices, self.last_cmd_result = client.fire_command(self.protocol, self.last_cmd, self.last_input)
                            if self.last_cmd_result == protocols.CommandFireResult.FireError:
                                logging.error('Error firing command : ' + str(input_choices))
                                return None
                            elif self.last_cmd_result == protocols.CommandFireResult.FireAgainWithAnyInput:
                                input_choices = ['text']
                            elif self.last_cmd_result == protocols.CommandFireResult.FireAgainWithProvidedInput:
                                pass
                            elif self.last_cmd_result == protocols.CommandFireResult.FireDone:
                                pass
                            elif self.last_cmd_result == protocols.CommandFireResult.FireAgainWithCachedInput:
                                input_choices = self.last_input
                            else:
                                return None

                            if input_choices:
                                self.options = [v.replace(' ','_')+' ' for v in utils.flatten_object_to_list(input_choices)]
                            else:
                                self.options = []

                        if len(self.options) == 0 and self.last_cmd_result == protocols.CommandFireResult.FireDone:
                            if self.protocol.is_auto_transition_command(self.last_cmd):
                                self.options = [c+' ' for c in self.protocol.get_valid_transition_commands(self.last_cmd, None)]
                                self.last_cmd_result = None
                            else:
                                return None

          candidates_list = [x for x in self.options if x.startswith(text)]
          if state < len(candidates_list):
              return candidates_list[state]
          else:
              return None
        except:
          logging.exception('error in completion')
          return None
 

class SimpleCompleter():
    """
    Tab completion helper class.
    """
    def __init__(self):
        self.options = None
        pass

    def set_options(self, options):
        self.options = sorted(options)

    def complete(self, text, state):
        results = [x for x in self.options if x.startswith(text)] + [None]
        return results[state]
