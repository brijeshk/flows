#!/usr/bin/env python
"""
Sample Echo Bot for AutoMeton.
This bot echoes back messages it receives.
"""

import sys
import logging
from ws4py.client.threadedclient import WebSocketClient
import time
import signal
import json
import datetime


# GLOBALS
#-------------------------------------------#
g_ws = None
g_botuid = 'EchoBot'
g_server = 'autometon.com:5000'
#-------------------------------------------#


class BotClient(WebSocketClient):

    def opened(self):
        logging.info("websocket opened with server")


    def closed(self, code, reason=None):
        logging.info("websocket closed: " + str(code) + " " + str(reason))


    def received_message(self, message):
        logging.info("received: " + message.data)
        try:
            flows = json.loads(message.data)
        except:
            logging.exception("Not a valid AutoMeton websocket message.")
            return

        # message received is a list of flow deltas
        # let's assume there's only one flow here 
        self.process_message(flows[0])


    def send_message(userid, flow):
        requests.post('http://' + g_server + '/flowDelta/' + userid + '?notify=true', 
                      data=json.dumps([flow]), 
                      headers={'Content-type' : 'application/json'})


    def process_message(self, flow): 
        try:
            # each flow delta may contain a list of atoms (messages) that were sent
            # let's assume exactly one atom is part of the flow delta
            atom = flow['atoms'][-1]

            # build up echo response
            # this is where the bot would process input and compute output data
            data = { 'Respond' : { 'type' : 'string', 'value' : 'echoing ' + atom['data']['Text']['value'] } }
            state = 'Respond'
        except Exception as e:
            logging.exception('Error reading data from message')
            data = { 'Error' : { 'type' : 'Error', 'value' : str(e) } }
            state = 'Error'

        d = datetime.datetime.utcnow()
        timeNow = d.isoformat("T") + "Z"

        # build the response
        atom = { 'fid' : flow['fid'],
                 'aid' : flow['atoms'][-1]['aid'] + '_resp',
                 'crud' : 'c',
                 'state' : state,
                 'body' : None,
                 'data' : data,
                 'sender' : g_botuid,
                 'modified' : timeNow
               }
        flow['atoms'] = [atom]
        flow['modified'] = timeNow
        flow['crud'] = 'u'
        flow['state'] = state

        # send response to all users in the flow
        for u in flow['users']:
            if u != g_botuid:
                logging.info("sending response to " + u)
                self.send_message(u, [flow])
#-------------------------------------------#


def sigtermHandler(signum, frame):
    global g_ws
    logging.info('Received termination signal')
    if g_ws:
        g_ws.close()


# MAIN HANDLER
#-------------------------------------------#
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(levelname)-8s %(message)s')

    # setup termination handler
    signal.signal(signal.SIGINT, sigtermHandler)

    try:
        # open websocket
        g_ws = BotClient('ws://' + g_server + '/websocket/' + g_botuid, protocols=['http-only', 'chat'])
        g_ws.connect()
        print('Press Ctrl-C to terminate.')
        g_ws.run_forever()
    except KeyboardInterrupt:
        g_ws.close()
