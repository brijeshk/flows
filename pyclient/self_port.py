import protocols
import logging
import socket

def process_cmd(cmd, data):
    """
    Processes commands sent on the Self port. These commands are used to pull authorized data from 
    the user's dynamic profile, such as location, health-metrics, calendar, status etc.
    """
    logging.info("received self port cmd : {0}, {1}".format(cmd, data))
    if cmd == 'Hostname':
        return socket.gethostname()
    
    return None
