#!/bin/bash
echo "Killing running bots"
pkill -f "eventbot.py"
bots=( "stock" "weather" "translate" "yelp" "google")
for bot in ${bots[@]}
do
    echo "Starting bot $bot"
    nohup python eventbot.py -u $bot -p $bot > $bot.out &
    sleep 2
done
