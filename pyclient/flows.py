"""
Flows module.
Defines flow-related functions, including retrieving/creating/searching flows.
"""

import protocols
import datetime
import client
import imapsmtp
import xmpp
import logging
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
import base64
import uuid
import self_port
import json

Base = declarative_base()

db_session = None
app_notify_handler = None

# UTIL methods
#-----------------------------------------------------------------------------#

def _is_message_addressed_to_oneself(user_from, list_user_to):
    return len(list_user_to) == 1 and list_user_to[0].name == user_from.name
#-----------------------------------------------------------------------------#


class AtomCRUDType:
    create = 'c'
    update = 'u'
    delete = 'd'


class Flow(Base):
    __tablename__ = 'flow'
    id = Column(Integer, primary_key=True)
    uuid = Column(String(50), nullable=False)
    creator = Column(String(50), nullable=False)
    assignee = Column(String(50), nullable=True)
    protocol = Column(Integer, nullable=False)
    state = Column(String(50), nullable=True)
    subject = Column(String(255), nullable=True)
    users = Column(String(255), nullable=False)
    data = Column(String(4096), nullable=True)

    @staticmethod
    def _generate_uuid():
        return base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', '')

    @staticmethod
    def get_flow_by_id(flowid):
        try:
            return db_session.query(Flow).filter(Flow.id == int(flowid)).one()
        except Exception as e:
            return None

    @staticmethod
    def get_flow_by_uuid(flow_uuid):
        try:
            return db_session.query(Flow).filter(Flow.uuid == flow_uuid).one()
        except Exception as e:
            return None

    def refresh(self):
        db_session.refresh(self)

    def sync(self, forUser):
        _get_messages_via_transport_layer(self, forUser)

    def send_message(self, atom, receivers):
        # remove sender from receiver list
        to_users = [u for u in receivers if u != atom.sender]
        if len(to_users) > 0:
            self._send_message_via_transport_layer(atom.sender, to_users, atom)
        self.add_atom(atom)

    def add_atom(self, atom):
        logging.info('updating flow ' + str(self.id) + ' with atom, crud flag: ' + atom.crud_flag)
        if atom.state: self.state = atom.state
        if atom.data: self.data = atom.data
        if atom.crud_flag == AtomCRUDType.create:
            self.atoms.append(atom)
        else:
            if len(self.atoms) > 0 and \
               self.atoms[-1].sender == atom.sender:
                if atom.crud_flag == AtomCRUDType.update:
                    self.atoms[-1] = atom
                elif atom.crud_flag == AtomCRUDType.delete:
                    self.atoms.pop()
                    if len(self.atoms) == 0:
                        db_session.delete(self)
            else:
                logging.info('atom to be updated/deleted already replied to. adding a new atom')
                self.atoms.append(atom)
        db_session.commit()

    def delete(self):
        db_session.delete(self)
        db_session.commit()

    def _send_message_via_transport_layer(self, sender, receivers, atom):
        try:
            for r in receivers:
                xmpp.send_flow_message(r, atom, self)
            '''
            if sender.message_transport_type == 'email' and r.message_transport_type == 'email':
                imapsmtp.send_flow_message(sender.email, sender.password, [r.email for r in receivers], atom, self)
            '''
        except Exception:
            logging.exception('Error sending message via transport layer')

    def _get_messages_via_transport_layer(self, forUser):
        if forUser.message_transport_type == 'email':
            try:
                imapsmtp.fetch_flow_from_imap(forUser.email, forUser.password, '', self)
            except Exception:
                logging.exception('Error fetching messages from IMAP')
        else:
            self.refresh()


class Atom(Base):
    __tablename__ = 'atom'
    id = Column(Integer, primary_key=True)
    sender = Column(String(50), nullable=False)
    crud_flag = Column(String(10), nullable=False)
    state = Column(String(50), nullable=True)
    body = Column(String(255), nullable=True)
    data = Column(String(255), nullable=True)
    flow_id = Column(Integer, ForeignKey('flow.id'))
    flow = relationship("Flow", backref=backref('atoms', order_by=id))
    created = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)


class Contact(Base):
    __tablename__ = 'contact'
    id = Column(String, primary_key=True)

    @staticmethod
    def get_contacts():
        return db_session.query(Contact).all()

    @staticmethod
    def get_contact(userid):
        try:
            return db_session.query(Contact).filter(Contact.id == userid).one()
        except:
            return None

    @staticmethod
    def add_contact(userid):
        contact = Contact(userid)
        db_session.add(contact)
        db_session.commit()
        return contact

    def delete_contact(self):
        db_session.delete(self)
        db_session.commit()


def make_new_flow(portid, sender, toUsers, subject, flow_uuid):
    allUsers = toUsers
    if not sender in allUsers:
        allUsers.append(sender)
    flow = Flow(subject=subject, creator=sender, assignee=toUsers[0], protocol=portid, users=','.join(allUsers))
    if flow_uuid == None:
        flow.uuid = Flow._generate_uuid()
    else:
        flow.uuid = flow_uuid
    flow.atoms = []
    db_session.add(flow)
    db_session.commit()
    return flow


def find_flows(any_user, from_user, assignee, pid, states):
    q = db_session.query(Flow)
    if any_user:
        q = q.filter(Flow.users.like('%' + any_user + '%'))
    if from_user:
        q = q.filter(Flow.creator == from_user)
    if assignee:
        q = q.filter(Flow.assignee == assignee)
    if pid:
        q = q.filter(Flow.protocol == pid)
    if states:
        q = q.filter(Flow.state.in_(states))
    return q.all()


def message_received_handler(protocolid, flow_uuid, sender, receiver, state, data, body, crud_flag):
    logging.debug('message_receive_handler called')
    atom = Atom(sender=sender, state=state, data=data, body=body, crud_flag=crud_flag)
    flow = Flow.get_flow_by_uuid(flow_uuid)
    if not flow:
        logging.debug('flow doesn\'t exist, creating a new one in db')
        flow = make_new_flow(protocolid, sender, [receiver], None, flow_uuid)
    flow.add_atom(atom)
    logging.info('received msg saved to flow : ' + str(flow.id))
    if app_notify_handler:
        app_notify_handler(flow.id, sender, body[:40])

    # for self protocol, process and return automated response
    p = client.get_port(protocolid)
    if p and p['name'] == 'Self' and state not in ["Answer","Reject"]:
        result_data = self_port.process_cmd(state, data)
        u = client.get_user(receiver)
        if u:
            if result_data: state = "Answer"
            else: state = "Reject"
            atom = Atom(sender=receiver, state=state, data=result_data, body=None, crud_flag=AtomCRUDType.create)
            flow.send_message(atom, [sender])
        return


def login_to_transport_layer(u, password):
    logging.info('logging in user: transport ' +  str(u.message_transport_type)  + ', user: ' + u.name)
    if u.message_transport_type == 'email':
        authenticated = imapsmtp.login(u.email, password)
    else:
        authenticated = xmpp.login(u.name, u.name, message_received_handler)
    logging.info('authenticated: ' + str(authenticated))
    return authenticated


def logout_from_transport_layer(u):
    if u.message_transport_type == 'email':
        imapsmtp.logout()
    else: 
        xmpp.logout()
    logging.info('logged out: ' + u.name)


def init_db_session(dbfile='flows.db'):
    global db_session
    engine = create_engine('sqlite:///' + dbfile)
    Base.metadata.create_all(engine)
    db_session = scoped_session(sessionmaker(bind=engine))
    logging.info('inited db session')


def get_db_session():
    return db_session


def close_db_session():
    if db_session:
        db_session.close()
        logging.info('closed db session')


def set_notify_handler(handler):
    global app_notify_handler
    app_notify_handler = handler
    
 
if __name__ == '__main__':
    init_db_session()
    flow = Flow()
    flow.atoms = [ Atom(body='hi there'), Atom(body='how are you') ]
    db_session().add(flow)
    db_session().commit()
    db_session.close()
