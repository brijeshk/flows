#!/usr/bin/env python
"""
System User XMPP Bot.
This bot receives system user commands, processes them, and sends back messages with results 
"""

import sys
import logging
from optparse import OptionParser
import time
import threading
import sleekxmpp
import os
import signal
from flows import *
import importlib
import json


# Python versions before 3.0 do not use UTF-8 encoding
# by default. To ensure that Unicode is handled properly
# throughout SleekXMPP, we will set the default encoding
# ourselves to UTF-8.
if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding('utf8')
else:
    raw_input = input


# GLOBALS
#-------------------------------------------#
xmppSession = None
XMPP_SERVER = ('autometon.com', 5222)
#XMPP_SERVER = ('localhost', 5222)
systemUserId = None
#-------------------------------------------#


# SYSTEMBOT CLASS
#-------------------------------------------#
class SystemBot(sleekxmpp.ClientXMPP):
    """
    XMPP client to send/receive system user messages over XMPP.
    """
    thread = None
    callback_receive_func = None

    def __init__(self, jid, password, cb_receive_func=None):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)
        self.add_event_handler("session_start", self.start)
        self.add_event_handler("message", self.receive_flow_message)
        self.callback_receive_func = cb_receive_func

    def start(self, event):
        self.send_presence()
        self.get_roster()
        logging.info('XMPP client connected')

    def receive_flow_message(self, msg):
        if msg['type'] in ('chat', 'normal'):
            xmppmsg = msg['body']
            sender = getUserIdFromXmppJid(msg['from'].user)
            receiver = getUserIdFromXmppJid(msg['to'].user)
            if xmppmsg.startswith('MP##'):
                msgfields = xmppmsg.split('##')
                try:
                    header = json.loads(msgfields[1])
                    if not 'data' in header:
                        header['data'] = None
                    else:
                        # replace stringized version of data json blob with dict version
                        header['data'] = json.loads(header['data']) 
                    body = msgfields[2] if len(msgfields) > 2 else None
                except:
                    logging.info('Received invalid meta header: ' + msgfields[1])
                    return
            else:
                logging.info('Received non-flow msg: ' + body + ' from ' + sender)
                return
            logging.info('received flow atom: {0} {1} {2}'.format(header['fid'], header['state'], header['data']))
            logging.info('body: ' + str(body))
            systemMessageHandler(header['pid'], header['fid'],
                                 sender, receiver,
                                 header['state'], header['data'])

    def send_flow_message(self, to_jid, body):
        self.send_message(mto=to_jid,
                          mbody=body,
                          mtype='chat')
        logging.info('sent response message to {0} : {1}'.format(to_jid, body))

    def block_process(self):
        self.process(block=True)

    def start_listen_thread(self):
        self.thread = threading.Thread(target=self.block_process)
        self.thread.start()

    def stop_listen_thread(self):
        self.disconnect(wait=True)
        self.wait_for_process_thread()

    def wait_for_process_thread(self):
        self.thread.join()
#-------------------------------------------#


# GLOBAL FUNCTIONS
#-------------------------------------------#
def getXmppJidFromUserId(userId):
    return userId.replace('@gmail.com', '_gmail_com')


def getUserIdFromXmppJid(jid):
    return jid.replace('_gmail_com', '@gmail.com')


def loginToXmppServer(user_id, password, callback_receive_func=None):
    global xmppSession
    jid = getXmppJidFromUserId(user_id)
    logging.info('logging in user to xmpp : ' + jid)
    try:
        xmppSession = SystemBot(jid + '@localhost', jid, callback_receive_func)
        xmppSession.register_plugin('xep_0030') # Service Discovery
        xmppSession.register_plugin('xep_0199') # XMPP Ping
        if not xmppSession.connect(XMPP_SERVER):
            logging.error('Unable to connect to XMPP server')
            return False
        xmppSession.start_listen_thread()
        logging.info('XMPP session started')
        return True
    except:
        logging.exception('Error connecting to XMPP server')
        return False


def logoutFromXmppServer():
    logging.info('Logging out')
    global xmppSession
    if xmppSession:
        xmppSession.stop_listen_thread()
        logging.info('XMPP session disconnected')
        xmppSession = None


def sendFlowMessageViaXmpp(receiver, atom, flow):
    meta_header = { 'fid' : flow.uuid,
                    'pid' : flow.protocol,
                    'flag' : atom.crud_flag,
                    'state' : atom.state,
                    'data' : atom.data
                  }
    msg = 'MP##' + json.dumps(meta_header) + '##'
    if atom.body:
        msg += atom.body
    xmppSession.send_flow_message(getXmppJidFromUserId(receiver) + '@localhost', msg)


def sigtermHandler(signum, frame):
    print 'Received termination signal'
    logoutFromXmppServer()


def systemMessageHandler(portId, flowId, fromUserId, toUserId, cmdToExecute, dataInput):
    # execute the command
    try:
        cmdResult = executePortCommand(flowId, portId, cmdToExecute, dataInput)
        if cmdResult:
            data = json.dumps(cmdResult)
        else:
            data = None
        state = 'Result'
    except Exception as e:
        logging.exception('Error from port command execution: ')
        data = json.dumps({ 'Error' : { 'type' : 'Error', 'value' : str(e) } })
        state = 'Error'

    # build the response
    anAtom = Atom(flow_id=flowId, sender=toUserId, body=None, state=state, data=data, crud_flag=AtomCRUDType.create)
    theFlow = Flow(uuid=flowId, protocol=portId)

    # send to client
    sendFlowMessageViaXmpp(fromUserId, anAtom, theFlow)


def executePortCommand(flowid, portid, command, data):
    # load module connectors/[systemUserId].py
    m = importlib.import_module('connectors.' + systemUserId)
    # run command
    function = getattr(m, 'processCommand')
    return function(flowid, portid, command, data)
#-------------------------------------------#


# MAIN HANDLER
#-------------------------------------------#
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)-8s %(message)s')

    optp = OptionParser()
    optp.add_option("-j", "--jid", dest="jid",
                    help="JID to use")
    optp.add_option("-p", "--password", dest="password",
                    help="password to use")
    opts, args = optp.parse_args()

    if opts.jid == None or opts.password == None:
        print 'Usage: systembot -j user -p password'
        sys.exit(1)

    systemUserId = opts.jid

    # setup termination handler
    signal.signal(signal.SIGINT, sigtermHandler)

    # login to XMPP and process requests
    if not loginToXmppServer(opts.jid, opts.password, None):
        print 'Error logging in'
        sys.exit(1)

    raw_input('Press Ctrl-C to terminate.')
#-------------------------------------------#
