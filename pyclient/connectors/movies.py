import requests
import re

"""
MOVIES PORT DEFINITION
    _id: 5
    commands: 
        Find: 
            data: 
                Location: 
                    type: location
                    source: user
                Keyword: 
                    type: string
                    source: user
            transitions: [Result]
            botCommand: true
        Result: 
            data: 
                Nearby Movies: 
                    type: "[string]"
            transitions: [Pick]
            botCommand: false
        Pick:
            data: 
                Select: 
                    type: string
                    source: Result
                    inputAction: select1
            botCommand: false
    creator: movies
    name: Movies
    visibleTo: []
"""


gTMSAPIKey = 'jpnsrkhvqfkd8c6ynvzbugjp'


def getMoviesNearZipCode(zipcode, keyword):
    """
    Gets movie listings near zip code. Filters by optional keyword.
    """
    if not zipcode:
        raise Exception('Null zipcode provided')

    r = requests.get('http://data.tmsapi.com/v1.1/movies/showings?startDate=2015-08-22&zip=' 
                     + zipcode
                     + '&api_key=' + gTMSAPIKey)    
    if r.status_code != 200:
        raise Exception('Error from API: ' + str(r.status_code))

    # parse output and assemble result data structure
    movieInfo = []
    movieList = r.json()
    for m in movieList:
        for st in m['showtimes']:
            movie = { 'title' : m['title'], 'theatre' : st['theatre']['name'] }
            # TODO: filter results by optional keyword
            movieInfo.append(movie['title'] + ' at ' + movie['theatre'])
    resultData = { 'Nearby Movies' : { 'type' : '[string]', 'value' : movieInfo } }
    return resultData


def processCommand(flowid, portid, cmd, data):
    """
    Entry function to movies bot.
    Process command and report results.
    """
    if cmd == 'Find':
        # get the zipcode from data
        zipcode = data['Location']['value']
        keyword = data['Keyword']['value']
        return getMoviesNearZipCode(zipcode, keyword)
    else:
        raise Exception('Unknown command ' + cmd)


if __name__ == '__main__':
    movieList = getMoviesNearZipCode('94404', None)
    print movieList
