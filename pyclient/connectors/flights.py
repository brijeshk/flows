"""
google QPX flights api bot
takes: date|to|from
returns: top 10 results
"""
import requests
import protocols
import json
from datetime import *

def get_flights(orgn, dest, date):
    """
    Gets flight times for given date, to and from. 
    """
    results = []
    
    payload = {'request': { 'passengers': { 'adultCount': "1"}, 'slice': [{'origin': orgn,'destination': dest,'date': date}],'solutions': "10"}}
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    url = 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyDTVXrmXr27HhwrIbQb9eOeT2FURZ9TSY0'
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    
    if r.status_code != 200:
        raise Exception("Error from QPX API: " + str(r.status_code))

    response = r.json()

    for op in range(len(response['trips']['tripOption'])):
        trip_option = response['trips']['tripOption'][op]
        (price,total_duration, split_duration,airport_trail, stops, departure, arrival, carrier,link) = 0,0,"",[],-1,"","","",""
        
        price = trip_option['saleTotal']
        
        for sli in range(len(trip_option['slice'])):
            slice_option = trip_option['slice'][sli]
            total_duration = total_duration + slice_option['duration']
            
            for seg in range(len(slice_option['segment'])):
                segment_option = slice_option['segment'][seg]
                carrier = segment_option['flight']['carrier']
                
                for leg in range(len(segment_option['leg'])):  
                    stops+=1
                    leg_option = segment_option['leg'][leg]   
                    origin = leg_option['origin']
                    destination = leg_option['destination']
                    if origin not in airport_trail:
                        airport_trail.append(origin) 
                    if destination not in airport_trail:
                        airport_trail.append(destination) 

                    if departure == "":
                        departure =  leg_option['departureTime'][11:16]
                    arrival = leg_option['arrivalTime'][11:16]
                    #arrival = convert_24_to_12(arrival)
                    #departure = convert_24_to_12(departure)
        
        airport_trail = "-".join(airport_trail)
        total_duration = str(timedelta(minutes=total_duration))
        if stops == 0:
            stops = "Nonstop"
        elif stops == 1:
            stops = "1-stop"
        else:
            stops = str(stops) + "-" + "stops"
        """
        result = {
            'Option' : op+1,
            'Price' : price,
            'Duration' : total_duration,
            'Route' : airport_trail,
            'Stops' : stops,
            'Departure Time' : departure,
            'Arrival Time' : arrival,
            'Carrier' : carrier,
            'Link' : link
        }
        """
        line = "\t".join([str(op+1),price,total_duration,airport_trail,stops,departure,arrival,carrier,link])
        results.append(line)

    return { 'Flights' : { 'type' : '[string]', 'value' : results } }


def processCommand(flowid, portid, command, data):
    """
    Command router. This is the entry point method called by systembot.
    """
    if command == 'Get':
        fromPlace = data['From']['value']
        toPlace = data['To']['value']
        dateOfTravel = data['Date']['value'].replace('/','-')
        return get_flights(fromPlace, toPlace, dateOfTravel)
    else:
        raise Exception('Unknown command : ' + command)


if __name__ == '__main__':
    print get_flights('SFO', 'CHI', '2014-10-10')
