import requests
import feedparser
import json
import logging

"""
FEED PORT DEFINITION
    _id: 15
    commands: 
        Fetch: 
            data: 
                FeedSource: 
                    type: string
                    value: [ "CNN", "Hacker News", "BBC" ]
                    inputAction: select1
                    source: user
            transitions: [Result]
            botCommand: true
        Result: 
            data: 
                Items: 
                    type: "[string]"
            botCommand: false
    creator: feeds
    name: Feed
    visibleTo: []
"""


gFeedUrlMap = { 'CNN' : 'http://rss.cnn.com/rss/cnn_topstories.rss',
                'Hacker News' : 'https://news.ycombinator.com/rss',
                'BBC' : 'http://feeds.bbci.co.uk/news/rss.xml'
              }

gFlowLastUpdatedMap = {}


def fetchFeed(flowId, url):
    """
    Fetches RSS feed from given URL, only updating items from last fetch.
    """
    if not url:
        raise Exception('Null URL provided')

    # get current etag for this flow
    etagOrLastModified = gFlowLastUpdatedMap.get(flowId, None) 
    e = None
    m = None
    if etagOrLastModified:
        e = etagOrLastModified.split('__')[0]
        if e == '': e = None
        m = etagOrLastModified.split('__')[1]
        if m == '': m = None

    # fetch from remote
    items = []
    feed = feedparser.parse(url, etag=e, modified=m)
    
    if feed.status == 200:
        # update etag in global flow map
        try:
            gFlowLastUpdatedMap[flowId] = feed.etag + '__'
        except:
            try:
                gFlowLastUpdatedMap[flowId] = '__' + feed.modified
            except:
                logging.info('Remote Feed URL supports neither Etag nor Last-Modified header')

        # add to result set
        for entry in feed.entries:
            item = '<a href="' + entry.link + '">' + entry.title + '</a>'
            items.append(item)

    resultData = { 'Feed' : { 'type' : '[string]', 'value' : items } }    
    return resultData


def processCommand(flowid, portid, cmd, data):
    """
    Entry function for bot.
    """
    if cmd == 'Fetch':        
        feedName = data['FeedSource']['value']
        if not feedName in gFeedUrlMap:
            raise Exception('Unknown feed ' + feedName)

        feedUrl = gFeedUrlMap[feedName]
        return fetchFeed(flowid, feedUrl)    
    else:        
        raise Exception('Unknown command ' + cmd)


if __name__ == '__main__':
    feedItems = fetchFeed('http://news.ycombinator.com/rss', None)
    print json.dumps(feedItems, indent=4)
