"""
Yahoo stock api bot
"""

import requests
import protocols
import json

"""
STOCK PORT DEFINITION
    _id: 6
    commands: 
        Get: 
            data: 
                Symbol: 
                    type: string
                    source: user
                    desc: Enter Symbol
            transitions: [Result]
            botCommand: true
        Result: 
            data: 
                Quote: 
                    type: string
            botCommand: false
    creator: metonbot
    name: Stock
    visibleTo: []
"""


def processCommand(eid, symbol):
    """
    Gets stock quote for a given symbol.
    """
    if symbol == '':
        raise Exception('Text me like this<br>/stock GOOG')

    r = requests.get("http://download.finance.yahoo.com/d/quotes.csv?s=" + symbol + "&f=nl1")
    if r.status_code != 200:
        raise Exception("Error from Stock API: " + str(r.status_code))

    fields = r.text.strip().split('",')
    if len(fields) < 2:
        raise Exception('Invalid response returned from Stock API: ' + r.text)

    name = fields[0].replace('"','')
    price = fields[1]
    result = name + " is trading at " + price
    return result

if __name__ == '__main__':
    print get_stock_quote('GOOG')
