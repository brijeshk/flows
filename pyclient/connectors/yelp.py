"""
yelp api bot
takes: term|location(flex address)
returns: top 10 results
"""
import rauth
import time
import json
import itertools

"""
YELP PORT DEFINITION
    _id: 14
    commands: 
        Get: 
            data: 
                Term: 
                    type: string
                    source: user
                Location: 
                    type: string
                    source: user
            transitions: [Result]
            botCommand: true
        Result: 
            data: 
                Results: 
                    type: "[string]"
            transitions: [Pick]
            botCommand: false
        Pick: 
            data: 
                Select: 
                    type: url
                    source: Result
                    inputAction: select1
            botCommand: false
    creator: yelp
    name: yelp
    visibleTo: []
"""


def get_results(results):
    """
    Takes api results in json.text format from get_api_results()
    returns json object with list of results (image url not included yet)
    """
    results_list = []
    results = json.loads(results)
    for biz in range(len(results['businesses'])):
        business = results['businesses'][biz]
        (rating,name, review_count,url,phone,is_closed,categories, address, city, state, postal_code) = 0,"",0,"","","","","","","",""
        try:
            rating = business['rating']
            review_count = business['review_count']
            name = business['name']
            url = business['url']
            address = " ".join(business['location']['address'])
            city = business['location']['city']
            postal_code = business['location']['postal_code']
            state = business['location']['state_code']
            categories = [x.lower() for x in list(itertools.chain(*business['categories']))]
            categories = ",".join(list(set(categories)))
            line = '<a href="' + url + '">' + name + ' (rating: ' + str(rating) + ')' + '</a>'
            results_list.append(line)
        except KeyError:
            # Key is not present
            pass			
    return '<br>'.join(results_list)


def get_api_results(params):
	"""
	Sends request to Yelp and downloads results.
	"""
	#Obtain these from Yelp's manage access page
	consumer_key = "7mDSqgRR2wR82IX3nkfbQg"
	consumer_secret = "0o0pD9bdSPr7uGA_YzgM_NpkYC4"
	token = "29Rkdw8h2etfYgtjzvBYB_vmb_FiYXUC"
	token_secret = "blijmM-svDQXdSoI0KPDczGUnNU"
	
	session = rauth.OAuth1Session(
		consumer_key = consumer_key
		,consumer_secret = consumer_secret
		,access_token = token
		,access_token_secret = token_secret)

	request = session.get("http://api.yelp.com/v2/search",params=params)
	
	#Transforms the JSON API response into a Python dictionary
	data = request.text
	session.close()
	
	return data


def get_search_parameters(term,location):
    #See the Yelp API for more details
    params = {}
    params["term"] = term
    if ';' in location:
        params["ll"] = location.replace(";",",")
    else:
        params["location"] = location
    params["radius_filter"] = "5000"
    params["limit"] = "10"
    return params


def processCommand(eid, msg):
    """
    Command router. This is the entry point method called by systembot.
    """
    fields = msg.split(' ')
    if len(fields) >= 2:
        term = fields[0]
        location = fields[1]
        params = get_search_parameters(term,location)
        api_results = get_api_results(params)
        return get_results(api_results)
    else:
        raise Exception('Text me like this<br>/yelp food 94404')


if __name__ == '__main__':
	params = get_search_parameters('restaurant','94401')
 	api_results = get_api_results(params)
	print get_results(api_results)

