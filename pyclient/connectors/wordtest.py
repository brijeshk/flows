import requests
import protocols
import json

def get_stock_quote(symbol):
    """
    Gets stock quote for a given symbol.
    """
    if not symbol:
        return None

    r = requests.get("http://download.finance.yahoo.com/d/quotes.csv?s=" + symbol + "&f=nl1")
    if r.status_code != 200:
        raise Exception("Error from Stock API: " + str(r.status_code))

    fields = r.text.strip().split(',')
    if len(fields) < 2:
        raise Exception('Invalid response returned from Stock API: ' + r.text)

    name = fields[0].replace('"','')
    price = fields[1]
    result = { "Name" : { "type" : "string", "value" : name }, "Price" : { "type" : "price", "value" : price } }
    return result


def processCommand(flowid, portid, command, data):
    print 'Received command ' + command + ' with data ' + str(data)
    """
    Command router. This is the entry point method called by systembot.
    """
    if command == 'Get':
        symbol = data['Symbol']['value']
        return get_stock_quote(symbol)
    else:
        raise Exception('Unknown command : ' + command)


if __name__ == '__main__':
    print get_stock_quote('GOOG')
