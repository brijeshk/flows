"""
translate bot
takes: string, lang
returns: translated string
"""
import goslate
import requests

gs = goslate.Goslate()


def translate(text, lang):  
    code = None
    lang_codes = gs.get_languages()
    if len(lang) > 2:
        lang = lang.title()
        for c,l in lang_codes.items():
            if l == lang:
                code = c
                break
    elif lang in lang_codes:
        code = lang

    if code == None:
        raise Exception('Unknown language: ' + lang)

    translated = gs.translate(text, code)
    return translated


def processCommand(eid, msg):
    """
    Command router. This is the entry point method called by systembot.
    """
    fields = msg.split(':', 1)
    if len(fields) >= 2:
        lang = fields[0].strip()
        text = fields[1].strip()
        return translate(text, lang)
    else:
        raise Exception('Text me like this<br>/translate French : this is cool')


if __name__ == '__main__':
    print translate('this is very nice', 'fr')

