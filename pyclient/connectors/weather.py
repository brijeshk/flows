"""
Simple Weather Bot.
"""
import requests

API_KEY='23f53082767e71a45632ecf0ca1f8ac7'

def get_weather_forecast(cityOrZip):
    """
    Gets weather forecast for a given city or US zipcode.
    """
    if not cityOrZip:
        return None

    r = requests.get('http://api.openweathermap.org/data/2.5/weather?APPID=' + API_KEY + '&q=' + cityOrZip)
    if r.status_code == 200:
        j = r.json()
        return j['weather'][0]['description']
    else:
        raise Exception("Error from OpenWeather API: " + str(r.status_code))


def processCommand(eid, msg):
    if msg != '':
        return get_weather_forecast(msg)
    else:
        raise Exception('Text me like this<br>/weather London<br>/weather 94404')
