"""
google search bot
takes: term
returns: top 8 results
"""
import time
import json
import requests

def get_search_results(query):  
    r = requests.get('http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=8&q=' + query)
    j = r.content
    results = json.loads(j)
    list = []
    for index,result in enumerate(results['responseData']['results']):
        list.append('<a rel="external" href="' + result['url'] + '">' + result['titleNoFormatting'] + '</a>');
    return '<br>'.join(list)


def processCommand(eid, msg):
    """
    Command router. This is the entry point method called by systembot.
    """
    if msg == '':
        raise Exception('Text me like this<br>/google cupcake recipes')
    return get_search_results(msg)


if __name__ == '__main__':
    list = get_search_results('roof shingles')
    print '\n'.join(list)
