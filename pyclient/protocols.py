"""
Protocols module.
"""

import graph

#-------------------------------------------#
class DataType:
    NoData                    = 0
    String                    = 1
    KeyValueMap               = 2
    MultiChoiceString         = 3
    MultiChoiceTime           = 4
    Quantity                  = 5
    Price                     = 7
    Location                  = 8
    Time                      = 9
    Image                     = 10
    Event                     = 11
    URL                       = 12
#-------------------------------------------#


#-------------------------------------------#
def get_command_graph(port):
    cmdGraph = {}
    for c in port['commands'].keys():
        t = port['commands'][c]['transitions']
        if t != None:
            cmdGraph[c] = t
    return cmdGraph

def get_valid_transition_commands(port, command, data=None):
    cmd_set = None
    if command: # there's a prior state, get valid transitions
        g = get_command_graph(port)
        cmd_set = g.get(command, set())
        cmd_set.add('Say')
    else: # no prior state, get start states
        cmd_set = graph.get_roots(get_command_graph(port)) 
        if port['canStartWithSay']:
            cmd_set.add('Say')
    return cmd_set

def get_action_commands(port):
    return graph.get_sources(get_command_graph(port))

def is_auto_transition_command(command):
    return (len(command) > 0) and command[0].islower()

def is_messageable_command(port, command):
    return command != 'select' and command != 'Info'
#-------------------------------------------#


