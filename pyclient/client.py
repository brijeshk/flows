"""
Client wrapper to interact with port server.
"""

import requests
import logging
import protocols
import json
import utils
import users

HOST = 'http://localhost:5000'
USERID = None
PASSWD = None

def set_host(host):
    global HOST
    HOST = host

def set_creds(userid, passwd):
    global USERID, PASSWD
    USERID = userid
    PASSWD = passwd

def login_user(username, token):
    r = requests.get(HOST + '/users/login',
                     auth=(username, token))
    return (r.status_code == 200)

def register_user(username, password, email, transport):
    d_user = { 'username' : username, 'email': email, 'transport' : transport, 'password' : password }
    requests.post(HOST + '/users', 
                  data=json.dumps(d_user),
                  headers={'Content-type': 'application/json'})

def update_user(username, old_password, new_password, email, transport):
    d_user = { 'username' : username, 'email': email, 'transport' : transport, 'password' : new_password }
    requests.put(HOST + '/users', 
                 data=json.dumps(d_user),
                 auth=(username, old_password),
                 headers={'Content-type': 'application/json'})

def get_ports_exposed_by_user(byUser):
    r = requests.get(HOST + '/ports/user/' + byUser)
    if r.status_code != 200:
        raise Exception(r.text)
    logging.debug(r.json())
    return r.json()

def get_ports_exposed_by_user_to_user(byUser, toUser):
    r = requests.get(HOST + '/ports/user/' + byUser + '/' + toUser)
    if r.status_code != 200:
        raise Exception(r.text)
    logging.debug(r.json())
    return r.json()

def get_ports_visible_to_user(username, keyword):
    r = requests.get(HOST + '/ports/visible/' + username, params={'q':keyword, 'publicPorts':'true'})
    logging.debug(r.json())
    return r.json()

def get_templates_visible_to_user(username, keyword):
    r = requests.get(HOST + '/ports/templates', params={'q':keyword})
    logging.debug(r.json())
    return r.json()

def get_port(pid):
    r = requests.get(HOST + '/ports/id/' + str(pid))
    logging.debug(r.json())
    return r.json()

def fire_command(port, cmd, data):
    params = { 'cmd' : cmd, 'data' : data }
    r = requests.get(HOST + '/ports/id/{0}/fire_cmd'.format(port.id), params=params)
    j = r.json()
    logging.debug(j)
    return j[0], j[1]

def post_flow_delta(userid, flows):
    requests.post(HOST + '/flowDelta/' + userid + '?notify=true', 
                  data=json.dumps(flows), 
                  headers={'Content-type' : 'application/json'})

def post_event_msg(atom):
    requests.post(HOST + '/eventAtoms',
                  data=json.dumps(atom),
                  auth=(USERID, PASSWD),
                  headers={'Content-type' : 'application/json'})

'''
def get_connected_users(user):
    r = requests.get(HOST + '/users')
    logging.debug(r.json())
    return [users.User(d) for d in r.json()]
'''

def add_user_connection(user1, user2):
    requests.put(HOST + '/contacts/' + user1 + '/' + user2)

def remove_user_connection(user1, user2):
    requests.delete(HOST + '/contacts/' + user1 + '/' + user2)

def get_user(username):
    r = requests.get(HOST + '/users/id/' + username)
    logging.debug(r.json())
    return users.User(r.json())

def new_port(port):
    requests.post(HOST + '/ports', 
                  data=json.dumps(port),
                  headers={'Content-type': 'application/json'})


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(levelname)s %(funcName)s: %(message)s')
    ports = get_ports_exposed_by_user('ramya')
    print(ports)
