#!/usr/bin/python

import urwid
import flows
import client
import protocols
import urwid.html_fragment
import logging
import utils
import argparse
import getpass
import json
from collections import OrderedDict


# GLOBALS
#---------------------------------------------------------------------#    
me = None
my_contacts = []
my_visible_ports = []
my_visible_templates = []

FLOW_FORMAT_STR = '{: >4} {: >20} {: >30} {: >20} {: >30}'
FLOW_FORMAT_HDR = [ 'id', 'gate', 'with', 'last-action', 'last-message' ]
FLOWGROUP_FORMAT_STR = '{: >30} {: >30}'
FLOWGROUP_FORMAT_HDR = [ 'with', 'gate' ]
PORT_FORMAT_STR = '{: >8} {: >30} {: >15}'
PORT_FORMAT_HDR = [ 'id', 'name', 'owner' ]
KEY_HELPER_STR = 'q=Quit n=New /=Filter f=Flows g=FlowGroups w=Waiting t=ToDo r=Reply <-=Back p=VisibleGates l=Templates m=MyGates d/D=Delete R=Refresh c=Contacts'
#---------------------------------------------------------------------#


# UTIL functions
#---------------------------------------------------------------------#
def strme(user, me):
    return str(user) if user != me else 'me'

def get_port_by_name(name):
    for p in my_visible_ports:
        if p['name'] == name: return p
    return None

def get_port_by_id(id):
    for p in my_visible_ports:
        if p['id'] == id: return p
    try:
        p = client.get_port(id)
        my_visible_ports.append(p)
        return p
    except:
        return None

def get_template_by_id(id):
    for p in my_visible_templates:
        if p['id'] == id: return p
    return None

def get_user_by_id(id):
    for u in my_contacts:
        if u.id == id: return u
    try:
        u = client.get_user(id)
        my_contacts.append(u)
        return u
    except:
        return None
#---------------------------------------------------------------------#


# VIEW TYPES
#---------------------------------------------------------------------#    
class ViewType:
    FlowList = 1
    PortList = 2
    UserList = 3
    TemplateList = 4
    FlowDetail = 5
    PortDetail = 6
    UserDetail = 7
    TemplateDetail = 8
    FlowGroupList = 9
#---------------------------------------------------------------------#    

# TERMINAL COLORS
#---------------------------------------------------------------------#    
class TerminalColors:
    HEADER = '\033[95m'
    OKGREEN = '\033[92m'
    OKBLUE = '\033[94m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
#---------------------------------------------------------------------#    

# FLOW ITEM WIDGET
#---------------------------------------------------------------------#    
class ItemWidget(urwid.WidgetWrap):
    def __init__(self, id, text):
        self.id = id
        self.description = urwid.AttrWrap(urwid.Text(text), 'body', 'focus')
        self.content = text
        self.__super.__init__(self.description)
    def selectable (self):
        return True
    def keypress(self, size, key):
        return key
#---------------------------------------------------------------------#


# POPUP MENU WIDGET
#---------------------------------------------------------------------#
class PopUpDialog(urwid.WidgetWrap):
    signals = ['close']
    def __init__(self):
        body = [urwid.Text(title), urwid.Divider()]
        choices = [ 'Choice1', 'Choice2' ]
        for c in choices:
            button = urwid.Button(c)
            urwid.connect_signal(button, 'click', lambda button:self_emit("close"))
            body.append(urwid.AttrMap(button, None, focus_map='reversed'))
        choiceList = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        fill = urwid.Filler(choiceList)
        self.__super.__init__(urwid.AttrWrap(fill, 'popbg'))

class MyPopUpLauncher(urwid.PopUpLauncher):
    def __init__(self):
        self.__super.__init__(urwid.Button("click-me"))
        urwid.connect_signal(self.original_widget, 'click', lambda button: self.open_pop_up())

    def create_pop_up(self):
        pop_up = PopUpDialog()
        urwid.connect_signal(pop_up, 'close', lambda button: self.close_pop_up())
        return pop_up

    def get_pop_up_parameters(self):
        return {'left':0, 'top':1, 'overlay_width':32, 'overlay_height':7}
#---------------------------------------------------------------------#


# EDIT BOX WIDGET
#---------------------------------------------------------------------#
class CustomEdit(urwid.Edit):
    __metaclass__ = urwid.signals.MetaSignals
    signals = ['done','cmp']
    def keypress(self, size, key):
        if key == 'enter':
            urwid.emit_signal(self, 'done', self.get_edit_text())
            return
        elif key == 'esc':
            urwid.emit_signal(self, 'done', None)
            return
        elif key == ':':
            if not urwid.emit_signal(self, 'cmp', self.get_edit_text()+':'):
                return 
        elif key == '@':
            urwid.emit_signal(self, 'cmp', '')
            return
        urwid.Edit.keypress(self, size, key)
#---------------------------------------------------------------------#


# MAIN APP CLASS
#---------------------------------------------------------------------#
class MyApp(object):
    def __init__(self):
        self.cur_selected_flow = None
        self.cur_filtered_flows = None
        self.flow_groups = None
        palette = [
            ('body', '', '', 'standout'),
            ('focus', 'white', 'dark red', 'standout'),
            ('head', 'white', 'dark blue'),
            ('reversed', 'standout', '')
            ]
        walker = urwid.SimpleListWalker([])
        self.listbox = urwid.ListBox(walker)
        self.status_bar = None
        self.saved_view = None
        self.input_bar = None
        self.view_type = None
        self.view = urwid.Frame(urwid.AttrWrap(self.listbox, 'body'))
        flows.set_notify_handler(self.msg_received_handler)
        self.handler_flowgroups()
        loop = urwid.MainLoop(self.view, palette, unhandled_input=self.keystroke)
        loop.run()

    def msg_received_handler(self, flowId, sender, body):
        self.update_status('Received msg from ' + sender + ' in flow ' + str(flowId) + ': ' + body)

    def update_flow_groups_listbox(self):
        # build a map of flow groups (flows grouped by users and port)
        self.flow_groups = OrderedDict()
        for f in self.cur_filtered_flows:
            p = get_port_by_id(f.protocol)
            pn = p['name'] if p else 'Unknown'
            group_key = ','.join([u for u in f.users.split(',') if u != me.name]) + ':' + pn
            if group_key in self.flow_groups:
                self.flow_groups.get(group_key).append(f)
            else:
                self.flow_groups[group_key] = [f]
        items = []
        self.view.set_header(urwid.AttrMap(urwid.Text(FLOWGROUP_FORMAT_STR.format(*FLOWGROUP_FORMAT_HDR)), 'head'))
        for k,f in self.flow_groups.items():
            strlist = []
            u = k.split(':')[0]
            if u == '':
                u = 'me'
            g = k.split(':')[1]
            strlist.append(u)
            strlist.append(g)
            line = FLOWGROUP_FORMAT_STR.format(*strlist)
            item = ItemWidget(k, line)
            items.append(item)
            logging.info(k)
        self.listbox = urwid.ListBox(urwid.SimpleListWalker(items))
        self.view.set_body(self.listbox)
        self.view.set_focus('body')    
        self.view_type = ViewType.FlowGroupList
        
    def update_flows_listbox(self):
        items = []
        self.view.set_header(urwid.AttrMap(urwid.Text(FLOW_FORMAT_STR.format(*FLOW_FORMAT_HDR)), 'head'))
        for f in self.cur_filtered_flows:
            strlist = []
            strlist.append(str(f.id))
            port = get_port_by_id(f.protocol)
            if port is None:
                portname = 'Unknown'
            else:
                portname = port['name']
            strlist.append(portname)

            withUsers = ','.join([u for u in f.users.split(',') if u != me.name])
            if withUsers == '':
                withUsers = 'me'
            strlist.append(withUsers)

            s = f.atoms[-1].state
            if not s: s = 'Say'
            a = f.atoms[-1].sender
            if a == me.name:
                a = 'I ' + s.lower()
            else:
                a = a + ' ' + s.lower() + 's'
            strlist.append(a)
            
            if len(f.atoms) > 0 and f.atoms[-1].body:
                strlist.append(f.atoms[-1].body[:30])
            else:
                strlist.append('')
            line = FLOW_FORMAT_STR.format(*strlist)
            item = ItemWidget(f.id, line)
            items.append(item)
        # remember old list position and restore
        fpos = None
        if self.listbox is not None and self.view_type == ViewType.FlowList:
            try:
                fpos = self.listbox.focus_position
            except:
                pass
        self.listbox = urwid.ListBox(urwid.SimpleListWalker(items))
        self.view.set_body(self.listbox)
        if fpos:
            try:
                self.listbox.set_focus(fpos)
            except:
                pass
        self.view.set_focus('body')    
        self.view_type = ViewType.FlowList

    def update_ports_listbox(self, show_my_ports):
        items = []
        self.view.set_header(urwid.AttrMap(urwid.Text(PORT_FORMAT_STR.format(*PORT_FORMAT_HDR)), 'head'))
        if show_my_ports:
            ports = client.get_ports_exposed_by_user(me.name)
        else:
            ports = my_visible_ports
        for p in ports:
            strlist = []
            strlist.append(str(p['id']))
            strlist.append(p['name'])
            strlist.append(p['creator'])
            line = PORT_FORMAT_STR.format(*strlist)
            items.append(ItemWidget(p['id'], line))
        self.listbox = urwid.ListBox(urwid.SimpleListWalker(items))
        self.view.set_body(self.listbox)
        self.view.set_focus('body')
        self.view_type = ViewType.PortList

    def update_templates_listbox(self):
        items = []
        self.view.set_header(urwid.AttrMap(urwid.Text(PORT_FORMAT_STR.format(*PORT_FORMAT_HDR)), 'head'))
        for p in my_visible_templates:
            strlist = []
            strlist.append(str(p['id']))
            strlist.append(p['name'])
            strlist.append(p['creator'])
            line = PORT_FORMAT_STR.format(*strlist)
            items.append(ItemWidget(p['id'], line))
        self.listbox = urwid.ListBox(urwid.SimpleListWalker(items))
        self.view.set_body(self.listbox)
        self.view.set_focus('body')
        self.view_type = ViewType.TemplateList

    def update_footer(self, status_text, edit_text, edit_done_handler):
        self.status_bar = urwid.Columns([urwid.AttrMap(urwid.Text(status_text), 'head')])
        if edit_text:
            self.input_bar = CustomEdit(edit_text)
            urwid.connect_signal(self.input_bar, 'cmp', self.edit_complete_handler)
            urwid.connect_signal(self.input_bar, 'done', edit_done_handler)
        else:
            self.input_bar = urwid.Text(KEY_HELPER_STR)
        pile = urwid.Pile([self.status_bar, self.input_bar])
        self.view.set_footer(pile)
        if edit_text:
            self.view.set_focus('footer')
        else:
            self.view.set_focus('body')

    def update_status(self, text):
        self.update_footer(text, None, None)

    def edit_complete_handler(self, text):
        # parse currently entered text to determine what next to complete
        fields = text.split(':')
        choices = []
        status = None

        if len(fields) <= 1:
            # nothing there
            # return True
            if me.contacts:
                choices = me.contacts + [me.name]
            else:
                return True
        elif len(fields) <= 2:
            # user set, so find ports exposed by user
            users = fields[0].split(',')
            try:
                ports = client.get_ports_exposed_by_user_to_user(users[0], me.name)
                choices = [p['name'] for p in ports]
            except Exception as e:
                logging.exception('Exception getting ports for user ' + users[0])
                self.update_status('Error getting ports exposed by user : ' + str(e))
                return
        else:
            # user and port set
            users = fields[0].split(',')
            port = get_port_by_name(fields[1])
            
            if len(fields) <= 3:
                # no command present
                if self.cur_selected_flow:
                    cur_state = self.cur_selected_flow.state
                else:
                    cur_state = None
                choices = protocols.get_valid_transition_commands(port, cur_state)
                if choices is None or len(choices) == 0:
                    # no command
                    self.input_bar.insert_text('::')
                    status = 'Enter Message'
            else:
                # at least one command already present
                # now find if last fixed token is a command or data
                # user:port:cmd:data
                # OR
                # user:port:cmd:data:cmd
                # that will tell us if we need to complete data or cmd
                if len(fields) % 2 == 0: # even, need to complete data based on last cmd and data
                    cmd = fields[-2]
                    if cmd == '':
                        return True

                    # is there any previous data input?
                    data = fields[-3] if len(fields) > 4 else None

                    if cmd in port['commands']:
                        cmd_data = port['commands'][cmd]['data']
                    else:
                        cmd_data = None
                    if cmd_data:
                        # choice hints supported only for first data item
                        data_keys = cmd_data.keys()
                        data0 = cmd_data[data_keys[0]]
                        # input from user or previous state?
                        if 'source' in data0 and data0['source'] != 'user':
                            # find source state by walking back flow atoms
                            if self.cur_selected_flow:
                                for a in self.cur_selected_flow.atoms[::-1]:
                                    if 'source' in data0 and a.state == data0['source']:
                                        # use the data from there
                                        prev_cmd_data = json.loads(a.data)
                                        data_keys = prev_cmd_data.keys()
                                        data0 = prev_cmd_data[data_keys[0]]
                                        break
                        t = data0['type']
                        if 'value' in data0 and data0['value']:
                            choices = data0['value']
                        else:
                            status = '|'.join(data_keys)
                    else:
                        # no input needed for command
                        self.input_bar.insert_text(':')
                        status = 'Enter Message'

                else: # odd, need to complete cmd based on last cmd
                    cmd = fields[-3]
                    if not protocols.is_messageable_command(port, cmd):
                        choices = protocols.get_valid_transition_commands(port, cmd)
                    else:
                        status = 'Enter Message'
                        
        if status:
            buttons = [urwid.AttrMap(urwid.Text(status), 'focus')]
            self.status_bar = urwid.Columns(buttons)
        elif len(choices) == 0:
            return True
        elif len(choices) == 1:
            self.input_bar.insert_text(':' + ''.join(choices))
            return False
        else:
            self.status_bar = urwid.BoxAdapter(self.popup_menu(choices), len(choices))
        pile = urwid.Pile([self.status_bar, self.input_bar])
        self.view.set_footer(pile)
        return True
        
    def handler_exit(self):
        raise urwid.ExitMainLoop()

    def handler_new(self):
        if self.view_type == ViewType.PortList:
            self.update_footer('Expose new gate', 'New gate name: ', self.gate_edit_done)
        else:
            self.cur_selected_flow = None
            self.update_footer('Start new flow (user:port:cmd:data:msg)', 'Msg: ', self.send_edit_done)

    def handler_reply(self):
        self.update_footer('Reply (:port:cmd:data:msg)', 'Msg: ', self.send_edit_done)
        port = get_port_by_id(self.cur_selected_flow.protocol)
        self.input_bar.insert_text(':' + port['name'])

    def handler_filter(self):
        self.update_footer('Filter By User or Gate', 'User or Gate: ', self.filter_edit_done)

    def handler_todo(self):
        fl = []
        for f in flows.find_flows(me.name, None, me.name, None, None):
            port = get_port_by_id(f.protocol)
            if f.state in protocols.get_action_commands(port):
                fl.append(f) 
        self.cur_filtered_flows = fl
        self.update_flows_listbox()
        self.update_status('My ToDo')

    def handler_waiting(self):
        fl = []
        for f in flows.find_flows(me.name, me.name, None, None, None):
            port = get_port_by_id(f.protocol)
            if f.state in protocols.get_action_commands(port):
                fl.append(f) 
        self.cur_filtered_flows = fl
        self.update_flows_listbox()
        self.update_status('Waiting on others')

    def handler_all(self):
        self.cur_filtered_flows = flows.find_flows(me.name, None, None, None, None)
        self.update_flows_listbox()
        self.update_status('All Flows')
        
    def handler_discover(self):
        self.update_ports_listbox(False)
        self.update_status('Gates visible to me')

    def handler_expose(self):
        self.update_ports_listbox(True)
        self.update_status('Gates exposed by me')

    def handler_flowgroups(self):
        self.cur_filtered_flows = flows.find_flows(me.name, None, None, None, None)
        self.update_flow_groups_listbox()
        self.update_status('Flow Groups')
        
    def handler_templates(self):
        self.update_templates_listbox()
        self.update_status('Port Templates')

    def handler_settings(self):
        pass

    def handler_delete(self, confirm):
        if confirm:
            self.update_footer('Delete Flow?', 'y/n: ', self.delete_edit_done)
        else:
            self.delete_edit_done('y')

    def format_data_as_keyvalues(self, data):
        result = []
        if data and data != '{}':
            dictdata = json.loads(data)
            for k in dictdata:
                result.append(k + ': ' + (str(dictdata[k]['value']) if 'value' in dictdata[k] else ''))        
        return '; '.join(result)

    def handler_flowgroup_detail(self):
        group_key = self.listbox.get_focus()[0].id
        self.cur_filtered_flows = self.flow_groups[group_key]
        self.update_flows_listbox()
        self.update_status('Flows in Group')

    def handler_flow_detail(self):
        self.view.set_header(urwid.AttrMap(urwid.Text('Flow ' + str(self.cur_selected_flow.id)), 'head'))
        lines = []
        for atom in self.cur_selected_flow.atoms:
            ft = atom.created.strftime('%Y-%m-%d %H:%M')
            l = '\n({0}) {1}: [{2}] [{3}]\n{4}'.format(ft,
                                                        strme(atom.sender, me),
                                                        atom.state,
                                                        self.format_data_as_keyvalues(atom.data),
                                                        atom.body)
            lines.append(urwid.Text(l))        
        self.view.set_body(urwid.ListBox(lines))
        self.view_type = ViewType.FlowDetail

    def handler_port_detail(self):
        pid = self.listbox.get_focus()[0].id
        port = get_port_by_id(pid)
        self.view.set_header(urwid.AttrMap(urwid.Text('Gate ' + str(port['name'])), 'head'))
        detail = 'Id: {0}\n\nName: {1}\n\nDescription: {2}\n\nCreator: {3}\n\nVisible To: {4}\n'.format(port['id'],
                                                                                                            port['name'],
                                                                                                            port['description'],
                                                                                                            port['creator'],
                                                                                                            ', '.join(port['visibleTo']))
        detail += '\nInfo: ' + str(port['info'])
        detail += '\n\nState Transitions:\n    ' + '\n    '.join([k+" -> "+', '.join(v) for k,v in protocols.get_command_graph(port).iteritems()])
        detail += '\n\nState Data:\n    ' + '\n    '.join([k+" : "+str(v['data']) for k,v in port['commands'].iteritems()])
        self.view.set_body(urwid.ListBox([urwid.Text(detail)]))
        self.view_type = ViewType.PortDetail

    def handler_template_detail(self):
        pid = self.listbox.get_focus()[0].id
        port = get_template_by_id(pid)
        self.view.set_header(urwid.AttrMap(urwid.Text('Template ' + str(port['name'])), 'head'))
        detail = 'Id: {0}\n\nName: {1}\n\nDescription: {2}\n\nCreator: {3}\n'.format(port['id'],
                                                                                     port['name'],
                                                                                     port['description'],
                                                                                     port['creator'])
        detail += '\nInfo: ' + str(port['info'])
        detail += '\n\nState Transitions:\n    ' + '\n    '.join([k+" -> "+', '.join(v) for k,v in protocols.get_command_graph(port).iteritems()])
        detail += '\n\nState Data:\n    ' + '\n    '.join([k+" : "+str(v['data']) for k,v in port['commands'].iteritems()])
        self.view.set_body(urwid.ListBox([urwid.Text(detail)]))
        self.view_type = ViewType.TemplateDetail

    def keystroke(self, input):
        if input in ('q', 'Q'):
            self.update_status('Logging out')
            self.handler_exit()
            return
        self.cur_selected_flow = None
        if input is 'enter':
            oid = self.listbox.get_focus()[0].id
            if self.view_type == ViewType.FlowGroupList:
                self.handler_flowgroup_detail()
            elif self.view_type == ViewType.FlowList:
                self.cur_selected_flow = flows.Flow.get_flow_by_id(oid)
                self.handler_flow_detail()
            elif self.view_type == ViewType.PortList:
                self.handler_port_detail()
            elif self.view_type == ViewType.TemplateList:
                self.handler_template_detail()
        elif input == 'left':
            if self.view_type == ViewType.FlowList:
                self.handler_flowgroups()
            elif self.view_type == ViewType.FlowDetail:
                self.view.set_header(urwid.AttrMap(urwid.Text(FLOW_FORMAT_STR.format(*FLOW_FORMAT_HDR)), 'head'))
                self.view.set_body(self.listbox)
                self.view_type = ViewType.FlowList
            elif self.view_type == ViewType.PortDetail:
                self.view.set_header(urwid.AttrMap(urwid.Text(PORT_FORMAT_STR.format(*PORT_FORMAT_HDR)), 'head'))
                self.view.set_body(self.listbox)
                self.view_type = ViewType.PortList
            elif self.view_type == ViewType.TemplateDetail:
                self.view.set_header(urwid.AttrMap(urwid.Text(PORT_FORMAT_STR.format(*PORT_FORMAT_HDR)), 'head'))
                self.view.set_body(self.listbox)
                self.view_type = ViewType.TemplateList
        elif input == 'p':
            self.handler_discover()
        elif input == 'm':
            self.handler_expose()
        elif input == 'g':
            self.handler_flowgroups()
        elif input == 'l':
            self.handler_templates()
        elif input == '/':
            self.handler_filter()
        elif input == 't':
            self.handler_todo()
        elif input == 'w':
            self.handler_waiting()
        elif input == 'f':
            self.handler_all()
        elif input == 'n':
            self.handler_new()
        elif input == 'r':
            fid = self.listbox.get_focus()[0].id
            self.cur_selected_flow = flows.Flow.get_flow_by_id(fid)
            self.handler_reply()
        elif input == 'd':
            fid = self.listbox.get_focus()[0].id
            self.cur_selected_flow = flows.Flow.get_flow_by_id(fid)
            self.handler_delete(True)
        elif input == 'D':
            fid = self.listbox.get_focus()[0].id
            self.cur_selected_flow = flows.Flow.get_flow_by_id(fid)
            self.handler_delete(False)
        elif input == 'R':
            self.update_flows_listbox()
            
    def filter_edit_done(self, content):
        if content:
            pid,user = None,None
            u = get_user_by_id(content)
            if u == None:
                port = get_port_by_name(content)
                if port == None:
                    self.update_status('No user or port by that name found')
                    return
                else:
                    pid = port['id']
            else:
                user = u.name
            logging.info('filtering by ' + str(user) + ' or port ' + str(pid))
            self.cur_filtered_flows = flows.find_flows(user, None, None, pid, None)
            self.update_flows_listbox()
            self.update_status('Filter: ' + content)
        else:
            self.update_status('Ready')

    def send_edit_done(self, content):
        if not content:
            self.update_status('Canceled')
            return
        users,portname,cmd,data,msg = grok_meta_line(content)
        if self.cur_selected_flow:
            port = get_port_by_id(self.cur_selected_flow.protocol)
            toUsers = self.cur_selected_flow.users.split(',')
        else:
            port = get_port_by_name(portname)               
            toUsers = users
        if data != None and cmd in port['commands']:
            cmddata = port['commands'][cmd]['data']
            if cmddata:
                datafields = data.split('|')
                i = 0
                newdata = {}
                for dk,dv in cmddata.iteritems():
                    newdata[dk] = {}
                    newdata[dk]['value'] = datafields[i]
                    newdata[dk]['type'] = dv['type']
                    i += 1
                    data = json.dumps(newdata)

        # determine which users in the flow to send message to
        # if a bot is involved, then only bot-targeted commands need to be sent to it
        ownerCmd = False
        if cmd in port['commands'] and port['commands'][cmd]['actor'] == 'owner':
            ownerCmd = True
        filtered_receivers = []
        logging.info(toUsers)
        for uid in toUsers:
            if uid == me.name:
                continue
            u = get_user_by_id(uid)
            if u == None:
                self.update_status('Error finding user ' + uid)
                return
            # if receiver is a bot, send message to bot only if command is not a owner-only cmd
            if u.bot and ownerCmd:
                logging.info('Skipping bot as receiver because this message is not targeted to bot')
                pass
            else:
                filtered_receivers.append(u.name)

        if not self.cur_selected_flow:
            self.cur_selected_flow = flows.make_new_flow(port['id'], me.name, filtered_receivers, None, None)

        atom = flows.Atom(sender=me.name, body=msg, state=cmd, data=data, crud_flag=flows.AtomCRUDType.create)
        self.cur_selected_flow.send_message(atom, filtered_receivers)
        self.update_status('Sent')
        self.handler_flow_detail()

    def delete_edit_done(self, content):
        if content in ('y','Y'):
            self.cur_selected_flow.delete()
            self.update_status('Deleted ' + str(self.cur_selected_flow.id))
            for f in self.cur_filtered_flows:
                if f.id == self.cur_selected_flow.id:
                    self.cur_filtered_flows.remove(f)            
            self.cur_selected_flow = None                
            self.update_flows_listbox()
        else:
            self.update_status('Ready')

    def gate_edit_done(self, content):
        global my_visible_ports
        newPort = { 'name' : content, 
                    'creator' : me.name,
                    'canStartWithSay' : True, 
                    'extensionPorts' : [0,1], # General and Appointment ports included 
                  }
        try:
            client.new_port(newPort)
            # update visible ports
            my_visible_ports = client.get_ports_visible_to_user(me.name, '')
            self.update_ports_listbox(True)
            self.update_status('Gate created')
        except Exception as e:
            logging.exception('Error creating new port')
            self.update_status('Error creating gate: ' + str(e))

    def popup_menu(self, choices):
        buttons = []
        for c in choices:
            button = urwid.Button(c)
            urwid.connect_signal(button, 'click', self.popup_menu_item_chosen, c)
            buttons.append(urwid.AttrMap(button, None, focus_map='reversed'))
        return urwid.ListBox(urwid.SimpleFocusListWalker(buttons))

    def popup_menu_item_chosen(self, button, choice):
        #self.view.set_body(self.listbox)
        buttons = [urwid.AttrMap(urwid.Text(''), 'focus')]
        self.status_bar = urwid.Columns(buttons)
        self.input_bar.insert_text(choice)
        pile = urwid.Pile([self.status_bar, self.input_bar])
        self.view.set_footer(pile)
        self.view.set_focus('footer')

    def make_popup_menu(self, choices):
        top = urwid.Overlay(self.popup_menu(choices), urwid.SolidFill(u'\N{MEDIUM SHADE}'),
                            align='center', width=('relative', 60),
                            valign='middle', height=('relative', 60), min_width=20, min_height=9)
        self.view.set_body(self.popup_menu(choices))
        self.view.set_focus('body')
#---------------------------------------------------------------------#


# LOGIN and LOGOUT to server
#---------------------------------------------------------------------#
def login_user(user, passwd):
    global me, my_contacts, my_visible_ports, my_visible_templates

    # login user to port server as well as xmpp server
    if user != 'admin': 
        if not client.login_user(user, passwd):
            print 'Login failed. Invalid credentials?'
            return None
        me = client.get_user(user)
        if not me:
            print 'Unable to get user details from server.'
            return None
        if not flows.login_to_transport_layer(me, passwd):
            print 'Error logging in user to transport layer.'
            return None
        
    # setup my contacts
    my_contacts = []
    if me.contacts:
        my_contacts = [client.get_user(c) for c in me.contacts]

    # setup user's visible ports
    my_visible_ports = client.get_ports_visible_to_user(me.name, '')
    my_visible_templates = client.get_templates_visible_to_user(me.name, '')
    
    prompt = user + '> '
    return user


def logout_user():
    if me:
        flows.logout_from_transport_layer(me)
#---------------------------------------------------------------------#


# Meta line input parser
#---------------------------------------------------------------------#
def grok_meta_line(line):
    users,portname,cmd,data,msg = None,None,None,None,None
    fields = line.split(':')
    if len(fields) < 2: 
        raise Exception("Not enough tokens to parse")
    users = fields[0].split(',')
    portname = fields[1]
    if len(fields) > 2:
        cmd = fields[2]
    if len(fields) > 3:
        data = fields[3]
    if len(fields) > 4:
        msg = fields[4]
    return users,portname,cmd,data,msg
#---------------------------------------------------------------------#


# MAIN EXEC
#---------------------------------------------------------------------#
if __name__ == '__main__':
    logging.basicConfig(filename='/tmp/repl.log', level=logging.DEBUG, format='%(levelname)s %(funcName)s: %(message)s')
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', help='server to connect to', default='http://autometon.com:5000')
    parser.add_argument('-f', '--flowdb', help='flow db file to use', default='flows.db')
    args = parser.parse_args()
    flows.init_db_session(args.flowdb)
    client.set_host(args.server)
    u = raw_input('User: ')
    p = getpass.getpass()
    if login_user(u, p):
        try:
            MyApp()
        except Exception as e:
            logging.exception('Exception in app')
            print 'Exception in app : ' + str(e)
        finally:
            logout_user()
    flows.close_db_session()
#---------------------------------------------------------------------#
