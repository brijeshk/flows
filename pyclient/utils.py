import json
import logging
from attrdict import AttrDict

'''
class json_to_object: 
    def __init__(self, json_data):
        if isinstance(json_data, basestring):
            json_data = json.loads(json_data)
        self.json_data = json_data
 
    def __getattr__(self, key):
        if key in self.json_data:
            if isinstance(self.json_data[key], (list, dict)):
                return json_to_object(self.json_data[key])
            else:
                return self.json_data[key]
        else:
            raise Exception('There is no json_data[\'{key}\'].'.format(key=key))
 
    def __str__(self):
        return str(self.json_data)

    def __iter__(self):
        return iter([json_to_object(j) for j in self.json_data])

    def __repr__(self):
        out = self.__dict__
        return '%r' % (out['json_data'])
'''

def flatten_object_to_list(nested, prefix=None):
    if nested is None:
        return []
    elif isinstance(nested, list):
        if prefix:
            return [(prefix + '.' + i) for i in nested]
        else:
            return nested
    elif isinstance(nested, dict):
        flat = []
        for k in nested.keys():
            if prefix:
                kprefix = prefix + '.' + k
            else:
                kprefix = k
            flat += flatten_object_to_list(nested[k], kprefix)
        return flat
    else:
        if prefix:
            return [prefix + '.' + nested]
        else:
            return [nested]

if __name__ == '__main__':
    print(flatten_object_to_list({'a':['b','c'], 'd':['e','f'], 'g':{'h':'r'}}))
    print(flatten_object_to_list({'a':'b'}))
    print(flatten_object_to_list('a'))
    print(flatten_object_to_list(['London,UK','SanFrancisco,US','Beijing,China']))
    temp = '{"a":[1,2],"e":[{"f":1,"k":2}], "g":{"t":"gtg","r":[1,2,4]}}'
    x = json.loads(temp)
    print(str(x))
    obj = AttrDict(x)
    print(obj.a)
    print(obj.g.t)
    print(obj.e)
    for i in obj.g.r:
        print(i)
