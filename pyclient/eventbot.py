#!/usr/bin/env python
"""
System User XMPP Bot.
This bot receives system user commands, processes them, and sends back messages with results 
"""

import sys
import logging
import argparse
from ws4py.client.threadedclient import WebSocketClient
import time
import threading
import client
import os
import signal
import importlib
import json
import datetime


# GLOBALS
#-------------------------------------------#
g_ws = None
g_uid = None
g_cmdFunc = None
#g_server = 'autometon.com'
g_server = 'localhost:5000'
#-------------------------------------------#


class BotClient(WebSocketClient):

    def opened(self):
        logging.info("websocket opened with server")


    def closed(self, code, reason=None):
        logging.info("websocket closed: " + str(code) + " " + str(reason))


    def received_message(self, message):
        logging.info("received: " + message.data)
        try:
            msg = json.loads(message.data)
        except:
            logging.error("Not valid flow json. Probably not a flow msg")
            return
        self.processFlowMessage(msg['data'][0])


    def processFlowMessage(self, msg): 
        try:
            body = msg['body']
            if len(body) > 0 and body[0] == '/':
                fields = body.split(' ', 1)
                if len(fields) >= 2:
                    body = fields[1]
                else:
                    body = ''
            logging.info('executing bot cmd with ' + body)
            body = g_cmdFunc(msg['eid'], body)
        except Exception as e:
            logging.exception('Error from bot command execution: ')
            body = str(e)

        d = datetime.datetime.utcnow()
        timeNow = d.isoformat("T") + "Z"

        # build the response
        receivers = msg['receivers']
        if not msg['sender'] in receivers:
            receivers.append(msg['sender'])
        if g_uid in receivers:
            receivers.remove(g_uid)
        atom = { 'eid' : msg['eid'],
                 'id' : msg['id'] + '_r',
                 'body' : body,
                 'sender' : g_uid,
                 'receivers' : receivers
               }
        logging.info(atom)

        # send response to server
        client.post_event_msg(atom)
#-------------------------------------------#


def sigtermHandler(signum, frame):
    global g_ws
    logging.info('Received termination signal')
    if g_ws:
        g_ws.close()


# MAIN HANDLER
#-------------------------------------------#
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(levelname)-8s %(message)s')

    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--uid', help='bot userid', default='AutoBot')
    parser.add_argument('-p', '--password', help='bot password', default='AutoBot')
    args = parser.parse_args()

    if args.uid == None or args.password == None:
        print('Usage: metonbot.py -u userid -p password')
        sys.exit(1)

    client.set_host('http://' + g_server)
    client.set_creds(args.uid, args.password)

    g_uid = args.uid
    m = importlib.import_module('connectors.' + g_uid.lower())
    g_cmdFunc = getattr(m, 'processCommand')
    if (g_cmdFunc == None):
        print('Cannot load command module')
        sys.exit(1)

    # setup termination handler
    signal.signal(signal.SIGINT, sigtermHandler)

    # open websocket
    try:
        g_ws = BotClient('ws://' + g_server + '/websocket/' + args.uid, protocols=['http-only', 'chat'])
        g_ws.connect()
        print('Press Ctrl-C to terminate.')
        g_ws.run_forever()
    except KeyboardInterrupt:
        g_ws.close()
#-------------------------------------------#
