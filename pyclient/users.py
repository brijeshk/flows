"""
User management functions.
"""

MESSAGE_TRANSPORT_TYPES = ('email', 'native', 'sms', 'xmpp')

class User(dict):
    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        self.__dict__ = self
