#!/usr/bin/env python
"""
System User XMPP Bot.
This bot receives system user commands, processes them, and sends back messages with results 
"""

import sys
import logging
import argparse
from ws4py.client.threadedclient import WebSocketClient
import time
import threading
import client
import os
import signal
import importlib
import json
import datetime


# GLOBALS
#-------------------------------------------#
g_ws = None
g_botPorts = None
#g_server = 'autometon.com:5000'
g_server = 'localhost:5000'
#-------------------------------------------#


class BotClient(WebSocketClient):

    def opened(self):
        logging.info("websocket opened with server")


    def closed(self, code, reason=None):
        logging.info("websocket closed: " + str(code) + " " + str(reason))


    def received_message(self, message):
        #logging.info("received: " + message.data)
        try:
            flows = json.loads(message.data)
        except:
            logging.error("Not valid flow json. Probably not a flow msg")
            return
        self.processFlowMessage(flows[0])


    def processFlowMessage(self, flow): 
        try:
            atom = flow['atoms'][-1]
            data = self.executePortCommand(flow['fid'], flow['pid'], atom['state'], atom['data'])
            state = 'Respond'
        except Exception as e:
            logging.exception('Error from port command execution: ')
            data = { 'Error' : { 'type' : 'Error', 'value' : str(e) } }
            state = 'Error'

        d = datetime.datetime.utcnow()
        timeNow = d.isoformat("T") + "Z"

        # build the response
        atom = { 'fid' : flow['fid'],
                 'aid' : flow['atoms'][-1]['aid'] + '_resp',
                 'crud' : 'c',
                 'state' : state,
                 'body' : None,
                 'data' : data,
                 'sender' : 'AutoBot',
                 'modified' : timeNow
               }
        flow['atoms'] = [atom]
        flow['modified'] = timeNow
        flow['crud'] = 'u'
        flow['state'] = state

        # send response to server
        for u in flow['users']:
            if u != "AutoBot":
                logging.info("sending response to " + u)
                client.post_flow_delta(u, [flow])


    def executePortCommand(self, flowid, portid, command, data):
        port = None
        for p in g_botPorts:
            if p['id'] == portid:
                port = p
                break
        if port == None:
            logging.error('Unknown portid: ' + str(portid))
            raise Exception('Unknown portid')

        m = importlib.import_module('connectors.' + p['name'].lower())
        # run command
        function = getattr(m, 'processCommand')
        return function(flowid, portid, command, data)

#-------------------------------------------#


def sigtermHandler(signum, frame):
    global g_ws
    logging.info('Received termination signal')
    if g_ws:
        g_ws.close()


# MAIN HANDLER
#-------------------------------------------#
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)-8s %(message)s')

    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--uid', help='bot userid', default='AutoBot')
    parser.add_argument('-p', '--password', help='bot password', default='AutoBot')
    args = parser.parse_args()

    if args.uid == None or args.password == None:
        print('Usage: metonbot.py -u userid -p password')
        sys.exit(1)

    client.set_host('http://' + g_server)

    # get info about ports bot exposes
    g_botPorts = client.get_ports_exposed_by_user(args.uid)    

    # setup termination handler
    signal.signal(signal.SIGINT, sigtermHandler)

    # open websocket
    try:
        g_ws = BotClient('ws://' + g_server + '/websocket/' + args.uid, protocols=['http-only', 'chat'])
        g_ws.connect()
        print('Press Ctrl-C to terminate.')
        g_ws.run_forever()
    except KeyboardInterrupt:
        g_ws.close()
#-------------------------------------------#
