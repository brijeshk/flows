var _db = null;

function initLocalDb(uid) {
    _db = new PouchDB('event-' + uid);
    console.log('local db inited');
}

function deleteLocalDb(uid) {
    _db.destroy();
    console.log('local db destroyed');
}

function getEventsLocal(fn, me) {
    _db.allDocs({'include_docs':true}).then(function(result) {
        var evts = [];
        $.each(result.rows, function(i,r) { 
            if (r.doc) {
                if (! r.doc.messages) r.doc.messages=[]; 
                updateEventFlags(r.doc, me);
                evts.push(r.doc); 
            }
        });
        if (fn) fn(evts);
    }).catch(function(err) { 
        console.log('error getting local events', err);
    });
}

function saveEventLocal(e) {
    e._id = e.id;
    _db.put(e).then(function(resp) {
        console.log('created/updated pouchdb doc');
        e._rev = resp.rev; 
    }).catch(function(err) {
        console.log('error upserting new pouchdb doc', err);
    });
}

function deleteEvent(e) {
    _db.remove(e).catch(function(err) { console.log(err); });
}

function setupLocalReminder(eid, about, time, isUpdate) {
    if (! time || time == '') return;
    console.log('setting up local reminder');
    var d = new Date(time);
    if (isUpdate) {
      cordova.plugins.notification.local.update({
        id: eid,
        title: "BeeWaggle",
        text: about,
        at: d
      });
    }
    else {
      cordova.plugins.notification.local.schedule({
        id: eid,
        title: "BeeWaggle",
        text: about,
        at: d
      });
    }
}

function sortEvents(events) {
    var now = new Date();
    events.sort(function(a,b) { 
        var da = new Date(a.startTime);
        var db = new Date(b.startTime);
        if (da >= now && db >= now) {
            // future events sorted ascending
            if (da == db) return 0; 
            else return (da > db) ? 1 : -1;
        }
        else if (da <= now && db <= now) {
            // past events sorted descending
            if (da == db) return 0; 
            else if (a.startTime == "0001-01-01T00:00:00Z") return -1;
            else if (b.startTime == "0001-01-01T00:00:00Z") return 1;
            else return (da > db) ? -1 : 1;
        }
        else {
            // future always sorted before past
            if (da <= now) return 1;
            else return -1;
        }
    });
}
