angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, $locstore, $location, $ionicHistory,
                                $myToast, $cordovaLocalNotification, $cordovaGeolocation,
                                $cordovaDevice, $cordovaPush,
                                $state, $ionicModal, $timeout, $ionicPopup, $ionicSideMenuDelegate) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = { 'username' : $rootScope.userid, 'password': $rootScope.password, 'phoneNumber':'' };
  // Apply localnotification click event on controller init	once
  $rootScope.$on('$cordovaLocalNotification:click',
		function (event, notification, state) {
	  		//console.log("on local notification click.....",event, notification, state);
	  		if( state == "background" && notification.id != 1){
	  			//$location.path('app/event/'+notification.id);
	  			notification.data = JSON.parse(notification.data);
	  			$state.go('app.single', { 'eventId':notification.data.eventId });
			}else{
				$location.path('app/events/Upcoming');
			}
  });
  
  //Function to handle google map UI issue and slide menu issue
  $rootScope.isMenuOpen = $ionicSideMenuDelegate.isOpen.bind($ionicSideMenuDelegate);
  
  // sync event changes from server
  // both events and messages are only temporarily stored on server. permanent storage is on client.
  $scope.syncEvents = function() {
      console.log('refreshing events from server since ', $rootScope.lastSync);	 
      wsSyncEvents($rootScope.lastSync).then(function(deltaEvents) {
          // TODO: time gap here, server should send last sync time
          $rootScope.lastSync = ISODateString(new Date());
          $locstore.set("ls", $rootScope.lastSync);
          var eventId = 1;// Init event id with 1 to assign it to local notification. If id is 1 then on click on local notification we will 
          // redirect to event list otherwise redirect to spacific event screen. 	
          if (!deltaEvents || deltaEvents.length == 0) return;
          $.each(deltaEvents, function(i,de) {
              var greps = $.grep($rootScope.events, function(e) { return e.id==de.id; } );
              if (greps.length > 1) {
                  // how's that possible? 
                  console.log("!!delta event " + de.id + " matches multiple cached events!!");
                  return;
              }
              if (greps.length == 0) {
                  // new event
                  de.img = getEventImg(de.etype);
                  if (de.startTime != "0001-01-01T00:00:00Z") de.startTimeLocal = new Date(de.startTime).toLocaleString();
                  de.messages = [];
                  saveEventLocal(de);
                  // set flag if needs response
                  if (de.owner != $rootScope.me && ! de.inviteeStatus[$rootScope.me]) {
                      de.flags = 'p';
                  }
                  console.log('adding new delta event');
                  updateEventFlags(de, $rootScope.me);
                  $rootScope.events.push(de);
              }
              else {
                  // existing event update
                  var be = greps[0];
                  console.log('updating existing base event', be.id);
                  be.name = de.name;
                  be.startTime = de.startTime;
                  if (be.startTime != "0001-01-01T00:00:00Z") be.startTimeLocal = new Date(be.startTime).toLocaleString();
                  be.endTime = de.endTime;
                  be.etype = de.etype;
                  be.inviteeStatus = de.inviteeStatus;
                  be.invitees = de.invitees;
                  be.state = de.state;
                  be.data = de.data;
                  be.place = de.place;
                  be.quorum = de.quorum;
                  be.img = getEventImg(be.etype);
                  saveEventLocal(be);
                  updateEventFlags(be, $rootScope.me);
              }
          });
          sortEvents($rootScope.events);
          $rootScope.$broadcast('EventsUpdated');
          // local notification
          try {
              if ($rootScope.appPaused) {
                  if (deltaEvents.length == 1) {
                      notif = deltaEvents[0].owner + ' invites: ' + deltaEvents[0].name;
					  eventId = deltaEvents[0].id;
                  }
                  else {
                      notif = 'Received some event invites';
                      eventId = 1;
                  }
                  var notification_data = {};
                  notification_data.eventId = eventId;
                  
                  $cordovaLocalNotification.schedule({
                      id: eventId,
                      title: 'BeeWaggle',
                      text: notif,
                      data: notification_data
                  });
              }
          } catch(e) {}
      }).fail(function(e) {
          console.log('Error syncing events from server', e);
      });
  }

  // websocket notification handler
  $scope.listenToSocketMessages = function(m) {
      if (m.type == 'SyncEvents') {
          // got a sync now message
          $scope.syncEvents();
      }
      else if (m.type == 'EventMsg') {
          // got event messages
          // find and add to right events
          var found = false;
          var eventId = 1;// Init event id with 1 to assign it to local notification. If id is 1 then on click on local notification we will 
          $.each(m.data, function(i,atom) {
              $.each($rootScope.events, function(i,e) {
                  if (e.id == atom.eid) {
                      if (atom.body.indexOf('#eta') == 0) {
                          var x = atom.body.slice(4);
			  if (!e.lastEta) e.lastEta = {};
                          e.lastEta[atom.sender] = x;
                      }
                      else if (atom.body.indexOf('#ETA?') == 0) {
                          if (!e.lastEta) e.lastEta = {};
                          e.lastEta[$rootscope.me] = 'requested';
                      }
                      /*
                      else if (atom.body.indexOf('#mylocation') == 0) {
                          // got a location update from someone
                          console.log('received location update', atom);
                          if (!e.lastLocations) e.lastLocations = {};
                          e.lastLocations[atom.sender] = atom.body.substring(12);
                          saveEventLocal(e);
                          return;
                      }
                      else if (atom.body.indexOf('#location') == 0) {
                          // got a location request from someone
                          // TODO: prompt user or read some setting
                          console.log('received location request, sending my location');
                          var posOptions = {timeout: 5000, enableHighAccuracy: false};
                          $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
                              var latP  = position.coords.latitude;
                              var longP = position.coords.longitude;
                              var date = new Date();
                              var ratom = { 'eid' : atom.eid,
                                            'id' : $rootScope.userid.substring(0,3) + Math.round(date/1000), // just need a reasonably unique id
                                            'receivers' : [atom.sender],
                                            'sender' : $rootScope.userid,
                                            'body' : '#mylocation:' + latP + ',' + longP,
                                            'state': 'LocMsg',
                                            'modified' : ISODateString(date)
                              };
                              console.log('sending atom', ratom);
                              wsSendEventMessage(ratom);
                          },function(error){
                       	      console.log("gps error",error);
                          });
                          return;
                      }*/
                      else {
                          if (!e.messages) e.messages = [];
			  e.messages.splice(0, 0, atom); // add to front of array
                          //e.messages.push(atom);
			  if (e.flags == '') e.flags = 'u';
                      }
                      updateEventFlags(e, $rootScope.me);
                      saveEventLocal(e);
                      found = true;
                  }
              });
          });
          if (found == false) return;  // message on deleted event, ignore

          $rootScope.$broadcast('EventUpdated');
          // local notification
          try {
              if ($rootScope.appPaused) {
                  if (m.data.length == 1) {
                      notif = m.data[0].sender + ': ' + m.data[0].body.substring(0,50);
                      eventId = m.data[0].eid;
                  }
                  else {
                      notif = 'Received a few messages';
                      eventId = 1;
                  }
                  var notification_data = {};
                  notification_data.eventId = eventId;
                  
                  $cordovaLocalNotification.schedule({
                      id: eventId,
                      title: 'BeeWaggle',
                      text: notif,
                      data: notification_data
                  });
              }
          } catch(e) {}
      }
  };

  // push notification handler
  $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
    switch(notification.event) {
      case 'registered':
        if (notification.regid.length > 0) {
          console.log('push device token = ' + notification.regid);
          $rootScope.pushToken = notification.regid;
          wsUpdateUserPushToken($rootScope.me, $rootScope.pushToken);
        }
        break;
      case 'message':
        console.log('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
        if (!$rootScope.appPaused && notification.payload && notification.payload.atom && notification.payload.atom.eid){
            $rootScope.notifiedEventId = notification.payload.atom.eid;
        }
        break;
      case 'error':
        console.log('GCM error = ' + notification.msg);
        break;
      default:
        console.log('An unknown GCM event has occurred');
        break;
    }
  });

  $scope.doRegister = function() {
      var u = $scope.loginData.username;
      // set password same as username for testing now
      // will replace with unique device id later
      var p = u;
      var ph = $scope.loginData.phoneNumber;
      if (u == '' || ph == '') {
          $myToast.show('Please enter your phone number and a unique username');
          return;
      }
      /*
      try {
          p = $cordovaDevice.getUUID();
      } catch(err) {
          console.log('could not read device uuid', err);
      }*/
      wsRegisterUser(u, ph, p).then(function() {
          $locstore.set('u', u);
          $locstore.set('p', p);
          $locstore.set('ls', "0");
          $myToast.show('Registration successful');
          $scope.autoLogin(u, p, "0");
      }).fail(function(err) { 
          console.log('failed register', err);
          $myToast.show('Registration failed. Please try a different username');
      });
  };

  $scope.doLogin = function() {
      var u = $scope.loginData.username;
      var p = u;
      var ph = $scope.loginData.phoneNumber;
      if (u == '') {
          $myToast.show('Please enter the username that you registered with');
          return;
      }
      $locstore.set('u', u);
      $locstore.set('p', p);
      $locstore.set('ph', ph);
      $locstore.set('ls', "0");
      $scope.autoLogin(u, p, "0");
  };

  $scope.doLogout = function() {
      $locstore.removeItem('u');
      $locstore.removeItem('p');
      $locstore.removeItem('ph');
      $locstore.removeItem('ls');
      $scope.loginStatus = 'loggedOut';
      $rootScope.userid = null;
      $rootScope.me = null;
      $rootScope.lastSync = null;
      $rootScope.password = null;
      $rootScope.events = [];
  }

  // open websocket to server, initial sync
  $scope.autoLogin = function(u, p, ls) {
      //$myToast.show('Logging in...'); 
      $scope.loginStatus = 'loggingIn';
      console.log('using creds', u, p);
      wsSetCreds(u, p);
      $rootScope.userid = u;
      $rootScope.me = u;
      $rootScope.password = p;
      $rootScope.lastSync = ls;
      $rootScope.events = [];
      initLocalDb(u);
      console.log('reading events from local db');
      getEventsLocal(function(rows) { 
          $rootScope.events = rows; 
          sortEvents($rootScope.events);
          console.log('read events from local db');
          $scope.loginStatus = 'loggedIn';
          $scope.syncEvents();
          openWebSocketToServer(u, $scope.listenToSocketMessages);
	  $rootScope.filterCat = 'Upcoming';
	  // disable history for next view switch
	  $ionicHistory.nextViewOptions({ disableAnimate: true, disableBack: true });
          if ($rootScope.notifiedEventId) {
	      $location.path('app/event/' + $rootScope.notifiedEventId);
              $rootScope.notifiedEventId = null;
          } 
          else {
	      $location.path('app/events/Upcoming');
          }
      }, $rootScope.me);

  };

  $scope.$on('SyncNow', $scope.syncEvents);
  $scope.$on('AppResumed', function() {
      if ($rootScope.me && ! checkWebSocketToServer()) {
          console.log('websocket lost, reconnecting...');
          openWebSocketToServer($rootScope.me, listenToSocketNotifications);
      }
  });
  
  // check for saved creds
  var u = $locstore.get('u', null);
  var p = $locstore.get('p', null);
  var ph = $locstore.get('ph', '');
  if (u && p) {
      console.log('found user creds in localstorage', u, p);
      $scope.loginData.username = u;
      $scope.loginData.password = p;
      $scope.loginData.phoneNumber = ph;
      var ls = $locstore.get('ls', "0");
      $scope.autoLogin(u, p, ls);
  }				
  else {
      $scope.loginStatus = 'loggedOut';
      $location.path('app/login');
  }
})

.controller('EventListCtrl', function($scope, $rootScope, $stateParams, $location, $locstore, $myToast, $ionicHistory) {
  
    $scope.filterBy = function(f) {
        $rootScope.filterCat = f;
        $.each($rootScope.events, function(i,e) {
            if (f == 'Upcoming') {
              e.filtered = ! e.past;
            }
            else if (f == 'Past') {
              e.filtered = e.past;
            }
            else if (f == 'Todo') {
              e.filtered = (e.flags == 'p' || e.flags == 'q' || e.flags == 't');
            }
            else {
              e.filtered = (getEventCategory(e.etype) == f);
            }
        });
    };

    $scope.switchToTab = function(f) {
        $scope.filterBy(f);
        //$ionicHistory.nextViewOptions({ disableAnimate: true, disableBack: true });
        //$location.path('app/events/' + f);
    }

    $scope.toggleShowDelete = function() { $scope.showDeleteButtons = !$scope.showDeleteButtons; };

    $scope.newEvent = function() {
        $location.path('app/event/-1');
    };
    $scope.cloneEvent = function(eid) {
        $location.path('app/event/clone:' + eid);
    };

    $scope.deleteEvent = function(event) {
        for(var i = $rootScope.events.length-1; i >= 0; i--) {
          if ($rootScope.events[i].id == event.id) {
            deleteEvent(event);
            $rootScope.events.splice(i,1);
            //wsDeleteEvent(event).then(function() { $myToast.show('Event deleted'); });
            break;
          }
        }
    };

    $scope.toggleSearch = function() {
        $scope.showSearch = !$scope.showSearch;
        if ($scope.showSearch == false) {
            // reset filter
            $scope.filterBy('Upcoming');
        }
    };
    $scope.searchEvents = function(term) {
        $.each($rootScope.events, function(i,e) {
            e.filtered = (e.name.toLowerCase().indexOf(term.toLowerCase())>=0);
        });
    };

    $scope.doRefresh = function() {
        $rootScope.$broadcast('SyncNow');
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$apply();
    };

    $scope.$on('EventsUpdated', function(m) {
        console.log('received EventsUpdated trigger');
        $scope.filterBy($scope.filterCat);
        $scope.$apply();
    });

    $scope.getEventFlagIcon = function(event) {
        if (event.flags == 'u') return 'ion-email-unread';
        else if (event.flags == 'q') return 'ion-help-circled';
        else if (event.flags == 'p') return 'ion-android-alert';
        else if (event.flags == 't') return 'ion-android-alert';
        else return '';
    };
    $scope.getEventFlagString = function(event) {
        switch (event.flags) {
          case 'u': return 'buzz';
          case 'q': return 'todo';
          case 'p': return 'reply';
          case 't': return 'todo';
          default: return '';
        }
    }
    $scope.getUserColor = function(u) {
        var c = "#E3E3E3";
        colors = ['#CB99C9', '#DEA5A4', '#966FD6', '#77DD77', '#779ECB', '#C23B22', '#CFCFC4', 
            '#B19CD9', '#667A44', '#FFB347', '#836953', '#FF6961', '#B39EB5', '#FFD1DC', 
            '#F49AC2', '#AEC6CF', '#FDFD96'];   
        if (u != "Me" && u.length >= 2) {
            var hash = 0;
            for(i = 0; i < 2; i++){
                chr = u.charCodeAt(i);
                hash = hash+chr;
            }
            hash = (hash % 17);
            c = colors[hash];
        }
        return c;       
    }

    console.log('enter EventListCtrl');
    $scope.showDeleteButtons = false;
    $scope.showSearch = false;
    $scope.tabEventCats = getAllEventCategories();
    var f = $stateParams.filterCat;
    if (f) $rootScope.filterCat = f;
    $scope.filterBy($rootScope.filterCat);
})

.controller('EventCtrl', function($scope, $state, $rootScope,
                                  $stateParams, $myToast, $location, $timeout,
                                  $ionicActionSheet, $cordovaActionSheet, $ionicModal,
                                  $cordovaContacts, $ionicPopover, $ionicPopup, $cordovaNetwork,
                                  $timeout, $cordovaGeolocation, $cordovaSms,
                                  $ionicSlideBoxDelegate, $ionicScrollDelegate, $ionicTabsDelegate, $ionicPlatform) {
    $scope.$on('$ionicView.enter', function(e) {
           var findEvent = function(eid) {
	        for (var i in $rootScope.events) {
	            var e = $rootScope.events[i];
	            if (e.id == eid) {
	                e.myStatus = e.inviteeStatus[$rootScope.userid];
	                if (!e.name || e.name == '') e.name = e.etype;
	                e.readOnly = (e.owner != $rootScope.userid) || (e.state == 'Cancelled');
                        if (! e.lastEta) e.lastEta = {};
	                return e;
	            }
	        }
	    }
	    $scope.inputMsg = '';
	    var eid = $stateParams.eventId;
	    if (eid == "-1") {
	        // new event
	        $scope.event = { 'owner':  $rootScope.userid, 'messages' : [], 'invitees' : [], 'quorum' : 1, 'lastEta' : {} };
	        $scope.editing = true;
	    }
	    else if (eid.indexOf('clone:')==0) {
	        // clone event
	        var e = findEvent(eid.substring(6));
	        if (e) {
	            $scope.event = jQuery.extend(true, {}, e);
                    $scope.event.owner = $rootScope.me;
                    var idx = $scope.event.invitees.indexOf($rootScope.me);
                    if (idx >= 0) {
                        $scope.event.invitees.splice(idx, 1);
                    }
	        }
	        else {
	            $scope.event = { 'owner':  $rootScope.userid, 'quorum' : 1 };
	        }
	        $scope.event.messages = [];
	        $scope.event.id = null;
	        $scope.editing = true;
	    }
	    else {
	        // open event
	        $scope.event = findEvent($stateParams.eventId);
	        $scope.editing = false;
	    }
	    if ($scope.event) {
	        console.log($scope.event);
	    }
	    
	    // setup autocomplete for event type
	    $scope.acAllTypes = getAllEventTypes();
	    
	    // setup autocomplete for contacts (that are also using autometon)
	    $scope.acAllContacts = $rootScope.allContacts;
            $scope.acAllTypes = getAllEventTypes();
	    
	    $scope.rootAUBList = ['Bots', 'People'];
	    $scope.aubList = $scope.rootAUBList; 
            $scope.etaOptions = [ 'Driving', 'Arrived', 'Delayed', '5 mins', '15 mins', '30 mins', '1 hr' ];
	    
	    // show map on demand, when user clicks Update button
	    $scope.showMap = false;
    });
     
    /*	
	$scope.initMap = function(){
		//$scope.map = null;
		setTimeout(function(){
			ionic.DomUtil.ready(function() {
				document.addEventListener("deviceready", function() {
					var div = document.getElementById("map_canvas_googleMap");
					const GOOGLE = new plugin.google.maps.LatLng(41.878114,-87.629798); //18.5634237,73.7827549
					//const defaultLocation = new plugin.google.maps.LatLng(41.796875,140.757007);
						      // Initialize the map view
						      map = window.plugin.google.maps.Map.getMap(div, {
								 // 'backgroundColor': 'white',
								  'mapType': plugin.google.maps.MapTypeId.ROADMAP,
								  'controls': {
								    'compass': true,
								    'myLocationButton': true,
								    'indoorPicker': true,
								    'zoom': true
								  },
								  'gestures': {
								    'scroll': true,
								    'tilt': true,
								    'rotate': true,
								    'zoom': true
								  },
								  'camera': {
								    'latLng': GOOGLE,
								    'tilt': 30,
								    'zoom': 3, //13
								    'bearing': 50
								  }
							});
						map.setBackgroundColor("white");
						map.addEventListener(plugin.google.maps.event.MAP_READY, $scope.onMapReady);
						map.clear();
				},false);
			});
		},500);

		window.addEventListener('native.hidekeyboard', keyboardHideHandler);
		window.addEventListener('native.showkeyboard', keyboardShowHandler);
		function keyboardHideHandler(e){
			map.setClickable( true );		    
		}
		function keyboardShowHandler(e){
			map.setClickable( false );
		}

	}

	$scope.initMap();
    */
	
    // refresh view if new message received on event 
    $scope.$on('EventUpdated', function(m) {
        console.log('received evt update');
        $scope.$apply();
        $ionicScrollDelegate.scrollBottom();
    });
  
    /* 
    $ionicModal.fromTemplateUrl('templates/editEventModal.html', {
      scope: $scope,
    }).then(function(modal) {
      $scope.editModal = modal;
    });
    $scope.$on('$destroy', function() {
      $scope.editModal.remove();
    });*/

    // setup autocomplete for contacts (that are also using autometon)
    $scope.acCompleteContacts = function(query) {
        var filtered = [];
        $.each($scope.acAllContacts, function(i,c) {
            if (c.indexOf(query)>=0) filtered.push(c);
        });
        return filtered;
    }
 
    // event create or update handler
    $scope.createOrUpdateEvent = function() {
        console.log('creating or updating event');
        var eid = $scope.event.id;
        if (!eid) {
            // new event
            $scope.event.id = generateUUID();
            $scope.event.inviteeStatus = {};
            $scope.event.inviteeStatus[$rootScope.me] = 'yes';
            if (! $scope.event.invitees) { 
                $scope.event.invitees = [];
            }
            else if (typeof $scope.event.invitees == 'string') {
                $scope.event.invitees = $scope.event.invitees.split(',');
            }
            $scope.event.state = ($scope.event.invitees.length == 0) ? 'Scheduled' : 'Pending';
            if ($scope.event.quorum > ($scope.event.invitees.length+1)) $scope.event.quorum = $scope.event.invitees+1;
            $scope.event.data = {};
        }
        $scope.event.img = getEventImg($scope.event.etype);
        if ($scope.event.startTimeLocal) {
            var s = translateTime($scope.event.startTimeLocal);
            if (s == null) {
                $myToast.show("I can't understand the time. Please enter it differently.");
                return;
            }
            $scope.event.startTime = s;
            $scope.event.startTimeLocal = s.toLocaleString();
        }

        if (!eid) {		
            wsCreateEvent($scope.event).then(function() {
                $rootScope.events.push($scope.event);
                sortEvents($rootScope.events);
                setupLocalReminder($scope.event.id, $scope.event.name, $scope.event.startTime, false);
                $scope.editing = false;
		$location.path('app/events/Upcoming');
                $myToast.show("Event created");
            }).fail(function() {
                console.log('event creation failed on server!');		
                $myToast.show("Event creation failed!");
            });		
        }
        else {
            console.log($scope.event);
            wsUpdateEvent($scope.event).then(function() {
                $scope.editing = false;
		$location.path('app/events/Upcoming');
                $myToast.show("Event updated");
            }).fail(function() {
                console.log('event update failed on server!');		
                $myToast.show("Event update failed!");
            });
        }
    }

    $scope.cancelEvent = function() {
        $scope.event.state = 'Cancelled';
        wsUpdateEvent($scope.event).then(function() {
            $myToast.show('Event cancelled');
        }).fail(function() {
            console.log('event update failed on server!');
        });
    }

    $scope.navSlide = function(index) {
        $ionicSlideBoxDelegate.slide(index, 100);
    }
    /*$scope.onSlideChanged = function(index) {
        if (index==1) {
            // map slide
            if ($scope.showMap == false) $scope.drawMarkersOnMap();
        }
    }*/
 
    // setup popover for actions/users/bots
    $ionicPopover.fromTemplateUrl('templates/msgPopover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.popover = popover;
    });
    $scope.selectAUB = function(aub) {
        if (aub == 'Actions') {
            // find last action addressed to me
            var lastAction = null;
            for (var i in $scope.event.messages) {
                var m = $scope.event.messages[i];
                if (m.body[0] == '#') {
                    if (m.sender != $rootScope.me) { 
                        lastAction = m.body.split(' ')[0];
                    }                         
                    break;
                }
            }
            $scope.aubList = getValidActions(lastAction);            
        }
        else if (aub == 'Bots') {
            $scope.aubList = ['/yelp','/google','/translate','/stock','/weather'];
        }
        else if (aub == 'People') {
            if ($rootScope.me == $scope.event.owner) {
                $scope.aubList = $.map($scope.event.invitees, function(a) { return '@'+a });
            }
            else {
                $scope.aubList = $.map($scope.event.invitees, function(a) { return (a==$rootScope.me)?('@'+$scope.event.owner):('@'+a) });
            }
        }
        else {
            $scope.inputMsg = aub + ' ' + $scope.inputMsg;
            $scope.popover.hide();
            $scope.aubList = $scope.rootAUBList;
            angular.element('#msgBox').focus();
        }
    }
    $scope.$on('popover.hidden', function() {
        $scope.aubList = $scope.rootAUBList;
    });

    $ionicPopover.fromTemplateUrl('templates/menuPopover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.menuPopover = popover;
    });
    $ionicPopover.fromTemplateUrl('templates/typePopover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.typePopover = popover;
    });
    $scope.selectType = function(e) {
       $scope.event.etype = e;
       $scope.typePopover.hide();
    };
    $ionicPopover.fromTemplateUrl('templates/etaPopover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.etaPopover = popover;
    });
    $scope.selectEta = function(e) {
       $scope.etaPopover.hide();
       $scope.sendEta(e);
    }
    $scope.sendEta = function(e) {
       $scope.inputMsg = '#eta ' + e;
       $scope.sendMessage();
       $scope.inputMsg = '';
       $scope.event.lastEta[$rootScope.me] = e;
       $scope.event.flags = '';
       $scope.$apply();
    }
    $scope.requestEta = function(userid) {
        if (userid == null) {
          if ($scope.event.owner == $rootScope.userid) {
            receivers = $scope.event.invitees.slice();
          } else {
            receivers = $scope.event.invitees.slice();
            receivers.push($scope.event.owner);
          }  
        }
        else {
          receivers = [userid];
        }
        var date = new Date();
        var atom = { 'eid' : $scope.event.id,
	       'id' : $rootScope.userid.substring(0,3) + Math.round(date/1000), // just need a reasonably unique id
	       'receivers' : receivers,
	       'sender' : $rootScope.userid,
	       'body' : '#ETA?',
	       'state': 'EtaRequestMsg',
	       'modified' : ISODateString(date)
        };
        wsSendEventMessage(atom).then(function() { 
            console.log('requested eta'); 
            $scope.event.lastEta[userid] = 'requested';
            $scope.$apply();
        }).fail(function() {console.log('failed to request eta');});
    } 
    $scope.showEtaPopover = function() {
        $scope.navSlide(1);
        $timeout(function() {
            angular.element('#sendEtaButton').triggerHandler('click');
        }, 100);
    }

    // setup popover for add contact
    $ionicPopover.fromTemplateUrl('templates/addContactPopover.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.contactPopover = popover;
    });
    $scope.selectContact = function(c) {
        var t = $scope.event.invitees.slice();
        t.push(c);
        $scope.event.invitees = t;
        $scope.contactPopover.hide();
    };
    var sanitizePhNum = function(p) {
	var p1 = p.replace(/[^\d]/g, "");
        // special case for US numbers beginning with country code 1
        if (p1.length == 11 && p1[0] == '1') {
            p1 = p1.substring(1);
        }
        return p1;
    };
    $scope.pickContactUsingNativeUI = function () {
        $cordovaContacts.pickContact().then(function(phoneContact) {
            console.log(phoneContact);
            var bwContact;
			var notBwContact;
            if (phoneContact.phoneNumbers) {
                $.each(phoneContact.phoneNumbers, function(i,p) { 
                    var p1 = sanitizePhNum(p.value);
					notBwContact = sanitizePhNum(p.value);
                    for (var i in $rootScope.allContacts) {
                        var c = $rootScope.allContacts[i];
                        if (sanitizePhNum(c.phone) == p1) {
                            bwContact = c.id;
                            break;
                        }
                    }
                });
            }
            if (bwContact) {
                $scope.selectContact(bwContact);
            } else {
                //$myToast.show('Sorry, this contact is not a BeeWaggle user (yet). Spread the buzz!');
				// Popup function
				$scope.showPopup = function() {
					$scope.msgPhone = {};
					$scope.msgPhone.phone = notBwContact;
					// An elaborate, custom popup
					var myPopup = $ionicPopup.show({
						templateurl: '',
						title: '',
						subTitle: 'Sorry, this contact is not a BeeWaggle user (yet). Spread the buzz!',
						scope: $scope,
						buttons: [
							{ text: 'Cancel' },
							{
								text: '<b>Invite</b>',
								type: 'button-positive',
								onTap: function(e) {
									if (!$scope.msgPhone.phone) {
										//don't allow the user to close unless the phone number is not defined
										e.preventDefault();
									} else {
										//return $scope.msgPhone.phone;
										$scope.msgPhone.message = $rootScope.me + " wants to invite you to a BeeWaggle event! Download the app here: http://autometon.com/dl/bw.apk";
										var smsOptions = {
											replaceLineBreaks: false, // true to replace \n by a new line, false by default
											android: {
												//intent: 'INTENT'  // send SMS with the default SMS app
												intent: ''        // send SMS without open any other app
											}
										}
										$scope.sendSms=function(){
											console.log($scope.msgPhone.phone);
											console.log($scope.msgPhone.message);
													
											$cordovaSms
											.send($scope.msgPhone.phone, $scope.msgPhone.message, smsOptions)
											.then(function() {
												// Success! SMS was sent
												console.log('Success');
												$myToast.show('SMS Sent.');
											}, function(error) {
												// An error occurred
												console.log(error);
												$myToast.show('Error while sending SMS.');
											});//then
										}
										
										return $scope.sendSms();
									}
								}
							}
						]
					});

					myPopup.then(function(res) {
						console.log('Tapped!', res);
					});
				};
				
				$scope.showPopup();
			}
        });
    };

    // handlers for yes/no/maybe updates
    $scope.sendYes = function() {
        $myToast.show('Sending Yes...');
        wsSendEventResponse($scope.event.id, "yes").then(function() {            
            console.log('sent yes response');
            $scope.event.inviteeStatus[$rootScope.userid] == 'yes';
            $scope.event.myStatus = 'yes';
            $scope.event.flags = '';
            setupLocalReminder($scope.event.id, $scope.event.name, $scope.event.startTime, false);
            $myToast.show('Added reminder');
            $rootScope.$broadcast('EventUpdated');
        }).fail(function() {
            $myToast.show('Failed sending. Try again');
	      });
    }
    $scope.sendNo = function() {
        $myToast.show('Sending No...');
        wsSendEventResponse($scope.event.id, "no").then(function() {
            console.log('sent no response');
            $scope.event.inviteeStatus[$rootScope.userid] == 'no';
            $scope.event.myStatus = 'no';
            $scope.event.flags = '';
            $rootScope.$broadcast('EventUpdated');
        }).fail(function() {
            $myToast.show('Failed sending. Try again');
        });
    }
    $scope.sendMaybe = function() {
        $myToast.show('Sending Maybe...');
        wsSendEventResponse($scope.event.id, "maybe").then(function() {
            console.log('sent maybe response');
            $scope.event.inviteeStatus[$rootScope.userid] == 'maybe';
            $scope.event.myStatus = 'maybe';
            $scope.event.flags = '';
            $rootScope.$broadcast('EventUpdated');
        }).fail(function() {
            $myToast.show('Failed sending. Try again');
        });
    }

    // handler for message send
    $scope.sendMessage = function() {
        if (! $scope.inputMsg) {
            $myToast.show('Write something in the message box');
            return;
        }

        var date = new Date();
        var receivers = [];
        var body = $scope.inputMsg;

        // check if message is privately addressed to single user
        var rx = /@(\S*)/g;
        var m = rx.exec(body);
        if (m && m.length >= 2) {
            receivers = [m[1]];
        }
        else {
            // add all invitees
            if ($scope.event.owner == $rootScope.userid) {
              receivers = $scope.event.invitees.slice();
            }
            else {
              receivers = $scope.event.invitees.slice();
              receivers.push($scope.event.owner);
            }
            // add bot if any (bot messages go to all invitees)
            var rx = /\/(\S*)(.*)/g;
            var m = rx.exec(body);
            if (m && m.length >= 2) {
                receivers.push(m[1].trim());
            }
        }
        console.log('sending to', receivers);
       
        var etaMsg = (body.indexOf('#eta') == 0); 
        var atom = { 'eid' : $scope.event.id,
            'id' : $rootScope.userid.substring(0,3) + Math.round(date/1000), // just need a reasonably unique id
            'receivers' : receivers,
            'sender' : $rootScope.userid,
            'body' : body.trim(),
            'state': 'TxtMsg',
            'modified' : ISODateString(date)
        };

        /*
        if (atom.body.indexOf('#location') == 0) atom.state = 'LocMsg';
        else if (atom.body.indexOf('#mylocation') == 0) {
            var posOptions = {timeout: 5000, enableHighAccuracy: false};
            $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
                var latP  = position.coords.latitude;
                var longP = position.coords.longitude;
                atom.body = '#mylocation:' + latP + ',' + longP;
                atom.state = 'LocMsg';
                wsSendEventMessage(atom).then(function() {
                  $scope.inputMsg = '';
                  $myToast.show('Sent current location');
                });
            }, function(err) {
                console.log('error getting my position');
            });
            return;
        }*/
           
        wsSendEventMessage(atom).then(function() {
            console.log('sent message');
	    $scope.inputMsg = '';
            if (! etaMsg) {
	        //$scope.event.messages.push(atom);
	        $scope.event.messages.splice(0, 0, atom);
	        saveEventLocal($scope.event);
	        updateEventFlags($scope.event, $rootScope.me);
                $ionicTabsDelegate.select(2);
	        $rootScope.$broadcast('EventUpdated');
            }
        }).fail(function() {
            $myToast.show('Failed sending message. Try again');
        });
    }

    $scope.newEvent = function() {
        console.log('creating new event');
        $location.path('app/event/-1');
    };

    $scope.cloneEvent = function() {
        console.log('cloning event');
        $scope.menuPopover.hide();
        var copyEvent = jQuery.extend(true, {}, $scope.event);
        $location.path('app/event/clone:' + $scope.event.id);
    };
    
    $scope.editEvent = function() {
        $scope.editing = true;
        $scope.menuPopover.hide();
    }

    // translate time string using chrono
    var translateTime = function(t) {
        if (!t || t == '') return '';
        var parsedTime = chrono.parse(t);
        if (! parsedTime || ! parsedTime[0]) return null;
        return parsedTime[0].start.date();
    }

    $scope.selectMessageTab = function() {
        if ($scope.event.flags == 'u') $scope.event.flags = '';
    }

    /*
    // setup map stuff

    // show map on demand, when user clicks Update button
    //***$scope.showMap = false;

    $scope.updateUserLocations = function() {
      // send out location requests to all invitees
      if ($scope.event.owner == $rootScope.userid) {
          receivers = $scope.event.invitees.slice();
      }
      else {
          receivers = $scope.event.invitees.slice();
          receivers.push($scope.event.owner);
      }
      var date = new Date();
      var ratom = { 
          'eid' : $scope.event.id,
          'id' : $rootScope.userid.substring(0,3) + Math.round(date/1000), // just need a reasonably unique id
          'receivers' : receivers,
          'sender' : $rootScope.userid,
          'body' : '#location',
          'state': 'LocMsg',
          'modified' : ISODateString(date)
      };
      console.log('sending location requests', ratom);
      wsSendEventMessage(ratom);
     
      // then find own user's location
      $cordovaGeolocation.getCurrentPosition().then(function (position) {
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        if (!$scope.event.lastLocations) $scope.event.lastLocations = {};
        $scope.event.lastLocations['me'] = lat + ',' + long;
      },function(error){
    	  console.log("gps error",error);
      });
    }
    
    $scope.drawMarkersOnMap = function() {
        var center = null;
        if ($scope.event.lastLocations && $scope.event.lastLocations['me']) {
           center = { latitude: $scope.event.lastLocations['me'].split(',')[0],
                      longitude: $scope.event.lastLocations['me'].split(',')[1]
                    };
           
           var my_location = new plugin.google.maps.LatLng(center.latitude,center.longitude);
           map.setCenter(my_location);
        }
        console.log(center);
       // $scope.map = { zoom: 4, bounds: {} };
        //if (center) $scope.map.center = center;
        // people markers
        var i = 0;
        $scope.markers = [];
        if ($scope.event.lastLocations) {
          map.clear();	// remove All mearkers from map
          i++;
          $.each($scope.event.lastLocations, function(k,v) {
            var la = parseFloat(v.split(',')[0]);
            var lo = parseFloat(v.split(',')[1]);
            console.log(k,la,lo);
            $scope.markers.push({
              id: i,
              latitude: la,
              longitude: lo,
              icon: 'img/map-markers/empty.png',
              title : '<h4>' + k + '</h4>',
              options: {
                labelContent : '<br/><span style="font-size:13px">'+k+'</span>',
                labelClass: 'markerClassForOthers icon ion-ios-location',
                labelStyle: {},
                labelInBackground: false
              }
            });
            	// show markers on map
            	const user_position = new plugin.google.maps.LatLng(la,lo);
	            map.addMarker({
	                'position': user_position,
	                'title': k
	              }, function(marker) {
	
	                marker.showInfoWindow();
	
	              });
            
            i++;
          });
        }
        if($scope.event_position){
	          map.addMarker({
	              'position': $scope.event_position,
	               icon: 'blue',
	              'title':  $scope.event.name
	            }, function(marker){	    	
	              marker.showInfoWindow();	    	
	            });
        }
        $scope.showMap = true;
    }

    // get position of user and then set the center of the map to that position
    $scope.mapUpdatedTime = new Date(); // this needs to fetch from localstorage
    $scope.updateMap = function() {
      $scope.mapUpdatedTime = new Date(); // this needs to store in localdb
      $myToast.show('Getting latest locations of your fellow bees...');
      $scope.updateUserLocations();
      // draw after 5 seconds
      $timeout($scope.drawMarkersOnMap, 5000);
    };
    */
    
    $scope.disableTap = function(){
        container = document.getElementsByClassName('pac-container');
        // disable ionic data tab
        angular.element(container).attr('data-tap-disabled', 'true');
        // leave input field if google-address-entry is selected
        angular.element(container).on("click", function(){
            document.getElementById('Autocomplete').blur();
        });
    }
})

.controller('SettingsCtrl', function($scope, $rootScope, $stateParams, $timeout, $state) {
})
