// This file contains functions that communicate with the server API.

var g_serverAddr = "https://autometon.com";
var g_authHeader = null;
var g_websocketConnection = null;
var _user = null;
var _pass = null;
var _actionGraph = { '#todo' : [ '#done' ]
                   };
                   
//
// sets remote server address depending on app context
//
function setRemoteServer(addr) {
    g_serverAddr = addr;
}

function authHeader(u, p) {
    return { "Authorization": "Basic " + btoa(u + ":" + p) };
}

function wsSetCreds(u, p) {
	_user = u;
	_pass = p;
}

function wsSyncEvents(sinceTime) {	
	var dfd = jQuery.Deferred();
	$.ajax({
	  type: "GET",
	  url: g_serverAddr + "/eventDelta/" + _user + "/" + sinceTime,
	  headers: authHeader(_user, _pass),
	  success: function(d) { 
		  dfd.resolve(d);
	  },
	  error: function(request, error) {
		  console.log("Error from GET events/ : " + error);
		  dfd.reject(request.responseText);
	  }
	});
	return dfd.promise();
}

function wsSendEventMessage(atom) {
	var dfd = jQuery.Deferred();
	$.ajax({
	  type: "POST",
	  url: g_serverAddr + "/eventAtoms",
	  headers: authHeader(_user, _pass),
      contentType: 'application/json',
      data: JSON.stringify(atom),
	  success: function(d) {
		  dfd.resolve(d);
	  },
	  error: function(request, error) {
		  console.log("Error from POST eventAtoms : " + error);
		  dfd.reject(request.responseText);
	  }
	});
	return dfd.promise();
}

function wsCreateEvent(e) {
	var dfd = jQuery.Deferred();
	$.ajax({
	  type: "POST",
	  url: g_serverAddr + "/events",
	  headers: authHeader(_user, _pass),
      contentType: 'application/json',
      data: JSON.stringify(e),	  
	  success: function(d) { 
		  dfd.resolve(d);
	  },
	  error: function(request, error) {
		  console.log("Error from POST events/ : " + error);
		  dfd.reject(request.responseText);
	  }
	});
	return dfd.promise();	
}

function wsUpdateEvent(e) {
	var dfd = jQuery.Deferred();
	$.ajax({
	  type: "PUT",
	  url: g_serverAddr + "/events/" + e.id,
	  headers: authHeader(_user, _pass),
      contentType: 'application/json',
      data: JSON.stringify(e),
	  success: function(d) {
		  dfd.resolve(d);
	  },
	  error: function(request, error) {
		  console.log("Error from PUT events/ : " + error);
		  dfd.reject(request.responseText);
	  }
	});
	return dfd.promise();	
}

function wsDeleteEvent(e) {
	var dfd = jQuery.Deferred();
	$.ajax({
	  type: "DELETE",
	  url: g_serverAddr + "/events/" + e.id,
	  headers: authHeader(_user, _pass),
	  success: function(d) {
		  dfd.resolve(d);
	  },
	  error: function(request, error) {
		  console.log("Error from DELETE events/ : " + error);
		  dfd.reject(request.responseText);
	  }
	});
	return dfd.promise();	
}

function wsUpdateUserPushToken(userid, token) {
	var dfd = jQuery.Deferred();
	$.ajax({
	  type: "PUT",
	  url: g_serverAddr + "/pushtoken/" + userid,
	  headers: authHeader(_user, _pass),
      contentType: 'application/json',
      data: JSON.stringify({'token': token}),
	  success: function(d) {
		  dfd.resolve(d);
	  },
	  error: function(request, error) {
		  console.log("Error from PUT /pushtoken/:uid : " + error);
		  dfd.reject(request.responseText);
	  }
	});
	return dfd.promise();	
}

function wsSendEventResponse(eid, response) {
	var dfd = jQuery.Deferred();
	$.ajax({
	  type: "PUT",
	  url: g_serverAddr + "/eventInvite/" + eid + "/" + _user + "/" + response,
	  headers: authHeader(_user, _pass),
	  success: function(d) {
		  dfd.resolve(d);
	  },
	  error: function(request, error) {
		  console.log("Error from PUT eventInvite/ : " + error);
		  dfd.reject(request.responseText);
	  }
	});
	return dfd.promise();    	
}

function getAllEventTypes() {
  //return ['Coffee', 'Lunch', 'Dinner', 'Game', 'Party', 'Movie', 'Pickup', 'Dropoff', 'Project', 'Call', 'Meeting', 'Reminder', 'CarPool', 'Class', 'Trip', 'Other'];
  return ['Meetup', 'Pickup', 'Call', 'Reminder', 'Trip', 'Other'];
}

function getAllEventCategories() {
  return ['Upcoming', 'Meetup', 'Call', 'Reminder', 'Other'];
}

function getEventCategory(etype) {
  if ($.inArray(etype, getAllEventCategories()) >= 0) return etype;
  else return 'Other';
}

function getEventImg(etype) {
    if ($.inArray(etype, ['Pickup','Dropoff','CarPool']) >= 0)
      return 'ion-model-s';
    else if ($.inArray(etype, ['Lunch']) >= 0)
      return 'ion-pizza';
    else if ($.inArray(etype, ['Dinner']) >= 0)
      return 'ion-wineglass';
    else if ($.inArray(etype, ['Coffee']) >= 0) 
      return 'ion-coffee';
    else if ($.inArray(etype, ['Game']) >= 0) 
      return 'ion-ios-baseball';
    else if ($.inArray(etype, ['Movie']) >= 0) 
      return 'ion-ios-film';
    else if ($.inArray(etype, ['Party']) >= 0)
      return 'ion-wand';
    else if ($.inArray(etype, ['Project']) >= 0)
      return 'ion-ios-list';
    else if ($.inArray(etype, ['Call']) >= 0)
      return 'ion-ios-telephone';
    else if (etype == 'Reminder') 
      return 'ion-android-alarm-clock';
    else
      return 'ion-android-alarm-clock';
}

function updateEventFlags(e, me) {
    // is the event past?
    // defn of past is event.startTime is smaller than 1 hour ago from now
    var now = new Date();
    now.setHours(now.getHours() - 1);
    var t = now;
    if (e.startTime != "0001-01-01T00:00:00Z") t = new Date(e.startTime);
    if (t < now) { e.past = true; e.state = 'Past'; }

    // if no messages, return
    if (! e.messages) return;

    // determine action flag
    // iterate messages from most recent to least recent
    // count unsatisfied actions not addressed to anyone, or specifically to me
    var nTodo = 0, nQuestion = 0, nEta = 0;
    for (var i = e.messages.length; i-- > 0; ) {
        var a = e.messages[i];
        if (a.sender != me && a.body.indexOf('#todo') >= 0) {
            if (a.body.indexOf('@') < 0 || (a.body.indexOf('@' + me) >= 0))
                nTodo++;
        }
        else if (a.sender == me && a.body.indexOf('#done') >= 0) {
            nTodo = 0; // response satisfies all previous questions
        }
    }
    if (nTodo>0) e.flags = 't';
    else if (nQuestion>0) e.flags = 'q';
    e.etaPending = (e.lastEta && e.lastEta[me] == 'requested');
}

function getValidActions(curAction) {
    var a = [];
    if (curAction && _actionGraph[curAction]) {
        a = _actionGraph[curAction];
    }
    return a.concat(Object.keys(_actionGraph));
}

//
// checks if there is an online connection
//
function isOnline() {
    if ('onLine' in navigator) {
       return navigator.onLine;
    }
    else {
        // default to true optimistically
        return true;
    }
}

//
// Sends user credentials to server to authenticate.
// Returns a boolean indicating status.
//
function loginUser(userid, password) {
    console.log("api: logging in user " + userid);
    g_authHeader = authHeader(userid, password); 
    var authenticated = false;
    $.ajax({
        type: "GET",
        url: g_serverAddr + "/users/login",
        //username: userid,
        //password: password,
        headers: g_authHeader,
        async: true,
        success: function(data) {
            authenticated = true;
        },
        error: function(request, error) {
            console.log("Error from GET /users/login: " + request.responseText);
        }
    });
    return authenticated;
}

//
// Registers a new user with server.
//
function wsRegisterUser(userid, ph, password) {
    console.log("api: registering user", userid, ph, password);
    var u = { 'id' : userid, 'phone': ph, 'password': password, 'name': userid };
    var dfd = jQuery.Deferred();
    $.ajax({
        type: "POST",
        url: g_serverAddr + "/users",
        contentType: 'application/json',
        data: JSON.stringify(u),
        //headers: authHeader('admin', 'admin'), // TODO: need authorization to create user
        success: function(d) {
            dfd.resolve(d);
        },
        error: function(request, error) {
            console.log("Error from POST /users: " + request.responseText);
            dfd.reject(request.responseText);
        }
    });
    return dfd.promise();
}

//
// Gets the public user profile of a given user from server.
// If user doesn't exist, returns null.
//
function getUser(userid) {
    console.log("api: get user " + userid);
    var user = null;
    $.getJSON(g_serverAddr + '/users/id/' + userid)
        .done(function(data) { user = data; })
        .fail(function(jqxhr, textStatus, error) { console.log("Error getting user profile: " + textStatus + ", " + error); });
    return user;
}

//
// given a list of phone numbers, finds users registered by these numbers
//
function wsFindUsersByPhoneNumber(phoneNumbers) {
    console.log("api: finding users by phone numbers");
	var dfd = jQuery.Deferred();
    $.ajax({
        type: "PUT",
        url: g_serverAddr + "/searchusers",
        data: JSON.stringify(phoneNumbers),
        contentType: 'application/json',
        success: function(data) {
            dfd.resolve(data);
        },
        error: function(request, error) {
            console.log("Error from PUT /searchusers: " + request.responseText);
            dfd.reject(request.responseText);
        }
    });
    return dfd.promise();
}

//
// sends password reset request for userid
//
function sendForgotPasswordRequest(userid) {
    console.log('api: sending forgot password request');
    var err = null;
    $.ajax({
        type: "POST",
        url: g_serverAddr + "/users/gentoken/" + userid,
        success: function(data) {
        },
        error: function(request, error) {
            console.log("Error from POST /users/gentoken: " + request.responseText);
            err = request.responseText;
        }
    });
    return err;
}

function discoverPorts(userid, term, distance) {
    console.log("api: discovering ports");
    var data = null;
    var q = term ? ('?q=' + term) : '';
    $.getJSON(g_serverAddr + '/ports/discover/' + userid + '/' + distance + q)
        .done(function(d) { data = d; })
        .fail(function(jqxhr, textStatus, error) { console.log("Error getting discovered ports: " + textStatus + ", " + error); });
    return data;
}

//
// Opens a websocket connection to server, to receive flow messages sent by others.
// 
function openWebSocketToServer(userid, receiveCallback) {
    if (! "WebSocket" in window) {
        alert("WebSockets not supported by your browser. No notifications, sorry!");
        return;
    }

    var loc = window.location, new_uri;
    if (loc.protocol === "https:") {
        url = "wss://";
    } else {
        url = "ws://";
    }
    if (g_serverAddr == "..") {
        url += loc.host;
    }
    else {
        url += g_serverAddr.replace('http://', '').replace('https://', '');
    }
    url += ":5000/websocket/" + userid;
    console.log("connecting to websocket url: " + url);

    g_websocketConnection = new WebSocket(url);
    g_websocketConnection.onopen = function() {
        console.log("websocket connection opened");
    };

    g_websocketConnection.onmessage = function (evt) { 
        var received_msg = evt.data;
        console.log("received websocket msg: " + received_msg);
        var flowDelta = JSON.parse(evt.data);
        receiveCallback(flowDelta);
    };

    g_websocketConnection.onclose = function() { 
        g_websocketConnection = null;
        console.log("websocket connection closed"); 
    };
}

//
// Checks health of current websocket connection, returns boolean
// 
function checkWebSocketToServer() {
    if (g_websocketConnection) {
        return (g_websocketConnection.readyState == WebSocket.OPENING || g_websocketConnection.readyState == WebSocket.OPEN);
    }
    else {
        return false;
    }
}

//
// Closes websocket connection to server.
//
function closeWebSocketToServer() {
    if (g_websocketConnection) {
        g_websocketConnection.close();
        g_websocketConnection = null;
        console.log("websocket closed");
    }
}
