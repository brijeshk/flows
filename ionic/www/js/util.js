// This file contains utility functions.

//
// is phonegap application or browser app?
//
function isDeviceApp() {
    if (window._cordovaNative) {
        console.log('Running as mobile app');
        return true;
    }
    else {
        console.log('Running as browser app');
        return false;
    }
}

//
// util function to format time as RFC3339
//
function ISODateString(d){
 function pad(n) {return n<10 ? '0'+n : n}
 return d.getUTCFullYear()+'-'
      + pad(d.getUTCMonth()+1)+'-'
      + pad(d.getUTCDate())+'T'
      + pad(d.getUTCHours())+':'
      + pad(d.getUTCMinutes())+':'
      + pad(d.getUTCSeconds())+'Z'
}

//
// util function to generate UUID
//
function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
}

//
// computes size of browser local storage used
//
function getLocalStorageSize() {
    var allStrings = '';
    for(var key in window.localStorage){
        if(window.localStorage.hasOwnProperty(key)){
            allStrings += window.localStorage[key];
        }
    }
    return allStrings ? 3 + ((allStrings.length*16)/(8*1024)) + ' KB' : '0 KB';
}

function myToast(msg, timeout) {
    var $toast = $('<div class="ui-loader ui-overlay-shadow ui-body-e ui-corner-all"><p style="font-size:14px">' + msg + '</p></div>');

    $toast.css({
        display: 'block', 
        background: 'darkslategray',
        color: 'white',
        'border-color': '#B85353',
        opacity: 0.95, 
        position: 'fixed',
        padding: '3px',
        'text-align': 'center',
        width: '300px',
        left: $(window).width() / 2 - 150,
        top: 100
    });

    var removeToast = function(){
        $(this).remove();
    };

    $toast.click(removeToast);

    $toast.appendTo($.mobile.pageContainer).delay(timeout);
    $toast.fadeOut(400, removeToast);
}

//
// shows a self-dismissing toast-like popup message at bottom
//
function showPopupMsg(msg, timeout) {    
    myToast(msg, timeout || 3000);    
}


//
// extract tokens from a string matching a template
//
function extractTokensFromText(template, text, tokens) {
    var tokenList = [], regexpTemplate = template.replace(/\./g, '\\.');
    
    // Find the order of your tokens
	var i, tokenIndex, tokenEntry;
    for (var k in tokens) {
        var m = '{' + tokens[k] + '}';
        tokenIndex = template.indexOf(m);
		
        // Token found
		if (tokenIndex > -1) {
			regexpTemplate = regexpTemplate.replace(m, '(.+)');
			tokenEntry = {
				index: tokenIndex,
				token: tokens[k]
			};

			for (i = 0; i < tokenList.length && tokenList[i].index < tokenIndex; i++);
			
            // Insert it at index i
			if (i < tokenList.length) {
				tokenList.splice(i, 0, tokenEntry);
			}
            // Or push it at the end
            else {
				tokenList.push(tokenEntry);
			}
		}
    }

    regexpTemplate = regexpTemplate.replace(/\{[^{}]+\}/g, '.*');

    var match = new RegExp(regexpTemplate).exec(text.toLowerCase()), result = null;
    
    if (match) {
        result = {};
	
        // Find your token entry
        for (i = 0; i < tokenList.length; i++) {
            result[tokenList[i].token] = match[i + 1];
        }
    }

    return result;
}
