// Ionic Starter App

angular.module('ionic.utils', ['ionic','ngAutocomplete'])
.factory('$locstore', ['$window', function($window) {
    return {
      set: function(key, value) {
        $window.localStorage[key] = value;
      },
      get: function(key, defaultValue) {
        return $window.localStorage[key] || defaultValue;
      },
      setObject: function(key, value) {
        $window.localStorage[key] = JSON.stringify(value);
      },
      getObject: function(key) {
        return JSON.parse($window.localStorage[key] || '{}');
      },
      removeItem: function(key) {
        $window.localStorage.removeItem(key);
      }
    }
}])
.factory('$myToast', ['$cordovaToast', function($cordovaToast) {
    return {
      show: function(str) {
        try {
          $cordovaToast.showLongCenter(str);
        } 
        catch (e) {}
      }
    }
}]);

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ionic.utils', 'starter.controllers', 'ngCordova', 'nemLogging', 'ion-google-place', 'angularMoment'])


.run(function($ionicPlatform, $rootScope, $cordovaContacts, $location, $animate, $cordovaPush, $state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
    }

    // turn off animations for perf
    $animate.enabled(false);

    $rootScope.appPaused = false;
    var onAppPause = function() {
        console.log("app paused");
        $rootScope.appPaused = true;
    }
    var onAppResume = function() {
        console.log("app resumed");
        $rootScope.appPaused = false;
        $rootScope.$broadcast('AppResumed');
    }
    document.addEventListener("pause", onAppPause, false);
    document.addEventListener("resume", onAppResume, false);

    // Disable BACK button on home
    $ionicPlatform.registerBackButtonAction(function(e) {
        if ($state.current.name == "app.events"){
            console.log('disabling close app on hardware back button');
        }
        else {
            navigator.app.backHistory();
        }
    }, 100);

    // register for push notifications from GCM
    var registerForPush = function() {
        // senderID is Project Number for AutoMeton from Google Developer Console
        console.log('registering for push notifications...');
        var pushConfig = { 'senderID' : ' 304744626895' };
        $cordovaPush.register(pushConfig).then(function(result) {
          console.log('registered for push notifications');
        }, function(err) {
          console.log('!!error from register push notification', err);
        });
    }

    try {
        $rootScope.allContacts = [];
        console.log('reading device contacts using plugin');
        $cordovaContacts.find({filter:''}).then(function(clist) {
            var phList = [];
            $.each(clist, function(i,c) {
                //if (c.name.formatted && c.name.formatted!='') 
                //  $rootScope.allContacts.push(c.name.formatted); 
                if (c.phoneNumbers) {
                    $.each(c.phoneNumbers, function(i,p) { phList.push(p.value); });
                }
            });
            wsFindUsersByPhoneNumber(phList).then(function(users) {
                var arr = [];
                $.each(users, function(i,u) { arr.push(u); });
                /*// uniqify the list
                arr = $.grep(arr, function(v, k){
                    return $.inArray(v ,arr) === k;
                });*/
                $rootScope.allContacts = arr;
                //$rootScope.allContacts.sort();
                console.log('allcontacts', $rootScope.allContacts);
            });
        });

        // TODO: ideally, register device for push notifications only on first sync
        // for testing, let's register each time
        //if (ls == "0") {
        registerForPush();
        //}
    } catch(e) {}
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $compileProvider) {

  /*
  uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyDTVXrmXr27HhwrIbQb9eOeT2FURZ9TSY0',
        v: '3.20',
        libraries: 'weather,geometry,visualization',
        language: 'en',
        sensor: 'false',
  })*/

  $ionicConfigProvider.navBar.alignTitle('left');

  // perf tuning
  //$ionicConfigProvider.scrolling.jsScrolling(false);
  $ionicConfigProvider.views.transition('none');
  $compileProvider.debugInfoEnabled(false);

  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html'
      }
    }
  })

  .state('app.settings', {
      url: '/settings',
      views: {
        'menuContent': {
          templateUrl: 'templates/settings.html',
		  controller: 'SettingsCtrl'
        }
      }
    })
    .state('app.events', {
      url: '/events/:filterCat',
      views: {
        'menuContent': {
          templateUrl: 'templates/events.html',
          controller: 'EventListCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/event/:eventId',
    views: {
      'menuContent': {
        templateUrl: 'templates/event.html',
        controller: 'EventCtrl'
      }
    }
  });
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
})

.directive('disableTap', function($timeout) {
  return {
    link: function() {

      $timeout(function() {
        document.querySelector('.pac-container').setAttribute('data-tap-disabled', 'true')
      },500);
    }
  };
})

.directive('holdList', ['$ionicGesture', function($ionicGesture) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      $ionicGesture.on('hold', function(e) {

        var content = element[0].querySelector('.item-content');

        var buttons = element[0].querySelector('.item-options');
        var buttonsWidth = buttons.offsetWidth;

        ionic.requestAnimationFrame(function() {
          content.style[ionic.CSS.TRANSITION] = 'all ease-out .25s';
		  
          if (!buttons.classList.contains('invisible')) {
            content.style[ionic.CSS.TRANSFORM] = '';
            setTimeout(function() {
              buttons.classList.add('invisible');
            }, 250);
          } else {
            buttons.classList.remove('invisible');
            content.style[ionic.CSS.TRANSFORM] = 'translate3d(-' + buttonsWidth + 'px, 0, 0)';
          }
        });

      }, element);
    }
  };
}]);
