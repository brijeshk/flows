
% SeMail (Semantic Mail) 

# Typical day in the life of an IT worker

- Wake up
- Out of the 100+ emails I received overnight, what do I need to respond to?
    - Mentally filter, organize by priority, flag
- What defects/tasks were assigned to me, and which ones do I need to comment on?
    - Logon to tracker website, browse, triage
- Do I need to approve any expense reports or vacation requests?
- What is the status of the tasks I assigned to members of my team to complete by today?
    - Logon to task tracker website, apply my team tasks filter, check 
- My rolled up status report is due to my manager by today. What was accomplished during the week?
    - Recollect what happened during the week, look at email history, write summary
- I am reaching close to 90% of my mailbox capacity. 
    - Archive, delete
- Do useful work inbetween

# Problem 

- Email is not structured enough to be a rich collaboration mechanism in the IT world
    - onus on users to organize & filter
    - onus on users to interpret intent based on unstructured text (subject & body of email)
    - no easy way of answering even simple questions like "what tasks and replies are others dependent on me today?"
- No single integrated application for communication, task & project management
    - loose integration at best (e.g. bug-tracker sending email notification when a task is assigned to a user)
    - user's outstanding 'work' is a combination of information stored in disconnected stores (email, bug tracker, project management tool, documents etc.)

# Rethinking enterprise groupware

- Enhance email to include metadata about **intent/state, context, action, relationships**
- Develop a single *desktop* application that's the mother of email, project management, bug tracking
- Create a one stop shop for a user to access & interact with any collaborative information
- Let system perform smart house-keeping, don't need users to ever delete/archive content manually

# Email can be more than just a message

- What is my intent when I send a message to you (or a group)?
    - Do I want a reply from you? (or am I answering your question?)
    - Am I assigning a task to you?
    - Am I announcing something to a group?
    - Am I requesting information from a group?
- What if I am able to tag my intent when I send a message to you?
    - _Want Reply_, _Task_, _Announcement_, _Need Help_, etc.
- And your email client automatically organizes messages by sender's intent?
    - Show me all the messages that others are waiting for a reply from me
    - Show me all my tasks for the day assigned by my boss
    - Are there any project related announcements that I haven't read yet?
- Conceptually different from rule-based filtering at receiver

# And include context & custom metadata

- Context may be anything that makes sense in your group or organization
    - projects, departments, function etc.
- Context can be auto-discovered or explicitly specified
- Context helps partition, organize and search easily
    - Find me all mails from the QA group related to Project X subfeature Y (not equal to a text search)
- State is used to signify the currency of the information being messaged
    - And with this resolution, this issue is _closed_
    - Here's a new issue reported by the customer that has been _triaged_, but _not investigated_
- State can include deadlines, urgency etc.
    - Here's a task I want you to resolve before end of week
- Metadata can be entirely custom (defined by a project group)

# Let's formalize these concepts

- **semail** : an atomic, unmodifiable, semantically rich message that's sent from one user to update a _flow_
    - a _semail_ contains text, usual email headers (sender/receiver/priority etc.), intent, context, state, action, custom metadata
    - all _semails_ that reside on one server become part of _flows_
    - _semails_ that cross domains are treated as traditional emails 
- **flow** : a modifiable thread of communication that includes context, intent/state, relationships, custom metadata and an ordered history of _semail_ updates 
    - a _flow_'s context, state and other metadata may be updated by its constituent _semails_ 
    - rules specify how individual _semails_ affect the context of the _flow_ they are directed to (e.g. last-update-wins etc.)
    - infact, a _semail_ is nothing but an atomic create/update of a _flow_

# My Work Queue(s) 

- Automatic queues to organize pending pending work based on state & context
    - To-Reply, To-Do, To-Fix, To-Review items presented in one logical view
    - Never need to worry about things 'slipping through the cracks'
    - Much more powerful than simply organizing messages by folders or labels 

# Active Flows

- Quickly access _flows_ that are active (recently updated) in one integrated client. 
- ??

# Relationships 

- Emails have a single related-ness concept - threads
    - this is already captured inside a single _flow_
- Project management tools have a couple of dependency concepts 
    - tasks and subtasks, dependency between tasks
- In reality, communication-related information is a graph of relationships
    - a flow may be a fork off a parent flow (forwards and branches) 
    - a flow may consist of sub-flows (subtasks for a composite task)
    - a flow may be dependent on other flows (project dependencies)
    - a flow may be linked to other flows (related bugs etc.)

# Flow Graphs

- SeMail models flow relations as graphs 
- Prebuilt relation types and custom ones
    - fork_of, part_of, depends_on, dupe_of, like
- Visualize communication as graphs or trees, with metadata overlaid on top 
    - Can expose interesting patterns in information flow 
    - For example, a seed idea can grow deep branches
    - A whole project may be born from a single flow of communication between 2 users
    - The evolution of such ideas is entirely recorded in the system
- Visualize hubs of information flow pivoted by users
    - Who's contributing most (creatively) to a project?
    - Who's the operational bottleneck in a team?

# Connecting to existing systems

- Using SeMail doesn't preclude an organization from using existing systems if desired
- Flexible plugin infrastructure to support interactions
- Send IMAP-based mail on every flow update (especially useful in cross-domain communication)
- Connect to systems like JIRA, TeamForge etc. using their API. Translate SeMail operations to these legacy systems

# Business Opportunity

- Groupware is a mature market, we are not targeting a groupware replacement
- There's a potential opportunity at the nexus of communication & project management 
- Business Model

# Modeling peer-to-peer email

- traditional email is modeled as letters exchanged between users (threaded conversations are simply views on top of that model)
- let's model it as a conversation flow happening on a whiteboard, with participants communicating by writing on the board (akin to IM)
- conversations could be private (1-on-1), multi-user (mailing lists), or public (newsgroups)
- A _semail_ is always targeted to a _flow_, not to another user
- A _receivers_ attribute shall make the _flow_ visible to other users
- Both sender and receivers are then peer users that may update the _flow_ 
- A _flow_ doesn't belong to one user or reside in one inbox, it belongs to the system
- Client app presents access to all _flows_ that user has been involved in 

# Modeling business/project processes 

- Bug Tracking
    - a bug _flow_ may have context=Project, state=Open/Resolved/Closed, assignee=user 
    - each _semail_ may update the _flow_ state and/or assignee
- Code/Document/Design Reviews:
    - a review _flow_ may have context=Project, state=Unreviewed/Reviewed/Accepted, reviewers=users
- Project management
    - track dependencies between tasks, subtasks 
    - track and manage deadlines

