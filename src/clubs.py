"""
Club management functions.
"""
from mongoengine import *
import protocols
import users


class Club(Document):
    name = StringField(required=True, primary_key=True, unique=True)
    creator = StringField()
    own_protocols = ListField(ReferenceField(protocols.FlowProtocol))
    users = ListField(StringField())
    public = BooleanField()
    def __str__(self):
        return self.name


def new_club(club_name, creator, users, public):
    """
    Create a new club with specified users.
    The Converse protocol is the only default protocol assigned to the club.
    One other custom protocol may be added later.
    """
    g = Club(name=club_name, creator=creator, users=users, public=public)
    g.own_protocols = [protocols.get_converse_protocol()]
    g.save()
    return g


def get_club(club_name):
    return Club.objects.get(name=club_name)


def add_user_to_club(club_name, user_name):
    o_club = get_club(club_name)
    o_club.users.append(user_name)
    o_club.save()


def remove_user_from_club(club_name, user_name):
    o_club = get_club(club_name)
    o_club.users.remove(user_name)
    o_club.save()


def get_clubs_by_user(user_name):
    return Club.objects(users=user_name)


def get_public_clubs():
    return Club.objects(public=True)


def get_clubs_by_creator(user_name):
    return Club.objects(creator=user_name)


def set_club_port(club_name, portid):
    """
    Sets club port (overwrites any previously set port). 
    Converse port is always available.
    """
    o_club = get_club(club_name)
    o_club.own_protocols = [protocols.get_converse_protocol()]
    o_club.own_protocols.append(protocols.find_protocol_by_id(portid))
    o_club.save()
    return o_club
