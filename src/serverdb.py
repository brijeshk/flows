"""
Db management functions.
"""

from mongoengine import *
import users
import protocols


def init_db(db, host):
    """
    Initialize connection to db.
    :type db: str
    :type host: str
    """
    connect(db, host=host)


def clear_db():
    """
    Reset the db - drops all data!
    """
    users.User.drop_collection()
    protocols.FlowProtocol.drop_collection()
