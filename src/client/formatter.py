"""
Module to print out flows as JSON or text (for command line tools).
"""

FLOW_FORMAT_STR = '{: >4} {: >20} {: >20} {: >10} {: >20} {: >20}'
FLOW_FORMAT_HDR = [ 'id', 'users', 'port', 'state', 'todo', 'subject' ]
FLOW_FORMAT_HDR_SEP = '-' * 80
format_json = False


class TerminalColors:
    HEADER = '\033[95m'
    OKGREEN = '\033[92m'
    OKBLUE = '\033[94m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'


def strme(user, me):
    return str(user) if user != me else 'me'

def print_header():
    print FLOW_FORMAT_STR.format(*FLOW_FORMAT_HDR)
    print FLOW_FORMAT_HDR_SEP

def print_atom(atom, me):
    ft = atom.created.strftime('%Y-%m-%d %H:%M')
    t = (':' + atom.data) if atom.data else ''
    atomColor = TerminalColors.OKBLUE if atom.sender == me else TerminalColors.OKGREEN
    print atomColor + '({0}) {1} -> [{2}{3}] {4}'.format(ft,
                                             strme(atom.sender, me),
                                             atom.state,
                                             t,
                                             atom.body) + TerminalColors.ENDC

def print_flow(me, f, detail=False):
    if format_json:
        print f.to_json()
        return

    if not detail:
        strlist = []
        strlist.append(str(f.id))
        strlist.append(f.users.replace(me, 'me'))
        strlist.append(str(f.protocol))
        strlist.append(str(f.state))
        if f.assignee:
            strlist.append(strme(f.assignee, me))
        else:
            strlist.append('')
        strlist.append(f.subject)
        print FLOW_FORMAT_STR.format(*strlist)
    else:
        for atom in f.atoms:
            print_atom(atom, me)


def print_flows(me, flows, detail=False):
    if format_json:
        print '[' + ','.join([f.to_json() for f in flows]) + ']'
    else:
        if not detail:
            print_header()
        for f in flows:
            print_flow(me, f, detail)
