#!/usr/bin/python
"""
Module to send and receive Flow mails using an SMTP/IMAP server.
A flow sent as an email shall contain an X-Header called X-Flow prefixed with #MP.
At any point, this module can fetch flows from IMAP by executing an
IMAP query on 'X-Flow=#MP' and 'SENTSINCE=lastSyncTime'.
"""
import imaplib
import email
import smtplib
import quopri
import HTMLParser
import logging
import flows


SMTP_SERVER = 'smtp.gmail.com'
SMTP_PORT = 587
IMAP_SERVER = 'imap.gmail.com'


session = None

def login(sender, password):
    """
    Authenticates user credentials against IMAP server.
    :rtype: boolean
    """
    try:
        session = imaplib.IMAP4_SSL(IMAP_SERVER)
        session.login(sender, password)
        return True
    except Exception as e:
        logging.exception('IMAP login failed')
        return False


def logout():
    if session:
        session.logout()
        session = None


def _extract_body_from_message(message):
    """
    Extracts body of email message.
    """
    def toutf(part):
        charset = part.get_content_charset() if part.get_content_charset() is not None else 'ascii'
        s = part.get_payload(decode=True)
        if part.get_content_type() == "text/html":
            h = HTMLParser.HTMLParser()
            return h.unescape(unicode(s, charset))
        else:
            return unicode(quopri.decodestring(s), charset)

    maintype = message.get_content_maintype()
    if maintype == 'multipart':
        for part in message.get_payload():
            if part.get_content_maintype() == 'text':
                return toutf(part)
            elif maintype == 'text':
                return toutf(message)
    else:
        return toutf(message)


def _fetch_all_flows_from_imap(user, password):
    """
    Fetches all messages in a given IMAP account sent from our system.
    """
    session.select("inbox")
    imap_query = '(HEADER X-Flow "#MP")'
    result, data = session.uid('search', None, imap_query)
    if result == 'OK':
        ids = data[0].split()
        for i in ids:
            result, data = session.uid('fetch', i, '(RFC822)')
            raw_email = data[0][1]
            email_message = email.message_from_string(raw_email)

            for item in email_message.items():
                print item[0] + ': ' + item[1]
            print '---------------------'
    session.close()


def fetch_flow_from_imap(user, password, subjectSearchTerm, flow):
    """
    Fetches messages in a given IMAP account part of specified flow.
    """
    session.select("inbox")

    # all messages containing X-Flow header with this flow uuid
    imap_query = '(HEADER X-Flow "#MP:{0}")'.format(flow.uuid)
    # TODO: fetch only items since last sync
    #imapQuery = '(HEADER X-Flow "#MP:{flowid}") (SENTSINCE {startdate}) (SENTBEFORE {enddate})'.format(
    #            flowid=flow.uuid,
    #            startdate=(datetime.date.today() - datetime.timedelta(3)).strftime("%d-%b-%Y"),
    #            enddate=(datetime.date.today()).strftime("%d-%b-%Y"))

    result, data = session.uid('search', None, imap_query)
    if result == 'OK':
        flow.atoms = []
        ids = data[0].split()
        for i in ids:
            result, data = session.uid('fetch', i, '(RFC822)')
            if result == 'OK':
                raw_email = data[0][1]
                email_message = email.message_from_string(raw_email)

                # parse message details from X-Flow header
                atom = flows.Atom()
                atom.id = email_message['Message-ID']
                atom.sender = email_message['From']
                atom.body = _extract_body_from_message(email_message)
                flow_x_header = email_message['X-Flow']
                fields = flow_x_header.split(':')
                if len(fields) < 2:
                    logging.warn('X-Flow header has no flow id : ' + flow_x_header)
                    continue
                flow_uuid = fields[1]
                if len(fields) > 2:
                    protocol_name = fields[2]
                if len(fields) > 3:
                    atom.state = fields[3]
                if len(fields) > 4:
                    atom.data = fields[4]
                flow.atoms.append(atom)
            else:
                logging.error('Error retrieving message ' + i + ': ' + result)
        #if len(flow.atoms) > 0:
            # save last message's state onto flow
            #flow.state = flow.atoms[-1].state
            #flow.data = flow.atoms[-1].data
    else:
        logging.error('Error executing search on IMAP: ' + result)

    session.close()


def send_flow_message(sender, password, receiver, atom, flow):
    """
    Sends a message via SMTP.
    :rtype: str, unique msg_id for the sent mail
    """
    # create X-Flow header as '#MP:flowID:protocol:state:data
    flow_x_header = '#MP:' + str(flow.uuid)
    if flow.protocol:
        flow_x_header += ':' + str(flow.protocol)
    if atom.state:
        flow_x_header += ':' + atom.state
    if atom.data:
        flow_x_header += ':' + atom.data

    atom.id = email.utils.make_msgid()
    headers = ["From: " + sender,
               "Subject: " + flow.subject,
               "To: " + receiver,
               "X-Flow: " + flow_x_header,
               "Message-ID: " + atom.id,
               "MIME-Version: 1.0",
               "Content-Type: text/html"]
    if flow.atoms:
        thread_message_ids = [a.id for a in flow.atoms]
        headers.append('References: ' + ' '.join(thread_message_ids))
    headers = "\r\n".join(headers)

    S = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    S.ehlo()
    S.starttls()
    S.ehlo()
    S.login(sender, password)
    S.sendmail(sender, receiver, headers + "\r\n\r\n" + atom.body)
    S.quit()
    flow.atoms.append(atom)


if __name__ == '__main__':
    '''
    f = Flow(subject='hi', creator='brijesh', users=['brijesh', 'ramya'],
             protocol=protocols.find_protocols_by_name('Music-Lessons')[0])
    f.atoms = []
    send_flow_message('brijeshsk@gmail.com', '',
                       'rambri@gmail.com',
                       'is this class available?',
                       f)
    fetch_flow_via_imap('brijeshsk@gmail.com', '', '')
    '''
    _fetch_all_flows_from_imap('rambri@gmail.com', '')
