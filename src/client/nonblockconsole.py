import sys
import select
#import tty
#import termios
import time

class NonBlockingConsole(object):

    def __enter__(self):
        self.old_settings = termios.tcgetattr(sys.stdin)
        #tty.setcbreak(sys.stdin.fileno())
        return self

    
    def get_data(self):
        if select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], []):
            return sys.stdin.read(1)
        return False


if __name__ == '__main__':
    # Use like this
    with NonBlockingConsole() as nbc:
        i = 0
        while 1:
            time.sleep(2)
            print i
            i += 1

            if nbc.get_data() == '\x1b':  # x1b is ESC
                break
