"""
Flows module.
Defines flow-related functions, including retrieving/creating/searching flows.
"""

import protocols
import datetime
import client
import imapsmtp
import xmpp
import logging
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
import base64
import uuid

Base = declarative_base()

db_session = None


class AtomCRUDType:
    create = 'c'
    update = 'u'
    delete = 'd'


class Flow(Base):
    __tablename__ = 'flow'
    id = Column(Integer, primary_key=True)
    uuid = Column(String(50), nullable=False)
    creator = Column(String(50), nullable=False)
    assignee = Column(String(50), nullable=True)
    protocol = Column(Integer, nullable=False)
    state = Column(String(50), nullable=True)
    subject = Column(String(255), nullable=True)
    users = Column(String(255), nullable=False)
    data = Column(String(255), nullable=True)

 
class Atom(Base):
    __tablename__ = 'atom'
    id = Column(Integer, primary_key=True)
    sender = Column(String(50), nullable=False)
    crud_flag = Column(String(10), nullable=False)
    state = Column(String(50), nullable=True)
    body = Column(String(255), nullable=True)
    data = Column(String(255), nullable=True)
    flow_id = Column(Integer, ForeignKey('flow.id'))
    flow = relationship("Flow", backref=backref('atoms', order_by=id))
    created = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)


def _generate_base64_uuid():
    return base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', '')


def getFlow(flowid):
    """
    Returns a flow by id.
    :type flowid: str or int
    :rtype: Flow
    """
    try:
        return db_session.query(Flow).filter(Flow.id == int(flowid)).one()
    except Exception as e:
        #logging.exception('Error getting flow ' + flowid)
        return None


def getFlowByUUID(flow_uuid):
    """
    Returns a flow by its uuid.
    :type flow_uuid: str
    :rtype: Flow
    """
    try:
        return db_session.query(Flow).filter(Flow.uuid == flow_uuid).one()
    except Exception as e:
        return None


def refreshFlow(flow):
    """
    Refreshes the state of a flow from db.
    """
    db_session.refresh(flow)


def syncFlow(f, forUser):
    """
    Syncs flow by fetching latest messages from transport layer.
    """
    _get_messages_via_transport_layer(f, forUser)


def findFlows(any_user, from_user, assignee, port, states):
    """
    Finds flows by user, assignee, state.
    :type any_user: str
    :type from_user: str or None
    :type assignee: str or None
    :type port: str or None
    :type states: list[str] or None
    :rtype: list[Flow]
    """
    q = db_session.query(Flow)
    if any_user:
        q = q.filter(Flow.users.like('%' + any_user + '%'))
    if from_user:
        q = q.filter(Flow.creator == from_user)
    if assignee:
        q = q.filter(Flow.assignee == assignee)
    if port:
        q = q.filter(Flow.protocol == port.id)
    if states:
        q = q.filter(Flow.state.in_(states))
    return q.all()


def _is_message_addressed_to_oneself(user_from, list_user_to):
    return len(list_user_to) == 1 and list_user_to[0].name == user_from.name


def newFlow(protocolid, fromUser, toUsers, subject, body, state, data=None):
    """
    Creates a new flow and sends message through transport.
    """
    atom = Atom(sender=fromUser.name, body=body, state=state, data=data, crud_flag=AtomCRUDType.create)
    flow = _create_flow_in_db(protocolid, atom,
                              [u.name for u in toUsers],
                              subject,
                              _generate_base64_uuid())

    # flow addressed to oneself, avoid sending a message through the transport
    # else user will receive the same message
    if not _is_message_addressed_to_oneself(fromUser, toUsers):
        _send_message_via_transport_layer(flow, fromUser, toUsers, atom)

    return flow


def _create_flow_in_db(protocolid, atom, toUsers, subject, flow_uuid=None):
    """
    Creates a new flow in db.
    """
    allUsers = toUsers
    if not atom.sender in allUsers:
        allUsers.append(atom.sender)
    flow = Flow(subject=subject, creator=atom.sender, assignee=toUsers[0], protocol=protocolid, users=','.join(allUsers))
    if flow_uuid:
        flow.uuid = flow_uuid
    if atom.state:
        # TODO: enable when get_valid_transition_commands is able to handle auto-transitions
        #if not state in protocols.get_valid_transition_commands(protocolid, None, None):
        #    raise Exception('Invalid command ' + state)
        flow.state = atom.state
    if atom.data:
        flow.data = atom.data
    # save the flow into client db
    flow.atoms = [atom]
    db_session.add(flow)
    db_session.commit()
    return flow


def _send_message_via_transport_layer(flow, sender, receivers, atom):
    """
    Sends message using any of the selected transport layers:
    native, email (SMTP), sms, xmpp.
    Currently supports native, email, xmpp.
    Currently supports one receiver only.
    """
    try:
        if sender.message_transport_type == 'email' and \
           receivers[0].message_transport_type == 'email':
            imapsmtp.send_flow_message(sender.email,
                                       sender.password,
                                       receivers[0].email,
                                       atom,
                                       flow)
        elif sender.message_transport_type == 'xmpp' and \
             receivers[0].message_transport_type == 'xmpp':
            xmpp.send_flow_message(receivers[0].name, atom, flow)
    except Exception:
        logging.exception('Error sending message via transport layer')


def _get_messages_via_transport_layer(flow, forUser):
    """
    Fetches flow messages from the selected transport layer.
    Currently supports native, IMAP only.
    """
    if forUser.message_transport_type == 'email':
        # fetch from imap transport
        try:
            imapsmtp.fetch_flow_from_imap(forUser.email, forUser.password, '', flow)
        except Exception:
            logging.exception('Error fetching messages from IMAP')
    else:
        # update from native db
        flow = getFlow(flow.id)


def message_received_handler(protocolid, flow_uuid, sender, receiver, state, data, body, crud_flag):
    """
    Callback func for handling asynchronous received messages (like from XMPP).
    Saves message to flow table.
    """
    logging.debug('message_receive_handler called')
    atom = Atom(sender=sender, state=state, data=data, body=body, crud_flag=crud_flag)
    flow = getFlowByUUID(flow_uuid)
    if flow:
        if state:
            flow.state = state
        if data:
            flow.data = data
        _update_flow_with_atom(flow, atom)
        db_session.commit()
        logging.info('received msg saved to flow : ' + flow.id)
    else:
        logging.debug('flow doesn\'t exist, creating a new one in db')
        _create_flow_in_db(protocolid, atom, [receiver], None, flow_uuid)


def newCompositeFlow(fromUser, toUsers, subject, body, state):
    """
    Creates a new composite Converse flow and sub-flows, one for each receiver.
    """
    fp = protocols.FlowProtocol.objects.get(name='Converse')
    flow = newFlow(fp.id, fromUser, [toUsers[0]], subject, body, state)
    flow.save()
    links = []
    for toUser in toUsers:
        subFlow = newFlow(fp.id, fromUser, [toUser], subject, body, state)
        link = Link(linkType='parent_of', flowid=subFlow.flowid)
        links.append(link)
    flow = linkFlows(flow.flowid, links)
    return flow
    

def updateFlow(flow, fromUser, body, state, crud_flag, data=None):
    """
    Updates flow with a new message, and sets state.
    """
    receivers = []
    for u in flow.users.split(','):
        if u != fromUser.name:
            receivers.append(client.get_user(u))
    if len(receivers) == 0:
        # sent to oneself?
        receivers = [fromUser]
    if state:
        # TODO: enable when get_valid_transition_commands is able to handle auto-transition commands
        #fp = flow.protocol
        #if not state in protocols.get_valid_transition_commands(fp.id, flow.state, None):
        #    raise Exception('Invalid command ' + state)
        flow.state = state
    if data:
        flow.data = data
    flow.assignee = receivers[0].name
    atom = Atom(sender=fromUser.name, body=body, state=state, data=data, crud_flag=crud_flag)

    if not _is_message_addressed_to_oneself(fromUser, receivers):
        _send_message_via_transport_layer(flow, fromUser, receivers, atom)

    _update_flow_with_atom(flow, atom)
    return flow


def _update_flow_with_atom(flow, atom):
    logging.info('updating flow ' + str(flow.id) + ' with atom, crud flag: ' + atom.crud_flag)
    if atom.crud_flag == AtomCRUDType.create:
        flow.atoms.append(atom)
    else:
        if len(flow.atoms) > 0 and \
           flow.atoms[-1].sender == atom.sender:
            if atom.crud_flag == AtomCRUDType.update:
                flow.atoms[-1] = atom
            elif atom.crud_flag == AtomCRUDType.delete:
                flow.atoms.pop()
                if len(flow.atoms) == 0:
                    db_session.delete(flow)
        else:
            logging.info('atom to be updated/deleted already replied to. adding a new atom')
            flow.atoms.append(atom)
    db_session.commit()


def linkFlows(flowid, links):
    """
    Links a flow to a set of other flow ids.
    """
    flow = Flow.objects.get(flowid=int(flowid))
    if flow.links: flow.links.append(links)
    else: flow.links = links
    flow.save()
    return flow


def forkFlow(flowid, protocolid, fromUser, toUsers, body, state):
    """
    Clones a flow and links to original flow.
    """
    origFlow = Flow.objects.get(flowid=int(flowid))
    if not state: state = origFlow.state
    f = newFlow(protocolid, fromUser, toUsers, origFlow.subject, body, state)
    link = Link(linkType='fork_of', flowid=str(origFlow.flowid))
    return linkFlows(f.flowid, [link])

'''
def refreshCompositeFlow(flowid):
    """
    Refreshes flow state based on linked (sub) flows.
    """
    flow = Flow.objects.get(flowid=int(flowid))
    if flow.links is None or len(flow.links) == 0:
        return flow
    n_completed = 0
    n_total = len(flow.links)
    for link in flow.links:
        subflow = Flow.objects.get(flowid=link.flowid)
        if not subflow.state in flow.protocol.action_commands:
            n_completed += 1
    if n_completed >= n_total:
        flow.blocked = False
        if flow.state == 'do':
            flow.progress = 100
            flow.state = 'done'
    else:
        flow.blocked = True
        if flow.state == 'do':
            flow.progress = int(float(n_completed)*100.0/float(n_total))
    flow.save()
    return flow
''' 

def deleteFlow(flowid, user):
    """
    Removes a user from a flow, and if no users reference the flow, permanently deletes it.
    """
    flow = getFlow(int(flowid))
    if flow:
        db_session.delete(flow)


def login(u, password):
    """
    Logs user into transport session.
    :rtype: boolean
    """
    logging.info('logging in user: transport ' +  str(u.message_transport_type)  + ', user: ' + u.name)
    if u.message_transport_type == 'email':
        authenticated = imapsmtp.login(u.email, password)
    elif u.message_transport_type == 'xmpp':
        authenticated = xmpp.login(u.name, u.name, message_received_handler)
    else:
        authenticated = (u.password == password)
    logging.info('authenticated: ' + str(authenticated))
    return authenticated


def logout(u):
    """
    Logs user out of transport session.
    """
    if u.message_transport_type == 'email':
        imapsmtp.logout()
    elif u.message_transport_type == 'xmpp':
        xmpp.logout()
    logging.info('logged out: ' + u.name)


def init_db_session():
    global db_session
    #logging.info('creating fresh flows.db')
    engine = create_engine('sqlite:///flows.db')
    Base.metadata.create_all(engine)
    db_session = scoped_session(sessionmaker(bind=engine))
    logging.info('inited db session')


def get_db_session():
    return db_session


def close_db_session():
    if db_session:
        db_session.close()
        logging.info('closed db session')


if __name__ == '__main__':
    init_db_session()
    flow = Flow()
    flow.atoms = [ Atom(body='hi there'), Atom(body='how are you') ]
    db_session().add(flow)
    db_session().commit()
    db_session.close()
