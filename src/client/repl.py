#!/usr/bin/python
"""
A REPL command line interface to the system.
Invoke as: python repl.py [user]
"""

import cmd
import sys
from cStringIO import StringIO
import argparse
import protocols
import flows
import users
import formatter
import readline
import time
import nonblockconsole
import pc
import getpass
import logging
import client
import re
import json
import os


class Repl(cmd.Cmd):
    """
    REPL class.
    """
    prompt = 'repl> '
    me = None
    my_contacts_user_list = None

    def login_user(self, user, passwd):
        if user != 'admin': 
            if not client.login_user(user, passwd):
                print 'Login failed. Invalid credentials?'
                return None
            self.me = client.get_user(user)
            if not self.me:
                print 'Unable to get user details from server.'
                return None
            if not flows.login(self.me, passwd):
                print 'Error logging in user to transport layer.'
                return None
        self.my_contacts_user_list = client.get_connected_users(None)
        self.prompt = user + '> '
        return user

    def logout_user(self):
        if self.me:
            flows.logout(self.me)

    def do_quit(self, line):
        return True

    def do_exit(self, line):
        return True

    def do_EOF(self, line):
        return True

    def do_users(self, line):
        for u in self.my_contacts_user_list:
            print u.to_json()

    def help_users(self):
        print 'list registered users'

    def do_ports(self, line):
        if not line:
            line = ''
        for p in client.get_ports_visible_to_user(self.me.name, line):
            print p.to_json()

    def help_ports(self):
        print 'usage: ports [search_keyword]'
        print 'if no keyword provided, list my flow ports.'
        print 'else, list anyone\'s ports that has a keyword match.'

    def do_find(self, line):
        port = None
        states = None
        from_user = None
        com = pc.SimpleCompleter()
        com.set_options([u.name for u in self.my_contacts_user_list])
        readline.set_completer(com.complete)
        d = raw_input('From: ')
        if d:
            from_user = d
        ports = client.get_ports_visible_to_user(self.me.name, '')
        com.set_options([p.name for p in ports])
        portname = raw_input('Port: ')
        if portname == '':
            portname = None
        else:
            for port in ports:
                if port.name == portname:
                    break
            if port:
                com.set_options([c for c in port.get_valid_transition_commands(None, None)])
        s = raw_input('State: ')
        if s != '':
            states = s.split(',')
        formatter.print_flows(self.me.name, flows.findFlows(self.me.name, from_user, None, port, states))

    def help_find(self):
        print 'usage: find'
        print 'list flows that i\'m involved in, with optional filtering by user/port/state.'

    def do_flows(self, line):
        formatter.print_flows(self.me.name, flows.findFlows(self.me.name, None, None, None, None))

    def help_flows(self):
        print 'usage: flows'
        print 'list all flows in my flows database.'

    def do_flowgraph(self, line):
        fromUser = None
        state = None
        args = line.split()
        if len(args) > 0:
            for arg in args:
                if arg.startswith('@'): fromUser = arg[1:] 
                elif arg.startswith('#'): state = arg[1:]

        # find flows that have no incoming edges
        allFlows = set([])
        sinks = set([])
        for f in flows.findFlows(self.me.name, fromUser, None, state):
            allFlows.add(int(f.flowid))
            if f.links:
                for l in f.links:
                    sinks.add(int(l.flowid))
        sources = allFlows - sinks

        # print them
        print formatter.print_header()
        for fid in sources: 
            f = flows.getFlow(flowid=fid)
            formatter.print_flow(self.me.name, f)
            if f.links:
                for l in f.links:
                    lf = flows.getFlow(flowid=l.flowid)
                    formatter.print_flow(self.me.name, lf)

    def do_flow(self, flowid):
        if not flowid:
            print 'Usage: flow flowid'
            return
        f = flows.getFlow(int(flowid))
        flows.syncFlow(f, self.me)
        formatter.print_flows(self.me.name, [f], True)

    def help_flow(self):
        print 'usage: flow flowid'
        print 'display all messages in a given flow.'

    def do_mkuser(self, line):
        if not line:
            print 'Usage: mkuser username email'
            return
        fields = line.split()
        if len(fields) != 2:
            print 'Usage: mkuser username email'
            return
        password = getpass.getpass()
        client.register_user(fields[0], password, fields[1], 'xmpp')

    def do_updateuser(self, line):
        if not line:
            print 'Usage: updateuser email'
            return
        new_password = getpass.getpass()
        self.me.email = line
        # TODO: save in client?
        client.update_user(self.me.name, self.me.password, new_password, self.me.email, self.me.message_transport_type)
        self.me.password = new_password

    def help_setpass(self):
        print 'set my password (this password is used to authenticate against external mail accounts).'

    def help_mkuser(self):
        print 'usage: mkuser username'
        print 'create a new user.'

    '''
    def get_cmd_from_user(self, flow, protocol, command, completer):
        """
        :type protocol: FlowProtocol
        Gets command and associated input from user.
        Fires command in a loop, which informs what input it needs, and when it has sufficient input.
        """
        input_data = None
        input_trail = []
        while True:
            completer.set_options([name for name in protocol.get_valid_transition_commands(command, None)])
            command = raw_input('Command (tab for options): ')
            if command == '':
                command = None
                break
            while True:
                print 'Firing command ' + command + '...'
                input_choices, fire_result = client.fire_command(protocol, command, input_data)
                if fire_result == protocols.CommandFireResult.FireError:
                    print 'Error firing command : ' + str(input_choices)
                    return None, None
                elif fire_result == protocols.CommandFireResult.FireAgainWithAnyInput:
                    input_prompt = 'Input (enter text): '
                elif fire_result == protocols.CommandFireResult.FireAgainWithProvidedInput:
                    input_prompt = 'Input (tab for options): '
                elif fire_result == protocols.CommandFireResult.FireDone and input_choices:
                    input_prompt = 'Input (tab for options): '
                elif fire_result == protocols.CommandFireResult.FireAgainWithCachedInput:
                    if flow and flow.data:
                        input_choices = flow.data
                        input_prompt = 'Input (tab for options): '
                    else:
                        print 'No cached flow data present. Command needs it!'
                        return
                else:
                    break
                if input_choices:
                    completer.set_options([v for v in utils.flatten_object_to_list(input_choices)])
                else:
                    completer.set_options([])
                input_data = raw_input(input_prompt)
                input_trail.append(input_data)
                if fire_result == protocols.CommandFireResult.FireDone:
                    break
            completer.set_options([])
            if not protocol.is_auto_transition_command(command):
                break

        return command, input_trail
    '''

    def do_send(self, line):
        com = pc.ProgressiveCompleter()
        com.set_options([u.name for u in self.my_contacts_user_list])
        readline.set_completer(com.complete)
        print 'Enter message metadata. Use tab-complete till no more expansion available.'
        line = raw_input('Meta: ')
        subject = com.last_cmd
        if com.last_input:
            subject += ':' + com.last_input
        if not com.protocol.is_messageable_command(com.last_cmd):
            print 'Non message-able command selected. No message sent.'
            return
        print('Message text (. to abandon, empty line to finish):'),
        body = '\n'.join(iter(raw_input, ''))
        if body == '.':
            print('Abandoning message.')
            return
        toUsers = [u for u in self.my_contacts_user_list if u.name == com.user]
        f = flows.newFlow(com.protocol.id, self.me, toUsers, subject, body, com.last_cmd, com.last_input)
        formatter.print_flows(self.me.name, [f], True)

    def help_send(self):
        print 'usage: send'
        print 'send a message to myself or another user in a new flow (communication thread).'

    def do_del(self, line):
        args = line.split()
        if len(args) < 1:
            print 'Usage: del flowid'
            return
        flows.deleteFlow(args[0], self.me.name)

    def help_del(self):
        print 'usage: del flowid'
        print 'delete a flow permanently.'

    def do_mkport(self, line):
        args = line.split()
        if len(args) < 1:
            print 'Usage: mkport portname'
            return
        newPortName = args[0]
        portCmds = raw_input('Cmds (cmd1:[cmd2,cmd3],cmd4,...): ')
        portData = raw_input('Data (cmd1:[data1,data2],cmd2:data4,...): ')
        commandGraph = json.loads('{' + re.sub(r'([a-zA-Z0-9_]+)', r'"\1"', portCmds) + '}')
        commandData = json.loads('{' + re.sub(r'([a-zA-Z\$\.\-0-9_]+)', r'"\1"', portData) + '}')
        port = protocols.FlowProtocol(name=newPortName,
                            creator=self.me.name,
                            command_transitions=commandGraph,
                            command_processors=commandData)
        client.new_port(port)

    def help_mkport(self):
        print 'usage: mkport portname'
        print 'create a port and expose to other users. provide commands (verbs) and data (nouns).'
        print ''
        print 'example port to advertise a classifieds ad:'
        print 'mkport Nice_Patio_Furniture'
        print '   Cmds: select:[MakeOffer],MakeOffer:[AcceptOffer,RejectOffer],AcceptOffer:[Pay],Pay:[Shipped]'
        print '   Data: select:[Patio_Chairs,Patio_Table]'
        print ''
        print 'example port to advertise a sports interest:'
        print 'mkport Tennis_Games:'
        print '   Cmds: Info:[],Just_Hit:[Accept,Later,Reject],Play_Match:[Accept,Later,Reject]'
        print '   Data: Info:{Level:4.5,Age:31,Sex:Female}'
        print ''
        print '\'select\' and \'Info\' are special verbs. \'select\' is used to select a data option and automatically ' \
              'transition to the next command. \'Info\' is used to present informational attributes about the port.'


    def do_reply(self, line):
        args = line.split()
        if len(args) < 1:
            print "Usage: reply flowid [c|u|d]"
            return
        f = flows.getFlow(args[0])
        crud_flag = flows.AtomCRUDType.create
        if len(args) > 1:
            if args[1] == 'u':
                crud_flag = flows.AtomCRUDType.update
            elif args[1] == 'd':
                crud_flag = flows.AtomCRUDType.delete
                flows.updateFlow(f, self.me, None, None, crud_flag, None)
                return

        flows.syncFlow(f, self.me)
        port = client.get_port(f.protocol)
        if not port:
            print 'Unknown protocol in flow : ' + str(f.protocol)
            return
        com = pc.ProgressiveCompleter()
        com.reply_mode = True
        com.set_options([c for c in port.get_valid_transition_commands(f.state, None)])
        com.protocol = port
        com.last_cmd = f.state
        com.user = f.creator
        com.last_input = f.data
        readline.set_completer(com.complete)
        print 'Enter message metadata. Use tab-complete till no more expansion available.'
        line = raw_input('Meta: ')
        print('Message text (. to abandon, empty line to finish):'),
        body = '\n'.join(iter(raw_input, ''))
        if body == '.':
            print('Abandoning message.')
            return
        flows.updateFlow(f, self.me, body, com.last_cmd, crud_flag, com.last_input)
        formatter.print_flows(self.me.name, [f], True)

    def do_watch(self, line):
        args = line.split()
        if len(args) < 1:
            print "Usage: watch flowid"
            return
        int_messages_in_flow = 0
        f = flows.getFlow(args[0])
        if not f:
            print 'Error: no flow with id ' + args[0]
            return
        port = client.get_port(f.protocol)
        com = pc.ProgressiveCompleter()
        com.reply_mode = True
        com.set_options([c for c in port.get_valid_transition_commands(f.state, None)])
        com.protocol = port
        com.last_cmd = f.state
        com.user = f.creator
        readline.set_completer(com.complete)
        while True:
            with nonblockconsole.NonBlockingConsole() as nbc:
                while True:
                    key = nbc.get_data()
                    if  key == '\x1b':  # x1b is ESC
                        return
                    elif key == '\x73': # x73 is s
                        break
                    flows.refreshFlow(f)
                    if len(f.atoms) > int_messages_in_flow:
                        for atom in f.atoms[int_messages_in_flow:]:
                            formatter.print_atom(atom, self.me.name)
                        print 'Listening for new messages [press esc to cancel, s to send new msg]...'
                    int_messages_in_flow = len(f.atoms)
                    time.sleep(2)
                print 'Enter message metadata. Use tab-complete till no more expansion available.'
                line = raw_input('Meta: ')
                if com.protocol.is_messageable_command(com.last_cmd):
                    print('Enter message text (. to abandon, empty line to finish):'),
                    body = '\n'.join(iter(raw_input, ''))
                    if body == '.':
                        print('Abandoning message.')
                        print 'Listening for new messages [press esc to cancel, s to send new msg]...'
                    else:
                        flows.updateFlow(f, self.me, body, com.last_cmd, com.last_input, flows.AtomCRUDType.create)

    def help_reply(self):
        print "usage: reply flowid [u|d]"
        print 'send a message in an existing flow.'
        print 'use the \'u\' flag to amend your last message, and the \'d\' flag to delete your last message. ' \
              'the u and d flags work only if your message is the last one in the flow.'

    def do_link(self, line):
        args = line.split()
        if len(args) < 3:
            print "Usage: link src_flow_id link_type target_flow_id"
            return
        link = flows.Link(linkType=args[1], flowid=int(args[2]))
        f = flows.linkFlows(args[0], [link])
        formatter.print_flows(self.me.name, [f], True)

    def do_waiting(self, line):
        my_created_flows = flows.findFlows(self.me.name, self.me.name, None, None, None)
        formatter.print_header()
        for f in my_created_flows:
            port = client.get_port(f.protocol)
            if f.state in port.get_action_commands():
                formatter.print_flow(self.me.name, f)

    def help_waiting(self):
        print 'usage: waiting'
        print 'list all flows that are waiting on some action from others.'

    def do_todo(self, line):
        my_assigned_flows = flows.findFlows(self.me.name, None, self.me.name, None, None)
        formatter.print_header()
        for f in my_assigned_flows:
            port = client.get_port(f.protocol)
            if f.state in port.get_action_commands():
                formatter.print_flow(self.me.name, f)

    def help_todo(self):
        print 'usage: todo'
        print 'list all flows that are waiting for some action from me.'

    def emptyline(self):
        pass

## END OF CLASS


## function to wrap the Repl and process one command
## the repl's stdin and stdout are harvested via strings
def run_cmd(user, command, body):
    old_stdout = sys.stdout
    old_stdin = sys.stdin
    sys.stdout = mystdout = StringIO()
    sys.stdin = mystdin = StringIO()
    if not body:
        body = ''
    mystdin.truncate(0)
    mystdin.write(body + '\n')
    mystdin.seek(0)
    repl = Repl()
    formatter.format_json = True
    u = repl.login_user(user, user)
    if u:
        repl.onecmd(command)
        repl.logout_user()
    sys.stdout = old_stdout
    sys.stdin = old_stdin
    return mystdout.getvalue()


###-------main---------###

if __name__ == '__main__':
    logging.basicConfig(filename='repl.log', level=logging.DEBUG, format='%(levelname)s %(funcName)s: %(message)s')

    if 'libedit' in readline.__doc__:
        readline.parse_and_bind("bind ^I rl_complete")
    else:
        readline.parse_and_bind("tab: complete")

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', help='server to connect to', default='http://ec2-54-186-9-137.us-west-2.compute.amazonaws.com:5000')
    parser.add_argument('-j', '--json', help='output in json format', action='store_true')
    args = parser.parse_args()
    if args.json:
        formatter.format_json = True

    print 'Welcome to the \033[91mAutoMEton\033[0m command shell. From this shell, you can:\n' \
          '* initiate new conversational flows on ports (\'send\')\n' \
          '* reply to them (\'reply\')\n' \
          '* chat (\'watch\')\n' \
          '* discover ports (\'ports\')\n' \
          '* search through your flows by port context and current state (\'find\')\n' \
          '* expose ports (\'mkport\') etc.\n' \
          'Please use the help command to see detailed help about each operation.'
    print '----'
    print 'Using port server: ' + args.server
    print '----'
    client.set_host(args.server)

    if os.path.exists('auth.json'):
        try:
            f = open('auth.json', 'r')
            cred_dict = json.load(f)
            user = cred_dict['user']
            passwd = cred_dict['password']
            print 'Using credentials from auth.json'
            f.close()
        except:
            print 'Create auth.json in following format: {\"user\":abc@email.com, \"password": password_or_token}'
            sys.exit(1)
    else:        
        user = raw_input('User: ')
        passwd = getpass.getpass()
    
    myRepl = Repl()
    try:
        flows.init_db_session()
        if myRepl.login_user(user, passwd):
            myRepl.cmdloop()
    except Exception as e:
        logging.exception('Error from login or command loop')
        print 'Error in login or command processing:'
        print str(e)
    finally:
        myRepl.logout_user()
        flows.close_db_session()
