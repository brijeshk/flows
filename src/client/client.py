"""
Client wrapper to interact with REST web-service.
Flows are cached in a SQLite3 database on client.
"""

import requests
import logging
import protocols
import users
import json

HOST = 'http://localhost:5000'


def set_host(host):
    global HOST
    HOST = host

def login_user(username, token):
    r = requests.get(HOST + '/users/login',
                     auth=(username, token))
    return (r.status_code == 200)

def register_user(username, password, email, transport):
    d_user = { 'username' : username, 'email': email, 'transport' : transport, 'password' : password }
    requests.post(HOST + '/users', 
                  data=json.dumps(d_user),
                  headers={'Content-type': 'application/json'})

def update_user(username, old_password, new_password, email, transport):
    d_user = { 'username' : username, 'email': email, 'transport' : transport, 'password' : new_password }
    requests.put(HOST + '/users', 
                 data=json.dumps(d_user),
                 auth=(username, old_password),
                 headers={'Content-type': 'application/json'})

def get_ports_exposed_by_user(username):
    r = requests.get(HOST + '/ports/user/' + username)
    logging.debug(r.json())
    return [protocols.FlowProtocol.from_json(json.dumps(jp)) for jp in r.json()]

def get_ports_visible_to_user(username, keyword):
    r = requests.get(HOST + '/ports/visible/' + username, params={'q':keyword})
    logging.debug(r.json())
    return [protocols.FlowProtocol.from_json(json.dumps(jp)) for jp in r.json()]

def get_port(pid):
    r = requests.get(HOST + '/ports/id/' + str(pid))
    logging.debug(r.json())
    return protocols.FlowProtocol.from_json(json.dumps(r.json()))

'''
def get_valid_commands(port, state):
    if state:
        params = {'cur': state}
    else:
        params = None
    r = requests.get(HOST + '/ports/{0}/valid_cmds'.format(port.id), params=params)
    logging.debug(r.json())
    return r.json()

def get_action_commands(port):
    if port:
        params = { 'pid': port.id }
    else:
        params = None
    r = requests.get(HOST + '/ports/action_cmds', params=params)
    logging.debug(r.json())
    return r.json()
'''

def fire_command(port, cmd, data):
    params = { 'cmd' : cmd, 'data' : data }
    r = requests.get(HOST + '/ports/id/{0}/fire_cmd'.format(port.id), params=params)
    j = r.json()
    logging.debug(j)
    return j[0], j[1]

def get_connected_users(user):
    r = requests.get(HOST + '/users')
    logging.debug(r.json())
    return [users.User.from_json(json.dumps(u)) for u in r.json()]

def get_user(username):
    r = requests.get(HOST + '/users/id/' + username)
    logging.debug(r.json())
    return users.User.from_json(json.dumps(r.json()))

def new_port(port):
    portDict = {}
    portDict['creator'] = port.creator
    portDict['command_transitions'] = port.command_transitions
    portDict['command_processors'] = port.command_processors
    portDict['name'] = port.name
    requests.post(HOST + '/ports', 
                  data=json.dumps(portDict),
                  headers={'Content-type': 'application/json'})


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(levelname)s %(funcName)s: %(message)s')
    get_connected_users('brijesh')
    ports = get_ports_exposed_by_user('ramya')
    fire_command(ports[3], 'Join', None)

