"""
Tests for Model API.
"""

import flows
import formatter
import client
import protocols
import unittest


projectPid = None

class ApiTests(unittest.TestCase):
    """
    Unit-tests for Flows.
    """

    @classmethod
    def setUpClass(cls):
        global projectPid, u1, u2
        flows.init_db_session()
        u1 = client.get_user('brijesh')
        u2 = client.get_user('ramya')
        plist = client.get_ports_for_user('brijesh')
        for p in plist:
            if p.name == 'Converse':
                projectPid = p.id
                break

    @classmethod
    def tearDownClass(cls):
        flows.close_db_session()

    def test_send_and_reply(self):
        f = flows.newFlow(projectPid, u1, [u2], 'test1', 'please send me a reply', 'Ask')
        f = flows.updateFlow(f, u2, 'this is the reply', 'Answer', 'c')
        formatter.print_flows(None, [f], True)
        assert f.state == 'Answer'

    def test_do_and_done(self):
        f = flows.newFlow(projectPid, u1, [u2], 'task1', 'this is a new task', 'Do')
        fs = flows.findFlows(u2.name, None, None, None, ['Do'])
        assert len(fs) == 1
        f = flows.updateFlow(f, u2, 'task is done', 'Done', 'c')
        fs = flows.findFlows(u2.name, None, u2.name, None, ['Do'])
        formatter.print_flows(None, [f], True)
        assert len(fs) == 0


if __name__ == '__main__':
    unittest.main()
