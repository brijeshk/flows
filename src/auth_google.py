from flask import Blueprint, redirect, url_for, session, Response, request, current_app
from flask_oauth import OAuth
import json
from functools import wraps
from flask import request, Response
import users
import logging

# client id and secret for AutoMeton application
# configured at https://code.google.com/apis/console
GOOGLE_CLIENT_ID = '159202924793-ghl8aejej9jihibrk88rcsko5t23o24a.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = 'NV6AHpM6onX-5HuMvupCmlB7'

mod_auth = Blueprint('auth', __name__, url_prefix='/auth')
oauth = OAuth()

google = oauth.remote_app('google',
                          base_url='https://www.google.com/accounts/',
                          authorize_url='https://accounts.google.com/o/oauth2/auth',
                          request_token_url=None,
                          request_token_params={'scope': 'https://www.googleapis.com/auth/userinfo.email',
                                                'response_type': 'code'},
                          access_token_url='https://accounts.google.com/o/oauth2/token',
                          access_token_method='POST',
                          access_token_params={'grant_type': 'authorization_code'},
                          consumer_key=GOOGLE_CLIENT_ID,
                          consumer_secret=GOOGLE_CLIENT_SECRET)


def check_credentials(username, password):
    current_app.logger.info('checking credentials: ' + username + ' ' + password)
    aUser = users.get_user(username)
    if not aUser:
        return {'status':'User doesn\'t exist'}, False
    if aUser.password != password:
        return {'status':'Password/Token invalid'}, False
    else:
        return {'status':'Verified'}, True

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth:
            return Response('No login credentials supplied', 401, {'WWW-Authenticate': 'Basic realm="Login Required"'})
        else:
            status,ok = check_credentials(auth.username, auth.password)
            if not ok:
                return Response(json.dumps(status), 403, mimetype='application/json')
        return f(*args, **kwargs)
    return decorated

def get_user_info_from_identity_provider(access_token):
    from urllib2 import Request, urlopen, URLError
    headers = {'Authorization': 'OAuth '+access_token}
    req = Request('https://www.googleapis.com/oauth2/v1/userinfo',
                  None, headers)
    try:
        res = urlopen(req)
    except URLError, e:
        if e.code == 401:
            # Unauthorized - bad token
            session.pop('access_token', None)
            return redirect(url_for('auth.login'))
        return res.read()
    return res.read()


@mod_auth.route('/check')
@requires_auth
def verify_auth():
    result = {'status':'verified'}
    return Response(json.dumps(result),
                    status=200,
                    mimetype='application/json')


@mod_auth.route('/login')
def login():
    user_name = request.args.get('u', None)
    if not user_name:
        return Response("Provide email via u parameter", status=401)

    if not '@gmail.' in user_name:
        return Response("Email provider not supported", status=401)

    session['user_name'] = user_name
    callback = url_for('auth.authorized', _external=True)
    if '@gmail.' in user_name:
        return google.authorize(callback=callback)


@mod_auth.route('/authorized')
@google.authorized_handler
def authorized(resp):
    access_token = resp['access_token']
    session['access_token'] = access_token, ''
    # for which user?
    user = session.get('user_name', '')
    # save token into user db
    try:
        users.update_user(user, access_token)
    except Exception as e:
        logging.exception('Error updating user')
        logging.info('Trying to create new user')
        u = users.new_user(user, email=user, password=access_token, transport='xmpp')

    result = { 'auth_token' : access_token, 'user' : user }
    return Response(json.dumps(result),
                    status=200,
                    mimetype='application/json')


@google.tokengetter
def get_access_token():
    return session.get('access_token')
