

def flatten_object_to_list(nested, prefix=None):
    if isinstance(nested, list):
        if prefix:
            return [(prefix + '.' + i) for i in nested]
        else:
            return nested
    elif isinstance(nested, dict):
        flat = []
        for k in nested.keys():
            if prefix:
                kprefix = prefix + '.' + k
            else:
                kprefix = k
            flat += flatten_object_to_list(nested[k], kprefix)
        return flat
    else:
        if prefix:
            return [prefix + '.' + nested]
        else:
            return [nested]

if __name__ == '__main__':
    print flatten_object_to_list({'a':['b','c'], 'd':['e','f'], 'g':{'h':'r'}})
    print flatten_object_to_list({'a':'b'})
    print flatten_object_to_list('a')
    print flatten_object_to_list(['London,UK','SanFrancisco,US','Beijing,China'])