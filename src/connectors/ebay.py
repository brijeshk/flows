from ebaysdk.finding import Connection
import sys
import pprint
import protocols


def find_items(search_term):
    if not search_term:
        return None, protocols.CommandFireResult.FireAgainWithAnyInput
    else:
        try:
            api = Connection(appid='BrijeshK-6bb3-4740-8172-bf62900fd7d8')
            api.execute('findItemsAdvanced',
                        {'keywords': search_term,
                         'itemFilter': [
                                            {'name': 'Condition',
                                             'value': 'Used'},
                                            {'name': 'LocatedIn',
                                             'value': 'US'}
                                       ],
                         'paginationInput' : {'entriesPerPage' : 10, 'pageNumber' : 1}
                        }
            )
            items = api.response_dict()
            result = []
            for item in items['searchResult']['item']:
                itemstr = item['title']['value'] + ", $" + item['sellingStatus']['currentPrice']['value']
                result.append(itemstr)
            return result, protocols.CommandFireResult.FireDone
        except Exception as e:
            print str(e)
            return str(e), protocols.CommandFireResult.FireError


def buy_item(item):
    if not item:
        return None, protocols.CommandFireResult.FireAgainWithCachedInput
    else:
        #print 'Buying item...'
        return 'bought ' + item, protocols.CommandFireResult.FireDone


if __name__ == '__main__':
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(find_items(sys.argv[1]))
