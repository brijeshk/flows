import requests
import protocols


def get_weather_forecast(city):
    if not city:
        return None

    r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=' + city)
    if r.status_code == 200:
        j = r.json()
        return j['weather'][0]['description']
    else:
        raise Exception('Error from API')
