import requests
import protocols
import re

movieRePattern = re.compile('>(\w+)\s*</a>')

def getMoviesNearZipCode(zipcode):
    """
    Gets theater listings near zip code.
    """
    if not zipcode:
        # provide options
        return None, protocols.CommandFireResult.FireAgainWithAnyInput
    else:
        # process input
        r = requests.get('http://pipes.yahoo.com/pipes/pipe.run?ZipCode={0}&_id=Gle_P_Hc3RGo3H61LXO0Kg&_render=json'.format(zipcode))
        if r.status_code == 200:
            movieInTheaterList = []
            theatersDictList = r.json()['value']['items']
            for theaterDict in theatersDictList:
                movieList = movieRePattern.findall(theaterDict['description'])
                for movie in movieList:
                    movieInTheaterList.append(theaterDict['title'] + '.' + movie)
            return movieInTheaterList, protocols.CommandFireResult.FireDone
        else:
            return str(r.status_code), protocols.CommandFireResult.FireError

if __name__ == '__main__':
    movieList, _ = getMoviesNearZipCode(94404)
    print movieList
