"""
Flows module.
Defines flow-related functions, including retrieving/creating/searching flows.
"""

from mongoengine import *
import protocols
import datetime
import imapsmtp
import xmpp
import users
import logging


class Atom(EmbeddedDocument):
    """
    Model class for an atomic unmodifiable message.
    """
    sender = StringField()
    body = StringField()
    state = StringField()
    data = StringField()
    id = StringField()
    created = DateTimeField(default=datetime.datetime.now)
    #meta = {'ordering': ['created']}


class Link(EmbeddedDocument):
    """
    Model class for a link between one flow and another.
    """
    linkType = StringField(choices=protocols.LINKTYPES)
    flowid = IntField()


class Flow(Document):
    """
    Model class for a flow. A flow models a list of messages, a protocol, a current state.
    """
    flowid = SequenceField(primary_key=True)
    protocol = ReferenceField(protocols.FlowProtocol)
    users = ListField(StringField())
    creator = StringField()
    assignee = StringField()
    subject = StringField()
    context = StringField()
    blocked = BooleanField(default=False)
    progress = IntField()
    state = StringField()
    data = StringField()
    links = ListField(EmbeddedDocumentField(Link))
    atoms = ListField(EmbeddedDocumentField(Atom))
    meta = {'ordering': ['flowid']}


def getFlow(flowid):
    """
    Returns a flow by id.
    :type flowid: str or int
    :rtype: Flow
    """
    return Flow.objects.get(flowid=int(flowid))


def syncFlow(f, forUser):
    """
    Syncs flow by fetching latest messages from transport layer.
    """
    _get_messages_via_transport_layer(f, forUser)


def findFlows(any_user, from_user, assignee, port, states):
    """
    Finds flows by user, assignee, state.
    :type any_user: str
    :type from_user: str or None
    :type assignee: str or None
    :type port: str or None
    :type states: list[str] or None
    :rtype: list[Flow]
    """
    q = Q(users=any_user)
    if from_user: q = q & Q(atoms__sender=from_user)
    if assignee: q = q & Q(assignee=assignee)
    if states: q = q & Q(state__in=states)
    if port:
        pids = [p.id for p in protocols.find_protocols_by_name(port)]
        q = q & Q(protocol__in=pids)
    return Flow.objects(q)


def newFlow(protocolid, fromUser, toUsers, subject, body, state, data=None):
    """
    Creates a new flow.
    """
    fp = protocols.find_protocol_by_id(protocolid)
    if fromUser not in toUsers:
        allUsers = [fromUser] + toUsers
    else:
        allUsers = toUsers
    flow = Flow(subject=subject, creator=fromUser, users=allUsers, protocol=fp)
    flow.atoms = []
    atom = Atom(sender=fromUser, body=body, state=state, data=data)
    if state:
        # TODO: enable when get_valid_transition_commands is able to handle auto-transitions
        #if not state in protocols.get_valid_transition_commands(protocolid, None, None):
        #    raise Exception('Invalid command ' + state)
        flow.state = state
        flow.assignee = toUsers[0]   ## TODO: first assignee?
    _send_message_via_transport_layer(flow, fromUser, toUsers, atom)
    return flow


def _connect_to_transport_layer(username):
    user = users.get_user(username)
    if user.message_transport_type == 'xmpp':
        xmpp.login(user.name, user.password)
    elif user.message_transport_type == 'email':
        imapsmtp.login(user.email, user.password)


def _send_message_via_transport_layer(flow, sender, receivers, atom):
    """
    Sends message using any of the selected transport layers:
    native, email (SMTP), sms, xmpp.
    Currently supports native, email, xmpp.
    Currently supports one receiver only.
    """
    senderU = users.get_user(sender)
    receiverU = users.get_user(receivers[0])
    try:
        if senderU.message_transport_type == 'email' and \
           receiverU.message_transport_type == 'email':
            imapsmtp.send_flow_message(senderU.email,
                                       senderU.password,
                                       receiverU.email,
                                       atom,
                                       flow)
        elif senderU.message_transport_type == 'xmpp' and \
             receiverU.message_transport_type == 'xmpp':
            xmpp.send_flow_message(receiverU.email, atom, flow)
        # save the atom into flow on client side
        flow.atoms.append(atom)
        flow.save()
    except Exception:
        logging.exception('Error sending message via transport layer')


def _get_messages_via_transport_layer(flow, forUser):
    """
    Fetches flow messages from the selected transport layer.
    Currently supports native, IMAP only.
    """
    forUserU = users.get_user(forUser)
    if forUserU.message_transport_type == 'email':
        # fetch from imap transport
        try:
            imapsmtp.fetch_flow_from_imap(forUserU.email, forUserU.password, '', flow)
        except Exception:
            logging.exception('Error fetching messages from IMAP')
    else:
        # update from native db
        flow = getFlow(flow.id)


def newCompositeFlow(fromUser, toUsers, subject, body, state):
    """
    Creates a new composite Converse flow and sub-flows, one for each receiver.
    """
    fp = protocols.FlowProtocol.objects.get(name='Converse')
    flow = newFlow(fp.id, fromUser, [toUsers[0]], subject, body, state)
    flow.save()
    links = []
    for toUser in toUsers:
        subFlow = newFlow(fp.id, fromUser, [toUser], subject, body, state)
        link = Link(linkType='parent_of', flowid=subFlow.flowid)
        links.append(link)
    flow = linkFlows(flow.flowid, links)
    return flow
    

def updateFlow(flow, fromUser, body, state, data=None):
    """
    Updates flow with a new message, and sets state.
    """
    receivers = [x for x in flow.users if not x == fromUser]
    if len(receivers) == 0:
        receivers = [fromUser]
    if state:
        # TODO: enable when get_valid_transition_commands is able to handle auto-transition commands
        #fp = flow.protocol
        #if not state in protocols.get_valid_transition_commands(fp.id, flow.state, None):
        #    raise Exception('Invalid command ' + state)
        flow.state = state
    if data:
        flow.data = data
    atom = Atom(sender=fromUser, body=body, state=state, data=data)
    _send_message_via_transport_layer(flow, fromUser, receivers, atom)
    return flow


def linkFlows(flowid, links):
    """
    Links a flow to a set of other flow ids.
    """
    flow = Flow.objects.get(flowid=int(flowid))
    if flow.links: flow.links.append(links)
    else: flow.links = links
    flow.save()
    return flow


def forkFlow(flowid, protocolid, fromUser, toUsers, body, state):
    """
    Clones a flow and links to original flow.
    """
    origFlow = Flow.objects.get(flowid=int(flowid))
    if not state: state = origFlow.state
    f = newFlow(protocolid, fromUser, toUsers, origFlow.subject, body, state)
    link = Link(linkType='fork_of', flowid=str(origFlow.flowid))
    return linkFlows(f.flowid, [link])

'''
def refreshCompositeFlow(flowid):
    """
    Refreshes flow state based on linked (sub) flows.
    """
    flow = Flow.objects.get(flowid=int(flowid))
    if flow.links is None or len(flow.links) == 0:
        return flow
    n_completed = 0
    n_total = len(flow.links)
    for link in flow.links:
        subflow = Flow.objects.get(flowid=link.flowid)
        if not subflow.state in flow.protocol.action_commands:
            n_completed += 1
    if n_completed >= n_total:
        flow.blocked = False
        if flow.state == 'do':
            flow.progress = 100
            flow.state = 'done'
    else:
        flow.blocked = True
        if flow.state == 'do':
            flow.progress = int(float(n_completed)*100.0/float(n_total))
    flow.save()
    return flow
''' 

def deleteFlow(flowid, user):
    """
    Removes a user from a flow, and if no users reference the flow, permanently deletes it.
    """
    flow = Flow.objects.get(flowid=int(flowid))
    if user in flow.users:
        flow.users.remove(user)
        flow.save()
    if flow.users == []: flow.delete()
