"""
Tests for server.
"""

import server
import unittest
import serverdb
import json
import users
import clubs
import protocols


class ServiceTests(unittest.TestCase):
    """
    Unit tests for web-services.
    """
    default_protocols = None

    @classmethod
    def setUpClass(cls):
        serverdb.init_db('flows', 'localhost')
        serverdb.clear_db()
        protocols.create_default_protocols()
        ServiceTests.default_protocols = protocols.get_default_protocols()
        ServiceTests.make_users()

    @classmethod
    def tearDownClass(cls):
        #db.clear_db()
        pass

    def setUp(self):
        self.app = server.app.test_client()

    @classmethod
    def make_users(cls):
        users.new_user('ramya')
        users.new_user('brijesh')
        users.new_user('manasi')
        users.new_user('nikhil')
        users.new_user('prateek')
        users.new_user('chantal')
        users.new_user('amitabh')
        users.new_user('ebay')
        users.new_user('movies')
        users.new_user('weather')
        clubs.new_club('parents', 'ramya', ['brijesh', 'ramya'], False)
        clubs.new_club('children', 'nikhil', ['nikhil', 'manasi'], False)
        clubs.new_club('friends', 'prateek', ['brijesh', 'prateek', 'chantal'], False)
        clubs.new_club('russian authors book club', 'prateek', [], True)
        print clubs.get_clubs_by_user('brijesh')

    def test_default_port(self):
        """
        Tests completion of protocol command based on current state and valid transitions.
        """
        print 'test_default_port'
        # test a valid command completion for a default persona
        for p in self.default_protocols:
            if p.name == 'Converse': break
        rv = self.app.get('/ports/{0}/valid_cmds?cur=Ask'.format(str(p.id)))
        print rv.data
        jout = json.loads(rv.data)
        assert jout == ['Answer','Cancel']
        # test the default personas for any user
        rv = self.app.get('/ports/user/brijesh')
        plist = json.loads(rv.data)
        assert plist[0]['name'] == 'Converse'
        # test retrieval of a persona by id
        rv = self.app.get('/ports/id/' + str(self.default_protocols[0].id))
        p = json.loads(rv.data)
        assert p['name'] == self.default_protocols[0].name

    def test_crud_port(self):
        """
        Tests creation and retrieval of ports by user and id.
        """
        print 'test_crud_port'
        command_processors = { 'Join': { 'Carnatic': ['Group','Private'], 'Hindustani': ['Group'] },
                               'Accept': { 'Times': ['Thursdays', 'Saturdays'] } }
        command_transitions = {'Join':['Accept','Decline']}
        port = dict(name='Music-Lessons',
                    creator='ramya',
                    command_transitions=command_transitions,
                    command_processors=command_processors,
                    visible_to=['brijesh','nikhil','manasi'])
        rv = self.app.post('/ports', 
                           data=json.dumps(port),
                           content_type='application/json')
        print rv.data
        p = json.loads(rv.data)
        # test one of its valid command completions
        rv = self.app.get('/ports/{0}/valid_cmds?cur=Join'.format(str(p['_id'])))
        print rv.data
        jout = json.loads(rv.data)
        assert jout == ['Accept','Decline']
        rv = self.app.get('/ports/{0}/fire_cmd?cmd=Join&data=Carnatic'.format(str(p['_id'])))
        j = json.loads(rv.data)

    def test_dynamic_port(self):
        """
        Tests usage of a dynamic weather data service.
        """
        print 'test_dynamic_port'
        for p in protocols.find_protocols_by_creator('weather'):
            if p.name == 'Hows-the-Weather': break
        rv = self.app.get('/ports/{0}/fire_cmd?cmd=Get'.format(str(p.id)))
        print rv.data
        j = json.loads(rv.data)
        assert "London,UK" in j[0]
        rv = self.app.get('/ports/{0}/fire_cmd?cmd=Get&data=London,UK'.format(str(p.id)))
        print rv.data

    def test_3way_converse(self):
        print 'test_3way_converse'
        command_processors = { 'select': { 'Personal_Tax':['Joint','Single'],
                                           'Business_Tax':['SmallBusiness'] },
                               'Schedule': 'calendar.schedule'}
        command_transitions = {"select":["Inquire","Schedule"],
                               "Schedule":["Accept","Decline"],
                               "Inquire":["Answer"]}
        port = dict(name='Tax-Services',
                    creator='prateek',
                    command_transitions=command_transitions,
                    command_processors=command_processors,
                    visible_to=[])
        rv = self.app.post('/ports', data=json.dumps(port), content_type='application/json')
        print rv.data

    def test_classifieds(self):
        print 'test_classifieds'
        command_processors = { 'select': 'Honda_PCX',
                               'Info' : { 'Honda_PCX' : { 'price' : '$1675', 'year' : '2007' } } }
        command_transitions = {'select':['Inquire','Offer','Info'],
                               'Offer':['Accept','Decline'],
                               'Inquire':['Answer']}
        port = dict(name='Classifieds',
                    creator='nikhil',
                    command_transitions=command_transitions,
                    command_processors=command_processors,
                    visible_to=[])
        rv = self.app.post('/ports', data=json.dumps(port), content_type='application/json')
        print rv.data


if __name__ == '__main__':
    unittest.main()
