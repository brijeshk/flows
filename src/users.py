"""
User management functions.
"""

from mongoengine import *
import protocols
import xmpp

MESSAGE_TRANSPORT_TYPES = ('email', 'native', 'sms', 'xmpp')


class User(Document):
    """
    Model class for a user.
    """
    name = StringField(required=True, primary_key=True, unique=True)
    email = StringField()
    password = StringField()
    message_transport_type = StringField(choices=MESSAGE_TRANSPORT_TYPES)
    own_protocols = ListField(ReferenceField(protocols.FlowProtocol))
    def __str__(self):
        return self.name


def new_user(user_name, email=None, password=None, transport=None):
    """
    Creates a user object.
    :type user_name: str
    :type email: str
    :rtype: User
    """
    # if no password specified, default to username
    if not password:
        password = user_name
    u = User(name=user_name, email=email, password=password, message_transport_type=transport)
    u.own_protocols = protocols.get_default_protocols()
    u.save()

    # we also need to create the user in the transport server if needed
    if transport == 'xmpp':
        xmpp.create_user_account_on_xmpp_server(user_name)
        
    return u


def get_user(user_name):
    """
    Gets a user object by name.
    :type user_name: str
    :rtype: User
    """
    u = User.objects.get(name=user_name)
    return u


def check_credentials(user_name, password):
    """
    Checks user name and password against stored credentials in db
    """
    u = User.objects.get(name=user_name)
    return (u and u.password == password)

    
def update_user(user_name, email, password, transport):
    """
    Updates user attributes.
    """
    o_user = get_user(user_name)
    o_user.password = password
    o_user.email = email
    o_user.transport = transport
    o_user.save()


def complete_user_name(prefix):
    """
    Completes a username given a prefix.
    :type prefix: str
    :rtype:list[User]
    """
    if prefix and prefix != '':
        ulist = User.objects(name__startswith=prefix)
    else:
        ulist = User.objects()

    # suppress password when sending user list to client
    ulist_no_password = []
    for u in ulist:
        u.password = None
        ulist_no_password.append(u)
    return ulist_no_password
