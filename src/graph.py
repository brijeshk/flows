"""
Performs simple graph queries.
This module assumes that a graph is a dict of {srcNode:list[destNode]}.
"""

def get_all_nodes(g):
    """
    Retrieves the full set of nodes.
    """
    all_nodes = set()
    for node in g.keys():
        all_nodes.add(node)
        for n in g[node]:
            all_nodes.add(n)
    return all_nodes


def get_roots(g):
    """
    Retrieves the set of nodes with no incoming edges.
    """
    return get_all_nodes(g) - get_targets(g)


def get_targets(g):
    """
    Retrieves the set of nodes with incoming edges.
    """
    sinks = set()
    for target_nodes in g.values():
        for node in target_nodes:
            sinks.add(node)
    return sinks


def get_sources(g):
    """
    Retrieves the set of nodes with outgoing edges.
    """
    return set(g.keys())


def get_sinks(g):
    """
    Retrieves the set of nodes with no outgoing edges.
    """
    return get_all_nodes(g) - get_sources(g)

if __name__ == '__main__':
    g = { 'a' : ['b','c'], 'b':['d','e'], 'e':['c'] }
    print get_roots(g)
    print get_sources(g)
    print get_targets(g)
    print get_sinks(g)
