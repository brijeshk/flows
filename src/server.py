"""
Webservice Flask App
Run as: python server.py
"""

import sys
import json
from functools import wraps
from flask import Flask
from flask import jsonify
from flask import request
from flask import Response

import protocols
import serverdb
import users
import clubs
import auth_google

# init app and related modules
app = Flask(__name__)
app.register_blueprint(auth_google.mod_auth)
app.secret_key = '@utometon-secret-key'

# init model db
serverdb.init_db('flows', 'localhost')

class InvalidUsage(Exception):
    """
    Class to wrap an exception into a dict object with a status_code and message
    """
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth:
            return Response('No login credentials supplied', 401, {'WWW-Authenticate': 'Basic realm="Login Required"'})
        else:
            if not users.check_credentials(auth.username, auth.password):
                return Response('Invalid credentials', 401)
        return f(*args, **kwargs)
    return decorated


@app.errorhandler(InvalidUsage)
def _handle_invalid_usage(error):
    """
    Util function to wrap an InvalidUsage exception into a HTTP error response
    """
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    app.logger.error(response)
    return response


def _make_json_resp(dbobject):
    """
    Util function to make a HTTP success response with JSON payload of a mongoengine object or list or set.
    """
    if type(dbobject) in [list, set]:
        json_list = [o.to_json() for o in dbobject]
        json_out = '[' + ','.join(json_list) + ']'
    else:
        json_out = dbobject.to_json()
    return Response(json_out, status=200, mimetype='application/json')


@app.route("/hello")
def hello():
    return "hello world"


'''
@app.route("/flows/user/<user>")
def find_flows(user):
    """
    Finds all flows involving given user.
    Optionally filters by state and by creator.
    """
    state = request.args.get('state', None)
    from_user = request.args.get('fromUser', None)
    port = request.args.get('port', None)
    fs = flows.findFlows(user, from_user, None, port, state)
    return _make_json_resp(fs)


@app.route("/flows/id/<int:fid>")
def get_flow_by_id(fid):
    """
    Gets full details of a flow by its id.
    """
    try:
        f = flows.getFlow(fid)
    except Exception as e:
        raise InvalidUsage(str(e), 404)
    else:
        return _make_json_resp(f)


@app.route("/flows", methods=["POST"])
def create_flow():
    """
    Creates a new flow.
    """
    try:
        f = flows.newFlow(request.form['pid'],
                          request.form['fromUser'],
                          request.form['toUsers'].split(','),
                          request.form['subject'],
                          request.form['body'],
                          request.form['cmd'])
    except Exception as e:
        raise InvalidUsage(str(e), 404)
    else:
        return _make_json_resp(f)


@app.route("/flows/id/<int:fid>", methods=["POST"])
def update_flow(fid):
    """
    Updates an existing flow with a new message and/or state.
    """
    try:
        f = flows.getFlow(fid)
        flows.updateFlow(f,
                         request.form['fromUser'],
                         request.form['body'],
                         request.form['cmd'])
    except Exception as e:
        raise InvalidUsage(str(e), 404)
    else:
        return _make_json_resp(f)
'''

@app.route("/clubs/user/<userid>")
def get_clubs_containing_user(userid):
    try:
        return _make_json_resp([c for c in clubs.get_clubs_by_user(userid)])
    except Exception as e:
        raise InvalidUsage(str(e), 404)


@app.route("/clubs/public")
def get_public_clubs():
    try:
        return _make_json_resp([c for c in clubs.get_public_clubs()])
    except Exception as e:
        raise InvalidUsage(str(e), 404)


@app.route("/ports/user/<user>")
def get_ports_exposed_by_user(user):
    """
    Gets ports exposed by user.
    """
    try:
        u = users.get_user(user)
        return _make_json_resp([p for p in protocols.find_protocols_by_creator(user)])
    except Exception as e:
        raise InvalidUsage(str(e), 404)


@app.route("/ports/visible_to/<user>")
def get_ports_visible_to_user(user):
    """
    Gets ports visible to user.
    """
    try:
        u = users.get_user(user)
        return _make_json_resp([p for p in protocols.find_protocols_visible_to_user(user)])
    except Exception as e:
        raise InvalidUsage(str(e), 404)


@app.route("/ports/id/<pid>")
def get_port_by_id(pid):
    """
    Gets port by id.
    """
    try:
        p = protocols.find_protocol_by_id(pid)
        return _make_json_resp(p)
    except Exception as e:
        raise InvalidUsage(str(e), 404)


@app.route("/ports/keyword")
def get_ports_by_keyword():
    """
    Finds ports by data keyword.
    """
    try:
        list_fp_matches = protocols.find_protocols_by_keyword(request.args.get('q', None))
        return _make_json_resp(list_fp_matches)
    except Exception as e:
        raise InvalidUsage(str(e), 500)


@app.route("/users/<name>")
def get_user(name):
    """
    Gets user by name.
    """
    try:
        u = users.get_user(name)
        return _make_json_resp(u)
    except Exception as e:
        raise InvalidUsage(str(e), 404)


@app.route("/ports", methods=["POST"])
def create_port():
    """
    Creates a port.
    """
    try:
        p = protocols.new_flow_protocol(request.json['name'],
                                        request.json['creator'],
                                        request.json['command_transitions'],
                                        request.json['command_processors'],
                                        request.json['visible_to'])
        return _make_json_resp(p)
    except Exception as e:
        app.logger.exception("create_port failed")
        raise InvalidUsage(str(e), 404)


@app.route("/ports/<pid>/valid_cmds")
def get_valid_commands(pid):
    """
    Gets valid command transitions
    """
    curstate = request.args.get('cur', None)
    port = protocols.find_protocol_by_id(pid)
    cmds = port.get_valid_transition_commands(curstate, None)
    return json.dumps([x for x in cmds])


@app.route("/ports/action_cmds")
def get_action_commands():
    """
    Gets valid command transitions
    """
    pid = request.args.get('pid', None)
    if pid:
        port = protocols.find_protocol_by_id(pid)
        cmds = port.get_action_commands()
    else:
        cmds = []
        for p in protocols.find_protocols_by_name(None):
            cmds += p.get_action_commands()
    return json.dumps([x for x in cmds])


@app.route("/ports/<pid>/fire_cmd")
def process_command(pid):
    """
    Fires a command with some input data.
    """
    cmd = request.args.get('cmd', None)
    data = request.args.get('data', None)
    port = protocols.find_protocol_by_id(pid)
    result = port.fire_command(cmd, data)
    return json.dumps(result)


@app.route("/users/login")
@requires_auth
def login_user():
    return Response(status=200)


@app.route("/users")
def get_users():
    """
    Completes a username given a prefix.
    """
    query = request.args.get('q', None)
    ulist = users.complete_user_name(query)
    return _make_json_resp(ulist)


@app.route("/users", methods=["POST"])
def register_user():
    """
    Registers a new user.
    """
    try:
        username = request.json['username']
        password = request.json['password']
        email = request.json['email']
        transport = request.json['transport']
        u = None
        try:
            u = users.get_user(u)
        except Exception as e:
            pass
        if u:
            raise InvalidUsage('User already exists', 403)
        else:
            app.logger.debug("creating new user " + username)
            u = users.new_user(username, email, password, transport)
        return _make_json_resp(u)
    except Exception as e:
        app.logger.exception("register_user failed")
        raise InvalidUsage(str(e), 403)


@app.route("/users", methods=["PUT"])
@requires_auth
def update_user():
    """
    Updates existing user.
    """
    try:
        username = request.json['username']
        password = request.json['password']
        email = request.json['email']
        transport = request.json['transport']
        u = None
        try:
            u = users.get_user(username)
        except Exception as e:
            raise InvalidUsage('User doesn\'t exist', 403)
        app.logger.debug("updating existing user " + username)
        users.update_user(username, email, password, transport)
        return "Success"
    except Exception as e:
        app.logger.exception("update_user failed")
        raise InvalidUsage(str(e), 403)


def print_routes():
    """
    Prints registered routes to stdout.
    """
    import urllib
    output = []
    for rule in app.url_map.iter_rules():
        methods = ','.join(rule.methods)
        line = urllib.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, rule))
        output.append(line)
    for line in sorted(output):
        print(line)


if __name__ == '__main__':
    host = sys.argv[1] if len(sys.argv) > 1 else 'localhost'
    print 'Connected to db ' + host
    print 'Registered routes:'
    print_routes()
    print ''
    print 'Starting webserver...'
    app.debug = True
    app.run(host='0.0.0.0')
