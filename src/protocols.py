"""
Protocols module.
Handles protocol definition, query.
"""

from mongoengine import *
import importlib
import graph
import utils

LINKTYPES = ('depends_on',
             'parent_of',
             'fork_of',
             'dupe_of',
             'like'
            )

class CommandFireResult:
    FireAgainWithAnyInput = 1
    FireAgainWithProvidedInput = 2
    FireAgainWithCachedInput = 3
    FireDone = 4
    FireError = 5


class FlowProtocol(Document):
    """
    Model class for a FlowProtocol.
    """
    id = SequenceField(primary_key=True)
    name = StringField(required=True)
    command_transitions = DictField()
    creator = StringField()
    command_processors = DictField()
    visible_to = ListField(StringField())

    def get_valid_transition_commands(self, command, data=None):
        """
        Returns the valid command transitions from a given command.
        :type command: str
        :type data: str
        :rtype: list[str]
        """
        if command:
            return self.command_transitions.get(command, [])
        else:
            return graph.get_roots(self.command_transitions)

    def get_action_commands(self):
        """
        Returns commands that have some pending transition - in other words, imply some action.
        """
        return graph.get_sources(self.command_transitions)

    def fire_command(self, command, data=None):
        """
        Fires a command. A command may process input data in multiple fire steps.
        It returns output data and a flag that indicates whether there's further input needed or not.
        E.g. a search command may fire like this:
          no_input -> no_output, FireAgainWithAnyInput
          some_input -> some_outputs, FireDone
        OR
          no_input -> some_output_choices, FireAgainWithProvidedInput
          output_choice -> more_outputs, FireAgainWithProvidedInput
          output_choice -> None, FireDone
        :type command: str
        :type data: str
        :rtype: dict or None, FireCommandResult
        """
        if not (self.command_processors and command in self.command_processors):
            # command needs no processing
            return None, CommandFireResult.FireDone

        if (isinstance(self.command_processors[command], str) or \
            isinstance(self.command_processors[command], unicode)) and \
           self.command_processors[command].startswith('.'):
            module_method = self.command_processors[command][1:].split('.')
            try:
                m = importlib.import_module('connectors.' + module_method[0])
                function = getattr(m, module_method[1])
                return function(data)
            except Exception as e:
                return str(e), CommandFireResult.FireError
        else:
            if not data:
                return self.command_processors[command], CommandFireResult.FireDone
            elif data in self.command_processors[command]:
                return self.command_processors[command][data], CommandFireResult.FireDone
            else:
                return None, CommandFireResult.FireDone
        return None, CommandFireResult.FireDone

    def is_auto_transition_command(self, command):
        """
        Returns a flag indicating if given command requires automatic transition to its successors.
        This is to facilitate multiple commands being executed in a row without needing a flow message being sent.
        Currently, commands that begin with lower case letters are auto-transition commands.
        :type command: str
        :rtype: boolean
        """
        return (len(command) > 0) and command[0].islower()

    def is_messageable_command(self, command):
        """
        Returns a flag indicating if command can be used as a final step in messaging a user.
        """
        return command != 'select' and command != 'Info'


def _create_converse_protocol():
    return new_flow_protocol('Converse',
                             'system',
                             { 'Ask' : ['Answer', 'Cancel'],
                               'Do' : ['Done', 'Cancel'],
                               'Tell' : [],
                               'FYI': [],
                             })


def _create_forsale_protocol():
    return new_flow_protocol('ForSale',
                             'system',
                             { 'Offer' : ['Accept', 'Reject', 'Recall'],
                               'Accept' : ['Pay'],
                               'Pay' : ['Ship'] })


def _create_appointment_protocol():
    return new_flow_protocol('Appointment',
                             'system',
                             { 'Request' : ['Options', 'Cancel'],
                               'Options' : ['Select', 'Cancel'],
                               'Select' : ['Accept', 'Decline']
                             })


def _create_weather_protocol():
    return new_flow_protocol('Hows-the-Weather',
                             'weather',
                             {'Get':[]},
                             {'Get': '.weather.get_weather_forecast'})


def _create_ebay_protocol():
    return new_flow_protocol('Ebay',
                             'ebay',
                             { 'Find':['Buy'], 'Buy':['Cancel'] },
                             { 'Find': '.ebay.find_items',
                               'Buy': '.ebay.buy_item'})

def _create_movies_protocol():
    return new_flow_protocol('Movies',
                             'movies',
                             { 'Find':['Buy'], 'Buy':['Cancel'] },
                             { 'Find': '.movies.getMoviesNearZipCode',
                               'Buy': '.movies.buyTicketsForMovie'})


def new_flow_protocol(name, creator, command_transitions, command_processors=None, visible_to=None):
    """
    Creates a new flow protocol.
    :type name: str
    :type creator: str
    :type command_transitions: dict[str, list[str]]
    :type command_processors: dict[str,str] or None
    :rtype: FlowProtocol
    """
    fp = FlowProtocol(name=name,
                      creator=creator,
                      command_transitions=command_transitions,
                      visible_to=visible_to,
                      command_processors=command_processors)
    fp.save()
    return fp


def find_protocols_by_name(pname):
    if pname:
        return FlowProtocol.objects(name__icontains=pname)
    else:
        return FlowProtocol.objects()


def find_protocols_by_creator(user):
    return FlowProtocol.objects(creator__in=[user,'system'])


def find_protocols_visible_to_user(user):
    # first get all ports that have no visible_to attribute, they are visible to the world
    lo_fp = [p for p in FlowProtocol.objects(visible_to__size=0)]
    # then add ports that are visible to this user
    for p in FlowProtocol.objects(visible_to=user):
        lo_fp.append(p)
    return lo_fp


def find_protocol_by_id(pid):
    return FlowProtocol.objects.get(id=int(pid))


def find_protocols_by_keyword(keyword):
    """
    Finds protocols by matching keywords in its data options (command_processors).
    Right now, this is an non-optimal brute force search. Needs to be made better.
    """
    if not keyword:
        return FlowProtocol.objects()

    lo_fp_matches = set(find_protocols_by_name(keyword))
    for fp in FlowProtocol.objects():
        if fp.command_processors:
            ls_flat_data = utils.flatten_object_to_list(fp.command_processors)
            for s in ls_flat_data:
                if keyword in s:
                    lo_fp_matches.add(fp)
                    break
    return lo_fp_matches


def create_default_protocols():
    """
    Creates system-default protocols.
    """
    _create_converse_protocol()
    _create_forsale_protocol()
    _create_appointment_protocol()
    _create_weather_protocol()
    _create_ebay_protocol()
    _create_movies_protocol()


def get_default_protocols():
    """
    Gets default protocols created by the system and available to all users.
    :rtype: list[FlowProtocol]
    """
    default_protocols = [p for p in find_protocols_by_creator('system')]
    return default_protocols


def get_converse_protocol():
    """
    Gets the default Converse protocol.
    """
    for p in find_protocols_by_name('Converse'):
        if p.creator == 'system':
            return p
    raise Exception('No system default Converse port found!')
