#!/usr/bin/env python
"""
XMPP transport layer.
"""

import sys
import logging
from optparse import OptionParser
import time
import threading
import sleekxmpp
import os

# Python versions before 3.0 do not use UTF-8 encoding
# by default. To ensure that Unicode is handled properly
# throughout SleekXMPP, we will set the default encoding
# ourselves to UTF-8.
if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding('utf8')
else:
    raw_input = input


session = None

XMPP_SERVER = ('ec2-54-186-9-137.us-west-2.compute.amazonaws.com', 5222)
#XMPP_SERVER = ('localhost', 5222)

class FlowXMPPClient(sleekxmpp.ClientXMPP):
    """
    XMPP client to send/receive flow messages over XMPP.
    """
    thread = None
    callback_receive_func = None

    def __init__(self, jid, password, cb_receive_func=None):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)
        self.add_event_handler("session_start", self.start)
        self.add_event_handler("message", self.receive_flow_message)
        self.callback_receive_func = cb_receive_func

    def start(self, event):
        self.send_presence()
        self.get_roster()
        logging.info('XMPP client connected')

    def receive_flow_message(self, msg):
        if msg['type'] in ('chat', 'normal'):
            body = msg['body']
            sender = get_user_name(msg['from'].user)
            receiver = get_user_name(msg['to'].user)
            if body.startswith('#MP'):
                msgfields = body.split('#')
                metafields = msgfields[1].split(':', 6)
                if len(msgfields) > 2:
                    msgbody = msgfields[2]
                else:
                    msgbody = None
                mp, flow_uuid, protocolid, crud_flag, state, data = [metafields[i] if i < len(metafields) else None for i in range(6)]
                logging.info('received flow msg: {0} {1} {2}'.format(flow_uuid, state, data))
                logging.info('body: ' + str(msgbody))
                if self.callback_receive_func:
                    try:
                        self.callback_receive_func(protocolid,
                                                   flow_uuid,
                                                   sender,
                                                   receiver,
                                                   state,
                                                   data,
                                                   msgbody,
                                                   crud_flag)
                    except:
                        logging.exception('Error from callback_receive_func')
            else:
                logging.info('Received non-flow msg: ' + msg['body'] + ' from ' + sender)

    def send_flow_message(self, to_jid, body):
        logging.debug('sending xmpp message to : ' + to_jid)
        self.send_message(mto=to_jid,
                          mbody=body,
                          mtype='chat')

    def block_process(self):
        self.process(block=True)

    def start_listen_thread(self):
        self.thread = threading.Thread(target=self.block_process)
        self.thread.start()

    def stop_listen_thread(self):
        self.disconnect(wait=True)
        self.wait_for_process_thread()

    def wait_for_process_thread(self):
        self.thread.join()


def get_xmpp_jid(user_name):
    return user_name.replace('@gmail.com', '_gmail_com')


def get_user_name(jid):
    return jid.replace('_gmail_com', '@gmail.com')


def login(user_id, password, callback_receive_func=None):
    global session
    jid = get_xmpp_jid(user_id)
    logging.info('logging in user to xmpp : ' + jid)
    try:
        session = FlowXMPPClient(jid + '@localhost', jid, callback_receive_func)
        session.register_plugin('xep_0030') # Service Discovery
        session.register_plugin('xep_0199') # XMPP Ping
        if not session.connect(XMPP_SERVER):
            logging.error('Unable to connect to XMPP server')
            return False
        session.start_listen_thread()
        logging.info('XMPP session started')
        return True
    except:
        logging.exception('Error connecting to XMPP server')
        return False


def logout():
    global session
    if session:
        session.stop_listen_thread()
        logging.info('XMPP session disconnected')
        session = None


def send_flow_message(receiver, atom, flow):
    msg = '#MP:' + str(flow.uuid) + ':' + str(flow.protocol) + ':' + str(atom.crud_flag)
    if atom.state:
        msg += ':' + atom.state
    if atom.data:
        msg += ':' + atom.data
    if atom.body:
        msg += '#' + atom.body
    session.send_flow_message(get_xmpp_jid(receiver) + '@localhost', msg)


def create_user_account_on_xmpp_server(user_id):
    ec_int = os.system('ejabberdctl register {0} localhost {1}'.
                       format(get_xmpp_jid(user_id), get_xmpp_jid(user_id)))
    if ec_int != 0:
        raise Exception('Error creating user in XMPP server. ejabberdctl returned: ' + str(ec_int))


if __name__ == '__main__':
    optp = OptionParser()
    optp.add_option("-j", "--jid", dest="jid",
                    help="JID to use")
    optp.add_option("-p", "--password", dest="password",
                    help="password to use")
    opts, args = optp.parse_args()

    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)-8s %(message)s')

    if not login(opts.jid, opts.password, None):
        print 'Error logging in'
        sys.exit(1)

    while True:
        time.sleep(3)
        to_jid = raw_input("Send To: ")
        if to_jid == '':
            break
        body = raw_input('Msg: ')
        session.send_flow_message(to_jid, body)
        print 'Message sent'
    logout()
