#!/bin/bash
# stop running server
killall autometon
pkill java
pkill -f "eventbot.py"

# build
cd ~/bitb/flows/goserver
./build.sh

# start
nohup ./autometon 2>&1 &

# wait till fully initialized
sleep 5

# start metonbot
cd ~/bitb/flows/pyclient
nohup python2.7 eventbot.py -u stock -p stock 2>&1 &
nohup python2.7 eventbot.py -u google -p google 2>&1 &
nohup python2.7 eventbot.py -u weather -p weather 2>&1 &
nohup python2.7 eventbot.py -u yelp -p yelp 2>&1 &
nohup python2.7 eventbot.py -u translate -p translate 2>&1 &

# start comics and johnnymeton bots
#cd ~/bitb/flows/ui/netbeans/AutometonBot
#export CLASSPATH=./lib/*:./dist/*
#nohup java com.auto.server.SystemBot -j Comics > comics.out &
#nohup java com.auto.server.bots.JohnnyMeton > johnny.out &

# list running processes for manual check
ps -ef | grep autometon 
ps -ef | grep -i bot
