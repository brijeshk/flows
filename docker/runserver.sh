#!/usr/bin/env sh
if [ ! -d /am/goserver ]; then
  echo "goserver directory not mounted. bailing."
  exit 1
fi
cd /am/goserver
if [ -f /am/bootstrap ]; then
  echo "Autometon databases not bootstrapped yet..."
  echo "Waiting 2 mins for dbs to init..."
  sleep 120
  echo "Setting up mysql..."
  cd sql
  ./boot.sh
  cd ../data
  echo "Setting up mongo..."
  ./boot.sh  
  rm /am/bootstrap
else 
  echo "Already bootstrapped"
  echo "sleeping 15 seconds for dbs to init..."
  sleep 15
fi
echo "starting autometon server..."
/am/goserver/autometon
