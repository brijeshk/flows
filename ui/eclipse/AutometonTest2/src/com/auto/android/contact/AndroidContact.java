package com.auto.android.contact;

import android.net.Uri;

import com.auto.model.User;

/* to import contacts from android */
public class AndroidContact {
	private String name;
	private String phone;
	private String email;
	private byte[] photo;
	private boolean isAutometon;
	private User user;
	private Uri uri;
	private String androidid;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public void setAndroidId(String id) {
		this.androidid = id;
	}
	public String getId() {
		return email;
	}
	public boolean isAutometon() {
		return isAutometon;
	}
	public void setIsAutometon(boolean b) {
		this.isAutometon = b;
	}
	
	public String getGroup() {
		String t = "Default";
		if (isAutometon)
			t = "Autometon contacts";
		return t;
	}
	public String getType(){
		if (isAutometon && user != null) {
			if (user.isBlacklisted()) return "Blacklisted";
			else if (user.isFriend()) return "Friend";
			else return "Autometon contact";
		}
		else {
			return "Android contact";
		}
	}
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public byte[] getPhoto() {
		return photo;
	}


	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}


	public AndroidContact() {
		
	}
	
	public String toString() {
		return name+", "+phone+", "+email;
	}
	public int hashCode() {
		return this.toString().hashCode();
	}
	public boolean equals(Object o) {
		if (o == null ) return false;
		return o.toString().equals(this.toString());
	}
	public void setUri(Uri mSelectedContactUri) {
		this.uri = mSelectedContactUri;
		
	}
	public Uri getUri() {
		return uri;
	}
	
}
