package com.auto.android.contact;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.model.FlowManager;
import com.auto.model.Port;
import com.auto.model.User;

public class ContactImporter extends Fragment implements
		 AdapterView.OnItemLongClickListener {

	private int bgcolor;
	protected String TAG = getClass().getName();
	private final static String[] FROM_COLUMNS = { Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? Contacts.DISPLAY_NAME_PRIMARY
			: Contacts.DISPLAY_NAME };

	private static final String[] PROJECTION = {
			Contacts._ID,
			Contacts.LOOKUP_KEY,
			Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? Contacts.DISPLAY_NAME_PRIMARY
					: Contacts.DISPLAY_NAME

	};

	private ExpandableListView listView;
	private ExpandableAndroidContactListAdapter adapter;
	private ArrayList<AndroidContact> userlist;
	
	private  ArrayList<AndroidContact> addedusers;
	private  ArrayList<AndroidContact> removedusers;
	
	private FlowManager man;

	// The column index for the _ID column
	private static final int CONTACT_ID_INDEX = 0;
	// The column index for the LOOKUP_KEY column
	private static final int LOOKUP_KEY_INDEX = 1;

	private static final String SELECTION = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? Contacts.DISPLAY_NAME_PRIMARY
			+ " LIKE ?"
			: Contacts.DISPLAY_NAME + " LIKE ?";

	// Defines a variable for the search string
	private String mSearchString;
	// Defines the array to hold values that replace the ?
	private String[] mSelectionArgs = { mSearchString };

	private Activity activity;

	/*
	 * Defines an array that contains resource ids for the layout views that get
	 * the Cursor column contents. The id is pre-defined in the Android
	 * framework, so it is prefaced with "android.R.id"
	 */
	private final static int[] TO_IDS = { android.R.id.text1 };

	// Define variables for the contact the user selects
	// The contact's _ID value
	long mContactId;
	// The contact's LOOKUP_KEY
	String mContactKey;
	// A content URI for the selected contact
	Uri mContactUri;
	// An adapter that binds the result Cursor to the ListView
	private SimpleCursorAdapter mCursorAdapter;

	private Button btnsave;
	private Button btncancel;
	private Button btnhelp;

	public ContactImporter() {
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		p("1. onAttach");

		this.activity = activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the fragment layout
		p("3. onCreateView");
		man = FlowManager.getManager();
		View v = inflater.inflate(R.layout.contact_importer_fragment,
				container, false);

		listView = (ExpandableListView) v.findViewById(android.R.id.list);
		
		
		TextView header = (TextView) v.findViewById(R.id.header);
		header.setText("Android Contact List");

		btnsave = (Button) v.findViewById(R.id.save);
		btncancel = (Button) v.findViewById(R.id.cancel);
		btnhelp = (Button) v.findViewById(R.id.help);
		btncancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("Cancel clicked ");
				
			}
	    });
	    btnhelp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("help clicked");				
				
			}
	    });
	    btnhelp.setVisibility(View.GONE);
	    btncancel.setVisibility(View.GONE);
	    btnsave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("=========  save clicked========= ");
				User me = man.getMe();
				
				for (AndroidContact u: addedusers) {
					p("ADDING contact "+u.getUser().getId());
					me.addUser(u.getUser());	
					// also add this user to the manager, and save it
					man.addUser(u.getUser());
					// TODO: do we check the server if this user exists? do we inflate it? ask
					// the user to send an email etc?
					
				}
				for (AndroidContact u: removedusers) {
					p("REMOVING contact  "+u.getUser().getId());
					man.getMe().removeUser(u.getUser());	
					// also add this user to the manager, and save it
					man.remove(u.getUser());

				}
				p("Updating current user:\n"+me.toJson());
				boolean ok = man.updateMe();
				if (!ok) {
					msg("I was not able to save the user");
				}
			}
	    });
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		p("4. onActivityCreated");
		// Always call the super method first
		super.onActivityCreated(savedInstanceState);	
		loadContacts();
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		p("Long item click");
		return false; // let the system show the context menu
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		if (v.getId() == android.R.id.list) {
			p("Context menu on user item");
			menu.setHeaderTitle("Action on contact");
			String[] menuItems = getResources().getStringArray(
					R.array.importusermenulist);
			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		} else
			p("Context menu not on list: " + v);

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		p("User picked menu " + item);
		String t = item.getTitle().toString().toLowerCase();

		ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item
				.getMenuInfo();
		View v = info.targetView;
		p("Target view " + v);
		p("Packed pos=" + info);
		int group = listView.getPackedPositionGroup(info.packedPosition);
		int child = listView.getPackedPositionChild(info.packedPosition);
		/*
		 * <item>Add contact to Autometon list</item> <item>Remove contact from
		 * Autometon list</item> <item>Blacklist/unblacklist contact in
		 * Autometon</item> <item>Mark/Unmark contact as friend in
		 * Autometon</item>
		 */
		AndroidContact user = (AndroidContact) adapter.getChild(group, child);
		if (t.startsWith("add")) {
			add(user);
		} else if (t.startsWith("remove")) {
			remove(user);
		} else if (t.indexOf("blacklist") > -1) {
			msg("blacklist/unblacklist user " + user.getName());
			if (user.getUser() == null) {
				add(user);
				user.getUser().setBlacklisted(user.getUser().isBlacklisted());
			}
		} else if (t.indexOf("friend") > -1) {
			msg("friend/unfriend user " + user.getName());
			if (user.getUser() == null) {
				add(user);
				user.getUser().setFriend(user.getUser().isFriend());
			}
		}
		return true;

	}

	private void add(AndroidContact user) {
		msg("ADDING " + user.getName());
		user.setIsAutometon(true);
		User u = convert(user);
		user.setUser(u);
		
		
		removedusers.remove(user);
		if (!addedusers.contains(user)) {
			addedusers.add(user);
			p("Adding added user: "+user);
		}

		
	}

	private User convert(AndroidContact user) {
		User u = new User(user.getEmail());
		u.setName(user.getName());
		u.setEmail(user.getEmail());
		u.setPhone(user.getPhone());
		u.setId(u.getEmail());
		user.setUser(u);
		return u;
	}

	private void remove(AndroidContact user) {
		msg("REMOVING " + user.getName());
		user.setIsAutometon(false);
		user.setUser(null);
		addedusers.remove(user);
		
		if (!removedusers.contains(user)) {
			removedusers.add(user);
			p("Adding removed user: "+user);
		}
		
	}


	public void setSearchString(String search) {
		mSearchString = search;
	}

	public byte[] openPhoto(String contactId) {
		p("Getting photo of " + contactId);
		Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI,
				Long.parseLong(contactId));
		Uri photoUri = Uri.withAppendedPath(contactUri,
				Contacts.Photo.CONTENT_DIRECTORY);
		Cursor cursor = activity.getContentResolver().query(photoUri,
				new String[] { Contacts.Photo.PHOTO }, null, null, null);
		if (cursor == null) {
			return null;
		}
		try {
			if (cursor.moveToFirst()) {
				byte[] data = cursor.getBlob(0);
				if (data != null) {
					return data;
				}
			}
		} finally {
			cursor.close();
		}
		return null;
	}

	
	private void loadContacts() {
		userlist = new ArrayList<AndroidContact>();
		addedusers = new ArrayList<AndroidContact>();
		removedusers = new ArrayList<AndroidContact>();
		
		ContentResolver cr = activity.getContentResolver(); // Activity/Application
															// android.content.Context
		Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
				null, null, null);
		p("Got results: " + cursor.getCount());
		if (cursor.moveToFirst()) {

			do {
				String id = cursor.getString(cursor
						.getColumnIndex(ContactsContract.Contacts._ID));

				Cursor pCur = cr.query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ " = ?", new String[] { id }, null);
				while (pCur.moveToNext()) {
					String phone = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					String contactid = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
					String name = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Identity.DISPLAY_NAME));
					String email = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));

					p("Got id " + contactid + ": " + name + ":" + phone+", email "+email);
					AndroidContact candroid = new AndroidContact();
					candroid.setName(name);
					candroid.setPhone(phone);
					candroid.setEmail(email);
					candroid.setPhoto(openPhoto(id));
					if ( (phone!= null && phone.length()>5) || (email != null && email.contains("@"))) {
						userlist.add(candroid);
						for (User autometonuser : FlowManager.getManager().getUsers()) {
							if (name != null && name.length() > 0) {
								if (name.equalsIgnoreCase(autometonuser.getName())) {
									p("same name: " + name+"/"+autometonuser);
									candroid.setIsAutometon(true);									
								}
							}
							if (phone != null && autometonuser.getPhone() != null
									&& autometonuser.getPhone().length() > 5) {
								if (phone.indexOf(autometonuser.getPhone()) > -1
										|| autometonuser.getPhone().indexOf(phone) > -1) {
									p("same phone: " + phone+"/"+autometonuser);
									candroid.setIsAutometon(true);
									
								}
							}
							if (email != null && autometonuser.hasEmail()) {
								if (email.equalsIgnoreCase(autometonuser.getEmail())) {
									p("same email: " + email+"="+autometonuser.getEmail());
									candroid.setIsAutometon(true);
								}
							}
							if (candroid.isAutometon()) {
								candroid.setUser(autometonuser);								
							}
						}
					}
					break;
				}
				pCur.close();

			} while (cursor.moveToNext());
		}
		addItems();
	}

	private void addItems() {
		p("============ ADD ITEMS ============");
		adapter = new ExpandableAndroidContactListAdapter(getActivity());
		p("adding groups");

		p("Adding all users");
		for (AndroidContact u : userlist) {
			p(" adding user " + u);
			adapter.add(u);
		}

		listView.setAdapter(adapter);
		for (int i = 0; i < adapter.getGroupCount(); i++) {
			listView.expandGroup(i);
		}
		adapter.notifyDataSetChanged();
		// p("after adding items: "+adapter.getGroupCount()+", counting children: "+adapter.getChildrenCount(0));
		listView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView listView, View v,
					int group, int pos, long id) {
				if (group >= 0 && pos >= 0) {
					AndroidContact user = (AndroidContact) adapter.getChild(
							group, pos);
					p("==============  onChildClick user :" + user);
					user.setIsAutometon(!user.isAutometon());					
					ImageView star = (ImageView) v.findViewById(R.id.star);
					if (user.isAutometon()) {
						star.setImageResource(AndroidGui.getStar());
						v.setBackgroundColor(Color.rgb(220, 220, 255));
						star.setVisibility(View.VISIBLE);						
						TextView txttype = (TextView) v.findViewById(R.id.lbltype);
						txttype.setText(user.getType());
						add(user);
					} else {
						v.setBackgroundColor(Color.LTGRAY);
						star.setVisibility(View.GONE);
						TextView txttype = (TextView) v.findViewById(R.id.lbltype);
						txttype.setText(user.getType());
						remove(user);
					}
				}
				return false;
			}
		});
	}

	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(this.getActivity(), s, Toast.LENGTH_SHORT).show();
		p("msg: " + s);
	}

}
