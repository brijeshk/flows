package com.auto.android.contact;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.model.FlowManager;
import com.auto.model.User;

public class UserArrayAdapter extends ArrayAdapter<User> {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	
	
	private TextView txtmsg;
	private TextView txtinfo;
	private List<User> datas = new ArrayList<User>();

	private FlowManager man;
	
	@Override
	public void add(User object) {
		man = FlowManager.getManager();
		datas.add(object);
		super.add(object);
	}

	public UserArrayAdapter(Context context) {
		super(context, android.R.id.text1);
	}

	public int getCount() {
		return this.datas.size();
	}

	public User getItem(int index) {
		return this.datas.get(index);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.user_li, parent, false);
		}
		User user = getItem(position);
		if (user == null) {
			p(" getView: got no user");
		}
		ImageView image = (ImageView) row.findViewById(R.id.icon);
	//	image.setBackgroundDrawable(AndroidGui.getDrawable(user));
		image.setImageResource(AndroidGui.getDrawable(user));
		txtmsg = (TextView) row.findViewById(R.id.lblname);	
		txtmsg.setText(user.getName());
							
		txtinfo = (TextView) row.findViewById(R.id.lbldetail);	
		String email = user.getEmail();
		if (user.isSystem()) email = "System user";
		else if (user.isSystem()) email = "Templates user";
		if (email != null) txtinfo.setText(email);				
		else txtinfo.setText("no email" );
		return row;
	}

	
	protected void p(String s) {
		Log.i(TAG, s);
	}

}