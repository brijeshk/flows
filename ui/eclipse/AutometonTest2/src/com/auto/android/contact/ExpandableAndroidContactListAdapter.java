package com.auto.android.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.auto.android.AndroidGui;
import com.auto.android.R;

public class ExpandableAndroidContactListAdapter extends
		BaseExpandableListAdapter {

	private Context _context;
	private List<String> types; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<AndroidContact>> typemap;

	public ExpandableAndroidContactListAdapter(Context context) {
		this._context = context;
		this.types = new ArrayList<String>();
		this.typemap = new HashMap<String, List<AndroidContact>>();

	}

	public void clear() {
		types.clear();
		typemap.clear();
	}

	public List<AndroidContact> get(String type) {
		return typemap.get(type);
	}

	public int getNr(String type) {
		List<AndroidContact> plist = get(type);
		if (plist == null) {
			return 0;
		} else
			return plist.size();
	}

	public void add(AndroidContact child) {
		String type = child.getGroup();
		if (!types.contains(type)) {
			types.add(type);

		}
		List<AndroidContact> plist = get(type);
		if (plist == null) {
			plist = new ArrayList<AndroidContact>();
			typemap.put(type, plist);
		}
		if (!plist.contains(child))
			plist.add(child);
		// p("added type " + type);
		this.notifyDataSetChanged();

	}

	
	public void remove(AndroidContact child) {
		String type = child.getGroup();
		List<AndroidContact> plist = get(type);
		if (plist == null) {
			plist = new ArrayList<AndroidContact>();
			typemap.put(type, plist);
		}
		plist.remove(child);
		this.notifyDataSetChanged();

	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this.typemap.get(this.types.get(groupPosition)).get(
				childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View row, ViewGroup parent) {

		final AndroidContact contact = (AndroidContact) getChild(groupPosition,
				childPosition);

		if (row == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = infalInflater.inflate(R.layout.android_user_li, null);
		}
		ImageView image = (ImageView) row.findViewById(R.id.icon);
		// image.setBackgroundDrawable(AndroidGui.getDrawable(user));
		byte[] photo = contact.getPhoto();
		if (photo != null) {
			Drawable dr = new BitmapDrawable(BitmapFactory.decodeByteArray(photo,
					0, photo.length));
			image.setImageDrawable(dr);
		}
		else image.setImageResource( R.drawable.teacher);
		TextView txtmsg = (TextView) row.findViewById(R.id.lblname);
		txtmsg.setText(contact.getName());

		TextView txttype = (TextView) row.findViewById(R.id.lbltype);
		txttype.setText(contact.getType());
		
		ImageView star = (ImageView) row.findViewById(R.id.star);
		if (contact.isAutometon()) {
			if (contact.getUser().isBlacklisted()) {
				star.setImageResource(AndroidGui.getCancel());
			}
			else if (contact.getUser().isFriend()) {
				star.setImageResource(AndroidGui.getStar());
			}
			else if (contact.getUser().isSystem()) {
				star.setImageResource(AndroidGui.getBot());
			}
		}
		else {
			star.setVisibility(View.GONE);
		}
		TextView txtinfo = (TextView) row.findViewById(R.id.lbldetail);
		String email = contact.getEmail();
		String phone = contact.getPhone();
		String details = "";
		if (phone != null && phone.length()>5) {
			details += phone+" ";
		}
		if (email != null && email.indexOf("@")>0) {
			details += email;
		} 
		txtinfo.setText(details);
		return row;
	}

	@Override
	public int getChildrenCount(int groupPosition) {

		int nr = this.typemap.get(this.types.get(groupPosition)).size();
		p("Nr children for group " + groupPosition + ":" + nr);
		return nr;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this.types.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		int nr = this.types.size();
		// p("getGroupCount "+nr);
		return nr;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);
		// p("Getting group view "+convertView);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	protected void p(String s) {
		Log.i("ExpandablePortList", s);
	}

}
