package com.auto.android.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.model.Contact;
import com.auto.model.Group;
import com.auto.model.User;

public class ExpandableContactListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<String> types; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<Contact>> typemap;

	public ExpandableContactListAdapter(Context context) {
		this._context = context;
		this.types = new ArrayList<String>();
		this.typemap = new HashMap<String, List<Contact>>();

	}

	public void clear() {
		types.clear();
		typemap.clear();
	}

	public List<Contact> get(String type) {
		return typemap.get(type);
	}

	public int getNr(String type) {
		List<Contact> plist = get(type);
		if (plist == null) {
			return 0;
		} else
			return plist.size();
	}

	public void add(Contact child) {
		String type = child.getType();
		if (child instanceof Group) {
			type = type+" groups";
		}
		type = Character.toUpperCase(type.charAt(0)) + type.substring(1);
		if (!types.contains(type)) {
			types.add(type);
			
		}
		List<Contact> plist = get(type);
		if (plist == null) {
			plist = new ArrayList<Contact>();
			typemap.put(type, plist);
		}
		if (!plist.contains(child))
			plist.add(child);
		//p("added type " + type);
		this.notifyDataSetChanged();

	}

	public void remove(Contact child) {
		String type = child.getType();
		List<Contact> plist = get(type);
		if (plist == null) {
			plist = new ArrayList<Contact>();
			typemap.put(type, plist);
		}
		plist.remove(child);
		this.notifyDataSetChanged();

	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this.typemap.get(this.types.get(groupPosition)).get(
				childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View row, ViewGroup parent) {

		final Contact contact = (Contact) getChild(groupPosition, childPosition);

		if (row == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = infalInflater.inflate(R.layout.user_li, null);
		}
		ImageView image = (ImageView) row.findViewById(R.id.icon);
		// image.setBackgroundDrawable(AndroidGui.getDrawable(user));
		image.setImageResource(AndroidGui.getDrawable(contact));
		TextView txtmsg = (TextView) row.findViewById(R.id.lblname);
		
		
		ImageView flagimage = (ImageView) row.findViewById(R.id.flagicon);
		// image.setBackgroundDrawable(AndroidGui.getDrawable(user));
		int res = AndroidGui.getFlagImage(contact);
		if (res >0) {
			flagimage.setImageResource(res);
		}
		else flagimage.setVisibility(View.GONE);
		
		String name = contact.getName()+" ";
		if (contact.hasPorts()) {
			name+="("+contact.getPorts().size()+" gates)";
			row.setBackgroundColor(Color.WHITE);
		}
		else {
			name+="(no visible gates)";
			row.setBackgroundColor(Color.LTGRAY);			
		}
		
		txtmsg.setText(name);

		TextView txtinfo = (TextView) row.findViewById(R.id.lbldetail);
		if (contact instanceof User) {	
			User user = (User)contact;
			String email = user.getEmail();
			if (user.isSystem())
				email = "System user";
			
			if (email != null) {				
				txtinfo.setText(email);
			}
			else txtinfo.setText("no email");
		}
		else {
			Group group = (Group)contact;
			if (group.getUsers() == null) {
				txtinfo.setText("No users in group");
			}
			else {
				txtinfo.setText(group.getUsers().toString());
			}
		}
		return row;
	}

	@Override
	public int getChildrenCount(int groupPosition) {

		int nr = this.typemap.get(this.types.get(groupPosition)).size();
		p("Nr children for group " + groupPosition + ":" + nr);
		return nr;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this.types.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		int nr = this.types.size();
		// p("getGroupCount "+nr);
		return nr;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);
		// p("Getting group view "+convertView);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	protected void p(String s) {
		Log.i("ExpandableContactList", s);
	}

}
