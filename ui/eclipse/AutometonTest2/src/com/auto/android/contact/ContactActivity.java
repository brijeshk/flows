package com.auto.android.contact;

import android.app.FragmentManager.OnBackStackChangedListener;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.auto.android.AutometonActivity;
import com.auto.android.R;
import com.auto.android.atoms.AtomlistFragment;
import com.auto.android.contact.UserlistFragment.OnUserFragmentListener;
import com.auto.android.data.DataFragment;
import com.auto.android.data.DataFragment.OnDataFragmentListener;
import com.auto.android.flows.FlowActivity;
import com.auto.android.flows.FlowlistFragment;
import com.auto.android.port.PortlistFragment;
import com.auto.android.state.StateCanvasFragment;
import com.auto.android.state.StateCanvasFragment.OnStateFragmentListener;
import com.auto.android.utils.UiUtils;
import com.auto.model.Contact;
import com.auto.model.ContactFilter;
import com.auto.model.FlowFilter;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;

public class ContactActivity extends AutometonActivity implements
		OnUserFragmentListener, PortlistFragment.OnPortFragmentListener,
		OnStateFragmentListener, OnDataFragmentListener,
		OnBackStackChangedListener {

	UserlistFragment contactFragment;
	PortlistFragment portlistFragment;
	StateCanvasFragment statecanvasFragment;
	DataFragment dataFragment;

	ContactImporter contactimporterFragment;
	private ContactFilter filter;
	private String search = "chantal";
	int fragCount = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		p("Creating ContactActivity with options menu " + options_menu);
		this.options_menu = R.menu.contact_activity_actions;
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.contact_activity);

		// Check that the activity is using the layout version with
		// the fragment_container FrameLayout
		if (findViewById(R.id.contact_fragment_container) != null) {
			if (savedInstanceState != null) {
				p("got a savedInstanceState, not adding fragment again");
				return;
			}
			contactFragment = new UserlistFragment();
			contactFragment.setArguments(getIntent().getExtras());
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			getFragmentManager().addOnBackStackChangedListener(this);
			fragCount = 0;
			transaction.add(R.id.contact_fragment_container, contactFragment)
					.commit();

		}
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			Contact user = (Contact) extras.get("USER");
			if (user != null) {
				p("Got user " + user);
				onUserSelected(user);
			}
		}
		// if users has no contacts yet, start importer
		if (!man.hasRealContacts() ) {
			UiUtils.msg(this, "No contacts yet", "To get started, pick at least one contact");
			p("user has no contacts, starting importer");
			this.showContactImporterFragment();
		}
	}
	

	protected void homeClicked() {
		 p("Home pressed. Show contact act");
	     this.startActivity(new Intent(this, ContactActivity.class)); 
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (super.onOptionsItemSelected(item))
			return true;
		int id = item.getItemId();
		if (id == R.id.action_find) {
			msg("Find contact");
			// don't save just yet
			showContactImporterFragment();
			return true;
		}
		return false;
	}

	protected void newObjectAction() {
		p("User picks new action");
		if (portlistFragment != null && this.portlistFragment.isVisible()) {
			p("create new port");
			portlistFragment.createPort(null);
		}
		else {
			showContactImporterFragment();
		}
		
	}
	@Override
	protected void searchAction(String txt) {
		p("searchAction: with search string " + txt + " NOT DONE");
		if (txt != null) {
			// find messages
			if (!this.menuSearch.isVisible()) {
				showSearch();
			}
			p("Finding messages containing " + txt);
			filter = new ContactFilter();
			filter.setSearchstring(txt);
			this.contactFragment.setFilter(filter);
			showContactFragment();

		}
	}

	private void showContactFragment() {

		if (contactFragment.isVisible())
			return;
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.contact_fragment_container, contactFragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}

	private void showContactImporterFragment() {

		if (contactimporterFragment == null)
			contactimporterFragment = new ContactImporter();
		msg("Finding contacts with name " + search);
		contactimporterFragment.setSearchString(search);

		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.contact_fragment_container,
				contactimporterFragment);
		transaction.addToBackStack(null);
		transaction.commit();
		
	}

	@Override
	public void onUserSelected(Contact user) {
		p("Got user or group from user fragment: " + user);
		fragCount = 1;
		portlistFragment = null;
		if (portlistFragment != null) {
			p("Already have portlistFragment, updating view");
			portlistFragment.updateView(user);
		} else {
			portlistFragment = new PortlistFragment(user);
			Bundle args = new Bundle();
			args.putSerializable(AtomlistFragment.ARG_PARAM1, user);
			portlistFragment.setArguments(args);
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			transaction.replace(R.id.contact_fragment_container,
					portlistFragment);
			transaction.addToBackStack("port");
			transaction.commit();
		}
	}

	@Override
	public void onPortSelected(Port port) {
		fragCount = 2;
		p("User clicked on port from portlistfragment. " + port);
		// this.startActivity(new Intent(this, ExposeActivity.class));
		statecanvasFragment = null;
		if (statecanvasFragment != null) {
			p("Already have statecanvasFragment, updating view");
			statecanvasFragment.updateView(port);
		} else {
			p("Creating a new StateCanvasFragment, adding port " + port);
			statecanvasFragment = new StateCanvasFragment(port);
			Bundle args = new Bundle();
			args.putSerializable(StateCanvasFragment.ARG_PARAM1, port);
			statecanvasFragment.setArguments(args);
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			// transaction.add
			transaction.replace(R.id.contact_fragment_container,
					statecanvasFragment);
			transaction.addToBackStack("state");
			transaction.commit();
		}
	}

	@Override
	public void onStateSelected(State state) {
		p("User clicked on state from statecanvasfragment. " + state);
		fragCount = 3;
		dataFragment = null;
		if (dataFragment != null) {
			p("Already have dataFragment, updating view");
			dataFragment.updateView(state);
		} else {
			p("Creating a new dataFragment");
			dataFragment = new DataFragment(state);
			Bundle args = new Bundle();
			args.putSerializable(DataFragment.ARG_PARAM1, state);
			dataFragment.setArguments(args);
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			transaction.replace(R.id.contact_fragment_container, dataFragment);
			transaction.addToBackStack("data");
			transaction.commit();
		}
	}

	@Override
	public void onDataSelected(InputData proc) {
		p("User selected data item " + proc);
		msg("You selected: " + proc);

	}

	@Override
	public void onBackStackChanged() {
		int backStackEntryCount = getFragmentManager().getBackStackEntryCount();
		p("Got backStackEntryCount=" + backStackEntryCount + ", fragCount= "
				+ fragCount);
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		if (fragCount == 3 && backStackEntryCount > 3) {
			p("putting state canvas back");
			transaction.remove(dataFragment).add(
					R.id.contact_fragment_container, statecanvasFragment);
		} else if (fragCount == 2 && backStackEntryCount > 2) {
			p("putting port frag back");
			transaction.remove(statecanvasFragment).add(
					R.id.contact_fragment_container, portlistFragment);
		} else if (fragCount == 1 && backStackEntryCount > 1) {
			p("putting user frag back");
			transaction.remove(portlistFragment).add(
					R.id.contact_fragment_container, contactFragment);
		}
		transaction.commit();
		fragCount--;

	}

}