package com.auto.android.contact;

import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Intents;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.android.discover.DiscoverActivity;
import com.auto.android.flows.FlowActivity;
import com.auto.android.prefs.PrefUtil;
import com.auto.android.utils.UiUtils;
import com.auto.model.Contact;
import com.auto.model.ContactFilter;
import com.auto.model.ContactNameComparator;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import com.auto.model.Group;
import com.auto.model.GroupNameComparator;
import com.auto.model.User;


/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class UserlistFragment extends Fragment  implements
	AdapterView.OnItemLongClickListener {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	private static FlowManager man;
	private AutometonApp app;
	private PrefUtil prefUtil;
	
	private OnUserFragmentListener mListener;

	//private UserArrayAdapter adapter;
	private ContactFilter filter;
	private ExpandableListView listView;
	private ExpandableContactListAdapter adapter;
	private TextView headerView;
	
	public static UserlistFragment newInstance() {
		
		UserlistFragment fragment = new UserlistFragment();
		Bundle args = new Bundle();
		return fragment;
	}


	public UserlistFragment() {
	
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		prefUtil = new PrefUtil(activity);
		try {
			mListener = (OnUserFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
		//this.activity = activity;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("2. onCreate");		
		app = (AutometonApp)this.getActivity().getApplication();
		man = app.getManager();
		
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		p("3. onCreateView");
		View view = inflater.inflate(R.layout.contact_fragment, null);
		headerView = (TextView) view.findViewById(R.id.contactheader);
	    updateHeader();
		listView = (ExpandableListView) view.findViewById(android.R.id.list);
		registerForContextMenu(listView);
	    return view;
	}


	private void updateHeader() {
		p("update title");
		if (headerView == null) {
			return;
		}
			
		
	    String title = "Contacts";
	    if (filter != null ) {
			title += " filtered by ";
			title += filter.getType();			
		}
		headerView.setText(title);
	}
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		p("4. onActivityCreated: adding data");
		addItems();
		
	}
	
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		p("Long item click");
		return false; // let the system show the context menu
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		p("onCreateContextMenu");
		if (v.getId() == android.R.id.list) {
			p("Context menu on user item");
			 ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo)menuInfo;
			    View target = info.targetView;
			    p("Target view "+target);
			    p("Packed pos="+info);
			    int group = this.listView.getPackedPositionGroup(info.packedPosition);
			    int child = this.listView.getPackedPositionChild(info.packedPosition);
			    Contact user = (Contact) adapter.getChild(group, child);
			    if (user == null) menu.setHeaderTitle("Action on contact");
			    else  menu.setHeaderTitle("Action on "+user.getName());
			String[] menuItems = getResources().getStringArray(
					R.array.usermenulist);
			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		} else
			p("Context menu not on list: " + v);

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		p("User picked menu " + item);
		String t = item.getTitle().toString().toLowerCase();

		ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item
				.getMenuInfo();
		View target = info.targetView;
		p("Target view " + target);
		p("Packed pos=" + info);
		int group = this.listView.getPackedPositionGroup(info.packedPosition);
		int child = this.listView.getPackedPositionChild(info.packedPosition);

		User user = (User) adapter.getChild(group, child);
		if (t.startsWith("new message")) {
			msg("create new message");
			Intent intent = new Intent(this.getActivity(), FlowActivity.class);
			intent.putExtra("ACTION", "CREATE");
			intent.putExtra("USER_ID", user.getId());
			this.startActivity(intent);
		}
		else if (t.startsWith("star")) {
			msg("star user " + user.getName());
		}
		else if (t.startsWith("invite")) {
			this.inviteUser(user);
		}
		else if (t.startsWith("create contact")) {
				createContact(user);
		} else if (t.startsWith("edit")) {
			msg("edit user " + user.getName());
			
			AndroidContact ac = findContact(user);
			 if (ac == null) {
				 msg("Could not find this user. Creating new contact");
				 createContact(user);
			 }
			 else {
			 // must first find user in list
				 Intent editIntent = new Intent(Intent.ACTION_EDIT);					
				 editIntent.setDataAndType(ac.getUri(),Contacts.CONTENT_ITEM_TYPE);
				 startActivity(editIntent);
			 }
		} else if (t.startsWith("delete")) {
			msg("delete user " + user.getName());			
		}
		return true;

	}
	private void createContact(User user) {
		this.inviteUser(user);		
		Intent intent = new Intent(Intents.Insert.ACTION);
		// Sets the MIME type to match the Contacts Provider
		intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
		intent.putExtra(Intents.Insert.EMAIL, user.getEmail());
		intent.putExtra(Intents.Insert.PHONE, user.getPhone());
		startActivity(intent);
	}
	public AndroidContact findContact(User autometonuser)  {
		ContentResolver cr = getActivity().getContentResolver(); // Activity/Application
		// android.content.Context
		Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		
		p("Finding user "+autometonuser.getPhone()+"/"+autometonuser.getEmail()+"in " + cursor.getCount()+" android users");
		if (cursor.moveToFirst()) {
			int mLookupKeyIndex = cursor.getColumnIndex(Contacts.LOOKUP_KEY);
			    // Gets the lookup key value
			String    mCurrentLookupKey = cursor.getString(mLookupKeyIndex);
			do {
				String id = cursor.getString(cursor
						.getColumnIndex(ContactsContract.Contacts._ID));

				Cursor pCur = cr.query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ " = ?", new String[] { id }, null);
				long mCurrentId = Long.parseLong(id);
				Uri mSelectedContactUri =
			            Contacts.getLookupUri(mCurrentId, mCurrentLookupKey);
				while (pCur.moveToNext()) {
					String phone = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					String contactid = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
					String name = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Identity.DISPLAY_NAME));
					String email = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));

					p("Got id " + contactid + "/"+mCurrentId+", "+ name + ":" + phone+", email "+email);
					AndroidContact candroid = new AndroidContact();
					candroid.setAndroidId(id);
					candroid.setUri(mSelectedContactUri);
					candroid.setName(name);
					candroid.setPhone(phone);
					candroid.setEmail(email);
					
					if ( (phone!= null && phone.length()>5) || (email != null && email.contains("@"))) {
						
							if (name != null && name.length() > 0) {
								if (name.equalsIgnoreCase(autometonuser.getName())) {
									p("same name: " + name+"/"+autometonuser);
									return candroid;						
								}
							}
							if (phone != null && autometonuser.getPhone() != null
									&& autometonuser.getPhone().length() > 5) {
								if (phone.indexOf(autometonuser.getPhone()) > -1
										|| autometonuser.getPhone().indexOf(phone) > -1) {
									p("same phone: " + phone+"/"+autometonuser);
									return candroid;	
									
								}
							}
							if (email != null && autometonuser.hasEmail()) {
								if (email.equalsIgnoreCase(autometonuser.getEmail())) {
									p("same email: " + email+"="+autometonuser.getEmail());
									p("android user is: "+candroid);
									return candroid;	
								}
							}							
						
					}
					break;
				}
				pCur.close();

			} while (cursor.moveToNext());		
		}
		return null;
	}

	public void inviteUser(final User user) {
		msg("Inviting user " + user.getName());
		
		if (user.hasEmail() && user.hasPhone()) {
			UiUtils.pick(this.getActivity(), user.getName()+" has a phone and email. How do you prefer to send the invite?", 
					"Via Email, Via SMS, Cancel",
					new UiUtils.AnswerPicked() {						
						@Override
						public void answerPicked(int which) {
							if (which < 0 || which >1) {
								p("user cancelled");
							}
							else if (which ==0  ){
								inviteViaEmail(user);
							}
							else {
								inviteViaSms(user);
							}							
						}					
					});
		}
		else if (user.hasEmail()) {
			UiUtils.yesOrNo(this.getActivity(), "Email", user.getName()+" has an email address (no phone nr). Would you like me send an Email invite?",
					new UiUtils.YesAnswered() {
						@Override
						public void yesAnswered(boolean yes) {
							if (yes) {
								inviteViaEmail(user);
							}							
						}					
					});
		}
		else if (user.hasPhone()) {
			UiUtils.yesOrNo(this.getActivity(), "SMS", user.getName()+" has a phone number (no email). Would you like me send an SMS invite?",
					new UiUtils.YesAnswered() {
						@Override
						public void yesAnswered(boolean yes) {
							if (yes) {
								inviteViaSms(user);
							}							
						}					
					});
		}
		else {
			// no phone or email
			UiUtils.msg(this.getActivity(), "No contact info", user.getName()+" has no email or phone info. I don't know how to contact this person");
		}
	}
	private void inviteViaEmail(User user) {
		Intent intent = new Intent(Intent.ACTION_SEND); 
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, user.getEmail());
		intent.putExtra(Intent.EXTRA_SUBJECT, "AutoMeton");
		String msg = "Dear "+user.getName()+",\n";
		msg +="I'd like to chat with you with AutoMeton:\n"
				+ "Homepage: <a href=\""+prefUtil.getHomepage()+"\"</a>AutoMeton\n"
				+ "App link: <a href=\""+prefUtil.getAppLink()+"\"</a>AutoMeton App\n\nBest wishes,\n"
				+man.getMe().getName();
		intent.putExtra(Intent.EXTRA_TEXT, msg);
		startActivity(Intent.createChooser(intent, "Send Email"));
		this.startActivity(intent);		
	}
	private void inviteViaSms(User user) {
		Intent sendIntent = new Intent(Intent.ACTION_VIEW); 
		sendIntent.setType("vnd.android-dir/mms-sms");
		sendIntent.putExtra("sms_body", "Let's chat on AutoMeton!\n"
				+ "Homepage: "+prefUtil.getHomepage()+"\n"
				+ "App: "+prefUtil.getAppLink()+"\n"
				+"Best wishes, "+man.getMe().getName());
		sendIntent.putExtra("address", user.getPhone());		
		this.startActivity(sendIntent);
		
	}
	private void addItems() {
		p("============ ADD ITEMS ============");
		adapter = new ExpandableContactListAdapter(getActivity());
		p("adding groups");
		if (filter != null) p("got filter "+filter);
		List<Group> groups = man.getGroups();
		// sort groups
		Collections.sort(groups, new GroupNameComparator());
		if (groups != null) {
			for (Group g: groups) {
				if (filter == null || filter.filter(g)) {
				p("adding group "+g);
				adapter.add(g);
				}
			}
		}
		p("Adding all users" );
		List<User> users = man.getUsers();
		Collections.sort(users, new ContactNameComparator());
		for (User u: users) {
			if (!u.isTemplates()) {
				if (filter == null || filter.filter(u)) {
				//p(" adding user "+u);
					adapter.add(u);
				}
			}
		}
		
		listView.setAdapter(adapter);
		for (int i = 0; i < adapter.getGroupCount(); i++){
			listView.expandGroup(i);
		}
		adapter.notifyDataSetChanged();
		//p("after adding items: "+adapter.getGroupCount()+", counting children: "+adapter.getChildrenCount(0));
		
		
		listView.setOnChildClickListener(new OnChildClickListener(){

			@Override
			public boolean onChildClick(ExpandableListView listView, View v,
					int group, int pos, long id) {
				p("Got group "+group+", pos "+pos+", id"+id);
				if (group >=0 && pos >=0) {
					Contact user = (Contact) adapter.getChild(group, pos);
					if (null != mListener) {
						p("onChildClick user :"+user);
						mListener.onUserSelected(user);
					}
				}
				return false;
			}});
	}
	

	@Override
	public void onDetach() {
		super.onDetach();
		
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnUserFragmentListener {
		// 
		public void onUserSelected(Contact user);
	}
	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(this.getActivity(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG) p("msg: " + s);
	}


	public void setFilter(ContactFilter filter) {
		this.filter = filter;
		if (this.adapter != null) {
			this.addItems();
			updateHeader();
		}
	}

	

}
