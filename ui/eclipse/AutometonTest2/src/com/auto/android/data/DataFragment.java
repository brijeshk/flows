package com.auto.android.data;

import java.io.Serializable;
import java.util.List;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AndroidGui;
import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.State;


/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class DataFragment extends ListFragment {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	public static final String ARG_PARAM1 = "state";
	private static FlowManager man;
	private AutometonApp app;
	
	private OnDataFragmentListener mListener;

	private DataArrayAdapter adapter;
	private Activity activity;
	private State state;
	private InputData proc;
	
	public static DataFragment newInstance(State state) {
		
		DataFragment fragment = new DataFragment(state);
		Bundle args = new Bundle();
		args.putSerializable(ARG_PARAM1, (Serializable)state);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public DataFragment(State state) {
		this.state = state;
	}
	public DataFragment() {
	
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnDataFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnDataFragmentListener");
		}
		this.activity = activity;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("2. onCreate");
		
		if (getArguments() != null) {
			state = (State)getArguments().getSerializable(ARG_PARAM1);
			p("Got arguments. state is "+state);
		}
		else p("Got no arguments");
		app = (AutometonApp)this.getActivity().getApplication();
		man = app.getManager();				
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		p("3. onCreateView");
		adapter = new DataArrayAdapter(getActivity(), R.layout.data_li);		
		addItems();
	    View view = inflater.inflate(R.layout.data_fragment, null);
	    TextView headerView = (TextView) view.findViewById(R.id.dataheader);
	    TextView txtdesc = (TextView) view.findViewById(R.id.lbldesc);
	    TextView txtactor = (TextView) view.findViewById(R.id.lblactor);
	    TextView txttype = (TextView) view.findViewById(R.id.lbltype);
	    TextView txtdata = (TextView) view.findViewById(R.id.lbldata);
	    
	    
	    ImageView imageView = (ImageView) view.findViewById(R.id.stateimage);
	    imageView.setImageResource(AndroidGui.getDrawable(state));
	    p("Udating state view with: "+state);
	    if (state != null) {
	    	p("state, data"+state.getData());
	    	headerView.setText("Action "+state.getName());
	    }
	    else p("Got no state");
		
		String who = "anyone";
		if (state != null) {
			if (state.actorIsFlowOwner())  who = "flow owner";
			else if (state.actorIsPortOwner())  who = "port owner";
			else if (state.actorIsToggle())  who = "not prev sender";
			List<State> next = state.getNextStates();
			
			txttype.setText((next != null ? next.toString() : "none"));
			txtdesc.setText((state.getDescription() != null && state.getDescription().length()>0 ? state.getDescription() : "no description"));
			txtdata.setText(( state.hasData() ? state.getData() : "no data"));
		}
		
		txtactor.setText(who);
		
	    return view;
	}
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		p("4. onActivityCreated:data array adapter");
		ListView listView = (ListView)this.getListView();	
		listView.setAdapter(adapter);	
	}
	
	public void updateView(State state) {
		p("updateView with state "+state);
		if (adapter != null) {
			adapter.clear();
		}
		addItems();
		
	}
	private void addItems() {
		p("Adding data for state "+state);
		if (state == null) {
			p("Got no state");
			return;
		}
		if (state.getInputDatas()!= null) {
			for (InputData p: state.getInputDatas()) {
				adapter.add(p);
			}
		}
		else p("State has no data");
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
	}
	@Override
	public void onDetach() {
		super.onDetach();		
		mListener = null;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		if (null != mListener) {
			// Notify the active callbacks interface (the activity, if the
			// fragment is attached to one) that an item has been selected.
			InputData proc = state.getInputDatas().get(position);
			p("-------- onListItemClick proc :"+proc);
			if (proc != null) mListener.onDataSelected(proc);
		}
	}

	
	public interface OnDataFragmentListener {		
		public void onDataSelected(InputData proc);
	}
	
	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(this.getActivity(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG) p("msg: " + s);
	}

	

}
