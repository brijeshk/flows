package com.auto.android.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.auto.android.R;
import com.auto.model.FlowManager;
import com.auto.model.InputData;

public class DataArrayAdapter extends ArrayAdapter<InputData> {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	
	
	private TextView txtmsg;
	private TextView txttype;
	private TextView txtvalues;
	private TextView lblvalues;
	private TextView txtinfo;
	private List<InputData> datas = new ArrayList<InputData>();

	private FlowManager man;
	
	@Override
	public void add(InputData object) {
		man = FlowManager.getManager();
		datas.add(object);
		super.add(object);
	}

	public DataArrayAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	public int getCount() {
		return this.datas.size();
	}

	public InputData getItem(int index) {
		return this.datas.get(index);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.data_li, parent, false);
		}
		InputData proc = getItem(position);
		
		txtmsg = (TextView) row.findViewById(R.id.dataname);
		txttype = (TextView) row.findViewById(R.id.datatype);
		txtvalues = (TextView) row.findViewById(R.id.datavalues);
		String typename = proc.getType().getLabel();
		
		txtmsg.setText(proc.getName());
		txttype.setText(typename);
							
		txtinfo = (TextView) row.findViewById(R.id.datainfo);
		if (proc.getValues() != null) {
			txtvalues.setText( proc.getValues().toString());
		}
		else {
			txtvalues.setVisibility(View.GONE);
			lblvalues = (TextView) row.findViewById(R.id.lblvalues);
			lblvalues.setVisibility(View.GONE);
		}
		
		
		if (proc.getInputDatas() != null) {
			txtinfo.setText("Sub data elements: "+ proc.getInputDatas().size()+" subitems");
		}
		else  {
			p("proc has no data: "+proc);
			txtinfo.setVisibility(View.GONE);
		}
				
		return row;
	}

	
	protected void p(String s) {
		Log.i(TAG, s);
	}

}