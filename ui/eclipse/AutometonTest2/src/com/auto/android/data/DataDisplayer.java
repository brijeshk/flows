package com.auto.android.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jivesoftware.smack.util.Base64;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.InputType;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.auto.android.R;
import com.auto.model.DataType;
import com.auto.model.DataValue;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import com.auto.model.InputData;

public class DataDisplayer {

	private String TAG = getClass().getName();
	private boolean DEBUG = true;
	private static final String NAME_KEY = "NAME";
	/** OS RELATED */
	private static final int PICK_IMAGE = 1;

	private Activity context;
	private AlertDialog dialog;
	private AlertDialog caldialog;
	private int listbgcolor = -1;
	private EditText datatextinput;
	private int timesCalled = 1;// for bug in date picker!
	/** for entering data */
	private List<DataValue> currentvalues;
	private InputData currentdata;
	private Flow flow;
	private FlowManager man;
	private boolean[] curselection;
	public DataDisplayer(Flow flow, Activity context) {
		this.context = context;
		this.flow = flow;
		this.man = FlowManager.getManager();
	}

	public void showData(InputData nextdata, DialogListener listener) {
		if (nextdata == null) {
			p("no data");
			return;
		}
		p("=========== showData: " + nextdata);
		if (nextdata.isHidden() && flow != null && !man.isMe(this.flow.getCreator())) {
			p("Data is hidden, don't show");
		}
		this.currentdata = nextdata.clone();

		currentvalues = currentdata.getValues();
		if (currentvalues.size() > 1) {
			showDialog(createValuesView(currentdata.getValues()), null,
					listener, false);

		} else {
			DataValue dv = currentdata.getValue();
			DataValue val = new DataValue(currentdata.getName());
			if (dv != null)
				val.setValue(dv.getValue());
			showTextDialog(val, currentdata.getType(),
					currentdata.getDescription(), listener);

		}

	}
	
	private void showDialog(final SimpleAdapter adapter, List selectedItems,
			final DialogListener listener, final boolean allowMultiple) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		p("Creating dialog with " + adapter.getCount() + " items");
		final ListView lv = new ListView(context);
		lv.setAdapter(adapter);
		curselection = new boolean[adapter.getCount()];
		builder.setView(lv);
		if (selectedItems != null && !selectedItems.isEmpty()) {
			for (Object select : selectedItems) {

				for (int pos = 0; pos < adapter.getCount(); pos++) {
					Object obj = extracItem(adapter.getItem(pos));
					if (obj != null && obj.equals(select)) {
						lv.setItemChecked(pos, true);
						curselection[pos] = true;
					}
				}
			}
		}
		if (allowMultiple)
			lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		else
			lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> av, View v, int pos,
					long arg3) {
				
				p("===================== setOnItemClickListener");
				ColorDrawable d;
				if (listbgcolor < 0) {
					Drawable bg = v.getBackground();
					if (bg != null && bg instanceof ColorDrawable) {
						listbgcolor = ((ColorDrawable) bg).getColor();
					} else
						listbgcolor = Color.TRANSPARENT;
				}
				curselection[pos] = !curselection[pos];

				p("user clicked. Curselection "+pos+" is "+curselection[pos]+", change color.  v is " + v);
				
				
				if (curselection[pos]) {
					v.setBackgroundColor(Color.LTGRAY);
					p("Changing to gray");
				} else {
					v.setBackgroundColor(listbgcolor);
					p("Changing to default");
				}				
			}

		});

		
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface d, int arg1) {
				SparseBooleanArray ba = lv.getCheckedItemPositions();

				
					ArrayList items = new ArrayList();
					for (int i = 0; i < curselection.length; i++) {
						if (curselection[i]) {
							p("Got checked item: " + i);
							Object checkedItem = extracItem(adapter.getItem(i));
							items.add(checkedItem);
						}
					}
					listener.ok(items);
			}

		});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface d, int arg1) {
						 d.dismiss();
						 listener.cancel();
					}

				});
		dialog = builder.create();
		dialog.show();
	}
	private void showTextDialog(final DataValue dv, DataType type,
			String message, final DialogListener listener) {

		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle(dv.getName());
		alert.setMessage(message);

		datatextinput = new EditText(context);

		if (dv.getValue() != null) {
			datatextinput.setText(dv.getValue());
		}
		if (type.isUrl()) {
			datatextinput.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
		} else if (type.isQuantity()) {
			datatextinput.setInputType(InputType.TYPE_CLASS_NUMBER);
			final NumberPicker np = new NumberPicker(context);
			np.setMinValue(0);
			np.setWrapSelectorWheel(true);
			datatextinput = null;
			alert.setView(np);
			int nr = 1;
			if (dv.getValue() != null) {
				try {
					nr = Integer.parseInt(dv.getValue());
				} catch (Exception e) {
				}
			}
			np.setValue(nr);
			alert.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							String value = "" + np.getValue();
							dv.setValue(value);
							if (listener != null)
								listener.ok(dv);
						}
					});

		} else if (type.isPrice()) {
			datatextinput.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);
		} else if (type.isDate()) {
			// input.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
			Calendar cal = Calendar.getInstance();
			int day = cal.get(Calendar.DAY_OF_MONTH);
			int month = cal.get(Calendar.MONTH);
			int year = cal.get(Calendar.YEAR);
			timesCalled = 1;
			DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
				public void onDateSet(DatePicker view, int selectedYear,
						int selectedMonth, int selectedDay) {
					if (view.isShown()) {
						timesCalled += 1;
						if ((timesCalled % 2) == 0) {
							String ans = selectedDay + "/"
									+ (selectedMonth + 1) + "/" + selectedYear;
							dv.setValue(ans);
							caldialog.dismiss();
							if (listener != null)
								listener.ok(dv);
						}
					}
				}
			};
			caldialog = new DatePickerDialog(context, datePickerListener, year,
					month, day);
			caldialog.show();
			return;
		} else if (type.isImage()) {
			p("Got an image");
			datatextinput = null;
			ImageView view = new ImageView(context);
			byte[] imgbytes = Base64.decode(dv.getValue());
			Bitmap bmp = BitmapFactory.decodeByteArray(imgbytes, 0, imgbytes.length);
			view.setImageBitmap(bmp);
			alert.setView(view);
			alert.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							
							if (listener != null)
								listener.ok(dv);
						}
					});
		}

		if (datatextinput != null) {
			// Set an EditText view to get user input
			alert.setView(datatextinput);
			alert.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							String value = datatextinput.getText().toString();
							dv.setValue(value);
							if (listener != null)
								listener.ok(dv);
						}
					});
		}
		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						if (listener != null)
							listener.cancel();
					}
				});

		alert.show();

	}

	private Map<String, ?> createMap(DataValue u) {
		return createRow(u.getValue(), u);
	}

	private Map<String, Object> createRow(String value1, Object obj) {
		Map<String, Object> row = new HashMap<String, Object>();
		row.put(NAME_KEY, value1);
		row.put("obj", obj);
		return row;
	}

	public interface DialogListener {
		public void ok(Object val);
		
		public void cancel();
	}

	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
		if (DEBUG)
			p("msg: " + s);
	}

	private SimpleAdapter createValuesView(List<DataValue> values) {
		ArrayList listdata = new ArrayList<Map<String, ?>>();
		String[] from = { NAME_KEY, };
		int[] to = { R.id.lblname, };
		for (DataValue u : values) {
			listdata.add(createMap(u));
		}
		SimpleAdapter simpleadapter = new SimpleAdapter(context, listdata,
				R.layout.listitem_text, from, to);
		return simpleadapter;
	}

	private Object extracItem(Object checkedItem) {
		Map selection = (Map) checkedItem;
		if (!selection.containsKey(NAME_KEY)) {
			p("nothing clicked that I know: " + selection);
			return null;
		}
		String name = (String) selection.get(NAME_KEY);
		Object obj = selection.get("obj");
		p("User selected object with name " + name + ":" + obj);
		return obj;
	}
}
