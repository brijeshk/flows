package com.auto.android.data;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.R;
import com.auto.model.InputData;

public class SubDataView {

	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;

	Context context;
	View parentView;
	int count = 0;
	TableLayout tl;
	
	public SubDataView(Context context, View parentView) {
		this.context = context;
		this.parentView = parentView;
		init();
	}
		
	private void init() {
		
		tl = (TableLayout) parentView.findViewById(R.id.main_table);
		// add header
		TableRow tr_head = new TableRow(context);
		tr_head.setId(10);
		tr_head.setBackgroundColor(Color.GRAY);
		tr_head.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		
		// add header cols
		TextView label_date = new TextView(context);
        label_date.setId(20);
        label_date.setText("Name");
        label_date.setTextColor(Color.WHITE);
        label_date.setPadding(5, 5, 5, 5);
        tr_head.addView(label_date);// add the column to the table row here

        TextView label_weight_kg = new TextView(context);
        label_weight_kg.setId(21);// define id that must be unique
        label_weight_kg.setText("Value)"); // set the text for the header 
        label_weight_kg.setTextColor(Color.WHITE); // set the color
        label_weight_kg.setPadding(5, 5, 5, 5); // set the padding (if required)
        tr_head.addView(label_weight_kg); // add the column to the table row here
        
        tl.addView(tr_head, new TableLayout.LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));
	}

	public void addProc(InputData proc) {
		TableRow tr = new TableRow(context);
		if(count%2!=0) tr.setBackgroundColor(Color.GRAY);
		tr.setId(100+count);
		tr.setLayoutParams(new LayoutParams(
		LayoutParams.FILL_PARENT,
		LayoutParams.WRAP_CONTENT));

		//Create two columns to add as table data
		 // Create a TextView to add date
		TextView labelDATE = new TextView(context);
		labelDATE.setId(200+count); 
		labelDATE.setText(proc.getName());
		labelDATE.setPadding(2, 0, 5, 0);
		labelDATE.setTextColor(Color.WHITE);
		tr.addView(labelDATE);
		TextView labelWEIGHT = new TextView(context);
		labelWEIGHT.setId(200+count);
		labelWEIGHT.setText(proc.toString());
		labelWEIGHT.setTextColor(Color.WHITE);
		tr.addView(labelWEIGHT);

		// finally add this to the table row
		tl.addView(tr, new TableLayout.LayoutParams(
			                    LayoutParams.FILL_PARENT,
			                    LayoutParams.WRAP_CONTENT));
		count++;
				    
	}
	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
		if (DEBUG)
			p("msg: " + s);
	}

	
}
