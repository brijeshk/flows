package com.auto.android.data;

import java.io.Serializable;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.model.FlowManager;
import com.auto.model.InputData;


/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class SubDataFragment extends Fragment {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	public static final String ARG_PARAM1 = "proc";
	private static FlowManager man;
	private AutometonApp app;
	
	private Activity activity;
	private InputData proc;
	private SubDataView sub;
	
	public static SubDataFragment newInstance(InputData proc) {
		
		SubDataFragment fragment = new SubDataFragment(proc);
		Bundle args = new Bundle();
		args.putSerializable(ARG_PARAM1, (Serializable)proc);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public SubDataFragment(InputData proc) {
		this.proc = proc;
	}
	public SubDataFragment() {
	
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);		
		this.activity = activity;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("2. onCreate");
		if (getArguments() != null) {
			proc = (InputData)getArguments().getSerializable(ARG_PARAM1);			
		}
		app = (AutometonApp)this.getActivity().getApplication();
		man = app.getManager();				
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		p("3. onCreateView");
		
	    View view = inflater.inflate(R.layout.subdata_table, null);
	    TextView headerView = (TextView) view.findViewById(R.id.dataheader);
	    
	    p("Udating header view with proc : "+proc);
		headerView.setText("Data "+proc.getName());
    
		sub = new SubDataView(activity, view);
		updateView(proc);
	    return view;
	}
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		p("4. onActivityCreated: set up view");
		
	}
	
	public void updateView(InputData inputdata) {
		p("TODO test updateView with inputdata "+inputdata);
		if (inputdata.hasSubData()) {
			for (InputData p: proc.getInputDatas()) {
				sub.addProc(p);
			}
		}
		
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		//ListView lv = this.getListView();
		//lv.setDivider(null);
		//lv.setDividerHeight(0);
	}
	@Override
	public void onDetach() {
		super.onDetach();		
	}

	
	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(this.getActivity(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG) p("msg: " + s);
	}

	

}
