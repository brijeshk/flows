package com.auto.android;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ExpandableListFragment extends Fragment implements
		OnCreateContextMenuListener, ExpandableListView.OnChildClickListener,
		ExpandableListView.OnGroupCollapseListener,
		ExpandableListView.OnGroupExpandListener {

	
	final private Handler mHandler = new Handler();

	final private Runnable mRequestFocus = new Runnable() {
		public void run() {
			mList.focusableViewAvailable(mList);
		}
	};

	final private AdapterView.OnItemClickListener mOnClickListener = new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			onListItemClick((ListView) parent, v, position, id);
		}
	};

	final private OnChildClickListener mOnChildClickListener = new OnChildClickListener() {

		@Override
		public boolean onChildClick(ExpandableListView arg0, View arg1,
				int arg2, int arg3, long arg4) {
			return ExpandableListFragment.this.onChildClick(arg0, arg1, arg2,
					arg3, arg4);
		}
	};

	ExpandableListAdapter mAdapter;
	ExpandableListView mList;
	View mEmptyView;
	TextView mStandardEmptyView;
	View mListContainer;
	boolean mSetEmptyText;
	boolean mListShown;
	boolean mFinishedStart = false;

	public ExpandableListFragment() {
	}


	/**
	 * Attach to list view once Fragment is ready to run.
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
	}

	/**
	 * Detach from list view.
	 */
	@Override
	public void onDestroyView() {
		mHandler.removeCallbacks(mRequestFocus);
		mList = null;
		super.onDestroyView();
	}

	/**
	 * This method will be called when an item in the list is selected.
	 * Subclasses should override. Subclasses can call
	 * getListView().getItemAtPosition(position) if they need to access the data
	 * associated with the selected item.
	 *
	 * @param l
	 *            The ListView where the click happened
	 * @param v
	 *            The view that was clicked within the ListView
	 * @param position
	 *            The position of the view in the list
	 * @param id
	 *            The row id of the item that was clicked
	 */
	public void onListItemClick(ListView l, View v, int position, long id) {
	}

	/**
	 * Provide the cursor for the list view.
	 */
	public void setListAdapter(ExpandableListAdapter adapter) {
		boolean hadAdapter = mAdapter != null;
		mAdapter = adapter;
		if (mList != null) {
			mList.setAdapter(adapter);
			if (!mListShown && !hadAdapter) {
				// The list was hidden, and previously didn't have an
				// adapter. It is now time to show it.
				setListShown(true, getView().getWindowToken() != null);
			}
		}
	}

	/**
	 * Set the currently selected list item to the specified position with the
	 * adapter's data
	 *
	 * @param position
	 */
	public void setSelection(int position) {		
		mList.setSelection(position);
	}

	public long getSelectedPosition() {
		
		return mList.getSelectedPosition();
	}

	public long getSelectedId() {
		
		return mList.getSelectedId();
	}

	public ExpandableListView getExpandableListView() {
		
		return mList;
	}

	/**
	 * The default content for a ListFragment has a TextView that can be shown
	 * when the list is empty. If you would like to have it shown, call this
	 * method to supply the text it should use.
	 */
	public void setEmptyText(CharSequence text) {
		
		if (mStandardEmptyView == null) {
			throw new IllegalStateException(
					"Can't be used with a custom content view");
		}
		mStandardEmptyView.setText(text);
		if (!mSetEmptyText) {
			mList.setEmptyView(mStandardEmptyView);
			mSetEmptyText = true;
		}
	}

	/**
	 * Control whether the list is being displayed. You can make it not
	 * displayed if you are waiting for the initial data to show in it. During
	 * this time an indeterminant progress indicator will be shown instead.
	 *
	 * <p>
	 * Applications do not normally need to use this themselves. The default
	 * behavior of ListFragment is to start with the list not being shown, only
	 * showing it once an adapter is given with
	 * {@link #setListAdapter(ListAdapter)}. If the list at that point had not
	 * been shown, when it does get shown it will be do without the user ever
	 * seeing the hidden state.
	 *
	 * @param shown
	 *            If true, the list view is shown; if false, the progress
	 *            indicator. The initial value is true.
	 */
	public void setListShown(boolean shown) {
		setListShown(shown, true);
	}

	/**
	 * Like {@link #setListShown(boolean)}, but no animation is used when
	 * transitioning from the previous state.
	 */
	public void setListShownNoAnimation(boolean shown) {
		setListShown(shown, false);
	}

	/**
	 * Control whether the list is being displayed. You can make it not
	 * displayed if you are waiting for the initial data to show in it. During
	 * this time an indeterminant progress indicator will be shown instead.
	 *
	 * @param shown
	 *            If true, the list view is shown; if false, the progress
	 *            indicator. The initial value is true.
	 * @param animate
	 *            If true, an animation will be used to transition to the new
	 *            state.
	 */
	private void setListShown(boolean shown, boolean animate) {
		p("setListShown: "+shown);
		if (mList == null) return;
		if (mListShown == shown) {
			return;
		}
		mListShown = shown;
		if (shown) {
			if (animate) {
				mListContainer.startAnimation(AnimationUtils.loadAnimation(
						getActivity(), android.R.anim.fade_in));
			}
			mListContainer.setVisibility(View.VISIBLE);
		} else {
			if (animate) {
				mListContainer.startAnimation(AnimationUtils.loadAnimation(
						getActivity(), android.R.anim.fade_out));
			}
			mListContainer.setVisibility(View.GONE);
		}
	}

	/**
	 * Get the ListAdapter associated with this activity's ListView.
	 */
	public ExpandableListAdapter getExpandableListAdapter() {
		return mAdapter;
	}

	

	@Override
	public void onGroupExpand(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGroupCollapse(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onChildClick(ExpandableListView arg0, View arg1, int arg2,
			int arg3, long arg4) {
		p("onChildClick");
		return false;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
	}

	public void onContentChanged() {
		p("=========  onContentChanged ========== ");
		View emptyView = getView().findViewById(android.R.id.empty);
		mList = (ExpandableListView) getView().findViewById(android.R.id.list);
		if (mList == null) {
			throw new RuntimeException(
					"Your content must have a ExpandableListView whose id attribute is "
							+ "'android.R.id.list'");
		}
		if (emptyView != null) {
			mList.setEmptyView(emptyView);
		}
		mList.setOnChildClickListener(this);
		mList.setOnGroupExpandListener(this);
		mList.setOnGroupCollapseListener(this);
		p("got mList, got adapter: "+mAdapter+": nr groups: "+mAdapter.getGroupCount());
		//if (mFinishedStart) {
			setListAdapter(mAdapter);
		//}
		setListShown(true, false);
		//mFinishedStart = true;
	}
	 protected void p(String s) {
			Log.i("ExpandableListFragment", s);
		}
}