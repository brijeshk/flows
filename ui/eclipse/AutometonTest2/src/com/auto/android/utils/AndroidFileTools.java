package com.auto.android.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;

import com.auto.utils.FileToolsIF;

/**
 *
 * @author Chantal Roth
 */
public class AndroidFileTools implements FileToolsIF {

	private static AndroidFileTools ft;

	private Context context;

	public static AndroidFileTools getFileTools(Context context) {
		if (ft == null) {
			ft = new AndroidFileTools();
			ft.context = context;
		}
		return ft;
	}

	//public InputStream getExtSdStream(String filename) {
//		return Environment.getDownloadCacheDirectory();

	/* Checks if external storage is available for read and write */
	public boolean isExternalStorageWritable() {
	    String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state)) {
	        return true;
	    }
	    return false;
	}

	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable() {
	    String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state) ||
	        Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
	        return true;
	    }
	    return false;
	}
	public static String addSlashOrBackslash(File fdir) {
		return addSlashOrBackslash(fdir.toString());
	}

	public static String addSlashOrBackslash(String dir) {
		if (dir == null) {
			// err("addSlashOrBackslash: dir is null!");
			return "";
		}
		if (!dir.endsWith("/") && !dir.endsWith("\\")) {
			if (dir.indexOf("\\") > -1) {
				dir += "\\";
			} else {
				dir += "/";
			}
		}
		return dir;
	}

	/**
	 * Returns the content of a file as a string.
	 *
	 */
	public String getFileAsString(String filename) {
		// p("getFileAsString: File is "+filename);
		if (filename == null) {
			warn("Filename is null");
			return null;
		}

		String ret = "";

		InputStream inputStream = null;
		
		p("Checking SD card first for "+filename);

		try {
			inputStream = context.openFileInput(filename);	
			if (inputStream != null) return getFileAsString(inputStream);
		} catch (IOException e) {
			err("File not found: " + e.toString());
		}
		
		p("Not on SD card. Trying assets for "+filename);
		AssetManager assetManager = context.getAssets();
		try {
			inputStream = assetManager.open(filename);
		} catch (IOException e) {
			err("Could not open " + filename + " from assets: "
					+ e.getMessage());
		}
		if (inputStream != null) {
			
			ret = getFileAsString(inputStream);
			p("File " + filename + " is in assets:\n"+ret);
		}
		
		
		return ret;
	}

	/**
	 * Returns the content of a file as a string.
	 *
	 */
	public String getFileAsString(InputStream is) {
		if (is == null) {
			p("Got no input stream");
			return null;
		}

		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(is,
					"UTF-8"));
			return getFileAsString(in);
		} catch (Exception e) {
			err(e);
		}
		return null;
	}

	/**
	 * Returns the content of a file as a string.
	 *
	 */
	public static String getFileAsString(BufferedReader in) {

		StringBuilder res = new StringBuilder();

		try {
			while (in.ready()) {
				// res.append((char)in.read());
				res.append(in.readLine());
				res.append("\n");
			}
			in.close();
			// p("getFileAsString: Result is:\n"+res);
		} catch (Exception e) {
			err(e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception ex) {
					err(ex);
				}
			}
		}

		return res.toString();
	}

	public static ArrayList<String> getFileAsArray(String filename) {
		// p("getFileAsString: File is "+filename);
		if (filename == null || !(new File(filename)).exists()) {
			warn("Filename is null or does not exist");
			return null;
		}
		ArrayList<String> res = new ArrayList<String>();

		try {

			BufferedReader in = new BufferedReader(new FileReader(filename),
					1024 * 1024);

			while (in.ready()) {
				res.add(in.readLine());
			}
			in.close();
			// p("getFileAsString: Result is:\n"+res);
		} catch (FileNotFoundException e) {
			err(e);
		} catch (IOException e) {
			err(e);
		}
		return res;
	}

	public static ArrayList<String> getFileAsArray(URL filename) {
		// p("getFileAsString: File is "+filename);
		if (filename == null) {
			warn("URL is null");
			return null;
		}
		ArrayList<String> res = new ArrayList<String>();

		try {
			URLConnection uc = filename.openConnection();
			InputStreamReader input = new InputStreamReader(uc.getInputStream());
			BufferedReader in = new BufferedReader(input);

			while (in.ready()) {
				// res.append((char)in.read());
				String line = in.readLine();
				if (line != null) {
					line = line.trim();
				} else {
					line = "";
				}
				res.add(line);

			}
			in.close();
			// p("getFileAsString: Result is:\n"+res);
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		return res;
	}

	public boolean writeStringToFile(String filename, String content) {
		return writeStringToFile(filename, content, false);
	}

	public boolean writeStringToFile(String filename, String content,
			boolean append) {
		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					context.openFileOutput(filename, append? Context.MODE_APPEND : Context.MODE_PRIVATE));
			outputStreamWriter.write(content);
			outputStreamWriter.close();
		} catch (IOException e) {
			err("Exception: File write failed: " + e.toString());
			return false;
		}
		return true;
	}

	/**
	 * ================== LOGGING =====================
	 */
	private static void err(String msg, Exception ex) {
		Logger.getLogger(AndroidFileTools.class.getName()).log(Level.SEVERE, msg, ex);
	}

	private static void err(Exception ex) {
		Logger.getLogger(AndroidFileTools.class.getName()).log(Level.SEVERE,
				ex.getMessage(), ex);
	}

	private static void err(String msg) {
		Logger.getLogger(AndroidFileTools.class.getName()).log(Level.SEVERE, msg);
	}

	private static void warn(String msg) {
		Logger.getLogger(AndroidFileTools.class.getName()).log(Level.WARNING, msg);
	}

	private static void p(String msg) {
		// System.out.println("FileTools: " + msg);
		//Logger.getLogger(AndroidFileTools.class.getName()).log(Level.INFO, msg);
	}

	public static boolean isUrl(String path) {
		if (path == null || path.length() < 1) {
			return false;
		}
		int pos = path.indexOf("://");
		if (pos > 0 && pos < 10) {
			return true;
		} else {
			return false;
		}
	}
}
