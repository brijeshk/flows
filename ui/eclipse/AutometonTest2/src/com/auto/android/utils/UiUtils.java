package com.auto.android.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.android.model.InputManager;

import com.auto.model.Atom;
import com.auto.model.Contact;
import com.auto.model.DataValue;
import com.auto.model.FlowManager;
import com.auto.model.Group;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.PortNameComparator;
import com.auto.model.State;
import com.auto.model.StateNameComparator;
import com.auto.model.User;
import com.auto.model.UserNameComparator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class UiUtils {
	
	private static final String NAME_KEY = "NAME";


	private Context context;
	private boolean[] curselection;
	private AlertDialog dialog;
	private AlertDialog caldialog;
	private FlowManager man;
	private int listbgcolor = -1;

	
	public UiUtils(Context context) {	
		this.context = context;
		man = FlowManager.getManager();
	}

	public SimpleAdapter createSmileyList() {
		String smileys[] = { ":-)", ";-)", ":-|", "8-)", ":-(", ">:-(" };

		ArrayList listdata = new ArrayList<Map<String, ?>>();
		String[] from = { NAME_KEY, "second", "icon" };
		int[] to = { R.id.lblname, R.id.lbldetail, R.id.icon };
		for (String s : smileys) {
			// TODO: add image
			listdata.add(createMap(s));
		}
		SimpleAdapter simpleadapter = new SimpleAdapter(context, listdata,
				R.layout.listitem_suggestion, from, to);
		return simpleadapter;
	}

	public SimpleAdapter createUserList(List<Contact> list) {
		if (list != null)
			Collections.sort(list, new UserNameComparator());
		ArrayList listdata = new ArrayList<Map<String, ?>>();
		String[] from = { NAME_KEY, "second", "icon" };
		int[] to = { R.id.lblname, R.id.lbldetail, R.id.icon };
		for (Contact c : list) {
			if (c instanceof User) {
				User u = (User) c;
				if (!u.isTemplates() && !man.isMe(u)) {
					listdata.add(createMap(u));
				}

			} else
				listdata.add(createMap((Group) c));
		}
		SimpleAdapter simpleadapter = new SimpleAdapter(context, listdata,
				R.layout.listitem_suggestion, from, to);
		return simpleadapter;
	}

	public SimpleAdapter createTopicList(List<Port> list) {
		if (list != null)
			Collections.sort(list, new PortNameComparator());
		ArrayList listdata = new ArrayList<Map<String, ?>>();
		String[] from = { NAME_KEY, "second", "icon" };
		int[] to = { R.id.lblname, R.id.lbldetail, R.id.icon };
		for (Port u : list) {
			listdata.add(createMap(u));
		}
		SimpleAdapter simpleadapter = new SimpleAdapter(context, listdata,
				R.layout.listitem_suggestion, from, to);
		return simpleadapter;

	}

	public SimpleAdapter createStateList(List<State> list) {
		if (list != null)
			Collections.sort(list, new StateNameComparator());
		ArrayList listdata = new ArrayList<Map<String, ?>>();
		String[] from = { NAME_KEY, "second", "icon" };
		int[] to = { R.id.lblname, R.id.lbldetail, R.id.icon };
		for (State s : list) {
			listdata.add(createMap(s));
		}
		SimpleAdapter simpleadapter = new SimpleAdapter(context, listdata,
				R.layout.listitem_suggestion, from, to);
		return simpleadapter;

	}

	public SimpleAdapter createStringView(List<String> values) {
		ArrayList listdata = new ArrayList<Map<String, ?>>();
		String[] from = { NAME_KEY, };
		int[] to = { R.id.lblname, };
		for (String u : values) {
			listdata.add(createMap(u));
		}
		SimpleAdapter simpleadapter = new SimpleAdapter(context, listdata,
				R.layout.listitem_text, from, to);
		return simpleadapter;
	}

	public SimpleAdapter createValuesView(List<DataValue> values) {
		ArrayList listdata = new ArrayList<Map<String, ?>>();
		String[] from = { NAME_KEY, };
		int[] to = { R.id.lblname, };
		for (DataValue u : values) {
			listdata.add(createMap(u));
		}
		SimpleAdapter simpleadapter = new SimpleAdapter(context, listdata,
				R.layout.listitem_text, from, to);
		return simpleadapter;
	}
	private Map<String, ?> createMap(User u) {
		return createRow(u.getName(), u.getEmail(), AndroidGui.getDrawable(u),
				u);
	}

	private Map<String, ?> createMap(Group u) {
		return createRow(u.getName(), u.getUserlist(),
				AndroidGui.getDrawable(u), u);
	}

	private Map<String, ?> createMap(Port u) {
		return createRow(
				u.getName(),
				u.getDescription() == null ? u.getCreator() : u
						.getDescription(), AndroidGui.getDrawable(u), u);
	}

	private Map<String, ?> createMap(State u) {
		return createRow(u.getName(),
				u.getDescription() == null ? "" : u.getDescription(),
				AndroidGui.getDrawable(u), u);
	}

	private Map<String, ?> createMap(InputData u) {
		return createRow(u.getName(), u.getNrSubElements() + " sub elements",
				AndroidGui.getDrawable(u), u);
	}

	private Map<String, ?> createMap(String s) {
		return createRow(s, s);
	}

	private Map<String, ?> createMap(DataValue u) {
		return createRow(u.getValue(), u);
	}

	private Map<String, Object> createRow(String value1, String value2,
			int value4, Object obj) {
		Map<String, Object> row = new HashMap<String, Object>();
		row.put(NAME_KEY, value1);
		row.put("second", value2);
		row.put("icon", Integer.toString(value4));
		row.put("obj", obj);
		return row;
	}

	private Map<String, Object> createRow(String value1, Object obj) {
		Map<String, Object> row = new HashMap<String, Object>();
		row.put(NAME_KEY, value1);
		row.put("obj", obj);
		return row;
	}
	public void showDialog(final SimpleAdapter adapter, List selectedItems,
			final ItemsSelected selectionListener, final boolean allowMultiple) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		// p("Creating dialog with " + adapter.getCount() + " items");
		// if we have just ONE, just select it
		if (adapter.getCount() == 1) {
			//p("Just selecting that one item " + adapter.getItem(0)
			//		+ ", no need to show list!");
			ArrayList items = new ArrayList();
			items.add(extracItem(adapter.getItem(0)));
			selectionListener.itemsSelected(items);
			return;
		}
		// add text search to list view?
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View dialogview = inflater.inflate(R.layout.dialog_list, null);

		//p("Creating dialog with search box");
		final EditText txt = (EditText) dialogview
				.findViewById(R.id.dialogsearch);
		txt.setHint("type to filter");
		final ListView lv = (ListView) dialogview.findViewById(R.id.dialoglist);

		lv.setAdapter(adapter);
		TextWatcher searchwatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s != null && s.length() > 0) {
				//	p("onTextChanged: :" + s);
					// search list - filter
					adapter.getFilter().filter(s);
				}
				//} else
				//	p("On textchange, no text: " + s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		};
		txt.addTextChangedListener(searchwatcher);
		curselection = new boolean[adapter.getCount()];
		builder.setView(dialogview);
		if (selectedItems != null && !selectedItems.isEmpty()) {
			for (Object select : selectedItems) {

				for (int pos = 0; pos < adapter.getCount(); pos++) {
					Object obj = extracItem(adapter.getItem(pos));
					if (obj != null && obj.equals(select)) {
						lv.setItemChecked(pos, true);
						curselection[pos] = true;
					}
				}
			}
		}
		if (allowMultiple)
			lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		else
			lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> av, View v, int pos,
					long arg3) {

				
				ColorDrawable d;
				if (listbgcolor < 0) {
					Drawable bg = v.getBackground();
					if (bg != null && bg instanceof ColorDrawable) {
						listbgcolor = ((ColorDrawable) bg).getColor();
					} else
						listbgcolor = Color.TRANSPARENT;
				}
				curselection[pos] = !curselection[pos];

				// p("user clicked. Curselection "+pos+" is "+curselection[pos]+", change color.  v is "
				// + v);

				if (curselection[pos]) {
					v.setBackgroundColor(Color.LTGRAY);
					// p("Changing to gray");
				} else {
					v.setBackgroundColor(listbgcolor);
					// p("Changing to default");
				}
				if (!allowMultiple) {
					// p("We allow just ONE item, close dialog");
					dialog.dismiss();
					ArrayList items = new ArrayList();
					items.add(extracItem(adapter.getItem(pos)));
					selectionListener.itemsSelected(items);

				}

			}

		});

		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface d, int arg1) {
				SparseBooleanArray ba = lv.getCheckedItemPositions();

				if (!allowMultiple) {
					ArrayList items = new ArrayList();
					for (int i = 0; i < curselection.length; i++) {
						if (curselection[i]) {
							Object checkedItem = extracItem(adapter.getItem(i));
							items.add(checkedItem);
							selectionListener.itemsSelected(items);
							break;
						}
					}

				} else {
					ArrayList items = new ArrayList();
					for (int i = 0; i < curselection.length; i++) {
						if (curselection[i]) {
							Object checkedItem = extracItem(adapter.getItem(i));
							items.add(checkedItem);
						}
					}
					selectionListener.itemsSelected(items);
				}
			}

		});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface d, int arg1) {
						d.dismiss();
						selectionListener.cancel();
					}

				});
		dialog = builder.create();
		dialog.show();
		if (!allowMultiple) {
			// dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
			dialog.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(
					View.GONE);
		}
	}

	private Map<String, ?> createRow(String value1, String value2,
			String value3, int value4, Object obj) {
		Map<String, Object> row = new HashMap<String, Object>();
		row.put(NAME_KEY, value1);
		row.put("second", value2);
		row.put("third", value3);
		row.put("icon", Integer.toString(value4));
		row.put("obj", obj);
		return row;
	}
	public Object extracItem(Object checkedItem) {
		Map selection = (Map) checkedItem;
		if (!selection.containsKey(NAME_KEY)) {			
			return null;
		}
		String name = (String) selection.get(NAME_KEY);
		Object obj = selection.get("obj");
		// p("User selected object with name " + name + ":" + obj);
		return obj;
	}

	public Bitmap getBitmap(Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		}

		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;

	}

	public void getDrawable(Bitmap bitmap) {
		Drawable d = new BitmapDrawable(context.getResources(), bitmap);
	}
	
	public static void msg(Activity act, String title, String mymessage)
	   {
	   new AlertDialog.Builder(act)
	      .setMessage(mymessage)
	      .setTitle(title)
	      .setCancelable(true)
	       .setPositiveButton(android.R.string.ok,
	         new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int whichButton){}
	         })
	      
	      .show();
	   }
	
	
	public static String getStateColor(Context context, State state) {
		String statecolor = "<font color='gray'>";
		
		int barcolor = android.R.color.holo_blue_dark;
		
		if (state != null) {

			barcolor = AndroidGui.getBarColor(state);
			String color="#"+context.getResources().getString(barcolor).substring(3);
			//p("Bar color is "+barcolor+", resulting string: "+color);
			statecolor = "<font color='"+color+"'>"; 
		}
		return statecolor;
	}
	public static void setHtmlText(TextView txt, String html) {
		txt.setText(Html.fromHtml(html), TextView.BufferType.SPANNABLE);
	}
	public static int getStateBarColor(Context context, State state) {
		
		int barcolor = android.R.color.holo_blue_dark;
		
		if (state != null) {

			barcolor = AndroidGui.getBarColor(state);
			String color="#"+context.getResources().getString(barcolor).substring(3);
			//p("Bar color is "+barcolor+", resulting string: "+color);
		 
		}
		return barcolor;
	}
	
	public static void pick(Activity act, String title, String options, AnswerPicked callback) {
		String[] list = options.split(",");
		pick(act, title, list, callback);
	}
	public static void pick(Activity act, String title, String[] options, final AnswerPicked callback) {
		
		AlertDialog.Builder alert = new AlertDialog.Builder(act);
		alert.setTitle(title).setItems(options,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						callback.answerPicked(which);
					}
				});
		alert.show();
	}
	public static void yesOrNo(Activity act, String title, String mymessage, final YesAnswered callback)
	   {
	   new AlertDialog.Builder(act)
	      .setMessage(mymessage)
	      .setTitle(title)
	      .setCancelable(true)
	      .setPositiveButton(android.R.string.yes,
	 	         new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int whichButton){
		        	 callback.yesAnswered(true);
		         }
		         })
	      .setNegativeButton(android.R.string.no,
	         new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int whichButton){
	        	 callback.yesAnswered(false);
	         }
	         
	         })
	      .show();
	   }
	
	public interface QuestionAnswered {
		public void questionAnswered(String answer);
		public void questionCancelled();
	}
	public interface YesAnswered {
		public void yesAnswered(boolean yes);		
	}
	public interface AnswerPicked {
		public void answerPicked(int which);		
	}
	public static void ask(Activity act, String title, String message, final QuestionAnswered callback) {
		AlertDialog.Builder alert = new AlertDialog.Builder(act);

		alert.setTitle("Title");
		alert.setMessage("Message");

		// Set an EditText view to get user input 
		final EditText input = new EditText(act);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
		  String value = input.getText().toString();
		  callback.questionAnswered(value);
		  }
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
			  callback.questionCancelled();
		  }
		});

		alert.show();
	}
	private interface ValueEntered {
		public void valueEntered(DataValue val);

		public void cancel();
	}

	public interface ItemsSelected {
		public void itemsSelected(List items);

		public void cancel();
	}
}
