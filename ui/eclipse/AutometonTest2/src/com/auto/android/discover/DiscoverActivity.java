package com.auto.android.discover;

import android.content.Intent;
import android.os.Bundle;

import com.auto.android.AutometonActivity;
import com.auto.android.R;
import com.auto.android.expose.ExposeActivity;
import com.auto.model.User;
import com.tokenautocomplete.TokenCompleteTextView.TokenListener;

public class DiscoverActivity extends AutometonActivity implements
		TokenListener {

	
	User user;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.message_fragment);

	}
	protected void homeClicked() {
		 p("Home pressed. Show disc act");
	     this.startActivity(new Intent(this, DiscoverActivity.class)); 
	}
	@Override
	public void onTokenAdded(Object token) {
		p("Added: " + token);
	}

	@Override
	public void onTokenRemoved(Object token) {
		p("Removed: " + token);
	}

	
}