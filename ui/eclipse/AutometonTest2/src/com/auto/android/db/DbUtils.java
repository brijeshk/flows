/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.android.db;

import java.sql.Date;
import java.util.HashMap;
import java.util.logging.Logger;

import android.database.Cursor;

import com.auto.db.DbMap;

/**
 *
 * @author Chantal
 */
public class DbUtils {

    private static final Logger log = Logger.getLogger(DbUtils.class.getName());

    public static void show(Cursor rs) {
        int cols = rs.getColumnCount();
		for (int c = 0; c < cols; c++) {
		    String name = rs.getColumnName(c);
		    int type = rs.getType(c);              
		    p("Got col " + c + ":" + name + ", " + type);
		}
    }

    /**
     * extract data from result set - to be db independent after that
     * @param rs ResultSet
     * @return DbMap, like a HashMap but simplifies getting data from it
     */
    public static DbMap extract(Cursor rs) {
        HashMap<String, Object> map = new HashMap<String, Object>();     
        int cols = rs.getColumnCount();
		for (int c = 0; c < cols; c++) {            	
		    String name = rs.getColumnName(c);
		    int type = rs.getType(c);
		    //String clazz = m.getColumnClassName(c);
		    Object val = null;
		    if (type == Cursor.FIELD_TYPE_STRING) {
		    	val = rs.getString(c);
		        if (name.contains("date") || name.contains("created")) {
		            long millis = Long.parseLong((String)val);
		            val = new Date(millis);
		        }
		    		    
		    } else if (type == Cursor.FIELD_TYPE_INTEGER) {
		    	if (name.contains("date") || name.contains("created")) {
		    		val = rs.getLong(c);
			    }
		    	else val = rs.getInt(c);
		        
		    } else if (type == Cursor.FIELD_TYPE_FLOAT) {
		        val = rs.getFloat(c);
		    }                
		   
		    map.put(name, val);
		}
        return new DbMap(map);
    }

    public static void main(String args[]) {
        // test();
    }

    private static void p(String msg) {
        log.info(msg);
    }

    private static void warn(String msg) {
        log.warning(msg);
    }
}
