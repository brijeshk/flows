package com.auto.android.db;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.auto.db.SqlListeDesktopHelper;
import com.auto.db.SqlLiteHelper;
import com.auto.model.Atom;
import com.auto.model.AtomData;
import com.auto.model.Flow;
import com.auto.model.FlowData;

public class SqlLiteAndroidHelper implements SqlLiteHelper {

	private static final Logger log = Logger
			.getLogger(SqlLiteAndroidHelper.class.getName());

	private DbHelper dbmanager;
	private SQLiteDatabase db;

	public SqlLiteAndroidHelper(Context context) {
		p("Creating SqlLiteAndroidHelper");
		dbmanager = new DbHelper(context);

		dbmanager.recreate(dbmanager.getWritableDatabase());
	}

	@Override
	public void connect() {
		db = dbmanager.getWritableDatabase();
	}

	public void insert(FlowData fd) {
		connect();
		if (exists(fd)) {
			p("flow exists, using update instead");
			update(fd);
			return;
		}
		try {
			p("=================  Got insert: " + fd.getInsertString());
			db.execSQL(fd.getInsertString());
			updateFlowData(fd);

		} catch (SQLException ex) {
			System.err.println("Could not insert FlowData: " + ex.getMessage());
			Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(
					Level.SEVERE, null, ex);
		}

	}

	public void insert(AtomData ad) {
		connect();
		if (exists(ad)) {
			p("atom exists, using update instead");
			update(ad);
			return;
		}
		try {
			p(" ====================  Got insert: " + ad.getInsertString());
			db.execSQL(ad.getInsertString());
			update(ad);
		} catch (SQLException ex) {
			System.err.println("Could not insert atom: " + ex.getMessage());
			Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(
					Level.SEVERE, null, ex);
		}

	}

	@Override
	public void close() {
		try {
			if (db != null) {
				db.close();
			}
		} catch (SQLException e) {
			warn("Close failed: " + e.getMessage());
		}
	}

	private static void p(String msg) {
		// log.info(msg);
	}

	private static void warn(String msg) {
		log.warning(msg);
	}

	public boolean exists(FlowData fd) {
		Cursor c = query(fd.existsString());
		if (c == null || c.isClosed() || !c.moveToFirst()) {
			return false;
		} else
			return true;
	}

	public boolean exists(AtomData ad) {
		Cursor c = query(ad.existsString());
		if (c == null || c.isClosed() || !c.moveToFirst()) {
			return false;
		} else
			return true;
	}

	@Override
	public List<FlowData> getFlows() {
		List<FlowData> flows = new ArrayList<FlowData>();

		Cursor c = query("select * from flow");
		if (c == null || c.isClosed() || !c.moveToFirst())
			return null;
		try {
			do {
				FlowData fd = FlowData.create(DbUtils.extract(c));
				flows.add(fd);
			} while (c.moveToNext());
		} catch (SQLException ex) {
			Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		return flows;
	}

	@Override
	public List<AtomData> getAtoms() {
		List<AtomData> atoms = new ArrayList<AtomData>();

		try {
			Cursor c = query("select * from atom");
			if (c == null || c.isClosed() || !c.moveToFirst())
				return null;

			do {

				AtomData fd = AtomData.create(DbUtils.extract(c));
				atoms.add(fd);
			} while (c.moveToNext());

		} catch (SQLException ex) {
			Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		return atoms;
	}

	private Cursor query(String sql) {
		p(sql);
		SQLiteDatabase db = dbmanager.getReadableDatabase();
		return db.rawQuery(sql, null);
	}

	private void update(String sql) {
		p(sql);
		SQLiteDatabase db = dbmanager.getWritableDatabase();
		db.execSQL(sql);
	}

	private class DbHelper extends SQLiteOpenHelper {
		final static String DATABASE_NAME = "autometon";
		final static int DATABASE_VERSION = 1;

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			p("DbHelper: Created DbHelper for db " + DATABASE_NAME);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			p("DbHelper: ========== onCreate called");
			onUpgrade(db, 0, DATABASE_VERSION);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			p("DbHelper: ==========  onUpgrade called: " + oldVersion + "->"
					+ newVersion + ", creating tables");
			recreate(db);

		}

		public boolean isTableExists(String tableName) {
			return isTableExists(tableName, true);
		}

		public boolean isTableExists(String tableName, boolean openDb) {

			try {
				Cursor cursor = query("select DISTINCT tbl_name from sqlite_master where tbl_name = '"
						+ tableName + "'");
				if (cursor != null) {
					if (cursor.getCount() > 0) {
						cursor.close();
						return true;
					}
					cursor.close();
				}
			} catch (Exception e) {
				p("Db does not exist yet: " + e.getMessage());
			}
			return false;
		}

		public void recreate(SQLiteDatabase db) {
			boolean dbIsNew = true;//false;
			
			//if (!isTableExists("flow") || !isTableExists("atom") || !atomHasField("aid")) {
				dbIsNew = true;
				db.execSQL("drop table if exists flow");
				db.execSQL("drop table if exists atom");
				db.execSQL(FlowData.getCreateString());
				db.execSQL(AtomData.getCreateString());

				for (FlowData f : FlowData.createTestData()) {
					p("FLOW insert: " + f.getInsertString());
					db.execSQL(f.getInsertString());
				}

				for (AtomData a : AtomData.createTestData()) {
					p("ATOM insert: " + a.getInsertString());
					db.execSQL(a.getInsertString());
				}
			//} else
				
			// check if flows has field flags
			if (!dbIsNew && !flowHasField("flags")) {
				// update to add this field
				p("Flow does not have field flags yet, adding it");
				db.execSQL("ALTER TABLE flow ADD COLUMN flags string");
			}
			if (!dbIsNew && !flowHasField("modified")) {
				// update to add this field
				p("Flow does not have modified yet, adding it");
				db.execSQL("ALTER TABLE flow ADD COLUMN modified integer");
			}
			if (!dbIsNew && !atomHasField("read")) {
				// update to add this field
				p("Atom does not have field read yet, adding it");
				db.execSQL("ALTER TABLE atom ADD COLUMN read integer");
			}
			if (!dbIsNew && !atomHasField("modified")) {
				// update to add this field
				p("Atom does not have field modified yet, adding it");
				db.execSQL("ALTER TABLE atom ADD COLUMN modified integer");
			}
		}

	}

	private boolean existsColumnInTable(String inTable, String columnToCheck) {
		try {
			// query 1 row
			Cursor mCursor = query("SELECT * FROM " + inTable + " LIMIT 0");

			// getColumnIndex gives us the index (0 to ...) of the column -
			// otherwise we get a -1
			if (mCursor.getColumnIndex(columnToCheck) != -1)
				return true;
			else
				return false;

		} catch (Exception Exp) {
			// something went wrong. Missing the database? The table?
			p("When checking whether a column exists in the table, an error occurred: "
					+ Exp.getMessage());
			return false;
		}
	}

	public boolean flowHasField(String fieldname) {
		return existsColumnInTable("flow", fieldname);
	}

	public boolean atomHasField(String fieldname) {
		return existsColumnInTable("atom", fieldname);
	}

	@Override
	public void update(FlowData flowData) {
		update(flowData.getUpdateString());
		updateFlowData(flowData);
	}

	private void updateFlowData(FlowData flowData) {
		String datasql = flowData.getUpdateDataString();
		if (flowData.getData() != null) {
			SQLiteDatabase db = dbmanager.getWritableDatabase();
			db.rawQuery(datasql, new String[] { flowData.getData() });
		}
	}

	@Override
	public void delete(Flow flow) {
		for (Atom a : flow.getAtoms()) {
			delete(a.getAtomData());
		}
		update(flow.getFlowData().getDeleteString());

	}

	@Override
	public void update(AtomData atomData) {
		// the update string is WITHOUT data.
		// DATA has to be updated separtely
		update(atomData.getUpdateString());
		updateAtomData(atomData);
		updateAtomBody(atomData);

	}

	private void updateAtomData(AtomData atomData) {
		String datasql = atomData.getUpdateDataString();

		if (atomData.getData() != null) {
			p("========== updating atom data: " + atomData.getData());
			SQLiteDatabase db = dbmanager.getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put("data", atomData.getData());
			db.update("atom", cv, "aid=" + atomData.getAid(), null);

			// db.execSQL(datasql, new String[] { atomData.getData() });
			p("Checking atom data");
			Cursor c = query("select * from atom where flow_id="
					+ atomData.getFlow_id() + " and aid =" + atomData.getAid());
			if (c != null && !c.isClosed() && c.moveToFirst()) {
				int n = c.getColumnIndex("data");
				p("Found atom, data is: " + c.getString(n));
			} else
				p("Data was not stored");
		} else
			p("Got not no atom data. Not updating it");
	}

	private void updateAtomBody(AtomData atomData) {
		String datasql = atomData.getUpdateBodyString();
		if (atomData.getBody() != null) {
			p("updating body data: " + atomData.getBody());
			SQLiteDatabase db = dbmanager.getWritableDatabase();
			// db.execSQL(datasql, new String[] { atomData.getBody() });
			ContentValues cv = new ContentValues();
			cv.put("body", atomData.getBody());
			db.update("atom", cv, "aid=" + atomData.getAid(), null);
		} else
			p("Got not no atom body. Not updating it");
	}

	private static String q(String s) {
		return "\"" + s + "\"";
	}

	@Override
	public void delete(AtomData atomData) {
		update(atomData.getDeleteString());

	}

}
