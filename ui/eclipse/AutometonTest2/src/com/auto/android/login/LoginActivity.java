package com.auto.android.login;

import java.util.List;

import org.jivesoftware.smack.SmackAndroid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.android.db.SqlLiteAndroidHelper;
import com.auto.android.flows.FlowActivity;
import com.auto.android.prefs.PrefUtil;
import com.auto.android.service.MessagingService;
import com.auto.android.utils.AndroidFileTools;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import com.auto.model.User;
import com.auto.net.ChatServiceListener;
import com.auto.net.WebSocketConnector;

public class LoginActivity extends Activity {

	private static final String TAG ="Login";
	
	EditText txt_email;
	EditText txt_pw;
	EditText txt_phone;
	Button btn_login;
	AutometonApp app;
	FlowManager man;
	private PrefUtil prefUtil;
	SharedPreferences sharedPref ;
	int trials;
	User me;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		trials = 0;
		app = (AutometonApp)this.getApplication();
		prefUtil = new PrefUtil(this);
		sharedPref =PreferenceManager.getDefaultSharedPreferences(this);
		
		TextView lbl = (TextView) findViewById(R.id.lblregister);
		lbl.setText("To register or change your password, please visit:");
		
		TextView txtlink = (TextView) findViewById(R.id.register);
		txtlink.setMovementMethod(LinkMovementMethod.getInstance());
		String homepage = prefUtil.getHomepage();
		
		//txtlink.setText(Html.fromHtml("<a href=\""+homepage+"\">Register or change password</a>"));
		txtlink.setText(Html.fromHtml(homepage));
	    
		txt_email = (EditText) this.findViewById(R.id.email);
		txt_phone = (EditText) this.findViewById(R.id.phone);
		txt_pw = (EditText) this.findViewById(R.id.pw);
		
		
		txt_email.setText(sharedPref.getString("login", "chantal"));
		txt_pw.setText(sharedPref.getString("password", "chantal"));
		
		String defaultphonenr = "+41 76 399 09 32";
		
		TelephonyManager tMgr =(TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
	    String mPhoneNumber = null;
	    try {
	    	mPhoneNumber = tMgr.getLine1Number();
		    // also get preferences
		    p("phone mr from telemanager: "+mPhoneNumber);
		    if (mPhoneNumber == null || mPhoneNumber.trim().length()<1) {
		    	mPhoneNumber = defaultphonenr;
		    }
	    }
	    catch (Exception e) {
	    	p("Exception reading phone number. Defaulting to test number : " + e.getMessage());
	    	mPhoneNumber = defaultphonenr;
	    }
		txt_phone.setText(mPhoneNumber);
		
		btn_login = (Button)this.findViewById(R.id.login);
		btn_login.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				loginClicked();				
			}		
		});
	}
	private void loginClicked() {
		if (trials > 3) {
			msg("Please go to the website to register or change pw");
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(prefUtil.getHomepage()));
			startActivity(browserIntent);
			p("======= EXITING AFTER TOO MANY WRONG LOGINS========");
			finish();
			return;
		}
		trials++;
		String email = txt_email.getText().toString();
		String pw = txt_pw.getText().toString();
		String phone= txt_phone.getText().toString();
		Log.d(TAG, "login clicked: "+email+"/"+pw+"/"+phone );
		Editor ed = sharedPref.edit();
		ed.putString("login", email);
		ed.putString("password", pw);
		ed.commit();
		
		man = FlowManager.getManager( new SqlLiteAndroidHelper(this), AndroidFileTools.getFileTools(this));
		
		me = new User(email);
		me.setName(email);
		me.setPassword(pw);
	    me.setPhone(phone);
		try {
            msg("Logging in...");
            new LoginTask().execute();
						
			// only start flow intent if login was successful
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("LoginActivity", "Could not log in",e);
			msg("Could not log in, please try again: "+e.getMessage());
		}
		
	}
	private class LoginTask extends AsyncTask {           
		@Override
		protected Object doInBackground(Object... arg0) {
			 try {
				SmackAndroid.init(LoginActivity.this);
				java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
			    java.lang.System.setProperty("java.net.preferIPv4Stack", "true");			    
			    int code = man.login(me);			    
				if (code != 200) {
					runOnUiThread(new Runnable() {
						public void run() {
							msg("Could not log in, please try again or register");
						    }
						});					
					
				} else {
					p("Login successful");
					p("Main user is: "+man.getMe());
					app.setManager(man);					
		            new ConnectTask().execute();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 return null;
		}
  }
	  private class ConnectTask extends AsyncTask {           
			@Override
			protected Object doInBackground(Object... arg0) {
				 try {
					SmackAndroid.init(LoginActivity.this);
					WebSocketConnector conn = new WebSocketConnector(prefUtil.getWebSocketUrl());
					conn.setChatServiceListener(man);
					
					boolean ok = man.loginService(conn);
					if (ok) {
						p("Loading data from db and sever");			            
			            man.loadData();
			            p("starting chat server login task");
						conn.setChatServiceListener(man);
						startService(new Intent(MessagingService.class.getName()));
						//Intent intent = new Intent(this, MainTabActivity.class);
			            Intent intent = new Intent(LoginActivity.this, FlowActivity.class);
						startActivity(intent);
					}
					else {
						msg("Could not log in, please try again");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 return null;
			}
      }

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	 private void p(String s) {
     	//System.out.println("ActivityBottomTabs: "+s);
     	Log.i(TAG, s);
     }
     private void msg(String s) {
     	Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
     	p("msg: "+s);
     }
}
