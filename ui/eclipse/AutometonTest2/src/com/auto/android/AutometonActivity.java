package com.auto.android;

import java.util.ArrayList;
import java.util.Locale;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.contact.ContactActivity;
import com.auto.android.discover.DiscoverActivity;
import com.auto.android.expose.ExposeActivity;
import com.auto.android.flows.FlowActivity;
import com.auto.android.login.LoginActivity;
import com.auto.android.prefs.AutometonPreferencesActivity;
import com.auto.android.utils.UiUtils;
import com.auto.model.FlowManager;

public class AutometonActivity extends Activity {
	protected static FlowManager man;
	protected AutometonApp app;
	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;
	public static final int FILTER_ID = 99;

	protected EditText searchtextview;
	protected TextWatcher searchwatcher;
	protected MenuItem closeSearch;
	protected MenuItem menuSearch;
	protected MenuItem voiceMenu;

	private final int REQ_CODE_SPEECH_INPUT = 100;

	protected int options_menu = R.menu.main_activity_actions;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (AutometonApp) this.getApplication();
		man = app.getManager();

		// savedInstanceState.get
		if (man == null || man.getMe() == null) {
			msg("Need to login first: manager is null or me is null");
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			return;
		}
		// hideTitle();
		restoreActionBar();
	}

	// private void hideTitle() {
	// ViewGroup decorView = (ViewGroup) getWindow().getDecorView();
	// ActionBarOverlayLayout root = (ActionBarOverlayLayout)
	// decorView.getChildAt(0);
	// FrameLayout titleContainer = (FrameLayout) root.getChildAt(0);
	// TextView title = (TextView) titleContainer.getChildAt(0);
	// title.setVisibility(View.GONE);
	// }

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		// actionBar.setNavigationMode(ActionBar.n);
		// actionBar.setDisplayShowTitleEnabled(false);

		//actionBar.setTitle("");
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// p("onCreateOptionsMenu called");
		getMenuInflater().inflate(options_menu, menu);

		searchtextview = new EditText(this);
		searchtextview.setHint("search");
		// first, make it invisible
		searchtextview.setVisibility(View.GONE);
		// searchtextview.setOnClickListener(new );
		searchtextview.setText(null);
		menuSearch = menu.getItem(2);
		closeSearch = menu.getItem(3);
		closeSearch.setVisible(false);
		voiceMenu = menu.getItem(4);
		//voiceMenu.setVisible(false);
		menuSearch.setVisible(false);
		menuSearch.setActionView(searchtextview);
		LayoutParams params = (LayoutParams) searchtextview.getLayoutParams();
		if (params == null) {
			params = new LayoutParams(200, 100);
			searchtextview.setLayoutParams(params);
		} else
			params.width = 200;

		searchwatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s != null && s.length() > 0) {
					// p("onTextChanged: :" + s);
					searchAction(s.toString());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		};
		searchtextview.addTextChangedListener(searchwatcher);

		return true;
	}

	protected void textChanged(String s) {
		p("textChanged: :" + s);
		searchAction(s);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// p("onPrepareOptionsMenu called");
		// Inflate the menu; this adds items to the action bar if it is present.
		if (menu.size() < 2) {
			getMenuInflater().inflate(R.menu.options_actions, menu);
		}
		return true;
	}

	protected void searchAction() {
		p("User picks search");
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Search threads");
		String message = "Please enter a search string:";
		alert.setMessage(message);

		final EditText datatextinput = new EditText(this);

		alert.setView(datatextinput);
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = datatextinput.getText().toString();
				if (value != null && value.trim().length() > 0) {
					searchAction(value);
				} else
					p("User didn't enter anything");

			}
		});
		alert.show();
	}

	protected void searchAction(String txt) {
		p("searchAction: intent FlowActivity with search string " + txt);
		Intent intent = new Intent(this, FlowActivity.class);
		if (txt != null) {
			intent.putExtra("ACTION", "FIND");
			intent.putExtra("FINDSTRING", txt);
		}
		this.startActivity(intent);
	}

	protected void newObjectAction() {
		p("User picks new action");

		Intent intent = new Intent(this, FlowActivity.class);
		intent.putExtra("ACTION", "CREATE");
		this.startActivity(intent);
	}
	
	protected void homeClicked() {
		 p("Home pressed. Show flow fragment");
	     this.startActivity(new Intent(this, FlowActivity.class)); 
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		int id = item.getItemId();
		if (id == android.R.id.home) {
			homeClicked();
			return true;
		} else if (id == R.id.action_search_button) {
			showSearch();
			return true;
		} else if (id == R.id.action_search_x) {
			hideSearch();
			return true;
		} else if (id == R.id.action_new || id == R.id.action_create) {
			newObjectAction();
			return true;
		} else if (id == R.id.action_flows) {
			this.startActivity(new Intent(this, FlowActivity.class));
			return true;
		} else if (id == R.id.action_discover) {
			// p("User picks discover activity");
			this.startActivity(new Intent(this, DiscoverActivity.class));
			return true;
		} else if (id == R.id.action_expose) {
			// p("User picks expose activity");
			this.startActivity(new Intent(this, ExposeActivity.class));
			return true;
		} else if (id == R.id.action_contacts) {
			// p("User picks contacts activity");
			this.startActivity(new Intent(this, ContactActivity.class));
			return true;
		} else if (id == R.id.action_settings) {
			// p("Starting preferences activity");
			// this.startActivityForResult(new Intent(this,
			// AutometonPreferencesActivity.class), SHOW_PREFERENCES);
			this.startActivity(new Intent(this,
					AutometonPreferencesActivity.class));
			return true;
		} else if (id == R.id.action_voice) {
			p("test: voice recognition test");
			promptSpeechInput();
			return true;
		} else if (id == R.id.action_test) {
			p("test: voice recognition test");
			promptSpeechInput();
			return true;
		} else if (id == R.id.action_deleteallcontacts) {
			p("develop: delete all (people) contacts");
			man.deleteAllPeopleContacts();
			return true;
		} else if (id == R.id.action_deleteallflows) {
			p("develop: action_deleteallflows - just locally");
			man.deleteAllFlows(false);
			return true;
		} else if (id == R.id.action_newuser) {
			p("develop: pretending to be new user");
			newUserAction();
			return true;
		}
		// action_deleteallcontacts
		return super.onOptionsItemSelected(item);
	}
	protected void newUserAction() {
		// what should happen if the user is new? Some explanation? User import?
		// check what state we are at?
		if (!man.hasRealContacts()) {
			UiUtils.msg(this, "Welcome to AutoMeton", "To get started, pick at least one contact");
			//start import action
			this.startActivity(new Intent(this, ContactActivity.class));
			// but also start import
		}
		else {
			// user already has contacts, but maybe no ports  to use yet?
			if (!man.hasPorts()) {
				// user cannot send any message yet
				UiUtils.msg(this, "Invite friends", "Your contacts have not shared any gates yet. Invite them to join so you can start to chat.");
				// ask if it is ok to send email?
				UiUtils.ask(this, "Send Email?", "Would you like me to send an email to your friends?", new UiUtils.QuestionAnswered(){

					@Override
					public void questionAnswered(String answer) {
						msg("not done");
						
					}
					@Override
					public void questionCancelled() {
						
					}
					
				});
			}
		}
		
		
	}

	protected void hideSearch() {
		if (searchtextview == null)
			return;
		searchtextview.setVisibility(View.GONE);
		// s tart search

		menuSearch.setVisible(false);
		closeSearch.setVisible(false);
		// clear search
		searchtextview.setText(null);
		searchAction(null);
	}

	protected void hideVoice() {		
		if (voiceMenu != null) voiceMenu.setVisible(false);		
	}
	protected void showVoice() {		
		if (voiceMenu != null) voiceMenu.setVisible(true);		
	}
	protected void showSearch() {
		searchtextview.setVisibility(View.VISIBLE);
		// s tart search
		menuSearch.setVisible(true);
		closeSearch.setVisible(true);
		searchtextview.requestFocus();
	}

	private void promptSpeechInput() {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say something");
		try {
			startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
		} catch (ActivityNotFoundException a) {
			msg("Sorry, could not find voice input software");
		}
	}

	/**
	 * Receiving speech input
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case REQ_CODE_SPEECH_INPUT: {
			if (resultCode == RESULT_OK && null != data) {

				ArrayList<String> result = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				userSaid(result);
			}
			break;
		}

		}
	}

	protected void userSaid(ArrayList<String> result) {
		msg(result.get(0));
		for (String s : result) {
			p("User might have said: " + s);
		}
	}

	protected void p(String s) {
		Log.i(TAG, TAG + ": " + s);
	}

	protected void msg(String s) {
		Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG)
			p("msg: " + s);
	}
}
