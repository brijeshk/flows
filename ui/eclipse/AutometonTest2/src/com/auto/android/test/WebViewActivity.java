package com.auto.android.test;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import com.auto.android.AutometonActivity;
import com.auto.android.R;

public class WebViewActivity extends AutometonActivity {	
	WebView webview;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test);
		
    	webview = (WebView)this.findViewById(R.id.webView); 
    	webview.getSettings().setJavaScriptEnabled(true);
    	webview.loadUrl("file:///android_asset/www/input.htm");
    	webview.addJavascriptInterface(new JavaScriptInterface(this), "jsInterface");
	}
		

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("sendDataToJavaScript");
		menu.add("updatePort");
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getTitle().equals("sendDataToJavaScript")) {
			webview.loadUrl("javascript:setSearchData([{\"key\":\"To\",\"value\":\"prateek\"}]);");
			return true;
		}
		else if (item.getTitle().equals("updatePort")) {
			updatePort("['a', 'b', 'c', 'd', 'e', 'f']");
			return true;
		}
		return false;
	}
	private void updatePort(String portsvalues) {
		webview.loadUrl("javascript:updatePort("+portsvalues+");");		
	}
	
	public class JavaScriptInterface {
	    Context mContext;

	    /** Instantiate the interface and set the context */
	    JavaScriptInterface(Context c) {
	        mContext = c;
	    }

	    @JavascriptInterface
	    /** Show a toast from the web page */
	    public String showToast(String toast) {
	        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
	        return toast;
	    }
	    
	    @JavascriptInterface
	    public void sendResultToJava(String json){
	    	msg("Got result from JavaScript: "+json);
	    }
	}
}