package com.auto.android.test;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import com.auto.android.R;

public class TestActivity extends Activity {	
	WebView webview;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test);
		
    	webview = (WebView)this.findViewById(R.id.webView); 
    	webview.getSettings().setJavaScriptEnabled(true);
    	webview.loadUrl("file:///android_asset/www/test.html");
    	webview.addJavascriptInterface(new JavaScriptInterface(this), "jsInterface");
	}
		

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("MakeItDoubleByJS");
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getTitle().equals("MakeItDoubleByJS")) {
			webview.loadUrl("javascript:MakeItDoubleByJS(6);");
			return true;
		}
		return false;
	}
	
	public class JavaScriptInterface {
	    Context mContext;

	    /** Instantiate the interface and set the context */
	    JavaScriptInterface(Context c) {
	        mContext = c;
	    }

	    @JavascriptInterface
	    /** Show a toast from the web page */
	    public String showToast(String toast) {
	        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
	        return toast;
	    }
	    
	    @JavascriptInterface
	    public int makeItDoubleByJava(int val){
	    	return val*2;
	    }
	}
}