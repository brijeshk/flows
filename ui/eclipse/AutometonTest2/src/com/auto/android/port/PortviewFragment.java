package com.auto.android.port;

import java.io.Serializable;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AndroidGui;
import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.android.model.InputManager;
import com.auto.model.FlowManager;
import com.auto.model.Port;
import com.auto.model.User;
import com.auto.net.WebService;

/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class PortviewFragment extends Fragment implements
		AdapterView.OnItemLongClickListener {

	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;
	public static final String ARG_PARAM1 = "port";
	private static FlowManager man;
	private AutometonApp app;
	private Port port;
	private Port previousport;

	private Button btnsave;
	private Button btncancel;
	private Button btndelete;
	private Button btnstates;
	private SeekBar seekbar;
	private TextView visvalue;
	private TextView txtactions;
	private EditText txtname;
	private EditText txtdesc;

	private OnPortViewFragmentListener mListener;

	public static PortviewFragment newInstance(Port port) {

		PortviewFragment fragment = new PortviewFragment(port);
		Bundle args = new Bundle();
		args.putSerializable(ARG_PARAM1, (Serializable) port);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public PortviewFragment(Port port) {
		this.port = port;
	}

	public PortviewFragment() {

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		p("Long item click");
		return false; // let the system show the context menu
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == android.R.id.list) {
			p("Context menu on atom item");
			menu.setHeaderTitle("Action on gate");

			String[] menuItems = getResources().getStringArray(
					R.array.portmenulist);
			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		} else
			p("Context menu not on list: " + v);

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		String t = item.getTitle().toString().toLowerCase();
		p("User picked menu " + item + ":" + t);
		return false;

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnPortViewFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnPortViewFragmentListener");
		}
		// this.activity = activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("2. onCreate");
		if (getArguments() != null) {
			port = (Port) getArguments().getSerializable(ARG_PARAM1);
		}
		app = (AutometonApp) this.getActivity().getApplication();
		man = app.getManager();

	}

	private void updateSeekBar(int val) {
		String txt = "just me";
		if (val < 0) val = 4;
		if (val == 1)
			txt = "specific contacts";
		else if (val == 2)
			txt = "all my contacts";
		else if (val == 3)
			txt = "my contacts and their friends";
		else if (val == 4)
			txt = "everyone";
		visvalue.setText("Visibility: " + txt);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		p("3. onCreateView");
		saveOldCopy();

		View view = inflater.inflate(R.layout.port_view_fragment, null);

		visvalue = (TextView) view.findViewById(R.id.visibletovalue);

		seekbar = (SeekBar) view.findViewById(R.id.visibletobar);
		seekbar.setMax(4);
		//seekbar.setProgress(0);
		int vis = port.getVisibilityLevel();
		updateSeekBar(vis);

		seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int val,
					boolean fromUser) {
				updateSeekBar(val);
				// if progress is 2, we need to pick a list of contacts
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});

		p("Got seekbar: " + seekbar);
		btnsave = (Button) view.findViewById(R.id.save);
		btncancel = (Button) view.findViewById(R.id.cancel);
		btnstates = (Button) view.findViewById(R.id.actions);
		btndelete = (Button) view.findViewById(R.id.delete);

		
		TextView visactions = (TextView)view.findViewById(R.id.visactions);
		visactions.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("open state list canvas");
				mListener.showStateCanvas(port);
			}
		});
		
		
		btnsave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("Save clicked. Get all values first");
				int visible = seekbar.getProgress();
				p("TODO: compute visible to based on how visible the gate should be");
				if (visible == 0) { // just me
					port.setVisibilityLevel(0);
				} else if (visible == 1) { // "specific contacts"
					port.setVisibilityLevel(1);
					// TODO
					msg("ASK USER TO PICK CONTACTS");
				} else if (visible == 2) { // "all my contacts";
					port.setVisibilityLevel(2);

				} else if (visible == 3) { // my contacts and their friends;
					port.setVisibilityLevel(3);

				} else { // public
					port.setVisibilityLevel(-1);

				}
				// also get name, description etc
				port.setName(txtname.getText().toString());
				port.setDescription(txtdesc.getText().toString());
				
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						man.update(port);
					}
				});
				t.start();
				mListener.onPortEdited(port);
			}
		});
		btncancel.setVisibility(View.GONE);
		btncancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("Cancel clicked");
				p("RESTORE PREVIOUS COPY");
				previousport.copyValuesTo(port);
				mListener.onPortEdited(port);
			}
		});
		btndelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("delete clicked");
				man.delete(port);
				mListener.onPortEdited(null);
			}
		});
		btnstates.setVisibility(View.GONE);
		btnstates.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("open state list canvas");
				mListener.showStateCanvas(port);
			}
		});
		TextView headerView = (TextView) view.findViewById(R.id.portheader);
		String name = port.getName();

		txtactions = (TextView) view.findViewById(R.id.lblactions);
		txtname = (EditText) view.findViewById(R.id.lblname);
		txtname.setText(port.getName());

		txtdesc = (EditText) view.findViewById(R.id.lbldetail);
		String desc = port.getDescription();
		String actions = port.getStates().toString();
		if (actions == null || actions.length() < 1) {
			actions = "no actions";
		}
		if (desc == null || desc.length() < 1) {
			desc = "";
		}
		txtdesc.setText(desc);
		txtactions.setText(actions);
		txtactions.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("open state list canvas");
				mListener.showStateCanvas(port);
			}
		});
		// TODO: edit drawable...
		ImageView image = (ImageView) view.findViewById(R.id.icon);
		image.setImageResource(AndroidGui.getDrawable(port));

		p("Udating header view with: " + name);
		headerView.setText("Details for " + name);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		p("4. onActivityCreated");

	}

	private void saveOldCopy() {
		previousport = port.copy();
	}

	public void setPort(Port port) {
		p("updateView with Port " + port);
		this.port = port;
		saveOldCopy();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onDetach() {
		super.onDetach();

		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnPortViewFragmentListener {
		//
		public void onPortEdited(Port port);

		public void showStateCanvas(Port port);
	}

	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(this.getActivity(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG)
			p("msg: " + s);
	}

}
