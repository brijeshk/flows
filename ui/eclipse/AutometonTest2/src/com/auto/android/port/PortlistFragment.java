package com.auto.android.port;

import java.io.Serializable;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.android.flows.FlowActivity;
import com.auto.android.model.InputManager;
import com.auto.model.Contact;
import com.auto.model.ContactInfo;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import com.auto.model.Port;
import com.auto.model.User;


/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class PortlistFragment extends Fragment implements  AdapterView.OnItemLongClickListener  {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	public static final String ARG_PARAM1 = "flow";
	private static FlowManager man;
	private AutometonApp app;
	private Contact user;

	private Button btnOptions;
	private OnPortFragmentListener mListener;
	private ExpandableListView listView;
	private ExpandablePortListAdapter adapter;
	
	
	private CheckBox[] checktags ;
	
	final CharSequence[] myoptions = { "Create gate" };

	final CharSequence[] otheroptions = { "Create message" };
	private Activity activity;
	
	public static PortlistFragment newInstance(Contact user) {
		
		PortlistFragment fragment = new PortlistFragment(user);
		Bundle args = new Bundle();
		args.putSerializable(ARG_PARAM1, (Serializable)user);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public PortlistFragment(Contact user) {
		this.user = user;
	}
	public PortlistFragment() {
	
	}
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		p("Long item click");
	    return false; // let the system show the context menu
	  }

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuinfo) {		
		p("onCreateContextMenu");
	  if (v.getId()==android.R.id.list) {
		  p("Context menu on atom item");
		  ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo)menuinfo;
		    View target = info.targetView;
		    p("Target view "+target);
		    p("Packed pos="+info);
		    int group = this.listView.getPackedPositionGroup(info.packedPosition);
		    int child = this.listView.getPackedPositionChild(info.packedPosition);
		    
		    Port port = (Port) adapter.getChild(group, child);
		    if (port == null) menu.setHeaderTitle("Action on GATE");
		    else menu.setHeaderTitle("Action on "+port.getName());
	    
	    String[] menuItems = getResources().getStringArray(R.array.portmenulist);
	    for (int i = 0; i<menuItems.length; i++) {
	      menu.add(Menu.NONE, i, i, menuItems[i]);
	    }
	  }
	  else p("Context menu not on list: "+v);
	 
	} 
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		p("User picked menu "+item);
		String t = item.getTitle().toString().toLowerCase();
				
		ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo)item.getMenuInfo();
	    View target = info.targetView;
	    p("Target view "+target);
	    p("Packed pos="+info);
	    int group = this.listView.getPackedPositionGroup(info.packedPosition);
	    int child = this.listView.getPackedPositionChild(info.packedPosition);
	    
	    Port port = (Port) adapter.getChild(group, child);
	    if (t.startsWith("new message")) {
			p("create new message. starting flow activity");
			Intent intent = new Intent(this.getActivity(), FlowActivity.class);
			intent.putExtra("ACTION", "CREATE");
			intent.putExtra("PORT_ID", ""+port.getId());
			intent.putExtra("USER_ID", port.getCreator());
			this.startActivity(intent);
			
		}
	    else if (t.startsWith("add") || t.startsWith("create")) {
			p("create gate like "+port.getName());
			this.createPort(port);	
		}
		else if (t.startsWith("edit") ) {
			msg("edit gate "+port.getName());
			if (null != mListener) {
				p("onChildClick port :"+port);
				mListener.onPortSelected(port);
			}
			
		}
		else if (t.startsWith("delete")) {
			msg("delete gate "+port.getName());
			man.delete(port);
			adapter.remove(port);
			this.listView.invalidate();
			
		}		
		return true;
		
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = activity;
		try {
			mListener = (OnPortFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
		//this.activity = activity;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("2. onCreate");
		if (getArguments() != null) {
			user = (Contact)getArguments().getSerializable(ARG_PARAM1);			
		}
		app = (AutometonApp)this.getActivity().getApplication();
		man = app.getManager();
		
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		p("3. onCreateView");
		
	    View view = inflater.inflate(R.layout.port_fragment, null);
	    TextView headerView = (TextView) view.findViewById(R.id.portheader);
	    String who =user.getName();
	    
	    // TODO: add contact info also such as name, share ports etc
	    
	    btnOptions = (Button) view.findViewById(R.id.btnoptions);
	    
	    btnOptions.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				p("btnOptions clicked. Show options");
				showOptions();
			}

		});
	    p("Udating header view with: "+who);
		headerView.setText(app.getString(R.string.Ports)+" for "+who);
		listView = (ExpandableListView) view.findViewById(android.R.id.list);
		registerForContextMenu(listView);
		
		
		TextView userHeader = (TextView) view.findViewById(R.id.userheader);
		TextView tinfo = (TextView) view.findViewById(R.id.info);
		final LinearLayout lltags = (LinearLayout)view.findViewById(R.id.lltags);
		final CheckBox allowgates = (CheckBox)view.findViewById(R.id.checkallowgates);
		
		if (user instanceof User && FlowManager.getManager().isMe((User)user)) {
			userHeader.setText("My details");
			lltags.setVisibility(View.GONE);
			allowgates.setVisibility(View.GONE);
			tinfo.setText("This is me :-)");
		}
		else {
			userHeader.setText("Details for "+this.user.getName());
			final ContactInfo info = FlowManager.getManager().getMe().getInfoOn(user);
			p("Got contact info: "+info);
			if (info == null) {
				
				lltags.setVisibility(View.GONE);
				allowgates.setVisibility(View.GONE);
				if (user instanceof User) {
					User u = (User)user;
					if (u.isSystem()) tinfo.setText("This is a system user");
					
					else {
						tinfo.setText("This user does not have me in contacts");
						p("ERROR: ========== Got no contact info on "+user);
					}
				}
				else {
					tinfo.setText("This is a group");
				}
				
			}
			else {
				tinfo.setVisibility(View.GONE);
				boolean allow = info.isSharePorts();
				allowgates.setText(""+user.getName()+" can use my gates");
				allowgates.setChecked(allow);
				p("info is: "+info.toJson()+", allow="+allow);
				allowgates.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

				       @Override
				       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
				    	   info.setSharePorts(allowgates.isSelected());
				    	   p("Setting allow gates .Info is now: "+info);
				    	   p("TODO: check if save works. me="+FlowManager.getManager().getMe().toJson());
				    	   //FlowManager.getManager().updateMe();
				       }
				   }
				);    
				String posstags[] = ContactInfo.getPossibleTags();
				
				checktags = new CheckBox[posstags.length];
				for (int i = 0; i < posstags.length; i++) {
					String tag = posstags[i];
					final CheckBox ch = new CheckBox(activity);
					ch.setText(tag);;
					if (info.hasTag(tag)) ch.setChecked(true);
					lltags.addView(ch);
					ch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

					       @Override
					       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
					    	   String tag = buttonView.getText().toString();
					    	   p("User clicked on "+tag);
					    	   if (isChecked) {
					    		   info.addTag(tag);
					    	   }
					    	   else info.removeTag(tag);
					    	   
					    	   p("Info is now: "+info);
					    	   p("TODO: check if save works. me="+FlowManager.getManager().getMe().toJson());
					    	   //FlowManager.getManager().updateMe();
					       }
					   }
					); 
				}
				
			}
		}
		
	    return view;
	}
	private void tagsEdited(ContactInfo info, String tags) {
		if (tags != null && tags.length()>0) {
			p("Got tags:"+tags);
			p("TODO: parse, add to contactinfo, save");
			
			//info.setTags(taglist);
			 p("TODO: check if save works. me="+FlowManager.getManager().getMe().toJson());
	    	   //FlowManager.getManager().updateMe();
		}
	}
	private void showOptions() {
		// if we are at the beginning, just show users
		
		AlertDialog.Builder alert = new AlertDialog.Builder(this.getActivity());
		final boolean isme = user instanceof User && FlowManager.getManager().isMe((User)user);
		final CharSequence[] options =  isme ? myoptions: otheroptions;
		alert.setTitle("What would you like to do?").setItems(options,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						if (which == 0) {
							if (isme) {
								msg("create gate");
								InputManager im = new InputManager(getActivity());
								im.createTopic("",
									new InputManager.PortCreatedListener() {
						
										@Override
										public void portCreated(Port port) {
											if (mListener != null){
												mListener.onPortSelected(port);
											}
										}
								});		
							}
							else {
								
							
								p("create new message. starting flow activity");
								Intent intent = new Intent(getActivity(), FlowActivity.class);
								intent.putExtra("ACTION", "CREATE");
								intent.putExtra("USER_ID", user.getId());
								startActivity(intent);
							}
						}
					}
				});
		alert.show();

	}
	
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		p("4. onActivityCreated, adding items");
		
		addItems();
	}
	
	
	
	public void createPort(final Port oldport) {
		InputManager im = new InputManager(getActivity());
		im.createTopic(oldport != null ? "new "+ oldport.getName() : "new gate",
			new InputManager.PortCreatedListener() {

				@Override
				public void portCreated(Port newport) {
					if(oldport != null) {
						newport.setType(newport.getType());
					}
					if (mListener != null){
						mListener.onPortSelected(newport);
					}
				}
		});
		
	}

	
	public void updateView(Contact user) {
		p("updateView with Contact "+user);
		this.user = user;
		
		addItems();
		
	}
	private void addItems() {
		p("============ ADD ITEMS ============");
		adapter = new ExpandablePortListAdapter(getActivity());
		
		//p("Adding ports for user "+user);
		for (Port port: user.getPorts()) {
			//p(" adding "+port);
			adapter.add(port);
		}
		listView.setAdapter(adapter);
		for (int i = 0; i < adapter.getGroupCount(); i++){
			listView.expandGroup(i);
		}
		adapter.notifyDataSetChanged();
		p("after adding items: "+adapter.getGroupCount()+", counting children: "+adapter.getChildrenCount(0));
		listView.setOnChildClickListener(new OnChildClickListener(){

			@Override
			public boolean onChildClick(ExpandableListView listView, View v,
					int group, int pos, long id) {
				p("Got group "+group+", pos "+pos+", id"+id);
				if (group >=0 && pos >=0) {
					Port port = (Port) adapter.getChild(group, pos);
					if (null != mListener) {
						p("onChildClick port :"+port);
						mListener.onPortSelected(port);
					}
				}
				return false;
			}});
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	
		//lv.setDivider(null);
		//lv.setDividerHeight(0);
	}
	@Override
	public void onDetach() {
		super.onDetach();
		
		mListener = null;
	}


	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnPortFragmentListener {
		// 
		public void onPortSelected(Port port);
	}
	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(this.getActivity(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG) p("msg: " + s);
	}

	

}
