package com.auto.android.port;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.model.FlowManager;
import com.auto.model.Port;

public class PortArrayAdapter extends ArrayAdapter<Port> {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	
	
	private TextView txtmsg;
	private TextView txtinfo;
	private List<Port> datas = new ArrayList<Port>();

	private FlowManager man;
	
	@Override
	public void add(Port object) {
		man = FlowManager.getManager();
		datas.add(object);
		super.add(object);
	}

	public PortArrayAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	public int getCount() {
		return this.datas.size();
	}

	public Port getItem(int index) {
		return this.datas.get(index);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.port_li, parent, false);
		}
		Port port = getItem(position);
		
		txtmsg = (TextView) row.findViewById(R.id.lblname);	
		txtmsg.setText(port.getName());
							
		txtinfo = (TextView) row.findViewById(R.id.lbldetail);	
		if (port.getDescription() != null) {
			txtinfo.setText(port.getDescription());
		}
		else  {
			String desc = port.getStates().toString();			
			txtinfo.setText(desc);
		}
		ImageView image = (ImageView) row.findViewById(R.id.icon);
		image.setImageResource(AndroidGui.getDrawable(port));
		return row;
	}

	
	protected void p(String s) {
		Log.i(TAG, s);
	}

}