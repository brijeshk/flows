package com.auto.android.port;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.model.Port;
 
public class ExpandablePortListAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<String> types; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Port>> portmap;
 
    
    public ExpandablePortListAdapter(Context context) {
        this._context = context;
        this.types = new ArrayList<String>();
        this.portmap = new HashMap<String, List<Port>>();
    
    }
 
    public void clear() {
    	types.clear();
    	portmap.clear();
    }
    public List<Port> getPorts(String type) {
    	return portmap.get(type);
    }
    public int getNrPorts(String type) {		
	 	List<Port> plist = getPorts(type);
	 	if (plist == null) {
	 		return 0;
	 	}
	 	else return plist.size();
    }
 
    public void add(Port port) {
    	String type = port.getGrouping();
    	type = type+" "+_context.getString(R.string.ports);
    	
    	if (!types.contains(type)) types.add(type);
    	List<Port> plist = getPorts(type);
    	if (plist == null) {
    		plist = new ArrayList<Port>();
    		portmap.put(type, plist);
    	}
    	if (!plist.contains(port))plist.add(port);
    //	p("added type "+type);
    	this.notifyDataSetChanged();
    	
    }
    public void remove(Port port) {
    	String type = port.getType();
    	List<Port> plist = getPorts(type);
    	if (plist == null) {
    		plist = new ArrayList<Port>();
    		portmap.put(type, plist);
    	}
    	plist.remove(port);
    	this.notifyDataSetChanged();
    	
    }
    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.portmap.get(this.types.get(groupPosition))
                .get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition,
            boolean isLastChild, View row, ViewGroup parent) {
 
    	final Port port = (Port) getChild(groupPosition, childPosition);
        
        if (row == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = infalInflater.inflate(R.layout.port_li, null);
        }
        TextView  txtmsg = (TextView) row.findViewById(R.id.lblname);	
		txtmsg.setText(port.getName());
							
		TextView txtinfo = (TextView) row.findViewById(R.id.lbldetail);
		txtinfo.setText("");
		if (port.getDescription() != null && port.getDescription().length()>0) {
			txtinfo.setText(port.getDescription());
		}
		else  {
			if (port.isSimple()) {
				txtinfo.setText("simple "+_context.getString(R.string.port)+" without states");
			}
			else { 
				String desc = port.getStates().toString();		
				if (desc.length()>2) {
					txtinfo.setText(desc);
				}
			}
		}
		ImageView image = (ImageView) row.findViewById(R.id.icon);
		image.setImageResource(AndroidGui.getDrawable(port));
	//	p("Created child view for group "+groupPosition+", child ps "+childPosition);
        return row;
    }
   
    @Override
    public int getChildrenCount(int gp) {
    	//p("Got group Position:"+gp);
    	if (gp >=0 && gp < types.size()) {
	        int nr = this.portmap.get(this.types.get(gp))
	                .size();
	      //  p("Nr children for group "+gp+":"+nr);
	        return nr;
    	}
    	else return 0;
    }
 
    @Override
    public Object getGroup(int groupPosition) {
        return this.types.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        int nr= this.types.size();
     //   p("getGroupCount "+nr);
        return nr;
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
 
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
      //  p("Getting group view "+convertView);
        return convertView;
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
    protected void p(String s) {
		Log.i("ExpandablePortList", s);
	}

}
