package com.auto.android;

import android.app.Application;

import com.auto.model.FlowManager;

public class AutometonApp extends Application{
	
	FlowManager man;
	
	public FlowManager getManager() {
		return man;
	}
	
	public void setManager(FlowManager man) {
		this.man = man;
	}

}
