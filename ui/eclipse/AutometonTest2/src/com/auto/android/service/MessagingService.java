package com.auto.android.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.smack.SmackAndroid;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.auto.android.prefs.PrefUtil;
import com.auto.android.utils.AndroidFileTools;
import com.auto.model.Flow;
import com.auto.model.User;
import com.auto.net.AbstractChatService;
import com.auto.net.FlowChatListener;
import com.auto.net.FlowChatService;
import com.auto.net.WebSocketConnector;

public class MessagingService extends Service implements FlowChatListener {
	private static final String TAG = MessagingService.class.getSimpleName();

	private Timer timer;
	private AbstractChatService chatservice;
	
	private User me;
	
	private List<FlowChatListener> listeners = new ArrayList<FlowChatListener>();

	private final LocalBinder mBinder = new LocalBinder();

	private Activity context;
	private PrefUtil prefUtil;
	
	public boolean loginIfNeeded(Activity context, User user) {
		this.me = user;
		prefUtil = new PrefUtil(context);
		this.context = context;	
		if (chatservice == null) {
			java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
		    java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
			
			WebSocketConnector conn = new WebSocketConnector(prefUtil.getWebSocketUrl());			
			chatservice = new FlowChatService(conn, me, new AndroidFileTools(), this);
			conn.setChatServiceListener(chatservice);
		}				
		
		
		return true;
	}
		public void connect() {
		    Thread t = new Thread(new Runnable() {

		        @Override
		        public void run() {		            
		            SmackAndroid.init(context);
		            Log.i(TAG, "=================== Created Messaging Service that is listening to XMPP messages ======== ");
		            try {
		            	chatservice.connect();		            
		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		            Log.i(TAG, "=================== CONNECTED ======== ");
		        }
		    });
		    t.start();
		}
	
	public class LocalBinder extends Binder {
		public MessagingService getService() {		
			return MessagingService.this;
		}
	}
	@Override
	public void receive(String crud, List<Flow> flows) {
		Log.i(TAG, "======================================================================================");
		Log.i(TAG, "================================= received flows "+flows);
		for (FlowChatListener l: listeners) {
			l.receive(crud, flows);
		}
		
	}

	public void addListener(FlowChatListener l ) {
		if (!listeners.contains(l))this.listeners.add(l);
	}
	public void removeListener(FlowChatListener l ) {
		this.listeners.remove(l);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	private TimerTask updateTask = new TimerTask() {
		@Override
		public void run() {
			//Log.i(TAG, "			updateTask: check db for new messages? listeners: "+listeners);
		}
	};

	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(TAG, "Service creating");
		
		timer = new Timer("MessagingService");
		timer.schedule(updateTask, 1000L, 30 * 1000L);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "Service destroying");

		timer.cancel();
		timer = null;
	}
	
}
