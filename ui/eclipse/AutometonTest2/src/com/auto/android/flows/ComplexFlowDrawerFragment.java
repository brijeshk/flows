package com.auto.android.flows;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.android.contact.ContactActivity;
import com.auto.android.flows.drawer.SimpleGroupArrayAdapter;
import com.auto.android.flows.drawer.SimplePortArrayAdapter;
import com.auto.android.flows.drawer.SimpleUserArrayAdapter;
import com.auto.android.login.LoginActivity;
import com.auto.android.model.InputManager;
import com.auto.model.Contact;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import com.auto.model.Group;
import com.auto.model.Port;
import com.auto.model.User;

/**
 * Fragment used for managing interactions for and presentation of a navigation
 * drawer. See the <a href=
 * "https://developer.android.com/design/patterns/navigation-drawer.html#Interaction"
 * > design guidelines</a> for a complete explanation of the behaviors
 * implemented here.
 */
public class ComplexFlowDrawerFragment extends Fragment {

	/**
	 * Remember the position of the selected item.
	 */
	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	/**
	 * Per the design guidelines, you should show the drawer on launch until the
	 * user manually expands it. This shared preference tracks this.
	 */
	private static final String PREF_USER_LEARNED_DRAWER = "flow_drawer_learned";

	/**
	 * A pointer to the current callbacks instance (the Activity).
	 */
	private NavigationDrawerCallbacks mCallbacks;

	/**
	 * Helper component that ties the action bar to the navigation drawer.
	 */
	private ActionBarDrawerToggle mDrawerToggle;

	private DrawerLayout mDrawerLayout;
	
	private View mFragmentContainerView;

	private int currentSelectedPort = 0;
	private boolean mFromSavedInstanceState;
	private boolean mUserLearnedDrawer;

	private ListView usedportListView;
	private ListView userListView;
	
	private SimplePortArrayAdapter usedportAdapter;
	private SimpleUserArrayAdapter userAdapter;
	
	private InputManager inputmanager;
	
	public ComplexFlowDrawerFragment() {
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Read in the flag indicating whether or not the user has demonstrated
		// awareness of the
		// drawer. See PREF_USER_LEARNED_DRAWER for details.
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

		if (savedInstanceState != null) {
			currentSelectedPort = savedInstanceState
					.getInt(STATE_SELECTED_POSITION);
			mFromSavedInstanceState = true;
		}
		

		// Select either the default item (0) or the last selected item.
	//	selectPort(currentSelectedPort);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of
		// actions in the action bar.
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view =  inflater.inflate(
				R.layout.complex_flow_navigation_drawer, container, false);
				
		
		 ClickableSpan laterSpan=new ClickableSpan(){
			    @Override public void onClick(    View widget){
			      //loginLaterListener.onClick(widget);
			    }
			    @Override public void updateDrawState(    TextPaint ds){
			      ds.setUnderlineText(true);
			      ds.setColor(Color.rgb(68,68,68));
			    }
			  };
		
		 Button flowactionspinner = (Button)view.findViewById(R.id.buttonactions);
		 
		 String[] items = new String[] {"Show all threads", "Filter threads...", "Group threads..."};
		 final ArrayAdapter<String> spinneradapter = new ArrayAdapter<String>(this.getActivity(),
		         android.R.layout.simple_spinner_dropdown_item, items);
		 
		 flowactionspinner.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				new AlertDialog.Builder(getActivity())
				  .setTitle("Pick an action")
				  .setAdapter(spinneradapter, new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				    	selectSpinner(spinneradapter.getItem(which));
				      dialog.dismiss();
				    }
				  }).create().show();				
			}
			 
		 });
		 
		 ImageView iconme = (ImageView) view.findViewById(R.id.iconme);		
		 final User me = FlowManager.getManager().getMe();
		 iconme.setImageResource(AndroidGui.getDrawable(me));
		 iconme.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// show contacts page of me
				Intent intent = new Intent(getActivity(), ContactActivity.class);
				intent.putExtra("USER", me);
				startActivity(intent);
			}
			 
		 });
		 
		 
		 
		 TextView contacts = (TextView) view.findViewById(R.id.txtcontacts);
		 TextView discover = (TextView) view.findViewById(R.id.txtdiscover);
		 TextView expose = (TextView) view.findViewById(R.id.txtexpose);
		 expose.setVisibility(View.GONE);
		 discover.setVisibility(View.GONE);
		 contacts.setVisibility(View.GONE);
		 contacts.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					close();
					if (mCallbacks != null) {					
						mCallbacks.onNavigationDrawerAction("contacts");
					}
				}				
			 });
		 discover.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					close();
					if (mCallbacks != null) {					
						mCallbacks.onNavigationDrawerAction("discover");
					}
				}				
			 });
		 
		 expose.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					close();
					if (mCallbacks != null) {					
						mCallbacks.onNavigationDrawerAction("expose");
					}
				}				
			 });
		 
		 TextView userallflows = (TextView) view.findViewById(R.id.userallflows);
		 TextView gateallflows = (TextView) view.findViewById(R.id.gateallflows);
		 userallflows.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				close();
				if (mCallbacks != null) {
					// group by user
					mCallbacks.onNavigationDrawerGroupBySelected("users");
				}
			}
			 
		 });
		 
		 gateallflows.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					close();
					if (mCallbacks != null) {
						// group by user
						mCallbacks.onNavigationDrawerGroupBySelected("gates");
					}
				}
				 
			 });
		 
		usedportListView = (ListView) view.findViewById(R.id.portlistused);
		userListView = (ListView) view.findViewById(R.id.userlist);
		
		usedportAdapter = new SimplePortArrayAdapter(this.getActivity());
		userAdapter = new SimpleUserArrayAdapter(this.getActivity());
		
		usedportListView.setAdapter(usedportAdapter);
		userListView.setAdapter(userAdapter);
		
	
		usedportListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectPort(position);
			}
		});
		userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectUser(position);
			}
		});
		
		
		/* currently not showing my ports... remove code once we are sure */
			
		
		FlowManager man = FlowManager.getManager();
		
		List<Flow> flows = man.getFlows();
		List<User> users = new ArrayList<User>();
		ArrayList<Port> ports = new ArrayList<Port>();
		for (Flow f: flows ) {
			List<Contact> fusers = f.getUsers();
			for (Contact c: fusers) {
				if (c instanceof User) {
					User u = (User)c;
					if (!users.contains(u)) users.add(u);
				}
			}
			Port port = f.getPort();
			if (port != null && !ports.contains(port))ports.add(port);
		}
		for (Port p: ports) {
			usedportAdapter.add(p);
		}
		for (User u: users) {
			userAdapter.add(u);
		}
//		if (man.getGroups() != null) {
//			for (Group g: man.getGroups()) {
//				groupAdapter.add(g);
//			}
//		}
		
		return view;
	}
	private void selectPort(int position) {
		currentSelectedPort = position;
		if (this.usedportListView != null) {
			this.usedportListView.setItemChecked(position, true);
		}
		close();
		if (mCallbacks != null) {
			mCallbacks.onNavigationDrawerPortSelected(usedportAdapter.getItem(position));
		}
	}
	private void selectUser(int position) {
		currentSelectedPort = position;
		if (userListView != null) {
			userListView.setItemChecked(position, true);
		}
		close();
		if (mCallbacks != null) {
			mCallbacks.onNavigationDrawerUserSelected(userAdapter.getItem(position));
		}
	}
	
	private void close() {
		if (mDrawerLayout != null) {
			mDrawerLayout.closeDrawer(mFragmentContainerView);
		}
	}
	private void selectSpinner(String action) {		
		close();
		if (mCallbacks != null) {
			mCallbacks.onNavigationDrawerSpinnerSelected(action);
		}
	}

	public boolean isDrawerOpen() {
		return mDrawerLayout != null
				&& mDrawerLayout.isDrawerOpen(mFragmentContainerView);
	}

	/**
	 * Users of this fragment must call this method to set up the navigation
	 * drawer interactions.
	 *
	 * @param fragmentId
	 *            The android:id of this fragment in its activity's layout.
	 * @param drawerLayout
	 *            The DrawerLayout containing this fragment's UI.
	 */
	public void setUp(int fragmentId, DrawerLayout drawerLayout) {
		mFragmentContainerView = getActivity().findViewById(fragmentId);
		mDrawerLayout = drawerLayout;

		// set a custom shadow that overlays the main content when the drawer
		// opens
		//mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);
		
		Drawable background = mDrawerLayout.getBackground();
		if (background != null) background.setAlpha(255);
		mDrawerLayout.setScrimColor(Color.parseColor("#CCCCCCCC"));
		// set up the drawer's list view with items and click listener

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the navigation drawer and the action bar app icon.
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.navigation_drawer_open, /*
										 * "open drawer" description for
										 * accessibility
										 */
		R.string.navigation_drawer_close /*
										 * "close drawer" description for
										 * accessibility
										 */
		) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				if (!isAdded()) {
					return;
				}

				getActivity().invalidateOptionsMenu(); // calls
														// onPrepareOptionsMenu()
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if (!isAdded()) {
					return;
				}

				if (!mUserLearnedDrawer) {
					// The user manually opened the drawer; store this flag to
					// prevent auto-showing
					// the navigation drawer automatically in the future.
					mUserLearnedDrawer = true;
					SharedPreferences sp = PreferenceManager
							.getDefaultSharedPreferences(getActivity());
					sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true)
							.apply();
				}

				getActivity().invalidateOptionsMenu(); // calls
														// onPrepareOptionsMenu()
			}
		};

		// If the user hasn't 'learned' about the drawer, open it to introduce
		// them to the drawer,
		// per the navigation drawer design guidelines.
//		if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
//			mDrawerLayout.openDrawer(mFragmentContainerView);
//		}

		// Defer code dependent on restoration of previous instance state.
		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});

		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.inputmanager = new InputManager(activity);
		try {
			mCallbacks = (NavigationDrawerCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					"Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, currentSelectedPort);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Forward the new configuration the drawer toggle component.
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// If the drawer is open, show the global app actions in the action bar.
		// See also
		// showGlobalContextActionBar, which controls the top-left area of the
		// action bar.
		if (mDrawerLayout != null && isDrawerOpen()) {
			inflater.inflate(R.menu.global, menu);
			showGlobalContextActionBar();
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	//	log.i("onOptionsItemSelected");
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		if (item.getItemId() == R.id.action_flows) {
			Toast.makeText(getActivity(), "Thread action", Toast.LENGTH_SHORT)
					.show();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Per the navigation drawer design guidelines, updates the action bar to
	 * show the global app 'context', rather than just what's in the current
	 * screen.
	 */
	private void showGlobalContextActionBar() {
		ActionBar actionBar = getActionBar();
	//	actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	//	actionBar.setTitle(R.string.app_name);
	}

	private ActionBar getActionBar() {
		return getActivity().getActionBar();
	}

	/**
	 * Callbacks interface that all activities using this fragment must
	 * implement.
	 */
	public static interface NavigationDrawerCallbacks {
		/**
		 * Called when an item in the navigation drawer is selected.
		 */
		void onNavigationDrawerGroupBySelected(String groupby);
		void onNavigationDrawerAction(String action);
		void onNavigationDrawerPortSelected(Port port);
		void onNavigationDrawerUserSelected(User user);
		void onNavigationDrawerGroupSelected(Group group);
		void onNavigationDrawerSpinnerSelected(String action);
	}
	
	  protected void p(String s) {
			Log.i("ComplexFlowDrawerFragment", s);
		}
}
