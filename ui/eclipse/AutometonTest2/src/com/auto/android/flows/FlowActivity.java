package com.auto.android.flows;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.auto.android.AutometonActivity;
import com.auto.android.R;
import com.auto.android.atoms.AtomlistFragment;
import com.auto.android.contact.ContactActivity;
import com.auto.android.discover.DiscoverActivity;
import com.auto.android.expose.ExposeActivity;
import com.auto.android.service.MessagingService;
import com.auto.android.service.MessagingService.LocalBinder;
import com.auto.model.Atom;
import com.auto.model.Flow;
import com.auto.model.FlowFilter;
import com.auto.model.FlowGroup;
import com.auto.model.FlowManager;
import com.auto.model.Group;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import com.auto.net.FlowChatListener;

public class FlowActivity extends AutometonActivity implements FlowChatListener,
		FlowlistFragment.OnFlowFragmentListener,
		AtomlistFragment.OnAtomFragmentListener,
		SimpleFlowDrawerFragment.NavigationDrawerCallbacks {

	FlowlistFragment flowFragment;
	AtomlistFragment atomFragment;
	MessagingService mService;
	FlowFilter filter;
	GestureDetector gesture;
	FlowGroup group;
	private SimpleFlowDrawerFragment mNavigationDrawerFragment;

	boolean mBound = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("onCreate: =========== Creating FlowActivity=========== ");
		this.setContentView(R.layout.flow_activity);
		man = FlowManager.getManager();
		man.addChatListener(this);
		// Check that the activity is using the layout version with
		// the fragment_container FrameLayout
		gesture = new GestureDetector(this,
	            new GestureDetector.SimpleOnGestureListener() {

	                @Override
	                public boolean onDown(MotionEvent e) {
	                    return true;
	                }
	                
	                @Override
	                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
	                    float velocityY) {
	                    p("GestureDetector: onFling has been called!");
	                    final int SWIPE_MIN_DISTANCE = 120;
	                    final int SWIPE_MAX_OFF_PATH = 250;
	                    final int SWIPE_THRESHOLD_VELOCITY = 200;
	                    try {
	                        if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
	                            return false;
	                        if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
	                            && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                            p("Right to Left");
	                            nextOrPrevAction(false);
	                        } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
	                            && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                            p("Left to Right");
	                            nextOrPrevAction(true);
	                        }
	                    } catch (Exception e) {
	                        // nothing
	                    }
	                    return super.onFling(e1, e2, velocityX, velocityY);
	                }
	            });
		// intent.putExtra("ACTION", "CREATE");
		// intent.putExtra("USER_ID", user.getId());
		Bundle extras = getIntent().getExtras();
		
		String action = null;
		User user = null;
		Port port = null;
		String searchstring = null;
		//intent.putExtra("ACTION", "FIND");
		//intent.putExtra("FINDSTRING", txt);
		if (extras != null) {
			action = extras.getString("ACTION");		
			String userid = extras.getString("USER_ID");
			searchstring = extras.getString("FINDSTRING");
			if (userid != null) {
				user = man.getUser(userid);
				p("GOT USER ID FROM INTENT: "+userid+", user="+user);
			}
			String portid = extras.getString("PORT_ID");	
			if (portid != null) {
				port = man.getPort(Integer.parseInt(portid));
			}
			p("onCreate:======= Got extras: user="+user+", gate="+port+", actio="+action);
		}
		
		if (savedInstanceState != null) {
			p("onCreate:  got a savedInstanceState, not adding fragment again");
		} 
		else { 
			this.hideVoice();
			flowFragment = new FlowlistFragment();
			flowFragment.setFilter(filter);
			flowFragment.setGroup(group);
			flowFragment.setArguments(getIntent().getExtras());
			getFragmentManager().beginTransaction()
					.add(R.id.flow_fragment_container, flowFragment)
					.commit();
		}
		
		if (action != null && action.equalsIgnoreCase("create")) {
			p("onCreate: create action: starting atom fragment");
			showAtomFragment(null);
			atomFragment.newMessageAction(user, port);
		}
		else {			
			if (searchstring != null) {
				p("onCreate:Got a search string "+searchstring+", showing search box");
				//searchtextview.setText(searchstring);
				searchAction(searchstring);
			}
		}
		

		mNavigationDrawerFragment = (SimpleFlowDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
		
		View v = findViewById(R.id.flow_fragment_container);
		v.setOnTouchListener(new View.OnTouchListener() {
	            @Override
	            public boolean onTouch(View v, MotionEvent event) {
	                return gesture.onTouchEvent(event);
	            }
	        });

	}
	

       
	@Override
	public boolean onTouchEvent(MotionEvent event){ 
	        
	    int action = MotionEventCompat.getActionMasked(event);
	        
	    switch(action) {
	        case (MotionEvent.ACTION_DOWN) :
	            p("onTouchEvent: Action was DOWN");
	     
	            return true;
	        case (MotionEvent.ACTION_MOVE) :
	        	p("onTouchEvent: Action was MOVE");
	            return true;
	        case (MotionEvent.ACTION_UP) :
	        	p("onTouchEvent: Action was UP");
	            return true;
	        case (MotionEvent.ACTION_CANCEL) :
	        	p("onTouchEvent: Action was CANCEL");
	            return true;
	        case (MotionEvent.ACTION_OUTSIDE) :
	        	p("onTouchEvent: Movement occurred outside bounds " +
	                    "of current screen element");
	            return true;      
	        default : 
	            return super.onTouchEvent(event);
	    }      
	}
	
	private void nextOrPrevAction(boolean next) {
		if (this.isAtomfragmentShowing()) {
			p("Showing next message");
			List<Flow> shownflows = this.flowFragment.getAllFlows();
			Flow curflow = this.atomFragment.getFlow();
			if (curflow != null) {
				int where = shownflows.indexOf(curflow);
				if (next) {
					where++;				
					if (where >= shownflows.size()) where = 0;
				}
				else {
					where--;
					if (where < 0) where = shownflows.size()-1;
				}
				this.showAtomFragment(shownflows.get(where));
			}
			
		}
	}

	@Override
	public void onBackPressed()
	{
	    p("on back pressed");
	   
	    super.onBackPressed();
	    

	}

	public void homeClicked() {
		 p("Home pressed. Show flow fragment if we are in the atom list, else open drawer");
	     if (this.flowFragment.isVisible()) {
	    	 mNavigationDrawerFragment.toggle();
	     }
	     else this.showFlowFragment();
	     
	}
	protected void userSaid(ArrayList<String> result) {
		msg(result.get(0));
		for (String s : result) {
			p("User might have said: " + s);
		}
		if (this.atomFragment != null && atomFragment.isVisible()) {
			atomFragment.userSaid(result);
		}
		
	}
	@Override
	protected void searchAction(String txt) {
		if (txt != null) {
			// find messages
			if (!this.menuSearch.isVisible()) {
							
				showSearch();
			}
			p("Finding messages containing "+txt);
			filter = new FlowFilter();
			filter.setSearchstring(txt);
			flowFragment.setFilter(filter);
			showFlowFragment();
		}
		
	}

	protected void newObjectAction() {
		p("newObjectAction: User picks new action");
		showAtomFragment(null);
		atomFragment.newMessageAction(null, null);
		//atomFragment.checkifNewAction();

	}

	@Override
	protected void onStart() {
		super.onStart();
		// Bind to LocalService
		Intent intent = new Intent(this, MessagingService.class);
		p("Binding to messaging service...");
		bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	}

	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// We've bound to LocalService, cast the IBinder and get
			// LocalService instance
			LocalBinder binder = (LocalBinder) service;
			mService = (MessagingService) binder.getService();
			if (man == null)
				man = FlowManager.getManager();
			p("============== Connecting to Messaging service, user "
					+ man.getMe());
			mService.addListener(FlowActivity.this);
			mService.loginIfNeeded(FlowActivity.this, man.getMe());
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBound = false;
		}
	};

	@Override
	public void receive(String crud, List<Flow> flows) {
	//	p("=================== receive: got flows: "+flows);
		//already did this in manager before it got here man.update(flow, crud);
		runOnUiThread(new Runnable() {
			public void run() {
				p("updating flows in flowFragment");
				flowFragment.addAllFlows();
				
			}
		});
	}

	@Override
	protected void onStop() {
		super.onStop();
		// Unbind from the service
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
	}

	@Override
	public void onFlowSelected(Flow flow) {
	//	p("Got flow from flow fragment: " + flow);		
		showAtomFragment(flow);
	}

	private boolean isAtomfragmentShowing() {
		return (atomFragment != null && atomFragment.isVisible());
	}
	private void showAtomFragment(Flow flow) {		
		p("showAtomFragment: Showing flow "+flow);
		hideSearch();
		this.showVoice();
		if (isAtomfragmentShowing()) {
			p("showAtomFragment Already have atom fragment, updating view");
			atomFragment.updateView(flow);
		} else {
			if (atomFragment == null) {
				p("atom fragment is null, creating new one");
				atomFragment = new AtomlistFragment(flow);	
				atomFragment.setGestureDetector(gesture);
				Bundle args = new Bundle();
				args.putSerializable(AtomlistFragment.ARG_PARAM1, flow);
				atomFragment.setArguments(args);
				atomFragment.setArguments(getIntent().getExtras());
				
			}
			else {
				p("Already have atom fragment, updating view");
				atomFragment.updateView(flow);
			}
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			p("FragmentTransaction: showing atom fragment");
			transaction.replace(R.id.flow_fragment_container, atomFragment);
			transaction.addToBackStack(null);
			transaction.commit();
			
		}
	}

	@Override
	public void onFragmentInteraction(Atom atom) {
		p("User clicked on atom " + atom);

	}

	@Override
	public void onNavigationDrawerSpinnerSelected(String action) {
		p(" ========================== User picked action: " + action);
//		msg("Handle action " + action);
		// currently just show all flows
		action = action.toLowerCase();
		// if filter > ask which filter
		// otherwise show all
		filter = new FlowFilter();
		flowFragment.setFilter(filter);
		showFlowFragment();

	}

	@Override
	public void onNavigationDrawerPortSelected(Port port) {
		p(" ========================== User picked a port: " + port);
		msg("filtering flows by gate " + port.getName());
		filter = new FlowFilter();
		filter.setPort(port);
		flowFragment.setFilter(filter);
		showFlowFragment();
	}

	@Override
	public void onNavigationDrawerUserSelected(User user) {
		p(" ========================== User picked a user: " + user);

		filter = new FlowFilter();
		filter.setUser(user);
		flowFragment.setFilter(filter);
		showFlowFragment();
	}

	private void showFlowFragment() {

		if (flowFragment.isVisible()) return;
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.flow_fragment_container, flowFragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}

	@Override
	public void onNavigationDrawerGroupSelected(Group group) {
		p(" ========================== User picked a group: " + group);
		filter = new FlowFilter();
		filter.setUsers(group.getUsers());
		showFlowFragment();
	}

	/** CURRENTLY NOT CALLED! */
	public void onNavigationDrawerItemSelected(int position) {
		p(" ========================== User picked a flow order or filter, pos: "
				+ position);

		filter = null;
		group = null;
		if (position == 0) {
			filter = null;
		} else if (position == 1) {
			filter = new FlowFilter();
			filter.setUser(man.getFlows().get(0).getCreator());
			msg("Filtering by user " + filter.getUsers());
		} else if (position == 2) { // topic
			filter = new FlowFilter();

			filter.setPort(man.getPortByName("General"));
			msg("Filtering by port " + filter.getPort() + " for now");
		} else if (position == 3) { // date
			filter = new FlowFilter();
			msg("Filtering by one month ago for now");
			long DAY = 3600 * 1000 * 24;
			filter.setStartDate(new Date(System.currentTimeMillis() - DAY * 30));
		} else if (position == 4) { // contact
			group = new FlowGroup();
			group.setUsers(true);
			msg("Grouping by user");

		} else if (position == 5) { // date
			group = new FlowGroup();
			group.setDate(true);
			msg("Grouping by date");

		} else if (position == 6) { // topic
			group = new FlowGroup();
			group.setPort(true);
			msg("Grouping by " + getString(R.string.port));

		} else if (position == 7) { // state
			group = new FlowGroup();
			group.setState(true);
			msg("Grouping by state");

		} else {
			msg("Unknown selection for filter or gorup: " + position);
		}
		if (flowFragment != null) {
			flowFragment.setFilter(filter);
			flowFragment.setGroup(group);
			p("Recreating flow view by clearing list and adding all flows (filtered and grouped)");

		} else
			p("Flow fragment is still null, not filtering yet");

	}

	@Override
	public void onNavigationDrawerAction(String action) {
		p(" ========================== User picked a action: " + action);
		if (action.startsWith("cont")) {
			this.startActivity(new Intent(this, ContactActivity.class));
			return;
		} else if (action.startsWith("ex")) {
			this.startActivity(new Intent(this, ExposeActivity.class));
			return;
		} else if (action.startsWith("disc")) {
			this.startActivity(new Intent(this, DiscoverActivity.class));
			return;
		}
		else if (action.startsWith("todo")) {
			// filter
	//		p("Filtering by todo");
			filter = new FlowFilter();
			filter.setTodo(true);
		}
		else if (action.startsWith("all")) {
			// filter
	//		p("Showing all threads");
			filter = null;
			
		}
		else if (action.startsWith("waiting")) {
			// filter
	//		p("Filtering by waiting");
			filter = new FlowFilter();
			filter.setWaiting(true);
		}
		if (flowFragment != null) {
			flowFragment.setFilter(filter);
			p("Recreating flow view by clearing list and adding all flows (filtered and grouped)");

		} else
			p("Flow fragment is still null, not filtering yet");

	}

	@Override
	public void onNavigationDrawerGroupBySelected(String groupby) {
		p(" ========================== User picked a groupby: " + groupby);
		p("grouping flows by " + groupby);
		// clear filter ?
		
		group = new FlowGroup();
		if (groupby.equalsIgnoreCase("gates")) {
			group.setPort(true);
		} else if (groupby.equalsIgnoreCase("date")) {
			group.setDate(true);
		} else if (groupby.equalsIgnoreCase("users")) {
			group.setUsers(true);
		} 
		else if (groupby.equalsIgnoreCase("date")) {
			group.setDate(true);
		}
		else if (groupby.equalsIgnoreCase("state")) {
			group.setState(true);
		}
		
		flowFragment.setGroup(group);
		
	}

}