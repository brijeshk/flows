package com.auto.android.flows.drawer;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.auto.android.R;
import com.auto.model.User;

public class SimpleUserArrayAdapter extends ArrayAdapter<User> {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	
	
	private TextView txtmsg;
	private List<User> datas = new ArrayList<User>();
	
	@Override
	public void add(User object) {
		datas.add(object);
		super.add(object);
	}

	public SimpleUserArrayAdapter(Context context) {
		super(context, android.R.id.text1);
	}

	public int getCount() {
		return this.datas.size();
	}

	public User getItem(int index) {
		return this.datas.get(index);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.user_li_simple, parent, false);
		}
		User user = getItem(position);
		
		txtmsg = (TextView) row.findViewById(R.id.lblname);	
		txtmsg.setText("@"+user.getName());
									
		return row;
	}

	
	protected void p(String s) {
		Log.i(TAG, s);
	}

}