package com.auto.android.flows;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.android.prefs.PrefUtil;
import com.auto.android.utils.UiUtils;
import com.auto.model.Atom;
import com.auto.model.Contact;
import com.auto.model.Flow;
import com.auto.model.FlowGroup;
import com.auto.model.FlowManager;
import com.auto.model.User;
import com.auto.utils.StringTools;

public class ExpandableFlowListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<String> types; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<Flow>> typemap;
	private PrefUtil prefUtil;
	private FlowGroup group;
	public ExpandableFlowListAdapter(Context context, FlowGroup group) {
		this._context = context;
		this.group = group;
		this.types = new ArrayList<String>();
		this.typemap = new HashMap<String, List<Flow>>();
		this.prefUtil = new PrefUtil(context);
	}

	public void clear() {
		types.clear();
		typemap.clear();
	}

	public List<Flow> get(String type) {
		return typemap.get(type);
	}	

	public int getNr(String type) {
		List<Flow> plist = get(type);
		if (plist == null) {
			return 0;
		} else
			return plist.size();
	}

	public void add(Flow child) {
		if (group != null && group.isUsers()) {
			for (Contact c: child.getUsers()) {
				if (c instanceof User) {
					User u = (User)c;
					if (FlowManager.getManager().isMe(u)) {
						// don't add myself
						continue;
					}
				}
				String type = c.getName();
				add(child, type);
			}
		}
		else {
			String type = getType(child);
			add(child, type);
		}
				
	}

	public List<Flow> getAllFlows() {
		ArrayList<Flow> flows = new ArrayList<Flow>();
		
		for (int t = 0; t < types.size(); t++) {
			String type = types.get(t);
			List<Flow> plist = get(type);
			if (plist != null) {
				flows.addAll(plist);
			}
		}
		return flows;
		
	}
	private void add(Flow child, String type) {
		if (!types.contains(type)) {
			types.add(type);
			
		}
		List<Flow> plist = get(type);
		if (plist == null) {
			plist = new ArrayList<Flow>();
			typemap.put(type, plist);
		}
		if (!plist.contains(child)) {
			plist.add(child);
			//p("add: got type " + type+", name ");
		}
	}
	public String getType(Flow child) {
		
		String type = "";
		if (child.getPort() != null) {
			type = child.getPort().getName();
		}
		else {
			p("Flow has no Port: "+child.getCreator());
			type = child.getCreator().getName();
		}
		if (group == null) {
			//p("Creating default grouping based on preferences");
			group = prefUtil.getDefaultGroup();
		}
		if (group != null) {
			if (group.isPort())
				 type = "Gate "+ child.getPort().getName();
			else if (group.isState())
				if (child.getState() == null) {
					p("Error: flow has no state: "+child);
					type = "Say";
				}
				else type = "State "+ child.getState().getName();			
			else if (group.isDate()) {
				Atom at = child.getFirstAtom();
				if (at != null) {
					Date date = at.getCreated();
					Calendar cal =GregorianCalendar.getInstance();
					cal.setTime(date);
					type = cal.getDisplayName(GregorianCalendar.MONTH, GregorianCalendar.LONG, Locale.getDefault());
				}
				
			}
		}
		return type;
	}
	public void remove(Flow child) {
		String type = getType(child);
		List<Flow> plist = get(type);
		if (plist == null) {
			plist = new ArrayList<Flow>();
			typemap.put(type, plist);
		}
		plist.remove(child);
		this.notifyDataSetChanged();

	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this.typemap.get(this.types.get(groupPosition)).get(
				childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public Flow getFLow(int group, int child) {
		return (Flow) getChild(group, child);
	}
	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View row, ViewGroup parent) {

		final Flow flow = (Flow) getChild(groupPosition, childPosition);

		if (row == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = infalInflater.inflate(R.layout.flow_li, null);
		}
		// TODO
		
		ImageView image = (ImageView) row.findViewById(R.id.icon);
		
		ImageView imagelbl= (ImageView) row.findViewById(R.id.iconlabel);
		if (flow.isStarred()) {
			imagelbl.setImageResource(AndroidGui.getStar());
		}
		else imagelbl.setVisibility(View.GONE);
		
		// image.setBackgroundDrawable(AndroidGui.getDrawable(user));
		if (flow.getCreator() != null){
			image.setImageResource(AndroidGui.getDrawable(flow.getCreator()));
		}
		TextView lblport = (TextView) row.findViewById(R.id.lblport);
		lblport.setText(flow.getPort().getName());

		TextView lblsub = (TextView) row.findViewById(R.id.lblsubject);
		if (flow.hasSubject()) {
			lblsub.setText(flow.getSubject());
		}
		else lblsub.setVisibility(View.GONE);
		
		
		
		TextView lblstate = (TextView) row.findViewById(R.id.lblstate);
		String statecolor = UiUtils.getStateColor(_context, flow.getState());
		
		if (flow.getState() != null) {			
			lblstate.setText(Html.fromHtml(statecolor+flow.getState().getName()+"</font>"), TextView.BufferType.SPANNABLE);
		}
		else lblstate.setVisibility(View.GONE);
		
		TextView lblusers= (TextView) row.findViewById(R.id.lblusers);
		String others = flow.getOtherUsers();
				
		int unread = flow.getNrUnread();
		if (unread > 0) {
			others +=" (<b>"+unread+"</b>/"+flow.getNrAtoms()+")";
		}
		else others +=" ("+flow.getNrAtoms()+")";
		lblusers.setText(Html.fromHtml(others), TextView.BufferType.SPANNABLE);
				
		
		TextView lbldata = (TextView) row.findViewById(R.id.lbldata);
		if (flow.getLastAtom() != null) {
			Atom last  = flow.getLastAtom();
			String msg = last.getBody();
			if (msg == null || msg.equals("null")) {
				msg = "";
			}
			if (msg.length()>30) {
				msg = msg.substring(0, 30)+"...";
			}
			else msg = msg +" ";
			
			lbldata.setText( msg);
		}
		
		TextView txtdate = (TextView) row.findViewById(R.id.lbl_flow_date);
		
		String sdate = StringTools.getSmartDateString(flow.getLastAtom().getCreated());		
		// if date is just minutes, no line		
		txtdate.setText(sdate);
		
		TextView colorbar = (TextView) row.findViewById(R.id.colorbar);
		int barcolor = UiUtils.getStateBarColor(_context, flow.getState());
		colorbar.setBackgroundResource(barcolor);

		return row;
	}

	@Override
	public int getChildrenCount(int groupPosition) {

		int nr = this.typemap.get(this.types.get(groupPosition)).size();
	//	p("Nr children for group " + groupPosition + ":" + nr);
		return nr;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this.types.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		int nr = this.types.size();
		// p("getGroupCount "+nr);
		return nr;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		//lblListHeader.setTypeface(null, Typeface.BOLD);
		int nrflows = this.getChildrenCount(groupPosition);
		
		int unread = 0;
		for (int f = 0; f < nrflows; f++) {
			Flow flow = (Flow)this.getChild(groupPosition, f);
			unread += flow.getNrUnread();
		}
		if (unread > 0) {
			headerTitle+=" ("+unread+")";
		}
		if (!isExpanded) headerTitle = "+ " + headerTitle;
		else headerTitle = "- " + headerTitle;
		lblListHeader.setText(headerTitle);
		
		//ExpandableListView eLV = (ExpandableListView) parent;
	   // eLV.expandGroup(groupPosition);
		// p("Getting group view "+convertView);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	protected void p(String s) {
		Log.i("ExpandableFlowList", s);
	}

}
