package com.auto.android.flows.drawer;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.auto.android.R;
import com.auto.model.Port;

public class SimplePortArrayAdapter extends ArrayAdapter<Port> {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	
	
	private TextView txtmsg;
	private List<Port> datas = new ArrayList<Port>();
	
	@Override
	public void add(Port object) {
		datas.add(object);
		super.add(object);
	}

	public SimplePortArrayAdapter(Context context) {
		super(context, android.R.id.text1);
	}

	public int getCount() {
		return this.datas.size();
	}

	public Port getItem(int index) {
		if (index <= datas.size()) return this.datas.get(index);
		else return null;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.port_li_simple, parent, false);
		}
		Port port = getItem(position);
		
		txtmsg = (TextView) row.findViewById(R.id.lblname);	
		txtmsg.setText(port.getName());
									
		return row;
	}

	
	protected void p(String s) {
		Log.i(TAG, s);
	}

}