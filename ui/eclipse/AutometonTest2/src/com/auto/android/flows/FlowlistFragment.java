package com.auto.android.flows;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.android.prefs.PrefUtil;
import com.auto.android.utils.UiUtils;
import com.auto.android.utils.UiUtils.ItemsSelected;
import com.auto.model.Atom;
import com.auto.model.Contact;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowFilter;
import com.auto.model.FlowGroup;
import com.auto.model.FlowManager;
import com.auto.model.Group;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;

/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class FlowlistFragment extends Fragment implements
		AdapterView.OnItemLongClickListener {

	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;
	private UiUtils utils;

	private static FlowManager man;
	private AutometonApp app;
	private PrefUtil prefUtil;
	private OnFlowFragmentListener mListener;

	private Contact curcontact;
	private Port curport;
	private String curaction;
	
	private List<Contact> users;
	private Port port;
	private State state;
	private List<InputData> datas;
	private String usermessage;

	public static final String ARG_PARAM1 = "flowfilter";
	public static final String ARG_PARAM2 = "flowgroup";
	private Activity activity;

	private List<Map<String, ?>> flowdata;
	private FlowFilter filter;

	private TextView headerView;
	private FlowGroup group;

	private ExpandableListView listView;
	private ExpandableFlowListAdapter adapter;

	private PrefUtil prefutil;

	// TODO: Rename and change types of parameters
	public static FlowlistFragment newInstance(FlowFilter filter,
			FlowGroup group) {
		man = FlowManager.getManager();
		
		// get default grfouping from preferences
		FlowlistFragment fragment = new FlowlistFragment(filter, group);
		fragment.p("0. newInstance");
		Bundle args = new Bundle();
		args.putSerializable(ARG_PARAM1, (Serializable) filter);
		args.putSerializable(ARG_PARAM2, (Serializable) group);
		fragment.setArguments(args);

		return fragment;
	}

	public FlowlistFragment(FlowFilter filter, FlowGroup group) {
		this.filter = filter;
		this.group = group;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public FlowlistFragment() {

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		utils = new UiUtils(activity);
		prefUtil = new PrefUtil(activity);
		p("1. onAttach");
		prefutil = new PrefUtil(activity);
		try {
			mListener = (OnFlowFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
		this.activity = activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		p("2. onCreate");
		if (getArguments() != null) {
			filter = (FlowFilter) getArguments().getSerializable(ARG_PARAM1);
			group = (FlowGroup) getArguments().getSerializable(ARG_PARAM2);
		//	p("Got filter: " + filter);
		//	p("Got group: " + group);
		}
		app = (AutometonApp) this.getActivity().getApplication();
		man = app.getManager();

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		p("Long item click");
		return false; // let the system show the context menu
	}

	public void addAllFlows() {
		if (man == null)
			return;
	//	p("======================== addAllFlows ================= ");
		adapter = new ExpandableFlowListAdapter(getActivity(), group);

		flowdata = new ArrayList<Map<String, ?>>();
		List<Flow> flows = man.getFlows();
		if (flows == null) {
			p("Got no flows at all");
			return;
		}

		if (group != null) {
		//	p(" SORTING FLOWS BY GROUP: " + group);
			group.group(flows);
		}
		if (filter != null) {
			p(" FILTERING FLOWS BY: " + filter);

		}
		int total = 0;
		for (Flow flow : flows) {
		//	p("Got flow: "+flow.getId()+", "+flow.getSubject()+", atoms: "+flow.getNrAtoms()+", users:"+flow.getUsers()+"/"+flow.getFlowData().getUsers());
			if (filter != null) {
				boolean ok = filter.filter(flow);
				// p("flow "+flow+" passes: "+ok);
				if (ok) {
					addFlow(flow);
					total++;
				}
			} else {
				// p("Got no filter, adding flow "+flow);
				addFlow(flow);
				total++;
			}
		}

		listView.setAdapter(adapter);
		// TODO: also add option to only collapse if there are too many
		int collapseWhenMore = prefUtil.getCollapseWhenMore();
		
		if (total < collapseWhenMore) {
			for (int i = 0; i < adapter.getGroupCount(); i++) {
				listView.expandGroup(i);
			}
		}
		else {
			for (int i = 0; i < adapter.getGroupCount(); i++) {
				listView.collapseGroup(i);
			}
		}
		adapter.notifyDataSetChanged();
		listView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView listView, View v,
					int group, int pos, long id) {
			//	p("Got group " + group + ", pos " + pos + ", id " + id);
				if (group >= 0 && pos >= 0) {
					Flow flow = (Flow) adapter.getChild(group, pos);
					if (null != mListener) {
						p("onChildClick flow :" + flow);
						mListener.onFlowSelected(flow);
					}
				}
				return false;
			}
		});
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		if (v.getId() == android.R.id.list) {
			p("Context menu on flow item");
			menu.setHeaderTitle("Action on flow");
			String[] menuItems = getResources().getStringArray(
					R.array.flowmenulist);
			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		} else
			p("Context menu not on list: " + v);

	}

	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		p("User picked menu " + item);
		String t = item.getTitle().toString().toLowerCase();

		ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item
				.getMenuInfo();
		View target = info.targetView;
		p("Target view " + target);
		p("Packed pos=" + info);
		int group = this.listView.getPackedPositionGroup(info.packedPosition);
		int child = this.listView.getPackedPositionChild(info.packedPosition);

		final Flow flow = (Flow) adapter.getChild(group, child);
		if (t.startsWith("star")) {
			msg("star flow " + flow.getSubject());
			if (flow.isStarred())
				flow.unstar();
			else
				flow.star();
			target.invalidate();
			man.update(flow, true);
			// reload();
		} else if (t.startsWith("add user")) {
			msg("adding user");
			// show list of users
			List<Contact> allusers = man.getContacts();
			final List<Contact> users = flow.getUsers();
			
			this.utils.showDialog(utils.createUserList(allusers), null,
					new ItemsSelected() {
				@Override
				public void itemsSelected(List items) {
					p("Got users: " + items);
					if (items != null) {
						for (Object item : items) {
							p("adding  user " + item);
							Contact u = (Contact)item;
							if (!users.contains(u))users.add(u);						
						}
						flow.setUsers(users);
						man.update(flow, true);
						
					} 				
				}

				@Override
				public void cancel() {
				}
			}, true);
		} else if (t.startsWith("remove user")) {
			
			final List<Contact> users = flow.getUsers();
			msg("removing user. users are "+users);
			this.utils.showDialog(utils.createUserList(users), null,
					new ItemsSelected() {
				@Override
				public void itemsSelected(List items) {
					p("Got users: " + items);
					if (items != null) {
						for (Object item : items) {
							p("removing user " + item);
							User u = (User)item;
							users.remove(u);						
						}
						flow.setUsers(users);
						man.update(flow, true);
					} 				
				}

				@Override
				public void cancel() {
				}
			}, true);
			
			// show list of users from 
		} else if (t.startsWith("delete")) {
			msg("deleting thread " + flow.getSubject());
			man.delete(flow, true);
			adapter.remove(flow);
			this.listView.invalidate();
			reload();
		}

		return true;

	}

	private void reload() {
		this.addAllFlows();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		p("3. onCreateView");

		View view = inflater.inflate(R.layout.flow_fragment, null);
		headerView = (TextView) view.findViewById(R.id.flowheader);
		updateHeader();

		listView = (ExpandableListView) view.findViewById(android.R.id.list);
		registerForContextMenu(listView);
		return view;
	}

	private void updateHeader() {
	//	p("update title");
		if (headerView == null) {
			return;
		}
			

		String title = "Threads ";
		if (group != null && !group.isNothing()) {
			title += "grouped by ";
			title += group.getType();
			title = title.replace("port", getString(R.string.port));
		}
		else title += "grouped by gate";
		
		if (filter != null && !filter.isNothing()) {
			title += " filtered by ";
			title += filter.getType();
			title = title.replace("port", getString(R.string.port));
		}
		char first = Character.toUpperCase(title.charAt(0));
		headerView.setText(first + title.substring(1));
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		p("4. onActivityCreated: setting simple adapter");
		man = FlowManager.getManager();
		addAllFlows();
		setHasOptionsMenu(true);
		if (curaction != null && curaction.equalsIgnoreCase("create")) {
			this.curaction = null;
			this.newMessageAction(curcontact, curport);
			curcontact = null;
			curport = null;
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	public List<Flow> getAllFlows() {
		return adapter.getAllFlows();
	}
	private void addFlow(Flow flow) {
		Port port = flow.getPort();
	//	p("addFlow: " + flow);
		if (port == null) {
			err("	Flow has no port: " + flow);
			return;
		}
		if (flow.getUsers()== null) {
			err("	Flow has no isers: " + flow);
			return;
		}

		adapter.add(flow);

	}

	public FlowFilter getFilter() {
		return filter;
	}

	public void setFilter(FlowFilter filter) {
		this.filter = filter;
		if (this.adapter != null) {
			this.addAllFlows();
			updateHeader();
		}
	}

	public FlowGroup getGroup() {
		return group;
	}

	public void setGroup(FlowGroup group) {
		this.group = group;
		updateHeader();
		if (this.adapter != null) {
			this.addAllFlows();
		
		}
	}

	private void createFlow() {
		if (port == null) {
			err("Port is null, cannot create flow");
			return;
		}
		if (state == null) {
			if (port.isCanStartWithSay()) {
				state = port.getState("say");
				p("   it is ok, state is say: " + state);

			} else {
				err("Not ok, cannot be say");
				return;
			}
		}

		FlowData fd = new FlowData(man.getNextFlowNr());
		fd.setState(state.getName());
		p("Creating flow with port " + port + "and state " + state);
		fd.setCreator(man.getMe().getName());
		Contact first = users.get(0);
		User user = null;
		if (first instanceof User) {
			user = (User) first;
		} else {
			Group g = (Group) first;
			user = g.getUsers().get(0);

		}
		fd.setAssignee(user.getId());
		fd.setProtocol(port.getId());
		
		Flow flow = man.addCreatedFlow(fd, true);
		flow.setUsers(users);

		Atom atom = flow.getFirstAtom();

		atom.getAtomData().setBody(usermessage);
		if (port.hasSettings()) {
			p("============== This port has seetings. Checking preferences for settings of this port, and add to data if needed.========== ");

			for (InputData set : port.getSettings()) {
				InputData datatoadd = prefutil.getPrefValue(port, set);
				p("Got data to add from settings: " + datatoadd);
				if (datatoadd != null) {
					if (datas == null) {
						datas = new ArrayList<InputData>();
					}
					datas.add(datatoadd);
				}
			}
		}

		if (datas != null) {
			atom.getAtomData().setData(InputData.toJson(datas));
			p("Atom data is: " + atom.getData());
			flow.getFlowData().setData(atom.getData());
		}
		p("Flow users are: " + flow.getUsers());
		flow.setState(state);
		flow.setCrud("c");				
		atom.setCrud("c");
		
		man.update(flow,  true);
		p("Adding all flows, and updating listview, and selecting this new flow");
		addAllFlows();
		
		if (mListener != null)
			mListener.onFlowSelected(flow);

	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFlowFragmentListener {
		//
		public void onFlowSelected(Flow flow);
	}

	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void err(String s) {
		Log.e(TAG, s);
	}

	protected void msg(String s) {
		if (this.getActivity() != null)
			Toast.makeText(this.getActivity(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG)
			p("msg: " + s);
	}

//	public Flow getFlow() {
//		return this.f
//	}

	public void newMessageAction(Contact user, Port port) {
		this.curaction = "CREATE";
		this.curport = port;
		this.curcontact = user;
		
	}

}
