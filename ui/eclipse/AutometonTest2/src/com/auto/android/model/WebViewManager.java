package com.auto.android.model;

import android.app.Activity;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

public class WebViewManager {

	private static final boolean DEBUG = true;
	private WebView webview;
	private Activity activity;
	
	public WebViewManager(WebView webview, Activity activity) {
		this.webview = webview;
		this.activity = activity;
	}
	
	public void updateUser(String values) {
		p("============ Updating javascript users with: " + values);
		webview.loadUrl("javascript:updateUser(" + values + ");");

	}

	public void updatePort(String values) {
		p("============ Updating javascript ports with: " + values);
		webview.loadUrl("javascript:updatePort(" + values + ");");

	}

	public void updateState(String values) {
		p("============ Updating javascript state with: " + values);
		webview.loadUrl("javascript:updateState(" + values + ");");

	}
	public void updateSelect(String key, String cat, String place, String values) {
		p("============ Updating javascript state with: " + values);
		webview.loadUrl("javascript:updateSelect("+q(key)+", "+q(cat)+", "+q(place)+", " + values + ");");

	}
	private String q(String s) {
		return "'"+ s +"'";
	}
	
	protected void p(String s) {
		Log.i("WebViewManager", s);
	}

	protected void msg(String s) {
		Toast.makeText(activity, s, Toast.LENGTH_SHORT).show();
		if (DEBUG) p("msg: " + s);
	}
}
