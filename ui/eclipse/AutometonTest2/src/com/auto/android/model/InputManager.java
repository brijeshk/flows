package com.auto.android.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;

import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import com.auto.utils.ThreadListener;

public class InputManager {

	FlowManager man;
	Context context;
	public InputManager(Context context) {
		this.context = context;
		man = FlowManager.getManager();
	}
	
	public interface PortCreatedListener {
		public void portCreated(Port port);
	}
	public interface ValueEnteredListener {
		public void valueEntered(String value);
	}
	
	
//	public void createTopic(String word) {
//		createTopic(word, null);
//	}
	public void ask(String title, String value, final ValueEnteredListener listener) {
		
			AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
			builder.setTitle(title);
	
			// Set up the input
			final EditText input = new EditText(this.context);
			// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
			input.setInputType(InputType.TYPE_TEXT_VARIATION_SHORT_MESSAGE);
			if (value != null && value.length()>0) input.setText(value);
			builder.setView(input);
		
			// Set up the buttons
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			    	dialog.dismiss();
			        String updatedvalue = input.getText().toString();
			        p("User enteredc "+updatedvalue);
			        listener.valueEntered(updatedvalue);
			    }
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        dialog.cancel();
			    }
			});
	
			builder.show();		
	}
	
//	public void createTopic(String word) {
//		createTopic(word, null);
//	}
	public void createTopic(String word, final PortCreatedListener listener) {
		
			AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
			builder.setTitle("Title");
	
			// Set up the input
			final EditText input = new EditText(this.context);
			// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
			input.setInputType(InputType.TYPE_TEXT_VARIATION_SHORT_MESSAGE);
			if (word != null && word.length()>0) input.setText(word);
			builder.setView(input);
		
			// Set up the buttons
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			    	dialog.dismiss();
			        String topic = input.getText().toString();
			        p("User wants to create topic "+topic);
			        // do it in a thread
			        // do not save it on the server yet (?)
			        man.createNewPort(topic, true, new ThreadListener() {

						@Override
						public void threadDone(Object result) {
							Port port = (Port)result;
							if (port != null) listener.portCreated(port);
							
						}
			        	
			        });
			        
			       
			    }
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        dialog.cancel();
			    }
			});
	
			builder.show();
//		}
//		else {
//			 p("User wants to create topic "+word);
//	        Port port = man.createNewPort(word, true);	       
//	        man.getMe().add(port);
//		}
		
	}
	public String getJsonPortList(User user) {
		// user ports of user
    	String res = "[";
    	List<Port> ports = user.getPorts();
    	for (Port p: ports) {
    		res += "'"+p.getName()+"'"+",";
    	}
    	res = res.substring(0,res.length()-1);
    	res += "]";
    	return res;
	}
	public Map<String, String> getMap(String json) {
		HashMap<String,String> map = new HashMap();
		JSONObject j = null;
		try {
			j = new JSONObject(json);
		} catch (JSONException e) {
			e.printStackTrace();
			p("Could not parse json: "+json);
			return null;
		}
    	Iterator<String> it = j.keys();
    	while (it.hasNext()) {
    		String key = it.next();
    		try {
				p("Got value: "+key+"="+j.get(key));
				map.put(key.toLowerCase(), j.get(key).toString());
			} catch (JSONException e) {
				p("Problem getting "+key+" from "+json);
				e.printStackTrace();
			}
    	}
    	return map;
	}
	
	protected void p(String s) {
		Log.i("InputManager", s);
	}
	public String getJsonProcList(InputData proc) {
		String res = "[";
    	List<InputData> list = proc.getInputDatas();
    	for (InputData u: list) {
    		res += "'"+ u.getName()+"'"+",";
    	}
    	res = res.substring(0,res.length()-1);
    	res += "]";
    	return res;
	}
	public String getJsonProcList(State state) {
		String res = "[";
    	List<InputData> list = state.getInputDatas();
    	
    	for (InputData u: list) {
    		res += "'"+ u.getName()+"'"+",";
    	}
    	res = res.substring(0,res.length()-1);
    	res += "]";
    	return res;
	}
	public String getJsonStateList(Port port) {
		String res = "[";
    	List<State> list = port.getFirstStates(null);
    	for (State u: list) {
    		res += "'"+ u.getName()+"'"+",";
    	}
    	res = res.substring(0,res.length()-1);
    	res += "]";
    	return res;
	}
	public String getJsonUserList() {
		// user list
    	String res = "[";
    	List<User> list = man.getUsers();
    	for (User u: list) {
    		res += "'"+ u.getName()+"'"+",";
    	}
    	res = res.substring(0,res.length()-1);
    	res += "]";
    	return res;
	}
}
