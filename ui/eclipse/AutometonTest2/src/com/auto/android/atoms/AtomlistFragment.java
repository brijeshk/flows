package com.auto.android.atoms;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AndroidGui;
import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.android.data.DataDisplayer;
import com.auto.android.data.DataDisplayer.DialogListener;
import com.auto.android.flows.FlowActivity;
import com.auto.android.model.InputManager;
import com.auto.android.msg.MessageComposer;
import com.auto.android.prefs.PrefUtil;
import com.auto.model.Atom;
import com.auto.model.AtomData;
import com.auto.model.AtomDateComparator;
import com.auto.model.Contact;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;
import com.auto.model.Group;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import com.auto.net.FlowChatListener;

/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class AtomlistFragment extends ListFragment implements FlowChatListener,
		AdapterView.OnItemLongClickListener {

	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;
	public static final String ARG_PARAM1 = "flow";
	private static FlowManager man;
	private AutometonApp app;
	private Flow flow;

	private OnAtomFragmentListener mListener;
	private AtomArrayAdapter adapter;
	private InputManager im;
	private Activity activity;
	private State state;
	private Port port;
	private InputData proc;
	private String usermessage;
	private ImageView imagePort;
	private List<InputData> datas;
	private PrefUtil prefutil;
	private MessageComposer composer;
	private ListView listview;
	private String curaction;
	private User curcontact;
	private Port curport;
	private TextView headerView;
	private GestureDetector gesture;
	public static AtomlistFragment newInstance(Flow flow) {

		AtomlistFragment fragment = new AtomlistFragment(flow);
		Bundle args = new Bundle();
		args.putSerializable(ARG_PARAM1, (Serializable) flow);
		fragment.setArguments(args);

		return fragment;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public AtomlistFragment(Flow flow) {
		this.flow = flow;
	}

	public AtomlistFragment() {

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		if (v.getId() == android.R.id.list) {
			p("Context menu on atom item");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			menu.setHeaderTitle("Action on atom");

			String[] menuItems = getResources().getStringArray(
					R.array.atommenulist);
			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		} else
			p("Context menu not on list: " + v);

	}
	public void setGestureDetector(GestureDetector gesture) {
		this.gesture = gesture;
				
	}
	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		p("User picked menu " + item);
		AdapterView.AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		View target = info.targetView;
		p("Target view " + target);
		p("Item pos=" + info.position);

		Atom atom = adapter.getItem(info.position);

		String t = item.getTitle().toString().toLowerCase();

		if (t.startsWith("star")) {
			msg("star atom");
		} else if (t.startsWith("add")) {
			msg("adding something");
		} else if (t.startsWith("delete")) {
			if (flow == null)
				return true;
			
			msg("delete atom " + atom.getId());
			if (flow.getNrAtoms() <=1) {
				p("Just one or fewer atoms left, deleting entire flow");				
				man.delete(flow, true);
				FlowActivity act = (FlowActivity)activity;
				act.homeClicked();
			}
			else { 
				man.delete(atom, true);
				this.flow.getAtoms().remove(atom);
				adapter.remove(atom);
				this.addAtoms();
			}			
			
		}

		return true;

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		p("user clicked long on pos " + position);
		return false; // let the system show the context menu
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		p("onAttach called");
		prefutil = new PrefUtil(activity);
		try {
			mListener = (OnAtomFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
		this.activity = activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("2. onCreate");
		if (getArguments() != null) {
			flow = (Flow) getArguments().getSerializable(ARG_PARAM1);
		}
		app = (AutometonApp) this.getActivity().getApplication();
		
		if (man == null) man = FlowManager.getManager();
		man.addChatListener(this);
		im = new InputManager(activity);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		p("3. onCreateView");

		View view = inflater.inflate(R.layout.atom_fragment, null);
		
		view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            	p("got motion event on atomfragment");
                return gesture.onTouchEvent(event);
            }
        });
		headerView = (TextView) view.findViewById(R.id.atomheader);
		TextView stateView = (TextView) view.findViewById(R.id.lblstate);

		listview = (ListView) view.findViewById(android.R.id.list);
		registerForContextMenu(listview);

		listview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) { 
            	p("got motion event on listview");
                return gesture.onTouchEvent(event);
            }
        });
		if (flow != null && flow.getState() != null) {
			int barcolor = AndroidGui.getBarColor(flow.getState());
			String color = "#"
					+ getResources().getString(barcolor).substring(3);
			//p("Got color: "+color+" for state "+flow.getState());
			String statecolor = "<font color='" + color + "'>";

			String txt = statecolor + flow.getState().getName() + "</font>";
			stateView
					.setText(Html.fromHtml(txt), TextView.BufferType.SPANNABLE);
		} else
			stateView.setVisibility(View.GONE);

		updateHeader();

		if (flow != null) {
			this.state = flow.getState();
			this.port = flow.getPort();
		} else {
			port = curport;
		}

		p("onCreateView: ++++++++++++++++++++++++ ceating new composer +++++++++++++++++++++++++++++++");
		composer = new MessageComposer(this.activity, view);		
		composer.setFlow(flow);
		composer.setListener(new MessageComposer.MsgListener() {

			@Override
			public boolean createObject() {
				port = composer.getPort();
				if (port == null) {
					p("NO port yet!");
					return false;
				}
				state = composer.getState();
		//		p("Got state from composer: " + state);
				datas = composer.getDatas();
				usermessage = composer.getText();
				createAtom(usermessage);
				return true;
			}
			
			@Override
			public boolean updateView() {
				p("============== Updateview called from messagecomposer");
				port = composer.getPort();					
				state = composer.getState();
			//	p("Got port: "+port+" and state from composer: " + state);
				datas = composer.getDatas();
				
				if (flow == null) {
					p("creating new flow");
					createFlow(true);
				}
				else {
				//	p("already had flow. State is now: "+state);
					flow.setPort(port);
					flow.setState(state);					
				//	p("Updating user list");
					flow.setUsers(composer.getUsers());
					// use thread
					
					man.update(flow, false);
					
					updateHeader();
				
				}
				return true;
			}
			
		});
		
		return view;
	}

	private void updateHeader() {
		String who = "";
		if (flow != null) {
			who = " from " + flow.getCreator().getName();
		}

		if (flow != null && man.isMe(flow.getCreator())) {
			// use other users
			who = " to " + flow.getOtherUsers();
		} else if (this.curcontact != null) {
			who = " to " + curcontact.getName();
		}
		if (flow != null && flow.hasSubject()) {
			p("adding subect "+flow.getSubject());
			who += ": "+flow.getSubject();
		}
	//	p("Uddating header view with: " + who);
		if (flow != null)
			headerView.setText(flow.getPort().getName() + who);
		else if (curport != null)
			headerView.setText(curport.getName() + who);
		else
			headerView.setText(who);
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		p("4. onActivityCreated: setting simple adapter");

		setHasOptionsMenu(true);

		checkifNewAction();

		addAtoms();

	}

	public void checkifNewAction() {
		if (curaction != null && curaction.equalsIgnoreCase("create")) {
			p("got curaction: " + curaction);
		
			flow = null;
			if (composer != null) {
				composer.createNewMessage();
				curaction = null;
				curcontact = null;
				curport = null;
			}	
			else p("ERROR: Got no composer");
		}
		else {
			p("no curaction");
		}
	}
	private void createFlow(boolean update) {
		if (composer == null) {
			p("Cannot create flow, no composer");
			return;
		}
		if (composer.getUsers() == null || composer.getUsers().size()<1) {
			p("Cannot create flow, no users in composer");
			return;
		}
		port = composer.getPort();
		if (flow != null || port == null ) {
			p("Cannot create flow. port"+port);			
			p("flow: "+flow);
			return;
		}
		if (flow == null) {
			FlowData fd = new FlowData(man.getNextFlowNr());
			if (state != null) fd.setState(state.getName());
			p("Creating flow with port " + port + "and state " + state);
			fd.setCreator(man.getMe().getName());			
			fd.setProtocol(port.getId());
			
			
			flow = man.addCreatedFlow(fd, false);
			flow.setUsers(composer.getUsers());
			
		}
		
		if (update) updateView(flow);
	}

	private void createAtom(String msg) {
		p("========= createAtom ======");
		this.state = composer.getState();
		this.port = composer.getPort();
		if (flow == null) {
			this.createFlow(false); 
		}
		flow.setUsers(composer.getUsers());
		
		
		AtomData ad = new AtomData();
		ad.setBody(msg);		
		ad.setCrud_flag("c");
		ad.setSender(man.getMe().getId());
		ad.setCreated(System.currentTimeMillis());
		ad.setState(state.getName());
		p("Stateis: "+state+" , flow users are: " + flow.getUsers());
		
		ad.setFlow_id(flow.getId());
		flow.setState(state);
		
		Atom atom = man.addCreatedAtom(flow, ad);
		if (atom != null) {
			atom.setSender(man.getMe());
			if (datas != null) {
				atom.getAtomData().setData(InputData.toJson(datas));
				p("Atom data is: " + atom.getData());
			}
			flow.setUsers(composer.getUsers());
			flow.setPort(port);
			p("Flow users are: " + flow.getUsers());
			p("Atom sender: " + atom.getSender());
		
			flow.setCrud("u");
			man.update(flow,  false);
			
			if (flow.getNrAtoms() <2) flow.setCrud("c");
			else flow.setCrud("u");
			for (Atom a: flow.getAtoms()) {
				a.setCrud("");
			}
			atom.setCrud("c");
			man.update(atom,  true);
			
			this.updateView(flow);
			if (mListener != null)
				mListener.onFragmentInteraction(atom);
		}

	}

	public void updateView(Flow flow) {
		p("======== updateview with flow: "+flow);
		this.flow = flow;
		
		// reset all data
		p("Setting curaction to null");
		this.curaction = null;
		this.curcontact = null;
		this.curport = null;
		if (flow != null) {
			p("updateView with flow " + flow);
			composer.setFlow(flow);
			updateHeader();
			addAtoms();
		}

	}

	private void addAtoms() {
		if (flow == null) {
			//p("no flow yet");
			return;
		}
			
		adapter = new AtomArrayAdapter(activity);
		List<Atom> atoms = flow.getAtoms();
		Collections.sort(atoms, new AtomDateComparator());
		for (Atom atom : atoms) {
		//	p("Adding atom "  + atom.getId() + ", state="+ atom.getState()+", body="
	//				+ atom.getBody() + ", sender=" + atom.getSender().getName());
			adapter.add(atom);
		}
		listview.setAdapter(adapter);
		composer.setFlow(flow);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ListView lv = this.getListView();
		// if (prefutil.showBubbles()) {
		lv.setDivider(null);
		lv.setDividerHeight(0);
		// }
	}

	@Override
	public void onDetach() {
		super.onDetach();

		mListener = null;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		if (flow == null) {
			p("No flow yet");
			return;
		}
		if (null != mListener) {
			// Notify the active callbacks interface (the activity, if the
			// fragment is attached to one) that an item has been selected.
			position = Math.min(flow.getAtoms().size() - 1, position);
			position = Math.max(0, position);

			Atom atom = flow.getAtoms().get(position);
			p("onListItemClick atom :" + atom);
			if (atom.getData() != null) {

				List<InputData> datas = atom.extractData();
				if (datas != null && datas.size() > 0) {
					InputData data = datas.get(0);
					showData(data);
				} else
					msg("This message has no data attached to it");
			} else
				msg("This message has no data attached to it");
			if (atom != null)
				mListener.onFragmentInteraction(atom);
		}
	}

	private void showData(InputData data) {
		DialogListener listener = new MyDialogListener(data);
		DataDisplayer disp = new DataDisplayer(this.flow, this.activity);
		disp.showData(data, listener);
	}

	private class MyDialogListener implements DialogListener {
		InputData data;

		public MyDialogListener(InputData data) {
			this.data = data;
		}

		@Override
		public void ok(Object val) {
			if (data.getNextData() != null) {
				showData(data.getNextData());
			}
		}

		@Override
		public void cancel() {

		}
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnAtomFragmentListener {
		//
		public void onFragmentInteraction(Atom atom);
	}

	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		if (activity != null && s != null)
			Toast.makeText(activity, s, Toast.LENGTH_SHORT).show();
		p("msg: " + s);
	}

	@Override
	public void receive(String crud, final List<Flow> flows) {
		p("===============================================================");
		p("=================== got flows: "+flows);
		p("===============================================================");
		
		activity.runOnUiThread(new Runnable() {
			public void run() {
				
				if (flows == null || flows.size()<1) {
					return;
				} else {
					boolean found = false;
					for (Flow f: flows) {
						if (f == flow) {
							found = true;
							p("Updating atom list view with this flow");
							updateView(f);
						}
						
					}
					if (!found) {
						msg("Got messages for different threads" );						
					} 
				}
			}
		});

	}

	public void newMessageAction(User user, Port port) {
		p("=========== newMessageAction from flowactivity, setting curaction =========== ");
		this.curaction = "CREATE";
		this.curport = port;
		this.curcontact = user;
		
		
	}

	public void userSaid(ArrayList<String> result) {
		composer.setMessages(result);

	}

	public Flow getFlow() {
		return this.flow;
	}
}
