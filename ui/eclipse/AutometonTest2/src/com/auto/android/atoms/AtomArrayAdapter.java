package com.auto.android.atoms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jivesoftware.smack.util.Base64;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.android.prefs.PrefUtil;
import com.auto.android.utils.UiUtils;
import com.auto.model.Atom;
import com.auto.model.DataValue;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.utils.StringTools;

public class AtomArrayAdapter extends ArrayAdapter<Atom> {

	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;

	private TextView txtmsg;
	private TextView txtdata;
	private TextView txtdate;
	// private TextView txtstate;
	private TextView txtuser;
	// private ImageView icon_atom;
	private ImageView imagedata;
	private List<Atom> atoms = new ArrayList<Atom>();
	private RelativeLayout wrapper;
	private PrefUtil prefutil;
	private FlowManager man;

	@Override
	public void add(Atom object) {
		man = FlowManager.getManager();
		atoms.add(object);
		super.add(object);
	}

	public AtomArrayAdapter(Context context) {
		super(context, android.R.id.text1);
		prefutil = new PrefUtil(context);
	}

	public int getCount() {
		return this.atoms.size();
	}

	public Atom getItem(int index) {
		return this.atoms.get(index);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (prefutil.showBubbles()) {
				row = inflater.inflate(R.layout.atom_li_bubbles, parent, false);
			} else
				row = inflater.inflate(R.layout.atom_li_stateicon, parent,
						false);
		}

		View divider = (View) row.findViewById(R.id.divider);

		wrapper = (RelativeLayout) row.findViewById(R.id.wrapper);

		Atom atom = getItem(position);

		// are all atoms in the view automatically read? Or is there a time
		// delay like 5 seconds?
		Date afewsecsago = new Date(System.currentTimeMillis() - 5 * 1000);
		if (atom.getCreated().before(afewsecsago)) {
			if (atom.isUnread()) man.setRead(atom);
		}
		// what if the atom is UNread? Do we mark it bold? The message?

		//
		String sender = man.isMe(atom.getSender()) ? "I" : atom.getSender()
				.getName() + "";
		// add txt data
		txtmsg = (TextView) row.findViewById(R.id.lbl_atom_msg);
		String msg = atom.getBody();

		if (msg == null || msg.equals("null")) {
			msg = "";
			// txtmsg.setVisibility(View.GONE);
		}
		String txt = null;
		byte[] imgbytes = null;
		
		
		txtmsg.setText(msg);
		txtdata = (TextView) row.findViewById(R.id.lbl_atom_data);
		imagedata = (ImageView) row.findViewById(R.id.lbl_atom_imagedata);

		if (atom.getData() != null) {
			// parse atom data to
			String jsondata = atom.getData();
			List<InputData> datas = InputData.fromJsonArray(jsondata,
					atom.getState());
			txt = atom.getData();
			if (datas != null && datas.size() > 0) {
				txt = "";
				for (InputData data : datas) {
					DataValue value = data.getValue();
					if (data.getType().isImage()) {
						p("data is an image");
						txt = null;
						imgbytes = Base64.decode(value.getValue());

					} else {
						txt += data.getName() + ":";
						if (value != null)
							txt += value.getValue();
						else
							txt += "no value";
						txt += "; ";
					}
				}
			}
		}
		if (imgbytes == null) {
			imagedata.setVisibility(View.GONE);
		} else {
			Bitmap bmp = BitmapFactory.decodeByteArray(imgbytes, 0,
					imgbytes.length);
			this.imagedata.setImageBitmap(bmp);
		}
		if (txt == null) {
			txtdata.setVisibility(View.GONE);
		} else
			txtdata.setText(txt);

		// txtmsg.setText(atom.getData());

		txtdate = (TextView) row.findViewById(R.id.lbl_atom_date);

		String sdate = StringTools.getSmartDateString(atom.getCreated());
		// if date is just minutes, no line
		if (divider != null) {
			Date yesterday = new Date(
					System.currentTimeMillis() - 24 * 3600 * 1000);
			if (atom.getCreated().before(yesterday)) {
				divider.setBackgroundColor(Color.GRAY);
			} else {
				divider.setVisibility(View.GONE);
			}
		}

		txtdate.setText(sdate);

		
		
		
		// txtstate = (TextView) row.findViewById(R.id.lbl_atom_state);
		String verb = null;
		String statecolor = UiUtils
				.getStateColor(getContext(), atom.getState());

		int barcolor = UiUtils.getStateBarColor(getContext(), atom.getState());

		if (atom.getState() != null) {
			ImageView imagestate = (ImageView) row.findViewById(R.id.icon_atom);
			imagestate
					.setImageResource(AndroidGui.getDrawable(atom.getState()));
			verb = atom.getState().getName().toLowerCase();
		} else {
			p("Atom has no state: " + atom);
		}
		if (verb == null || verb.length() < 1) {
			verb = "say";
		}
		if (!man.isMe(atom.getSender())) {
			verb = verb + "s";
		}
		sender = "<font color='gray'>" + sender + "</font> " + statecolor
				+ verb + "</font>";
		txtuser = (TextView) row.findViewById(R.id.lbl_atom_user);
		txtuser.setText(Html.fromHtml(sender), TextView.BufferType.SPANNABLE);

		TextView colorbar = (TextView) row.findViewById(R.id.colorbar);
		colorbar.setBackgroundResource(barcolor);

		// icon_atom = (ImageView) row.findViewById(R.id.icon_atom);
		// icon_atom.setImageResource(AndroidGui.getDrawable(atom.getSender()));

		imagedata = (ImageView) row.findViewById(R.id.lbl_atom_imagedata);

		if (atom.isUnread()) {
			txtmsg.setTypeface(null, Typeface.BOLD);
			txtdata.setTypeface(null, Typeface.BOLD);
			txtdate.setTypeface(null, Typeface.BOLD);
			txtuser.setTypeface(null, Typeface.BOLD);
		}
		if (prefutil.showBubbles()) {
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);

			boolean left = man.isMe(atom.getSender());

			if (left) {
				params.setMargins(5, 5, 40, 5);
			} else {
				params.setMargins(40, 5, 5, 5);
			}
			wrapper.setBackgroundResource(left ? R.drawable.bubble_yellow
					: R.drawable.bubble_blue);
			wrapper.setGravity(left ? Gravity.LEFT : Gravity.RIGHT);
			wrapper.setLayoutParams(params);

		} else {
			// p("pref says no bubbles");
			wrapper.setGravity(Gravity.LEFT);
		}

		return row;
	}

	public Bitmap decodeToBitmap(byte[] decodedByte) {
		return BitmapFactory
				.decodeByteArray(decodedByte, 0, decodedByte.length);
	}

	protected void p(String s) {
		Log.i(TAG, s);
	}

}