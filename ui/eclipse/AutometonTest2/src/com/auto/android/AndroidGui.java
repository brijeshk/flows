package com.auto.android;

import com.auto.model.Contact;
import com.auto.model.ContactInfo;
import com.auto.model.FlowManager;
import com.auto.model.Group;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;

public class AndroidGui {

	private static String[] ABORTS= {"cancel", "abort", "decline", "reject" };
	private static String[] OK = {"accept", "ok", "done", "complete", "answer" };
	private static String[] SAY = {"say",  "tell" };
	private static String[] INFO = {"fyi", "answer" };
	private static String[] TIME = {"schedule", "agenda", "time", "date" };
	private static String[] PREFS = {"options", "preferences", "select", "pick" };
	private static String[] RESULT = {"result" };
	private static String[] HELP = {"help", "info", "show", "ask" };
	private static String[] ASSIGN = {"assign" , "do", "does"};
	private static String[] PAY = {"pay", "offer","buy"};
	
	
	private static String[] SYSTEM = {"movies", "ebay", "weather", "stock", "flights" };
	
	public static int getStar() {
		return R.drawable.apptheme_rate_star_small_on_holo_light;
	}
	public static int getCancel() {
		 return android.R.drawable.ic_menu_close_clear_cancel;
	}
	public static int getBot() {
		 return  R.drawable.robot;
	}
	public static int getDrawable(Port port) {
		String name = port.getName().toLowerCase();
		if (name.startsWith("converse") || name.startsWith("general")) return android.R.drawable.ic_dialog_email;
		else if (name.startsWith("appointment")) return android.R.drawable.ic_menu_day;
		else if (name.startsWith("forsale")) return android.R.drawable.ic_menu_info_details;
		else if (name.startsWith("sell")) return android.R.drawable.ic_menu_info_details;
		else if (name.startsWith("garden")) return android.R.drawable.ic_menu_manage;
		else if (name.startsWith("self")) return android.R.drawable.ic_menu_myplaces;
		else return android.R.drawable.ic_menu_compass;
	}
	public static int getBarColor(State state) {
		int color = android.R.color.darker_gray;
		
		String name = state.getName().toLowerCase();
		if (name.startsWith("select")) color=android.R.color.holo_blue_dark;
		else if (name.equals("do")) color= android.R.color.holo_blue_dark;
		else if (name.equals("assign")) color= android.R.color.holo_blue_dark;
		else if (name.equals("complete")) color= android.R.color.holo_green_dark;
		else if (name.startsWith("say")) color=android.R.color.darker_gray;
		else if (name.startsWith("result")) color=android.R.color.holo_green_dark;
		else if (name.startsWith("get")) color=android.R.color.holo_orange_light;
		else if (contains(name, OK)) color= android.R.color.holo_green_dark;
		else if (contains(name, ABORTS))color=android.R.color.holo_red_dark;
		else if (contains(name, PREFS))color= android.R.color.holo_orange_light;
		else if (contains(name, INFO)) color=android.R.color.holo_orange_light;
		else if (contains(name, TIME))color= android.R.color.holo_orange_light;
		else if (contains(name, PAY))color= android.R.color.holo_red_dark;
		return color;
	}
	
	public static int getDrawable(State state) {
		if (state  == null) return R.drawable.ic_menu_start_conversation;
		String name = state.getName().toLowerCase();
		if (name.startsWith("select")) return android.R.drawable.ic_menu_gallery;
		else if (contains(name, SAY)) return R.drawable.ic_menu_start_conversation;
		else if (name.startsWith("remind")) return R.drawable.ic_menu_notifications;		
		else if (name.startsWith("getavail")) return android.R.drawable.ic_menu_month;
		else if (name.startsWith("get")) return android.R.drawable.ic_menu_help;
		else if (contains(name, OK)) return R.drawable.ic_menu_mark;
		else if (contains(name, ABORTS)) return android.R.drawable.ic_menu_close_clear_cancel;
		else if (contains(name, PREFS)) return android.R.drawable.ic_menu_preferences;
		else if (contains(name, INFO)) return R.drawable.ic_menu_start_conversation;
		else if (contains(name, RESULT)) return android.R.drawable.ic_menu_view;
		else if (contains(name, ASSIGN)) return android.R.drawable.ic_menu_edit;
		else if (contains(name, TIME)) return android.R.drawable.ic_menu_agenda;
		else if (contains(name, HELP)) return android.R.drawable.ic_menu_help;
		else if (contains(name, PAY)) return R.drawable.dollar;
		else return R.drawable.ic_menu_start_conversation;
	}
	
	public static int getDrawable(User user) {
		String name = user.getName().toLowerCase();
		if (user.isSystem() || contains(user.getId(), SYSTEM)) return R.drawable.robot;
		else if (user.isTemplates()) return R.drawable.astronaut;
		
		if (name.startsWith("chantal")) return R.drawable.ninja;
		else if (name.startsWith("brijesh")) return R.drawable.king;
		else if (name.startsWith("pr")) return R.drawable.docctor;
		else if (name.startsWith("r")) return R.drawable.teacher;
		else if (name.startsWith("n")) return R.drawable.sportsman;
		else return R.drawable.gangster;
	}
	public static int getDrawable(Group group) {
		
		if (group.isPrivate()) return android.R.drawable.ic_menu_close_clear_cancel;
		else return R.drawable.ic_menu_allfriends;
	}
	public static int getDrawable(InputData proc) {
		String name = proc.getName().toLowerCase();
		return android.R.drawable.ic_menu_add;
	}
	
	private static boolean contains(String what, String[] list) {
		for (String s: list) {
			if (what.startsWith(s)) return true;
		}
		return false;
	}
	public static int getDrawable(Contact contact) {
		if (contact instanceof User) {
			return getDrawable((User)contact);
		}
		else {
			return getDrawable((Group)contact);
		}
		
	}
	public static int getFlagImage(Contact contact) {
		if (contact instanceof User) {
			User u = (User)contact;
			if (u.isSystem()) {
				return R.drawable.robot;
			}
			else {
				User me = FlowManager.getManager().getMe();
				ContactInfo info = me.getInfoOn(u);
				if (info == null) return -1;
				if (info.hasTag("blocked")) {
					return android.R.drawable.ic_menu_close_clear_cancel;
				}
				else if (info.hasTag("favorite") || info.hasTag("friend")) {
					return getStar();
				}
				else return -1;
			}
		}
		else {
			Group g = (Group) contact;
			return -1;
		}
	}
}
