package com.auto.android.msg;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.NumberPicker;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.android.model.InputManager;
import com.auto.android.utils.UiUtils;
import com.auto.model.Atom;
import com.auto.model.Contact;
import com.auto.model.DataType;
import com.auto.model.DataValue;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.PortNameComparator;
import com.auto.model.State;
import com.auto.model.User;
import com.auto.model.UserNameComparator;
import com.auto.android.utils.UiUtils.ItemsSelected;

public class MessageComposer {

	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;
	//private static final String NAME_KEY = "NAME";

	/** text box related */
	public float lastKnownX;
	public float lastKnownY;

	/** OS RELATED */
	private static final int PICK_IMAGE = 1;
	private Activity context;

	/** View elements **/
	private Button btnOptions;
	private Button btnSend;

	private TextView computedText;
	private EditText datatextinput;
	private int timesCalled = 1;// for bug in date picker!

	private TextView txt;
	private View parentView;
	private AlertDialog caldialog;
	
	/** data model */
	private FlowManager man;
	private List<Contact> users;
	private State state;
	private Port port;
	private List<InputData> datas;
	private Flow flow;
	private State previousState;
	/** events */
	private MsgListener listener;

	private boolean datashowing = false;
	private String id;
	
	/** for entering data */
	List<DataValue> currentvalues;
	InputData currentdata;

	InputManager inputmanager;

	// horizontal list view of states with images
	private GridView stategrid;

	private final String PORT = "Gate"; // Resources.getSystem().getString(R.string.port);

	final CharSequence[] all_options = { "Add/Change Subject", "Add Contacts (@)", "Change " + PORT,
			"Change Action", "Enter/Change Data", "Insert Smiley"
			 };

	final CharSequence[] options_nostate = { "Add/Change Subject", "Add Contacts (@)",
			"Change " + PORT, "Pick Action", "", "Insert Smiley"
			 };

	final CharSequence[] options_nodata = { "Add/Change Subject", "Add Contacts (@)",
			"Change " + PORT, "Pick Action", "Enter/Change Data",
			"Insert Smiley" };

	CharSequence[] options;

	private UiUtils utils;
	
	public MessageComposer(Activity act, View parentView) {
		id = ""+(int)(Math.random()*100);
	//	p("===================================================================");
		
		p("===================== created composer "+id+" =================");		
		this.context = act;
		this.inputmanager = new InputManager(act);
		this.parentView = parentView;
		utils = new UiUtils(act);
		init();
	}

	public void clear() {
	//	p("clear called");
		this.flow = null;
		this.port = null;
		this.state = null;
		this.users = null;
		this.previousState = null;
		if (txt != null)
			this.txt.setText("");
	}

	private void init() {
		man = FlowManager.getManager();

		txt = (EditText) parentView.findViewById(R.id.edit_message);
		// txt.setHint("Custom message");
		txt.setInputType(InputType.TYPE_CLASS_TEXT);
		txt.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				p("User clicked on txt");
				stategrid.setVisibility(View.VISIBLE);
				if (!hasUsers()) {
					showOptions();
					return true;
				}
				return false;
			}
		});
		txt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				p("User clicked on txt");
				stategrid.setVisibility(View.VISIBLE);
				if (!hasUsers())
					showOptions();
			}
		});
		// txt.sete

		btnOptions = (Button) parentView.findViewById(R.id.btnoptions);
		btnSend = (Button) parentView.findViewById(R.id.btnsend);
		// btnUser = (Button) parentView.findViewById(R.id.btnusers);
		// btnTopic = (Button) parentView.findViewById(R.id.btntopic);
		// btnData = (Button) parentView.findViewById(R.id.btndata);
		// btnState = (Button) parentView.findViewById(R.id.btnstate);
		computedText = (TextView) parentView
				.findViewById(R.id.computed_message);
		// computedText.setVisibility(View.GONE);
		addListeners();

		// state list
		stategrid = (GridView) parentView.findViewById(R.id.list_states);

		stategrid.setVisibility(View.GONE);
		stategrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				State state = (State) stategrid.getAdapter().getItem(position);
				p("Got state from pos " + position + "in grid: " + state);
				if (state != null) {
					setState(state);
				}
				if (state != null)
					showData();
				else
					showStates();

			}
		});

	}

	private class GridAdapter extends BaseAdapter {
		private Context mContext;
		private List<State> states;
		private int listbgcolor = -1;

		public GridAdapter(Context c, List<State> states) {
			mContext = c;
			this.states = states;
		}

		@Override
		public int getCount() {
			return states.size();
		}

		@Override
		public Object getItem(int position) {
			return states.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View grid;
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				grid = new View(mContext);
				grid = inflater.inflate(R.layout.grid_single, null);
				TextView textView = (TextView) grid
						.findViewById(R.id.grid_text);
				ImageView imageView = (ImageView) grid
						.findViewById(R.id.grid_image);
				State liststate = states.get(position);
				textView.setText(liststate.getName());
				// highlight selected state
				if (listbgcolor < 0) {
					Drawable bg = textView.getBackground();
					if (bg != null && bg instanceof ColorDrawable) {
						listbgcolor = ((ColorDrawable) bg).getColor();
					} else
						listbgcolor = Color.TRANSPARENT;
				}

				if (state != null
						&& state.getName()
								.equalsIgnoreCase(liststate.getName())) {
				//	p("Highlighting state " + state);
					textView.setBackgroundColor(Color.LTGRAY);
				} else {
					textView.setBackgroundColor(listbgcolor);
				}
				imageView.setImageResource(AndroidGui.getDrawable(liststate));
			} else {
				grid = (View) convertView;
			}
			return grid;
		}
	}

	private void updateStateView(List<State> states,
			boolean showDataAutomatically) {

		int count = 0;
		if (states != null) {
			count = states.size();
			// p("Updating states view with states: "+states);
		}

		int width = (int) (count * 120);
		LayoutParams params = new LayoutParams(width, LayoutParams.WRAP_CONTENT);
		params.height = 120;
		stategrid.setLayoutParams(params);
		stategrid.setNumColumns(count);

		if (count>0) {
			GridAdapter adapter = new GridAdapter(this.context, states);
			stategrid.setAdapter(adapter);
			if (state == null) state = (State) states.get(0);
			if (states.size() == 1) {
				if (state != null && state.isBotCommand()) {
					txt.setHint("msg is disabled for bots");
					// send message right a way!
				}
				if (this.datas == null || this.datas.size() < 1) {
					// p("Just one state, showing data? not when updating flow. Show automatically="+showDataAutomatically);
					if (showDataAutomatically)
						showData();
				}
			}
		}
	//	else p("updateStateView: Got no states");

		// mListFilter.setOnItemClickListener(this);
	}

	public void showUsers(List<Contact> selectedUsers) {

		if (selectedUsers != null)
			Collections.sort(selectedUsers, new UserNameComparator());

		showDialog(utils.createUserList(man.getContacts()), selectedUsers,
				new ItemsSelected() {
					@Override
					public void itemsSelected(List items) {
						// p("Got users: " + items);
						if (items != null) {
							for (Object item : items) {
								// p("adding user " + item);
								addUser((Contact) item);
							}
						} else {
						}
						if (users == null)
							showUsers(null);
						else if (hasUsers())
							showTopics();

					}

					@Override
					public void cancel() {
					}
					// pick one at a time
				}, false);
	}

	public void showTopics() {
		
		if (!hasUsers()) {
			msg("Pick a contact first");
			return;

		}
		p("showTopics: Show COMMON topics for users " + users);
		List<Port> commonportrs = man.getCommonTopics(users);
		if (commonportrs == null || commonportrs.size() < 1) {
			msg("There are no common gates for those users. Pick a different user list");
			showUsers(null);
			return;

		}
		Collections.sort(commonportrs, new PortNameComparator());

		showDialog(utils.createTopicList(commonportrs), port, new ItemsSelected() {
			@Override
			public void cancel() {
			}

			@Override
			public void itemsSelected(List items) {
				p("showTopics: itemsSelected: Got topics: " + items);
				if (items != null && items.size() > 0) {
					Port p = (Port) items.get(0);
					if (port != p) {
						p("setting  topic " + p);
						port = p;
						p("Clearing prev state because it is a new topic");
						previousState = null;
					}					
				}
				if (port != null) {
					p("itemsSelected: topic is "+port+", showing states next");
					showStates();
				} else
					showTopics();
			}
		}, false);
	}

	public List<State> getPossibleStates() {
		if (port == null) {
			p("No port, no possible states");
			return null;

		}
		List<State> states = null;
		if (this.previousState == null || previousState.isBotCommand()) {
			p("no prev state or prev is bot command");
			if (flow != null) {
				p("getting first states for creator "+flow.getCreator());
				states = port.getFirstStates(flow.getCreator());
			}
			else {
				p("getting first states with me as creator (flow is null)");
				states = port.getFirstStates(man.getMe());
			}
			// p("Got no previous states (or bot). first states are: " +
			// states);
		} else {

			if (flow != null) {
				states = port.getNextStates(flow, previousState, flow.getCreator(),
					this.hasBot());
			}
			else  {
				states = port.getNextStates(flow, previousState, man.getMe(),			
					this.hasBot());
			}
			// also add first state if this is a bot to start again from scratch
			p("Got prev state " + previousState + ",  next states are: "
					+ states);
		}
		if (states == null) {
			p("getPossibleStates: Found NO states for flow "+flow+", prevstate="+previousState+", hasbot="+this.hasBot()+", creator="+flow.getCreator());
		}
		return states;
	}

	public void showStates() {
		showStates(true);
	}

	public void showSmileys() {
		showDialog(utils.createSmileyList(), null, new ItemsSelected() {
			@Override
			public void itemsSelected(List items) {

				if (items != null) {
					for (Object item : items) {
						// p("adding smiley " + item);
						String sm = (String) item;
						txt.append(sm);
					}
				}
			}

			@Override
			public void cancel() {
			}
			// pick one at a time
		}, false);
	}

	public void showStates(boolean showDataAutomatically) {

		if (port == null ) {
			p("showStates: port is null, not showing states");
			//Exception e = new Exception("Checking caller stack");
			//e.printStackTrace();
			updateStateView(null, false);
			return;

		}
		List<State> states = getPossibleStates();
		p("showStates for "+port.getName()+": got possible state "+states);
		updateStateView(states, showDataAutomatically);
	}

	public void showCurrentData(InputData nextdata) {

		p("=========== showCurrentData: " + nextdata);
		if (nextdata.isHidden() && !man.isMe(this.flow.getCreator())) {
			p("Data is hidden, I am not creator, don't show");
		}
		this.currentdata = nextdata.clone();
		if (flow != null && flow.hasAtoms()) {
			currentdata = currentdata.computeInputDataForAtoms(flow.getAtoms());
		}
		currentvalues = new ArrayList<DataValue>();

		datashowing = true;
		if ((currentdata.isRequired() || currentdata.isOptional())
				&& currentdata.isSourceUser()) {

			if (currentdata.isSelect1()) {
				showDialog(utils.createValuesView(currentdata.getValues()), null,
						new ItemsSelected() {
							@Override
							public void cancel() {
								datashowing = false;
							}

							@Override
							public void itemsSelected(List items) {
								DataValue value = extractValue(items);
								datashowing = false;
								if (value != null) {
									if (!currentvalues.contains(value))
										currentvalues.add(value);
									currentdata.setValues(currentvalues);
									addData(currentdata);
								}
								if (currentdata.getNextData() != null)
									showCurrentData(currentdata.getNextData());
							}
						}, false);

			} else if (currentdata.isSelectN()) {

				showDialog(utils.createValuesView(currentdata.getValues()), null,
						new ItemsSelected() {
							@Override
							public void cancel() {
								datashowing = false;
							}

							@Override
							public void itemsSelected(List items) {
								datashowing = false;
								currentvalues = extractValues(items);
								currentdata.setValues(currentvalues);
								addData(currentdata);
								if (currentdata.getNextData() != null)
									showCurrentData(currentdata.getNextData());
							}
						}, true);

			} else {
				DataValue dv = currentdata.getValue();
				DataValue val = new DataValue(currentdata.getName());
				if (dv != null)
					val.setValue(dv.getValue());

				showTextDialog(val, currentdata.getType(),
						currentdata.getDescription(), new ValueEntered() {
							@Override
							public void cancel() {
								datashowing = false;
							}

							@Override
							public void valueEntered(DataValue val) {
								datashowing = false;
								// p("========= valueEntered value "+val+", currentvalues: "+currentvalues+", currentdata :"+currentdata);
								if (!currentvalues.contains(val))
									currentvalues.add(val);
								currentdata.setValues(currentvalues);
								addData(currentdata);
								if (currentdata.getNextData() != null)
									showCurrentData(currentdata.getNextData());
							}
						});

			}
		}
	}

	private void addData(InputData data) {
		if (datas == null)
			datas = new ArrayList<InputData>();
		if (!datas.contains(data))
			datas.add(data);
		// p("Added data: "+data);
		computeText();
	}

	private List<DataValue> extractValues(List items) {
		List<DataValue> values = new ArrayList<DataValue>();
		if (items != null && items.size() > 0) {
			for (Object item : items) {
				values.add((DataValue) items);
			}
		}
		return values;
	}

	private DataValue extractValue(List items) {
		if (items != null && items.size() > 0) {
			for (Object item : items) {
				return (DataValue) item;
			}
		}
		return null;
	}

	private void showData() {
		if (datashowing)
			return;

		List<InputData> stateinputdatas = state.getInputDatas();
		if (stateinputdatas == null || stateinputdatas.size() == 0) {
			p("state has no data");
			return;
		}
		InputData currentData = stateinputdatas.get(0);

		showCurrentData(currentData);
	}

	private void showTextDialog(final DataValue dv, DataType type,
			String message, final ValueEntered listener) {

		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle(dv.getName());
		if (message == null)
			message = "Please enter a value:";
		alert.setMessage(message);

		datatextinput = new EditText(context);

		if (dv.getValue() != null) {
			datatextinput.setText(dv.getValue());
		}
		if (type.isUrl()) {
			datatextinput.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
		} else if (type.isQuantity()) {
			datatextinput.setInputType(InputType.TYPE_CLASS_NUMBER);
			final NumberPicker np = new NumberPicker(context);
			np.setMinValue(0);
			np.setWrapSelectorWheel(true);
			datatextinput = null;
			alert.setView(np);
			int nr = 1;
			if (dv.getValue() != null) {
				try {
					nr = Integer.parseInt(dv.getValue());
				} catch (Exception e) {
				}
			}
			np.setValue(nr);
			alert.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							String value = "" + np.getValue();
							dv.setValue(value);
							p("--------------- Ok clicked: calling valueEntered");
							listener.valueEntered(dv);
						}
					});

		} else if (type.isPrice()) {
			datatextinput.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);
		} else if (type.isDate()) {
			// input.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
			Calendar cal = Calendar.getInstance();
			int day = cal.get(Calendar.DAY_OF_MONTH);
			int month = cal.get(Calendar.MONTH);
			int year = cal.get(Calendar.YEAR);
			timesCalled = 1;
			DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
				public void onDateSet(DatePicker view, int selectedYear,
						int selectedMonth, int selectedDay) {
					if (view.isShown()) {
						timesCalled += 1;
						if ((timesCalled % 2) == 0) {
							String ans = selectedDay + "/"
									+ (selectedMonth + 1) + "/" + selectedYear;
							dv.setValue(ans);
							caldialog.dismiss();
							p("--------------- Date Picker, onDateSet, calling valueEntered");
							listener.valueEntered(dv);
						}
					}
				}
			};
			caldialog = new DatePickerDialog(context, datePickerListener, year,
					month, day);
			caldialog.show();
			return;
		} else if (type.isImage()) {

			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);

			context.startActivityForResult(
					Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
			return;
		}

		if (datatextinput != null) {
			// Set an EditText view to get user input
			alert.setView(datatextinput);
			alert.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							String value = datatextinput.getText().toString();
							dv.setValue(value);
							listener.valueEntered(dv);
						}
					});
		}
		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						listener.cancel();
					}
				});

		alert.show();

	}

	public void showDialog(final SimpleAdapter adapter, Object selected,
			final ItemsSelected selectionListener, final boolean allowMultiple) {
		ArrayList selectedItems = new ArrayList();
		if (selected != null)
			selectedItems.add(selected);
		showDialog(adapter, selectedItems, selectionListener, allowMultiple);
	}

	public void showDialog(final SimpleAdapter adapter, List selectedItems,
			final ItemsSelected selectionListener, final boolean allowMultiple) {
		utils.showDialog(adapter, selectedItems, selectionListener, allowMultiple);
	}

	private String computeText() {
		String txt = "";
		// p("--------- Computetext");
		if (hasUsers()) {
			txt += "@";
			// p("Computetext: got users");
			for (int i = 0; i < users.size(); i++) {
				Contact u = users.get(i);
				
				if (u != null) {
					if (!u.equals(man.getMe())) {
						txt += u.getName();
						if (i + 1 < users.size())
							txt += ",";
					}
				}
			}
			txt += ":";
		} else {
			// p("Computetext: no users");
			computedText.setText("(no text yet)");
			computedText.setHint("Pick a user to create a new message");
		}
		if (port != null) {
			txt += port.getName();
		}
		// else p("Computetext: no port");
		if (state != null)
			txt += "." + state.getName();
		if (datas != null && datas.size() > 0) {
			// p("compute text, got datas: "+datas);
			for (int i = 0; i < datas.size(); i++) {
				InputData data = datas.get(i);
				DataValue val = data.getValue();
				txt += "." + data.getName() + "=";
				if (val == null)
					txt += "none";
				else
					txt += val.getValue();

			}

		}
		// p("Computetext: Got final text: " + txt);
		this.computedText.setText(txt);
		updateCallerView();

		return txt;
	}

	
	
	private void showOptions() {
		// if we are at the beginning, just show users

		p("=========== showOptions ===========");
		showText();
		if (!hasUsers()) {
			p("no users");
			this.showUsers(null);
			return;
		} else if (port == null) {
			showTopics();
			p("no port");
			return;
		} 
		if (state == null || !state.hasData()) {
			options = this.options_nostate;
		} else if (!hasData()) {
			options = this.options_nodata;
		} else
			options = this.all_options;

		p("got options: " + options);
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle("What would you like to do?").setItems(options,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

						/*
						 * all_options = { "Pick Contacts (@)", "Change " +
						 * PORT, "Change Action", "Enter/Change Data",
						 * "Insert Smiley", "Create label", "Test (for Chantal)"
						 */
						if (which == 0) {
							// enter or change subject
							InputManager im = new InputManager(context);
							im.ask("Subject", flow.getSubject(), new InputManager.ValueEnteredListener(){

								@Override
								public void valueEntered(String value) {
									flow.setSubject(value);
									updateCallerView();
								}								
							});
							
						}
						else if (which == 1) {
							showUsers(users);
						} else if (which == 2) {
							if (hasUsers())
								showTopics();
							else
								msg("You need to pick a contact first");

						} else if (which == 3) {
							if (hasPort())
								showStates();
							else
								msg("You need to pick a user and topic first");
						} else if (which == 4) {
							if (state != null) {
								showData();
							} else
								showStates();
						} else if (which == 5) {
							// show smileys
							showSmileys();
						} else if (which == 6) {
							createLabel(null);
						} else if (which == 7) {
							msg("Just testing.. doing nothing");
						} else {
							Toast.makeText(
									context,
									"You picked " + options[which]
											+ " (not implemented yet :-)",
									Toast.LENGTH_LONG).show();
						}

					}
				});
		alert.show();

	}

	private void createLabel(String word) {

	}

	protected void p(String s) {
		Log.i(TAG, id+":"+s);
	}

	protected void msg(String s) {
		Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
		if (DEBUG)
			p("msg: " + s);
	}

	private void addListeners() {

		btnOptions.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				p("btnOptions clicked. Show options");
				showOptions();
			}

		});
		btnSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				p("btnSend clicked. Sending msg");
				//hiding state grid
				stategrid.setVisibility(View.GONE);
				msgCompleted();
			}
		});
		// txt.setHint("Custom message");
		txt.setMovementMethod(LinkMovementMethod.getInstance());
		txt.setText("", BufferType.SPANNABLE);

		txt.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent ev) {
				if (ev.getAction() == MotionEvent.ACTION_DOWN) {
					lastKnownX = ev.getX();
					lastKnownY = ev.getY();
					stategrid.setVisibility(View.VISIBLE);
				//btn send 	p("got touch coordinates");
				}
				return false;
			}

		});
		txt.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				String word = getWord();
				p("Got long click, get word: " + word);
				stategrid.setVisibility(View.VISIBLE);
				if (word != null) {
					showWordContextMenu(word);
				}
				return false;
			}

		});

		txt.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View view, int code, KeyEvent ke) {

				stategrid.setVisibility(View.VISIBLE);
				if (ke.getAction() == KeyEvent.ACTION_DOWN) {
					if (code == KeyEvent.KEYCODE_AT) {
						p("onKey: @ entered");
						showUsers(users);
						return true;
					} else if (code == KeyEvent.KEYCODE_SLASH) {
						p("onKey: / entered -> topic if this is the beginning. len = "
								+ txt.getText().length());
						if (txt.getText().length() < 1 && hasUsers()) {
							showTopics();
							return true;
						} else
							return false;
					} else if (ke.isShiftPressed()) {
						p("onKey: SHIFT and code="+code+", @ would be: "+KeyEvent.KEYCODE_AT);
						if (code == 8) { // ! -> action
							// ! = 8 59
							int len = txt.getText().length();
							p("onKey: ! entered -> command. Txt len" + len);
							if (len < 1 && hasUsers() && hasPort()) {
								showStates();
								return true;
							} else
								return false;
							// # = 10 59
						} else if (code == 10) {
							p("onKey: # entered -> tag?");
							// if (hasUsers()) {
							// showStates();
							// }
							return false;
						}

					} else if (code == KeyEvent.KEYCODE_ENTER) {
						p("onKey: ENTER clicked. Complete message IF USER IS DONE");
						msgCompleted();
						return true;
					}

				}
				return false;
			}
		});

	}

	private void showWordContextMenu(final String word) {
		if (word == null || word.length() < 1)
			return;
		final CharSequence[] items = { "Create topic for " + word,
				"Create label for " + word };

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(word);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (which == 0) {
					p("User wants to create topic ");
					inputmanager.createTopic(word,
							new InputManager.PortCreatedListener() {

								@Override
								public void portCreated(Port port) {
									p("Port creation done");

								}
							});
				} else if (which == 1) {
					p("User wants to create label ");
					createLabel(word);
				}
			}

		});

		AlertDialog alert = builder.create();

		alert.show();
	}

	private String getWord() {
		int pos = getOffsetForPosition(txt, lastKnownX, lastKnownY);
		String s = txt.getText().toString();
		String word = "";
		if (pos > -1 && pos < s.length()) {
			// get word
			int start = pos;
			for (; start >= 0; start--) {
				char c = s.charAt(start);
				if (!Character.isDigit(c) && !(Character.isLetter(c))) {
					break;
				}
			}
			int end = pos;
			for (; end < s.length(); end++) {
				char c = s.charAt(end);
				if (!Character.isDigit(c) && !(Character.isLetter(c))) {
					break;
				}
			}
			start = Math.max(start, 0);
			end = Math.min(end, s.length());
			if (start > -1 && end < s.length()) {
				word = s.substring(start, end);
				p("Got touch at pos " + pos + ", start=" + start + ", end="
						+ end + ", which is:" + word);
			}

		} else
			p("Got touch at pos " + pos);
		word = word.trim();
		return word;
	}

	public int getOffsetForPosition(TextView textView, float x, float y) {
		if (textView.getLayout() == null) {
			return -1;
		}
		final int line = getLineAtCoordinate(textView, y);
		final int offset = getOffsetAtCoordinate(textView, line, x);
		return offset;
	}

	private int getOffsetAtCoordinate(TextView textView2, int line, float x) {
		x = convertToLocalHorizontalCoordinate(textView2, x);
		return textView2.getLayout().getOffsetForHorizontal(line, x);
	}

	private float convertToLocalHorizontalCoordinate(TextView textView2, float x) {
		x -= textView2.getTotalPaddingLeft();
		// Clamp the position to inside of the view.
		x = Math.max(0.0f, x);
		x = Math.min(textView2.getWidth() - textView2.getTotalPaddingRight()
				- 1, x);
		x += textView2.getScrollX();
		return x;
	}

	private int getLineAtCoordinate(TextView textView2, float y) {
		y -= textView2.getTotalPaddingTop();
		// Clamp the position to inside of the view.
		y = Math.max(0.0f, y);
		y = Math.min(textView2.getHeight() - textView2.getTotalPaddingBottom()
				- 1, y);
		y += textView2.getScrollY();
		return textView2.getLayout().getLineForVertical((int) y);
	}

	

//	private Map<String, ?> createRow(String value1, String value2,
//			String value3, int value4, Object obj) {
//		Map<String, Object> row = new HashMap<String, Object>();
//		row.put(NAME_KEY, value1);
//		row.put("second", value2);
//		row.put("third", value3);
//		row.put("icon", Integer.toString(value4));
//		row.put("obj", obj);
//		return row;
//	}

	public void addUser(Contact user) {
		if (user == null) {
			return;
		}
		if (users == null)
			users = new ArrayList<Contact>();
		p("addUser: adding user " + user);
		if (!users.contains(user)){
			users.add(user);
			checkUser(user);
		}
		
		computeText();

	}

	private void updateCallerView() {
		if (listener != null && port != null &&  flow != null) {
			listener.updateView();
			// p("Updating view of listener " + listener.getClass().getName());
		}
	}

	private void checkUser(Contact contact) {
		if (contact instanceof User) {
			User user = (User) contact;

			if (user.isSystem()) {
				// p("user "+contact+" is systemuser");
				txt.setEnabled(false);
				txt.setHint("msg is disabled for bots");
				txt.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						showOptions();

					}
				});
			} else {
				// p("user "+contact+" is normal user");
				txt.setEnabled(true);
				txt.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (!hasUsers())
							showOptions();

					}
				});
			}
		} else {
			// p("User is contact.");
			txt.setEnabled(true);
			txt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if (!hasUsers())
						showOptions();

				}
			});
		}
	}

	public void setListener(MsgListener listener) {
		this.listener = listener;
	}

	public List<Contact> getUsers() {
		return users;
	}

	public boolean hasBot() {
		if (!hasUsers())
			return false;
		else {
			for (Contact c : users) {
				if (c instanceof User) {
					User u = (User) c;
					if (u.isSystem())
						return true;
				}
			}
		}
		return false;
	}

	public Port getPort() {
		return port;
	}

	public State getState() {
		return state;
	}

	public List<InputData> getDatas() {
		return datas;
	}

	public boolean isAtom() {
		return flow != null;
	}

	private void msgCompleted() {
		p("=========  msgCompleted =========== ");
		if (!hasUsers()) {
			p("No users. Pick a contact first");
			// this.showUsers(null);
			// return;
		}
		if (port == null) {
			p("Got no port yet");
			this.showTopics();
			return;
		}
		if (state == null) {
			p("msgComplet: Got no state");
			List<State> states = getPossibleStates();
			if (states != null) {
				msg("Please pick a command (like "+states.get(0).getName()+" ) before sending");
			}
			else msg("Problem: there are no possible states. This is a bug...");
			this.showStates();
			return;
		}
		listener.createObject();
		// clear
		this.setFlow(flow);
	}

	public boolean hasUsers() {
		return users != null && users.size() > 0;
	}

	public boolean hasData() {
		return datas != null && datas.size() > 0;
	}

	public boolean hasPort() {
		return port != null;
	}

	public boolean hasState() {
		return state != null;
	}

	private void setTopic(Port port) {
		if (port != this.port) {
			p("Clearing prev state because it is a new topic");
			previousState = null;
		}
		this.port = port;
		
		computeText();
		if (port != null) {
			// p("setting topic "+port+", showing states");
			showStates(false);
			// btnTopic.setText(port.getName());
		}
		// else p("Got no topic");
	}

	private void setState(State state) {
		this.state = state;
		// if (state != null) btnState.setText(state.getName());
		if (state != null && state.isBotCommand()) {
			txt.setHint("msg is disabled for bots");
			// send message right a way!
		}
		showStates();
		computeText();
	}

	public void setFlow(Flow flow) {
		p("setflow "+flow);
		clear();
		this.flow = flow;

		if (txt != null)
			this.txt.setText("");
		else
			return;

		if (flow == null) {
			p("flow is null. setting user and port to null");
			this.setUsers(null);
			this.port = null;
			this.state = null;
			this.datas = null;
			this.previousState = null;
			updateStateView(null, false);
			return;
		}
		setUsers(flow.getUsers());
		setTopic(flow.getPort());
		this.previousState = flow.getState();
		if (flow != null && port != null) {
			showStates(false);
		} else
			updateStateView(null, false);

	}

	private void setUsers(List users) {
		this.users = users;
		if (users != null && users.size() > 0) {
			checkUser((Contact) users.get(0));
		}
		computeText();
	}

	private interface ValueEntered {
		public void valueEntered(DataValue val);

		public void cancel();
	}

	public interface MsgListener {
		public boolean createObject();

		public boolean updateView();
	}

	public Port getTopic() {
		return port;
	}

	public String getText() {
		return this.txt.getText().toString();
	}

	public void showText() {
		this.btnOptions.setVisibility(View.VISIBLE);
		this.txt.setVisibility(View.VISIBLE);
		this.btnSend.setVisibility(View.VISIBLE);
	}

	public void hideText() {
		this.txt.setVisibility(View.GONE);
		this.btnSend.setVisibility(View.GONE);

	}

	public void hideAll() {
		this.btnOptions.setVisibility(View.GONE);
		hideText();
	}

	public void createNewMessage() {
		p("createnewmessage called");
		
		this.showOptions();
		// also update parent view!

	}

	public void setContact(Contact user) {
		this.addUser(user);

	}

	public void setPort(Port port2) {
		this.port = port2;

	}

	public void setMessages(ArrayList<String> strings) {
		if (strings == null || strings.size() < 1)
			return;
		if (strings.size() == 1) {
			setMessage(strings.get(0));
			return;
		}
		showDialog(utils.createStringView(strings), null, new ItemsSelected() {
			@Override
			public void cancel() {
			}

			@Override
			public void itemsSelected(List items) {
				if (items != null) {
					for (Object item : items) {
						p("User picked " + item);
						setMessage(item.toString());
					}
				}
			}
		}, false);
	}

	public void setMessage(String string) {
		this.txt.setText(string);
	}

}
