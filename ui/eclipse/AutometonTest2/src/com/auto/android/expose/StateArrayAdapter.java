package com.auto.android.expose;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.auto.android.AndroidGui;
import com.auto.android.R;
import com.auto.model.FlowManager;
import com.auto.model.State;

public class StateArrayAdapter extends ArrayAdapter<State> {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	
	
	private TextView txtstate;
	private TextView txtdesc;
	private ImageView icon_atom;
	private List<State> states = new ArrayList<State>();
	private RelativeLayout wrapper;

	private FlowManager man;
	
	

	public StateArrayAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		man = FlowManager.getManager();
	}

	public int getCount() {
		return this.states.size();
	}

	@Override
	public void add(State object) {		
		states.add(object);
		super.add(object);
	}
	@Override
	public void remove(State object) {		
		states.remove(object);
		super.remove(object);
	}
	public void add(State state, int targetpos) {
		if (states.size()<1 || targetpos >= states.size() || targetpos< 0) {
			add(state);
		}
		else {
			p("Inserting state at position "+targetpos);			
			states.add(targetpos, state);
			this.clear();
			ArrayList<State> copy = new ArrayList<State>();
			for (State s: states) {
				copy.add(s);
			}
			states.clear();
			for (State s: copy) {
				this.add(s);
			}
		}
		
	}
	public State getItem(int index) {
		return this.states.get(index);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.command_li, parent, false);
		}	
		
		State state = getItem(position);
		//row.seton	
		txtstate = (TextView) row.findViewById(R.id.lblaction);
		TextView txtactor = (TextView) row.findViewById(R.id.lblactor);
		String text = state.getName();
		String who = "anyone";
		if (state.actorIsFlowOwner())  who = "flow owner";
		else if (state.actorIsPortOwner())  who = "port owner";
		else if (state.actorIsToggle())  who = "not prev sender";
		txtactor.setText(who);
		txtstate.setText(state.getName());
		
		
		String desc = "";
		//txtdesc = (TextView) row.findViewById(R.id.lbl_atom_user);	
		//txtdesc.setText(desc);
		
		icon_atom = (ImageView) row.findViewById(R.id.icon);			
		icon_atom.setImageResource(AndroidGui.getDrawable(state));
		
	//	p("Creating list view for state "+state);
				
		return row;
	}

	
	public Bitmap decodeToBitmap(byte[] decodedByte) {
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
	}
	
	protected void p(String s) {
		Log.i(TAG, s);
	}

	

}