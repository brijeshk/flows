package com.auto.android.expose;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.auto.android.AutometonActivity;
import com.auto.android.R;
import com.auto.android.atoms.AtomlistFragment;
import com.auto.android.contact.ContactActivity;
import com.auto.android.data.DataFragment;
import com.auto.android.data.DataFragment.OnDataFragmentListener;
import com.auto.android.model.InputManager;
import com.auto.android.port.PortlistFragment;
import com.auto.android.port.PortviewFragment;
import com.auto.android.state.StateCanvasFragment;
import com.auto.android.state.StateCanvasFragment.OnStateFragmentListener;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;

public class ExposeActivity extends AutometonActivity 
	implements PortlistFragment.OnPortFragmentListener, PortviewFragment.OnPortViewFragmentListener, 
	OnStateFragmentListener, OnDataFragmentListener{

	
	PortlistFragment portlistFragment;
	StateCanvasFragment statecanvasFragment;
	PortviewFragment portviewFragment;
	DataFragment dataFragment;	
	User user;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("Creating ExposeActivity... PASS USER");
		this.setContentView(R.layout.expose_activity);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			p("Got value from intent");
		    user = (User)extras.get("user");
		}
		if (user == null) {
			p("Got NO user, using me");
			user = man.getMe();
		}
		
		 // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.expose_fragment_container) != null) {
            if (savedInstanceState != null) {
            	p("got a savedInstanceState, not adding fragment again");
                return;
            }
            showPortList();
        }
        
	}
	protected void homeClicked() {
		 p("Home pressed. Show expose act");
	     this.startActivity(new Intent(this, ExposeActivity.class)); 
	}

	private void showPortList() {
		portlistFragment = new PortlistFragment(user);
		portlistFragment.setArguments(getIntent().getExtras());
		getFragmentManager().beginTransaction()
		        .add(R.id.expose_fragment_container, portlistFragment).commit();
	}
	@Override
	public void onDataSelected(InputData proc) {
		p("User selected data item " + proc);
		msg("You selected: " + proc);

	}
	protected void newObjectAction() {
		p("User picks new action");
		
		if (portlistFragment != null && this.portlistFragment.isVisible()) {
			p("create new port");
			portlistFragment.createPort(null);
		}
		else {
			msg("new not done");
		}
		
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (super.onOptionsItemSelected(item)) return true;
		int id = item.getItemId();
		if (id == R.id.action_create) {			
			msg("Create new gate");
			// don't save just yet
			InputManager im = new InputManager(this);
			im.createTopic("",
				new InputManager.PortCreatedListener() {
	
					@Override
					public void portCreated(Port port) {
						onPortSelected(port);
					}
			});
	
			return true;
		}
		return false;
	}
	@Override
	public void onPortSelected(Port port) {
		p("User clicked on port from portlistfragment. "+port);
		showPortViewFragment(port);
	}
	@Override
	public void onPortEdited(Port port) {
		p("User has created, deleted or edited port "+port);
		// refresh view?
		// showPortList();
		this.startActivity(new Intent(this, ExposeActivity.class));
		
	}
	private void showPortViewFragment(Port port) {
		p("Creating a new PortViewFragment for port "+port);
		portviewFragment = new PortviewFragment(port);
		Bundle args = new Bundle();
		args.putSerializable(PortviewFragment.ARG_PARAM1, port);
		portviewFragment.setArguments(args);
		portviewFragment.setPort(port);
		p("added port to args");
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.expose_fragment_container, portviewFragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}


	public void showStateCanvas(Port port) {
//		statecanvasFragment = null;
//		if (statecanvasFragment != null) {
//			p("Already have statecanvasFragment, updating view");
//			statecanvasFragment.updateView(port);
//		}
//		else {
//			showStateCanvas(port);
//		}
		p("Creating a new StateCanvasFragment for port "+port);
		statecanvasFragment = new StateCanvasFragment(port);
		Bundle args = new Bundle();
		args.putSerializable(StateCanvasFragment.ARG_PARAM1, port);
		statecanvasFragment.setArguments(args);
		statecanvasFragment.setPort(port);
		p("added port to args");
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.expose_fragment_container, statecanvasFragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}

	@Override
	public void onStateSelected(State state) {	
		p("User clicked on state from statecanvasfragment. "+state);
		
		dataFragment = null;
		if (dataFragment != null) {
			p("Already have dataFragment, updating view");
			dataFragment.updateView(state);
		}
		else {
			p("Creating a new dataFragment");
			dataFragment = new DataFragment(state);
			Bundle args = new Bundle();
			args.putSerializable(DataFragment.ARG_PARAM1, state);
			dataFragment.setArguments(args);
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			
			transaction.replace(R.id.expose_fragment_container, dataFragment);
			transaction.addToBackStack(null);
			transaction.commit();
		}
	}


}