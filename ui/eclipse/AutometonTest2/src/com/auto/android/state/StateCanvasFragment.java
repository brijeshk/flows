package com.auto.android.state;

import java.io.Serializable;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AndroidGui;
import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.android.expose.StateArrayAdapter;
import com.auto.model.FlowManager;
import com.auto.model.Port;
import com.auto.model.State;


/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class StateCanvasFragment extends Fragment {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	public static final String ARG_PARAM1 = "port";
	private static FlowManager man;
	private AutometonApp app;
	
	private Activity activity;
	
	private Port port;
	StateCanvasView canv;

	int x = 20;
	int y = 20;
	int dy = 10;
	int dx = 30;
	TextView txt;
	TextView title;
	ImageView left;
	ImageView right;
	LinearLayout lleft;
	LinearLayout lright;
	Context context;

	StateListView leftlist;
	StateListView rightlist;

	StateArrayAdapter adapterleft;
	StateArrayAdapter adapterright;
	
	private StateCanvasLayout statecanvasLayout;
	private View curleft;
	private View curright;
	
	OnStateFragmentListener mlistener;
	
	public static StateCanvasFragment newInstance(Port port) {
		
		StateCanvasFragment fragment = new StateCanvasFragment(port);
		Bundle args = new Bundle();
		args.putSerializable(ARG_PARAM1, (Serializable)port);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public StateCanvasFragment(Port port) {
		this.port = port;
	}
	public StateCanvasFragment() {
	
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);		
		this.activity = activity;
		try {
			mlistener = (OnStateFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnStateFragmentListener");
		}
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("2. onCreate");
		if (getArguments() != null) {
			Port p = (Port)getArguments().getSerializable(ARG_PARAM1);
			p("Got port "+p);
			if (p != null) port = p;
		}
		else p("Got no arguments");
		app = (AutometonApp)this.getActivity().getApplication();
		man = app.getManager();			
			
		
	}

	@Override
	public void setArguments(Bundle bundle) {
		super.setArguments(bundle);
		if (bundle != null) {
			port = (Port)bundle.getSerializable(ARG_PARAM1);
			p("Got port "+port);
		}
		else p("Got no arguments");
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		p("3. onCreateView for port:"+port);
		 
	    View view = inflater.inflate(R.layout.statecanvas_fragment, null);
	    
	    leftlist = (StateListView) view.findViewById(R.id.listViewLeft);
		rightlist = (StateListView) view.findViewById(R.id.listViewRight);
		statecanvasLayout = (StateCanvasLayout) view.findViewById(R.id.layout_leftright);
		adapterleft = new StateArrayAdapter(activity, R.layout.command_li);
		adapterright = new  StateArrayAdapter(activity, R.layout.command_li);
		canv = (StateCanvasView) view.findViewById(R.id.samplecanvas);
		
		title = (TextView) view.findViewById(R.id.title);
		lleft = (LinearLayout) view.findViewById(R.id.layout_left);
		lright = (LinearLayout) view.findViewById(R.id.layout_right);
		left = (ImageView) view.findViewById(R.id.image_left);
		right = (ImageView) view.findViewById(R.id.image_right);
		right.setImageResource(AndroidGui.getDrawable(man.getMe()));
		left.setImageResource(AndroidGui.getDrawable(man.getUser("chantal")));
		lright.setBackgroundResource(R.drawable.lgrayrr);

		lleft.setBackgroundResource(R.drawable.lgrayrr);

		p("Creating view with topic "+port);
		title.setText("Gate " + port.getName());

		
		this.leftlist.setAdapter(adapterleft);
		this.rightlist.setAdapter(adapterright);
		
		leftlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
				p("==========User clicked on left pos is " + pos);
				curleft = v;
				State state = leftlist.getState(curleft);				
				if (state != null && mlistener != null) {
					mlistener.onStateSelected(state);
				}
				else {
					p("Got no state or no mlistener: "+state+", "+mlistener);
				}
				
			}
		});

		leftlist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View v,
					int pos, long id) {
				p("User clicked LONG on left , pos is: "+pos);
				curleft = v;
				maybeConnect();
				return false;
			}});
		rightlist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View v,
					int pos, long id) {
				p("============User clicked LONG on right , pos is: "+pos);
				curright = v;
				maybeConnect();
				return false;
			}});
		
		rightlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				p("========User clicked on right , pos is: "+position);
				curright = v;
				State state = rightlist.getState(curright);
				if (state != null && mlistener != null) {
					mlistener.onStateSelected(state);
				}
				else {
					p("Got no state or no mlistener: "+state+", "+mlistener);
				}
				
			}
		});
		leftlist.setOnDragListener(new StateListDragListener(activity, leftlist, adapterleft, rightlist, adapterright));
		rightlist.setOnDragListener(new StateListDragListener(activity,rightlist, adapterright, leftlist, adapterleft));

		statecanvasLayout.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(final View v, MotionEvent e) {
				if (e.getAction() == android.view.MotionEvent.ACTION_DOWN) {
					final int x = (int) e.getRawX();
					int y = (int) e.getRawY();

					p("=========Got TOUCH event: " + x + "/" + y + ", cur dims="
							+ v.getWidth() + "/" + v.getHeight());

					if (y > v.getHeight() - 200) {
						final State s = new State("action");

						activity.runOnUiThread(new Runnable() {
							public void run() {
								addState(s, x < v.getWidth() / 2);								
							}						
						});
					}
				}
				return false;
			}
		});

	    return view;
	}
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		p("4. onActivityCreated: adding data to view");
		//p("==== Adding sates port "+port);
		addAllActions();
	}
	
	public void updateView(Port port) {
		p("TODO test updateView with Port "+port);
		this.port = port;
		addAllActions();
		
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		//ListView lv = this.getListView();
		//lv.setDivider(null);
		//lv.setDividerHeight(0);
	}
	private void maybeConnect() {
		if (curright != null && curleft != null) {			
			State sleft = leftlist.getState(curleft);
			State sright = rightlist.getState(curright);
			if (sleft != null && sright != null) {
				p("Connecting states");
				// which way? whatever was first... doing left right now
				sleft.addNext(sright);
				this.statecanvasLayout.invalidate();
			}
		}
	}

	private void addStatesRec(State st, boolean you) {
		addState(st, you);
		if (st.hasStates()) {
			List<State> after = port.getStatesAfter(st);
			if (after != null && after.size() > 0){ 
				for (State next: after){
				//	p("adding next state "+next);
					st.addNext(next);
					next.addPrev(st);
				//	p("Connecting state "+st+" and "+next);
					addStatesRec(next, !you);
				}
			}
		}
	}
	private void addAllActions() {
		adapterleft.clear();
		adapterright.clear();
		p("=========== adding all states ======== ");
		// first get beginning states:
		
		List<State> first = port.getAllFirstStates();		
		if (first == null) {
			p("Got no first states at all");
			return;
		}
		for (State st : first) {
			p(" adding first state to the left:"+st);
			boolean you = true;
			addStatesRec(st, you);
		
		}
				
	}
	
	private void addState(State st, boolean left) {
		if (left) adapterleft.add(st);
		else adapterright.add(st);
	}

	
	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(this.getActivity(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG) p("msg: " + s);
	}

	public interface OnStateFragmentListener {
		 
		public void onStateSelected(State state);
	}

	public void setPort(Port port) {
		this.port = port;
		
	}
	

}
