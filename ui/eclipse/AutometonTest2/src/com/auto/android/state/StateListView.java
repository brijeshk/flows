package com.auto.android.state;

import android.content.ClipData;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.auto.android.R;
import com.auto.android.expose.StateArrayAdapter;
import com.auto.model.State;

public class StateListView extends ListView{

	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;
	private State curstate;
	Drawable targetShape;
	public StateListView(Context context) {
		super(context);
		init();
	}

	public StateListView(Context context, AttributeSet set) {
		super(context, set);
		init();
	}
	
	public State getCurState() {
		return curstate;
	}

	private void setSelected(View v) {
		if (v == null) return;
		targetShape = getContext().getResources().getDrawable(R.drawable.yellowrr);
		v.setBackground(targetShape);
	}
	private void init() {
		setDividerHeight(0);
		setDivider(null);
		this.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> av, View v,
					int pos, long id) {
				curstate = getState(v);
				p("--- selected item, state "+curstate);
				if (curstate != null) setSelected(v);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
				
			}
			
		});
		this.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> av, View v, int pos,
					long id) {
				curstate = getState(v);
				p("--- Clicked item, state "+curstate);
				if (curstate != null) setSelected(v);
				
			}});
		
		setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> par, View v,
					int pos, long id) {
				p("User LONG clicked on pos " + pos);
				ClipData data = ClipData.newPlainText("", "");
			    DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
			    
			    p("====== starting drag");
			    curstate = getState(v);
				
		        v.startDrag( data, //data to be dragged
		        				shadowBuilder, //drag shadow
		        				v, //local data about the drag and drop operation
		        				0   //no needed flags
		        			  );		        		        
		        v.setVisibility(View.INVISIBLE);
				return true;
			}
		});
		
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(final View v, MotionEvent e) {
				if (e.getAction() == android.view.MotionEvent.ACTION_DOWN) {
				    int x = (int) e.getRawX();
					int y = (int) e.getRawY();
					int[] loc = new int[2];
					v.getLocationOnScreen(loc);
					int vx = loc[0];
					int vy = loc[1];
					int vw = v.getWidth();
					int vh = v.getHeight();
					
					curstate = getState(v);
					p("Got TOUCH event at : " + x + "/" + y+ ", curstate="+curstate);					
					x = x - vx;
					y = y - vy;
					
					int perx = x*100/vw;
					int pery = y*100/vh;
					if (perx> 80) {
						p("		User clicked on right side of state");
						if (pery < 50) {
							p("		User clicked on right side of state");
						}
					}
					else if (perx < 20) {
						p("		User clicked on left side of state");
					}
				}
				return false;
			}

		});
		
	}
	public State getState(View v) {
		if (v == null) {
			err("getState: Got now view");
			return null;
		}
		StateArrayAdapter ad = (StateArrayAdapter) getAdapter();
		final int pos = getPositionForView((View) v);
	//	p("Position for view "+v+": "+pos);
		if (pos > -1) {
			return ad.getItem(pos);
		}
		else return null;
	}
	public View getView(State st) {
		StateArrayAdapter ad = (StateArrayAdapter) getAdapter();
		//p("Getting view for state "+st);
		for (int i = 0; i < this.getChildCount(); i++) {
			View v = this.getChildAt(i);
			int pos = this.getPositionForView(v);
			if (pos >= 0) {
				State as = ad.getItem(pos);
				if (as.equals(st)) return v;
			}
		}
		return null;
	}
	
	protected void p(String s) {
		Log.i(TAG, TAG+":"+s);
	}
	protected void err(String s) {
		Log.e(TAG, TAG+":"+s);
	}

}
