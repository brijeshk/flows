package com.auto.android.state;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.widget.ListView;
import android.widget.Toast;

import com.auto.android.R;
import com.auto.android.expose.StateArrayAdapter;
import com.auto.model.State;

public class StateListDragListener implements OnDragListener {

	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;

	//Drawable normalShape;
	Drawable targetShape;
	Activity act;
	ListView targetlist;
	StateArrayAdapter targetad;
	ListView sourcelist;
	StateArrayAdapter sourcead;

	public StateListDragListener(Activity act, ListView sourcelist,
			StateArrayAdapter sourcead, ListView targetlist,
			StateArrayAdapter targetad) {
		this.sourcelist = sourcelist;
		this.sourcead = sourcead;
		this.targetlist = targetlist;
		this.targetad = targetad;
		this.act = act;
		//normalShape = act.getResources().getDrawable(R.drawable.lgrayrr);
		targetShape = act.getResources().getDrawable(R.drawable.greenrr);
		
	}

	@Override
	public boolean onDrag(View droptarget, DragEvent event) {

	//	p("Got onDrag: "+event.getAction()+", v="+v);
		// Handles each of the expected events
		switch (event.getAction()) {

		// signal for the start of a drag and drop operation.
		case DragEvent.ACTION_DRAG_STARTED:
			// do nothing
			break;

		// the drag point has entered the bounding box of the View
		case DragEvent.ACTION_DRAG_ENTERED:
			droptarget.setBackground(targetShape); // change the shape of the view
			break;

		// the user has moved the drag shadow outside the bounding box of the
		// View
		case DragEvent.ACTION_DRAG_EXITED:
			droptarget.setBackground(null); // change the shape of the view back
											// to normal
			break;

		// drag shadow has been released,the drag point is within the bounding
		// box of the View
		case DragEvent.ACTION_DROP:
			// if the view is the bottomlinear, we accept the drag item
			if (droptarget == targetlist || droptarget == sourcelist) {
				View moveditem = (View) event.getLocalState();
				//p("Got target view:need to find source position of this one");
			
				StateArrayAdapter sad = sourcead;
				int sourcepos = sourcelist.getPositionForView(moveditem);
				if (sourcepos < 0) {
					sourcepos = targetlist.getPositionForView(moveditem);
					sad = targetad;
				}
			//	p("Got source position: "+sourcepos);
				if (sourcepos > -1) {
					State state = sad.getItem(sourcepos);				
				//	p("need to remove state at pos " + sourcepos);
					sad.remove(state);
					sad.notifyDataSetChanged();
					// get target position!
					ListView lv = (ListView)droptarget;
					int x = (int) event.getX();
					int y = (int) event.getY();
					int targetpos = 0;
					for (int i = 0; i < lv.getChildCount(); i++) {
						View child = lv.getChildAt(i);
						int top = (int) child.getY();
						int bottom = top + child.getHeight(); 
				//		p("Checking if child "+top+"-"+ bottom+"  is surrounding event y = "+y);
						if (y > top && y < bottom) {							
							targetpos = lv.getPositionForView(child);
					//		p("Found child: "+child+" at pos "+targetpos);
						}
					}
					p("target coordinates: "+x+"/"+y);
				//	p("Inserting state at position "+targetpos);
					if (droptarget == targetlist) {		
						
						targetad.add(state, targetpos);
						targetad.notifyDataSetChanged();
					}
					else {						
						sourcead.add(state, targetpos);
						sourcead.notifyDataSetChanged();
					}
				}
				else {
					p("Got no source position for this item :"+moveditem);
				}
				
				moveditem.setVisibility(View.VISIBLE);
			} else {
				View view = (View) event.getLocalState();
				view.setVisibility(View.VISIBLE);
				p("You can't drop the image here");
				break;
			}
			break;

		// the drag and drop operation has concluded.
		case DragEvent.ACTION_DRAG_ENDED:
			droptarget.setBackground(null); // go back to normal shape

		default:
			break;
		}
		return true;
	}

	protected void p(String s) {
		Log.i(TAG, TAG+":"+s);
	}

	protected void msg(String s) {
		Toast.makeText(act, s, Toast.LENGTH_SHORT).show();
		if (DEBUG)
			p("msg: " + s);
	}

}