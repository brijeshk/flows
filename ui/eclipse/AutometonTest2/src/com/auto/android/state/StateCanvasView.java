package com.auto.android.state;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class StateCanvasView extends View {

	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;

	private Paint mPaint = new Paint();

    private static final int X = 0;
	private static final int Y = 1;
	
	private Paint p = new Paint(Color.WHITE);
	private void buildPoints() {
		p.setTextSize(15);
		
		this.setClickable(true);
		this.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				int x = (int) e.getRawX();
				int y = (int) e.getRawY();
				p("Got event: "+x+"/"+y);
				
			//	invalidate();
				return false;
			}
			
		});					
	}

	public StateCanvasView(Context context) {
		super(context);
		p("creating sample view");
		buildPoints();

	}

	// This constructor is very important because withouth of this
	// you can't insert this view in xml
	public StateCanvasView(Context context, AttributeSet attrs) {
		super(context, attrs);		
		buildPoints();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(measureWidth(widthMeasureSpec),
				measureHeight(heightMeasureSpec));
	}

	/**
	 * Determines the width of this view
	 * 
	 * @param measureSpec
	 *            A measureSpec packed into an int
	 * @return The width of the view, honoring constraints from measureSpec
	 */
	private int measureWidth(int measureSpec) {
		int result = 0;		
		
		measureSpec = getContext().getResources().getDisplayMetrics().widthPixels;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		result =specSize;
		if (specMode == MeasureSpec.UNSPECIFIED) {
			// We were told how big to be
			result = specSize;
		}
		else {
			
			p("specModel "+specMode);
		}

		p("measureWidth:"+result);
		return result;
	}

	/**
	 * Determines the height of this view
	 * 
	 * @param measureSpec
	 *            A measureSpec packed into an int
	 * @return The height of the view, honoring constraints from measureSpec
	 */
	private int measureHeight(int measureSpec) {
		int result = 0;
		// This is because of background image in relativeLayout, which is
		// 1000*1000px
		measureSpec = getContext().getResources().getDisplayMetrics().heightPixels;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.UNSPECIFIED) {
			// Here we say how Heigh to be
			result = specSize;
		}
		p("measureHeight:"+result);
	return result;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		Paint paint = mPaint;

		int paddingy = 120;
		int dx = 20;
		int dy = 20;
		p("onDraw called");
		canvas.translate(dx, dy);				
		
		int w = this.getWidth()-2*dx;
		int h = this.getHeight()-2*dy;
		
		
	}

	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG)
			p("msg: " + s);
	}
}
