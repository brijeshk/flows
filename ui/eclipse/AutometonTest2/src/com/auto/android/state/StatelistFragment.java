package com.auto.android.state;

import java.io.Serializable;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.auto.android.AutometonApp;
import com.auto.android.R;
import com.auto.android.atoms.AtomArrayAdapter;
import com.auto.android.model.InputManager;
import com.auto.model.Atom;
import com.auto.model.AtomData;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;


/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class StatelistFragment extends ListFragment {

	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	public static final String ARG_PARAM1 = "port";
	private static FlowManager man;
	private AutometonApp app;
	private Flow flow;

	private OnAtomFragmentListener mListener;

	private AtomArrayAdapter adapter;
	private InputManager im;
	private Activity activity;
	private State state;
	private Port port;
	private InputData proc;
	private String usermessage;
	private ImageView imagePort;
	
	public static StatelistFragment newInstance(Port port) {
		
		StatelistFragment fragment = new StatelistFragment(port);
		Bundle args = new Bundle();
		args.putSerializable(ARG_PARAM1, (Serializable)port);
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public StatelistFragment(Port port) {
		this.port = port;
	}
	public StatelistFragment() {
	
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnAtomFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
		this.activity = activity;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p("2. onCreate");
		if (getArguments() != null) {
			flow = (Flow)getArguments().getSerializable(ARG_PARAM1);			
		}
		app = (AutometonApp)this.getActivity().getApplication();
		man = app.getManager();
		im = new InputManager(this.activity);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		p("3. onCreateView");
		adapter = new AtomArrayAdapter(getActivity());		
		addItems();
	    View view = inflater.inflate(R.layout.atom_fragment, null);
	    TextView headerView = (TextView) view.findViewById(R.id.atomheader);
	    String who = " from "+ flow.getCreator().getName();
	    if (man.isMe(flow.getCreator())) {
	    	// use other users
	    	who = " to "+ flow.getOtherUsers();
	    }
	    p("Udating header view with: "+who);
		headerView.setText("Flow "+flow.getPort().getName()+who);
    		
		this.state = flow.getState();
		this.port = flow.getPort();
		

		
	    return view;
	}
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		p("4. onActivityCreated: setting simple adapter");
		ListView listView = (ListView)this.getListView();	
		listView.setAdapter(adapter);
		
	}
	
	
	
	
	
	public void updateView(Flow flow) {
		p("updateView with flow "+flow);
		if (adapter != null) {
			adapter.clear();
		}
		addItems();
		
	}
	private void addItems() {
		p("Adding atoms for flow "+flow);
		for (Atom atom: flow.getAtoms()) {
			adapter.add(atom);
		}
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ListView lv = this.getListView();
		lv.setDivider(null);
		lv.setDividerHeight(0);
	}
	@Override
	public void onDetach() {
		super.onDetach();
		
		mListener = null;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		if (null != mListener) {
			// Notify the active callbacks interface (the activity, if the
			// fragment is attached to one) that an item has been selected.
			Atom atom = flow.getAtoms().get(position);
			p("onListItemClick atom :"+atom);
			if (atom != null) mListener.onFragmentInteraction(atom);
		}
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnAtomFragmentListener {
		// 
		public void onFragmentInteraction(Atom atom);
	}
	protected void p(String s) {
		Log.i(TAG, s);
	}

	protected void msg(String s) {
		Toast.makeText(this.getActivity(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG) p("msg: " + s);
	}

	

}
