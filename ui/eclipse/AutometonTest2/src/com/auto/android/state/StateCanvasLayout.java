package com.auto.android.state;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.auto.android.R;
import com.auto.android.expose.StateArrayAdapter;
import com.auto.model.State;

public class StateCanvasLayout extends LinearLayout {
	protected String TAG = getClass().getName();
	protected boolean DEBUG = true;

	Paint pline;
	Paint pshad;
	Paint ptest;

	List<Line> lines;

	StateListView leftlist;
	StateListView rightlist;
	StateArrayAdapter aleft;
	StateArrayAdapter aright;
	
	public class Line {
		float x;
		float y;
		float x1;
		float y1;

		public Line(float x, float y, float x1, float y1) {
			this.x = x;
			this.y = y;
			this.x1 = x1;
			this.y1 = y1;
		}

		public void draw(Canvas c) {

			c.drawLine(x, y, x1, y1, pline);
			c.drawLine(x + 1, y + 1, x1 + 1, y1 + 1, pshad);
			c.drawCircle(x, y, 2, pline);
			c.drawCircle(x1, y1, 2, pline);

		}
	}

	public StateCanvasLayout(Context context) {
		super(context);
		this.setWillNotDraw(false);
		init();
	}

	public StateCanvasLayout(Context context, AttributeSet set) {
		super(context, set);
		init();
	}

	public void addLine(float f, float g, float h, float i) {
	//	p("adding line " + f + "/" + g + "-" + h + "/" + i);
		// need to subtract coords
		int[] loc = new int[2];
		this.getLocationOnScreen(loc);
		//p("Got loc: " + Arrays.toString(loc));
		int x = loc[0];
		int y = loc[1];
		if (f != g && g != h && h != i)
			lines.add(new Line(f - x, g - y, h - x, i - y));
	}

	private void init() {
		pline = new Paint(Paint.ANTI_ALIAS_FLAG);
		pline.setColor(Color.DKGRAY);
		pshad = new Paint(Paint.ANTI_ALIAS_FLAG);
		pshad.setColor(Color.GRAY);

		lines = new ArrayList<Line>();
		
		leftlist = (StateListView) this.findViewById(R.id.listViewLeft);
		rightlist = (StateListView) this.findViewById(R.id.listViewRight);
	}

	public View getChildAt(int x, int y) {
		int nr = this.getChildCount();
		p("Finding child at " + x + "/" + y + " out of " + nr + " children");
		for (int i = 0; i < nr; ++i) {
			View ch = this.getChildAt(i);
			if (ch.getX() < x && ch.getX() + ch.getWidth() > x && ch.getY() > y
					&& ch.getY() + ch.getWidth() < y) {
				p("Found child: " + ch);
				return ch;
			}
		}
		return null;

	}

	
	@Override
	public void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
	//	p("dispatchDraw onto exposeLayout, drawing " + lines.size() + " lines");
		// check states in view, 
		findConnections();
		drawConnections(canvas);
		
	}

	private void drawConnections(Canvas canvas) {
		for (Line l : lines) {
			l.draw(canvas);
		}
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

//		p("Drawing onto exposeLayout, drawing " + lines.size() + " lines");
		findConnections();
		drawConnections(canvas);
		
	}

	private void findConnections() {
		if (leftlist == null || rightlist == null) {
			leftlist = (StateListView) this.findViewById(R.id.listViewLeft);
			rightlist = (StateListView) this.findViewById(R.id.listViewRight);
		}
		if (leftlist == null || rightlist == null) return;
	//	p("Drawing connections between states");
		
		aleft = (StateArrayAdapter)leftlist.getAdapter();
		aright = (StateArrayAdapter)rightlist.getAdapter();
		lines.clear();
		for (int i = 0; i < aleft.getCount(); i++) {
			State sl = aleft.getItem(i);
			for (int j = 0; j < aright.getCount(); j++) {
				State sr = aright.getItem(j);
				if (sl.isConnected(sr)) {
				//	p("States "+sl+" and "+sr+" are connected. Getting views");
					View vleft = leftlist.getView(sl);
					View vright = rightlist.getView(sr);
					
					if (vleft != null && vright != null) {
						int yl = getMidY(vleft);
						int yr = getMidY(vright);
						int xl = getRight(vleft);
						int xr = getLeft(vright);
					//	p("--- Adding line connecting views");
						this.addLine(xl,  yl, xr, yr);
					}
					else {
					//	p(" --- left or right view is null: "+vleft+", "+vright);
					}
					
					
				}
			}
		}
	}
	private int getRight(View v) {
		Rect r = new Rect();
		int loc[] = new int[4];
		v.getLocalVisibleRect(r);		
		v.getLocationOnScreen(loc);
		return loc[0]+r.width()-v.getPaddingRight()/2;		
	}
	private int getLeft(View v) {
		Rect r = new Rect();
		int loc[] = new int[4];
		v.getLocalVisibleRect(r);		
		v.getLocationOnScreen(loc);
		return loc[0]+v.getPaddingLeft()/2;		
	}
	private int getMidY(View v) {
		Rect r = new Rect();
		int loc[] = new int[4];
		v.getLocalVisibleRect(r);		
		v.getLocationOnScreen(loc);
		return loc[1]+r.height()/2;		
	}
	

	protected void p(String s) {
		Log.i(TAG, TAG + ": " + s);
	}

	protected void msg(String s) {
		Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
		if (DEBUG)
			p("msg: " + s);
	}
}
