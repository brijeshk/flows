package com.auto.android.prefs;

import java.util.List;

import android.annotation.SuppressLint;
import android.preference.PreferenceActivity;
import android.util.Log;

import com.auto.android.R;

@SuppressLint("Override")
public class AutometonPreferencesActivity extends PreferenceActivity {

	protected String TAG = getClass().getName();
	
	@Override
    public void onBuildHeaders(List<Header> target) {
		
		loadHeadersFromResource(R.xml.preference_headers, target);
    }
	protected boolean isValidFragment(String fragmentName) {
		  return PrefsFragment.class.getName().equals(fragmentName) || FlowPrefsFragment.class.getName().equals(fragmentName);
		}
	protected void p(String s) {
		Log.i(TAG, TAG+": "+s);
	}
}
