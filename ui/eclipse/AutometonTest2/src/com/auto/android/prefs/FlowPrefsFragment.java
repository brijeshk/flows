package com.auto.android.prefs;

import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.provider.MediaStore;
import android.util.Log;

import com.auto.android.R;
import com.auto.model.DataType;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;

public class FlowPrefsFragment extends PreferenceFragment{
	
	FlowManager man;
	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	
	private HashMap<Integer, Preference> intentmap= new HashMap();
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // p("Starting FlowPrefsFragment fragment");
        // Load the preferences from an XML resource
    //    SharedPreferences sharedPref =PreferenceManager.getDefaultSharedPreferences(this);
        
        addPreferencesFromResource(R.xml.flowpreferences);
        PreferenceScreen root = this.getPreferenceScreen();
        
        // also add prefs based on flow and port properties
        man = FlowManager.getManager();
        List<Port> ports = man.getAllPorts();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        for (Port p: ports) {
        //	p("Got port: "+p.getName());
        	// check if port has preferences
        	if (p.hasSettings()) {
        		List<InputData> settings = p.getSettings();
        		p("Port has settings: "+settings);
        		PreferenceCategory cat = new PreferenceCategory(this.getActivity());
        		cat.setTitle("Settings for "+p.getName());
        		root.addItemFromInflater(cat);
        		for (InputData set: settings) {
        			DataType type = set.getType();
        			Preference pref = null;
        			 String key = p.getPreferenceKey(set);
        			if (type.isList()) {
        				ListPreference listPref = new ListPreference(this.getActivity());
        				String vals[] = new String[set.getValues().size()];
        				for (int i = 0; i < vals.length; i++) {
        					vals[i] = set.getValues().get(i).getValue();
        				}
        		        listPref.setEntries(vals);
        		        listPref.setEntryValues(vals);
        		        pref = listPref;        		       
        			}
        			else if (type.isInt() || type.isQuantity()) {
        				if (set.getName().toLowerCase().startsWith("frequency")) {
        					ListPreference listPref = new ListPreference(this.getActivity());
        					listPref.setEntries(R.array.frequencylist);
        					listPref.setEntryValues(R.array.frequencylistvalues);
        					pref = listPref;
        				}
        				else pref = new EditTextPreference(this.getActivity());           		              		       
        			}
        			else if (type.isImage() ) {
         			   pref = new Preference(this.getActivity());
         			   final int REQUEST = intentmap.size();
         			   intentmap.put(REQUEST, pref);
         			   pref.setOnPreferenceClickListener (new Preference.OnPreferenceClickListener(){
         		            public boolean onPreferenceClick(Preference preference){
         		                Intent intent = new Intent();
         		                intent.setType("image/*");
         		                intent.setAction(Intent.ACTION_GET_CONTENT);
         		                startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST);
         		                return true;
         		            }
         		        });
         			}
        			else {
        				pref = new EditTextPreference(this.getActivity());        		             		       
        			}
        			
        			String defValue = "";
        			if (set.getValue() != null && set.getValue().getValue() != null) {
        				defValue = 	set.getValue().getValue();
        			}
        			if (prefs.contains(key)) {
        				pref.setDefaultValue(prefs.getString(key, defValue));
        			}
        			
        			cat.addPreference(pref);
        			pref.setKey(key);
      		        pref.setTitle(set.getName()+" for "+p.getName());
      		        pref.setSummary(set.getDescription());
      		      
        			
        		}
        	}
        }
    }
	public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 

            if(resultCode == this.getActivity().RESULT_OK){  
                Uri selectedImage = imageReturnedIntent.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = this.getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();
                p("Got image file path: " + filePath);
                // now remember which preference it was
                Preference pref = intentmap.get(requestCode);
                p("Got pref: "+pref.getKey());
                SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).edit();
                prefs.putString(pref.getKey(), filePath);
                prefs.commit();
            }
        }
	
	protected void p(String s) {
		Log.i(TAG, TAG+": "+s);
	}

}

