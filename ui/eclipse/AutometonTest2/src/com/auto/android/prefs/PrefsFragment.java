package com.auto.android.prefs;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.util.Log;

import com.auto.android.R;
import com.auto.model.FlowManager;

public class PrefsFragment extends PreferenceFragment{
	
	FlowManager man;
	protected String TAG = getClass().getName();	
	protected boolean DEBUG = true;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //p("Loading general preferences");
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
       
    }
	
	protected void p(String s) {
		Log.i(TAG, TAG+": "+s);
	}

}

