package com.auto.android.prefs;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.auto.model.FlowGroup;
import com.auto.model.InputData;
import com.auto.model.Port;

public class PrefUtil {
	SharedPreferences prefs;
	protected String TAG = getClass().getName();
	Context context;

	public PrefUtil(Context act) {
		prefs = PreferenceManager.getDefaultSharedPreferences(act);
		this.context = act;
	}

	public boolean showBubbles() {
		// disable for now until we have time to fix the ui
		return false;
		//return prefs.getBoolean("bubbles", false);

	}

	public void setImage(String key, Bitmap realImage) {
		// Bitmap realImage = BitmapFactory.decodeStream(stream);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		realImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] b = baos.toByteArray();

		String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
		// textEncode.setText(encodedImage);
		Editor edit = prefs.edit();
		edit.putString(key, encodedImage);
		edit.commit();
	}

	public Bitmap getImage(String key) {
		String previouslyEncodedImage = prefs.getString(key, "");

		if (!previouslyEncodedImage.equalsIgnoreCase("")) {
			byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
			Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
			// imageConvertResult.setImageBitmap(bitmap);
			return bitmap;
		}
		return null;
	}

	

	// public int getDrawable(Port port) {
	// String name = port.getName().toLowerCase();
	// prefs.get
	// }
	public String getHomepage() {
		return prefs.getString("homepage", "http://www.autometon.com");
	}

	public String getWebSocketUrl() {
		return prefs.getString("websocket",
				"ws://autometon.com:5000/websocket/");
	}

	public String getAppLink() {
		return prefs.getString("applink",
				"https://play.google.com/store/apps/details?id=com.autometon");

	}

	public int getCollapseWhenMore() {
		return Integer.parseInt(prefs.getString("collapsewhenmorethan", "30"));

	}

	public FlowGroup getDefaultGroup() {
		String by = prefs.getString("sortflows", "date");
		FlowGroup group = new FlowGroup();
		if (by.equalsIgnoreCase("date")) {
			group.setDate(true);
		} else if (by.equalsIgnoreCase("gate") || by.equalsIgnoreCase("port")) {
			group.setPort(true);
		} else if (by.equalsIgnoreCase("contact")
				|| by.equalsIgnoreCase("user")) {
			group.setUsers(true);
		} else if (by.equalsIgnoreCase("state")
				|| by.equalsIgnoreCase("action")) {
			group.setState(true);
		} else {
			p("Unknown sortflows preference: " + by + ", using date instead");
			group.setDate(true);
		}
		return group;

	}

	// public boolean areThreadsExpanded() {
	// return prefs.getBoolean("expanded", true);
	//
	// }
	public InputData getPrefValue(Port port, InputData set) {
		String key = port.getPreferenceKey(set);
		p("Getting pref value " + key);
		if (prefs.contains(key)) {
			String value = prefs.getString(key, null);
			p("Found preference " + key + ":" + value);
			if (value == null) {
				return null;
			}
			InputData datatoadd = set.clone();
			datatoadd.getValue().setValue(value);
			return datatoadd;
		} else
			p("Could not find this preference");
		return null;
	}

	public void setPrefValue(Port port, InputData set) {
		p("setPrefValue: " + port.getPreferenceKey(set));
		if (set.getValue() != null) {
			String key = port.getPreferenceKey(set);
			Editor ed = prefs.edit();
			ed.putString(key, set.getValue().getValue());
			ed.commit();
		}
	}

	protected void p(String s) {
		Log.i(TAG, TAG + ": " + s);
	}
}
