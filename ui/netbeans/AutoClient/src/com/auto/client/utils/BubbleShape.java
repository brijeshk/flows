package com.auto.client.utils;

import java.awt.geom.*;
import java.awt.font.*;
import java.awt.*;
import java.util.*;

public class BubbleShape {

    private Graphics2D g2;
    private static int BORDER = 10;
    private double x = BORDER;				//Default x position
    private double y = BORDER;				//Default y position
    private double width = 10;			//Default width
    private double height = 10;			//Default height

    private float transparency = 1.0F;	//Level of transparency
    private boolean visible = true;	//Whether bubble is visible
    private FontRenderContext frc;
    private Font font = new Font("Arial", 1, 12);
    private Color colorLightOrange = new Color(255, 255, 230); 	
    private Color colorLightBlue = new Color(230, 230, 255); 	
    private Color colorLightGrey = new Color(189, 189, 189); 	
    private Color bubbleColor;				//Default colour
    private GeneralPath blackBubble = new GeneralPath();		//Black bubble shape
    private GeneralPath whiteBubble = new GeneralPath();		//White bubble shape

    private Shape black;
    private Shape white;
    private boolean left;
    private static final int DELTA_RIGHT = 20;
    
    public BubbleShape(Dimension d, boolean left) {
        this.left = left;
        width = d.getWidth();
        height = d.getHeight();
        if (left) bubbleColor = colorLightOrange;
        else bubbleColor = colorLightBlue;
    }

    /**
     * Creates two bubble shaped shapes
     */
    public void createBubbles() {
        blackBubble = new GeneralPath();
        whiteBubble = new GeneralPath();
        float WIDTH = (float) width;
        float HEIGHT = (float) height;
        float X = (float) x - 10 + 4;
        float Y = (float) y + HEIGHT + 4;
        blackBubble.moveTo(X - 5, Y - 5);
        blackBubble.lineTo(X + 10, Y - 20);
        blackBubble.lineTo(X + 10, Y - 10 - HEIGHT + 5);
        blackBubble.quadTo(X + 10, Y - 10 - HEIGHT, X + 15, Y - 10 - HEIGHT);
        blackBubble.lineTo(X + 10 + WIDTH - 5, Y - 10 - HEIGHT);
        blackBubble.quadTo(X + 10 + WIDTH, Y - 10 - HEIGHT, X + 10 + WIDTH, Y - 10 - HEIGHT + 5);
        blackBubble.lineTo(X + 10 + WIDTH, Y - 10 - 5);
        blackBubble.quadTo(X + 10 + WIDTH, Y - 10, X + 10 + WIDTH - 5, Y - 10);
        blackBubble.lineTo(X + 20, Y - 10);
        blackBubble.lineTo(X - 5, Y - 5);
        X = (float) x - 10;
        Y = (float) y + HEIGHT;
        whiteBubble.moveTo(X - 5, Y - 5);
        whiteBubble.lineTo(X + 10, Y - 20);
        whiteBubble.lineTo(X + 10, Y - 10 - HEIGHT + 5);
        whiteBubble.quadTo(X + 10, Y - 10 - HEIGHT, X + 15, Y - 10 - HEIGHT);
        whiteBubble.lineTo(X + 10 + WIDTH - 5, Y - 10 - HEIGHT);
        whiteBubble.quadTo(X + 10 + WIDTH, Y - 10 - HEIGHT, X + 10 + WIDTH, Y - 10 - HEIGHT + 5);
        whiteBubble.lineTo(X + 10 + WIDTH, Y - 10 - 5);
        whiteBubble.quadTo(X + 10 + WIDTH, Y - 10, X + 10 + WIDTH - 5, Y - 10);
        whiteBubble.lineTo(X + 20, Y - 10);
        whiteBubble.lineTo(X - 5, Y - 5);
        
        if (!left) {
           
           white =  translatedCopyOf(whiteBubble, DELTA_RIGHT, 0 );
           black =  translatedCopyOf(blackBubble, DELTA_RIGHT, 0);
        }
        else{
            black = blackBubble;
            white = whiteBubble;
        }
        
                
    }
    
    private void p(String s) {
        System.out.println("Bubble: "+s);
    }
    public Shape translatedCopyOf(Shape s, double dx, double dy)
    {       
	AffineTransform af = new AffineTransform();   
	Rectangle2D box = s.getBounds2D(); 
       // Note: the transformations get applied IN REVERSE ORDER!
	af.translate(dx,dy);
	
	return af.createTransformedShape(s);
    }

    public Shape scaledCopyOf(Shape s, double sx, double sy)
    {       
	AffineTransform af = new AffineTransform();   
	Rectangle2D box = s.getBounds2D(); 
        
	double x = box.getX();
	double y = box.getY();

        p("box is: x= "+x+". y="+y);
	// Note: the transformations get applied IN REVERSE ORDER!
	af.translate(x,y);
	af.scale(sx, sy);
	af.translate(-x, -y);
             
	return af.createTransformedShape(s);
    }
    /**
     * Displays the two bubble on screen
     */
    public void printBubbleImage() {
        g2.setPaint(Color.gray);
        
        g2.fill(black);
        g2.setPaint(bubbleColor);
        g2.fill(white);
        g2.setPaint(Color.black);
        g2.draw(white);
       
    }

    /**
     * Displays the current textual description on screen
     */
    public void printBubbleText(String txt) {

        int localX = (int)x + 50;
        int localY = (int)y + 10;   
        if (!left) {
            localX +=  DELTA_RIGHT;
        }
        FontMetrics metrics = g2.getFontMetrics(g2.getFont());
        int h = metrics.getHeight();
        String lines[] = txt.split("\n");
        for (String line: lines) {
            g2.drawString(line, localX, localY);
            localY += h;
        }
    }

    public void draw(Graphics2D g2d, String msg) {
        g2 = g2d;
        frc = g2.getFontRenderContext();

        if (visible) {
            createBubbles();
            printBubbleImage();
            printBubbleText(msg);
        }
    }

    /**
     * Gets the X position of the bubble shape
     */
    public double getX() {
        return x;
    }

    /**
     * Gets the Y position of the bubble shape
     */
    public double getY() {
        return y;
    }

    /**
     * Sets the X position of the bubble shape
     */
    public void setX(double xnew) {
        x = xnew;
    }

    /**
     * Sets the Y position of the bubble shape
     */
    public void setY(double ynew) {
        y = ynew;
    }

    /**
     * Gets the Height of the bubble shape
     */
    public double getHeight() {
        return height;
    }

    /**
     * Gets the Width of the bubble shape
     */
    public double getWidth() {
        return width;
    }

    /**
     * Sets the Height of the bubble shape
     */
    public void setHeight(double heightnew) {
        height = heightnew;
    }

    /**
     * Sets the Width of the bubble shape
     */
    public void setWidth(double widthnew) {
        width = widthnew;
    }

    /**
     * Gets the visibility of the bubble shape
     */
    public boolean getVisible() {
        return visible;
    }

    /**
     * Sets the visibility of the bubble shape
     */
    public void setVisible(boolean b) {
        visible = b;
    }

}
