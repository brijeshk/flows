/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.utils;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
import javax.swing.JList;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Chantal
 */
public class ActionJList extends JList {
    /*
     ** sends ACTION_PERFORMED event for double-click
     ** and ENTER key
     */

    ActionListener al;
    boolean multi;
    public ActionJList(Vector<String> words, boolean multi) {
        super(words);
        this.setBackground(new Color(255,255,230));
        this.multi = multi;
        if (multi)  setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        init();
    }

    public ActionJList(String[] it) {
        super(it);
        init();

    }

    private void init() {
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (al == null) {
                    return;
                }
                Object ob[] = getSelectedValues();
                if (ob.length > 1) {
                    return;
                }
                if (me.getClickCount() == 2) {
                    System.out.println("ActionJList: Sending ACTION_PERFORMED to ActionListener");
                    al.actionPerformed(new ActionEvent(this,
                            ActionEvent.ACTION_PERFORMED,
                            ob[0].toString()));
                    me.consume();
                }
            }
        });

        addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                if (al == null) {
                    return;
                }
                Object ob[] = getSelectedValues();
                if (ob.length > 1) {
                    return;
                }
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    System.out.println("ActionJList: Sending ACTION_PERFORMED to ActionListener");
                    al.actionPerformed(new ActionEvent(this,
                            ActionEvent.ACTION_PERFORMED,
                            ob[0].toString()));
                    ke.consume();
                }
            }
        });
        this.setSelectedIndex(0);
    }


    public void addActionListener(ActionListener al) {
        this.al = al;
    }
}
