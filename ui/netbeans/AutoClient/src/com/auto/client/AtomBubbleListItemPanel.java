/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client;

import com.auto.client.main.Gui;
import com.auto.client.utils.BubbleShape;
import com.auto.client.utils.ImageUtils;
import com.auto.model.Atom;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.ToolTipManager;

/**
 *
 * @author Chantal
 */
public class AtomBubbleListItemPanel extends javax.swing.JPanel {

    private static final Logger log = Logger.getLogger(AtomBubbleListItemPanel.class.getName());

    Atom atom;

    BufferedImage image = null;
    //   Flow flow;

    /**
     * Creates new form AtomListItemPanel
     *
     * @param atom
     * @param icon
     */
    public AtomBubbleListItemPanel(Atom atom) {
        initComponents();
        this.atom = atom;
        ToolTipManager t = ToolTipManager.sharedInstance();
        t.setInitialDelay(10);
        t.setDismissDelay(20000);
        lblImageLeft.setText(atom.getSender().getName());
        lblImageLeft.setToolTipText(atom.getSender().toString());
        lblImageLeft.setIcon(Gui.getIconForUser(atom.getSender().getName()));
        lblImageLeft.setHorizontalTextPosition(JLabel.CENTER);
        lblImageLeft.setVerticalTextPosition(JLabel.BOTTOM);
        Dimension dl = new Dimension(60, 60);
        lblImageLeft.setMinimumSize(dl);
        lblImageLeft.setMaximumSize(dl);
        lblImageLeft.setPreferredSize(dl);
        //lblImageLeft.setText(atom.getSender().getId());

        if (atom.getState() != null) {
            lblImageRight.setText(atom.getState().getName());
            lblImageRight.setIcon(Gui.getIconForState(atom.getState().getName()));
            lblImageRight.setHorizontalTextPosition(JLabel.CENTER);
            lblImageRight.setVerticalTextPosition(JLabel.BOTTOM);
            lblImageRight.setToolTipText(atom.getState().toString());
        } else {
            this.lblImageRight.setText("?");
        }
        
        Dimension d = new Dimension(300, 70);
        if (atom.getData() != null) {
            List<InputData> datas = atom.extractData();
            if (datas != null && datas.size()>0) {
                InputData data = datas.get(0);
                if (data.getType().isImage()) {
                    p("Got an image, need to convert it");
                    image = ImageUtils.decodeToImage(data.getValue().getValue());
                   // ImageIcon icon = new ImageIcon(buf);
                    d = new Dimension(image.getWidth()+150, image.getHeight()+30);
                }               
            }           
        }
        if (image == null) this.setMaximumSize(new Dimension(400, 100));
       
        lblImageRight.setMinimumSize(dl);
        lblImageRight.setMaximumSize(dl);
        lblImageRight.setPreferredSize(dl);
               
        setToolTipText(atom.toString());
       
        this.setMinimumSize(d);
        //this.setMaximumSize(new Dimension(400, 100));
        this.setPreferredSize(d);
        
    }
    private class MyMouseListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            
        }

        @Override
        public void mousePressed(MouseEvent e) {
            
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            
        }

        @Override
        public void mouseExited(MouseEvent e) {
            
        }
    
    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Dimension dim = this.getSize();
        int w = (int)dim.getWidth()-40;
        int h = (int)dim.getHeight()-10;
        Dimension d = new Dimension(w, h);
       // p("Drawing bubble of size "+d);
        boolean left = !FlowManager.getManager().isMe(atom.getSender());
        BubbleShape s = new BubbleShape(d, left);
        //BubbleShape s = new BubbleShape(d, true);
        Graphics2D gg = (Graphics2D)g;
        String msg = "";
        if (atom.getBody() != null) msg = atom.getBody();
        if (image == null)   msg += "\n"+atom.getSimpleDataString();
        
        s.draw(gg, msg);
        int x = 15;
        int y = 10;
        int x1= w-70;
        if (left) {
            
            lblImageLeft.getIcon().paintIcon(this, gg, x, y);
            lblImageRight.getIcon().paintIcon(this, gg, x1, y);
            gg.drawString(lblImageLeft.getText(),x, h-10 );
            gg.drawString(lblImageRight.getText(),x1, h-10 );
        }
        else {
            x = x + 20;
            x1 = x1 + 20;
            lblImageLeft.getIcon().paintIcon(this, gg, x, y);
            lblImageRight.getIcon().paintIcon(this, gg, x1, y);
            gg.drawString(lblImageLeft.getText(),x, h-10 );
            gg.drawString(lblImageRight.getText(),x1, h-10 );
        }
        if (image != null) {            
            gg.translate(x+40, y);
            gg.drawImage(image, null ,null);
        }
    }
    
    @Override
    public void addMouseListener(MouseListener m) {
        super.addMouseListener(m);
        lblImageLeft.addMouseListener(m);
        lblTop.addMouseListener(m);
        lblBottom.addMouseListener(m);
        lblImageRight.addMouseListener(m);
    }

    private void p(String s) {
        log.info(s);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblImageRight = new javax.swing.JLabel();
        lblImageLeft = new javax.swing.JLabel();
        panCenter = new javax.swing.JPanel();
        lblTop = new javax.swing.JLabel();
        lblBottom = new javax.swing.JLabel();
       
        setLayout(null);

        Insets insets = this.getInsets();
        Dimension sleft = lblImageLeft.getPreferredSize();
        Dimension sright = lblImageRight.getPreferredSize();
        lblImageLeft.setBounds(15 + insets.left, 5 + insets.top,
             sleft.width, sleft.height);
        
        lblImageRight.setBounds(this.getWidth()- insets.right-sright.width-15, 5 + insets.top,
             sright.width, sright.height);

        lblImageRight.setText("status right");
        add(lblImageRight);

        lblImageLeft.setText("iconleft");
        add(lblImageLeft);

    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblBottom;
    private javax.swing.JLabel lblImageLeft;
    private javax.swing.JLabel lblImageRight;
    private javax.swing.JLabel lblTop;
    private javax.swing.JPanel panCenter;
    // End of variables declaration//GEN-END:variables
}
