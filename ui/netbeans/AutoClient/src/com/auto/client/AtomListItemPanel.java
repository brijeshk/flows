/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client;

import com.auto.client.main.Gui;
import com.auto.client.utils.ImageUtils;
import com.auto.model.Atom;
import com.auto.model.InputData;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.ToolTipManager;

/**
 *
 * @author Chantal
 */
public class AtomListItemPanel extends javax.swing.JPanel {

    private static final Logger log = Logger.getLogger(AtomListItemPanel.class.getName());

    Atom atom;

    //   Flow flow;

    /**
     * Creates new form AtomListItemPanel
     *
     * @param atom
     * @param icon
     */
    public AtomListItemPanel(Atom atom) {
        initComponents();
        this.atom = atom;
        ToolTipManager t = ToolTipManager.sharedInstance();
        t.setInitialDelay(10);
        lblImageLeft.setText(atom.getSender().getName());
        lblImageLeft.setToolTipText(atom.getSender().toString());
        lblImageLeft.setIcon(Gui.getIconForUser(atom.getSender().getName()));
        lblImageLeft.setHorizontalTextPosition(JLabel.CENTER);
        lblImageLeft.setVerticalTextPosition(JLabel.BOTTOM);
        Dimension dl = new Dimension(60, 60);
        lblImageLeft.setMinimumSize(dl);
        lblImageLeft.setMaximumSize(dl);
        lblImageLeft.setPreferredSize(dl);
        //lblImageLeft.setText(atom.getSender().getId());

        this.lblTop.setText("<html><h4>" + atom.getBody() + "</h4></html>");

        if (atom.getState() != null) {
            lblImageRight.setText(atom.getState().getName());
            lblImageRight.setIcon(Gui.getIconForState(atom.getState().getName()));
            lblImageRight.setHorizontalTextPosition(JLabel.CENTER);
            lblImageRight.setVerticalTextPosition(JLabel.BOTTOM);
            lblImageRight.setToolTipText(atom.getState().toString());
        } else {
            this.lblImageRight.setText("?");
        }
        
        lblImageRight.setMinimumSize(dl);
        lblImageRight.setMaximumSize(dl);
        lblImageRight.setPreferredSize(dl);
        if (atom.getData() != null) {
            List<InputData> datas = atom.extractData();
            if (datas != null && datas.size()>0) {
                InputData data = datas.get(0);
                if (data.getType().isImage()) {
                    p("Got an image, need to convert it");
                    BufferedImage buf = ImageUtils.decodeToImage(data.getValue().getValue());
                    ImageIcon icon = new ImageIcon(buf);
                    lblBottom.setIcon(icon);
                }
                else this.lblBottom.setText("<html><font color='0000bb'>Data: " + atom.getSimpleDataString() + "</font></html>");
            }
            else {
                this.lblBottom.setText("<html><font color='0000bb'>Data: " + atom.getSimpleDataString() + "</font></html>");
            }
        }
        //else this.lblBottom.setText("<html><font color='0000bb'>No data</font></html>");
        lblBottom.setToolTipText(atom.toString());
        Dimension d = new Dimension(300, 70);
        this.setMinimumSize(d);
        this.setMaximumSize(new Dimension(400, 100));
        this.setPreferredSize(d);
        
    }

//    @Override
//    public void paintComponent(Graphics g) {
//        super.paintComponent(g);
//        Dimension dim = this.getSize();
//        int w = (int)dim.getWidth();
//        int h = (int)dim.getHeight();
//        Dimension d = new Dimension(w-20, h-20);
//        p("Drawing bubble of size "+d);
//        BubbleShape s = new BubbleShape(d);
//        Graphics2D gg = (Graphics2D)g;
//        s.draw(gg, "");
//    }
    
    @Override
    public void addMouseListener(MouseListener m) {
        super.addMouseListener(m);
        lblImageLeft.addMouseListener(m);
        lblTop.addMouseListener(m);
        lblBottom.addMouseListener(m);
        lblImageRight.addMouseListener(m);
    }

    private void p(String s) {
        log.info(s);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblImageRight = new javax.swing.JLabel();
        lblImageLeft = new javax.swing.JLabel();
        panCenter = new javax.swing.JPanel();
        lblTop = new javax.swing.JLabel();
        lblBottom = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setLayout(new java.awt.BorderLayout());

        lblImageRight.setText("status right");
        add(lblImageRight, java.awt.BorderLayout.EAST);

        lblImageLeft.setText("iconleft");
        add(lblImageLeft, java.awt.BorderLayout.WEST);

        panCenter.setLayout(new java.awt.BorderLayout());

        lblTop.setText("top line");
        panCenter.add(lblTop, java.awt.BorderLayout.CENTER);

        lblBottom.setText("bottom line");
        panCenter.add(lblBottom, java.awt.BorderLayout.PAGE_END);

        add(panCenter, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblBottom;
    private javax.swing.JLabel lblImageLeft;
    private javax.swing.JLabel lblImageRight;
    private javax.swing.JLabel lblTop;
    private javax.swing.JPanel panCenter;
    // End of variables declaration//GEN-END:variables
}
