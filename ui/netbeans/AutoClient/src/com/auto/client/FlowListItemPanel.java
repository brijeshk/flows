/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.client;

import com.auto.client.main.Gui;
import com.auto.model.Flow;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author Chantal
 */
public class FlowListItemPanel extends javax.swing.JPanel {
  private static final Logger log = Logger.getLogger(FlowListItemPanel.class.getName());

    Flow flow;
    /**
     * Creates new form FlowListItemPanel
     * @param flow
     * @param icon
     */
    public FlowListItemPanel(Flow flow) {
        initComponents();
        this.flow = flow;
        if (flow == null) {
            p("No flow...");
        }
        lblTop.setText("<html><h4>"+flow.getAtomUsers()+"</h4></html>");
        if (flow.getPort() != null) {
            lblTop.setToolTipText("Port: "+flow.getPort().getName()+", Assignee: "+flow.getAssignee().getId());
        }
        else lblTop.setToolTipText("Port: None, Assignee: "+flow.getAssignee().getId());
        lblImageLeft.setIcon(Gui.getIconForUser(flow.getCreator().getId()));
        lblImageLeft.setText(flow.getCreator().getName());
        lblImageLeft.setToolTipText(flow.getCreator().getEmail());
        lblImageLeft.setHorizontalTextPosition(JLabel.CENTER);
        lblImageLeft.setVerticalTextPosition(JLabel.BOTTOM);
        Dimension dl = new Dimension(60, 60);
        lblImageLeft.setMinimumSize(dl);
        lblImageLeft.setMaximumSize(dl);
        lblImageLeft.setPreferredSize(dl);
        
        String sub = flow.getSubject();
        if (sub == null && flow.getPort() != null) sub = flow.getPort().getName();
        lblBottom.setText("<html><font color='0000bb'>"+sub+"</font></html>");
        lblBottom.setToolTipText("Data: "+flow.getFlowData().getData());
        
        if (flow.getState() != null) {
            lblImageRight.setIcon(Gui.getIconForState(flow.getState().getName()));        
            lblImageRight.setToolTipText( "State: "+ flow.getState().getName()+", Todo: "+flow.getTodo());
        }
        lblImageRight.setText("    ");
        lblImageRight.setMinimumSize(dl);
        lblImageRight.setMaximumSize(dl);
        lblImageRight.setPreferredSize(dl);
        
        Dimension d = new Dimension(200, 80);
        this.setMinimumSize(d);
        this.setMaximumSize(new Dimension(400, 100));
       // this.setPreferredSize(d);
    }

  @Override
    public void addMouseListener(MouseListener m)  {
        super.addMouseListener(m);
        lblImageLeft.addMouseListener(m);
        lblTop.addMouseListener(m);
        lblBottom.addMouseListener(m);
        lblImageRight.addMouseListener(m);
    }
    private void p(String s) {
        log.info(s);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblImageRight = new javax.swing.JLabel();
        lblImageLeft = new javax.swing.JLabel();
        panCenter = new javax.swing.JPanel();
        lblTop = new javax.swing.JLabel();
        lblBottom = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setMaximumSize(null);
        setMinimumSize(null);
        setOpaque(false);
        setLayout(new java.awt.BorderLayout());

        lblImageRight.setText("status right");
        add(lblImageRight, java.awt.BorderLayout.EAST);

        lblImageLeft.setText("iconleft");
        add(lblImageLeft, java.awt.BorderLayout.WEST);

        panCenter.setLayout(new java.awt.BorderLayout());

        lblTop.setText("top line");
        panCenter.add(lblTop, java.awt.BorderLayout.CENTER);

        lblBottom.setText("bottom line");
        panCenter.add(lblBottom, java.awt.BorderLayout.PAGE_END);

        add(panCenter, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblBottom;
    private javax.swing.JLabel lblImageLeft;
    private javax.swing.JLabel lblImageRight;
    private javax.swing.JLabel lblTop;
    private javax.swing.JPanel panCenter;
    // End of variables declaration//GEN-END:variables
}
