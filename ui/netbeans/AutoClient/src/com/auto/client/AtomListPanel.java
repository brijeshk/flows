/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.client;

import com.auto.client.main.Gui;
import com.auto.model.Atom;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

/**
 *
 * @author Chantal
 */
public class AtomListPanel extends javax.swing.JPanel {
private static final Logger log = Logger.getLogger(AtomListPanel.class.getName());

    FlowManager man;
    /**
     * Creates new form AtomListPanel
     */
    public AtomListPanel(Flow flow) {
        initComponents();
        
         man = FlowManager.getManager();
        ImageIcon[] icons = Gui.LIST_STATE;
        int nr = flow.getAtoms().size();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); 
        p("Got "+nr+" atoms");
        
        for (Atom atom: flow.getAtoms()) {
            p("Adding atom: "+atom);
            addAtom(atom);
        }
    }
    public void addAtom(Atom atom) {       
        AtomBubbleListItemPanel item = new AtomBubbleListItemPanel(atom );            
        this.add(item); 
    }
    private void p(String s) {
        log.info(s);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
