/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.main;

import com.auto.model.FlowManager;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Properties;
import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 *
 * @author Chantal
 */
public class Main {

    public static JFrame mainFrame;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            //UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
        //    UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
          //  UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
            // start application            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         System.setProperty("java.util.logging.SimpleFormatter.format", "%4$s:%2$s %5$s %n");

        // get username and pw from command line
        Properties prop = Prefs.getPrefs();
        String username = prop.getProperty("username", "chantal");
        String pw = prop.getProperty("password", "chantal");

        if (args != null) {
            for (int i = 0; i < args.length; i += 2) {
                String key = args[i].toLowerCase();
                if (key.startsWith("-")) {
                    key = key.substring(1);
                }
                out("Got key " + key);
                if (i + 1 < args.length) {
                    String val = args[i + 1];
                    out("Got value " + val);
                    if (key.startsWith("user")) {
                        username = val;
                    } else if (key.equalsIgnoreCase("pw")) {
                        pw = val;
                    } else if (key.startsWith("pass")) {
                        pw = val;
                    } else {
                        out("Unknown parameter: " + key);
                    }
                } else {
                    out("Missing argument for " + key);
                }
            }
        }
        mainFrame = new JFrame();
        mainFrame.setIconImage(Gui.IMG_AUTO.getImage());

        mainFrame.getContentPane().add(new MainPanel(username, pw));
        mainFrame.setSize(450, 500);
        mainFrame.setLocation(450, 200);
        mainFrame.setVisible(true);
        mainFrame.addWindowListener(new WindowListener() {

            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                FlowManager.getManager().logout();
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
    }

    private static void out(String s) {
        System.out.println(s);
    }

}
