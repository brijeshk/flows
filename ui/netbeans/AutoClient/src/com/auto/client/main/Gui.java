/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.client.main;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Chantal
 */
public class Gui {
     private static final Logger log = Logger.getLogger(Gui.class.getName());

    public static final String IMG = "/images/16x16/";
    public static final String IMG32 = "/images/32x32/";
    public static final String IMGUSERS = "/images/32x32/users/";
   
    private static ImageIcon load16(String name) {
        URL url = Gui.class.getResource(IMG+name);
        p("Loading image "+IMG+name+">"+url);
         try {
             return new ImageIcon(ImageIO.read(url) );
         } catch (IOException ex) {
             Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
    }
    private static ImageIcon load32(String name) {
        URL url = Gui.class.getResource(IMG32+name);
        p("Loading image "+IMG32+name+": "+url);
         try {
             return new ImageIcon(ImageIO.read(url) );
         } catch (IOException ex) {
             Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
    }
    private static ImageIcon user(String name) {
        URL url = Gui.class.getResource(IMGUSERS+name);
        p("Loading image "+name+": "+url);
         try {
             return new ImageIcon(ImageIO.read(url) );
         } catch (IOException ex) {
             log.warning("COuld not find icon "+IMGUSERS+name);
             Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
    }
    public static ImageIcon IMG_AUTO = load32("speech-bubble-center-2.png");
    
    /** MAIN TAB ICONS */
    public static ImageIcon IMG_FLOWS = load32("speech-bubble-left-4.png");    
    public static ImageIcon IMG_ATOMS = load32("speech-bubble-center-2.png");    
    public static ImageIcon IMG_CONTACTS = load32("user.png");
    public static ImageIcon IMG_DISCOVER = load32("search.png");
    public static ImageIcon IMG_EXPOSE = load32("wifi-full.png");
    
    /** USERS */
    public static ImageIcon IMG_USER1 =load32("user.png");
    public static ImageIcon IMG_USER2 =load32("user-2.png");
    public static ImageIcon IMG_USERS =load32("users.png");
    public static ImageIcon IMG_USER_ANDROID =user("android1.png");    
    public static ImageIcon IMG_USER_KING =user("royal-king-icon.png");
    public static ImageIcon IMG_USER_ROBOT =user("robot-male-icon.png");
    public static ImageIcon IMG_USER_SPORT =user("sportsman-guy-icon.png");
    public static ImageIcon IMG_USER_TEACHER =user("teacher-female-icon.png");
    public static ImageIcon IMG_USER_OFFICER =user("officer-man-icon.png");
    public static ImageIcon IMG_USER_NINJA =user("ninja-icon.png");
    public static ImageIcon IMG_USER_DR =user("doctor-man-icon.png");
    
    public static ImageIcon[] LIST_USERS = {
        IMG_USER_KING, IMG_USER_DR, IMG_USER_ROBOT,IMG_USER_SPORT,IMG_USER_TEACHER,IMG_USER_TEACHER,IMG_USER_OFFICER,IMG_USER_NINJA            
    };
    
    /** ACTIONS */
    public static ImageIcon IMG_STAR =load32("star-2.png");
    public static ImageIcon IMG_DELETE =load32("bin.png");
    public static ImageIcon IMG_ALARM =load32("bell.png");
    public static ImageIcon IMG_AT =load32("@.png");
    public static ImageIcon IMG_AT_16 =load16("@.png");
    public static ImageIcon IMG_ADD =load32("add.png");
    public static ImageIcon IMG_ADD16 =load16("add.png");
    public static ImageIcon IMG_ATTACH =load32("attachment-2.png");
    
  //  public static ImageIcon IMG_CREATE = load32("create.png");
    
    
    /** OTHER USEFUL ICONS */
    public static ImageIcon IMG_ENVELOPE =load32("envelope.png");
    public static ImageIcon IMG_EYE =load32("eye.png");
    public static ImageIcon IMG_CALENDAR =load32("calendar.png");
    
    public static ImageIcon IMG_CHECK =load32("mark.png");
    public static ImageIcon IMG_QUESTION_YELLOW =load32("emblem-question-yellow.png");
    public static ImageIcon IMG_QUESTION_YELLOW16 =load16("emblem-question-yellow.png");
    public static ImageIcon IMG_EXCLAMATION_RED =load32("emblem-important-red.png");
    public static ImageIcon IMG_WEATHER_CLEAR =load32("brightness-high.png");
    public static ImageIcon IMG_KEY=load32("key.png");
    public static ImageIcon IMG_WEATHER_SHOWERS =load32("cloud.png");
    
    public static ImageIcon getIconForUser(String s) {
       
        if (s == null) {
            p("getIconForUser: name is null");
            return IMG_USER_ANDROID;
        }
        else s = s.toLowerCase();
        
        if (s.startsWith("chantal")) return IMG_USER_NINJA;
        else if (s.startsWith("brijesh")) return IMG_USER_KING;
        else if (s.startsWith("pr")) return IMG_USER_OFFICER;
        else if (s.startsWith("weather")) return IMG_WEATHER_CLEAR;
        else if (s.startsWith("movie")) return load32("tag.png");
        else if (s.startsWith("ebay")) return load32("tag-2.png");
        else if (s.startsWith("manasi")) return IMG_USER_SPORT;
        
        else return IMG_USER_ANDROID;
    }
    public static ImageIcon getIconForState(String s) {
        if (s == null) return load16("marquee.png");
        if (s.equalsIgnoreCase("ASK")) return IMG_QUESTION_YELLOW;
        else if (s.equalsIgnoreCase("DO"))return IMG_EXCLAMATION_RED;
        else if (s.equalsIgnoreCase("CANCEL")) return IMG_DELETE;
        else if (s.equalsIgnoreCase("DECLINE")) return IMG_DELETE;
        else if (s.equalsIgnoreCase("REJECT")) return IMG_DELETE;
        else if (s.equalsIgnoreCase("OPTIONS")) return load32("ellipsis.png");
        else if (s.equalsIgnoreCase("FYI")) return load32("envelope.png");
        else if (s.equalsIgnoreCase("TELL")) return load32("envelope.png");
        else if (s.equalsIgnoreCase("APPOINTMENT")) return load32("clock.png");
        else if (s.equalsIgnoreCase("ANSWER")) return load32("pen.png");
        else if (s.equalsIgnoreCase("PAY")) return load32("vinyl.png");
        else if (s.equalsIgnoreCase("SHIP")) return load32("tilde.png");
        else if (s.equalsIgnoreCase("ACCEPT")) return load32("mark.png");
        else if (s.equalsIgnoreCase("DONE")) return load32("mark.png");
        else return load32("marquee.png");
    }
      public static ImageIcon[] LIST_STATE = {
        IMG_STAR, IMG_ALARM,IMG_CHECK,IMG_EXCLAMATION_RED,IMG_QUESTION_YELLOW,IMG_WEATHER_SHOWERS,IMG_WEATHER_CLEAR            
    };
      
    private static void p(String s) {
       // log.info(s);
    }
    public static void main(String[] args) {
        
    }
}
