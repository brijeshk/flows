/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.client.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chantal
 */
public class Prefs {
    
    private static Properties props = new Properties();
    private static Prefs prefs = new Prefs();
  
    private Prefs() {
        load();
    }
    
    public static boolean isAutoLogin() {
        return props.getProperty("autologin", "true").equalsIgnoreCase("true");
    }
    public static Properties getPrefs()  {
        if (prefs == null) prefs = new Prefs();
        return props;
    }
            
    public static void save() {
        File f = new File("client.properties");
        OutputStream out;
        try {
            out = new FileOutputStream( f );
            props.store(out, "Client properties file");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Prefs.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Prefs.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    private void load() {
        InputStream in = getClass().getResourceAsStream("client.properties");
        try {
            props.load(in);
        } catch (IOException ex) {
            Logger.getLogger(Prefs.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(Prefs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
