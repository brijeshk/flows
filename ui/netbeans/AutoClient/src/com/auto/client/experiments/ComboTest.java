/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.client.experiments;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Chantal
 */
public class ComboTest extends JPanel {
    
     JComboBox box;
     JComboBox box1;
     DefaultComboBoxModel model;    
     DefaultComboBoxModel model1;    
     JTextField txt;
     Vector data1 ;
     Vector data ;
     JButton close;
     
    public ComboTest() {
        data1 = new Vector();
        data = new Vector();
        box = new JComboBox();
        box1 = new JComboBox();
        close = new JButton("x");
        
        data.add("this");
        data.add("this is");
        data.add("this is just");
        data.add("this is just an ");
        
        
        data1.add("another");
        data1.add("selection");
        data1.add("car");
        data1.add("airplane");
        box.setEditable(true);
        box1.setEditable(true);
        model = new DefaultComboBoxModel(data);
        model1 = new DefaultComboBoxModel(data1);
        box.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Changing data1");
                String val = box.getSelectedItem().toString();
                for (int i = 0; i < data1.size(); i++) {
                    data1.set(i, val+" "+data1.get(i));
                }
                txt.add(box1);
                txt.invalidate();
                txt.repaint();
                repaint();
                invalidate();
                revalidate();
                txt.paintImmediately(0,0,400,30);
            }
        });
        box.setModel(model);
        box1.setModel(model1);
        box.setOpaque(false);
        
        txt = new JTextField(80);
        
        Dimension dimtxt = new Dimension(250, 35);
        
        txt.setMinimumSize(dimtxt);
        txt.setPreferredSize(dimtxt);
        txt.setMaximumSize(dimtxt);
        add(txt);
        txt.setLayout(null);
        box.setBounds(2,2,100,22);
        box1.setBounds(125,2,100,22);
        txt.add(box);
        txt.add(close);
        close.setBounds(102, 2, 20,20);
        
        setLayout(new FlowLayout());
    }
    
    public static void main(String args[]) {
        JFrame f = new JFrame();
        f.getContentPane().add(new ComboTest());
        f.setSize(900,400);
        f.setLocation(500,500);
        f.setVisible(true);
        
    }
}
