/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client;

import com.auto.client.main.Main;
import com.auto.client.model.CmdSuggestor;
import com.auto.client.model.DataValueSuggestor;
import com.auto.client.model.GeneralSuggestor;
import com.auto.client.model.InputDataSuggestor;
import com.auto.client.model.MsgWithDelete;
import com.auto.client.model.PortSuggestor;
import com.auto.client.model.StateSuggestor;
import com.auto.client.model.UserSuggestor;
import com.auto.model.DataValue;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Chantal
 */
public class FlowComposer extends JPanel implements Composer{

    private static final Logger log = Logger.getLogger(FlowComposer.class.getName());

    private String wholestring;
    MsgWithDelete txt;

    Dimension dimtxt = new Dimension(250, 35);
    User user;
    UserSuggestor auser;
    PortSuggestor aport;
    StateSuggestor astate;
    DataValueSuggestor asub;
    CmdSuggestor acmd;
    InputDataSuggestor aproc;

    private int which;
    Port port;
    State state;
    InputData proc;

    GeneralSuggestor[] boxes;
    ActionListener listener;
    int height;
    public FlowComposer(ActionListener listener) {
        this.listener = listener;
        //  setLayout(new GridLayout(1, 8));
        setLayout(null);
        height = 130;
        which = 0;
        boxes = new GeneralSuggestor[20];
    }

    public void start() {
        addUserBox();
    }

    public String getResult() {
        return wholestring;
    }

    public void addActionListener(ActionListener listener) {
        this.listener = listener;
    }

    private void addMsgBox(JComponent p) {
        int width = 120;
        int gap = 2;
       
        int x = gap + which * (width + gap);
        p.setBounds(x, gap, width, height);

        this.add(p);
        if (which > 0) {
            boxes[which - 1].setEnabled(false);
            boxes[which - 1].repaint();
        }
        p("added msg box " + which + " at x=" + x);
        this.invalidate();
        this.revalidate();
        this.paintImmediately(0, 0, 500, height);
        p.paintImmediately(0, 0, 500, height);
        which++;
    }

    private void addBox(final GeneralSuggestor p) {
        int width = 120;
        int gap = 2;
       
        boxes[which] = p;
        p.setNr(which);
        if (which > 0) {
            boxes[which - 1].setEnabled(false);
            boxes[which - 1].repaint();
        }
        int x = gap + which * (width + gap);
        p.setBounds(x, gap, width, height);

        this.add(p);
        p.addDeleteListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                p("Delete called");
                int nr = p.getNr();
                GeneralSuggestor toremove = boxes[nr];
                remove(toremove);                
                if (nr > 0) {
                    boxes[nr - 1].setEnabled(true);
                    which--;
                } else {
                    cancel();
                }
                invalidate();
                revalidate();
                paintImmediately(0, 0, 500, 100);

            }
        });
        p("added box " + which + " at x=" + x);
        this.invalidate();
        this.revalidate();
        this.paintImmediately(0, 0, 500, height);
        p.paintImmediately(0, 0, 500, height);
        which++;
    }
    public void cancel() {
        listener.actionPerformed(new ActionEvent(this, 0, "CANCEL"));
    }
    private void addMsgBox() {
        txt = new MsgWithDelete("Msg");
        addMsgBox(txt);
        txt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {                
                ok();
            }
        });

    }
    
    public void ok() {
        String msg = null;
        if (txt != null) msg = txt.getValue();
        if (msg != null) {
            wholestring += msg;
        }
        FlowManager man = FlowManager.getManager();
        FlowData fd = new FlowData(man.getNextFlowNr());
        
        fd.setCreator(man.getMe().getId());
        if (port != null) fd.setProtocol(this.port.getId());
        if (state != null) fd.setState(this.state.getName());
        else p("NO STATE");
        fd.setUuid("ABCD"+fd.getId());
        if (txt != null) fd.setData(txt.getValue());
        if (proc != null) fd.setSubject(proc.getName());
        else p("NO PROC");
        
        fd.setAssignee(user.getId());
        fd.setUsers("["+man.getMe().getId()+","+user.getId()+"]");
        
        Flow flow = man.addCreatedFlow(fd, true);
        
        listener.actionPerformed(new ActionEvent(flow, 0, "OK"));
    }

    private void addSubBox() {
        asub = new DataValueSuggestor(Main.mainFrame, proc);
        addBox(asub);
        asub.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DataValue subproc = (DataValue) asub.getValue();
                if (subproc != null) {
                    wholestring += subproc.getName() + ".";
                    if (subproc.getSubValues()!= null && subproc.getSubValues().size() > 0) {
                        addSubBox();
                    } else {
                        addMsgBox();
                    }

                }
            }
        });
    }

    private void addProtBox() {

        aproc = new InputDataSuggestor(Main.mainFrame, state);
        addBox(aproc);
        aproc.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                InputData proc = (InputData) aproc.getValue();
                if (proc != null) {
                    wholestring += proc.getName() + ".";
                    if (proc.getValues()!= null) {
                        addSubBox();
                    } else {
                        addMsgBox();
                    }
                }
            }
        });
    }
     private void addCmdBox() {

        acmd = new CmdSuggestor(Main.mainFrame, port, state);
        addBox(acmd);
        acmd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                InputData proc = (InputData) acmd.getValue();
                if (proc != null) {
                    wholestring += proc.getName() + ".";
                    if (proc.getValues()!= null) {
                        addProtBox();
                    } else {
                        addMsgBox();
                    }
                }
            }
        });
    }

    private void addStateBox() {
       
        astate = new StateSuggestor(Main.mainFrame, port);
        addBox(astate);
        astate.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                state = (State) astate.getValue();
                if (state == null) {
                    return;
                }
                wholestring += state.getName() + ".";
                p("Got state: " + state);
                if (state.getInputDatas()!= null && state.getInputDatas().size()>0) {
                    addCmdBox();
                } else {
                    addMsgBox();
                }

            }

        });
    }

    private void addPortBox() {
      
        aport = new PortSuggestor(Main.mainFrame, user);
        addBox(aport);

        aport.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                port = (Port) aport.getValue();
                if (port == null) {
                    return;
                }
                p("Got port: " + port);
                wholestring += port.getName() + ".";

                if (port.getFirstStates()!= null) {
                    addStateBox();
                } else {
                    p("Port has no states: " + port);
                    log.info("Port " + port.getName() + " has no states");
                    return;
                }

            }

        });

    }

    public void addUserBox() {
        p("Creating userbox");
        auser = new UserSuggestor(Main.mainFrame);

        addBox(auser);

        auser.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                user = (User) auser.getValue();
                if (user == null) {
                    return;
                }
                p("Got user: " + user);
                if (user.getPorts() != null) {
                    wholestring = user.getId() + ".";
                    addPortBox();

                } else {
                    JOptionPane.showMessageDialog(null, "User " + user.getId() + " has no ports");
                    return;
                }
            }

        });

    }

    private void p(String s) {
        log.info(s);
    }
}
