/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.client;

import com.auto.client.main.Prefs;
import com.auto.client.main.MainPanel;
import com.auto.model.Flow;
import com.auto.model.FlowManager;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import com.auto.xmpp.ChatListener;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Chantal
 */
public class LoginPanel extends javax.swing.JPanel {

     FlowManager man = FlowManager.getManager();
   private static final Logger log = Logger.getLogger(LoginPanel.class.getName());

     Properties prefs = Prefs.getPrefs();
      MainPanel main;   
      private String username;
      private String pw;
    /**
     * Creates new form LoginPanel
     */
    public LoginPanel(MainPanel main, String username, String pw) {
        initComponents();
        this.main = main;
        //get data from preferences
        if (username == null) this.username = prefs.getProperty("username", "chantal");
        if (pw == null) this.pw = prefs.getProperty("password", "chantal");
        this.txtUser.setText(username);
        this.txtPw.setText(pw);
    }

    
    public void login() {
         try {
             // log in
             p("========== LOGIN ========= ");
             username = txtUser.getText();
             pw = new String(txtPw.getPassword());
             man.login(username, pw);
             man.loginXmpp();
             // load data
             man.loadData();
             main.setStatus("Logged in as "+man.getMe().getEmail());
             prefs.put("username", username);
             prefs.put("password", pw);
             Prefs.save();
             // get offline messages
             main.loginDone();
         } catch (Exception ex) {
             p("========    LOGIN FAILED =======:"+ex.getMessage());
             ex.printStackTrace();                          
         }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtUser = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtPw = new javax.swing.JPasswordField();
        btnLogin = new javax.swing.JButton();

        jTextField1.setText("jTextField1");

        setLayout(new java.awt.BorderLayout());

        jLabel1.setText("<html><h2>Login</h2?</html>");
        add(jLabel1, java.awt.BorderLayout.PAGE_START);

        jLabel2.setText("Username:");

        txtUser.setText("chantal");

        jLabel3.setText("Password:");

        txtPw.setText("chantal");

        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnLogin)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtPw, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                        .addComponent(txtUser)))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtPw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLogin)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        login();    
    }//GEN-LAST:event_btnLoginActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JPasswordField txtPw;
    private javax.swing.JTextField txtUser;
    // End of variables declaration//GEN-END:variables

    private void p(String msg) {
       log.info(msg);
    }
}
