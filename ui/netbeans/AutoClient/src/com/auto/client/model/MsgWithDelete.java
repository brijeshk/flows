/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.client.main.Gui;
import com.auto.model.FlowManager;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Chantal
 */
public class MsgWithDelete extends JPanel {

    private static final Logger log = Logger.getLogger(GeneralSuggestor.class.getName());

    protected static FlowManager man = FlowManager.getManager();
    protected String title;
    JButton btn;
    JTextField txt;
    JLabel lbl;

    public MsgWithDelete(String title) {
        super();
        this.title = title;
        createUI();

    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        txt.setEnabled(b);
        btn.setEnabled(b);
    }

    public void addActionListener(final ActionListener listener) {
        txt.addActionListener(listener);
        txt.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                //p("Got tab");
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    //  p("keyPressed Got tab");
                    listener.actionPerformed(new ActionEvent(txt, 0, "OK"));
                }
                else if (e.getKeyChar() == '@') {
                    p("Got @");
                    
                }
                

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
    }

    public void addDeleteListener(ActionListener listener) {
        btn.addActionListener(listener);
    }

    protected void createUI() {
        setLayout(null);
        // if (title != null) lbl = new JLabel(title);
        txt = new JTextField(50);
        txt.setEditable(true);
        txt.setToolTipText("Please enter a msg");
        btn = new JButton();
        btn.setIcon(Gui.IMG_DELETE);
        btn.setMargin(new Insets(0, 0, 0, 0));
        //   btn.setSize(25,25);
        int w = 90;
        int h = 25;
        int y = 27;
        lbl = new JLabel(title);
        lbl.setBounds(2, 2, w, h);
        txt.setBounds(2, y, w, h);
        btn.setBounds(w + 4, y, h, h);

        if (lbl != null) {
            add("North", lbl);
        }
        add("West", txt);
     //   add("East",btn);

    }

    public String getValue() {
        return txt.getText();
    }

    protected static void p(String msg) {
        log.info(msg);
    }

}
