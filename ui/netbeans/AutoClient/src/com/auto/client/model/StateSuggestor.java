/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JFrame;
 
/**
 *
 * @author Chantal
 */
public class StateSuggestor extends GeneralSuggestor {

    private static final Logger log = Logger.getLogger(PortSuggestor.class.getName());

    private Port port;
    private State state;
    
    public StateSuggestor(JFrame frame, Port port) {
        super(frame, "State");
        this.port = port;
        super.createUI();
    }
    public StateSuggestor(JFrame frame, Port port, State state) {
        super(frame, "Transition");
        this.state = state;
        this.port = port;
        super.createUI();
    }

    @Override
    protected Vector getWords() {
        Vector<String> words = new Vector<>();
        if (state != null) {
             for (String trans : state.getTransitions()) {
                words.add(trans);
            }
        }
        else if (port != null) {
            for (State p : port.getFirstStates()) {
                words.add(p.getName());
            }
        }
       // p( "Got words: "+words);
       return words;
    }

    @Override
    public Object getValue() {
        String word = getSelected();
        p("State: word=" + word);
        return port.getState(word);
       
    }

}
