/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.model.DataValue;
import com.auto.model.InputData;
import com.auto.model.State;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JFrame;
 
/**
 *
 * @author Chantal
 */
public class DataValueSuggestor extends GeneralSuggestor {

    private static final Logger log = Logger.getLogger(PortSuggestor.class.getName());

    private final InputData data;
    public DataValueSuggestor(JFrame frame, InputData data) {
        super(frame, "Value");
        this.data = data;
      createUI();
    }

    @Override
    protected Vector getWords() {
        Vector<String> words = new Vector<>();       
        for (DataValue p : data.getValues()) {
            words.add(p.getName());
        }
       return words;
    }

    @Override
    public Object getValue() {
        String word = getSelected();
        p("DataValue: word=" + word);
        return data.getSubValue(word);
    }

}
