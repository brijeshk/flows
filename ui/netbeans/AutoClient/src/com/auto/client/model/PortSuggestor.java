/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.model.Port;
import com.auto.model.User;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JFrame;
 
/**
 *
 * @author Chantal
 */
public class PortSuggestor extends GeneralSuggestor {

    private static final Logger log = Logger.getLogger(PortSuggestor.class.getName());

    private final User user;
    public PortSuggestor(JFrame frame, User user) {
        super(frame, "Ports");
        this.user = user;
        createUI();
    }

    @Override
    protected Vector getWords() {
        p("=========== Creating PortSuggestor ==========");
        Vector<String> words = new Vector<>();
        for (Port port : user.getPorts()) {
            words.add(port.getName());
        }
       // p( "Got words: "+words);
       return words;
    }

    @Override
    public Object getValue() {
        String word = getSelected();
        p("getPort: word=" + word);
        return user.getPort(word);
    }

}
