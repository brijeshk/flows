/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.client.Composer;
import com.auto.client.main.Main;
import static com.auto.client.model.MsgWithBoxes.p;
import com.auto.model.Atom;
import com.auto.model.AtomData;
import com.auto.model.DataValue;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;

/**
 *
 * @author Chantal
 */
public class AtomTextComposer extends MsgWithBoxes implements Composer {
   
    
    public AtomTextComposer(ActionListener listener, JFrame parentframe, String title, Flow flow) {
        super(listener, parentframe, title);
        this.flow = flow;
        this.port = flow.getPort();
        this.state = flow.getState();
        for (User u: flow.getUsers()) {
            if (u.isSystem()) bot = true;
            txtbody.setEditable(false);            
            txtbody.setText("(custom msg is disabled for bots)");
            break;
        }
        datas = new ArrayList<InputData>();
        // next state?
        p("=================   AtomTextComposer: port:"+port.getName()+", state="+state.getName());
        p("Got port: "+port.toString());
        p("Got next possible commands; "+port.getInputDatas(state.getName()));
     //   p("state json: "+state.toJSON());
        p("state trans: "+state.getTransitions());
        
    }

    public void start() {
        maybeAddSuggestor("cmd");
    }

    @Override
    protected boolean handleKey(KeyEvent e) {
         p("Got key char: "+e.getKeyChar());
        if (e.getKeyCode() == KeyEvent.VK_TAB) {
            p("keyPressed Got tab");
            extractSuggestorValue();
            return true;

        } else if (e.getKeyChar() == '.') {
            maybeAddSuggestor(".");
            return true;

        } else if (e.getKeyChar() == '#') {
            maybeAddSuggestor("#");
            return true;

        } else if (e.getKeyChar() == '!' ) {
            maybeAddSuggestor("!");
            return true;

        } else if (e.getKeyChar() == '@') {
            maybeAddSuggestor("@");
            return true;
        }
        return false;
    }

    @Override
    protected void handleSuggestorValue() {
        if (suggestor == null) return;
        if (suggestor instanceof UserSuggestor) {
            users = (List<User>) suggestor.getValues();
            p("Got users " + users);   
            maybeAddSuggestor("port");
        } else if (suggestor instanceof StateSuggestor) {
            state = (State) suggestor.getValue();
            p("Got state: " + state);
            maybeAddSuggestor("proc");
        } else  if (suggestor instanceof PortSuggestor) {
            port = (Port) suggestor.getValues();
            p("Got Port " + port);
            maybeAddSuggestor("state");
        } else if (suggestor instanceof CmdSuggestor) {
            state = (State) suggestor.getValue();
            p("Got State: " + state);                    
            maybeAddSuggestor("data");
        } 
    }

    // for input data, we have to iterate over all required and options fields
    
    @Override
    protected void maybeAddSuggestor(String context) {
        p("maybAddSuggestor: " + context);
       suggestor = null;
        if (context.equals("@") || context.equals("user")) {
            suggestor = new UserSuggestor(null);
            addSuggestor();
        } else if (context.equals("!") || context.equalsIgnoreCase("port")) {
            if (users == null)  return;            
            suggestor = new PortSuggestor(Main.mainFrame, users.get(0));
            addSuggestor();
        } else if (context.equals("#") || context.equalsIgnoreCase("trans")) {
            if (port == null || port.getFirstStates()== null || state == null || state.getTransitions()== null)  return;            
            suggestor = new StateSuggestor(Main.mainFrame, port, state);
            addSuggestor();
        } else if ( context.equalsIgnoreCase("state")) {
            if (port == null || port.getFirstStates()== null )  return;            
            suggestor = new StateSuggestor(Main.mainFrame, port);
            addSuggestor();
        } else if (context.equalsIgnoreCase("cmd")) {
            if (port == null || state == null || port.getInputDatas(state.getName()) == null) {
                maybeAddSuggestor("trans");
                return;
            }            
            suggestor = new CmdSuggestor(Main.mainFrame, port, state);
            addSuggestor();
        } else if (context.equalsIgnoreCase("data")) {
             suggestor = null;
            if (state == null || state.getInputDatas()== null) return;                      
            datas = InputManager.getInputForState(state, flow);
            super.computeText();
        } 
        else if (context.equals(".")) {
            p("Got dot...");
        }
    }
     @Override
     public void ok() {
      
        man = FlowManager.getManager();
        AtomData ad = new AtomData(man.getNextAtomNr());
        ad.setSender(man.getMe().getId());
        if (port != null) {
            ad.setPort_id(port.getId());
            flow.setPort(port);
        }
        if (state != null) {
            p("Got state "+state+" for atom");
            ad.setState(this.state.getName());
            flow.setState(state);
            
        }
        else {
            p("GOT NO STATE FOR ATOM");
            ad.setState(flow.getState().getName());
        }
        if (!bot) {
            ad.setBody(txtbody.getText());
        }
        ad.setCreated(System.currentTimeMillis());
        ad.setCrud_flag("c");
       
        
        Atom atom = man.addCreatedAtom(flow, ad);
        if (datas != null) {
            atom.setData(datas);
            man.update(atom, "u");
        }
        
        man.update(flow, "u");
        listener.actionPerformed(new ActionEvent(atom, 0, "OK"));
     }
   

}
