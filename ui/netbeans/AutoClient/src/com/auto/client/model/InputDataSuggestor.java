/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.model.InputData;
import com.auto.model.State;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JFrame;
 
/**
 *
 * @author Chantal
 */
public class InputDataSuggestor extends GeneralSuggestor {

    private static final Logger log = Logger.getLogger(PortSuggestor.class.getName());

    private final State state;
    public InputDataSuggestor(JFrame frame, State state) {
        super(frame, "Commands");
        this.state = state;
        createUI();
    }

    @Override
    protected Vector getWords() {
        Vector<String> words = new Vector<>();
        
        for (InputData p : state.getInputDatas()) {
            words.add(p.getName());
        }
       // p( "Got words: "+words);
       return words;
    }

    @Override
    public Object getValue() {
        String word = getSelected();
        return state.getInputData(word);
    }

}
