/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import static com.auto.client.model.GeneralSuggestor.man;
import com.auto.model.User;
import com.auto.model.UserNameComparator;
import com.auto.model.UserTypeComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author Chantal
 */
public class UserSuggestor extends GeneralSuggestor {

    private static final Logger log = Logger.getLogger(UserSuggestor.class.getName());

    public UserSuggestor(JFrame frame) {
        super(frame, "Users");  
        createUI();
    }

    @Override
    protected Vector getWords() {
        Vector<String> words = new Vector<>();
        List<User> users = man.getUsers();
        Collections.sort(users, new UserTypeComparator());
        
        for (User user : users) {
            p("Got user: "+user.getId());
            if (!man.isMe(user)) {               
                words.add(user.getId());
            }            
            else  p("This one is me: "+user.getId());
        }
        // bug in this component doesn't show last one
        words.add("");
     
       return words;
    }

    @Override
    public Object getValue() {
        String word = getSelected();
        p("getUser: word=" + word);
        return man.getUser(word);
    }
    
     @Override
    public List getValues() {
        List<String> values = getSelectedValues();
        List<User> res = new ArrayList<User>();
        for (String value: values) {
            p("getUser: word=" + value);
            res.add(man.getUser(value));
        }
        return res;
    }

}
