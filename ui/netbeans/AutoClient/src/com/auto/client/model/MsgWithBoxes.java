/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.client.main.Gui;
import com.auto.client.utils.ActionJList;
import com.auto.model.DataValue;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Chantal
 */
public class MsgWithBoxes extends JPanel {

    protected Port port;
    protected State state;
    protected InputData data;
    protected DataValue value;
    protected List<InputData> datas;
    protected Flow flow;
    protected List<User> users;
    boolean bot = false;

    private static final Logger log = Logger.getLogger(GeneralSuggestor.class.getName());
    protected static FlowManager man = FlowManager.getManager();
    protected String title;
    protected JButton options;
    protected JLabel txtcomp;
    protected JTextField txtbody;
    protected JLabel lbl;

    protected GeneralSuggestor suggestor;
    protected MyPopup pop;
    protected final JFrame parentframe;
    protected ActionListener listener;

    public MsgWithBoxes(ActionListener listener, JFrame parentframe, String title) {
        super();
        this.listener = listener;
        this.parentframe = parentframe;
        JFrame.setDefaultLookAndFeelDecorated(false);
        this.title = title;
        createUI();
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);       
        txtbody.setEditable(b);      
    }

    public void addActionListener(final ActionListener listener) {
        txtbody.addActionListener(listener);
        txtbody.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                if (suggestor != null) {
                    p("Got txt focusGained, extracting value");
                    extractSuggestorValue();
                }
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        KeyListener kl = new MyKeyListener();
        txtcomp.addKeyListener(kl);
        this.addKeyListener(kl);
    }

    private class MyPopup extends JFrame {

        public MyPopup(JComponent comp, String title) {
            super();
            super.setIconImage(Gui.IMG_QUESTION_YELLOW16.getImage());
            setTitle(title);
            setLayout(new FlowLayout());
          //  setUndecorated(true);
            setSize(250, 200);
          //  setBackground(new Color(0, 0, 0, 0));
            JPanel panel = new JPanel() {
//                @Override
//                protected void paintComponent(Graphics g) {
//                    if (g instanceof Graphics2D) {
//                        final int R = 240;
//                        final int G = 240;
//                        final int B = 255;
//                        Paint p = new GradientPaint(0.0f, 0.0f, new Color(R, G, B, 0),
//                                0.0f, getHeight(), new Color(R, G, B, 255), true);
//                        Graphics2D g2d = (Graphics2D) g;
//                        g2d.setPaint(p);
//                        g2d.fillRect(0, 0, getWidth(), getHeight());
//                    }
//                }
            };
            setContentPane(panel);
            setLayout(new GridBagLayout());

//            addComponentListener(new ComponentAdapter() {
//                // Give the window an elliptical shape.
//                // If the window is resized, the shape is recalculated here.
//                @Override
//                public void componentResized(ComponentEvent e) {
//                    //setShape(new RoundRectangle2D.Float(0, 0, 200, 250, 10, 10));
//                    setShape(new Rectangle2D.Double(0, 0, 200, 250));
//                }
//            });
            setAlwaysOnTop(true);
            // setOpacity(0.8f);           
            setLocationRelativeTo(this);
            add(comp);
            pack();
            setVisible(true);
        }
    }

    private class MyKeyListener implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            //p("Got tab");
        }

        @Override
        public void keyPressed(KeyEvent e) {
            handleKey(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

    protected boolean handleKey(KeyEvent e) {
        // p("Got key :"+e.getKeyChar());
        if (e.getKeyCode() == KeyEvent.VK_TAB) {
            p("keyPressed Got tab");
            extractSuggestorValue();
            return true;

        } else if (e.getKeyChar() == '.') {
            maybeAddSuggestor(".");
            return true;

        } else if (e.getKeyChar() == '@') {
            maybeAddSuggestor("@");
            return true;
        }
        return false;
    }

    protected void maybeAddSuggestor(String context) {
        p("maybeAddSuggestor: " + context);
        if (context.equals("@")) {
            suggestor = new UserSuggestor(null);
            addSuggestor();
        }
    }

    protected void addSuggestor() {

        
        p("Adding suggestor " + suggestor.getClass().getName());
       
        suggestor.createUI();

        if (suggestor.getWords().size() ==1) {
            suggestor.setFirstValue();
            computeText();
            handleSuggestorValue();
            return;
        }
        ActionJList box = (ActionJList) suggestor.getBox();

        pop = new MyPopup(box, suggestor.getName());
        pop.setLocation(500, 450);
        pop.requestFocusInWindow();
        box.requestFocus();
        box.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                p("Got suggestor action " + suggestor.getClass().getName() + " with value " + suggestor.getValue());
                extractSuggestorValue();
            }
        });

        box.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            }

            @Override
            public void focusLost(FocusEvent e) {
              //  p("Got suggestor focusLost, extracting value");
                //  extractSuggestorValue();
            }
        });
        pop.setVisible(true);
    }

    protected void handleSuggestorValue() {
        if (suggestor instanceof UserSuggestor) {
            users = (List<User>) suggestor.getValues();
            p("Got user " + users);
        } else {
            p("Don't know how to handle suggestor " + suggestor);
        }
    }

    protected boolean extractSuggestorValue() {
        p("=========extractSuggestorValue ======");
        if (pop != null) {
            pop.setVisible(false);
            pop.dispose();
            pop = null;
            //p("extractSuggestorValue: removed popover");
        }
        if (suggestor == null) {
            p("problem: extractSuggestorValue: no suggestor");
            return false;
        }
        String word = suggestor.getSelected();
        p("Got suggestor " + suggestor.getClass().getName() + ", using value: " + word);

        computeText();
        handleSuggestorValue();
        return true;
    }

    public boolean hasUsers() {
        return users != null && users.size() > 0;
    }

    public boolean hasData() {
        return datas != null && datas.size() > 0;
    }

    public boolean hasPort() {
        return port != null;
    }

    public boolean hasState() {
        return state != null;
    }

    protected void computeText() {
        String txt = "";
        if (hasUsers()) {
            txt += "@";
            for (int i = 0; i < users.size(); i++) {
                txt += users.get(i).getId();
                if (i + 1 < users.size()) {
                    txt += ",";
                }
            }
            txt += ".";
        } else {
            txt = "(no text yet)";
            this.txtcomp.setText(txt);
            return;
        }
        if (port != null) {
            txt += port.getName();
        }
        if (state != null) {
            txt += "." + state.getName();
        }
        if (datas != null && datas.size() > 0) {
            p("compute text, got datas: " + datas);
            for (int i = 0; i < datas.size(); i++) {
                InputData data = datas.get(i);
                DataValue val = data.getValue();
                txt += "." + data.getName() + "=";
                if (val == null) {
                    txt += "none";
                } else {
                    txt += val.getValue();
                }

            }
        }
        p("Computed text: "+txt);
        this.txtcomp.setText("Computed text: "+txt);
    }

    protected void createUI() {
        p("============== MSG WITH BOXES ==================");
        
        // if (title != null) lbl = new JLabel(title);
        txtcomp = new JLabel("Computed text:");
       // txtcomp.setLayout(null);
       // txtcomp.setEditable(false);
        txtcomp.setBackground(Color.lightGray.brighter());
        txtcomp.setEnabled(true);
        txtbody = new JTextField(40);
        txtbody.setEditable(true);
   
        int h = 25;
        options = new JButton();
        Dimension dim = new Dimension(h, h);
        JPanel dummy = new JPanel();
        dummy.setPreferredSize(dim);
        options.setPreferredSize(dim);
        options.setMaximumSize(dim);
        
        //options.setIcon(Gui.IMG_AT);
        options.setIcon(Gui.IMG_AT_16);
        options.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // show list of things to do
                String title = "Pick one";
                String msg= "What do you want to do?";
                if (!hasUsers()) {
                    maybeAddSuggestor("@");
                }
                else if (port == null) {
                    maybeAddSuggestor("!");
                }
                else if (state == null) {
                    maybeAddSuggestor("#");
                }
                else {
                    String[] options=  { "1) Add/Change users", "2) Change topic", "3) Change state", "4) Add/Change data" };
                    String first = "1) Add/Change users";
                    int ans = JOptionPane.showOptionDialog(parentframe, msg, title, JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null,  options, first);
                    if (ans != JOptionPane.CLOSED_OPTION) {
                        p("Got option: "+options[ans]);
                        if (ans == 0) maybeAddSuggestor("@");
                        else if (ans == 1)maybeAddSuggestor("!");
                        else if (ans == 2)maybeAddSuggestor("#");
                        else if (ans == 3)maybeAddSuggestor("data");
                    }
                }
            }
            
        });
       
        JPanel main = new JPanel();
        main.setLayout(new BorderLayout());
                
        JPanel pantxt = new JPanel();
        pantxt.setLayout(new BorderLayout() );
       JPanel pancomp = new JPanel();
        pancomp.setLayout(new BorderLayout() );
        
        pancomp.add("West", dummy);
        pancomp.add("Center", txtcomp);
        pantxt.add("West", options);
        pantxt.add("Center", txtbody);

        main.add("North", pancomp);
        main.add("South", pantxt);
        add("West", main);
       
    }

    public String getBody() {
        return txtbody.getText();
    }

    public String getComputdText() {
        return txtcomp.getText();
    }

    protected static void p(String msg) {
        log.info(msg);
    }

    public void cancel() {
        listener.actionPerformed(new ActionEvent(this, 0, "CANCEL"));
    }

    public void ok() {
        String msg = null;
        if (txtbody != null) {
            msg = txtbody.getText();
        }
        p("IMPLEMENT OK");
    }

    public String getResult() {
        return this.txtbody.getText();
    }

}
