/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.client.main.Gui;
import com.auto.client.utils.ActionJList;
import com.auto.model.FlowManager;
import com.auto.model.User;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

/**
 *
 * @author Chantal
 */
public abstract class GeneralSuggestor extends JPanel {

    private static final Logger log = Logger.getLogger(GeneralSuggestor.class.getName());

    protected static FlowManager man = FlowManager.getManager();
    protected final JFrame parentFrame;
    protected String title;
    private JButton btn;
   // private JComboBox box;
    private ActionJList box;
    private JLabel lbl;
    private int nr;
    
    boolean multi;

    public GeneralSuggestor(JFrame parentFrame, String title) {
        this(parentFrame, title, false);
    }
    public GeneralSuggestor(JFrame parentFrame, String title, boolean multi) {
        super();
        this.multi = multi;
        this.title = title;
        this.parentFrame = parentFrame;
    }

    public JList getBox() {
        return box;
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        box.setEnabled(b);
        btn.setEnabled(b);
    }

    public void addActionListener(final ActionListener listener) {
        box.addActionListener(listener);
       
        box.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                p("Got KeyEvent ");
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    p("keyPressed Got tab");
                    listener.actionPerformed(new ActionEvent(box, 0, "OK"));
                } else if (e.getKeyChar() == '.') {
                    p("keyPressed Got dot");
                    listener.actionPerformed(new ActionEvent(box, 0, "OK"));
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
    }

    public void addDeleteListener(ActionListener listener) {
        btn.addActionListener(listener);
    }

    void setFirstValue() {
        box.setSelectedIndex(0);
    }

    private class MyRenderer extends DefaultListCellRenderer {

        public MyRenderer() {
            super();

        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
           
            JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            String word = (String) value;
            //setIcon(Gui.IMG_AT);
            return label;
        }
    }

    protected void createUI() {
        setLayout(null);
        // if (title != null) lbl = new JLabel(title);
        Vector<String> words = (Vector<String>) getWords();
      //  Collections.sort(words);
       // DefaultComboBoxModel model = new DefaultComboBoxModel(words);
       //  DefaultListModel model = new DefaultListModel(words);
         box = new ActionJList(words, multi);
       // box.setRenderer(new MyRenderer());
       // box.setEditable(false);
        box.setFocusTraversalKeysEnabled(false);
        box.setToolTipText("Please pick a " + title);
        btn = new JButton();
        btn.setIcon(Gui.IMG_DELETE);
        btn.setMargin(new Insets(0, 0, 0, 0));
        //   btn.setSize(25,25);
        int w = 90;
        int h = 25;
        int y = 27;
        lbl = new JLabel(title);
        lbl.setBounds(2, 2, w, h);
        
        box.setBounds(2, y, w, h+25);
        btn.setBounds(w + 4, y, h, h);
        if (lbl != null) {
            add("North", lbl);
        }

        add("West", box);
        //add("East",btn);

    }

    protected abstract List<String> getWords();

    public String getSelected() {
       // return (String) box.getSelectedItem();
         return (String) box.getSelectedValue();
    }
    
    public List<String> getSelectedValues() {
       // return (String) box.getSelectedItem();        
         return (List<String> ) box.getSelectedValuesList();
    }

    public abstract Object getValue();
    
  
    public List getValues() {
       return null;
    }

    protected static void p(String msg) {
        log.info(msg);
    }

    /**
     * @return the nr
     */
    public int getNr() {
        return nr;
    }

    /**
     * @param nr the nr to set
     */
    public void setNr(int nr) {
        this.nr = nr;
    }

}
