/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.client.Composer;
import com.auto.client.main.Main;
import static com.auto.client.model.MsgWithBoxes.p;
import com.auto.model.Atom;
import com.auto.model.DataValue;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.JFrame;

/**
 *
 * @author Chantal
 */
public class FlowTextComposer extends MsgWithBoxes implements Composer {

    public FlowTextComposer(ActionListener listener, JFrame parentframe, String title, Flow flow) {
        super(listener, parentframe, title);
    }

    public void start() {
        maybeAddSuggestor("@");
    }

    @Override
    protected boolean handleKey(KeyEvent e) {
        p("Got key char: "+e.getKeyChar());
        if (e.getKeyCode() == KeyEvent.VK_TAB) {
            p("keyPressed Got tab");
            extractSuggestorValue();
            return true;

        } else if (e.getKeyChar() == '.') {
            maybeAddSuggestor(".");
            return true;
        } else if (e.getKeyChar() == '#') {
            maybeAddSuggestor("#");
            return true;

        } else if (e.getKeyChar() == '!' ) {
            maybeAddSuggestor("!");
            return true;

        } else if (e.getKeyChar() == '@') {
            maybeAddSuggestor("@");
            return true;
        }
        return false;
    }

    @Override
    protected void handleSuggestorValue() {
        if (suggestor instanceof UserSuggestor) {
            users = (List<User>) suggestor.getValues();
            p("Got users " + users);
            boolean bot = false;
            txtbody.setEnabled(true);
            txtbody.setText("");
            for (User u: users) {
                if (u.isSystem()) {
                    txtbody.setEnabled(false);
                    txtbody.setText("(custom msg is disabled for bots)");
                    break;
                }
            }
            maybeAddSuggestor("port");
        } else if (suggestor instanceof PortSuggestor) {
            port = (Port) suggestor.getValue();
            p("Got port: " + port);
            maybeAddSuggestor("state");
        } else if (suggestor instanceof StateSuggestor) {
            state = (State) suggestor.getValue();
            p("Got astate: " + state+", state data="+state.getInputDatas());
            maybeAddSuggestor("data");
        } 
    }

    @Override
    protected void maybeAddSuggestor(String context) {
        p("maybAddSuggestor: " + context);
       suggestor = null;
        if (context.equals("@") || context.equals("user")) {
            suggestor = new UserSuggestor(null);
            addSuggestor();
        } else if (context.equals("!") ||context.equalsIgnoreCase("port")) {
            if (users == null)  return;            
            suggestor = new PortSuggestor(Main.mainFrame, users.get(0));
            addSuggestor();
        } else if (context.equals("#") ||context.equalsIgnoreCase("state")) {
            if (port == null || port.getFirstStates() == null)  return;            
            suggestor = new StateSuggestor(Main.mainFrame, port);
            addSuggestor();
        } else if (context.equalsIgnoreCase("cmd")) {
            if (port == null || state == null || port.getInputDatas(state.getName()) == null)  return;            
            suggestor = new CmdSuggestor(Main.mainFrame, port, state);
            addSuggestor();
        } else if (context.equalsIgnoreCase("data")) {
            suggestor = null;
            if (state == null || state.getInputDatas()== null) {
                p("No state or no input datas: "+state);
                return;
            }   
            
            datas = InputManager.getInputForState(state, null);
            super.computeText();
        }
        else if (context.equals(".")) {
            p("Got dot...");
        }
    }
    @Override
     public void ok() {
        if (users == null || port == null) {
            cancel();
            return;
        }
        
        String msg = null;
        if (txtbody != null && !bot && txtbody.isEditable()) msg = txtbody.getText();
       
        man = FlowManager.getManager();
        FlowData fd = new FlowData(man.getNextFlowNr());
        
        fd.setCreator(man.getMe().getId());
        fd.setProtocol(port.getId());
                
        if (state != null) fd.setState(this.state.getName());
        else p("NO STATE");
        fd.setUuid("ABCD"+fd.getId());
        
        if (port != null) fd.setSubject(port.getName());
        else p("NO PROC");
        
        String susers = man.getMe().getId()+",";
        for (User u: users) {
            if (u.isSystem()) msg = null;
            susers = susers+u.getId()+",";
        }
        fd.setUsers(susers.substring(0, susers.length()-1));
        fd.setAssignee(users.get(0).getId());
        
        Flow flow = man.addCreatedFlow(fd, true);
        Atom atom = flow.getFirstAtom();
        if (msg != null && !bot) {
            fd.setData(msg);
            atom.getAtomData().setBody(msg);
        }
        if (datas != null) {
            p("Got data to put into atom data: "+datas);
            atom.setData(datas);
            man.update(atom, "u");
        }
        else {
            p("Flow has no formal data");
        }
        listener.actionPerformed(new ActionEvent(flow, 0, "OK"));
    }

}
