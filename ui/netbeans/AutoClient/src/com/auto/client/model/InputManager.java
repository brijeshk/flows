/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.client.main.Main;
import static com.auto.client.model.MsgWithBoxes.p;
import com.auto.model.DataType;
import com.auto.model.DataValue;
import com.auto.model.Flow;
import com.auto.model.InputData;
import com.auto.model.State;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 * manage input that the user has to provide
 *
 * @author Chantal
 */
public class InputManager {

    private static final Logger log = Logger.getLogger(InputManager.class.getName());

    public static List<InputData> getInputForState(State state, Flow flow) {
        p(" ================ getInputForState " + state);
        List<InputData> inputdataresults = new ArrayList<InputData>();
        for (InputData p : state.getInputDatas()) {
            // ask user for input on those
            InputData inputData = p.clone();
            if (flow != null && flow.hasAtoms()) {
                inputData = p.computeInputDataForAtoms(flow.getAtoms());
            }
            List<DataValue> values = new ArrayList<DataValue>();
            if (inputData.hasValues()) {
                if ((p.isRequired() || p.isOptional()) && p.isSourceUser()) {
                    p("Unser must enter data: " + p);
                    if (inputData.isSelect1()) {
                        // show list and pick one - but first get data                        
                        DataValue value = null;
                        do {
                            value = InputManager.pickOne(p, inputData.getValues());
                            if (value != null) {
                                values.add(value);
                            }
                        } while (value == null && p.isRequired());
                    } else if (inputData.isSelectN()) {
                        // show list and pick one - but first get data                        
                        List<DataValue> res = null;
                        do {
                            res = InputManager.pickN(p, inputData.getValues());
                        } while (res == null && p.isRequired());
                        if (res != null) {
                            values = res;
                        }
                    } else {
                        for (DataValue val : inputData.getValues()) {
                            String ans = InputManager.enterText(p, val);
                            if (ans != null) {
                                values.add(val);
                            }
                        }
                    }
                }
            } else {
                DataValue dv = new DataValue(p.getName());

                String ans = InputManager.enterText(p, dv);
                if (ans != null) {
                    values.add(dv);
                }

            }
            inputData.setValues(values);
            inputdataresults.add(inputData);

        }
        p("Got inputdataresults: " + inputdataresults);
        return inputdataresults;
    }

    public static DataValue pickOne(InputData in, List<DataValue> values) {
        String title = in.getName();
        String selection[] = new String[values.size()];
        if (values.size() ==1) {
            p("Just return that one value");
            return values.get(0);
        }
        for (int i = 0; i < values.size(); i++) {
            DataValue v = values.get(i);
            selection[i] = v.getValue();
        }
        String message = "Please pick one value";

        Object answer = JOptionPane.showInputDialog(Main.mainFrame, message, title, JOptionPane.QUESTION_MESSAGE, null, selection, selection[0]);
        //p("User entered: "+answer);
        for (int i = 0; i < values.size(); i++) {
            DataValue v = values.get(i);
            if (answer.equals(v.getValue())) {
                p("pick1: user selected " + v);
                return v;
            }
           // else p("User did not pick "+v.getValue()+":"+answer);
        }
        p("Input matches none of the values");
        return null;
    }

    public static List<DataValue> pickN(InputData in, List<DataValue> values) {
        int nr = values.size();
        List<DataValue> res = new ArrayList<DataValue>();
        String title = in.getName();
        JPanel p = new JPanel();
         if (values.size() ==1) {
            p("Just return that one value");
            res.add(values.get(0));
            return res;
        }
        JPanel listpanel = new JPanel();
        listpanel.setLayout(new GridLayout(nr, 1));
        p.setLayout(new BorderLayout());
        p.add("Center", listpanel);

        JCheckBox boxes[] = new JCheckBox[nr];
        for (int i = 0; i < nr; i++) {
            DataValue v = values.get(i);
            JCheckBox box = new JCheckBox(v.getValue());
            boxes[i] = box;
            listpanel.add(box);
        }
        String message = "Please pick all values that apply";
        p.add("North", new JLabel(message));
        Object answer = JOptionPane.showInputDialog(Main.mainFrame, p, title, JOptionPane.QUESTION_MESSAGE);
       
        for (int i = 0; i < nr; i++) {
            JCheckBox box = boxes[i];
            if (box.isSelected()) {
                p("pickN: user selected " + values.get(i));
                res.add(values.get(i));
            }
        }
        if (res.size() < 1) {
            p("User picked nothing");
        }
        return res;
    }

    public static String enterText(InputData in, DataValue dv) {
        String title = in.getName();
        DataType type = in.getType();
        if (type.isDate()) {
            UtilDateModel model = new UtilDateModel();
            JDatePanelImpl datePanel = new JDatePanelImpl(model);
            JDatePickerImpl datePicker = new JDatePickerImpl(datePanel);
            int ans = JOptionPane.showConfirmDialog(Main.mainFrame, datePicker, title, JOptionPane.OK_CANCEL_OPTION);
            if (ans != JOptionPane.CANCEL_OPTION) {
                Date d = (Date) datePicker.getModel().getValue();
                dv.setValue(d.getMonth() + "/" + d.getDay() + "/" + d.getYear());
            }
        } else if (type.isQuantity()) {
            JSpinner sp = new JSpinner();
            SpinnerNumberModel m = new SpinnerNumberModel();
            m.setMinimum(0);
            m.setValue(1);
            m.setStepSize(1);
            sp.setModel(m);
            int ans = JOptionPane.showConfirmDialog(Main.mainFrame, sp, title, JOptionPane.OK_CANCEL_OPTION);
            if (ans != JOptionPane.CANCEL_OPTION) {
                int nr = m.getNumber().intValue();
                dv.setValue("" + nr);
            }
        } else {
            String message = "Please pick enter a value for " + dv.getName();
            String ans = JOptionPane.showInputDialog(Main.mainFrame, message, title, JOptionPane.QUESTION_MESSAGE);
            dv.setValue(ans);
            p("enterText: user entred:" + ans);
        }
        
        return dv.getValue();
    }

    private static void p(String s) {
        log.info(s);
    }
}
