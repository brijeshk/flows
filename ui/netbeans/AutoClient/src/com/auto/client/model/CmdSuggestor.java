/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.client.model;

import com.auto.model.Port;
import com.auto.model.InputData;
import com.auto.model.DataValue;
import com.auto.model.State;
import com.auto.model.User;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JFrame;
 
/**
 *
 * @author Chantal
 */
public class CmdSuggestor extends GeneralSuggestor {

    private static final Logger log = Logger.getLogger(PortSuggestor.class.getName());

    private final Port port;
    private final State state;
    public CmdSuggestor(JFrame frame, Port port, State state) {
        super(frame, "Command");
        this.port = port;
        this.state = state;
        super.createUI();
    }

    @Override
    protected Vector getWords() {
        Vector<String> words = new Vector<>();
        
        for (State  p : state.getNextStates()) {
            words.add(p.getName());
        }
       // p( "Got words: "+words);
       return words;
    }

    @Override
    public Object getValue() {
        String word = getSelected();
        p("State: word=" + word);
        return port.getState(word);
        //return port.getState(word);
    }

}
