/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.nlp;

import java.util.Date;
import java.util.List;
import org.ocpsoft.prettytime.nlp.PrettyTimeParser;

/**
 *
 * @author Chantal
 */
public class Main {

    
    public void Main() {
    }
    
    private void test(String msg) {
        PrettyTimeParser parser = new PrettyTimeParser ();
        List<Date> dates = parser.parse(msg);
        System.out.println(msg+": -> Dates are:"+dates);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.test("tomorrow at 5pm");
        main.test("how about tonight at 7");
        main.test("in two days at noon");
        
        
    }
    
}
