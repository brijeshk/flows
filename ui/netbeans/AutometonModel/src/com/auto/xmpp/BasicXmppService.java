/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.xmpp;

import com.auto.model.Atom;
import com.auto.model.AtomData;
import com.auto.model.Contact;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;
import com.auto.model.Group;
import com.auto.model.User;
import com.auto.net.AbstractChatService;
import com.auto.net.ChatConnector;
import com.auto.net.ChatServiceListener;
import com.auto.net.FlowChatListener;
import com.auto.utils.FileToolsIF;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.ConnectionException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.util.Base64;
import org.jivesoftware.smack.util.dns.HostAddress;
import org.jivesoftware.smackx.offline.OfflineMessageManager;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public class BasicXmppService implements ConnectionListener, MessageListener, ChatConnector {

    private static final Logger log = Logger.getLogger(BasicXmppService.class.getName());

    protected User me;
    private XMPPTCPConnection conn;
    static final String XMPP_HOSTNAME = "autometon.com";
    static final String XMPP_SERVICE = "localhost";
    static final int XMPP_PORT = 5222;
    private ChatManager chatManager;
    private Chat chat;
    protected Exception lasterror;
    private OfflineMessageManager offlineManager;
    private FileToolsIF filetools;

    private ArrayList<String> threadPool = new ArrayList<String>();
    private ChatServiceListener listener;

    public BasicXmppService(User user, FileToolsIF ft, ChatServiceListener conl) {
        this.me = FlowManager.getManager().getMe();
        this.listener = conl;
        filetools = ft;
    }

    private SSLContext getSecureSSLContext() throws Exception {
        p("getSecureSSLContext ");
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        String certBlob = filetools.getFileAsString("cacert.cer");
        InputStream caInput = new ByteArrayInputStream(certBlob.getBytes());
        Certificate ca;
        try {
            ca = cf.generateCertificate(caInput);
            X509Certificate x509ca = (X509Certificate) ca;
            p("ca=" + x509ca.getSubjectDN());
        } finally {
            caInput.close();
        }

        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        // Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        // Create an SSLContext that uses our TrustManager
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, tmf.getTrustManagers(), null);
        return context;
    }

    private void receiveFlowMessage(Message xmppmsg) {
        String type = xmppmsg.getType().name();
        String from = xmppmsg.getFrom();
        String body = xmppmsg.getBody();
        receiveFlowMessage(from, body);
    }

    public boolean checkConnection() {

        boolean ok = true;
        int tries = 0;
        while (conn == null || !conn.isConnected() && tries < 3) {
            p("Not connected yet, connecting");
            connect();
            ok = false;
            tries++;
        }
        return ok;
    }

    public boolean connect() {
        if (me == null) {
            err("Got no user me: " + me);

        }
        return connect(me.getJid(), me.getPassword());

    }

    public boolean connect(String jid, String pw) {

        if (conn != null && conn.isConnected()) {
            return true;
        }

        ConnectionConfiguration config = new ConnectionConfiguration(XMPP_HOSTNAME, 5222, XMPP_SERVICE);
        config.setSecurityMode(SecurityMode.required);
        config.setCompressionEnabled(false);
        try {
            config.setCustomSSLContext(getSecureSSLContext());
        } catch (Exception e) {
            p("Error setting xmpp server certificate context: " + e.getMessage());
            e.printStackTrace();
            return false;
        }

        //SASLAuthentication.supportSASLMechanism("TSL", 0);
        SASLAuthentication.supportSASLMechanism("PLAIN", 0);
        SASLAuthentication.getRegisterSASLMechanisms();

        if (conn == null) {
            conn = new XMPPTCPConnection(config);
            conn.setPacketReplyTimeout(3000);
        }
        try {

            if (!conn.isConnected()) {
                //  p("Connecting");
                conn.connect();
            }

            createChatManager();
            p("========= LOGIN " + jid + " " + pw);
            conn.login(jid, pw);
            setStatus(true);
            getOfflineMessages();

            conn.addPacketListener(new PacketListener() {
                @Override
                public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                    p("======== processPacket: Packet: from " + packet.getFrom() + ",getExtensions=" + packet.getExtensions());
                }

            }, null);
            // p("SUCCESS: Logged in as " + jid);
            return true;
        } catch (ConnectionException ex) {
            err("Could not connect to " + config.getHostAddresses() + ": error is:\n" + ex + ", failed hosts: " + ex.getFailedAddresses());
            if (ex.getFailedAddresses() != null) {
                for (HostAddress h : ex.getFailedAddresses()) {
                    err("Failed with error: " + h.getErrorMessage());
                }
            }

            log.log(Level.SEVERE, ex.getMessage(), ex);
            ex.printStackTrace();
            lasterror = ex;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void createChatManager() {
        p("=========== Creating chat manager, adding chat listener");
        chatManager = ChatManager.getInstanceFor(conn);
        conn.addConnectionListener(this);

        chatManager.addChatListener(
                new ChatManagerListener() {
                    @Override
                    public void chatCreated(Chat chat, boolean createdLocally) {
                        chat.addMessageListener(BasicXmppService.this);
                    }
                });
    }

    public void getOfflineMessages() {
        p("=============== getOfflineMessages ==============");
        offlineManager = new OfflineMessageManager(conn);
        int count = 0;
        try {
            count = offlineManager.getMessageCount();
        } catch (SmackException.NoResponseException ex) {
            Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMPPException.XMPPErrorException ex) {
            Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SmackException.NotConnectedException ex) {
            Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
        }
        boolean supports = false;
        try {
            supports = offlineManager.supportsFlexibleRetrieval();
        } catch (SmackException.NoResponseException ex) {
            Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XMPPException.XMPPErrorException ex) {
            Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SmackException.NotConnectedException ex) {
            Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
        }
        p("============== offlineManager.supportsFlexibleRetrieval(): " + supports);

        try {
            setStatus(false);

            if (count > 0) {
                p("Got " + count + " offline messages");
                List<Message> messages = offlineManager.getMessages();
                p("============ Number of offline messages:: " + offlineManager.getMessageCount());
                Map<String, ArrayList<Message>> offlineMsgs = new HashMap<String, ArrayList<Message>>();
                for (Message msg : messages) {
                    p("receive offline messages, the Received from [" + msg.getFrom()
                            + "] the message:" + msg.getBody());
                    String fromUser = msg.getFrom().split("/")[0];
                    this.processMessage(null, msg);
                }
            } else {
                p("got no offline messages");
            }
            setStatus(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void receiveFlowMessage(String from, String body) {

        int sl = from.indexOf("/");
        if (sl > 0) {
            from = from.substring(0, sl);
        }
        p("+++++++++++++++  receiveFlowMessage from " + from);
        // find user in manager
        p("Sender is: " + from);

        if (body.startsWith("MP##")) {
            body = body.substring(4);
            String[] fields = body.split("##", 2);
            String json = fields[0];                        
            List<Flow> flows = FlowData.fromJsonArray(json);
            listener.processFlowMessages(flows);

        } else {
            p("Received NON flow msg: " + body);
            processNonFlowMessage(from, body);
        }
        p("------------ end receiveFlowMessage");
    }

    public void processNonFlowMessage(String from, String msg) {
        FlowManager man = FlowManager.getManager();
        if (man == null) {
            return;
        }
        err("Got NON FLOW MESSAGE:" + msg);
    }

    public boolean checkAvailability(String jid) {
        return conn.getRoster().getPresence(jid).isAvailable();
    }

    public void disconnect() {
        if (conn != null && conn.isConnected()) {
            setStatus(false);
            try {
                conn.disconnect();
            } catch (SmackException.NotConnectedException ex) {
                Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void sendMessage(String message, String buddyJID) {
        connect();
        p("Sending message size " + message.length() + " to user " + buddyJID);
        boolean chatExists = false;
        Chat c = null;
        for (String tid : threadPool) {
            if ((c = chatManager.getThreadChat(tid)) != null) {
                if (c.getParticipant().equals(buddyJID)) {
                    if (checkAvailability(buddyJID)) {
                        chatExists = true;
                        break;
                    } else {
                        threadPool.remove(tid);
                        break;
                    }
                }
            }
        }
        try {
            if (chatExists) {
                try {
                    Chat chat = c;
                    chat.sendMessage(message);
                } catch (SmackException.NotConnectedException ex) {
                    Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {

                Chat chat = chatManager.createChat(buddyJID, this);
                threadPool.add(chat.getThreadID());
                try {
                    chat.sendMessage(message);
                } catch (SmackException.NotConnectedException ex) {
                    Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (Exception e) {
            p("Could not send message: " + e);
            e.printStackTrace();
        }
    }

    public void setStatus(boolean available) {
        if (available) {
            try {
                //conn.sendPacket(new Presence(Presence.Type.subscribe));
                conn.sendPacket(new Presence(Presence.Type.available));
            } catch (SmackException.NotConnectedException ex) {
                Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                conn.sendPacket(new Presence(Presence.Type.unavailable));
            } catch (SmackException.NotConnectedException ex) {
                Logger.getLogger(BasicXmppService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Exception getLastError() {
        return lasterror;
    }

    @Override
    public void processMessage(Chat chat, Message message) {
        p("+++++ processMessage " + chat + ":" + message);
        try {
            this.receiveFlowMessage(message);
        } catch (Exception e) {
            err("Could not process message: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void displayBuddyList() {
        Roster roster = conn.getRoster();
        if (roster != null) {
            Collection<RosterEntry> entries = roster.getEntries();

            p("Buddy list: " + entries.size() + " buddy(ies):");
            for (RosterEntry r : entries) {
                p(r.getUser());
            }
        } else {
            p("Got no roster");
        }
    }

    public Chat createChat(User target, MessageListener listener) {
        return createChat(target.getJid(), listener);
    }

    public Chat createChat(String tojid, MessageListener listener) {
        p("CreateChat to " + tojid);
        checkConnection();
        chat = chatManager.createChat(tojid, listener);
        return chat;
    }

    protected void err(String s, Exception ex) {
        log.warning(s + ":" + ex.getMessage());
    }

    protected void p(String s) {
        log.info(this.me.getEmail() + ": >>>>>>>>>>>>>  " + s);
    }

    protected void err(String s) {
        log.severe(s);
    }

    protected void warn(String s) {
        log.warning(s);
    }

    @Override
    public void connectionClosed() {
        // p("Got connection closed, trying to reconnt");
        //this.checkConnection();
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        p("Got connection connectionClosedOnError, trying to reconnt: " + e);
        // this.checkConnection();
    }

    @Override
    public void reconnectingIn(int i) {
        //  p("reconnectingIn " + i);
    }

    @Override
    public void reconnectionSuccessful() {
        //    p("reconnectionSuccessful");
    }

    @Override
    public void reconnectionFailed(Exception e) {
        p("reconnectionFailed: " + e);
    }

    @Override
    public void connected(XMPPConnection xmppc) {
        //  p("Connected: " + xmppc);
    }

    @Override
    public void authenticated(XMPPConnection xmppc) {
        //p("authenticated: " + xmppc);
    }

    @Override
    public boolean send(FlowData flow, List<AtomData> atoms) {
        flow.setModified(System.currentTimeMillis());
        flow.setAtomlist(atoms);
        String msg = flow.toJson("u");
        List<Contact> target = flow.extractUsers();
        List<User> users = User.converToUsers(target);
        for (User u : users) {
            this.sendMessage(msg, u.getJid());
        }
        return false;
    }

}
