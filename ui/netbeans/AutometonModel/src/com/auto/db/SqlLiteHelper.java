/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.db;

import java.util.List;

import com.auto.model.AtomData;
import com.auto.model.Flow;
import com.auto.model.FlowData;

/**
 *
 * @author Chantal
 */
public interface SqlLiteHelper {

    public void connect();

    public void close();

    public List<FlowData> getFlows();

    public List<AtomData> getAtoms();

    public void insert(AtomData ad);

    public void insert(FlowData fd);

    public void update(FlowData flowData);

    public void delete(Flow flow);

    public void update(AtomData atomData);

    public void delete(AtomData atomData);

}
