/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.db;

import java.util.HashMap;

/**
 *
 * @author Chantal
 */
public class DbMap {
    private HashMap<String, Object> map;
 
    public DbMap(HashMap<String, Object>  map) {
        this.map = map;
    }
    
    public String get(String key) {
        return (String) map.get(key);
    }
    public int getInt(String key) {
        Object o = map.get(key);
        if (o == null) return 0;
        else {
            if (o instanceof Long) return ((Long)o).intValue();
            else return ((Integer)o).intValue();
        }
    }
    public long getLong(String key) {
        Object o = map.get(key);
        if (o == null) return 0;
        if (o instanceof Integer) {
        	return ((Integer)o).longValue();
        }
        else return ((Long)o).longValue();
    }

	public boolean getBoolean(String key) {
		return getInt(key) >0;
	}
    
}
