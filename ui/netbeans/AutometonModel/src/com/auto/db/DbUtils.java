/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.db;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 *
 * @author Chantal
 */
public class DbUtils {

    private static final Logger log = Logger.getLogger(DbUtils.class.getName());

    public static void show(ResultSet rs) {
        try {
            ResultSetMetaData m = rs.getMetaData();
            int cols = m.getColumnCount();
            for (int c = 1; c <=cols; c++) {
                String name = m.getColumnName(c);
                String type = m.getColumnTypeName(c);
                String clazz = m.getColumnClassName(c);
                p("Got col " + c + ":" + name + ", " + type + ", " + clazz);
            }
        } catch (SQLException e) {            
            e.printStackTrace();
        }
    }

    /**
     * extract data from result set - to be db independent after that
     * @param rs ResultSet
     * @return DbMap, like a HashMap but simplifies getting data from it
     */
    public static DbMap extract(ResultSet rs) {
        HashMap<String, Object> map = new HashMap<String, Object>();     
        try {
            ResultSetMetaData m = rs.getMetaData();
            int cols = m.getColumnCount();
            for (int c = 1; c <= cols; c++) {
                String name = m.getColumnName(c);
                String type = m.getColumnTypeName(c);
                //String clazz = m.getColumnClassName(c);
                Object val = null;
                if (type.equalsIgnoreCase("string") || type.equalsIgnoreCase("text")) {
                    val = rs.getString(c);
                } else if (type.startsWith("int")) {
                    val = rs.getLong(c);
                } else if (type.equalsIgnoreCase("float")) {
                    val = rs.getFloat(c);
                }                
                else if (type.equalsIgnoreCase("timestamp")) {
                    long millis = rs.getLong(c);
                    val = new Date(millis);
                }  
                map.put(name, val);
            }
        } catch (SQLException e) {
            warn("Could not extract data from rs: "+e.getMessage());
        }
        return new DbMap(map);
    }

    public static void main(String args[]) {
        // test();
    }

    private static void p(String msg) {
        log.info(msg);
    }

    private static void warn(String msg) {
        log.warning(msg);
    }
}
