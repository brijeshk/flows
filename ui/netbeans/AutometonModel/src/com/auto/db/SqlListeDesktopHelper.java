/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.auto.model.Atom;
import com.auto.model.AtomData;
import com.auto.model.Flow;
import com.auto.model.FlowData;

/**
 *
 * @author Chantal
 */
public class SqlListeDesktopHelper implements SqlLiteHelper {

    private static final Logger log = Logger.getLogger(SqlListeDesktopHelper.class.getName());

    private Connection connection;
    private Statement statement;
    private String dbname;
    
    public SqlListeDesktopHelper(String dbname) {
        this.dbname = dbname;
        createTables();
    }

    @Override
    public void connect() {
        if (connection == null) {
            try {
                // load the sqlite-JDBC driver using the current class loader
                Class.forName("org.sqlite.JDBC");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        try {
            if (connection == null || connection.isClosed()) {
                // create a database connection
                connection = DriverManager.getConnection("jdbc:sqlite:"+dbname+".db");
            }

        } catch (SQLException e) {
            warn("Could not connect: " + e.getMessage());
        }
        try {
                statement = connection.createStatement();
         
        } catch (SQLException ex) {
            Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void insert(FlowData fd) {
        connect();
        update(fd.getInsertString());
    }

    public void insert(AtomData ad) {
        connect();
        update(ad.getInsertString());
    }

    private void update(String sql) {
        try {
            //p("update: " + sql);
            statement.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void createTables() {
        p("=== dropping and creatingTables ===");
        connect();

        update("drop table if exists flow");
        update("drop table if exists atom");
        update(FlowData.getCreateString());
        update(AtomData.getCreateString());

        for (FlowData f : FlowData.createTestData()) {
            update(f.getInsertString());
        }

        for (AtomData a : AtomData.createTestData()) {
            update(a.getInsertString());
        }
    }

    @Override
    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            warn("Close failed: " + e.getMessage());
        }
    }

    private static void p(String msg) {
        log.info(msg);
    }

    private static void warn(String msg) {
        log.warning(msg);
    }

    public static void test() {
        SqlListeDesktopHelper h = new SqlListeDesktopHelper("test");
        h.getFlows();
    }

    public static void main(String args[]) {
        test();
    }

    private ResultSet query(String sql) {
        try {
            p("query: " + sql);
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<FlowData> getFlows() {
        List<FlowData> flows = new ArrayList<FlowData>();
        connect();
        try {
            ResultSet rs = query("select * from flow");
            while (rs.next()) {
                FlowData fd = FlowData.create(DbUtils.extract(rs));
                flows.add(fd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return flows;
    }

    @Override
    public List<AtomData> getAtoms() {
        List<AtomData> atoms = new ArrayList<AtomData>();
        connect();
        try {
            ResultSet rs = query("select * from atom");
            while (rs.next()) {
                AtomData fd = AtomData.create(DbUtils.extract(rs));
                atoms.add(fd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return atoms;
    }

    public AtomData findAtom(int id)  {
        connect();
        try {
            ResultSet rs = query(AtomData.getFindById(id));
            while (rs.next()) {
                AtomData ad = AtomData.create(DbUtils.extract(rs));
                return ad;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public FlowData findFlow(int id)  {
        connect();
        try {
            ResultSet rs = query(FlowData.getFindById(id));
            while (rs.next()) {
                FlowData fd = FlowData.create(DbUtils.extract(rs));
                return fd;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public FlowData findFlowByUuid(String uuid)  {
        connect();
        try {
            ResultSet rs = query(FlowData.getFindByUuId(uuid));
            while (rs.next()) {
                FlowData fd = FlowData.create(DbUtils.extract(rs));
                return fd;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SqlListeDesktopHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public void update(FlowData flowData) {
       connect();
       this.update(flowData.getUpdateString());       
    }
    @Override
    public void delete(Flow flow) {
       connect();
       for (Atom a: flow.getAtoms()) {
            this.update(a.getAtomData().getDeleteString());
       }
       this.update(flow.getFlowData().getDeleteString());
    }

    @Override
    public void update(AtomData atomData) {
        connect();
       this.update(atomData.getUpdateString());
    }

    @Override
    public void delete(AtomData atomData) {
       connect();
       this.update(atomData.getDeleteString());
    }
}
