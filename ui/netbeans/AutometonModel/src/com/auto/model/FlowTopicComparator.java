package com.auto.model;

import java.util.Comparator;

public class FlowTopicComparator implements Comparator<Flow>{

	@Override
	public int compare(Flow  fa, Flow fb) {
	
		Port a = fa.getPort();
		Port b = fb.getPort();
		if (a == null || b == null) return 0;
		return a.getName().compareTo(b.getName());
	}

}
