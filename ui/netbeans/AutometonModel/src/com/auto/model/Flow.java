/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Chantal
 */
public class Flow implements Serializable {

    private static final Logger log = Logger.getLogger(Flow.class.getName());

    private FlowData flowdata;

    // derived data
    private String todo;
    private List<Contact> users;
    private List<Atom> atoms;
    private State state;
    private Port port;
    private User creator;
    private User assignee;
    private List<Label> labels;

    private transient final FlowManager man;

    public Flow(FlowData flowdata) {
        this.flowdata = flowdata;
        man = FlowManager.getManager();
        labels = new ArrayList<Label>();
        atoms = new ArrayList<Atom>();
        this.port = man.getPort(flowdata.getProtocol());
        this.creator = man.getUser(flowdata.getCreator());
        this.assignee = man.getUser(flowdata.getAssignee());
        users = flowdata.extractUsers();
        if (creator != null) {
            if (!users.contains(creator))users.add(creator);
        }
        if (assignee != null) {
            if (!users.contains(assignee))users.add(assignee);
        }

        
        this.setUsers(users);
        if (port != null) {
            if (flowdata.getState() != null) {
                State state = port.getState(flowdata.getState());
                if (state == null) {
                    warn("new Flow: --- COULD NOT FIND STATE " + flowdata.getState());
                }
                this.state = state;
            }
        } else {
            warn("new Flow : FOUND NO PORT: " + flowdata.getProtocol());
            p("Ports are: "+man.getAllPorts());
            Exception e = new Exception();
            e.printStackTrace();        	           
        }
    }
    
    public Atom findPrevAtomWithStates(List<State> states) {
        if (!hasAtoms() || states == null) {
            return null;
        }
        for (int i = atoms.size() - 1; i >= 0; i--) {
            Atom atom = atoms.get(i);
            for (State s : states) {
                if (atom.getState().equals(s)) {
                    return atom;
                }
            }
        }
        return null;
    }

    public boolean userIsCreator() {
        return man.getMe().equals(creator);
    }

    public List<Label> getLabels() {
        return labels;
    }

    public void add(Label l) {
        if (!labels.contains(l)) // add label to flow data? to atoms?
        {
            labels.add(l);
        }
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    public FlowData getFlowData() {
        return flowdata;
    }

    private void p(String s) {
       // log.info(s);
    }

    private void warn(String s) {
        log.warning(s);
    }

    public List<Atom> getAtoms() {
        return atoms;
    }

    public boolean hasAtoms() {
        return atoms != null && atoms.size() > 0;
    }

    public List<Contact> getUsers() {
        return users;
    }
    public FlowData converToFlowData() {
        List<AtomData> adlist = new ArrayList<AtomData>();
        if (this.hasAtoms()) {
            for (Atom a: atoms) {
                adlist.add(a.getAtomData());
            }
        }
        flowdata.setAtomlist(adlist);
        return flowdata;
    }

    public List<User> getTargetUsers(boolean includeMe) {
        List<User> target = new ArrayList<User>();
        for (Contact c : users) {
            if (c instanceof User) {
                User user = (User) c;
                if (includeMe || !man.isMe(user)) {
                    target.add(user);
                }
            } else {
                Group g = (Group) c;
                for (User u : g.getUsers()) {
                    if (!man.isMe(u)) {
                        target.add(u);
                    }
                }
            }
        }
        return target;
    }
    

    public String getOtherUsers() {
        StringBuffer buf = new StringBuffer();
        int nr = users.size();
        for (int i = 0; i < nr; i++) {
            Contact c = users.get(i);
            if (c instanceof User) {
                User u = (User) c;
                if (u != null && u.getName() != null
                        && !FlowManager.getManager().isMe(u)) {
                    buf = buf.append(u.getName());
                    if (i + 1 < nr) {
                        buf = buf.append(", ");
                    }
                }
            } else {
                buf = buf.append(c.getName());
                if (i + 1 < nr) {
                    buf = buf.append(", ");
                }
            }

        }
        String s = buf.toString().trim();
        if (s.endsWith(",")) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    public void addAtom(Atom atom) {
        if (!atoms.contains(atom)) {
            //p("Added atom " + atom);
            atoms.add(atom);
        } else {
       //     p("Flow already has this atom: " + atom + ", flow is: " + toString());
            return;
        }
        if (!users.contains(atom.getSender())) {
            users.add(atom.getSender());
        }
        List<User> rec = atom.getReceivers();
        if (rec != null) {
	        for (User u : rec) {
	            if (!users.contains(u)) {
	                users.add(u);
	            }
	        }
        }

    }

    @Override
    public String toString() {
        String d = flowdata.getData();
        if (d == null) {
            d = "";
        }
        if (d.length() > 50) {
            d = d.substring(0, 50) + "...";
        }
        String s = "Flow " + flowdata.getId() + ", gate=" + this.getPort()
                + ", creator=" + this.getCreator() + ", state=" + this.getState() + ", atoms=" + this.getAtoms();
        return s;
    }
    public boolean hasSubject() {
    	String s = flowdata.getSubject();
    	return (s != null && s.length()>0 && port != null && !s.equalsIgnoreCase(this.port.getName()));
    }
    public String getSubject() {
        return flowdata.getSubject();
    }

    public String getUuid() {
        return flowdata.getUuid();
    }

    /**
     * @return the state
     */
    public State getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(State s) {
        // check if this is not a say or  remind state!
        if (s == null) {
            return;
        }

        if (this.state == null || !s.isSoftState()) {
            this.state = s;
        }

        if (this.state != null) {
            this.flowdata.setState(this.state.getName());
        }
    }

    /**
     * @return the todo
     */
    public String getTodo() {
        return todo;
    }

    /**
     * @param todo the todo to set
     */
    public void setTodo(String todo) {
        this.todo = todo;
    }

    /**
     * @param users2 the users to set
     */
    public void setUsers(List<Contact> contacts) {
        this.users = contacts;
        if (contacts == null) {
            flowdata.setUsers(null);
        } else {
            String s = "[";
            for (int i = 0; i < contacts.size(); i++) {
                Contact contact = contacts.get(i);
                if (contact == null) {
                    continue;
                }
                if (contact instanceof User) {
                    User user = (User) contact;
                    s += q(user.getId());
                    if (i + 1 < contacts.size()) {
                        s += ",";
                    }
                } else {
                    Group g = (Group) contact;
                    List<User> gusers = g.getUsers();
                    for (User u : gusers) {
                        s += q(u.getId());
                        if (i + 1 < contacts.size()) {
                            s += ",";
                        }
                    }
                }
            }
            if (s.endsWith(",")) {
                s = s.substring(0, s.length() - 1);
            }
            s = s + "]";
            flowdata.setUsers(s);
        }
       // p("setUsers: userlist is now: " + flowdata.getUsers());
        // cut off last ,
    }

    private static String q(String s) {
        return "\"" + s + "\"";
    }
    /**
     * @return the port
     */
    public Port getPort() {
        return port;
    }

    /**
     * @return the creator
     */
    public User getCreator() {
        return creator;
    }

    /**
     * @return the assignee
     */
    public User getAssignee() {
        return assignee;
    }

    /**
     * @param assignee the assignee to set
     */
    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public int getId() {
        return flowdata.getId();
    }

    public Atom getAtom(long aid) {
        for (Atom atom : atoms) {
            if (atom.getAtomData().getAid() == aid) {
                return atom;
            }
        }
        return null;
    }

    public Atom getLastAtom() {
        if (!this.hasAtoms()) return null;
        Atom last = atoms.get(0);
        for (Atom atom : atoms) {
            if (atom.getCreated().after(last.getCreated())) {
            	last = atom;
            }
        }
        return last;
    }

    public Atom getFirstAtom() {
        // won't work with sorting of course... will we?
    	if (!this.hasAtoms()) return null;
    	Atom first = atoms.get(0);
    	for (Atom atom : atoms) {
             if (atom.getCreated().before(first.getCreated())) {
            	 first = atom;
             }
        }
         return first;
    }

    @Override
    public boolean equals(Object o) {
        Flow f = (Flow) o;
        return f.getId() == this.getId();
    }

    @Override
    public int hashCode() {
        return this.getId();
    }

    public void setPort(Port port) {
        this.port = port;
        if(port != null) this.flowdata.setProtocol(port.getId());
    }

    public void addFlag(String f) {
        flowdata.addFlag(f);
    }

    public void star() {
        flowdata.addFlag("star");
    }

    public boolean isStarred() {
        return flowdata.hasFlag("star");
    }

    public void unstar() {
        flowdata.removeFlag("star");

    }

    public int getNrAtoms() {
        if (atoms == null) {
            return 0;
        } else {
            return atoms.size();
        }
    }

    public int getNrUnread() {
        if (!this.hasAtoms()) {
            return 0;
        }
        int nr = 0;
        for (Atom a : atoms) {
            if (a.isUnread()) {
                nr++;
            }
        }
        return nr;
    }

    public boolean isTodo() {
        //p("Checking isTodo for " + this);
        // no atoms, so ...?
        if (!this.hasAtoms()) {
            return true;
        }

        if (man.isMe(this.getLastAtom().getSender())) {
            //p("isTodo: I was the last sender, not todo");
            return false;
        } else {
			// the last person was not me. so this *might* be a todo item
            // but this also depends on the state... how exactly?
         //   p("isTodo: I was NOT the last sender, *might* be todo");
            if (this.getState().hasStates()) {
              //  p("isTodo: " + this.getState().getName() + " has next states");
                if (this.getUsers().size() > 2) {
                    // several users, it is always todo
               //     p("isTodo: several users, and it has next states -> TODO");
                    return true;
                } else {
              //      p("isTodo: 2 users, and it has next states -> TODO");
                    return true;
                }
            } // no outgoing edges
            else {
              //  p("isTodo: NO next next states, not todo");
                return false;
            }
        }
    }

    public String toJson(List<Atom> atoms, String crud) {
        String s = "{\n" + flowdata.toJson(crud);
        if (atoms != null) {
            s += "atoms: [\n";
            for (int i = 0; i < atoms.size(); i++) {
                Atom a = atoms.get(i);
                s += a.getAtomData().toJson(this.getFlowData());
                if (i + 1 < atoms.size()) {
                    s += ",\n";
                }
            }

            s += "]\n";
        }
        s += "\n}";
        return s;
    }

    public static String toJsonArray(List<Flow> flows, String crud) {
        String s = "{[";
        if (flows != null) {
            for (int i = 0; i < flows.size(); i++) {
                Flow f = flows.get(i);
                s += f.toJson(crud);
                if (i + 1 < flows.size()) {
                    s += ",\n";
                }
            }
        }
        s += "\n]}";
        return s;
    }

    public String toJson(String crud) {
    	
    	
        String s = "{\n" + flowdata.toJson(crud, false);
        // add atoms
        if (hasAtoms()) {
            s += flowdata.key("atoms")+ "[\n";
            for (int i = 0; i < atoms.size(); i++) {
                Atom a = atoms.get(i);
                s += a.getAtomData().toJson(flowdata);
                if (i + 1 < atoms.size()) {
                    s += ",\n";
                }
            }
            s += "]\n";
        }
        s += "\n}";
        return s;
    }

    public boolean isWaiting() {
      //  p("Checking isWaiting for " + this);
        // no atoms, so ...?
        if (!this.hasAtoms()) {
            return false;
        }
        if (!man.isMe(this.getLastAtom().getSender())) {
       //     p("isWaiting: I am NOT the last sender, so I am NOT waiting");
            return false;
        } else {
       //     p("isWaiting: I am the last sender, so I *might* be waiting");
			// I was the last person to send something. 
            // but if this qualifies as waiting depends also on the state
            // how exactly?
            if (this.getState().hasStates()) {
       //         p("isWaiting: " + this.getState().getName() + " has next states -> waiting");
                // there are outgoing states
                return true;
            } // no outgoing edges
            else {
         //       p("isWaiting: " + this.getState().getName() + " has NO next states -> NOT waiting (really?)");
                return false;
            }
        }
    }

    public void setModified(long m) {
        flowdata.setModified(m);

    }

   
    public Atom findAtom(long aid) {
        if (aid<=0 || atoms == null) {
            return null;
        }
        for (Atom a : atoms) {
            if (a.getAtomData().getAid()== aid) {
                return a;
            }
        }
        return null;
    }

	public void setCrud(String crud) {
		flowdata.setCrud(crud);
		
	}

	public String getCrud() {
		return flowdata.getCrud();
	}

	public void setSubject(String subject) {
		flowdata.setSubject(subject);
		
	}
}
