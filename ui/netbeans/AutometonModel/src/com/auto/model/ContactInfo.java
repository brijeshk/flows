package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ContactInfo implements Serializable{
	private static final Logger log = Logger.getLogger(Contact.class.getName());

	private String userid;
	private User contact;
	private List<String> tags;
	private static FlowManager man = FlowManager.getManager();
	private boolean sharePorts;
	private static String POSSIBLE_TAGS[] = {"favorite", "block", "friend"};
	
	public static String[] getPossibleTags() {
		return POSSIBLE_TAGS;
	}
	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public boolean isSharePorts() {
		return sharePorts;
	}

	public void setSharePorts(boolean sharePorts) {
		this.sharePorts = sharePorts;
	}

	public ContactInfo(String userid) {
		this.userid = userid;
		contact = man.getUser(userid); 
		tags = new ArrayList<String>();
	}
	
	public void addTag(String tag) {
		if (!tags.contains(tag))tags.add(tag);
	}
	public void removeTag(String tag) {
		tags.remove(tag);
	}
	public User getContact() {
		return contact;
	}
	public String getId() {
		return userid;
	}
	public boolean hasTag(String tag) {
		return tags.contains(tag);
	}
	
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o instanceof String) {
			return ((String)o).equals(userid);
		}
		else if (o instanceof ContactInfo) {
			return ((ContactInfo)o).userid.equals(userid);
		}
		else return false;
	}
	public int hashCode() {
		return userid.hashCode();
	}
	public String toString() {
		return toJson();
	}
	

	public String toJson() {
		String j = "";
		String TAB="       ";
		j += TAB+key(this.getId()) + "{\n" ;
		j += TAB+key("acceptPorts") + ""+this.sharePorts;
		if (tags != null && tags.size()>0) {
			j += ",\n"+TAB;
			j += key("tags") + "[" ;
			for (int i = 0; i < tags.size(); i++) {
				j += q(tags.get(i));
				if (i + 1  < tags.size()) j +=",";
			}
			j += "]\n";
		}
		
		j += "    \n}";
	 //   p("ContactInfo json: " + j);
	    return j;
	}
	 /**
     * turn String into a json key with quotes and column :
     */
    private String key(String s) {
        return q(s) + ": ";
    }

    /**
     * quote string for json
     */
    private String q(String s) {
        return "\"" + s + "\"";
    }
    protected static void p(String msg) {
        log.info(msg);
    }
}
