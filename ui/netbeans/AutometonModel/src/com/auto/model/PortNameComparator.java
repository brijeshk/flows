package com.auto.model;

import java.util.Comparator;

public class PortNameComparator implements Comparator<Port>{

	@Override
	public int compare(Port  fa, Port fb) {
		
		String a = fa.getName();
		String b = fb.getName();
		return a.compareTo(b);
	}

}
