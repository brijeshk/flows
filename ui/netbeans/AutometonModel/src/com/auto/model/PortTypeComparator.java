package com.auto.model;

import java.util.Comparator;

public class PortTypeComparator implements Comparator<Port>{

	@Override
	public int compare(Port  fa, Port fb) {
		
		String a = fa.getType();
		String b = fb.getType();
		if (a.equals(b)) {
			return fa.getName().compareTo(fb.getName());
		}
		else {
			return 1000*a.compareTo(b);
		}
	}

}
