package com.auto.model;

import java.util.Comparator;

public class FlowDateComparator implements Comparator<Flow>{

	@Override
	public int compare(Flow  fa, Flow fb) {
		// seconds is good enough :-)
		// we want the larger one first, which is the most recent
		Atom a = fa.getAtoms().get(0);
		Atom b = fb.getAtoms().get(0);
		return (int)((a.getAtomData().getCreated() - b.getAtomData().getCreated())/1000);
	}

}
