package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/** a group or a user */	
public class Contact implements Serializable {


    private static final Logger log = Logger.getLogger(Contact.class.getName());

    protected String name;
	protected String type;
	protected List<Label> labels;
	protected List<Port> ports;
	protected HashMap<String, ArrayList<ContactInfo>> contactidmap;
	protected ArrayList<ContactInfo> contactinfos;
	protected String id;
	
	public Contact() {
		ports = new ArrayList<Port>();
        contactinfos = new ArrayList<ContactInfo>();
	}

	
	public List<Port> getPorts() {
        return ports;
    }
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	public List<Port> getAllPossiblePorts() {
       ArrayList<Port> res = new ArrayList<Port>();
       if (ports != null) res.addAll(ports);
       if (canUserMyPorts()) {
    	   p("I can use my ports too");
    	   List<Port> mine =FlowManager.getManager().getMe().getPorts(); 
    	   for (Port p: mine) {
    		   if (!res.contains(p)) res.add(p);
    	   }
       }
       return res;
    }
	public ContactInfo getInfoOn(Contact user) {
		 for (ContactInfo ci: contactinfos) {
             if (ci.getId().equals(user.getId())) {
                 return ci;
             }
         }
		 return null;
	}
	

	public boolean canUserMyPorts() {
           for (ContactInfo ci: contactinfos) {
               if (ci.getId().equals(FlowManager.getManager().getMe().getId())) {
                   return ci.isSharePorts();
               }
           }
           p("Could not find contact info in user "+this+":"+contactinfos);
           return false;
       }
	public List<Port> getVisiblePorts(User to) {
		ArrayList<Port> vis = new ArrayList<Port> ();
		for (Port p : ports){ 
			if (p.isVisibleTo(to)) {
				vis.add(p);
			}
		}
		
		return vis;
    }
	protected ContactInfo getInfo(String id, List<ContactInfo> list) {
		if (list == null) return null;
		for (ContactInfo ci: list) {
			if (ci.getId().equals(id)) return ci;
		}
		return null;
	}
	public List<Label> getLabels() {
        return labels;
    }
	 public void add(Label l) {
	        if (!labels.contains(l)) labels.add(l);
	    }

	    public void setLabels(List<Label> labels) {
	        this.labels = labels;
	    }
    public void add(Port port) {
        ports.add(port);
    }

    public void setPorts(List<Port> ports) {
        this.ports = ports;
    }

    public void addPort(Port p) {
        if (ports == null) {
            ports = new ArrayList<Port>();
        }
        ports.add(p);
    }

	 public Port getPort(int id) {
	        return getPort(id, false);
	    }
	    public Port getFirstPort() {
	        return ports.get(0);
	    }
	    public Port getLastPort() {
	        return ports.get(ports.size()-1);
	    }
	    public Port getPort(int id, boolean warnIfNotFound) {
	    	
	        for (Port p : ports) {
	            if (p.getId() == id) {
	                return p;
	            }
	        }
	        if (warnIfNotFound) {
	            warn("Found no port with id " + id+" for contact "+toString()+". Got ports: ");
	            for (Port p : ports) {
	                p(p.getId()+": "+p.getName()+"; ");
	            }
	        }
	        return null;
	    }

	    public Port getPort(String name) {
	        for (Port p : ports) {
	            if (p.getName().equalsIgnoreCase(name)) {
	                return p;
	            }
	        }
	        warn("Found no port with name " + name);

	        return null;
	    }
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	

    @Override
    public String toString() {
        return name;
    }
	

    protected static void p(String msg) {
        log.info(msg);
    }

    protected static void warn(String msg) {
        log.warning(msg);
    }


	public boolean hasPorts() {
		return ports != null && ports.size()>0;
	}

}
