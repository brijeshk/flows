/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public class User extends Contact implements Serializable {

	
	private String email;
	private String phone;
	private String message_transport_type;
       
	public static final String DEFAULT_GROUP = "all";
	private String password;
	// {"_id": "ramya@localhost", "email": "ramya@localhost",
	// "message_transport_type": "xmpp", "own_protocols": [22, 23, 24]}

	// set after login
	private String auth;

	private boolean blacklisted;
	private boolean friend;

	private String type;

	public User(String id) {
		super();
		this.id = id;
		this.type = "People";
		this.password = id;
		if (id.startsWith("system") || id.startsWith("movies")
				|| id.startsWith("stock") || id.startsWith("ebay")
				|| id.startsWith("weather") || id.startsWith("flights")) {
			type = "Bots";
		} else if (id.startsWith("template")) {
			type = "Templates";
		}
		if (id.indexOf("@") > 0) {
			setEmail(id);
		} 
	}

	public static List<User> converToUsers(List<Contact> users) {
		List<User> target = new ArrayList<User>();

		for (Contact c : users) {
			if (c instanceof User) {
				User user = (User) c;
				target.add(user);

			} else {
				Group g = (Group) c;
				for (User u : g.getUsers()) {
					target.add(u);

				}
			}
		}
		return target;
	}

     
	public void removeUser(User user) {
		removeUserFromGroup(DEFAULT_GROUP, user.getId());
	}

	public void removeUserFromGroup(String groupname, String id) {
		ArrayList<ContactInfo> list = contactidmap.get(groupname);
		if (list == null) {
			list = new ArrayList<ContactInfo>();
			contactidmap.put(groupname, list);
		}
		ContactInfo ci = getInfo(id, list);
		if (ci != null) {
                    list.remove(ci);
                    contactinfos.remove(ci);
                }
	}

	public void addUser(User user) {
		addUserToGroup(DEFAULT_GROUP, user.getId());
	}

	public void addUserToGroup(String groupname, User user) {
		addUserToGroup(groupname, user.getId());
	}


	public void addUserToGroup(String groupname, String id) {
		ArrayList<ContactInfo> list = contactidmap.get(groupname);
		if (list == null) {
			list = new ArrayList<ContactInfo>();
			contactidmap.put(groupname, list);
		}              
		ContactInfo ci = getInfo(id, list);
		if (ci == null) ci = new ContactInfo(id);
                if (!contactinfos.contains(ci)) {
                    contactinfos.add(ci);
                }
		if (!list.contains(ci)) {
			list.add(ci);
		}
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isBlacklisted() {
		return blacklisted;
	}

	public void setBlacklisted(boolean blacklisted) {
		this.blacklisted = blacklisted;
	}

	public boolean isFriend() {
		return friend;
	}

	public void setFriend(boolean friend) {
		this.friend = friend;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isUser() {
		return type.equalsIgnoreCase("People");
	}

	public boolean isSystem() {
		return type.equalsIgnoreCase("Bots") || type.equalsIgnoreCase("Bot");
	}

	public boolean isTemplates() {
		return type.equalsIgnoreCase("Templates");
	}

	public User(String name, String email) {
		this(name);
		setEmail(email);
	}

	public String getJid() {
		// String jid = email.replace("@", "_");
		// jid = jid.replace(".", "_");
		// p(email+"-> jid: "+jid);
		return email;
		// return jid;
	}

	public String getPassword() {
		return password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String p) {
		phone = p;
	}

	@Override
	public String toString() {
		return name;
	}

	public static List<User> fromJsonArray(String json) {
		// p("Parsing to user:" + json);
		ArrayList<User> users = new ArrayList<User>();
		try {
			JSONArray ja = new JSONArray(json);
			for (int i = 0; i < ja.length(); i++) {
				JSONObject jo = ja.getJSONObject(i);
				User user = fromJsonObj(jo);
				if (user != null) {
					users.add(user);
					// p("Got user "+user.getName()+", emil="+user.getEmail()+", user.id="+user.getId());
				} else {
					p("Got NO user for json " + json);
				}
			}
		} catch (JSONException ex) {
			warn("Could not create user from " + json + ":" + ex.getMessage());
			return null;
		}
		return users;
	}

	public void setEmail(String e) {

		this.email = e;
		// pw is first part by default
		int at = email.indexOf("@");
		if (name == null) name = email;
		if (name != null && at > -1) {
			name = name.substring(0, at);           
		}
		if (password == null) {
			password = name;
		}
		// p("Email for "+getName()+" is: "+email);
	}

	public static User fromJsonObj(JSONObject jo) {
		String id;
		String email = "unknown";
		String phone = "";
		boolean bot = false;
		String message_transport_type = "unknown";
                ArrayList<ContactInfo> infos = new ArrayList<ContactInfo>();
		try {
			id = null;
			if (jo.has("name")) {
				id = jo.getString("name");
			} else if (jo.has("_id")) {
				id = jo.getString("_id");
			}

			if (jo.has("email")) {
				email = jo.getString("email");                
			}

			if (jo.has("message_transport_type")) {
				message_transport_type = jo.getString("message_transport_type");
			}

			if (jo.has("phone")) {
				phone = jo.getString("phone");
			}

			if (jo.has("bot")) {
				bot = jo.getBoolean("bot");
			}

			HashMap<String, ArrayList<ContactInfo>> contactidmap = new HashMap<String, ArrayList<ContactInfo>>();

			if (jo.has("contacts")
					&& !jo.get("contacts").toString().equalsIgnoreCase("null")) {

				JSONObject jcontacts = jo.getJSONObject("contacts");
				
				if (jcontacts != null) {
					Iterator<String> ids = jcontacts.keys();
					for (; ids.hasNext(); ) {
						String cid = ids.next();
						Object ocontact = jcontacts.get(cid);
						ContactInfo info = null;;

						//p("Processing contactinfo: "+cid+":"+ocontact);
						if (ocontact != null && !ocontact.toString().equals("null") && ocontact instanceof JSONObject) {
							JSONObject jc = (JSONObject)ocontact;
							Iterator<String> kit = jc.keys();
							ArrayList<String> tags = new ArrayList<String>();
							boolean share = false;
							for (;kit.hasNext();) {
								String key = kit.next();
								if (key.equalsIgnoreCase("tags")) {
									Object ot = jc.get("tags");
									if (ot != null && !ot.toString().equals("null") && ot instanceof JSONArray) {                        				  
										p("Got tags: "+ot);
										JSONArray at = (JSONArray)ot;
										for (int t = 0; t < at.length(); t++) {
											String tag = at.getString(t);
											if (tags != null && !tags.equals("null") && !tags.contains(tag)) {
												tags.add(tag);
											}
										}
									}
								}
								else if (key.equalsIgnoreCase("acceptPorts")) {
									share = jc.getBoolean(key);
								//	p("share is: "+share);
								}
								else {
									warn("Unknown key for contact: "+key);									

								}
							}
							info = new ContactInfo(cid);
							info.setSharePorts(share);
							if (tags != null && tags.size()>0) {
								for (String tag: tags) {
									info.addTag(tag);
									p("Found tag: "+tag);
								}
							}
						}

						if (info != null) infos.add(info);
					}
				}
				contactidmap.put("all", infos);


			}
			// p("fromJsonObj: id=name="+id+", email="+email);
			User user = new User(id);
			user.setName(id);
			user.contactidmap = contactidmap;
                        for (ContactInfo ci: infos) {
                            user.contactinfos.add(ci);
                        }
			if (bot) {
				user.setType("bot");
			} else {
				user.setType("user");
			}
			user.setPhone(phone);
			if (!email.equalsIgnoreCase("unknown")) {
				user.setEmail(email);
			} else {
				user.setEmail(id);
			}

			user.message_transport_type = message_transport_type;
			return user;
		} catch (Exception ex) {
			warn("Could not create user from " + jo + ":" + ex.getMessage());
			ex.printStackTrace();
			return null;
		}

	}

	public static User fromJson(String json) {
		User user = null;
		try {
			JSONObject jo = new JSONObject(json);
			user = fromJsonObj(jo);
		} catch (JSONException ex) {
			warn("Could not create user from " + json + ":" + ex.getMessage());
			return null;
		}
		// p("Got user: " + user.toString());
		return user;
	}

	public static List<User> test() {
		String json = "[{'_id': 'ramya', 'email': 'ramya@localhost', 'message_transport_type': 'xmpp'},{'_id': 'brijesh', 'email': 'brijesh@localhost', 'message_transport_type': 'xmpp'},{'_id': 'manasi'},{'_id': 'nikhil'},{'_id': 'prateek', 'email': 'prateek@localhost', 'message_transport_type': 'xmpp'},{'_id': 'amitabh', 'email': 'amitabh@localhost', 'message_transport_type': 'xmpp'},{'_id': 'ebay'},{'_id': 'movies'},{'_id': 'weather'},{'_id': 'brijeshsk@gmail.com', 'email': 'brijeshsk@gmail.com', 'message_transport_type': 'xmpp'},{'_id': 'rambri@gmail.com', 'email': 'rambri@gmail.com', 'message_transport_type': 'xmpp'},{'_id': 'prat.kr@gmail.com', 'email': 'prat.kr@gmail.com', 'message_transport_type': 'xmpp'},{'_id': 'prateek.kr@gmail.com', 'email': 'prateek.kr@gmail.com', 'message_transport_type': 'xmpp'},{'_id': 'chantal', 'email': 'chantal', 'message_transport_type': 'xmpp'},{'_id': 'chantal@gmail.com', 'email': 'chantal@gmail.com', 'message_transport_type': 'xmpp'}]";
		return fromJsonArray(json);
	}

	public static void main(String args[]) {
		test();
	}

	
	/**
	 * @return the email
	 */
	public String getEmail() {       
		return email;
	}

	/**
	 * @return the message_transport_type
	 */
	public String getMessage_transport_type() {
		return message_transport_type;
	}

	public String getAuth() {
		return auth;
	}

	/**
	 * @param auth the auth to set
	 */
	public void setAuth(String auth) {
		this.auth = auth;
	}

	public void setPassword(String pw) {
		this.password = pw;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof User)) return false;
		User u = (User) o;
		if (id != null && u.getId() != null) {
			return id.equals(u.getId());
		} else if (email != null && u.getEmail() != null) {
			return email.equalsIgnoreCase(u.getEmail());
		} else if (phone != null && u.getPhone() != null) {
			return phone.equals(u.getPhone());
		} else if (name != null && u.getName() != null) {
			return phone.equalsIgnoreCase(u.getName());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		if (id != null) {
			return id.hashCode();
		} else if (email != null) {
			return email.toLowerCase().hashCode();
		} else if (phone != null) {
			return phone.hashCode();
		} else {
			return name.toLowerCase().hashCode();
		}
	}

	/*
	 * { "id":"chantal", "name":"chantal", "password":"chantal", "email":"",
	 * "phone":"", "bot":false, "contacts":{ "Friends" : [ userid1, userid2...]
	 * , "Business" : [ userid3, userid4] ... }, "message_transport_type":"" },
	 */
	public String toJson() {
		String j = "{\n";
		String sep = ",\n";
		j += key("id") + q(this.getId()) + sep;
		j += key("name") + q(this.getName()) + sep;
		j += key("password") + q(this.getPassword()) + sep;
		j += key("phone") + q(this.getPhone()) + sep;
		j += key("bot")
				+ q(new Boolean(this.isSystem()).toString().toLowerCase())
				+ sep;
		j += key("message_transport_type")
				+ q(this.getMessage_transport_type()) + sep;
		// TODO: contacts
		if (contactidmap != null) {
			j += key("contacts");
			boolean firstone = true;
			for (Iterator<String> it = contactidmap.keySet().iterator(); it
					.hasNext();) {
				String groupname = it.next();
				if (firstone) {
					j += " { ";
					firstone = false;
					}
				ArrayList<ContactInfo> contactids = contactidmap.get(groupname);
				if (contactids != null) {
					j += "{\n";
					for (ContactInfo cid : contactids) {

						j = j + "\n     "+cid.toJson()+",";
					}
					if (j.endsWith(",")) {
						j = j.substring(0, j.length() - 1);
					}
					j += "\n}";
				} else {
					j += "{},";
				}
				if (j.endsWith(",")) {
					j = j.substring(0, j.length() - 1);
				}
			}

		}
		j += "\n}";
		p("User json: " + j);
		return j;
	}

	/**
	 * turn String into a json key with quotes and column :
	 */
	private String key(String s) {
		return q(s) + ": ";
	}

	/**
	 * quote string for json
	 */
	private String q(String s) {
		return "\"" + s + "\"";
	}

	/**
	 * add json {} parenthesis
	 */
	private String par(String s) {
		return "{ " + s + "} ";
	}

	/**
	 * add json [] parenthesis
	 */
	private String l(String s) {
		return "[ " + s + "]";
	}

	public boolean hasEmail() {
		return email != null && email.contains("@") && email.length() > 4;
	}

	public boolean hasPhone() {
		return phone != null && phone.length() > 5;
	}

}
