/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public class InputData implements Serializable {

    private static final Logger log = Logger.getLogger(InputData.class
            .getName());

    private String name;

    private List<InputData> subdatalist;
    private DataType dataType;
    private InputAction inputAction;
    /* if the source is from user or the name of the state this is from */
    private String source;
    private String description;
    private List<DataValue> values;
    private String jsonvalue;
    private InputData nextData;
    // pointer to state
    private State state;

    public InputData(String name) {
        this.name = name;
        setDataType(DataType.Text);
        values = new ArrayList<DataValue>();
    }

    public static InputData createError(String msg) {
        InputData data = new InputData("Error");
        DataValue v = new DataValue(data.name, msg);
        v.setValue(msg);
        data.add(v);
        return data;
    }

    public void setValues(List<DataValue> values) {
        this.values = values;
    }

    public List<InputData> getInputDatas() {
        return subdatalist;
    }

    public boolean isSourceUser() {
        return source == null || source.equalsIgnoreCase("user")
                || source.trim().length() < 1;
    }

    public DataType getType() {
        return dataType;
    }

    public State getState() {
        return state;
    }

    /**
     * compute the the data input for this particular state and the list of
     * atoms. if the data is of type
     *
     * @param atoms
     * @return InputData object either based on atom data or else basedon the
     * state data
     */
    public InputData computeInputDataForAtoms(List<Atom> atoms) {
        // to do: if getInputSource().isLastStateData()...
        // sort last to first, or else, sort by state?

        if (!isSourceUser()) {
            Collections.sort(atoms, new AtomDateComparator());
            for (int a = atoms.size() - 1; a >= 0; a--) {
                Atom atom = atoms.get(a);
                State astate = atom.getState();
                if (astate.getName().equalsIgnoreCase(source)) {
                    // check if the source
                    InputData prevdata = astate.getDataOfType(dataType);
                    if (prevdata != null && prevdata.hasValues()) {
                        InputData res = prevdata.clone();
                        String jsondata = atom.getData();
                        res.values = prevdata.fromJson(jsondata);
                        p("Found previous data in source " + source + ":" + res
                                + ", json data in atom was: " + jsondata);
                        return res;
                    }
                }
            }
            warn("========== Source is "
                    + source
                    + ", not user input, but could NOT find data in previous atoms: "
                    + atoms);
        }
        // if we end up here, we found no previous data
        // so let's see if the data itself is there
        if (this.hasValues()) {
            return this.clone();
        }
        // there was no state data. So get user input -> return this as well ;

        return this.clone();
    }

    public List<DataValue> fromJson(String jsondata) {
        return fromJson(this, jsondata);
    }

    public static List<DataValue> fromJson(InputData in, String jsondata) {
        if (!jsondata.startsWith("[")) {
            jsondata = "[" + jsondata + "]";
        }
        JSONArray jvalueobject = null;
        try {
            jvalueobject = new JSONArray(jsondata);
        } catch (JSONException ex) {
            Logger.getLogger(InputData.class.getName()).log(Level.SEVERE, null,
                    ex);
        }
        List<DataValue> res = DataValue.fromJsonArray(jvalueobject, in);
        return res;

    }

    /**
     * Create a copy of this input data, so that the original value is not
     * overwritten
     */
    @Override
    public InputData clone() {
        InputData copy = new InputData(this.name);
        copy.dataType = this.dataType;
        copy.description = this.description;
        copy.inputAction = this.inputAction;
        copy.nextData = this.nextData;
        copy.state = this.state;
        copy.source = this.source;

        if (subdatalist != null) {
            copy.subdatalist = new ArrayList<InputData>();
            for (InputData id : this.subdatalist) {
                copy.subdatalist.add(id.clone());
            }
        }
        if (this.values != null) {

            for (DataValue val : this.values) {
                DataValue vclone = val.clone();
                copy.values.add(vclone);
            }
        }
        return copy;

    }

    public static List<InputData> fromJsonArray(String json, State state) {
        if (json == null || json.equals("") || json.equals("{}") || json.equals("[]")) {
            return null;
        }
        try {
            ArrayList<InputData> datas = new ArrayList<InputData>();
            if (json.startsWith("[")) {
                JSONArray ja = new JSONArray(json);
                p("JSon array has length " + ja.length());
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = ja.getJSONObject(i);
                    Iterator<String> it = jo.keys();

                    String dataname = it.next();
                    p("Got data name: " + dataname);
                    InputData data = fromJsonObj(jo, dataname);
                    data.state = state;
                    datas.add(data);
                    if (datas.size() > 1) {
                        InputData last = datas.get(datas.size() - 2);
                        last.setNextData(data);
                    }

                }
            } else if (json.startsWith("ERROR") || json.startsWith("\"ERROR")) {
                int col = json.indexOf(":");
                String msg = json.substring(col + 1).trim();
                InputData data = InputData.createError(msg);
                data.state = state;
                datas.add(data);
            } else {
                // { a: {}} etc
                try {
                    JSONObject jomap = new JSONObject(json);
                    Iterator<String> keyit = jomap.keys();
                    while (keyit.hasNext()) {
                        String dataname = keyit.next();
                        p("Got data name: " + dataname);
                        InputData data = fromJsonObj(jomap, dataname);
                        data.state = state;
                        datas.add(data);
                        if (datas.size() > 1) {
                            InputData last = datas.get(datas.size() - 2);
                            last.setNextData(data);
                        }
                    }
                } catch (JSONException e1) {
                    p("Could not create json object from: " + json + ":"
                            + e1.getMessage());
                    e1.printStackTrace();
                }
            }
            p("=============================== ALL DATA FROM JSON\n" + datas);
            return datas;
        } catch (JSONException e) {
            p("Could not create json object from: " + json + ":"
                    + e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public static List<InputData> fromJsonArray(JSONObject ct, State state) {
        ArrayList<InputData> datas = new ArrayList<InputData>();
        Iterator<String> it = ct.keys();
        //   p("============= PARSING DATA: " + ct);
        InputData last = null;
        for (; it.hasNext();) {
            String dataname = it.next();
            //      p("Got data name: " + dataname);
            InputData data = fromJsonObj(ct, dataname);
            data.state = state;

            if (data != null && !datas.contains(data)) {
                datas.add(data);
                if (last != null) {
                    last.setNextData(data);
                }
                last = data;
            }

        }
        return datas;
    }

    /* What to ask the user */
    public String getQuestionForInput() {
        return this.getName();
    }

    private void add(DataValue value) {
        values.add(value);
    }

    public List<DataValue> getValues() {
        return values;
    }

    public DataValue getValue() {
        if (values == null || values.size() < 1) {
        	DataValue v = new DataValue(getName());
        	values = new ArrayList<DataValue>();
        	values.add(v);
            return v;
        } else {
            return values.get(0);
        }
    }

    /*
     * "data":{ "Choose Work":{ "type":3, "desc":"", "value":[
     * "YardWork.Landscaping", "YardWork.Paving", "Maintenance.Recurring" ]
     * "To":{ "type":1, "desc":"", "value":null }
     */
    public InputAction getInputAction() {
        return inputAction;
    }

    public boolean isInfo() {
        return inputAction == InputAction.INFO;
    }

    public boolean isHidden() {
        return inputAction == InputAction.INFO;
    }

    public boolean isOptional() {
        return inputAction == InputAction.OPTIONAL;
    }

    public boolean isRequired() {
        return inputAction == InputAction.REQUIRED || isSelect1()
                || isSelectN();
    }

    public boolean isSelect1() {
        return inputAction == InputAction.SELECT1;
    }

    public boolean isSelectN() {
        return inputAction == InputAction.SELECTN;
    }

    public static InputData fromJsonObj(JSONObject ct, String dataname) {
        InputData data = new InputData(dataname);
        p("========== creating InputData " + dataname + " for json: \n" + ct);
        try {
            JSONObject jinputdata = ct.getJSONObject(dataname);
            // p("========== jinputdata "+jinputdata);
            if (jinputdata.has("type")) {
                String val = jinputdata.getString("type");
                data.setDataType(DataType.getType(val));
                p("Got type :" + data.getDataType());
            }

            if (jinputdata.has("source")) {
                String val = jinputdata.getString("source");
                data.setSource(val);
                // p("Got source :"+val);
            }
            if (jinputdata.has("inputAction")) {
                String val = jinputdata.getString("inputAction");
                data.inputAction = InputAction.find(val);
                // p("Got inputAction :"+data.getInputAction());
            }
            if (jinputdata.has("value")) {
                data.jsonvalue = "" + jinputdata.get("value");
                // p("Got value: "+data.jsonvalue);
                if (!data.jsonvalue.equalsIgnoreCase("null")) {
                    // p("============== processing value for "+dataname+": "+data.jsonvalue);
                    Object jvalueobject = jinputdata.get("value");
                    // value could just be a string, or a list, or a json object

                    List<DataValue> values = DataValue.fromJsonArray(
                            jvalueobject, data);
                    // p("--- Got "+values.size()+" datavalue objects");
                    data.values = values;
                } else {
                    data.values = null;
                    // p("Value is null");
                }
            } else {
                p("Data has no value field");
            }
        } catch (Exception e) {
            e.printStackTrace();
            err("Could not process json: " + ct.toString() + "\nbecause:"
                    + e.getMessage());
            System.exit(0);
        }
        // p("fromJsonObj at end is: "+data);
        return data;

    }

    public boolean hasSubData() {
        return subdatalist != null && subdatalist.size() > 0;
    }

    public int getNrSubElements() {
        if (subdatalist == null) {
            return 0;
        }
        return subdatalist.size();
    }

    public void add(InputData p) {
        if (subdatalist == null) {
            subdatalist = new ArrayList<InputData>();
        }
        subdatalist.add(p);
    }

    public String getName() {
        return name;
    }

    public InputData getSubData(String name) {
        for (InputData p : subdatalist) {
            if (p.getName().equalsIgnoreCase(name)) {
                return p;
            }
        }
        // p("Found no Proc with name " + name);
        return null;
    }

    public DataValue getSubValue(String value) {
        if (value == null) {
            return null;
        }
        if (this.values != null) {
            for (DataValue dv : values) {
                if (dv.value != null && dv.value.equalsIgnoreCase(value)) {
                    return dv;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String s = "Data " + name + ", t=" + this.dataType.getName()
                + ", values:\n";

        if (subdatalist != null) {

            for (int i = 0; i < subdatalist.size(); i++) {
                InputData p = subdatalist.get(i);
                s += p.toString();
                if (i + i < subdatalist.size()) {
                    s += "\n";
                }
            }
        }
        if (values != null) {
            for (DataValue dv : values) {
                s += dv.toString() + " ";
            }
        }
        return s;
    }

    public static String toJson(List<InputData> datas) {
        if (datas == null || datas.size() < 1) {
            return null;
        }
        String sep = ",\n";
        String json = "{";
        boolean many = false;
        if (datas.size() > 1) {
            many = true;
        }

        for (int i = 0; i < datas.size(); i++) {
            json += datas.get(i).toJson(true);
            if (i + 1 < datas.size()) {
                json += sep;
            }
        }
        json += "}";

        return json;
    }

    public String toJson(boolean brief) {
        String sep = ",";
        String j = key(getName()) + "{ ";
        j += key("type") + q(this.getDataType().getName()) + sep;
        if (!brief) {
            j += key("source") + q(source) + sep;
        }
        if (!brief) {
            j += key("inputAction") + q(inputAction.getName()) + sep;
        }
        if (!brief && this.getDescription() != null) {
            j += key("description") + q(this.getDescription()) + sep;
        }
        j += key("value");
        if (jsonvalue != null) {
            j += jsonvalue + sep;
        } else {
            if (values.size() == 1) {
                DataValue dv = values.get(0);
                j += q(dv.getValue()) + sep;
            } else {
                j += "[";
                for (int i = 0; i < values.size(); i++) {
                    DataValue dv = values.get(i);
                    j += q(dv.getValue());
                    if (i + 1 < values.size()) {
                        j += ",";
                    }
                }
                j += "]";
            }
        }
        if (subdatalist != null) {
            for (int i = 0; i < subdatalist.size(); i++) {
                InputData sub = subdatalist.get(i);
                j += sub.toJson(brief);
                if (i + 1 < subdatalist.size()) {
                    j += ",";
                }
            }
        }
        if (j.endsWith(",")) {
            j = j.substring(0, j.length() - 1);
        }
        j += "}";
        // p("Got json: " + j);
        return j;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        return ((InputData) o).name.equals(name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    private static void warn(String msg) {
        log.warning(msg);
    }

    private static void err(String msg) {
        log.severe(msg);
    }

    private static void p(String msg) {
      //  log.info(msg);
    }

    /**
     * turn String into a json key with quotes and column :
     */
    private String key(String s) {
        return q(s) + ": ";
    }

    /**
     * quote string for json
     */
    private String q(String s) {
        return "\"" + s + "\"";
    }

    /**
     * add json {} parenthesis
     */
    private String par(String s) {
        return "{ " + s + "} ";
    }

    /**
     * add json [] parenthesis
     */
    private String l(String s) {
        return "[ " + s + "]";
    }

    private DataType getDataType() {
        return dataType;
    }

    private void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    /**
     * @return the nextData
     */
    public InputData getNextData() {
        return nextData;
    }

    /**
     * @param nextData the nextData to set
     */
    public void setNextData(InputData nextData) {
        this.nextData = nextData;
    }

    public boolean hasValues() {
        return this.values != null && this.values.size() > 0;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    public void add(String the_value) {
        DataValue val = new DataValue(getName());
        val.setValue(the_value);
        this.add(val);
    }

    public void setType(DataType type) {
        this.dataType = type;
    }

}
