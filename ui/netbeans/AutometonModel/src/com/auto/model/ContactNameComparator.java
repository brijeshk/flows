package com.auto.model;

import java.util.Comparator;

public class ContactNameComparator implements Comparator<Contact>{

	@Override
	public int compare(Contact  a, Contact b) {
	
		if (a == null || b == null) return 0;
		if (a.getName() == null || b.getName() == null) return 0;
		return a.getName().compareTo(b.getName());
	}

}
