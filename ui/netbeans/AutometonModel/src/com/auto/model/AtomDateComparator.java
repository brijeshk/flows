package com.auto.model;

import java.util.Comparator;

public class AtomDateComparator implements Comparator<Atom>{

	@Override
	public int compare(Atom a, Atom b) {
		// seconds is good enough :-)
		// we want the larger one first, which is the most recent
		return (int)((a.getAtomData().getCreated() - b.getAtomData().getCreated())/1000);
	}

}
