package com.auto.model;

import java.util.Comparator;

public class UserNameComparator implements Comparator<Contact>{

	@Override
	public int compare(Contact  fa, Contact fb) {
		
		String a = fa.getName();
		String b = fb.getName();
		if (a == null) return 0;
		return a.compareTo(b);
	}

}
