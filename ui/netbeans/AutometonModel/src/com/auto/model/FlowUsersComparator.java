package com.auto.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FlowUsersComparator implements Comparator<Flow>{

	@Override
	public int compare(Flow  fa, Flow fb) {
		
		List<Contact> la = fa.getUsers();
		List<Contact> lb = fb.getUsers();
		String a = "";
		String b = "";
		Collections.sort(la, new UserNameComparator());
		Collections.sort(lb, new UserNameComparator());
		for (Contact u: la) {
			a = a + u.toString()+";";
		}
		
		return a.compareTo(b);
	}

}
