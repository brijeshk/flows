package com.auto.model;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/* group flows by topic, state, date or users */
public class FlowGroup {

	private static final Logger log = Logger.getLogger(FlowGroup.class
			.getName());

	private boolean port;
	
	public String getType() {
		if (isDate())  return  "date";
		else if (isPort())  return "gate";
		else if (isUsers())  return "user";
		else if (isState())  return "state";
		else return "unknown";
	}
	public boolean isNothing() {
		return !port && !state && !date && !users;
	}
	public boolean isPort() {
		return port;
	}

	public void setPort(boolean port) {
		this.port = port;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public boolean isDate() {
		return date;
	}

	public void setDate(boolean date) {
		this.date = date;
	}

	public boolean isUsers() {
		return users;
	}

	public void setUsers(boolean users) {
		this.users = users;
	}

	private boolean state;
	private boolean date;

	private boolean users;

	public FlowGroup() {

	}

	/* the flow passes, it a specified object in the filter matches */
	public void group(List<Flow> flows) {

		if (port ) {
			Collections.sort(flows, new FlowTopicComparator());

		}
		if (state ) {
			Collections.sort(flows, new FlowStateComparator());

		}
		if (users ) {
			Collections.sort(flows, new FlowUsersComparator());

		}
		if (date ) {
			Collections.sort(flows, new FlowDateComparator());
		}
		p("flows are sorted. Next, create a map or thing to group by, and map it");
	}

	@Override
	public String toString() {
		return "FlowGroup (port=" + port + ", state=" + state + ", users="
				+ users + ", date=" + date + ")";
	}

	private static void p(String msg) {
		log.info(msg);
	}

	private static void warn(String msg) {
		log.warning(msg);
	}

	
}
