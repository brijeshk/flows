package com.auto.model;

import java.util.Comparator;

public class FlowStateComparator implements Comparator<Flow>{

	@Override
	public int compare(Flow  fa, Flow fb) {
		
		State a = fa.getState();
		State b = fb.getState();
		if (a == null || b == null) return 0;
		return a.getName().compareTo(b.getName());
	}

}
