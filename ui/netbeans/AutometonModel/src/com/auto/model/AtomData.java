/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.logging.Logger;

import com.auto.db.DbMap;
import com.auto.utils.DateTools;

/**
 *
 * @author Chantal
 */
public class AtomData implements Serializable {

    private static final Logger log = Logger.getLogger(AtomData.class.getName());

    private String sender;
    private int flow_id;
    private int port_id;
    private String body;
    private String crud_flag;
    private String state;
    private String data;
    private int read;
    private String flags;
    private long aid;
    
    // date in ms
    private long created;
    // date in ms
    private long modified;

    public AtomData() {        
        this.createUUID();
        this.created = System.currentTimeMillis();
    }

    public AtomData(String sender, String crud_flag, String state,
            String data, String body, int flow_id, long created) {
    	this( 0, sender, crud_flag, state, data, body, flow_id, created);
    	 this.createUUID();
    }
    public AtomData(long aid, String sender, String crud_flag, String state,
            String data, String body, int flow_id, long created) {
        this();
        this.sender = sender;
        this.body = body;
        this.aid = aid;
        this.crud_flag = crud_flag;
        this.flow_id = flow_id;
        this.state = state;
        this.data = data;
        this.flags = "";
        this.setRead(false);
        if (created <= 0) {
            this.created = System.currentTimeMillis();
        } else {
            this.created = created;
        }
        this.modified = created;
    }

    public void createUUID() {
    	
   	 //this.uuid = UUID.randomUUID().toString();
    	this.aid = System.currentTimeMillis()+(int)(Math.random()*100);
   }
    public String toJson(FlowData fd) {
        String sep = ",\n";
        String j = " { \n";
        String said = fd.getCreator().substring(0, 3).toUpperCase()+aid;
        j += key("aid") + q(said) + sep;
        j += key("crud") + q(crud_flag) + sep;
        j += key("sender") + q(sender) + sep;
        j += key("body") + q(body) + sep;
        j += key("data") + data + sep;
        j += key("state") + q(state) + sep;

        String id = this.sender.substring(0, 2)+this.created;
        j += key("read") + q(id) + sep;
        j += key("fid") + q(fd.getUuid()) + sep;
        j += key("flags") + q(this.flags) + sep;
        // "2014-12-14T18:23:03Z",
        if (modified == 0) modified = System.currentTimeMillis();
        Date d = new Date(modified);
        // TODO: use calendar and localized string
        String ds = DateTools.toString(d);
        j += key("modified") + q(ds);
        j += "\n}";
      //  p("Atom json: " + j);
        return j;
    }

    /**
     * turn String into a json key with quotes and column :
     */
    private String key(String s) {
        return "    "+ q(s) + ": ";
    }

    /**
     * add json {} parenthesis
     */
    private String par(String s) {
        return "{ " + s + "} ";
    }

    /**
     * add json [] parenthesis
     */
    private String l(String s) {
        return "[ " + s + "]";
    }

    public AtomData( long aid, String sender, String crud_flag, String state,
            String body, String data, int flow_id) {
        this( aid, sender, crud_flag, state, body, data, flow_id, 0);
    }

    public static AtomData create(DbMap map) {
        // read the result set

        try {
            
            AtomData atom = new AtomData();
            atom.setSender(map.get("sender"));
            atom.setBody(map.get("body"));
            atom.setCrud_flag(map.get("crud_flag"));
            atom.setFlow_id(map.getInt("flow_id"));
            atom.setState(map.get("state"));
            atom.setAid(map.getLong("aid"));
            atom.setData(map.get("data"));
            atom.setRead(map.getBoolean("read"));
            atom.setCreated(map.getLong("created"));
            atom.setModified(map.getLong("modified"));
            atom.setFlags(map.get("flags"));
            
            if (atom.created <= 0) {
            	atom.created = System.currentTimeMillis();
            }
           // p("Got atom: " + atom + ", data=" + atom.getData());
            return atom;
        } catch (Exception e) {
            warn("Could not create atom: " + e.getMessage() + ":" + map.toString());
            e.printStackTrace();
        }
        return null;
    }

    public String existsString() {
        return "select * from atom where flow_id=" + this.getFlow_id() + " and created=" + this.getCreated();
    }

    public static String getFindById(int id) {
        return "select * from atom where id=" + id;
    }

    public static String getCreateString() {
        String res = "create table atom (aid string, sender string, crud_flag string, state string, "
                + "body string, data string, flow_id integer, created int, modified int, read integer, flags string )";
        //     p("getCreateString: "+res);
        return res;
    }

    public String getUpdateDataString() {
        String res = "update atom set data = ?  where aid = " + aid;
     //   p("Got DATA update string: " + res);
        return res;
    }

    public String getUpdateBodyString() {
        String res = "update atom set body = ?  where aid = " + aid;
     //   p("Got BODY update string: " + res);
        return res;
    }

    public String getUpdateString() {

        String res = "update atom set ";
        res += "sender=" + q(getSender()) + ", \n";
        res += "crud_flag=" + q(getCrud_flag()) + ", \n";
        res += "aid=" + aid + ", \n";
        res += "state=" + q(getState()) + ", \n";
        //res += "body="+q(getBody())+", \n";
        res += "flow_id=" + getFlow_id() + ", \n";
        res += "created=" + getCreated() + ", \n";
        res += "modified=" + getModified() + ", \n";
        res += "read=" + getRead() + ", \n";
        res += "flags=" + q(flags)+ " \n";
        // res += "port_id="+getPort_id()+"\n";
        res += "\n where aid = " + aid; 
     //   p("getUpdateString: " + res);
        return res;
    }

    public int getRead() {
        return read;
    }

    public String getDeleteString() {
        String res = "delete from atom where aid = " + aid;
    //    p("getDeleteString: " + res);
        return res;
    }

    public String getInsertString() {
        // date is time in ms
        if (this.state == null) {
            Exception e = new Exception("Got no state");
            e.printStackTrace();
            System.exit(0);;

        }
        String res = "insert into atom values(" + aid+ ", " + q(getSender()) + "," + q(getCrud_flag()) + ","
                + q(getState()) + ",null, null, " + getFlow_id() + ", " + getCreated() + ", " + getModified() + ", " + getRead() + ","+ q(flags)+")";

        // p("getInsertString: "+res);
        return res;
    }

    private static String q(String s) {
        return "\"" + s + "\"";
    }

    public static List<AtomData> createTestData() {
        ArrayList<AtomData> atoms = new ArrayList<AtomData>();
        long now = System.currentTimeMillis();
        long minute = 60 * 1000;
        long hour = minute * 60;
        long day = hour * 24;

        atoms.add(new AtomData(1, "chantal", "c", "Say", "", "what movie is playing?", 1, now - 2 * day));
        atoms.add(new AtomData(2, "brijesh", "c", "Ask", "", "how about out edge of tomorow?", 1, now - 4 * minute));
        atoms.add(new AtomData(3, "chantal", "c", "Ask", "", "Sure. what time?", 1, now - 1 * minute));
        atoms.add(new AtomData(4, "brijesh", "c", "Say", "", "the movie is at 6pm", 1, now));

//        atoms.add(new AtomData(4, "chantal", "c", "Request", "", "Do you want to buy the guitar?", 2, now - day));
//        atoms.add(new AtomData(5, "brijesh", "c", "Accept", "", "I want to buy it", 2, now - 8 * minute));
//        atoms.add(new AtomData(6, "chantal", "c", "Say", "", "it is 500.-", 2, now - 5 * minute));
//        atoms.add(new AtomData(7, "brijesh", "c", "Ship", "", "It is on the way", 2, now - 2 * minute));
//
//        atoms.add(new AtomData(8, "chantal", "c", "Request", "", "can you help with gardening?", 3, now - 2 * day));
//        atoms.add(new AtomData(9, "brijesh", "c", "Options", "", "sure wich a command", 3, now - day));
//        atoms.add(new AtomData(10, "chantal", "c", "Select", "", "TreeCutting", 3, now - 3 * hour));
//        atoms.add(new AtomData(11, "brijesh", "c", "Accept", "", "sounds ok", 3, now - 4 * hour));
//        
        return atoms;
    }

    @Override
    public String toString() {
        return "AtomData (aid=" + getAid() + ", body=" + getBody() + ", c=" + getCrud_flag() + ", state=" + getState() + ", date=" + new Date(getCreated()) + ", fid=" + this.getFlow_id() + ", data=" + getData() + ")";
    }

    private static void p(String msg) {
         log.info(msg);
    }

    private static void warn(String msg) {
        log.warning(msg);
    }

    private static void test() {
        List<AtomData> atoms = createTestData();
        for (AtomData fd : atoms) {
            p("TestAtom: " + fd);
        }
    }

    public static void main(String args[]) {
        test();
    }

  
    public String getSender() {
        return sender;
    }

    
    public void setSender(String s) {
        this.sender = s;
    }

    /**
     * @return the flow_id
     */
    public int getFlow_id() {
        return flow_id;
    }

    /**
     * @param flow_id the flow_id to set
     */
    public void setFlow_id(int flow_id) {
        this.flow_id = flow_id;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return the crud_flag
     */
    public String getCrud_flag() {
        return crud_flag;
    }

    /**
     * @param crud_flag the crud_flag to set
     */
    public void setCrud_flag(String crud_flag) {
        this.crud_flag = crud_flag;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the created
     */
    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long m) {
        this.modified = m;
    }

    @Override
    public boolean equals(Object o) {
        return ((AtomData) o).aid == aid;
    }

    @Override
    public int hashCode() {
        return (int) aid;
    }

    /**
     * @return the port_id
     */
    public int getPort_id() {
        return port_id;
    }

    /**
     * @param port_id the port_id to set
     */
    public void setPort_id(int port_id) {
        this.port_id = port_id;
    }

    public boolean isRead() {
        return read > 0;
    }

    private void setRead(int read) {
        this.read = read;
    }

    void setRead(boolean read) {
        this.read = read ? 1 : 0;
    }
     public String getFlags() {
        return flags;
    }
    
    public void setFlags(String flags) {
        this.flags = flags;
    }
    
    public boolean hasFlag(String f) {
        return flags != null && flags.contains(f);
    }
    
    public void addFlag(String f) {
        if (flags == null || flags.length() < 1) {
            flags = f;
        }
        
        if (!flags.contains(f)) {
            flags = flags + "," + f;
        }
      //  p("added flag " + f + ": " + flags);
    }
    public void removeFlag(String f) {
        if (flags == null || flags.length() < 1 || !flags.contains(f)) {
            return;
        }
        int pos = flags.indexOf(f);
        int len = f.length() + 1;
        flags = flags.substring(0, pos) + flags.substring(pos + len);
     //   p("removed flag " + f + ": " + flags);
    }
    public boolean hasId() {
    	return aid >0 ;
    }
    /**
     * @return the aid
     */
    public long getAid() {
        return aid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setAid(long aid) {
        this.aid = aid;
    }
}
