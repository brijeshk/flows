package com.auto.model;

import java.util.Comparator;

public class GroupNameComparator implements Comparator<Group>{

	@Override
	public int compare(Group  a, Group b) {
	
		
		return a.getName().compareTo(b.getName());
	}

}
