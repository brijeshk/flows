package com.auto.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class FlowFilter {
    private static final Logger log = Logger.getLogger(FlowFilter.class.getName());

	private Port port;
	private State state;
	private Date startDate;
	private Date endDate;
	private List<User> users;
	private boolean todo;
	private boolean waiting;
	
	private String searchstring;
	public FlowFilter() {
		
	}
	
	/* the flow passes, it a specified object in the filter matches */
	public boolean filter(Flow flow) {
		if (flow == null || flow.getPort() == null) return false;
		if (port != null) {
			if (!flow.getPort().equals(port)) {
				p("Flow does not match port "+port+": "+flow.getPort());
				return false;
			}
			
		}
		if (state != null) {
			if (!flow.getState().equals(state)) {
				p("Flow does not match state "+state+": "+flow.getState());
				return false;
			}
			
		}
		if (users != null) {
			
			for (User u: users) { 
				
				if (flow.getUsers() == null || !flow.getUsers().contains(u)) {
					p("Flow does not match users "+users+": "+flow.getUsers());
					return false;
					
				}
			}
			
		}
		if (startDate != null) {
			Atom at = flow.getAtoms().get(0);
			if (at.getCreated().before(startDate)) {
				p("Flow before start date "+startDate+": "+at.getCreated());
				return false;
			}
		}
		if (endDate != null) {
			Atom at = flow.getAtoms().get(0);
			if (at.getCreated().after(endDate)) {
				p("Flow after end date "+endDate+": "+at.getCreated());
				return false;
			}
		}
		if (searchstring != null) {
			String s = searchstring.toLowerCase();
			p("checkking if flow contains "+s);
			if (flow.getPort() != null) {
				if (flow.getPort().getName().contains(s)) {
			//		p("port "+flow.getPort().getName()+" contains "+s);
					return true;
				}
			}
			if (flow.getSubject().contains(s)) {
		//		p("Subject "+flow.getSubject()+" contains "+s);
				return true;
			}
			
			if (flow.getUsers() != null) {
				for (Contact c: flow.getUsers()) {
					if (has((User)c, s)) return true;
				}
			}
			if (flow.getAtoms() != null) {
				for (Atom a: flow.getAtoms()) {
					if (a.getBody() != null && a.getBody().contains(s)) {
					//	p("atom body "+a.getBody() +" contains "+s);
						return true;
					}
					if (a.getData() != null && a.getData().contains(s)) {
					//	p("atom data "+a.getData() +" contains "+s);
						return true;
					}
					if (a.getState() != null && a.getState().getName().contains(s)){
					//	p("atom state "+a.getState() +" contains "+s);
						return true;
					}
					if (has(a.getSender(), s)) {
					//	p("atom sender "+a.getSender() +" contains "+s);
						return true;
					}
										
				}
			}
			return false;
		}
		if (todo) {
			return flow.isTodo();
		}
		if (waiting) {
			return flow.isWaiting();
		}
		p("flow passes all filters "+toString()+": "+flow);
		return true;
	}
	
	private boolean has(User u, String s) {
		if (u == null) return false;
		if ((u.getName() != null && u.getName().contains(s)) || 
				((u.getEmail() != null && u.getEmail().contains(s) ) || 				
					( u.getPhone() != null &&  u.getPhone().contains(s))))  {
		//	p("User contains  :"+s);
			return true;
		}
		else return false;
	}
	
	 	@Override
	    public String toString() {
	        String s =  "FlowFilter (";
	        if (port != null) s += "port="+port+"; ";
	        if (state != null) s += "state="+state+"; ";
	        if (users != null && users.size()>0) s += "users="+users+"; ";
	        if (startDate != null) s += "startDate="+startDate+"; ";
	        if (endDate != null) s += "endDate="+endDate+"; ";
	        if (searchstring != null) s += "searchstring="+searchstring+"; ";
	        if (todo ) s += "todo; ";
	        if (waiting ) s += "waiting; ";
	        s = s +")";
	        return s;
	    }

	    private static void p(String msg) {
	        log.info(msg);
	    }

	    private static void warn(String msg) {
	        log.warning(msg);
	    }


	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	public void setUser(User user) {
		this.users = new ArrayList<User>();
		users.add(user);
	}

	public void addUser(User user) {
		if (users == null) this.users = new ArrayList<User>();
		users.add(user);
	}
	public Port getPort() {
		return port;
	}

	public void setPort(Port port) {
		this.port = port;
	}

	public String getType() {		
		if (this.port != null)  return  "topic "+port.getName();
		else if (this.startDate != null || this.endDate != null)  return "date";
		else if (this.users != null)  return "user";
		else if (this.state != null)  return "state "+state.getName();
		else if (this.searchstring != null)  return searchstring;
		else return "unknown";		
	}

	public boolean isNothing() {
		return port == null && state == null && users == null && startDate == null && endDate == null && searchstring == null;
	}

	public void setSearchstring(String searchstring) {
		this.searchstring = searchstring;
		
	}

	public void setTodo(boolean b) {
		this.todo = b;
		
	}
	public void setWaiting(boolean b) {
		this.waiting = b;
		
	}
}
