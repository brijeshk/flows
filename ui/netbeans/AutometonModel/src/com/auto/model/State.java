/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public class State implements Serializable {

    private static final Logger log = Logger.getLogger(State.class.getName());
  
    private static final long serialVersionUID = 1L;
    private String name;
    private List<String> trans;
    // "function":"weather.get_weather_forecast",
    private List<InputData> inputdatas;
    
    private List<State> prevstates;
    private List<State> nextstates;

    private String data;
    private String description;
    private boolean botCommand;

    private String actor;
    
    public State(String name) {
        this.name = name;
        if (name == null || name.equalsIgnoreCase("null")) {
        	err("State has no name");
        	Exception e = new Exception("no name");
        	e.printStackTrace();
        	//System.exit(0);
        }
        trans = new ArrayList<String>();
        nextstates = new ArrayList<State>();
        prevstates = new ArrayList<State>();
    }
    public boolean actorIsAnyone() {
    	return actor == null || actor.length()<1 || actor.equalsIgnoreCase("anyone") || actor.equalsIgnoreCase("null");
    }
    public boolean actorIsToggle() {
    	return actor != null && actor.equalsIgnoreCase("toggle");
    }
    public boolean actorIsFlowOwner() {
    	return actor != null && actor.equalsIgnoreCase("originator");
    }
    public boolean actorIsPortOwner() {
    	return actor != null && actor.equalsIgnoreCase("owner");
    }

    public void add(InputData p) {
        if (inputdatas == null) {
            inputdatas = new ArrayList<InputData>();
        }
        inputdatas.add(p);
    }

    public InputData getDataOfType(DataType type) {
        if (inputdatas == null) return null;
        for (InputData data: inputdatas) {
            if (data.getType().equals(type)) {
                return data;
            }
        }
        return null;
    }
    public static List<State> fromJsonArray(JSONObject ct, Port port) {
        ArrayList<State> states = new ArrayList<State>();
        Iterator<String> it = ct.keys();
        for (; it.hasNext();) {
            String statename = it.next();
            State state = fromJsonObj(ct, statename, port);
            if (state != null) {
                states.add(state);
            }
        }
        return states;
    }
    
    public boolean isBotCommand() {
    	return botCommand;
    }

    
    /*
     * "data":{  
               "Choose Work":{  
                  "type":3,
                  "desc":"",
                  "value":[  
                     "YardWork.Landscaping",
                     "YardWork.Paving",
                     "TreeCutting",
                     "Maintenance.OneTime",
                     "Maintenance.Recurring"
                  ]
               }
            },
     */
    public static State fromJsonObj(JSONObject ct, String statename, Port port) {
        State state = new State(statename);
       
        try {
            JSONObject jstate = ct.getJSONObject(statename);
           
            if (jstate.has("botCommand")) {
            	state.botCommand = jstate.getBoolean("botCommand");
            }
            if (jstate.has("actor")) {
            	state.actor = jstate.getString("actor");
            }
            if (jstate.has("data")) {
                Object odata = jstate.get("data");
                state.data = ""+odata;
                if (odata != null && !state.data.equalsIgnoreCase("null")) {
                //    p("====================  about to parse state "+statename+"  data: \n"+state.data);                    
                    if (odata instanceof JSONObject) { 
                        
                        JSONObject dataobject = jstate.getJSONObject("data");
                        state.inputdatas = InputData.fromJsonArray(dataobject, state);
                //        p(state.getName()+":\n"+state.inputdatas);
                    }
                    else {
                        p("Data is not JSONObject: "+odata);
                    }
                }
            }
            
            Object obj = jstate.get("transitions");
            if (obj != null && obj != JSONObject.NULL && obj instanceof JSONArray) {
                JSONArray trans = jstate.getJSONArray("transitions");
                if (trans != null) {
                    for (int t = 0; t < trans.length(); t++) {
                        String next = trans.getString(t);
                        state.add(next);
                    }
                }
            }
            else if (obj != null  && obj != JSONObject.NULL ) {
                warn("Don't know how to handle transitions  "+obj+":"+obj.getClass().getName());
            }
           
        } catch (Exception e) {
            e.printStackTrace();
            err("Could not process json: " + ct.toString() + "\nbecause:" + e.getMessage());
            System.exit(0);
        }
        return state;

    }

   

    public void addNext(State s) {
        if (s != null && s.getName() != null && !nextstates.contains(s)) {
            nextstates.add(s);
        }
    }

    public void removeNext(State s) {
        nextstates.remove(s);
    }

    public void addPrev(State s) {
        if (s != null && s.getName() != null &&!prevstates.contains(s)) {
            prevstates.add(s);
        }
    }

    public void removPrev(State s) {
        prevstates.remove(s);
    }

    public List<State> getNextStates() {
        return nextstates;
    }

   
    public List<State> getPrevStates() {
        return prevstates;
    }

    public boolean isConnected(State st) {
        return prevstates.contains(st) || nextstates.contains(st);
    }

    public List<InputData> getInputDatas() {
        return inputdatas;
    }

    public List<String> getTransitions() {
        return trans;
    }

    public String getName() {
        return name;
    }

    public InputData getInputData(String name) {
        for (InputData p : inputdatas) {
            if (p.getName().equalsIgnoreCase(name)) {
                return p;
            }
        }
        // p("Found no Proc with name " + name);
        return null;
    }

    public void add(String name) {

        if (!trans.contains(name)) {
            trans.add(name);
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public String toJson() {
        String j = "{\n";
        String sep = ",\n";
        String tab = "       ";
        j += tab+key("name") + q(this.getName()) + sep;
        if (!this.actorIsAnyone()) j += tab+key("actor") + q(this.actor) + sep;
        j += tab+key("botCommand") + this.botCommand + sep;
        if (this.getDescription() != null) j +=tab+ key("description") + q(this.getDescription()) + sep;
        
        int nr = trans.size();
        j += tab+key("transitions")+"[";
        if (nr > 0) {
	        for (int i = 0; i < nr; i++) {
	            String st = trans.get(i);
	            j += q(st);
	            if (i + 1 < nr) {
	                j += ",";
	            }
	        }	    
        }
        j += "]";
        if (data != null) {
        	 j += sep+tab+key("data")+data;
        }             
        else j += "\n"+tab+ "}";
        return j;
    }
    
   

    @Override
    public boolean equals(Object o) {
        return ((State) o).name.equalsIgnoreCase(name);
    }

    @Override
    public int hashCode() {
        return name.toLowerCase().hashCode();
    }

    public boolean hasStates() {
        return this.trans != null && trans.size() > 0;
    }


    public String getData() {
        return data;
    }

    public void setData(String inputData) {
        this.data = inputData;
    }


    /**
     * turn String into a json key with quotes and column :
     */
    private String key(String s) {
        return q(s) + ": ";
    }

    /**
     * quote string for json
     */
    private String q(String s) {
        return "\"" + s + "\"";
    }

    /**
     * add json {} parenthesis
     */
    private String par(String s) {
        return "{ " + s + "} ";
    }

    /**
     * add json [] parenthesis
     */
    private String l(String s) {
        return "[ " + s + "]";
    }
    private static void warn(String msg) {
        log.warning(msg);
    }
    private static void err(String msg) {
        log.severe(msg);
    }

    private static void p(String msg) {
        //log.info(msg);
    }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public boolean hasData() {
		return this.getData() != null && this.getData().length()>0 && !this.getData().equalsIgnoreCase("null");
	}
	/** soft states do not update the flow state */
	public boolean isSoftState() {
		return this.getName().equalsIgnoreCase("say") || this.getName().equalsIgnoreCase("remind");
	}
}
