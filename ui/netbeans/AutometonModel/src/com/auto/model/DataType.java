package com.auto.model;

import java.io.Serializable;
import java.util.logging.Logger;

public class DataType implements Serializable {

    private static final Logger log = Logger.getLogger(DataType.class
            .getName());

    /*
     * 	NoData   = "null"
     String   = "string"
     Quantity = "quantity"
     Price    = "price"
     Location = "location"
     Time     = "time"
     Image    = "image"
     URL      = "url"
     Map      = "map"    // recursive map type
     */
    private String name;
    private String label;
    private boolean list;

    private static final DataType[] DATA_TYPES = {
        DT("No data", "null"),
        DT("String", "string"),
        DT("Quantity", "quantity"),
        DT("Price", "price"),
        DT("Location", "location"),
        DT("Time", "time"),
        DT("Image", "image"),
        DT("URL", "url"),
        DT("Map", "map"),
        DT("int", "int")
    };

    public static final DataType NoData = DATA_TYPES[0];
    public static final DataType Text = DATA_TYPES[1];
    public static final DataType Quantity = DATA_TYPES[2];
    public static final DataType Price = DATA_TYPES[3];
    public static final DataType Location = DATA_TYPES[4];
    public static final DataType Time = DATA_TYPES[5];
    public static final DataType Image = DATA_TYPES[6];
    public static final DataType Url = DATA_TYPES[7];
    public static final DataType Map = DATA_TYPES[8];
    public static final DataType Int = DATA_TYPES[9];

    private static final DataType DT(String label, String name) {
        return new DataType(label, name);
    }

    /* can be used at some point to validate the input */
    public boolean validate(Object value) {
        if (isText()) return value instanceof String;
        if (isQuantity()) return value instanceof Number;
        return true;
    }
    public String getLabel() {
        return label;
    }
    public boolean isText() {
        return this.label.equalsIgnoreCase(Text.label);
    }
    public boolean isNoData() {
        return this.label.equalsIgnoreCase(NoData.label);
    }
    /* do we separate date and time? */
    public boolean isDate() {
        return this.label.equalsIgnoreCase(Time.label);
    }
    public boolean isTime() {
        return this.label.equalsIgnoreCase(Time.label);
    }
    public boolean isPrice() {
        return this.label.equalsIgnoreCase(Price.label);
    }

    public boolean isImage() {
        return this.label.equalsIgnoreCase(Image.label);
    }
    public boolean isInt() {
        return this.label.equalsIgnoreCase(Int.label);
    }
    
    public boolean isRecursive() {
        return this.label.equalsIgnoreCase(Map.label);
    }

    public boolean isLocation() {
        return this.label.equalsIgnoreCase(Location.label);

    }

    public boolean isQuantity() {
        return this.label.equalsIgnoreCase(Quantity.label);

    }

    public boolean isUrl() {
        return this.label.equalsIgnoreCase(Url.label);
    }

    private DataType(String label, String name) {
        this.name = name;
        this.label = label;
        setList(false);
    }

    public String getName() {
        return this.name;
    }

    public static DataType getType(String name) {
        boolean islist = false;
        name = name.toLowerCase();
        if (name.startsWith("[")) {
            islist = true;
            name = name.substring(1, name.length() - 1);

        }
        for (DataType t : DataType.DATA_TYPES) {
            if (name.startsWith(t.name)) {
                if (islist) {
                    t = new DataType(t.label, t.name);
                    t.setList(true);
                }
                return t;
            }
        }
        err("Found no type with name "+name+", using text");
        return DataType.Text;
    }

    public boolean isList() {
        return list;
    }

    public void setList(boolean list) {
        this.list = list;
    }

    private static void err(String msg) {
        log.severe(msg);
    }

    private static void p(String msg) {
        log.info(msg);
    }

}
