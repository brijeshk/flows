/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.model;

import java.io.Serializable;

/**
 *
 * @author Chantal
 */
public enum InputAction  implements Serializable  {
    /** NORMAL means user must input value and it is required */
    REQUIRED("required"), OPTIONAL("optional"), SELECT1("select1"), SELECTN("selectn"), INFO("info"), HIDDEN("hidden");
    
    private String name;
    
    private InputAction(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public static InputAction find(String name) {
        if (name == null || name.trim().length() ==0 || name.equalsIgnoreCase("null") || name.equalsIgnoreCase("default")) {
            return REQUIRED;
        }
        for (InputAction in: InputAction.values()) {
            if (in.getName().equalsIgnoreCase(name)) return in;
        }
        
        return null;
    }
}
