/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Chantal
 */
public class Atom implements Serializable {

    private static final Logger log = Logger.getLogger(Atom.class.getName());

    AtomData atomdata;

    // derived data    
    private State state;
    private Flow flow;
    private User sender;
    private Date created;
   
    private transient final FlowManager man;

    public Atom(AtomData atomdata) {
        this.atomdata = atomdata;
        man = FlowManager.getManager();
        this.flow = man.getFlow(atomdata.getFlow_id());
        this.sender = man.getUser(atomdata.getSender());
        //p("Creating atom: "+atomdata);
        this.state = new State (atomdata.getState());
       
    }

    public String toJson (FlowData fd) {
    	return atomdata.toJson(fd);
    }
   
    public Date getCreated() {
        if (created == null) {
            created = new Date(atomdata.getCreated());
        }
        return created;
    }

    public String getSimpleDataString() {
        p("getSimpleDataString");
        String res = "";
        if (getData() != null) {
            // parse atom data to 
            String jsondata = getData();
            p("data json: "+jsondata);
            List<InputData> datas = InputData.fromJsonArray(jsondata, getState());
            p("Got :"+datas.size()+" data items");
            if (datas != null && datas.size() > 0) {
               
                for (int i = 0; i < datas.size(); i++) {
                    InputData data = datas.get(i);
                    if (data.hasValues()) {
                        p("Got :"+data.getValues().size()+" value items");
                        for (DataValue value : data.getValues()) {                        
                            res += value.getName() + "=";
                            p("Got "+value);
                            if (value != null) {
                                res += value.getValue();
                            } else {
                                res += "no value";
                            }
                            res += "\n";
                        }
                    }
                }
            }
            return res.trim();
        }
        
        return res;
    }

    public List<User> getReceivers() {
        List<User> rec = new ArrayList<User>();
        //log.info("Getting receivers for flow with sender " + getSender().getId() + ", flow has " + flow.getUsers().size() + " users: " + flow.getUsers());
        if (flow.getUsers() == null) {
        	err("Flow "+flow+" has no (known) users");
        	return null;
        }
        
        for (Contact c : flow.getUsers()) {
        	if (c instanceof User) {
        		User u=  (User)c;        	
	            if (!u.getId().equalsIgnoreCase(getSender().getId())) {
	                rec.add(u);
	             //   log.info("Got receiver: " + u);
	            }
        	}
        	else {
        		Group g = (Group) c;
        		List<User> users = g.getUsers();
        		for (User u: users) {
        			 if (!u.getId().equalsIgnoreCase(getSender().getId())) {
     	                rec.add(u);
     	        //        log.info("Got receiver: " + u);
     	            }
        		}
        	}
        }
        return rec;
    }

    public String getCrud() {
        return atomdata.getCrud_flag();
    }

    @Override
    public String toString() {
    	String d = getData();
    	if (d == null) d = "";
    	if (d.length()>50) d = d.substring(0, 50)+"...";
        String s = "Atom " + atomdata.getAid() + ", data=" + d + ", b=" + this.getBody() + ", state=" + this.getState() + ", created=" + atomdata.getCreated();
        return s;
    }

    public String getBody() {
    	
        return atomdata.getBody();
    }

    public String getData() {
        return atomdata.getData();
    }

    public List<InputData> extractData() {
        if (getData() == null) {
            return null;
        }
        List<InputData> datas = InputData.fromJsonArray(this.getData(), getState());
        return datas;
    }

    public AtomData getAtomData() {
        return atomdata;
    }

    public void setMessage(String msg) {
        this.atomdata.setBody(msg);
    }

    public void setBody(String msg) {
        this.atomdata.setBody(msg);
    }

    public void setData(List<InputData> datas) {
        log.info("Setting data to atom: " + datas);
        String j = InputData.toJson(datas);
        log.info("resulting json data: " + j);
        this.atomdata.setData(j);
    }

    /**
     * @return the state
     */
    public State getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * @return the flow
     */
    public Flow getFlow() {
        return flow;
    }

    /**
     * @return the sender
     */
    public User getSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(User sender) {
        this.sender = sender;
    }

    public int getFlow_id() {
        return atomdata.getFlow_id();
    }

    @Override
    public boolean equals(Object o) {
        Atom f = (Atom) o;
        if (f.getFlow_id() != this.getFlow_id() ) return false;
        return f.getAtomData().equals(atomdata);
    }

    @Override
    public int hashCode() {
        return atomdata.hashCode();
    }
    public long getAid() {
    	return atomdata.getAid();
    }
    public long getId() {
    	return atomdata.getAid();
    }
    private static void warn(String msg) {
		log.warning(msg);
	}

	private static void err(String msg) {
		log.severe(msg);
	}

	private static void p(String msg) {
	//	log.info(msg);
	}

	public boolean isUnread() {
		return !atomdata.isRead();
	}

	public void setRead(boolean b) {
		atomdata.setRead(b);
		
	}

	public void setCrud(String crud) {
		atomdata.setCrud_flag(crud);
		
	}

}
