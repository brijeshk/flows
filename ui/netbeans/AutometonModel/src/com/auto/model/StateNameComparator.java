package com.auto.model;

import java.util.Comparator;

public class StateNameComparator implements Comparator<State>{

	@Override
	public int compare(State  fa, State fb) {
		
		String a = fa.getName();
		String b = fb.getName();
		return a.compareTo(b);
	}

}
