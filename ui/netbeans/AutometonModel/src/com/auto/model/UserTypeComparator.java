package com.auto.model;

import java.util.Comparator;

public class UserTypeComparator implements Comparator<User>{

	@Override
	public int compare(User  fa, User fb) {
		
		String a = fa.getType();
		String b = fb.getType();
		if (a.equals(b)) {
			return fa.getId().compareTo(fb.getId());
		}
		else {
			return 1000*a.compareTo(b);
		}
	}

}
