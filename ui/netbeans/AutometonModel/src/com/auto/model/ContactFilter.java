package com.auto.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class ContactFilter {
    private static final Logger log = Logger.getLogger(ContactFilter.class.getName());

	private String searchstring;
	
	public ContactFilter() {
		
	}
	
	/* the flow passes, it a specified object in the filter matches */
	public boolean filter(Contact contact) {
		if (contact == null ) return false;
		
		if (searchstring != null) {
			String s = searchstring.toLowerCase();
			p("checkking if flow contains "+s);
			if (contact.getPorts() != null) {
				for (Port p: contact.getPorts()) {
				if (p.getName().contains(s)) {
			//		p("port "+flow.getPort().getName()+" contains "+s);
					return true;
				}
				}
			}
			if (contact.getName().contains(s)) {
		//		p("Subject "+flow.getSubject()+" contains "+s);
				return true;
			}
			if (contact instanceof User) {
				User u = (User)contact;
			
				if (u.getEmail() != null && u.getEmail().contains(s)) {
				//		p("Subject "+flow.getSubject()+" contains "+s);
						return true;
					}
				if (u.getPhone() != null && u.getPhone().contains(s)) {
					//		p("Subject "+flow.getSubject()+" contains "+s);
							return true;
						}
			
			}
			return false;
		}
		p("contact passes all filters "+toString()+": "+contact);
		return true;
	}
	
	private boolean has(User u, String s) {
		if (u == null) return false;
		if ((u.getName() != null && u.getName().contains(s)) || 
				((u.getEmail() != null && u.getEmail().contains(s) ) || 				
					( u.getPhone() != null &&  u.getPhone().contains(s))))  {
		//	p("User contains  :"+s);
			return true;
		}
		else return false;
	}
	
	 	@Override
	    public String toString() {
	        String s =  "FlowFilter (";
	        
	        if (searchstring != null) s += "searchstring="+searchstring+"; ";
	        s = s +")";
	        return s;
	    }

	    private static void p(String msg) {
	       // log.info(msg);
	    }

	    private static void warn(String msg) {
	        log.warning(msg);
	    }


	

	public String getType() {		
		return searchstring;
				
	}

	

	public void setSearchstring(String searchstring) {
		this.searchstring = searchstring;
		
	}
}
