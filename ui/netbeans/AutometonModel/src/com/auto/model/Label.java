package com.auto.model;

import java.io.Serializable;

public class Label implements Serializable{

	String name;
	
	public Label(String name) {
		this.name = name;
	}
	public String toString() {
		return name;
	}
	
	public int hashCode() {
		return name.hashCode();
	}
	public boolean equals(Object o) {
		return o.toString().equals(name);
	}
	
}
