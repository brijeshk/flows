/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import com.auto.db.DbMap;
import com.auto.utils.DateTools;

/**
 * Base object that maps to the sqllight data object and only contains the basic
 * fields has methods to insert itself into a db, and instantiate itself from a
 * resultset
 *
 * @author Chantal
 */
public class FlowData implements Serializable {
    
    private static final Logger log = Logger.getLogger(FlowData.class.getName());
    
    private static final int NEW = 0x00000001;
    public static final int READ = 0x00000002;
    public static final int DELETED = 0x00000004;
    
    private int id;
    private int flag; // new, deleted, read etc
    private String uuid;
    private int protocol=-1;
    private String subject;
    private String creator;
    private String assignee;
    private String state;
    private String users;
    private String data;
    private String flags;
    private String crud;
    // date in ms
    private long modified;
    
    private List<AtomData> atomlist;
    
    public FlowData(int id) {
        this.id = id;
        createUUID();
        
    }
    
    public boolean isNew() {
        return (flag & NEW) == NEW;
    }
    
    public boolean isRead() {
        return (flag & READ) == READ;
    }
    
    public void setNew(boolean b) {
        flag = flag | NEW;
    }
    
    public void setRead(boolean b) {
        flag = flag | READ;
    }
    private void createUUID() {
    	 this.uuid = UUID.randomUUID().toString();
    }
    
    public FlowData(int id, String uuid, String creator, String assignee, int protocol,
            String state, String subject, String users, String data, int flag) {
        this(id);
        this.uuid = uuid;
        this.creator = creator;
        this.assignee = assignee;
        this.protocol = protocol;
        this.state = state;
        this.subject = subject;
        this.users = users;
        this.flags = "";
        this.data = data;
        this.flag = flag;
    }
    
    public String toJson(String crud) {
    	return toJson(crud, true);
    }
    public String toJson(String crud, boolean withatoms) {
        String sep = ",\n";
        String j = " { \n";
        
        j += key("crud") + q(crud) + sep;
        j += key("users") + this.users + sep;
        j += key("modified") + q(DateTools.toString(new Date(modified))) + sep;
        j += key("data") + q(data) + sep;
        j += key("state") + q(state) + sep;
        j += key("fid") + q(uuid) + sep;
        j += key("pid") + this.protocol + sep;
        j += key("flags") + q(this.flags) + sep;
        j += key("subject") + q(this.subject);
        if (withatoms) {
        	j += sep;
	        j += key("atoms") + "[";
	        if (atomlist != null) {        
	           for (int a = 0; a < atomlist.size(); a++) {
	                AtomData ad = atomlist.get(a);
	                j += ad.toJson(this);
	                if (a + 1 <atomlist.size()) { 
	                    j += ",\n";
	                }
	            }                
	        }
	        j += "]";        
        	j += "}\n";
        }
        //p("Flowdata json: " + j);
        return j;
    }
    public List<Contact> extractUsers() {
        List<Contact> users= new ArrayList<Contact>();
        FlowManager man = FlowManager.getManager();
        String userlist = getUsers();
        if (userlist != null) {
            if (userlist.startsWith("[")) {
                userlist = userlist.substring(1, userlist.length());
            }
            if (userlist.endsWith("]")) {
                userlist = userlist.substring(0, userlist.length() - 1);
            }
            //p("processing userslist " + userlist);
            String[] userids = userlist.split(",");
            for (String id : userids) {
            	id = id.replace("\"" , "");
                User u = man.getUser(id);
                if (u != null && !users.contains(u)) {
                    users.add(u);
                } else if (u == null) {
                    warn("Could not find user " + id);
                }
            }
        }
        return users;
    }
    public static List<FlowData> fromJsonArrayNoDb(String res) {
        ArrayList<FlowData> flowlist = new ArrayList<FlowData>();
        try {
            JSONArray jflows = new JSONArray(res);
            for (int f = 0; f < jflows.length(); f++) {
                JSONObject jo = jflows.getJSONObject(f);
                String uuid = jo.getString("fid");
                int portid = jo.getInt("pid");
                p("FlowData.fromJsonArrayNoDb: fot port id: "+portid);
                String fcrud = jo.getString("crud");
                String flowstate = jo.getString("state");
                String flags = jo.has("flags") ? jo.getString("flags") : null;
                String subject = jo.has("subject") ? jo.getString("subject") : null;
                JSONArray ju = jo.getJSONArray("users");
                JSONArray jatoms = jo.getJSONArray("atoms");
                
                String flowdata = ""+jo.get("data");
                if (flowdata.equalsIgnoreCase("null")) flowdata = null;
                String fmodified = jo.getString("modified");
                Date flowdate = DateTools.parseRFC3339Date(fmodified);
                ArrayList<String> users = new ArrayList<String>();
                if (ju != null && ju.length() > 0) {
                    for (int u = 0; u < ju.length(); u++) {
                        String userid = ju.getString(u);
                        users.add(userid);
                    }
                }
                
                int fid = 0;
                String creator = null;
                
                ArrayList<AtomData> atoms = new ArrayList<AtomData>();
                if (jatoms != null && jatoms.length() > 0) {
                    for (int a = 0; a < jatoms.length(); a++) {
                        JSONObject jatom = (JSONObject) jatoms.getJSONObject(a);
                        String from = jatom.getString("sender");
                        String body = jatom.getString("body");
                        if (creator == null) {
                            creator = from;;
                            
                        }
                        String amodified = jatom.getString("modified");
                        Date atomdate = DateTools.parseRFC3339Date(amodified);
                        String state = jatom.getString("state");
                        String acrud = jatom.getString("crud");
                        String id = null;
                        if (jatom.has("aid")) {
                        	id = jatom.getString("aid");
                        	p("got aid: "+id);
                        }
                        if (id == null || id.length()<1) { 
                        	id = from.substring(0,2)+System.currentTimeMillis();
                        }
                        long aid = 0;
                        if (id.length()> 3) {
                        	aid = Long.parseLong(id.substring(3));
                        }
                        else {
                        	aid = Long.parseLong(id);
                        }
                        String atomdata = ""+jatom.get("data");
                        String aflags = jo.has("flags") ? jo.getString("flags") : null;
                        int  read = jo.has("read") ? jo.getInt("read") : 0;
                        if (atomdata == null || atomdata.equalsIgnoreCase("null") || atomdata.equals("{}") || atomdata.equals("[]")) {
                            atomdata = null;
                        }
                        // public AtomData(int id, String sender, String crud_flag, String state,
                        //String data,  String body, int flow_id, long created) {
                        AtomData ad = new AtomData(aid, from, acrud, state, atomdata, body, fid, atomdate.getTime());
                        atoms.add(ad);
                        if (ad.getPort_id() <=0) ad.setPort_id(portid);
                        ad.setFlags(aflags);
                        ad.setRead(read > 0? true: false);
                    }
                }
                
                FlowData fd = new FlowData(fid, uuid, creator, null, portid, flowstate, null, ju.toString(), flowdata, 0);
                if (subject != null) fd.setSubject(subject);
                fd.setCrud(fcrud);
                fd.setFlags(flags);
                fd.setModified(flowdate.getTime());
                //p("fromJsonArrayNoDb: Got Flowdata: {\n" + fd.toJson(fcrud) + "\n}\n");
               // p("fromJsonArrayNoDb: Gate: " + fd.getProtocol());
                fd.setAtomlist(atoms);
                // check atoms if we have to save or update them
                flowlist.add(fd);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            p("Could not parse: " + res);
        }
        return flowlist;
    }

    public static List<Flow> fromJsonArray(String res) {
    	//p("Parsing flows\n"+res+"\n");
        ArrayList<Flow> flowlist = new ArrayList<Flow>();
        FlowManager man = FlowManager.getManager();
        List<FlowData> fdlist = fromJsonArrayNoDb(res);
        if (fdlist == null) {
        	p("Got no flow data list from "+res);
            return null;
        }
        for (FlowData fd : fdlist) {
            Flow flow = man.getFlowByUuid(fd.getUuid());            
            int fid = fd.getId();
            //p("Processing fd: "+fd);
            List<AtomData> atoms = fd.getAtomlist();
            fd.setAssignee(man.getMe().getId());
            
            if (flow == null) {
                // create new flow
            	//p("creating new flow");
                fid = man.getNextFlowNr();
                fd.setId(fid);
                flow = man.addCreatedFlow(fd, false);
                // check if flow is fine, does it have a known sender etc
                
            } else {
            	// update flow data
                fid = flow.getId();
                if (fd.getSubject()!= null) {
                	flow.setSubject(fd.getSubject());
                }                
                FlowData oldfd = flow.getFlowData();
                if (fd.getProtocol() != oldfd.getProtocol()) {
                	oldfd.setProtocol(fd.getProtocol());
                	flow.setPort(man.getPort(fd.getProtocol()));
                }
                oldfd.setUsers(fd.getUsers());
                fd = oldfd;
                
            }
            if (flow != null) {
	            Port port = man.getPort(fd.getProtocol());
	            if (port == null) {
	            	warn("Got no port. Protocol is: "+fd.getProtocol());
	            }
	            else {
		            flow.setPort(port);
		            flow.setState(port.getState(fd.getState()));
	            }
	            for (AtomData ad : atoms) {
	            //    p("Got AtomData: " + ad.toJson(fd.getUuid()));
	            //    Atom atom = flow.findAtom(ad.getUuid());
	                if (!ad.hasId()) {
	                	ad.createUUID();
	                }
	                Atom atom = flow.findAtom(ad.getAid());
	                if (atom == null && !ad.getCrud_flag().equals("d")) {
	                    // create                   
	                    atom = man.addCreatedAtom(flow, ad);
	                }
	                else p("Already have atom or should delete it: "+atom);
	                if (ad.getCrud_flag().equals("d")) {                	
	                    man.delete(ad, false);
	                } else if (ad.getCrud_flag().equals("u")) {
	                    man.update(atom, false);
	                } else if (ad.getCrud_flag().equals("r")) {
	                    atom.getAtomData().setModified(System.currentTimeMillis());
	                    man.update(atom, false);
	                }
	                
	            }
	            String fcrud = fd.getCrud();
	            if (fcrud != null && fcrud.equals("d")) {
	                // delete flow
	                man.delete(flow, false);
	            } else  {
	                man.update(flow, false);
	            }
            }
            flowlist.add(flow);
            
        }
        
        return flowlist;
    }
    
    public String key(String s) {
        return q(s) + ": ";
    }
    
    public static FlowData create(DbMap map) {
        // read the result set

        try {
            int id = map.getInt("id");
            FlowData flow = new FlowData(id);
            flow.setSubject(map.get("subject"));
            flow.setCreator(map.get("creator"));
            flow.setAssignee(map.get("assignee"));
            flow.setProtocol(map.getInt("protocol"));
            flow.setState(map.get("state"));
            flow.setUsers(map.get("users"));
            flow.setData(map.get("data"));

            //     p("Got flow: " + flow);
            return flow;
        } catch (Exception e) {
            warn("Could not create flow: " + e.getMessage() + ":" + map.toString());
        }
        return null;
    }
    
    public long getModified() {
        return modified;
    }
    
    public void setModified(long m) {
        this.modified = m;
    }
    
    public static String getCreateString() {
        String res = "create table flow (id integer, uuid string, creator string, assignee string, protocol integer, "
                + "state string, subject string, users string, data string, flags string, modified int)";
        //     p("getCreateString: "+res);
        return res;
    }
    
    public String getInsertString() {
        String res = "insert into flow values(" + getId() + ", " + q(getUuid()) + "," + q(getCreator()) + "," + q(getAssignee()) + ","
                + getProtocol() + ","
                + q(getState()) + "," + q(getSubject()) + ", " + q(getUsers().replace("\"", "")) + ", null, " + q(flags) + ", " + modified + ")";
        //p("getInsertString: " + res);
        return res;
    }
    
    public static String getFindById(int id) {
        return "select * from flow where id=" + id;
    }
    
    public String existsString() {
        return "select * from flow where uuid=" + q(uuid);
    }
    
    public static String getFindByUuId(String uuid) {
        return "select * from flow where uuid=" + q(uuid);
    }
    
    public String getUpdateDataString() {
        String res = "update flow set data = ?  where id = " + getId();
 //       p("Got DATA update string: " + res);
        return res;
    }
    
    public String getUpdateString() {
//    	if (state == null) {
//    		Exception e = new Exception("flow has no state");
//    		e.printStackTrace();
//    	}
        String res = "update flow set ";
        res += "uuid=" + q(getUuid()) + ", \n";
        res += "creator=" + q(getCreator()) + ", \n";
        res += "assignee=" + q(getAssignee()) + ", \n";
        res += "protocol=" + getProtocol() + ", \n";
        res += "modified=" + getModified() + ", \n";
        res += "state=" + q(getState()) + ", \n";
        res += "subject=" + q(getSubject()) + ", \n";
        res += "users=" + q(getUsers().replace("\"", "")) + ", \n";
        res += "flags=" + q(getFlags()) + " \n";
        // res += "data="+q(getData())+"\n";
        res += "\n where id = " + getId();
     //   p("getUpdateString: " + res);
        return res;
    }
    
    public String getDeleteString() {
        String res = "delete from flow where id = " + getId();
     //   p("getDeleteString: " + res);
        return res;
    }
    
    private static String q(String s) {
        return "\"" + s + "\"";
    }
    
    public static List<FlowData> createTestData() {
        ArrayList<FlowData> flows = new ArrayList<FlowData>();
        flows.add(new FlowData(1, "XYZ1", "chantal", "brijesh", 0, "Ask", "movie today", "chantal,brijesh", "movie today", 0));
//        flows.add(new FlowData(2, "XYZ2", "brijesh", "chantal", 1, "Offer", "guitar for sale", "chantal,brijesh", "guitar for sale", 0));
//        flows.add(new FlowData(3, "XYZ3", "chantal", "brijesh", 9, "Gardening", "Instruments?", "chantal,brijesh", "Instruments?", 0));
//        
        return flows;
    }
    
    @Override
    public String toString() {
        return "FlowData (id=" + getId() + ", c=" + getCreator() + ", a=" + getAssignee() + ", u=" + getUsers() + ", s=" + getState() + ", subj=" + getSubject() + ", d=" + getData() + ", p=" + this.getProtocol() + ", flag=" + flag + ", flags=" + flags + ")";
    }
    
    private static void p(String msg) {
       //   log.info(msg);
    }
    
    private static void warn(String msg) {
        log.warning(msg);
    }
    
    private static void test() {
        List<FlowData> flows = createTestData();
        for (FlowData fd : flows) {
            p("TestFlow: " + fd);
        }
    }
    
    public static void main(String args[]) {
        test();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the protocol
     */
    public int getProtocol() {
        return protocol;
    }

    /**
     * @param protocol the protocol to set
     */
    public void setProtocol(int protocol) {
        this.protocol = protocol;
    }

    /**
     * @return the creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * @param creator the creator to set
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return the assignee
     */
    public String getAssignee() {
        return assignee;
    }

    /**
     * @param assignee the assignee to set
     */
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the users
     */
    public String getUsers() {
    	if (users == null) users = "";
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(String users) {
        this.users = users;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }
    
    public String getFlags() {
        return flags;
    }
    
    public void setFlags(String flags) {
        this.flags = flags;
    }
    
    public boolean hasFlag(String f) {
        return flags != null && flags.contains(f);
    }
    
    public void addFlag(String f) {
        if (flags == null || flags.length() < 1) {
            flags = f;
        }
        
        if (!flags.contains(f)) {
            flags = flags + "," + f;
        }
        p("added flag " + f + ": " + flags);
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }
    
    @Override
    public boolean equals(Object o) {
        return ((FlowData) o).id == id;
    }
    
    @Override
    public int hashCode() {
        return id;
    }
    
    public void removeFlag(String f) {
        if (flags == null || flags.length() < 1 || !flags.contains(f)) {
            return;
        }
        int pos = flags.indexOf(f);
        int len = f.length() + 1;
        flags = flags.substring(0, pos) + flags.substring(pos + len);
        p("removed flag " + f + ": " + flags);
    }
    
    public List<AtomData> getAtomlist() {
        return atomlist;
    }
    
    public void setAtomlist(List<AtomData> atomlist) {
        this.atomlist = atomlist;
    }

    /**
     * @return the crud
     */
    public String getCrud() {
    	if (crud == null) crud  ="";
        return crud;
    }

    /**
     * @param crud the crud to set
     */
    public void setCrud(String crud) {
        this.crud = crud;
    }
}
