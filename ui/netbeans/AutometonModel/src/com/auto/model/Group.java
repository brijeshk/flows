/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public class Group extends Contact implements Serializable {

    
    private User creator;
    private List<User> users;
    private String userlist;
    
    public Group(String id) {
    	super();
        this.id = id;
        this.type = "private";       
     
        
    }
    public boolean isPublic() {
    	return type.equalsIgnoreCase("public");
    }
    public boolean isPrivate() {
    	return !isPublic();
    }    

    public static List<Group> fromJsonArray(String json) {
    //   p("Parsing to groups:" + json);
        ArrayList<Group> groups = new ArrayList<Group>();
        try {
            JSONArray ja = new JSONArray(json);
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jo = ja.getJSONObject(i);
                Group group = fromJsonObj(jo);
                if (group != null) {
                	groups.add(group);
                    p("Got group "+group.getName()+", group.id="+group.getId());
                } else {
                    p("Got NO group for json "+json);
                }
            }
        } catch (JSONException ex) {
            warn("Could not create groups from " + json + ":" + ex.getMessage());
            return null;
        }
        return groups;
    }
   
    public static Group fromJsonObj(JSONObject jo) {
        int id;
        String creator = null;
        String userlist = null;
        String name = "no name";
        boolean ispublic = false;
        FlowManager man = FlowManager.getManager();
        try {
            id = jo.getInt("id");
            
            if (jo.has("name")) name = jo.getString("name");
            if (jo.has("creator")) creator = jo.getString("creator");
            if (jo.has("users")) userlist = jo.getString("users");
            if (jo.has("ispublic")) ispublic = jo.getBoolean("ispublic");
            if (jo.has("userlist")) userlist = jo.getString("userlist");
          
          //   p("fromJsonObj: id=name="+id+", email="+email);
            Group group = new Group(""+id);
            group.setName(name);
            if (ispublic) group.setType("public");
            else  group.setType("private");
            group.creator = man.getUser(creator);
            group.userlist = userlist;
            group.parseUsers(userlist);
            
           
            return group;
        } catch (JSONException ex) {
            warn("Could not create user from " + jo + ":" + ex.getMessage());
            return null;
        }

    }
	public void parseUsers(String userlist) {
		if (userlist.startsWith("[")) userlist = userlist.substring(1);
		if (userlist.endsWith("]")) userlist = userlist.substring(0, userlist.length()-1);
		FlowManager man = FlowManager.getManager();
		if (userlist != null && userlist.length()>0) {
			/// parse user list
			String[] items = userlist.split(",");
			for (String it: items) {
				if (it.startsWith("\"")) it = it.substring(1, it.length()-1);
				if (it.startsWith("\'")) it = it.substring(1, it.length()-1);
				User us = man.getUser(it);
				if (us != null) {
					add(us);
				}
			}
		}
		// now get common ports
		if (ports == null || ports.size()<1) {
			ports = man.getCommonUserTopics(users);
		}
	}

    private void add(User us) {
		if (users == null) users = new ArrayList<User>();
		if (us != null && !users.contains(us)) users.add(us);
		
	}
	public static Group fromJson(String json) {
    	Group group = null;
        try {
            JSONObject jo = new JSONObject(json);
            group = fromJsonObj(jo);
        } catch (JSONException ex) {
            warn("Could not create user from " + json + ":" + ex.getMessage());
            return null;
        }
       // p("Got user: " + user.toString());
        return group;
    }

    public static List<Group> test() {
        String json = "[{'_id': 'ramya', 'email': 'ramya@localhost', 'message_transport_type': 'xmpp'},{'_id': 'brijesh', 'email': 'brijesh@localhost', 'message_transport_type': 'xmpp'},{'_id': 'manasi'},{'_id': 'nikhil'},{'_id': 'prateek', 'email': 'prateek@localhost', 'message_transport_type': 'xmpp'},{'_id': 'amitabh', 'email': 'amitabh@localhost', 'message_transport_type': 'xmpp'},{'_id': 'ebay'},{'_id': 'movies'},{'_id': 'weather'},{'_id': 'brijeshsk@gmail.com', 'email': 'brijeshsk@gmail.com', 'message_transport_type': 'xmpp'},{'_id': 'rambri@gmail.com', 'email': 'rambri@gmail.com', 'message_transport_type': 'xmpp'},{'_id': 'prat.kr@gmail.com', 'email': 'prat.kr@gmail.com', 'message_transport_type': 'xmpp'},{'_id': 'prateek.kr@gmail.com', 'email': 'prateek.kr@gmail.com', 'message_transport_type': 'xmpp'},"
        		+ "{'_id': 'chantal', 'email': 'chantal', 'message_transport_type': 'xmpp'}]";
        return fromJsonArray(json);
    }

    public static void main(String args[]) {
       // test();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the email
     */
    public User getCreator() {
    	return creator;
    }

   
    @Override
    public boolean equals(Object o) {
    	if (!(o instanceof Group)) return false;
        return ((Group) o).id == id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
    public String getUserlist() {
        if (users == null) return null;
        else return users.toString();
    }
	public List<User> getUsers() {
		return this.users;
	}
	public void setUserlist(String userlist) {
		this.userlist = userlist;
		
	}

   
}
