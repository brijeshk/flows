/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public class Port implements Serializable {

	private static final Logger log = Logger.getLogger(Port.class.getName());

	private int id;
	private String name;
	private String creator;
	private Contact owner;
	
	private String description;
	private List<State> states;
	private String json;
	private String jsettings;
	private List<InputData> settings;
	private String type;
	private String exposedBy;
	private List<Label> labels;
	private List<String> visible_to;
	private boolean canStartWithSay;

	private int visibilityLevel;
	
	
	private static FlowManager man = FlowManager.getManager();

	// command_transitions describes a command state machine.
	// command_processors describes what output each command produces. In many
	// cases, these are static selection choices (can be nested), but in some
	// cases, could be dynamic data obtained remotely. In such dynamic cases,
	// the command can
	public Port(int id, String name) {
		this.id = id;
		this.name = name;
		this.setType("Topics");
		states = new ArrayList<State>();
	}
	public void setId(int id) {
		this.id = id;
	}

	public Port(int id, String name, String type) {
		this.id = id;
		this.name = name;
		this.setType(type);
		states = new ArrayList<State>();
	}

	public boolean isVisibilityPublic() {
		return visibilityLevel == -1;
	}
	public boolean isVisibilityMe() {
		return visibilityLevel == 0;
	}
	public boolean isVisibilitySpecific() {
		return visibilityLevel == 1;
	}
	public List<String> getVisibleTo() {
		return visible_to;
	}

	public void setVisibleTo(List<String> visible_to) {
		this.visible_to = visible_to;
	}

	public boolean isVisibleTo(User to) {
		if (visible_to == null || visible_to.size() < 1) {
			warn("Port " + this + " is not visible to anyone");
		}
		for (String vis : visible_to) {
			if (vis.equalsIgnoreCase(to.getId())
					&& vis.equalsIgnoreCase(to.getEmail())) {
				return true;
			}
		}
		return false;
	}

	public List<Label> getLabels() {
		return labels;
	}

	public void add(Label l) {
		if (!labels.contains(l))
			labels.add(l);
	}
	public int getVisibilityLevel() {
		return visibilityLevel;
	}
	public void setVisibilityLevel(int visibilityLevel) {
		this.visibilityLevel = visibilityLevel;
	}

	public void setLabels(List<Label> labels) {
		this.labels = labels;
	}

	@Override
	public String toString() {
		return "Port (" + getId() + "," + name + ", " + creator + ", " + states
				+ ")";
	}

	public State getState(String statename) {
		if (statename == null) {
			warn("Statename is null in getState");
			return null;
		}
		for (State s : states) {
			if (s.getName().equalsIgnoreCase(statename)) {
				return s;
			}
			if (s.getInputDatas() != null) {
				for (InputData p : s.getInputDatas()) {
					if (p.getName().equalsIgnoreCase(statename)) {
						return new State(p.getName());
					}
				}
			}
		}
		// also check sub...

		if (statename.equalsIgnoreCase("say")) {
			State newstate = new State(statename);
			states.add(newstate);
			return newstate;
		}
		warn("Found no state with name " + statename + " for port "
				+ this.getId() + ":" + this.getName() + ", got:");

		System.out.println(states);
//		 try {
//		 warn("JSON IS: " + new JSONObject(json).toString(3));
//		 } catch (JSONException ex) {
//		 Logger.getLogger(Port.class.getName()).log(Level.SEVERE, null, ex);
//		 }
		
		State newstate = new State(statename);
		states.add(newstate);
		return newstate;

	}

	public boolean isTemplate() {
		return this.type.equalsIgnoreCase("templates");
	}

	public boolean isLabel() {
		return this.type.equalsIgnoreCase("labels") || this.states == null
				|| this.states.size() == 0;
	}

	public boolean isTopic() {
		return this.type.equalsIgnoreCase("topics");
	}

	public List<InputData> getInputDatas(String statename) {
		State state = getState(statename);
		if (state == null) {
			warn("No state " + statename + " -> no valid commands");
			return null;
		}
		return state.getInputDatas();
	}

	public static List<Port> fromJsonArray(String json, Contact contact) {
		ArrayList<Port> ports = new ArrayList<Port>();
		try {
			JSONArray ja = new JSONArray(json);
			for (int i = 0; i < ja.length(); i++) {
				JSONObject jo = ja.getJSONObject(i);
				Port port = fromJsonObj(jo, contact);
				if (port != null) {
					
					port.owner = contact;
					port.json = jo.toString();
					ports.add(port);
				}
			}
		} catch (JSONException ex) {
			warn("Could not create ports from " + json + ":" + ex.getMessage());
			ex.printStackTrace();
			return null;
		}
		return ports;
	}
	

	public String toJson() {
		String j = "{\n";
		String sep = ",\n";
		String tab = "   ";
		j += key("name") + q(this.getName()) + sep;
		j += key("creator") + q(this.getCreator()) + sep;
		if (this.getDescription() != null)
			j += key("description") + q(this.getDescription()) + sep;
		if (states != null) {
			j += key("commands")+"{\n";
			String st = "";
			for (int i = 0; i < states.size(); i++) {
				State s = (State) states.get(i);
				st += tab+key(s.getName());
				String val = s.toJson();
				st += val;
				if (i + 1 < states.size()) {
					st += tab+sep;
				}
			}
			j += st +"\n}"+ sep;
		}
		if (this.visible_to != null) {
			String vals = "[";
			int nr = visible_to.size();
			for (int i = 0; i < nr; i++) {
				vals += visible_to.get(i);
				if (i + 1 < nr) {
					vals += sep;
				}
			}
			j += key("visible_to") + vals+"]"+sep;
		}
		if (this.exposedBy != null) {
			j += key("exposedBy") + sep;
		}
		j += key("visibleToDegree") + this.getVisibilityLevel() + sep;
		j += key("canStartWithSay") + this.canStartWithSay;
		//j += key("settings") + this.s + sep;
		j += "\n}";
		p("Port json: "+j);
		return j;
	}

	private String removeSep(String s) {
		s = s.trim();
		if (s.endsWith(",")) {
			s = s.substring(0, s.length() - 1);
		}
		return s;
	}

	/**
	 * turn String into a json key with quotes and column :
	 */
	private String key(String s) {
		return q(s) + ": ";
	}

	/**
	 * quote string for json
	 */
	private String q(String s) {
		return "\"" + s + "\"";
	}

	/**
	 * add json {} parenthesis
	 */
	private String par(String s) {
		return "{ " + s + "} ";
	}

	/**
	 * add json [] parenthesis
	 */
	private String l(String s) {
		return "[ " + s + "]";
	}

	public List<State> getStates() {
		return states;
	}

	/**
	 * return list of possible starting states
	 */
	public List<State> getAllFirstStates() {
		return getFirstStates( null);
	}
	public List<State> getFirstStates(User flowcreator) {
		// TODO: cache this list, but update when new states are added
		List<State> firststates = new ArrayList<State>();
		
		if (states == null)
			return firststates;
		for (State maybe : states) {
			// if state has no transitions to it, then it is a first state
			boolean found = false;
			for (State state : states) {
				if (!state.equals(maybe)
						&& state.getTransitions().contains(maybe.getName())) {
					found = true;
					break;
				}
			}
			if (!found) {
				if (!firststates.contains(maybe)) {
					if (canExecuteState(null, maybe, flowcreator)) {
						firststates.add(maybe);
						
					}
				}
			}
		}

		return firststates;
	}

	
	public boolean canExecuteState(Flow flow, State maybe, User flowcreator) {
		if (flowcreator == null) {
			// we want all states, just return it
			return true;
		}
		p("Checking if flowcreator "+flowcreator+" can execute state "+maybe);
		User me = man.getMe();
		User portowner = man.getUser(this.getCreator());
		if (maybe.getName() != null && maybe.getName().length()>1 && !maybe.getName().equalsIgnoreCase("error")) {
			// check if state can be executed by anyone, or else, who can execute it
			if (maybe.actorIsAnyone() || 
					(maybe.actorIsFlowOwner() && me.equals(flowcreator))  ||
					(maybe.actorIsPortOwner() && me.equals(portowner)))   {
						return true;
			}
			if (maybe.actorIsToggle()) {
				// if atom of previous state was NOT sent by me, then it is fine
				// what if there is no prevatom? Then this should not be possible
				List<State> prevstates = maybe.getPrevStates();
				if (prevstates == null || prevstates.size()<1) {
					err("State with toggle should have prev states: "+maybe);
				}
				Atom prevatom = null;
				if (flow != null) prevatom = flow.findPrevAtomWithStates(prevstates);
				if (prevatom == null) {
					err(maybe +" is toggle, but got no prev atom??? That should not be possible!");
					return false;
				}
				if (man.isMe(prevatom.getSender())) { 
					p(maybe +" is toggle, but I had sent the prev atom, so I can't use this state");
					return false;
				}
				else return true;
			}
		}
		return false;
	}
	
	public static Port fromJsonObj(JSONObject jo, Contact user) {
		int id;
		List<State> states = new ArrayList<State>();
		String name = "unknown";
		String creator = "unknown";
		String description = null;
		boolean canStartWithSay = false;
		int vis = 0;
		String exposed = null;
		List<String> visible_to = null;
		// p("Processing ports of user " + user.getId());
		try {
			id = jo.getInt("id");

			if (jo.has("name")) {
				name = jo.getString("name");
			}
			if (jo.has("exposedBy")) {
				exposed = jo.get("exposedBy").toString();
			}
			if (jo.has("description")) {
				description = jo.getString("description");
			}
			if (jo.has("creator")) {
				creator = jo.getString("creator");
			}
			Port port = new Port(id, name);
			
			if (jo.has("canStartWithSay")) {
				canStartWithSay = jo.getBoolean("canStartWithSay");
			}
			if (jo.has("visibleToDegree")) {
				vis = jo.getInt("visibleToDegree");
			}
			if (jo.has("visibleTo") && !jo.isNull("visibleTo")) {
				JSONArray avis = jo.getJSONArray("visibleTo");
				if (avis != null) {
					visible_to = new ArrayList<String>();
					for (int i = 0; i < avis.length(); i++) {
						String visibleto = avis.get(i).toString();
						visible_to.add(visibleto);
						p(id + ":" + name + " is visible to: " + visibleto);
					}
				}

			}
			if (jo.has("commands") && !jo.get("commands").toString().equalsIgnoreCase("null")) {
				states = State
						.fromJsonArray(jo.getJSONObject("commands"), port);
			}
			if (jo.has("settings")) {

				Object osettings = jo.get("settings");
				port.jsettings = "" + osettings;
				if (osettings != null
						&& !port.jsettings.equalsIgnoreCase("null")) {
					// p("====================  about to parse state "+statename+"  data: \n"+state.data);
					if (osettings instanceof JSONObject) {

						JSONObject settingsobject = jo
								.getJSONObject("settings");
						port.settings = InputData.fromJsonArray(settingsobject,
								null);
						p(port.getName() + " settings:\n" + port.settings);
					} else {
						p("Data is not JSONObject: " + osettings);
					}
				}

			}
			port.exposedBy = exposed;
			port.creator = creator;
			port.states = states;
			port.description = description;
			port.visible_to = visible_to;
			port.setVisibilityLevel(vis);
			port.setCanStartWithSay(canStartWithSay);
			// p("User "+user.getName()+": got Port: " + port);
			port.connectStates(states);
			return port;
		} catch (JSONException ex) {
			warn("Could not create port from " + jo + ":" + ex.getMessage());
			// ex.printStackTrace();
			return null;
		}
	}

	public boolean hasSettings() {
		return settings != null && settings.size() > 0;
	}

	public List<InputData> getSettings() {
		return settings;
	}

	public void connectStates(List<State> states) {
		p(getName() + ": Connnecting states");
		if (states != null) {
			for (int i = 0; i < states.size(); i++) {
				State s = states.get(i);
				if (s.getTransitions() != null) {
					for (String trans : s.getTransitions()) {
						State next = getState(trans);
						if (next != null){
							next.addPrev(s);
							s.addNext(next);
						}
						
					}
				}
			}
		}
		if (canStartWithSay || states == null || states.size() == 0) {
			State say = getState("Say");
			if (say == null || say.getName() == null) {
				p("creating say state");
				say = new State("Say");
				if (states == null) states = new ArrayList<State>();
				if (!states.contains(say)) states.add(say);
			}
		} else
			p("NOT adding state say");
	}

	public static Port fromJson(String json, User user) {
		Port port = null;
		try {
			JSONObject jo = new JSONObject(json);
			port = fromJsonObj(jo, user);
                        if (port != null) {
                            port.owner = user;
                        }

		} catch (JSONException ex) {
			warn("Could not create Port from " + json + ":" + ex.getMessage());
			ex.printStackTrace();
			return null;
		}
		// p("Got Port: " + port.toString());
		return port;
	}

	public List<State> getAllNextStates(State state) {
		return getNextStates(null, state, null, false);
	}
	public List<State> getNextStates(Flow flow, State state, User flowcreator, boolean isbot) {
		List<State> nextlist = state.getNextStates();
		
		ArrayList<State> res = new ArrayList<State>();
		if (nextlist != null) {
			for (State s: nextlist) {
				if (this.canExecuteState(flow, s, flowcreator)) {
					if (!res.contains(s)) res.add(s);
				}
			}
		}
		
		if (isbot && res.size() > 0) {
			p("bot, removing results state");
			// last one is result, we don't want to show this one to the user
			res.remove(res.size() - 1);

		}
		if (res.size() < 1 || isbot) {
			// also add first states if not already added
			List<State> first = this.getFirstStates(flowcreator);
			for (State f : first) {
				if (f != null && !res.contains(f) && f.getName() != null && f.getName().length()>0) {
					p("Got no states or bot, adding first state " + f);
					res.add(f);
				}
			}
		}
		if (!isbot) {
			// also add reminder soft state reminder if I have sent the last atom
			if (flow != null && flow.hasAtoms() && man.isMe(flow.getLastAtom().getSender())) {
				res.add(new State("Remind"));
			}
		}
		return res;
	}

	private static void p(String msg) {
	//	 log.info(msg);
	}
	private static void err(String msg) {
		 log.warning(msg);
	}

	private static void warn(String msg) {
		log.warning(msg);
	}

	public static void main(String args[]) {
	}

	public String getPreferenceKey(InputData data) {
		String key = getId() + "_" + data.getName();
		return key;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the message_transport_type
	 */
	public String getCreator() {
		return creator;
	}

	@Override
	public boolean equals(Object o) {
		return ((Port) o).id == id;
	}

	@Override
	public int hashCode() {
		return id;
	}

	public List<State> getStatesAfter(State st) {
		List<State> res = new ArrayList<State>();
		if (!st.hasStates()) {
			return null;
		}
		for (String tr : st.getTransitions()) {
			State s = this.getState(tr);
			if (s == null) {
				warn("Could not find state for trans " + tr);
			} else {
				res.add(s);
			}

		}
		return res;
	}

	/**
	 * @return the canStartWithSay
	 */
	public boolean isCanStartWithSay() {
		return canStartWithSay;
	}

	/**
	 * @param canStartWithSay
	 *            the canStartWithSay to set
	 */
	public void setCanStartWithSay(boolean canStartWithSay) {
		this.canStartWithSay = canStartWithSay;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public String getGrouping() {
		if (this.isTemplate()) {
			return "templates";
		} else if (this.getCreator() != null
				&& man.isMe(man.getUser(this.getCreator()))) {
			return "My ";
		} else
			return "Default ";
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isSimple() {
		return this.states == null || states.size() < 1;
	}

	public void setCreator(String creator) {
		this.creator = creator;

	}
	public void copyValuesTo(Port p) {
		p.canStartWithSay = canStartWithSay;
		p.description = description;
		p.creator = creator;
		p.labels = labels;
		p.states = states;
		p.type = type;
		p.visible_to = visible_to;
		p.settings = settings;
		p.exposedBy = exposedBy;
		p.name = name;
		p.owner = owner;
	}
	public Port copy() {
		Port p = new Port(id, name);
		copyValuesTo(p);
		return p;
	}
	private Contact getOwner() {
		return owner;
	}
	private void setOwner(User owner) {
		this.owner = owner;
	}
	public boolean isExposedBy(User user) {
		return (isExposedToAll() && !user.isSystem()) || isExposedBy(user.getId()) || getCreator().equals(user.getId());
	}
	public boolean isExposedToAll() {
		return exposedBy != null && exposedBy.contains("all");
	}
	public boolean isExposedBy(String userid) {
		return exposedBy != null && exposedBy.contains(userid);
	}
	public String getExposedBy() {
		return exposedBy;
	}
	public void setName(String name) {
		this.name = name;
		
	}

}