/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.model;

import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public class DataValue implements Serializable {

	private static final Logger log = Logger.getLogger(DataValue.class
			.getName());

	private String name;
	private DataType dataType;
	String value;
	private Object ovalue;

	/** recursion? */
	private List<DataValue> datavalues;

	private static HashMap<String, DataValue> groupmap;

	public DataValue(String name) {
		this.name = name;
		dataType = DataType.Text;
	}

	public DataValue(String name, String value) {
		this.name = name;
		this.value = value;
		dataType = DataType.Text;
	}
	
	public static DataValue  createDate(String name, Date d) { 
		DataValue dv = new DataValue(name, null);
		dv.dataType = DataType.Time;
		dv.setValue(d);
		return dv;
	}
	public static DataValue  createPrice(String name, double v) { 
		DataValue dv = new DataValue(name, null);
		dv.dataType = DataType.Price;
		dv.setValue(""+v);
		return dv;
	}
        public static DataValue  create(String name, String value) { 
		DataValue dv = new DataValue(name, value);
		return dv;
	}
	public static DataValue  createQuantity(String name, double v) { 
		DataValue dv = new DataValue(name, null);
		dv.dataType = DataType.Quantity;
		dv.setValue(""+v);
		return dv;
	}

	
	public String getValue() {
		return value;
	}

	public void setValue(String val) {
		this.value = val;
	}
	
	public void setDateValue(long  t) {
		setValue(new Date(t));
	}
	public void setValue(Date d) {
		// convert to  selectedDay + "/"
			//	+ (selectedMonth + 1) + "/" + selectedYear;
		this.value = d.getDay()+"/"+d.getMonth()+"/"+(d.getYear()+1900);
		
	}

	@Override
	public DataValue clone() {
		DataValue c = new DataValue(this.name);
		c.value = this.value;
		c.dataType = this.dataType;
		c.ovalue = this.ovalue;
		return c;
	}

	public List<DataValue> getSubValues() {
		return datavalues;
	}

	public void add(DataValue dv) {
		if (datavalues == null)
			datavalues = new ArrayList<DataValue>();
		datavalues.add(dv);
	}

	/** ct is the content of the value, the part after "value": */
	public static List<DataValue> fromJsonArray(Object partaftervalue,
			InputData parentdata) {
		if (parentdata == null) {
			parentdata = new InputData("data");
		}
		groupmap = new HashMap<String, DataValue>();
		ArrayList<DataValue> datas = new ArrayList<DataValue>();

		if (partaftervalue instanceof JSONArray) {
			// p("========== DataValue: value is array: " + partaftervalue);
			JSONArray proclist = (JSONArray) partaftervalue;
			for (int i = 0; i < proclist.length(); i++) {
				try {
					Object ovalue = proclist.get(i);
					DataValue data = fromJsonObj(ovalue, parentdata);
					if (data.value != null && groupmap.containsKey(data.value)) {
						data = groupmap.get(data.value);
					}
					if (data != null && !datas.contains(data)) {
						datas.add(data);
					}
				} catch (Exception e) {
					e.printStackTrace();
					err("Could not process json: " + partaftervalue.toString()
							+ "\nbecause:" + e.getMessage());
					System.exit(0);
				}
			}
		} else if (partaftervalue instanceof JSONObject) {
			// p("========== DataValue: value is JSONObject: " +
			// partaftervalue);
			try {
				Object ovalue = (JSONObject) partaftervalue;
				DataValue data = fromJsonObj(ovalue, parentdata);
				if (data.value != null && groupmap.containsKey(data.value)) {
					data = groupmap.get(data.value);
				}
				if (data != null && !datas.contains(data)) {
					datas.add(data);
				}
			} catch (Exception e) {
				e.printStackTrace();
				err("Could not process json: " + partaftervalue.toString()
						+ "\nbecause:" + e.getMessage());
				System.exit(0);
			}
		} else if (partaftervalue instanceof String) {
			// p("========== DataValue: value is String: " + partaftervalue);
			DataValue dv = new DataValue(parentdata.getName());
			dv.setValue((String) partaftervalue);
			if (!datas.contains(dv)) {
				datas.add(dv);
			}
		}
		groupmap = null;
		// p("============= ALL DATA VALUES FOR INPUT "+partaftervalue+":\n"+datas);
		return datas;
	}

	public DataValue getSubValue(String value) {
		if (value == null)
			return null;
		if (this.datavalues != null) {
			for (DataValue dv : datavalues) {
				if (dv.getValue() != null
						&& dv.getValue().equalsIgnoreCase(value)) {
					return dv;
				}
			}
		}
		return null;
	}

	/*
	 * "YardWork.Landscaping"
	 */
	public static DataValue fromJsonObj(Object jdatavalue, InputData parentdata) {
		DataValue dvalue = new DataValue(parentdata.getName());

		if (jdatavalue == null) {
			return dvalue;
		}
		try {
			if (jdatavalue instanceof JSONObject) {
				// an object
				p("Data value is JSONObject: " + jdatavalue);
				JSONObject jvalueobject = (JSONObject) jdatavalue;
				Iterator<String> subkeys = jvalueobject.keys();
				String subdataname = subkeys.next();
				DataValue subdatavalue = new DataValue(subdataname);
				dvalue.add(subdatavalue);

			} else if (jdatavalue instanceof String) {
				String value = jdatavalue.toString();
				p("Got string " + value);
				dvalue = dvalue.parseStringValue(value, "");
				// p("Whole string value data structure:\n"+dvalue);
				groupmap.put(dvalue.value, dvalue);
			} else if (jdatavalue instanceof Number) {
				Number value = (Number) jdatavalue;
				dvalue.value = value.toString();
			} else {
				p("jdatavalue is not a string, not a list and not an object: "
						+ jdatavalue.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			err("Could not process jdataobject: " + jdatavalue.toString()
					+ "\nbecause:" + e.getMessage());
			System.exit(0);
		}
		return dvalue;

	}

	public DataValue parseStringValue(String valuewithdot, String sp) {
		// out(sp+"Processing "+valuewithdot);
		int dot = valuewithdot.indexOf(".");
		DataValue result = this;
		if (dot > -1) {
			String group = valuewithdot.substring(0, dot);
			// check if we already have
			if (groupmap.containsKey(group)) {
				// out(sp+"We already did group "+group+" reusing it");
				result = groupmap.get(group);
			}
			// else out(sp+"We did NOT do "+group+" yet");
			groupmap.put(group, result);
			String right = valuewithdot.substring(dot + 1);
			// p("Got dot, need to recurse, group="+group+", rightpart="+right);
			result.value = group;
			DataValue child = new DataValue(this.name);
			child = child.parseStringValue(right, sp + "      ");
			groupmap.put(child.value, child);
			result.add(child);

		} else {
			value = valuewithdot;

		}
		return result;
	}

	private void out(String s) {
		System.out.println(s);
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return toString("");
	}

	public String toString(String sp) {

		String s = sp + name + "=";
		if (value == null)
			s += "";
		else if (value.length() < 200)
			s += value;
		else
			s += value.substring(0, 50) + "...";
		if (this.datavalues != null) {
			for (DataValue dv : datavalues) {
				s += dv.toString(sp + "    ");
			}
		}
		return s;
	}

	public String toJson() {
		String j = "\"" + this.getName() + "\": " + q(value);
		return j;
	}

	public int hashCode() {
		if (value == null)
			return name.hashCode();
		return (name + "." + value).hashCode();
	}

	public boolean equals(Object o) {
		if (!(o instanceof DataValue))
			return false;
		DataValue d = (DataValue) o;
		if (value == null)
			return d.value == null && d.name.equals(name);
		else if (d.value == null)
			return value == null && d.name.equals(name);
		else {
			return (d.name + "." + d.value).equals((name + "." + value));
		}
	}

	private static void warn(String msg) {
		log.warning(msg);
	}

	private static void err(String msg) {
		log.severe(msg);
	}

	private static void p(String msg) {
		// log.info(msg);
	}

	/**
	 * turn String into a json key with quotes and column :
	 */
	private String key(String s) {
		return q(s) + ": ";
	}

	/**
	 * quote string for json
	 */
	private String q(String s) {
		return "\"" + s + "\"";
	}

	/**
	 * add json {} parenthesis
	 */
	private String par(String s) {
		return "{ " + s + "} ";
	}

	/**
	 * add json [] parenthesis
	 */
	private String l(String s) {
		return "[ " + s + "]";
	}

	/**
	 * @return the ovalue
	 */
	public Object getOvalue() {
		return ovalue;
	}

	/**
	 * @param ovalue
	 *            the ovalue to set
	 */
	public void setOvalue(Object ovalue) {
		this.ovalue = ovalue;
	}

}
