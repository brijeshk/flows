/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.auto.db.SqlLiteHelper;
import com.auto.net.ChatConnector;
import com.auto.net.ChatServiceListener;
import com.auto.net.FlowChatListener;
import com.auto.net.FlowChatService;
import com.auto.net.WebService;
import com.auto.utils.FileToolsIF;
import com.auto.utils.ThreadListener;

/**
 * Manage the entire data model, puts it together given the individual pieces
 *
 * @author Chantal
 */
public class FlowManager implements Serializable, ChatServiceListener {

	private static final Logger log = Logger.getLogger(FlowManager.class
			.getName());

	private List<FlowChatListener> chatlisteners;

	private FlowChatService chatservice;

	private User me;
	private List<User> users;
	private List<Group> groups;
	private List<Port> ports;
	private List<Flow> flows;
	private List<Atom> atoms;

	private static SqlLiteHelper dbhelper;
	private static WebService serv;
	private static FileToolsIF filetools;
	private static FlowManager manager;

	public static FlowManager getManager() {
		if (manager == null) {
			// p("========== NEED TO CREATE MANAGER WITH DB HELPER FIRST ===== ");
		}
		return manager;
	}

	public SqlLiteHelper getDbHelper() {
		return dbhelper;
	}

	public static FlowManager getManager(SqlLiteHelper db, FileToolsIF ft) {
		dbhelper = db;
		filetools = ft;
		if (manager == null) {
			manager = new FlowManager();
		}

		return manager;
	}

	public WebService getWebService() {
		return serv;
	}

	/**
	 * helper depends on context, swing or android
	 */
	private FlowManager() {

		users = new ArrayList<User>();
		flows = new ArrayList<Flow>();
		groups = new ArrayList<Group>();
		ports = new ArrayList<Port>();
		atoms = new ArrayList<Atom>();
		chatlisteners = new ArrayList<FlowChatListener>();
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	private static void p(String msg) {
		log.info(msg);
	}

	private static void warn(String msg) {
		log.warning(msg);
	}

	public void addChatListener(FlowChatListener listener) {
		if (!chatlisteners.contains(listener)) {
			this.chatlisteners.add(listener);
		}
	}

	public List<Port> getCommonTopics(List<Contact> contacts) {
		if (contacts == null || contacts.size() < 1) {
			return null;
		}
		List<User> users = User.converToUsers(contacts);
		return getCommonUserTopics(users);
	}

	/**
	 * return the ports that are common to all users
	 */
	public List<Port> getCommonUserTopics(List<User> users) {
		if (users == null || users.size() < 1) {
			return null;
		}
		users.remove(me);
		if (users.size() == 1) {
			return (users.get(0)).getAllPossiblePorts();
		} else if (users.size() < 1)
			return null;

		User first = users.get(0);
		List<Port> allports = first.getAllPossiblePorts();

		List<Port> commonports = new ArrayList<Port>();
		for (int p = 0; p < allports.size(); p++) {
			Port port = allports.get(p);
			int port_id = port.getId();
			boolean allhaveit = true;
			for (int u = 1; u < users.size(); u++) {
				User user = (User) users.get(u);

				if (user.getPort(port_id) == null) {
					allhaveit = false;
					break;
				}

			}
			if (allhaveit) {
				commonports.add(port);
			}
		}
		return commonports;

	}

	public void send(Atom atom) throws Exception {
		if (chatservice == null) {
			p("Cannot send atom, no chat service");
		}
		chatservice.send(atom.getFlow(), atom);

	}

	public Port getPort(int id) {
		return getPort(id, false);
	}

	public Port getPortByName(String name) {
		for (Port p : ports) {
			if (p.getName().equalsIgnoreCase(name)) {
				return p;
			}
		}

		return null;
	}

	public Port getPort(int id, boolean warn) {
		for (Port p : ports) {
			if (p.getId() == id) {
				return p;
			}
		}
		if (warn) {
			warn("Found no port with id " + id + ", got ids: ");

			for (Port p : ports) {
				System.out.print(p.getId() + ", ");
			}
			System.out.println();
			Exception e = new Exception();
			e.printStackTrace();
		}
		return null;
	}

	public int getNextFlowNr() {
		int max = 0;
		for (Flow f : flows) {
			if (f.getId() > max) {
				max = f.getId();
			}
		}
		return max + 1;
	}

	public User getMe() {
		if (me.getPassword() == null || me.getPorts().size() < 1) {
			User u = getUser(me.getId());
			if (u != null) {
				me = u;
			}
		}
		return me;
	}

	public Port createNewPortLabel(String name) {
		// get next id for port

		int maxid = 0;
		for (Port p : ports) {
			if (p.getId() > maxid) {
				maxid = p.getId();
			}
		}
		maxid++;
		// NEGATIVE ID INDICATES NEW ONE. NEEDS TO BE SAVED LATER
		Port port = new Port(-maxid, name);
		port.setType("Labels");
		port.setCreator(me.getId());
		p("Creating and saving new port " + port);
		ports.add(port);
		port.connectStates(null);
		Port resport = serv.create(port);
		return resport;
	}

	public User create(User user) {

		if (serv.create(user)) {
			users.add(user);
			return user;
		}
		return null;
	}

	public void createNewPort(String name, boolean save,
			final ThreadListener listener) {
		// id must be created on the server
		final Port port = new Port(-1, name);
		port.setCreator(me.getId());
		port.setType("default");
		p("Creating and saving new port " + port);
		port.connectStates(null);

		if (save) {
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					Port resport = serv.create(port);
					me.add(resport);
					ports.add(resport);
					listener.threadDone(resport);

				}
			});
			t.start();
		}

	}

	public User getUser(String id) {
		if (users == null || users.size() < 1) {
			//p("Got no users yet");
			return null;
		} else if (id == null || id.length() < 1) {
			return null;
		}

		for (User u : users) {
			if (u.getId().equalsIgnoreCase(id)
					|| (u.getName() != null && u.getName().equalsIgnoreCase(id))
					|| (u.getPhone() != null && u.getPhone().equalsIgnoreCase(
							id))
					|| (u.getEmail() != null && u.getEmail().equalsIgnoreCase(
							id))) {
				return u;
			}
		}

		warn("Found no user with id or email " + id);
		for (User u : users) {
			p(u.getId() + ", name=" + u.getName() + " email=" + u.getEmail());
		}
		Exception e = new Exception();
		e.printStackTrace();

		return null;
	}

	private void addSystemIfMissing() {
		if (users == null)
			return;
		for (User u : users) {
			if (u.getId().equalsIgnoreCase("system"))
				return;
		}
		User sys = new User("system");
		sys.setName("system");
		sys.setEmail("system");
		users.add(sys);
	}

	public User getUserByName(String name) {
		p("Finding user " + name);
		for (User u : users) {
			if (u.getName() != null && u.getName().equalsIgnoreCase(name)) {
				return u;
			}
		}
		warn("Found no user with name " + name);

		return null;
	}

	public Group getGroup(String id) {
		if (groups == null) {
			p("Got no Group yet");
			return null;
		}
		for (Group u : groups) {
			if (u.getId() == id) {
				return u;
			}
		}
		warn("Found no group with id  " + id + ", groups are: ");
		for (Group u : groups) {
			p(u.getId() + ": " + u.getName());
		}

		return null;
	}

	public Group getGroupByName(String name) {
		for (Group u : groups) {
			if (u.getName().equalsIgnoreCase(name)) {
				return u;
			}
		}
		warn("Found no Group with name " + name);

		return null;
	}

	private void loadFlows() {
		p("==================  load flows =================");
		// get flows - AFTER we have the users and ports
		if (users == null) {
			warn("loadFlows: Need to have users first");
			return;
		}
		if (ports == null) {
			warn("loadFlows: Need to have ports first");
			return;
		}

		List<FlowData> dflows = dbhelper.getFlows();

		flows = new ArrayList<Flow>();
		if (dflows != null) {
			for (FlowData df : dflows) {
				if (df != null) {
					Flow f = new Flow(df);
					// check if flow is ok
					if (f.getPort() == null || f.getState() == null
							|| f.getUsers() == null || f.getCreator() == null
							|| f.getId() == 0) {
						p("deleting incomplete flow: " + f);
						dbhelper.delete(f);
					} else {
						if (!flows.contains(f)) {
							flows.add(f);
						}
					}
				} else {
					warn("FlowData is null: " + df);
				}
				// p("Got flow with users: " + f.getAtomUsers());
			}
		}
		p("=========== end of load flows= ");
	}

	public Flow addCreatedFlow(FlowData fd, boolean withAtom) {
		return addCreatedFlow(fd, withAtom, true);
	}

	public Flow addCreatedFlow(FlowData fd, boolean withAtom, boolean updateDb) {

		// check if flow is fine, are there users, and are there
		Flow f = new Flow(fd);
		
		if (f.getUsers() == null || f.getPort() == null) {
			p("Flow has no users or no port, not inserting it");
			return null;
		}
		p("addCreatedFlow: =========================================== INSERTING FLOW\n"
				+ fd.getInsertString());
		
		flows.add(f);
		if (updateDb)
			dbhelper.insert(fd);
		String statename = null;
		if (fd.getProtocol() >= 0) {
			f.setPort(getPort(fd.getProtocol()));
			f.setModified(System.currentTimeMillis());
			statename = fd.getState();
			if (f.getPort() != null) {
				f.setState(f.getPort().getState(statename));
			}
			p("addCreatedFlow: port is " + f.getPort() + ", state is: "
					+ statename);
		}
		if (withAtom) {
			AtomData ad = new AtomData();
			ad.setBody(fd.getData());
			ad.setCreated(System.currentTimeMillis());
			ad.setSender(fd.getCreator());
			if (statename != null) {
				ad.setState(statename);
			}
			ad.setPort_id(f.getPort().getId());
			ad.setFlow_id(fd.getId());
			ad.setCrud_flag("c");
			ad.setData(fd.getData());
			this.addCreatedAtom(f, ad);
			p("Created flowdata " + fd + "with atomdata " + ad + ", f=" + f
					+ ", atomstate=" + ad.getState());
		}
		return f;
	}

	public void setRead(Atom atom) {
		atom.setRead(true);
		atom.setCrud("u");
		update(atom, false);
	}

	public void setUnRead(Atom atom) {
		atom.setRead(false);
		atom.setCrud("u");
		update(atom, false);
	}

	public void updateRemote(Atom atom, String crud) {
		atom.setCrud(crud);
		chatservice.send(atom.getFlow(), atom);
	}

	public void updateRemote(Flow flow, String crud) {
		flow.setCrud(crud);
		chatservice.send(flow);
	}

	public void update(Atom atom, boolean remote) {
		String crud = atom.getCrud();
		if (crud.equalsIgnoreCase("d")) {
			// p("Deleting atom "+atom);
			dbhelper.delete(atom.getAtomData());
			atoms.remove(atom);

		} else if (crud.equalsIgnoreCase("u")) {
			// p("Updating atom "+atom);
			dbhelper.update(atom.getAtomData());
		} else if (crud.equalsIgnoreCase("c")) {
			// p("Creating atom "+atom);
			dbhelper.insert(atom.getAtomData());
		} else if (crud != null && !crud.equals("")) {
			warn("Don't know what to do with crud " + crud + " for atom "
					+ atom);
		}
		if (remote)
			this.updateRemote(atom, crud);
	}

	public void update(AtomData atom) {
		String crud = atom.getCrud_flag();
		if (crud.equalsIgnoreCase("d")) {
			// p("Deleting atom "+atom);
			dbhelper.delete(atom);

		} else if (crud.equalsIgnoreCase("u")) {
			// p("Updating atom "+atom);
			dbhelper.update(atom);
		} else if (crud.equalsIgnoreCase("c")) {
			// p("Creating atom "+atom);
			dbhelper.insert(atom);
		} else if (crud != null && !crud.equals("")) {
			warn("Don't know what to do with crud " + crud + " for atom "
					+ atom);
		}

	}

	public void update(Flow flow, boolean remote) {
		String crud = flow.getCrud();
		if (crud == null)
			crud = "u";
		// p("===================== UPDATE FLOW ================");
		flow.setModified(System.currentTimeMillis());
		if (crud.equalsIgnoreCase("d")) {
			// p("update: Deleting flow "+flow);
			dbhelper.delete(flow);
			flows.remove(flow);
		} else if (crud.equalsIgnoreCase("u")) {
			// p("update: Updating flow "+flow);
			Flow existingflow = this.getFlow(flow.getId());
			if (existingflow != null) {
				try {
					dbhelper.update(flow.getFlowData());
					for (Atom a : flow.getAtoms()) {
						update(a, remote);
					}
				} catch (Exception e) {
					e.printStackTrace();

				}
			} else {
				flow.setCrud("c");
				update(flow, remote);
			}
		} else if (crud.equalsIgnoreCase("c")) {
			// p("update: Creating flow "+flow);
			// first check if it exists
			Flow existingflow = this.getFlow(flow.getId());
			if (existingflow != null) {
				flow.setCrud("u");
				update(flow, remote);
			} else {
				dbhelper.insert(flow.getFlowData());
				for (Atom a : flow.getAtoms()) {
					update(a, remote);
				}
			}
		} else {
			warn("Don't know what to do with crud " + crud + " for flow "
					+ flow);
		}
		if (remote)
			this.updateRemote(flow, crud);
	}

	public Atom addCreatedAtom(Flow flow, AtomData ad) {

		ad.setFlow_id(flow.getId());
		Atom a = new Atom(ad);

		if (a.getState() != null) {
			flow.setState(a.getState());
		} else {
			p("Atom has no state");
			a.setState(flow.getState());
		}
		if (atoms == null)
			atoms = new ArrayList<Atom>();
		// what to do with atoms that have no user or no state?
		if (a.getReceivers() == null ) {
			p("Atom has no (known) receivers, not inserting it");
			return null;
		} else {
			p("===================================== INSERTING ATOM " + ad
					+ " INTO DB");

			dbhelper.insert(ad);
			atoms.add(a);

			flow.addAtom(a);
		}
		// p("Created atomdata: " + ad);
		// p("Atom is: " + a);
		// p("Flow is now: "+flow);
		return a;
	}

	private void loadAtoms() {
		p("========= load atoms from db");
		// get flows - AFTER we have the users and ports
		if (users == null) {
			warn("loadAtoms: Need to have users first");
			return;
		}
		if (ports == null) {
			warn("loadAtoms: Need to have ports first");
			return;
		}

		List<AtomData> datoms = dbhelper.getAtoms();

		atoms = new ArrayList<Atom>();
		if (datoms != null) {
			for (AtomData da : datoms) {
				Atom a = new Atom(da);
				if (!atoms.contains(a)) {
					// p("loadAtoms: Found atom fid="+a.getFlow_id()+": aid=" +
					// a.getId() + ":" + a.toString());
					Flow flow = this.getFlow(a.getFlow_id());
					if (a.getState() == null) {
						warn("loadAtoms:atom  has no state:" + a
								+ ", deleting it");
						dbhelper.delete(da);
					} else if (flow == null) {
						warn("loadAtoms:Could not find flow " + a.getFlow_id()
								+ " for atom " + a + ", deleting it");
						dbhelper.delete(da);
					} else {
						atoms.add(a);
						flow.addAtom(a);
						if (flow.getPort() != null) {
							a.setState(flow.getPort().getState(da.getState()));
						}
					}

				}
				// p("Got atoms: " + a);
			}
		}
		if (this.getFlows() != null) {
			for (Flow f : this.getFlows()) {
				if (!f.hasAtoms() || f.getPort() == null
						|| f.getState() == null || f.getUsers() == null
						|| f.getCreator() == null) {
					p("deleting incomplete flow: " + f);
					dbhelper.delete(f);
				}
			}
		}
		if (this.getFlows() != null) {
			for (Flow f : this.getFlows()) {
				p("Got flow: " + f.toJson(""));
			}
		}
	}

	public void loadCachedWebData() {
		p("========== LOADING CACHED DATA ========");
		users = serv.getCachedUsers();
		addSystemIfMissing();
		ports = serv.getCachedPorts();
		if (ports == null) {
			ports = new ArrayList();
			p("No cached ports yet");
		}
		if (users != null) {
			for (User user : users) {
				List<Port> uports = serv.getCachedPorts(user);
				if (uports != null) {
					ports.addAll(uports);
					user.setPorts(uports);
				}
			}
		} else {
			p("Got NO cached users yet");
		}
		groups = serv.getCachedGroups();
		// for v2
		// if (groups != null) {
		// for (Group user : groups) {
		// List<Port> uports = serv.getCachedPorts(user);
		// if (uports != null) {
		// ports.addAll(uports);
		// user.setPorts(uports);
		// }
		// }
		// } else {
		// p("Got NO cached groups yet");
		// }
		// p("========== now load actual data via thread");
		Thread t = new Thread(new LoadWebData());
		t.start();
	}

	public void loadWebData() {

		p("=============== LOADING DATA FROM SERVER =========");
		users = serv.getUsers();
		addSystemIfMissing();
		ports = new ArrayList<Port>();
		List<Port> myports = serv.getPorts(me);
		me.setPorts(myports);
		p("My gates are: " + myports);
		List<Port> allports = serv.getPorts();
		if (users == null) {
			warn("Could not connect to server");
			return;
		}
		for (User user : users) {
			// p("user "+user.getName());
			if (allports != null) {
				for (Port uport : allports) {
					Port port = this.getPort(uport.getId(), false);
					if (port == null) {
						port = uport;
						ports.add(port);
					} else {
						ports.remove(port);
						ports.add(uport);
						port = uport;

					}

					if (port.isExposedBy(user)) {
						port = user.getPort(uport.getId());
						if (port == null) {
							port = uport;
							user.add(port);
						}
						// p("   exposes port "+port.getName()+", id="+port.getId());
					}
				}
			}
		}
		p("====  loading groups now");
		List<Group> loadedgroups = serv.getGroups();
		if (loadedgroups == null) {
			warn("Could not connect to server to load groups or got no groups");
			return;
		}
		if (groups == null) {
			groups = new ArrayList<Group>();
		}
		for (Group loadeduser : loadedgroups) {
			if (loadeduser == null) {
				warn("Loaded Group is null should not be possible");
				continue;
			}

			Group user = this.getGroup(loadeduser.getId());
			if (user == null) {
				user = loadeduser;
				groups.add(user);
			} else {
				groups.remove(user);
				groups.add(loadeduser);
				user = loadeduser;
			}
		}

	}

	public void loadFlowDelta() {
		p("======== GETTING FLOW DELTA =======");
		// get last synched time. this is a string

		List<Flow> flowdeltalist = serv.getFlowDelta(this.chatservice);
		if (flowdeltalist == null) {
			p("Got NO flowdeltalist");
		} else {
			// p("Got flowdeltalist ");
			// for (Flow flow: flowdeltalist) {
			// p("got flowdelta: "+flow);
			//
			// }
		}
		// p("===== end loadWebData ========== ");
	}

	public List<Port> getAllPorts() {
		return ports;
	}

	public User getUserByEmail(String from) {
		for (User u : users) {
			if (u.getEmail().equalsIgnoreCase(from)) {
				return u;
			}
		}
		// warn("Found no user with email " + from);

		return null;
	}

	public void addUser(User u) {
		if (!users.contains(u)) {
			users.add(u);
		}
	}

	public void logout() {
		if (chatservice != null) {
			this.chatservice.disconnect();
		}
	}

	public void setMe(User me) {
		this.me = me;
	}

	public void setPorts(List<Port> ports) {
		this.ports = ports;
	}

	public boolean hasUsers() {
		return users != null && users.size() > 0;
	}

	private class LoadWebData implements Runnable {

		@Override
		public void run() {
			loadWebData();
			loadFlowDelta();
		}
	}

	public void loadData() {
		p("loadData");
		// load this in a separate THREAD
		loadCachedWebData();
		loadFlows();
		loadAtoms();
		// now link flows with atoms

		// now CACHE THE DATA
	}

	public Flow getFlow(int flow_id) {
		if (flow_id < 1) {
			return null;
		}

		if (flows == null) {
			p("getFlowByUuid: got no flows yet");
			return null;
		}
		for (Flow f : flows) {
			if (f.getId() == flow_id) {
				return f;
			}
		}
		warn("Could not find flow with id " + flow_id);
		Exception e = new Exception("Could not find flow with id " + flow_id);
		e.printStackTrace();
		return null;
	}

	public Flow getFlowByUuid(String uuid) {
		if (flows == null) {
			p("getFlowByUuid: got no flows yet");
			return null;
		}
		for (Flow f : flows) {
			if (f.getUuid() != null && f.getUuid().equalsIgnoreCase(uuid)) {
				return f;
			}
		}
		// warn("Could not find flow with uuid " + uuid);
		return null;
	}

	public List<Flow> getFlows() {
		return flows;
	}

	public int login(User me) {
		this.me = me;
		serv = new WebService(me, filetools);
		int code = serv.login();
		return code;
	}

	public int login(String name, String pw) {
		me = new User(name);
		me.setName(name);
		me.setPassword(pw);
		serv = new WebService(me, filetools);
		int code = serv.login();
		return code;

	}

	public boolean loginService(ChatConnector connector) throws Exception {
		p("===========  Creating FlowChatService, chat listener");
		chatservice = new FlowChatService(connector, me, filetools,
				new FlowChatListener() {
					@Override
					public void receive(String crud, List<Flow> flows) {
						for (FlowChatListener list : chatlisteners) {
							list.receive(crud, flows);
						}
					}
				});
		boolean ok = chatservice.connect();
		if (!ok) {
			p("Not connected");
			return false;
		} else {
			p("Logged in via connector " + connector);
			return true;
		}
	}

	public boolean isMe(User user) {
		if (user == null) {
			warn("isme: user is null");
			return false;
		}
		return (user.getEmail().length() > 0 && user.getEmail()
				.equalsIgnoreCase(me.getEmail()))
				|| user.getId().equalsIgnoreCase(me.getId());
	}

	public boolean hasContacts() {
		return (users != null && users.size() > 0)
				|| (groups != null && groups.size() > 0);
	}

	public boolean hasRealContacts() {
		if (users != null) {
			for (User u : users) {
				if (!u.isSystem()) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean hasFlows() {
		return (flows != null && flows.size() > 0);
	}

	public boolean hasPorts() {
		return (ports != null && ports.size() > 0);
	}

	public boolean userHasPorts() {
		return (me != null && me.getPorts() != null && me.getPorts().size() > 0);
	}

	public List<Contact> getContacts() {
		List<Contact> list = new ArrayList<Contact>();
		if (groups != null) {
			list.addAll(groups);
		}
		if (users != null) {
			list.addAll(users);
		}
		return list;
	}

	public List<User> getUsers() {
		return users;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void delete(Flow flow, boolean remote) {
		flow.setCrud("d");
		update(flow, remote);
	}

	public void delete(Atom atom, boolean remote) {
		atom.setCrud("d");
		update(atom, remote);
	}

	public void delete(AtomData atom, boolean remote) {
		atom.setCrud_flag("d");
		update(atom);
	}

	public void delete(Port port) {
		this.ports.remove(port);
		this.serv.delete(port);

	}

	public void update(Port port) {
		if (port.getId() < 0) {
			p("NEW port with negative id, will save instead");
			serv.update(port);
		} else {
			serv.update(port);
		}
	}

	public boolean updateMe() {

		return serv.update(me);

	}

	public boolean update(User user) {
		if (user.getId() == null || user.getId().length() < 1) {
			p("NEW user with NO id, will save instead");
			return serv.update(user);
		} else {
			return serv.update(user);
		}
	}

	public void remove(Contact contact) {
		if (contact instanceof User) {
			remove((User) contact);
		} else {
			remove((Group) contact);
		}
	}

	public void remove(User u) {
		users.remove(u);

	}

	public void remove(Group g) {
		groups.remove(g);
	}

	/**
	 * removes all people contacts
	 */
	public void deleteAllPeopleContacts() {
		List<User> list = new ArrayList<User>();
		list.addAll(users);
		for (User u : list) {
			if (!u.isSystem()) {
				users.remove(u);
			}
		}
		groups.clear();

	}

	public void deleteAllFlows(boolean remote) {
		ArrayList<Flow> todelete = new ArrayList<Flow>();
		todelete.addAll(flows);
		for (Flow f : todelete) {
			this.delete(f, remote);
		}
	}

	@Override
	public void processFlowMessages(List<Flow> flows) {
		p("===============  processFlowMessages, calling listeners");
		for (FlowChatListener list : chatlisteners) {
			p("=====================calling FLowChatListener :"
					+ list.getClass().getName());
			list.receive("u", flows);
		}

	}

}
