/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public class JSON {

    public static String get(String key, String json) {
        JSONObject j;
        try {
            j = new JSONObject(json);
            if (j.has(key)) {
                return j.getString(key);
            } else {
                return null;
            }
        } catch (JSONException ex) {
            Logger.getLogger(JSON.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
