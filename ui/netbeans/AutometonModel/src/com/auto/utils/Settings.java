package com.auto.utils;

import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;


public class Settings {
	 
	private static final Logger log = Logger.getLogger(Settings.class.getName());

	private static final String FILE = "settings.json";
	private JSONObject settings;
	private FileToolsIF ft;
	private static Settings thesettings;
	
	public static Settings getSettings(FileToolsIF ft) {
		if (thesettings == null)  thesettings = new Settings(ft);
		return thesettings;
	}
	
	private Settings(FileToolsIF ft) {
		this.ft = ft;
		String content = ft.getFileAsString(FILE);
		if (content != null && content.length()>0) {
			p("Loading settings: "+content);
			try {
				settings = new JSONObject(content);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		else p("Found no settings yet");
		if (settings == null) {
			settings = new JSONObject();
		}
	}
	public void set (String name, String val) {
		try {
			settings.put(name, val);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			err("Could not set "+name+"="+val+":"+e.getMessage());
		}
		ft.writeStringToFile(FILE, settings.toString());
	}
	public String get(String name) {
                if (!settings.has(name)) {
                    p("No property "+name+" yet");
                    return null;
                }
		try {
			return settings.getString(name);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			err("Could not get "+name+":"+e.getMessage());
			return null;
		}
	}
	
	private static void p(String msg) {
		log.info(msg);
	}

	private static void err(String msg) {
		log.severe(msg);
	}

	private static void warn(String msg) {
		log.warning(msg);
	}
}
