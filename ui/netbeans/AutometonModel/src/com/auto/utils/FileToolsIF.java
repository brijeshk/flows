package com.auto.utils;

import java.io.InputStream;

public interface FileToolsIF {

	public String getFileAsString(String filename);

	public String getFileAsString(InputStream is);

	public boolean writeStringToFile(String filename, String content);

}
