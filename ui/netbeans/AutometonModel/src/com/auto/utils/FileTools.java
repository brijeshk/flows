/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.utils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Chantal Roth
 */
public class FileTools implements FileToolsIF{

  
	private static FileTools ft;
	
	public static FileTools getFileTools() {
		if (ft == null) ft = new FileTools();
		return ft;
	}
	
    public static String addSlashOrBackslash(File fdir) {
        return addSlashOrBackslash(fdir.toString());
    }

    public static String addSlashOrBackslash(String dir) {
        if (dir == null) {
            //  err("addSlashOrBackslash: dir is null!");
            return "";
        }
        if (!dir.endsWith("/") && !dir.endsWith("\\")) {
            if (dir.indexOf("\\") > -1) {
                dir += "\\";
            } else {
                dir += "/";
            }
        }
        return dir;
    }

   

    /**
     * Returns the content of a file as a string.
     *
     */
    public String getFileAsString(String filename) {
    	 //	p("getFileAsString: File is "+filename);
        if (filename == null) {
            warn("Filename is null");
            return null;
        }
        File f = new File(filename);
        if (!f.exists()) {
            p("File "+f.getAbsolutePath()+" does not exist");
            return null;
        }
        try {
        	BufferedReader in  = new BufferedReader(new FileReader(filename));
        	return getFileAsString(in);
        } catch (Exception e) {
        	err(e);
        }
        return null;
    }
    /**
     * Returns the content of a file as a string.
     *
     */
    public String getFileAsString(InputStream is) {    
    	if (is == null) {
    		p("Got no input stream");
    		return null;
    	}
    	
        try {
        	BufferedReader in  = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        	return getFileAsString(in);
        } catch (Exception e) {
        	err(e);
        }
        return null;
    }
    /**
     * Returns the content of a file as a string.
     *
     */
    public static String getFileAsString(BufferedReader in) {       
    	
        StringBuilder res = new StringBuilder();

        try {           
            while (in.ready()) {
                // res.append((char)in.read());
                res.append(in.readLine());
                res.append("\n");
            }
            in.close();
            //		p("getFileAsString: Result is:\n"+res);
        } catch (Exception e) {
            err(e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception ex) {
                   err(ex);
                }
            }
        }

        return res.toString();
    }


  

    public static ArrayList<String> getFileAsArray(String filename) {
        //	p("getFileAsString: File is "+filename);
        if (filename == null || !(new File(filename)).exists()) {
            warn("Filename is null or does not exist");
            return null;
        }
        ArrayList<String> res = new ArrayList<String>();

        try {

            BufferedReader in = new BufferedReader(new FileReader(filename), 1024 * 1024);

            while (in.ready()) {
                res.add(in.readLine());
            }
            in.close();
            //		p("getFileAsString: Result is:\n"+res);
        } catch (FileNotFoundException e) {
            err(e);
        } catch (IOException e) {
            err(e);
        }
        return res;
    }

    public static ArrayList<String> getFileAsArray(URL filename) {
        //	p("getFileAsString: File is "+filename);
        if (filename == null) {
            warn("URL is null");
            return null;
        }
        ArrayList<String> res = new ArrayList<String>();

        try {
            URLConnection uc = filename.openConnection();
            InputStreamReader input = new InputStreamReader(uc.getInputStream());
            BufferedReader in = new BufferedReader(input);

            while (in.ready()) {
                // res.append((char)in.read());
                String line = in.readLine();
                if (line != null) {
                    line = line.trim();
                } else {
                    line = "";
                }
                res.add(line);

            }
            in.close();
            //		p("getFileAsString: Result is:\n"+res);
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
        return res;
    }

    public boolean writeStringToFile(String filename, String content) {
        return writeStringToFile(filename, content, false);
    }

    public boolean writeStringToFile(String filename, String content, boolean append) {
        PrintWriter fout = null;
        try {
            fout = new PrintWriter(new BufferedWriter(new FileWriter(filename, append)));
            fout.print(content);
            fout.flush();
            fout.close();
            return true;
        } catch (FileNotFoundException e) {
            err("File " + filename + " not found");
        } catch (IOException e) {
            err("IO Exception");
        } finally {
            if (fout != null) {
                fout.flush();
                fout.close();
            }
        }
        return false;
    }

    public static boolean writeStringToFile(File f, String content, boolean append) {
        PrintWriter fout = null;
        try {
            fout = new PrintWriter(new BufferedWriter(new FileWriter(f, append)));
            fout.print(content);
            fout.flush();
            fout.close();
            return true;
        } catch (FileNotFoundException e) {
            err("File " + f + " not found");
        } catch (IOException e) {
            err("IO Exception");
        } finally {
            if (fout != null) {
                fout.flush();
                fout.close();
            }
        }
        return false;
    }

    public static boolean writeStringToFile(File f, StringBuffer content) {
        PrintWriter fout = null;
        try {
            fout = new PrintWriter(new BufferedWriter(new FileWriter(f)));
            fout.print(content);
            fout.flush();
            fout.close();
            return true;
        } catch (FileNotFoundException e) {
            err("File " + f + " not found");
        } catch (IOException e) {
            err("IO Exception");
        } finally {
            if (fout != null) {
                fout.flush();
                fout.close();
            }
        }
        return false;
    }

    public static boolean writeArrayToFile(String filename, ArrayList<String> content) {
        PrintWriter fout = null;
        try {
            fout = new PrintWriter(new BufferedWriter(new FileWriter(filename)));
            for (int i = 0; i < content.size(); i++) {
                fout.println(content.get(i));
            }
            fout.flush();
            fout.close();
            return true;
        } catch (FileNotFoundException e) {
            System.out.println("File " + filename + " not found");
        } catch (IOException e) {
            System.out.println("IO Exception");
        } finally {
            if (fout != null) {
                fout.flush();
                fout.close();
            }
        }
        return false;
    }

    /**
     * ================== LOGGING =====================
     */
    private static void err(String msg, Exception ex) {
        Logger.getLogger(FileTools.class.getName()).log(Level.SEVERE, msg, ex);
    }

    private static void err(Exception ex) {
        Logger.getLogger(FileTools.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
    }

    private static void err(String msg) {
        Logger.getLogger(FileTools.class.getName()).log(Level.SEVERE, msg);
    }

    private static void warn(String msg) {
        Logger.getLogger(FileTools.class.getName()).log(Level.WARNING, msg);
    }

    private static void p(String msg) {
        //System.out.println("FileTools: " + msg);
        Logger.getLogger(FileTools.class.getName()).log(Level.INFO, msg);
    }

    public static boolean isUrl(String path) {
        if (path == null || path.length() < 1) {
            return false;
        }
        int pos = path.indexOf("://");
        if (pos > 0 && pos < 10) {
            return true;
        } else {
            return false;
        }
    }
}
