package com.auto.utils;

public interface ThreadListener {
	public void threadDone(Object result);
}
