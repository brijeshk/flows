package com.auto.net;

import com.auto.model.User;
import com.auto.utils.Base64;
import com.auto.utils.FileToolsIF;

import java.io.IOException;
import java.util.logging.Logger;


public abstract class AbstractChatService implements ChatServiceListener{

	private static final Logger log = Logger
			.getLogger(AbstractChatService.class.getName());

	protected User me;
	protected FlowChatListener listener;
	protected Exception lasterror;
	protected FileToolsIF filetools;
	
	protected ChatConnector connector;
 
        public AbstractChatService(ChatConnector connector, User user, FileToolsIF ft) {
		this.me = user;
		this.connector = connector;
		filetools = ft;
	}
	public AbstractChatService(ChatConnector connector, User user, FileToolsIF ft, FlowChatListener listener) {
		this.me = user;
		this.connector = connector;
		this.listener = listener;
		filetools = ft;
	}
	
        public void setFlowChatListener(FlowChatListener listener) {
            this.listener= listener;
        }
	public boolean  connect() {
		return connector.connect(me.getEmail(), me.getPassword());
	}
	public void disconnect() {
		connector.disconnect();
	}
	

	public String converToBase64(byte[] byteArrayImage) {
		String encoded = Base64.encodeBytes(byteArrayImage);
		return encoded;
	}

	public byte[] decodeFromBase64(String base64) {
		byte[] decoded = null;
		try {
			decoded = Base64.decode(base64);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return decoded;
	}

	protected void err(String s, Exception ex) {
		log.warning(s + ":" + ex.getMessage());
	}

	protected void p(String s) {
		log.info(this.me.getEmail() + ": >>>>>>>>>>>>>  " + s);
	}

	protected void err(String s) {
		log.severe(s);
	}

	protected void warn(String s) {
		log.warning(s);
	}

}
