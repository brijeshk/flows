package com.auto.net;

import com.auto.model.AtomData;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

public class WebSocketClient {
	private static Logger log = Logger.getLogger(WebService.class.getName());
	
	protected ChatServiceListener listener;
        private org.java_websocket.client.WebSocketClient cc;
	private String serverurl;
        
	public WebSocketClient(String serverurl) {
		this.serverurl = serverurl;
	}

	public void setChatServiceListener(ChatServiceListener listener) {
		this.listener = listener;
	}
	public boolean send(FlowData flow, List<AtomData> atoms) {
            flow.setModified(System.currentTimeMillis());
            flow.setAtomlist(atoms);
            String msg =  "["+flow.toJson("u")+"]";            
            
            send(msg);
            return true;
        }
	
	public  void connect() {
            try {
            	String id = FlowManager.getManager().getMe().getId();
            	int at = id.indexOf("@");
            	//if (at>0) id = id.substring(0, at);
            	if (at > 0) {
            		p("===== warn, id contains @ character: "+id);
            	}
            	if (!serverurl.endsWith(id)) serverurl += id;
                p("Connecting to "+serverurl);

                
                cc = new org.java_websocket.client.WebSocketClient(new URI(serverurl), new Draft_17()) {
                
                @Override
                public void onMessage(String message) {
                    p("====================================================================\nonMessage: got payload:\n" + message + "\n");
                    List<Flow> flows = FlowData.fromJsonArray(message);
                    p("Calling processFlowMessages of listener "+listener.getClass().getName());
               	    listener.processFlowMessages(flows);
                    
                }


                @Override
                public void onOpen(ServerHandshake handshake) {
                    p("onOpen: You are connected to server: " + getURI() + "\n");   
                    
                }
                
                @Override
                public void onClose(int code, String reason, boolean remote) {
                    p("on Close: You have been disconnected from: " + getURI() + " Code: " + code + " " + reason + "\n");
                    
                }
                
                
                @Override
                public void onError(Exception ex) {
                    ex.printStackTrace();
                }
            };
            cc.connect();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            Logger.getLogger(WebSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
	public void disconnect() {
            cc.close();
        }
        
	public void send(String txt) {
            p("Sending to "+serverurl+":\n"+txt+"\n");
            cc.send( txt );
        }
	
	protected static void p(String msg) {
		log.info("======================= WEB SOCKET CLIENT: "+msg);
	}

	protected static void err(String msg) {
		log.severe("======================= WEB SOCKET CLIENT: "+msg);
	}

	protected static void warn(String msg) {
		log.warning("======================= WEB SOCKET CLIENT: "+msg);
	}
}
