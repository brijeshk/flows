/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.net;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;

//import android.util.Base64;


import com.auto.db.SqlListeDesktopHelper;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;
import com.auto.model.Group;
import com.auto.model.Port;
import com.auto.model.User;
import com.auto.utils.Base64;
import com.auto.utils.DateTools;
import com.auto.utils.FileTools;
import com.auto.utils.FileToolsIF;
import com.auto.utils.Settings;

/**
 *
 * @author Chantal
 */
public class WebService {

    private static Logger log = Logger.getLogger(WebService.class.getName());

    public static String host = "http://autometon.com:5000/";

    private User me;
    private static FileToolsIF ft;

    private Settings settings;

    public WebService(User me, FileToolsIF ft) {
        this.me = me;
        this.ft = ft;
        settings = Settings.getSettings(ft);
    }

    /**
     * { "users": [ "JohnnyMeton", "chantal" ], "pid": 20, "state": "Say",
     * "data": null, "atoms": [ { "crud": "c", "state": "Say", "sender":
     * "chantal", "body": "", "data": null, "modified": "2014-12-14T18:23:03Z",
     * "aid": 5, "fid": "df140207-86d5-4db7-bdbf-7485acf86f8e" } ], "modified":
     * "2014-12-14T18:23:03Z", "fid": "df140207-86d5-4db7-bdbf-7485acf86f8e",
     * "crud": "u"
     *
     *
     * [ {"fid":"df140207-86d5-4db7-bdbf-7485acf86f8e","pid":6,"crud":"u",
     * "users":["metonbot","chantal"], "state":"Error","data":null, "atoms":[
     * {"fid":"df140207-86d5-4db7-bdbf-7485acf86f8e","aid":1,
     * "crud":"c","sender":"metonbot","state":"Error",
     * "data":{"Error":{"type":"Error","value":"Unknown command : goog"} },
     * "body":"","modified":"2014-12-14T18:36:02.240817Z"}],
     * "modified":"2014-12-14T18:36:02.240817Z"} ]
     *
     * @param service
     * @param sinceTime
     * @return
     */
    /*
     * XXX TODO r.POST("/ports", HandleCreatePort) r.PUT("/ports/:pid",
     * HandleUpdatePort) r.POST("/users", HandleCreateUser) r.PUT("/users/:uid",
     * HandleUpdateUser)
     */
    public List<Flow> getFlowDelta(ChatServiceListener listener) {
        String path = "flowDelta";
        String lastSynchedTime = settings.get("lastsynchedtime");
        // past 30 days
        long day = 1000 * 24 * 3600;
        Date d = new Date(System.currentTimeMillis() - day * 30);
        // p("Date is: "+d);
        if (lastSynchedTime == null || lastSynchedTime.length() < 1) {
            lastSynchedTime = DateTools.toString(d);
        }
		// p("Using date: "+lastSynchedTime);
        // settings.set("lastsynchedtime", lastSynchedTime);

        String res = get(
                host + path + "/" + me.getId() + "/" + lastSynchedTime, true);

        p("===================  getFlowDelta ===========================");
        p(res);
        List<Flow> flows = FlowData.fromJsonArray(res);
        listener.processFlowMessages(flows);
        p("=================== END  getFlowDelta ===========================");
        return flows;
    }

	// TODO: need to get contacs *for a particular user*
    // http://autometon.com:5000/users
    public List<User> getUsers() {

        String path = "users";
        String res = get(host + path);

        // p("Got users json from host: \n" + res);
        try {
            if (res != null && res.length() > 0) {
                ft.writeStringToFile("users.json",
                        new JSONArray(res).toString(3));
            }
        } catch (JSONException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
        return User.fromJsonArray(res);

    }

    // http://autometon.com:5000/users
    public List<Group> getGroups() {

        String path = "groups/public";
        String res = get(host + path);

        p("Got groups json from host: \n" + res);
        try {
            if (res != null && res.length() > 0) {
                ft.writeStringToFile("groups.json",
                        new JSONArray(res).toString(3));
            }
        } catch (JSONException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
        List<Group> groups = Group.fromJsonArray(res);
        if (groups == null || groups.size() < 1) {
            groups = new ArrayList<Group>();
            p("Adding test group for now");
            Group g = new Group("1");
            g.setName("autometon");
            g.setType("public");
            g.setUserlist("chantal, brijesh, prateek");
            if (g.getUserlist() != null) {
                g.parseUsers(g.getUserlist());
            }
            groups.add(g);
        }
        return groups;
    }

    public List<User> getCachedUsers() {
        String res = ft.getFileAsString("users.json");
        if (res == null) {
            p("Found no cached users. Using test data for now from model package: users.json");

            InputStream is = this.getClass().getResourceAsStream("users.json");
            if (is == null) {
                warn("Got no inputstream for " + getClass().getName()
                        + ", file users.json");
            } else {
                res = ft.getFileAsString(is);
            }
        }
        if (res != null) {
            // p("Got cached users:\n"+res);
            List<User> users = User.fromJsonArray(res);
            User temp = new User("templates");
            users.add(temp);
            return users;
        } else {
            return null;
        }
    }

    public List<Group> getCachedGroups() {
        String res = ft.getFileAsString("groups.json");
        if (res == null) {
            p("Found no cached groups. Using test data for now from model package: groups.json");

            InputStream is = this.getClass().getResourceAsStream("groups.json");
            if (is == null) {
                warn("Got no inputstream for " + getClass().getName()
                        + ", file groups.json");
            } else {
                res = ft.getFileAsString(is);
            }
        }
        if (res != null) {
            // p("Got cached Groups:\n" + res);
            List<Group> groups = Group.fromJsonArray(res);
            return groups;
        } else {
            return null;
        }
    }

    public List<Port> getCachedPorts() {
        String res = ft.getFileAsString("ports.json");
        if (res == null) {
            p("Trying to read default ports for me from package");
            InputStream is = this.getClass().getResourceAsStream("ports.json");
            res = ft.getFileAsString(is);
        }
        // p("Got cached port :"+res);
        if (res != null && res.length() > 0) {
            // p("Got cached ports for user "+user.getId()+": "+res);
            return Port.fromJsonArray(res, null);
        } else {
            return null;
        }
    }

    public List<Port> getCachedPorts(User user) {
        String res = ft.getFileAsString("ports_" + user.getId() + ".json");
        if (res == null) {
            p("Trying to read default ports for user " + user.getId()
                    + " from package");
            InputStream is = this.getClass().getResourceAsStream(
                    "ports_" + user.getId() + ".json");
            res = ft.getFileAsString(is);
        }
        // p("Got cached port :"+res);
        if (res != null && res.length() > 0) {
            // p("Got cached ports for user "+user.getId()+": "+res);
            return Port.fromJsonArray(res, user);
        } else {
            return null;
        }
    }

    public List<Port> getPorts() {
        String path = "ports/visible/" + me.getId() + "?publicPorts=true";
        String res = get(host + path);

        try {
            if (res != null && res.length() > 0) {
                if (!res.startsWith("[")) {
                    res = "[" + res + "]";
                }
                ft.writeStringToFile("ports.json",
                        new JSONArray(res).toString(3));
            }
        } catch (JSONException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
        return Port.fromJsonArray(res, null);
    }

    public List<Port> getPorts(User user) {
        // /ports/visible/chantal
        String path = "ports/user/" + user.getId();
        String res = get(host + path);
        p("Got port from host for user " + user.getId() + "\n" + res);
        try {
            if (res != null && res.length() > 0) {
                if (!res.startsWith("[")) {
                    res = "[" + res + "]";
                }
                ft.writeStringToFile("ports_" + user.getId() + ".json",
                        new JSONArray(res).toString(3));
            }
        } catch (JSONException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE,
                    null, ex);
        }

        return Port.fromJsonArray(res, user);
    }

    // http://autometon.com:5000/users
    public List<Port> getPorts(Group group) {
        String path = "ports/group/" + group.getId();
        String res = get(host + path);
        // p("Got port from host for user " + user.getId() + "\n" + res);
        try {
            if (res != null && res.length() > 0) {
                if (!res.startsWith("[")) {
                    res = "[" + res + "]";
                }
                ft.writeStringToFile("ports_" + group.getId() + ".json",
                        new JSONArray(res).toString(3));
            }
        } catch (JSONException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE,
                    null, ex);
        }

        return Port.fromJsonArray(res, group);
    }

    /**
     * searches/inflates users if the id matches the id, email or phone number
     */
    public List<User> inflate(List<String> ids) {

        String path = "searchusers";
        String json = ids.toString();
        p("Inflating users: " + json);
        String res = put(host + path, json);

        p("Got users json from inflate: \n" + res);

        return User.fromJsonArray(res);

    }

    /*
     * r.POST("/ports", HandleCreatePort) r.PUT("/ports/:pid", HandleUpdatePort)
     * r.POST("/users", HandleCreateUser) r.PUT("/users/:uid", HandleUpdateUser)
     */
    public boolean update(Port port) {
        p("============= UPDATING PORT " + port.getId());
        String path = "ports/" + port.getId();
        String json = port.toJson();
        String ok = put(path, json);
        p("Got response " + ok + " for update port");
        // return ok != null;
        return ok != null;
    }

    public Port create(Port port) {
        p("============= CREATING PORT " + port.getName());
        String path = "ports";
        String json = port.toJson();
        String jsonresult = post(path, json);
        // now parse port
        User creator = FlowManager.getManager().getUser(port.getCreator());
        p("Response: " + jsonresult);
        Port resport = Port.fromJson(jsonresult, creator);

        p("Parsed to: " + resport);

        return resport;
    }

    public boolean update(User user) {
        p("============= UPDATING USER " + user.getId());
        String path = "users/" + user.getId();
        String json = user.toJson();
        String ok = put(path, json);
        if (ok == null) {
            p("Could NOT update user " + user);
        }
        return ok != null;
    }

    public boolean create(User user) {
        p("============= CREATING AM USER " + user.getName());
        String path = "users/";
        String json = user.toJson();
        String id = post(path, json);
        if (id.length() > 0) {
            user.setId(id);
            return true;
        } else {
            p("Could NOT create user " + user);
            return false;
        }
    }

    
    private static String put(String path, String json) {
        return postOrPut(host, path, json, "PUT");
    }

    private static boolean delete(String path) {
        return delete(host, path);
    }

    private static String post(String path, String json) {
        return postOrPut(host, path, json, "POST");
    }

    public static String postFlowDelta(String userid, String json,
            boolean notify) {
        String path = "flowDelta/" + userid + "?notify=" + notify;
        String res = post(path, json);
        p("Sent json to " + host + path + ", result: " + res);

        return res;
    }

    public int login() {
    	p("=========================  LOGIN ============== ");
    	int code = simpleget(host+"users/login", true );
    	p("Login response: "+code);
    	return code;
    }
    private static String postOrPut(String host, String path, String json, String method) {
        HttpURLConnection conn = null;
        HttpURLConnection.setFollowRedirects(true);
        String response = null;
        int code;
        p(method + " " + host + path + "\n" + json);
        try {
            URL url = new URL(host + path);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(method);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(true);
            conn.setReadTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            User me = FlowManager.getManager().getMe();
            conn.setRequestProperty("Authorization",getAuthString(me));
            OutputStreamWriter osw = new OutputStreamWriter(
                    conn.getOutputStream());
            osw.write(json);
            osw.flush();
            osw.close();

            code = conn.getResponseCode();

            p("Got response code: " + code);

            if (code != 200) {
                if (code == 307) {
                    p("Got temporary redirect. Trying to read new location from header");
                } else {
                    err(method + " to " + host + path + " failed: " + code);
                }
                Map<String, List<String>> map = conn.getHeaderFields();

                p("Header:");
                for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                    String key = entry.getKey();
                    p("Key : " + key + "=" + entry.getValue());
                    if (code == 307) {
                        if (key != null && key.equalsIgnoreCase("location")) {
                            String loc = entry.getValue().get(0);
                            p("Got redirect: " + loc);
                            if (!loc.equalsIgnoreCase(host + path) && !loc.equalsIgnoreCase(path)) {
                                return postOrPut(host, loc, json, method);
                            }
                        }
                    }
                }
            } else {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                String inputLine;
                if (response == null) {
                    response = "";
                }
                while ((inputLine = in.readLine()) != null) {
                    response += inputLine + "\n";
                }

                in.close();
            }
            p("read response: " + response);
        } catch (Exception e) {
            warn("Could NOT write to " + host + path + ": " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return response;

    }

    private static boolean delete(String host, String path) {
        HttpURLConnection conn = null;
        HttpURLConnection.setFollowRedirects(true);        
        int code;
        p("DELETE " + host + path);
        try {
            URL url = new URL(host + path);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("DELETE");
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(true);
            conn.setReadTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            User me = FlowManager.getManager().getMe();
            conn.setRequestProperty("Authorization",getAuthString(me));
            code = conn.getResponseCode();

            p("Got response code: " + code);

            if (code != 200) {
                err("DELETE " + host + path + " failed: " + code);
                return false;
            }
        } catch (Exception e) {
            warn("Could NOT write to " + host + path + ": " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return true;

    }

    private static void test() {
        User me = new User("chantal");
        me.setEmail("chantal");
        WebService serv = new WebService(me, FileTools.getFileTools());
        FlowManager man = FlowManager.getManager(new SqlListeDesktopHelper(
                "chantaltest"), FileTools.getFileTools());


//        WebSocketConnector conn = new WebSocketConnector(
//                "ws://autometon.com:500/websocket/chantal/");
//        conn.setChatServiceListener(man);
//		// ChatConnector conn = new BasicXmppService(me,
//        // FileTools.getFileTools(),
//        // conlistener);
//
//        man.loadWebData();
//        List<Port> allports = serv.getPorts();
//        for (User user : man.getUsers()) {
//            // now add the ports for this user
//            List<Port> ports = new ArrayList<Port>();
//            for (Port p : allports) {
//                if (p.getCreator().equalsIgnoreCase(user.getId())) {
//                    ports.add(p);
//                }
//            }
//          //  p("Got ports: " + ports);
//            user.setPorts(ports);
//        }
//
//        try {
//            p("Trying to login via connector " + conn);
//            man.loginService(conn);
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//       // serv.getFlowDelta(man);
//
//       // testCreatePort(me, serv);

    }

	private static void testCreatePort(User me, WebService serv) {
		p("============= CREATING PORT ========== ");
        Port port = new Port(-1, "AutometonTesting");
        port.setCreator(me.getId());
        port.setType("default");
        p("Creating and saving new port " + port);
        port.connectStates(null);

        Port resport = serv.create(port);
        
        p("Got port id: "+resport.getId());
        p("Deleting it now");
        serv.delete(resport);
	}

    public static String get(String fullurl) {
        return get(fullurl, false);
    }

    public static int simpleget(String fullurl, boolean show) {
        HttpURLConnection conn = null;
       int code = 0;
        HttpURLConnection.setFollowRedirects(true);
        if (show) { 
            p("Connecting to " + fullurl);
        }
        try {
            URL url = new URL(fullurl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setReadTimeout(5000);
            User me = FlowManager.getManager().getMe();
            if (me != null) conn.setRequestProperty("Authorization",getAuthString(me));            
            code = conn.getResponseCode();            
        } catch (Exception e) {
            warn("Could not read from " + fullurl + ": " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return code;
        
    }
    private static String getAuthString(User me) {
    	String auth = me.getId() + ":" + me.getPassword();
    	auth = "Basic "+ Base64.encodeBytes(auth.getBytes());
    	p("Setting Authorization to: "+auth);
    	return auth;
    }
    public static String get(String fullurl, boolean show) {
        HttpURLConnection conn = null;

        StringBuilder buf = new StringBuilder();
        HttpURLConnection.setFollowRedirects(true);
        if (show) {
            p("Connecting to " + fullurl);
        }
        try {
            URL url = new URL(fullurl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setReadTimeout(5000);
            User me = FlowManager.getManager().getMe();
            if (me != null) {
            		conn.setRequestProperty("Authorization",getAuthString(me));
            }
            int code = conn.getResponseCode();
            if (code >= 300 && code < 400) {
                // get the cookie if need, for login
                String newUrl = conn.getHeaderField("Location");
                if (show) {
                    p("============= REDIRECT ============ ");
                }
                showHeader(conn);
                // p("newUrl: " + newUrl);
            }
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String nextLine;
            while ((nextLine = br.readLine()) != null) {
                buf.append(nextLine);
                buf.append('\n');
            }
            in.close();
        } catch (Exception e) {
            warn("Could not read from " + fullurl + ": " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        String r = buf.toString();
        if (show) {
            if (r.length() < 200) {
                p(r);
            } else {
                p(r.substring(0, 200));
            }
        }
        return r;
    }

    public static void main(String args[]) {
        test();
    }

    private static void p(String msg) {
        log.info(msg);
    }

    private static void err(String msg) {
        log.severe(msg);
    }

    private static void warn(String msg) {
        log.warning(msg);
    }

    private static void showHeader(HttpURLConnection conn) {
        Map<String, List<String>> hmap = conn.getHeaderFields();

        for (String key : hmap.keySet()) {
            System.out.println(key + "=" + hmap.get(key));
        }
    }

    public boolean delete(Port port) {       
        p("============= DELETING PORT " + port.getName());
        String path = "ports/" + port.getId();
        return delete(path);
    }
}
