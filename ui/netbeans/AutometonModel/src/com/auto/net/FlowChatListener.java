/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.net;

import java.util.List;

import com.auto.model.Flow;

/**
 *
 * @author Chantal
 */
public interface FlowChatListener {
    
    public void receive(String crud, List<Flow> flows);
}
