/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.net;

import java.util.ArrayList;
import java.util.List;

import com.auto.model.Atom;
import com.auto.model.AtomData;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;
import com.auto.model.User;
import com.auto.utils.FileToolsIF;

/**
 *
 * @author Chantal
 */
public class FlowChatService extends AbstractChatService implements
        ChatServiceListener {

    public FlowChatService(ChatConnector connector, User user, FileToolsIF ft,
            FlowChatListener listener) {
        super(connector, user, ft, listener);

    }

    public FlowChatService(ChatConnector connector, User user, FileToolsIF ft) {
        super(connector, user, ft);

    }

    public void send(Flow flow) {
        //	p("Sending flow "+flow);		
        FlowData fd = flow.converToFlowData();
        fd.setModified(System.currentTimeMillis());
        List<User> users = flow.getTargetUsers(true);
        send(users, fd, fd.getAtomlist());
    }

    public boolean send(Flow flow, Atom atom) {
        FlowData fd = flow.getFlowData();
        fd.setModified(System.currentTimeMillis());
        List<AtomData> atoms = new ArrayList<AtomData>();
        atoms.add(atom.getAtomData());
        List<User> users = flow.getTargetUsers(true);
        send(users, fd, atoms);
        return true;
    }

    protected void send(final List<User> users, FlowData fd, List<AtomData> atoms) {
        fd.setModified(System.currentTimeMillis());
        fd.setAtomlist(atoms);
        final String json = "[" + fd.toJson(fd.getCrud()) + "] ";
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for (final User u : users) {
                    String res = WebService.postFlowDelta(u.getId(), json, !FlowManager.getManager().isMe(u));
                    p("Sent json to user "+u.getId()+", result: "+res);
                }
            }
        });
        t.start();
//		 Exception e = new Exception("Checking who is sending it");
//	    	e.printStackTrace();
    }

    @Override
    public void processFlowMessages(List<Flow> flows) {
        FlowManager man = FlowManager.getManager();
        if (man == null) {
            return;
        }
        //	p("processFlowMessages: Got flows " + flows);
        if (listener != null) {
            //		p("Sending to listener");
            listener.receive("u", flows);
        } else {
            err("GOT NO CHAT LISTENER");
        }
    }

}
