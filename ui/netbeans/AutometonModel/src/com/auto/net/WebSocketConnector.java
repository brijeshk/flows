package com.auto.net;

import com.auto.model.AtomData;
import com.auto.model.FlowData;
import java.util.List;

public class WebSocketConnector implements ChatConnector {

	WebSocketClient socket;

	public WebSocketConnector(String serverurl) {
		this.socket = new WebSocketClient(serverurl);
	}

	public void setChatServiceListener(ChatServiceListener listener) {
		socket.setChatServiceListener(listener);
	}

	@Override
	public boolean connect(String username, String pw) {
		socket.connect();
		return true;
	}

	@Override
	public void disconnect() {
		socket.disconnect();

	}

	public void send(String txt) {
		// p("use WebService postFlowDelta");
		socket.send(txt);
	}

	@Override
	public boolean send(FlowData flow, List<AtomData> atoms) {
		// p("use WebService postFlowDelta");
		return socket.send(flow, atoms);

	}

}
