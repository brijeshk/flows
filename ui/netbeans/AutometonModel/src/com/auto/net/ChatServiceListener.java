package com.auto.net;

import java.util.List;

import com.auto.model.Flow;
import com.auto.model.FlowData;

public interface ChatServiceListener {

	public void processFlowMessages(List<Flow> flows);

}
