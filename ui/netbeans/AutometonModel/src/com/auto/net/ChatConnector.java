package com.auto.net;

import java.util.List;

import com.auto.model.AtomData;
import com.auto.model.FlowData;

public interface ChatConnector {
	
	
	public boolean connect(String username, String pw);
	
	public void disconnect();
	
	public boolean send(FlowData flow, List<AtomData> atoms);
}
