AutometonModel
==============
Java model of flows that should be reusable for Desktop, Android and Web clients.
Code only includes minimal set of libraries - SMACK - to make it compatible between clients.
(On Android, the asmack library has to be used)

The model includes all handling of data not related to the UI.

AutoClient
==========
Simple Java Desktop Swing client that uses the data model of AutometonModel
Its main purposes is to have a demo client for developing and testing the model, and to see what kind of UI we want. Also, the goal is to move as much code as possible to the AutometonModel so that it can later be reused on Android.

Both are developed with Netbeans 8.0. In principle any IDE can be used, but Netbeans makes it easier to manage the UI and dependency.

To use it in Netbeans:
1. download Netbeans 8.x :-)
2. open both projects
3. add a dependency of the AutoClient to AutometonModel
