/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.server;

import com.auto.model.InputData;
import com.auto.model.User;
import java.util.List;

/**
 *
 * @author Chantal
 */
public interface BotIF {
    public BotMsg processCommand(int portid, String statename, String from, List<InputData> inputdatas, String message);
    
    public User getMe();

    public User loadMyDataModel() throws Exception;
}
