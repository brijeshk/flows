/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.server;

import com.auto.model.AtomData;
import com.auto.model.FlowData;
import com.auto.model.InputData;
import com.auto.model.State;
import com.auto.model.User;
import com.auto.utils.FileTools;
import com.auto.xmpp.BasicXmppService;
import java.util.List;

/**
 *
 * @author Chantal
 */
public class BotXmppService extends BasicXmppService{

    BotIF bot;

    public BotXmppService(User user, BotIF bot) {
        super(user, new FileTools(), null);
        this.bot = bot;
        super.connect();

    }

    @Override
    public void receiveFlowMessage(String from, String body) {
        
        int sl = from.indexOf("/");
        if (sl > 0) {
            from = from.substring(0, sl);
        }
        p("+++++++++++++++  receiveFlowMessage from " + from);
        // find user in manager
        p("Sender is: " + from);
       
        if (body.startsWith("MP##")) {
            body = body.substring(4);
            String[] fields = body.split("##", 2);
            String json = fields[0];
           
            List<FlowData> fdlist = FlowData.fromJsonArrayNoDb(json);
           
            for (FlowData fd: fdlist) { 
                p("Data: " + fd);  
                for (AtomData ad: fd.getAtomlist()) {
                    List<InputData> outputdatas = convert(ad.getData());
                    BotMsg botmsg = bot.processCommand(ad.getPort_id(), fd.getState(), ad.getState(), outputdatas, ad.getBody());        
                    p("botmsg result: uuid="+fd.getUuid()+", state="+botmsg.statename+", msg="+botmsg.msg+", portid="+ad.getPort_id());
                    ad.setBody(botmsg.msg); 
                    ad.setState(botmsg.statename);
                    ad.setData(InputData.toJson(botmsg.outputdatas));
                }
                super.send(fd, fd.getAtomlist());

            }
        } else {
            p("Received NON flow msg: " + body);
            processNonFlowMessage(from, body);
        }
      
    }


    public String convert(InputData data) {
        return data.toJson(true);
    }
    public String convert(List<InputData> outputdatas ) {
        return InputData.toJson(outputdatas);        
    }
    public InputData getDefaultResult(String result) {
        InputData res = new InputData("Result");
        res.add(result);
        return res;        
    }
    public InputData getDefaultError(String msg) {
        InputData res = new InputData("Error");
        res.add(msg);
        return res;        
    }
    public List<InputData> convert(String json) {
        return InputData.fromJsonArray(json, new State("Result"));
    }
    
    

}
