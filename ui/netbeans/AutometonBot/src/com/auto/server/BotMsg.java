/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.server;

import com.auto.model.InputData;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chantal
 */
public class BotMsg {
        
    List<InputData> outputdatas;
    String msg;
    String statename;
    
    public BotMsg(String statename, String msg,  List<InputData> outputdatas ) {
        this.outputdatas = outputdatas;
        this.msg = msg;
        this.statename = statename;
    }
    public BotMsg(String statename, String msg,  InputData outputdata ) {
        this.outputdatas = new ArrayList<InputData>();
        if (outputdata != null) outputdatas.add(outputdata);
        this.msg = msg;
        this.statename = statename;
    }
    
    public String toString() {
        String s = "";
    
        if (outputdatas != null && outputdatas.size()>0) {
            s = outputdatas.toString();
        }
       
        return "BotMsg("+statename+", "+s+", "+msg+")";
    }
}
