/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.server.bots;

import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.server.BotMsg;
import com.auto.server.SystemBot;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public class Weather extends AbstractBot {

    public Weather() {
        super();
        try {

            me = loadMyDataModel("Weather");
            man = FlowManager.getManager();
        } catch (Exception ex) {
            err("was not able to load my data model: " + ex);
            Logger.getLogger(Weather.class.getName()).log(Level.SEVERE, null, ex);

        }
    }
    /**
     * {"coord":{"lon":8.55,"lat":47.37}, "sys":
     * {"type":1,"id":6007,"message":0.2025,
     * "country":"CH","sunrise":1411535749,"sunset":1411579170}, "weather":[ { *
     * "id":520, "main":"Rain", "description":"light intensity shower
     * rain","icon":"09n" } ], "base":"cmc stations",
     * "main":{"temp":287.87,"pressure":1014,"humidity":72,
     * "temp_min":286.95,"temp_max":289.15},
     * "wind":{"speed":6.2,"deg":320,"gust":11.8},
     * "clouds":{"all":90},"dt":1411583610,"id":2657896,
     * "name":"Zurich","cod":200}
     *
     * @param from
     * @param jsondata
     * @param message
     * @return
     */
    @Override
    public BotMsg processCommand(int portid, String statename, String from, List<InputData> inputdatas, String message) {

       
        String place = this.getValueWithName("place", inputdatas);
        if (place == null) {
            place = "Bern";
        }
        JSONObject json = getJsonFromUrl("http://api.openweathermap.org/data/2.5/weather?q=" + place);
        String result = "no data";
        String what = this.getValueWithName("what", inputdatas);
        if (what == null) {
            what = "weather";
        }
        if (json != null) {

            what = what.toLowerCase();
            try {
                if (what.equalsIgnoreCase("weather")) {
                    result = json.getJSONArray("weather").getJSONObject(0).getString("description");
                } else if (what.startsWith("cloud")) {
                    result = ""+json.getJSONObject("clouds").get("all")+"%";
                } else {
                    JSONObject main = json.getJSONObject("main");
                    p("main is " + main);
                    double temp = 0;
                    if (what.startsWith("temp")) {
                        temp = main.getDouble("temp");
                    } else if (what.startsWith("max")) {
                        temp = main.getDouble("temp_max");
                    } else if (what.startsWith("min")) {
                        temp =  main.getDouble("temp_min");
                    }
                    double c =temp- 273.15;
                    double f = c*1.8+32.0;
                    result = Math.round(c*10)/10.0+ " C/ "+Math.round(f*10)/10.0+" F";
                }
            } catch (JSONException ex) {
                err("Could not get " + what + ": " + ex.getMessage() + ", json was:" + json);
                result = json.toString();
            }
        }
        InputData output = getDefaultResult(result);
        p("Got resonse: " + output);
        return new BotMsg("Respond", "Current " + what + " for " + place, output);
    }

    public static void test() {
        Weather w = new Weather();
        w.processCommand(0, "get","chantal", null, null);
    }

    public static void main(String[] args) {
        SystemBot bot = new SystemBot(new Weather());
        //test();
    }
}
