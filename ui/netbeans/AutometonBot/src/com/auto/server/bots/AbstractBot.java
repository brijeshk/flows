/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.server.bots;

import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import com.auto.net.WebService;
import com.auto.server.BotIF;
import com.auto.server.BotMsg;
import com.auto.utils.FileTools;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Chantal
 */
public abstract class AbstractBot implements BotIF {

    private static final Logger log = Logger.getLogger(AbstractBot.class.getName());

    protected User me;
    protected FlowManager man;

    
    @Override
    public abstract BotMsg processCommand(int portid, String statename, String from, List<InputData> inputdatas, String message);

    public InputData getDataWithName(String name, List<InputData> inputdatas) {
        if (inputdatas == null) {
            return null;
        }
        for (InputData data : inputdatas) {
            if (data.getName().equalsIgnoreCase(name)) {
                return data;
            }
        }
        return null;
    }

    @Override
    public User getMe() {
        String name = getClass().getName().toLowerCase();
        int last = name.lastIndexOf(".");
        User xmppUser = new User(name.substring(last + 1).toLowerCase());
        return xmppUser;
    }

    public String getValueWithName(String name, List<InputData> inputdatas) {
        if (inputdatas == null) {
            return null;
        }
        InputData data = getDataWithName(name, inputdatas);
        if (data != null && data.hasValues()) {
            return data.getValue().getValue();
        }
        return null;
    }

    public String get(String url) {
        return WebService.get(url);
    }

    public JSONObject getJsonFromUrl(String url) {
        String res = get(url);
        try {
            JSONObject json = new JSONObject(res);
            return json;
        } catch (JSONException ex) {
            err("Could not convert to json: " + res);
        }
        return null;
    }

    public String convert(InputData data) {
        return data.toJson(true);
    }

    public InputData getDefaultResult(String result) {
        InputData res = new InputData("Result");
        res.add(result);
        return res;
    }

    public InputData getDefaultError(String msg) {
        InputData res = new InputData("Error");
        res.add(msg);
        return res;
    }

    public List<InputData> convert(String json) {
        return InputData.fromJsonArray(json, new State("Result"));
    }

    @Override
    public User loadMyDataModel() throws Exception {
        return loadMyDataModel(getMe().getId());
    }
    protected User loadMyDataModel(String username) throws Exception {
        p("============= Loading data model "+username+" =============");
        User me = new User(username);
        me.setEmail(username);
        FlowManager man = FlowManager.getManager(null, FileTools.getFileTools());
        man.login(username, username);
        
        WebService serv = man.getWebService();
        List<User> users = serv.getUsers();
        p("got users: "+users);
        man.setUsers(users);
        p("Loading ports for "+me);
        List<Port> ports = serv.getPorts(me);
        man.setPorts(ports);
        me.setPorts(ports);
        p("My ports are: "+ports);
        p("============= End of loading data model =============");
        return me;
    }

    protected void p(String s) {
        log.info(getClass().getName()+":"+s);
    }

    protected void err(String s) {
        log.severe(s);
    }

    protected void warn(String s) {
        log.warning(s);
    }
}
