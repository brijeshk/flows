/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.server.bots;

import com.auto.model.DataType;
import com.auto.model.DataValue;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.Port;
import com.auto.model.State;
import com.auto.model.User;
import com.auto.server.BotMsg;
import com.auto.server.SystemBot;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chantal
 */
public class JohnnyMeton extends AbstractBot {

 
    // precanned messages for now
    private static final String WONTDOIT[] = {
        "I don't do it",
        "Nope",
        "No thanks",
        "I can't accept it",
        "Sorry, nope",
        "No",
        "Unfortunately no",
        "Sorry man"
    };
    private static final String COMPLETE[] = {
        "All done!",
        "done.",
        "completed",
        "I finished it",
        "I am done"
    };
    private static final String SOUNDSGOOD[] = {
        "Sounds good!",
        "cool.",
        "sure",
        "whatever",
        "OK",
        "why not?",
        "interesting...",
        "hmmm... ",
        "sounds interesting",
        "that's weird",
        "nice"
    };
     private static final String ACCEPT[] = {
        "Sounds good!",
        "great.",
        "I accept",
        "I can live with that",
        "OK",
        "why not?",
        "fine",
        "that's ok"        
    };
       private static final String ANSWER[] = {
        "Maybe",
        "Not sure.",
        "I can't decide",
        "Possibly",
        "OK",
        "Sorry, no",
        "I'm not sure",
        "I'm not ready to answer yet"        
    };
    private static final String DONTKNOW[] = {
        "No idea",
        "not sure.",
        "I have no clue",
        "what was that?",
        "no comprendo",
        "nix verstehen",
        "why?",
        "what?",
        "I don't get it, sorry",
        "Pardon me?"
    };
    private static final String ERRANDS[] = {
        "I've got to run some errands",
        "I am done with the errands",
        "Can you please get some cereal on the way home?",
        "I've got to pick up the car on the way back",
        "I've got to do some shopping",
        "I'm done with shopping",
        "can you please run by the post office?",
        "are you done?",
        "I forgot about the errands",
        "did you do the errands?"
    };
    private static final String TENNIS[] = {
        "I love tennis",
        "Who won the last game?",
        "Federer is cool",
        "Let's watch the game tonight",
        "Is there a game on today?",
        "The last game was boring",
        "Let's go play tennis soon",
        "How was the match?"
    };
    private static final String PROJECT[] = {
        "I am mostly done with the tasks",
        "How are you doing on the tasks?",
        "I am stuck with a task",
        "keep the project secret",
        "I've got just a few tasks left"
    };

    public JohnnyMeton() {
        super();
        try {

            me = loadMyDataModel("JohnnyMeton");
            man = FlowManager.getManager();
        } catch (Exception ex) {
            err("was not able to load my data model: " + ex);
            Logger.getLogger(JohnnyMeton.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    @Override
    public BotMsg processCommand(int portid, String statename, String from, List<InputData> inputdatas, String message) {
        //
        p("processCommand. me is: " + me+", portid="+portid);
        InputData output = null;
        if (me == null || man == null || !man.hasPorts()) {
            try {

                me = loadMyDataModel("JohnnyMeton");
                man = FlowManager.getManager();
            } catch (Exception ex) {
                err("was not able to load my data model: " + ex);
                Logger.getLogger(JohnnyMeton.class.getName()).log(Level.SEVERE, null, ex);
                return new BotMsg("Error", "I was not able to load my data model", output);
            }
        }
        Port port = null;
        State state = null;
        if (me.getPorts() != null) {
            port = me.getPort(portid, true);
            state = port.getState(statename);
            p("Port " + port + ", and state " + state);
        } else {
            err("Dat model has no ports");
        }
        if (port == null) {
            return new BotMsg("Error", "I was not able to load my data model, I found no port with id " + portid, output);
        }
        if (state == null) {
            return new BotMsg("Error", "I was not able to load my data model, I found no state for " + statename, output);
        }

        List<State> nextstates = state.getNextStates();
        if (nextstates == null || nextstates.size() < 1) {
            nextstates = port.getAllFirstStates();
        }
        p("Got next states: " + nextstates);
        if (nextstates == null || nextstates.size() < 1) {
            err("Got no next state or first state. This is an error. Using say as Plan B");
            nextstates.add(new State("Say"));
        }
        // pick one randomly
        int which = (int) Math.floor(Math.random() * nextstates.size());

        String responsemsg = pick(SOUNDSGOOD);
        State nextstate = nextstates.get(which);

        // make response dependent on the type of data it has and on the kind of state it is
        String s = nextstate.getName().toLowerCase();
        String p = port.getName().toLowerCase();
        String[] cancel = {"cancel", "abort", "deny", "recall"};
        String[] answer = {"answer", "reply"};
        String[] complete = {"complete", "done"};
        String[] accept = {"accept", "ok"};
        List<InputData> outputdatas = nextstate.getInputDatas();

        // check if we have to generate data
        if (outputdatas != null && outputdatas.size() > 0) {
            for (InputData outputdata : outputdatas) {
                responsemsg = generateData(outputdata);
            }
        }
        if (contains(cancel, s)) {
            p("got cancel state");
            responsemsg = pick(WONTDOIT);
        } else if (contains(complete, s)) {
            p("got complete state");
            responsemsg = pick(COMPLETE);
        } else if (contains(answer, s)) {
            p("got answer state");
            responsemsg = pick(ANSWER);
        } else if (contains(accept, s)) {
            p("got accept state");
            responsemsg = pick(ACCEPT);
        } else {
            p("Checking port name " + p);
            if (p.contains("errands")) {
                p("Got home_errands");
                responsemsg = pick(ERRANDS);
            } else if (p.contains("project")) {
                p("Got project_x_tasks");
                responsemsg = pick(PROJECT);
            } else if (p.contains("tennis")) {
                p("Got tennis");
                responsemsg = pick(TENNIS);
            } else if (p.contains("car")) {
                p("Got car_mechanical_services");

            } else {
                // probably general!
                p("Got something general, picking soundsgood");
                responsemsg = pick(SOUNDSGOOD);
            }
        }
        // 
        //

        //State next = port.get
        p("Created response: " + responsemsg);
        
        return new BotMsg(nextstate.getName(), responsemsg, outputdatas);
    }

    private String generateData(InputData data) {
        DataType type = data.getType();
        String name = data.getName();
        String response;
        int nr = 1;
        List<DataValue> values = new ArrayList<DataValue>();
        if (type.isList()) {
            nr = rand(5) + 2;
            if (type.isTime()) response = "Here are some possible times";
            else if (type.isPrice()) response = "Those prices would work";
            else if (type.isUrl()) response = "Here are some links";
            else if (type.isQuantity()) response = "Those are the quantities";
            else if (type.isLocation()) response = "here are some locations";
            else response="Here is the list";
        }
        else {
            if (type.isTime()) response = "How about this time?";
            else if (type.isPrice()) response = "This is the price";
            else if (type.isUrl()) response = "here is the link";
            else if (type.isQuantity()) response = "that many";
            else if (type.isLocation()) response = "Here is the location";
            else response="this one";
        }
        for (int i = 0; i < nr; i++) {
            DataValue dv = null;
            if (type.isTime()) {
                long now = System.currentTimeMillis();
                long delta = 1000 * 86000 * 5; // up to 5 days in the future
                long time = now + rand(delta);
                dv = DataValue.createDate(name, new java.util.Date(time));
                
            }
            else if (type.isPrice()) {
                dv = DataValue.createPrice(name, Math.random()*1000);
                
            }
            else if (type.isQuantity() || type.isInt()) {
                dv = DataValue.createPrice(name, rand(40));
            }
            else if ( type.isUrl()) {
                dv = DataValue.create(name, "http://some.url.com");
            }
            else if (type.isLocation()) {
                dv = DataValue.create(name, rand(1000000)+"/"+rand(10000000));             
            }
            else if (type.isText()) {
                dv = DataValue.create(name, "Item "+i);
            }
            values.add(dv);
        }
        data.setValues(values);
        return response;
    }

    private int rand(int max) {
        return (int) (Math.random() * max);
    }

    private long rand(long max) {
        return (long) (Math.random() * max);
    }

    private boolean contains(String[] list, String what) {
        for (String item : list) {
            if (item.equalsIgnoreCase(what)) {
                return true;
            }
        }
        return false;
    }

    private String pick(String[] list) {
        int which = (int) Math.floor(Math.random() * list.length);
        return list[which];
    }

    public static void test() {
        JohnnyMeton w = new JohnnyMeton();
        w.processCommand(1, "say", "chantal", null, null);
    }

    @Override
    public User getMe() {
        User xmppUser = new User("JohnnyMeton");
        return xmppUser;
    }

    public static void main(String[] args) {

//        {"id":"JohnnyMeton","name":"JohnnyMeton","password":"JohnnyMeton","email":"","phone":"","bot":false,
        //  "contacts":null,"message_transport_type":""},
        SystemBot bot = new SystemBot(new JohnnyMeton());

        //test();
    }

}
