/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.server.bots;

import com.auto.model.InputData;
import com.auto.server.BotMsg;
import java.util.List;

/**
 *
 * @author Chantal
 */
public class Ping extends AbstractBot {

    @Override
    public BotMsg processCommand(int portid, String statename, String from,List<InputData> inputdatas, String message) {       
        // just returns the same data, and Ping as message
        return new BotMsg("Result", "Ping", inputdatas);
    }
   
}
