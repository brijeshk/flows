/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.server.bots;

import com.auto.model.DataType;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.server.BotMsg;
import com.auto.server.SystemBot;
import com.auto.server.utils.ImageUtils;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Chantal
 */
public class Comics extends AbstractBot {

    public Comics() {
        super();
        try {

            me = loadMyDataModel("comics");
            man = FlowManager.getManager();
        } catch (Exception ex) {
            err("was not able to load my data model: " + ex);
            Logger.getLogger(Comics.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    @Override
    public BotMsg processCommand(int portid, String statename, String from, List<InputData> inputdatas, String message) {

        InputData output = null;

        String type = super.getValueWithName("Type", inputdatas);
        if (type == null) {
            type = "dilbert";
        }

        String responsetype = super.getValueWithName("Response", inputdatas);
        if (responsetype == null) {
            responsetype = "image";
        }

        type = type.toLowerCase();
        String url = null;
        Date d = new Date(System.currentTimeMillis());
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        int year = cal.get(Calendar.YEAR);
        String month = "" + cal.get(Calendar.MONTH);
        String day = "" + cal.get(Calendar.DAY_OF_MONTH);

        p("Date is: " + d + ", day=" + day);
        if (day.length() < 2) {
            day = "0" + day;
        }
        if (month.length() < 2) {
            month = "0" + month;
        }
        String base64 = null;
        InputData result = null;
        if (type.equalsIgnoreCase("garfield")) {
            url = "http://garfield.com/uploads/strips/" + year + "-" + month + "-" + day + ".jpg";
        } else if (type.startsWith("dilbert")) {
            url = "http://dilbert.com/strips/comic/" + year + "-" + month + "-" + day + "/";
            try {
                String page = super.get(url);
                //p("Got page: "+page);
                Document doc = Jsoup.parse(page);
                Elements el = doc.getElementsByClass("STR_Image");
                Elements images = el.get(0).getElementsByTag("img");
                Element img = images.first();
                url = "http://dilbert.com" + img.attr("src");
            } catch (Exception e) {
                err("Could not get dilbert image: " + e);
                e.printStackTrace();
                result = super.getDefaultError("Could not read the dilber page: " + e.getMessage());
            }
            p("Got dilbert url: " + url);
        }

        if (url != null) {
            p("Reading url " + url);
            if (responsetype.equalsIgnoreCase("url")) {
                result = super.getDefaultResult(url);
                return new BotMsg("Response", type + " comics for today:", result);
            } else {
                try {
                    BufferedImage image = ImageIO.read(new URL(url));
                    if (image != null) {
                        double percent = 400.0 / image.getWidth();
                        p("Scaling image " + percent + " percent");
                        image = ImageUtils.getScaledImage(image, percent);
                        base64 = ImageUtils.encodeToString(image);
                    } else {
                        p("Got no image");
                        result = super.getDefaultError("Could not get an image for today");
                    }
                } catch (IOException ex) {
                    p("Cold not get image from url " + url);
                    Logger.getLogger(Comics.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        if (base64 != null) {
            result = super.getDefaultResult(base64);
            result.setType(DataType.Image);
        } else {
            result = super.getDefaultError("Could not get an image for today");
        }
        p("Got result: " + result);
        /// do something with it
        return new BotMsg("Response", type + " comics for today:", result);
    }

    public static void test() {
        Comics bot = new Comics();
        bot.processCommand(0, "get", "chantal", null, null);
    }

    public static void main(String[] args) {
        SystemBot bot = new SystemBot(new Comics());
        //test();
    }

}
