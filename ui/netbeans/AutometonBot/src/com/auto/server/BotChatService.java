/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auto.server;

import com.auto.model.Atom;
import com.auto.model.AtomData;
import com.auto.model.Flow;
import com.auto.model.FlowData;
import com.auto.model.FlowManager;
import com.auto.model.InputData;
import com.auto.model.State;
import com.auto.model.User;
import com.auto.net.ChatConnector;
import com.auto.net.FlowChatListener;
import com.auto.net.FlowChatService;
import com.auto.utils.FileToolsIF;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chantal
 */
public class BotChatService extends FlowChatService {

    BotIF bot;

    public BotChatService(final BotIF bot, ChatConnector connector, User user, FileToolsIF ft) {
        super(connector, user, ft);
        this.bot = bot;
    }

    public List<InputData> convert(String json) {
        return InputData.fromJsonArray(json, new State("Result"));
    }

    @Override
    public void processFlowMessages(List<Flow> flows) {
        FlowManager man = FlowManager.getManager();
        if (man == null || flows == null) {
            return;
        }
        if (!man.hasPorts() || !man.hasUsers()) {
            try {
                bot.loadMyDataModel();
            } catch (Exception ex) {
                Logger.getLogger(BotChatService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        p("BotChatService: processFlowMessages " + flows);
        for (Flow flow : flows) {
            Atom last = flow.getLastAtom();

            FlowData fd = flow.getFlowData();
            p("BotChatService: flowdata: " + fd);
            // just process last atom
            AtomData ad = last.getAtomData();

            List<InputData> outputdatas = convert(ad.getData());
            
            
            BotMsg botmsg = bot.processCommand(flow.getPort().getId(), fd.getState(), ad.getState(), outputdatas, ad.getBody());
            AtomData reply  = new AtomData();
            reply.setCrud_flag("c");
            reply.setPort_id(ad.getPort_id());
            reply.setBody(botmsg.msg);
            reply.setSender(man.getMe().getId());
            reply.setModified(System.currentTimeMillis());
            reply.setState(botmsg.statename);
            reply.setData(InputData.toJson(botmsg.outputdatas));
            p("BotChatService: botmsg result: uuid=" + fd.getUuid() + ", state=" + botmsg.statename + ", msg=" + botmsg.msg + ", portid=" + ad.getPort_id() + ", atom=" + reply);
           
            List<AtomData> replylist = new ArrayList<AtomData>();
            replylist.add(reply);
            fd.setAtomlist(replylist);
            p("BotChatService: sending response, atomlist is:" + fd.getAtomlist());
            List<User> users = flow.getTargetUsers(false);
            send(users, fd, replylist);

        }
        // at end send 
    }

}
