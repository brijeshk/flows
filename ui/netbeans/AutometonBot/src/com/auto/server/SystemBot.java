/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.auto.server;

import com.auto.db.SqlListeDesktopHelper;
import com.auto.model.FlowManager;
import com.auto.model.User;
import com.auto.net.AbstractChatService;
import com.auto.net.FlowChatService;
import com.auto.net.WebService;
import com.auto.net.WebSocketConnector;
import com.auto.utils.FileTools;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chantal
 */
public class SystemBot {
    private static final Logger log = Logger.getLogger(SystemBot.class.getName());
    public static final String DEFAULT_PACKAGE = "com.auto.server.bots";
    AbstractChatService service;
    BotIF bot;
    User me;
    
    public SystemBot(BotIF bot) {
       this.bot = bot;
       me = bot.getMe();
       start();
    }
    public SystemBot(String botname) {
        this(new User(botname),DEFAULT_PACKAGE, botname );
    }
        
    public SystemBot(User xmppUser, String pack, String botname) {
        this.me = xmppUser;
        try {
            bot = (BotIF)Class.forName(pack+"."+botname).newInstance();
        } catch (Exception ex) {
            Logger.getLogger(SystemBot.class.getName()).log(Level.SEVERE, null, ex);
        }
        start();
    }
    private void start() {
        WebService serv = new WebService(me, FileTools.getFileTools());
        
        FlowManager man = FlowManager.getManager(new SqlListeDesktopHelper(
                        "chantaltest"), FileTools.getFileTools());

        man.login(me);
        WebSocketConnector conn = new WebSocketConnector("ws://autometon.com:5000/websocket/"+me.getId());
        conn.setChatServiceListener(new BotChatService(bot, conn, me, FileTools.getFileTools()));
        try {
            man.loginService(conn);
        } catch (Exception ex) {
            Logger.getLogger(SystemBot.class.getName()).log(Level.SEVERE, null, ex);
        }
        p("Connected to websocket as user "+me.getId()+" and connected it with bot "+bot.getClass().getName());
        justWait();
    }
    private void justWait() {
        while (true) {
            try {
                Thread.sleep(100);
            }
            catch (Exception e) {}
        }
    }
    public static void main(String[] args) {
        String jid = "Comics";
        String pwd = null;
        String pack = DEFAULT_PACKAGE;
        if (args != null && args.length>0) {
            for (int i = 0; i+1 < args.length; i+=2) {
                String key  = args[i].toLowerCase();
                String val = args[i+1];
                if (key.startsWith("-")) key = key.substring(1);
                if (key.startsWith("-")) key = key.substring(1);
                if (key.startsWith("j")) {
                    jid = val.trim();
                    
                }
                else if (key.startsWith("pac") || key.startsWith("pk")) {
                    pack = val.trim();
                }
                else if (key.startsWith("pas") || key.startsWith("pw")) {
                    pwd = val.trim();
                }
            }
        }
        
        User user = new User(jid.toLowerCase());
        if (pwd != null) user.setPassword(pwd);
        SystemBot bot = new SystemBot(user, pack, jid);
    }
    
     private static void p(String s) {
        log.info("SystemBot: "+s);
    }
}
