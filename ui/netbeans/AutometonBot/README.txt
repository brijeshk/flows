 ================== STARTING A BOT ============
 From Java:
 SystemBot bot = new SystemBot(new Weather());
 
 From the command line: 
 java com.auto.server.System.Bot -j weather
 
 java com.auto.server.System.Bot -j weather -pw weather
 
 java com.auto.server.System.Bot -j weather -package com.custom.mybots
 
 ================== IMPLEMENTING A BOT ============
 
 public class Ping extends AbstractBot {

    @Override
    public BotMsg processCommand(String from, String jsondata, String message) {       
        // just returns the same data, and Ping as message
        return new BotMsg("Ping", jsondata);
    }
   
}
