#!/bin/bash
if [ "$#" -ne 2 ]; then
   echo "Usage: $0 reseturl email"
   exit 1
fi
RESETURL=http://autometon.com:5000/$1
EMAIL=$2
echo -e "Looks like you forgot your password? Click here to reset:\n$RESETURL\nAfter resetting, login with the password same as your userid, and change it immediately." | mail -s "AutoMeton: Password Reset Request" -a "From: admin@autometon.com" $EMAIL
EC=$?
echo "Sending password reset mail to $EMAIL: exitcode=$EC"
exit $EC
