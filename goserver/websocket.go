package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

// userSocketMap is a map of user to websocket connections.
// If an entry exists in this map, it means the user is online and has a live connection.
// This map is used to notify online users of messages targeted to them.
var userSocketMap map[string]*websocket.Conn

func initWebSocketsHandler() {
	userSocketMap = make(map[string]*websocket.Conn)
}

// socketListener is a read loop listening to messages on an open websocket.
// This is primarily needed to detect socket close so we can update the global map.
func socketListener(userid string, c *websocket.Conn) {
	for {
		if _, _, err := c.NextReader(); err != nil {
			log.Println("Error from websocket read: ", err.Error())
			c.Close()
			log.Println("Closed websocket connection for user " + userid)
			break
		}
	}
	delete(userSocketMap, userid)
}

// HandleWebSocketConnection creates a new websocket connection for a given user.
// Kicks off a socket listener goroutine.
func HandleWebSocketConnection(c *gin.Context) {
	userid := c.Params.ByName("uid")
	if _, err := GetUser(userid, false); err != nil {
		log.Println("Invalid userid to initiate websocket: ", err.Error())
		return
	}
	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println(err)
		return
	}
	userSocketMap[userid] = conn
	log.Printf("Websocket connection established with client %v for user %s\n", conn.RemoteAddr, userid)
	go socketListener(userid, conn)
	go sendCachedMessages(userid)
}

// SendCachedMessages delivers all cached messages for a given user via websocket
func sendCachedMessages(userid string) (err error) {
	atoms, err := GetCachedMessages(userid)
	if err != nil {
		log.Println("Error getting cached messages", err.Error())
		return
	}
	if len(atoms) == 0 {
		return
	}
	cm := ControlMessage{Type: "EventMsg", Data: atoms}
	err = SendMessageOnWebSocket(userid, cm)
	if err != nil {
		log.Println("Error sending cached messages", err.Error())
		return
	}
	return
}

// SendMessageOnWebSocket sends a JSON message on a live websocket for a given user.
// If no live socket exists for that user, nothing is done.
func SendMessageOnWebSocket(userid string, data interface{}) (err error) {
	conn, ok := userSocketMap[userid]
	if ok == false || conn == nil {
		log.Printf("User %s is not online, nothing to do\n", userid)
		return
	}
	if err = conn.WriteJSON(data); err != nil {
		log.Println("Error sending message on websocket for user %s: %s\n", userid, err.Error())
		return
	}
	log.Println("Message sent on websocket for user ", userid)
	return
}

// IsUserOnline returns a boolean indicating if there's a websocket open
// for a given userid or not. Essentially says whether the user is online or not.
func IsUserOnline(userid string) bool {
	c, ok := userSocketMap[userid]
	return ok && (c != nil)
}

// Logs number (and names) of online users
func PrintWebSocketStats(listUsers bool) {
	log.Println("Number of online users: ", len(userSocketMap))
	if listUsers == true {
		for u, _ := range userSocketMap {
			log.Println(u)
		}
	}
}
