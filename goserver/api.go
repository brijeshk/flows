package main

import (
	"crypto/rand"
	"errors"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/alexjlockwood/gcm"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

var session *mgo.Session = nil
var db *mgo.Database = nil
var userPushTokenMap map[string]string

type bsonMArray []bson.M

func stringArrayContains(array []string, elem string) bool {
	for _, a := range array {
		if a == elem {
			return true
		}
	}
	return false
}

func stripChars(str, chr string) string {
	return strings.Map(func(r rune) rune {
		if strings.IndexRune(chr, r) < 0 {
			return r
		}
		return -1
	}, str)
}

func generateRandomToken() string {
	b := make([]byte, 8)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func InitDbSession() {
	log.Println("Connecting to db localhost")
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)
	db = session.DB("flows")
	userPushTokenMap = make(map[string]string)
	log.Println("Connected to db successfully")
}

func CloseDbSession() {
	if session != nil {
		session.Close()
		session = nil
		log.Println("Closed db session")
	}
}

func GetUser(userid string, includeBotContacts bool) (user User, err error) {
	err = db.C("user").Find(bson.M{"_id": userid}).One(&user)
	if err != nil {
		log.Printf("User %s doesn't exist : %s\n", userid, err.Error())
		return
	}
	if includeBotContacts == true {
		var bots []User
		err = db.C("user").Find(bson.M{"bot": true}).All(&bots)
		if err != nil {
			log.Println("Error getting bot users: ", err.Error())
			return
		}
		for _, b := range bots {
			c := Contact{AcceptPorts: false}
			user.Contacts[b.Id] = c
		}
		// add the fake helper user
		c := Contact{AcceptPorts: false}
		user.Contacts[HelperUser] = c
	}
	return
}

func AuthorizeUserInDb(userid string, password string) bool {
	var user User
	err := db.C("user").Find(bson.M{"_id": userid}).One(&user)
	if err != nil {
		log.Printf("User %s doesn't exist : %s\n", userid, err.Error())
		return false
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err == nil {
		return true
	}
	// TODO: remove direct comparison, left for legacy
	if user.Password == password {
		return true
	} else {
		return false
	}
}

func GetUsers() (users []User, err error) {
	err = db.C("user").Find(bson.M{}).All(&users)
	if err != nil {
		log.Println("Error getting users : " + err.Error())
	}
	// mask passwords
	for _, u := range users {
		u.Password = ""
	}
	return
}

func FindUsersByKeywords(keywords []string) (users []User, err error) {
	for _, k := range keywords {
		var u User
		// canonicalize phone number
		x := stripChars(k, "+() -.#")
		if len(x) == 10 && k[0] != '+' {
			// assume US code if none exists
			x = "1" + x
		}
		if e := db.C("user").Find(bson.M{"phone": x}).One(&u); e == nil {
			u.Password = ""
			users = append(users, u)
		}
		// TODO: find by phone number, email as well
	}
	return
}

func GetPort(portid string) (port Port, err error) {
	err = db.C("port").Find(bson.M{"_id": portid}).One(&port)
	if err != nil {
		log.Printf("Error getting portid %d : %s\n", portid, err.Error())
	}
	return
}

func GetPortsExposedByUserToUser(byUserId string, toUserId string) ([]Port, error) {
	var ports []Port
	var byUser User
	err := db.C("user").Find(bson.M{"_id": byUserId}).One(&byUser)
	if err != nil {
		log.Printf("User %s doesn't exist\n", byUserId)
		return nil, err
	}
	var portQuery bson.M
	if byUser.Bot == true {
		// bot users don't expose default ports like General
		portQuery = bson.M{"creator": byUserId}
	} else {
		//portQuery = bson.M{"$or": bsonMArray{{"creator": byUserId}, {"exposedBy": byUserId}, {"exposedBy": "all"}}}
		portQuery = bson.M{"creator": byUserId}
	}
	err = db.C("port").Find(portQuery).All(&ports)
	if err != nil {
		log.Println("Error finding ports by user: ", err.Error())
		return nil, err
	}
	// if toUser is same as byUser, return all ports (this then is a request for all ports exposed by a given user)
	if toUserId == "" || toUserId == byUserId {
		return ports, nil
	}
	// filter ports that are either explicitly exposed toUser +
	// those that are implicitly exposed (by virtue of "Contacts")
	_, toUserIsContactOfByUser := byUser.Contacts[toUserId]
	var filteredPorts []Port
	for _, p := range ports {
		if stringArrayContains(p.VisibleToUsers, "world") || // visible to world
			stringArrayContains(p.VisibleToUsers, toUserId) || // visible to toUser specifically
			(stringArrayContains(p.VisibleToUsers, "contacts") && toUserIsContactOfByUser == true) { // visible to byUser's contacts
			if p.Disabled == false {
				filteredPorts = append(filteredPorts, p)
			}
		}
	}
	return filteredPorts, nil
}

func GetDefaultPorts() ([]Port, error) {
	var ports []Port
	err := db.C("port").Find(bson.M{"creator": "system"}).All(&ports)
	if err != nil {
		log.Println("Error finding system-default ports: ", err.Error())
		return nil, err
	}
	return ports, nil
}

func GetPortsVisibleToUser(userid string, keyword string, publicPorts bool) ([]Port, error) {
	user, err := GetUser(userid, true)
	if err != nil {
		return nil, err
	}
	var ports []Port
	// find all ports created by user + visible explicitly to user + visible to world
	query := bson.M{"$or": bsonMArray{{"creator": userid}, {"visibleTo": userid}, {"visibleTo": "world"}}}
	err = db.C("port").Find(query).All(&ports)
	if err != nil {
		log.Printf("Error running visibleTo query: %s\n", err.Error())
		return nil, err
	}
	var filteredPorts []Port
	for _, p := range ports {
		// TODO: should we include public ports exposed by non-contact users?
		_, isContact := user.Contacts[p.Creator]
		if publicPorts || isContact {
			if keyword == "" || strings.Contains(strings.ToUpper(p.Name), strings.ToUpper(keyword)) || strings.Contains(strings.ToUpper(p.Creator), strings.ToUpper(keyword)) {
				filteredPorts = append(filteredPorts, p)
			}
		}
	}
	return filteredPorts, nil
}

func GetPortTemplates() ([]PortTemplate, error) {
	var ports []PortTemplate
	err := db.C("template").Find(bson.M{}).All(&ports)
	if err != nil {
		log.Println("Error getting port templates: ", err.Error())
		return nil, err
	}
	return ports, nil
}

func getNewIntegerId(counter string) (id int, err error) {
	type counterType struct {
		N int `bson:"n"`
	}
	var doc counterType
	change := mgo.Change{
		Update:    bson.M{"$inc": bson.M{"n": 1}},
		ReturnNew: true,
	}
	_, err = db.C("counters").Find(bson.M{"_id": counter}).Apply(change, &doc)
	if err != nil {
		log.Println("Error updating counter ", counter, " : ", err.Error())
		return
	} else {
		id = doc.N
	}
	return
}

func CreatePort(port *Port) (err error) {
	port.Id = port.Creator + "." + port.Name
	err = db.C("port").Insert(port)
	if err != nil {
		log.Println("Error creating new port: ", err.Error())
	}
	return
}

func UpdatePort(port *Port) (err error) {
	err = db.C("port").UpdateId(port.Id, port)
	if err != nil {
		log.Println("Error updating port: ", err.Error())
	} else {
		log.Println("Updated port ", port.Id)
	}
	return
}

func DeletePort(pid string) (err error) {
	err = db.C("port").RemoveId(pid)
	if err != nil {
		log.Println("Error deleting port: ", err.Error())
	} else {
		log.Println("Deleted port ", pid)
	}
	return
}

func CreateUser(user *User) (err error) {
	if user.Id == "" {
		err = errors.New("Empty userid")
		log.Println("Error creating user: ", err.Error())
		return
	}
	if user.Id == "all" {
		err = errors.New("all is a reserved id")
		log.Println("Error creating user: ", err.Error())
		return
	}
	_, err = GetUser(user.Id, false)
	if err == nil {
		err = errors.New("Userid " + user.Id + " already exists. Please use a different userid.")
		return
	}

	log.Println("Inserting user record into db")

	// encrypt password using bcrypt
	// TODO: turn on
	/*
		hp, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)
		if err != nil {
			log.Println("Error encrypting password: ", err.Error())
			return
		}
		user.Password = string(hp)
	*/

	err = db.C("user").Insert(user)
	if err != nil {
		log.Println("Error creating new user in db: ", err.Error())
		return
	}
	if user.Transport == "xmpp" {
		err = createXMPPUser(user)
	}
	// create default ports for user
	defaultPorts := MakeDefaultPorts()
	for _, p := range defaultPorts {
		p.Creator = user.Id
		CreatePort(&p)
	}
	return
}

func UpdateUser(user *User) (err error) {
	err = db.C("user").UpdateId(user.Id, user)
	if err != nil {
		log.Println("Error updating user: ", err.Error())
	} else {
		log.Println("Updated user ", user.Id)
	}
	return
}

//
// UpdateUserPushToken updates a user's registered device token for push notifications.
// When app starts up, it registers with GCM, receives a push token, and sends to server.
// Server stores this and uses it for later notifications to user.
//
func UpdateUserPushToken(userid string, token string) (err error) {
	userPushTokenMap[userid] = token
	log.Println("Updated user push token for " + userid + " : " + token)
	err = nil
	return
}

//
// ResetUserPassword resets the password for a given user iff the presented activation token matches the one in the db.
// The token must have been generated by an earlier call to GenerateUserToken and notified to user via email.
//
func ResetUserPassword(uid string, token string) (err error) {
	u, err := GetUser(uid, false)
	if err != nil {
		return
	}
	if u.Token == "" {
		err = errors.New("No activation token present for user!")
		log.Println(err.Error())
		return
	}
	if u.Token != token {
		log.Println("Token doesn't match! Malicious request detected")
		err = errors.New("Reset password request token is invalid!")
		return
	}
	// TODO: generate random password or set to phone number
	u.Password = uid
	// invalidate the token after one use
	u.Token = ""
	err = UpdateUser(&u)
	log.Println("*** Reset password for user " + uid)
	return
}

//
// GenerateUserToken generates a random expiring token for a given userid, and notifies user by email.
// A subsequent activation or password reset request must present this token before it has expired.
//
func GenerateUserToken(uid string) (err error) {
	u, err := GetUser(uid, false)
	if err != nil {
		return
	}
	if u.Email == "" {
		err = errors.New("No email configured for user. Password reset needs a valid email address")
		log.Println(err.Error())
		return
	}
	u.Token = generateRandomToken()
	// TODO: also save timestamp
	err = UpdateUser(&u)

	// TODO: send email to user with link to reset password with generated token
	cmd := exec.Command("./send-password-reset-mail.sh", "resetpass/"+uid+"?token="+u.Token, u.Email)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
	if err != nil {
		log.Println("Error: ", err.Error())
		return
	}
	log.Println("Reset password email sent to " + u.Email)
	return
}

func createXMPPUser(user *User) (err error) {
	xmppJid := strings.Replace(user.Id, "@gmail.com", "_gmail_com", -1)
	err = exec.Command("ejabberdctl", "register", xmppJid, "localhost", xmppJid).Run()
	if err != nil {
		log.Println("Error: ", err.Error())
		return
	}
	return
}

func deleteIthFromSlice(s []string, i int) (d []string) {
	if i < len(s)-1 {
		d = append(s[:i], s[i+1:]...)
	} else {
		d = s[:i]
	}
	return
}

func AddOrRemoveUserConnection(userid1 string, userid2 string, contact Contact, add bool) (err error) {
	user1, err := GetUser(userid1, false)
	if err != nil {
		return
	}
	_, err = GetUser(userid2, false)
	if err != nil {
		return
	}
	_, foundInContacts := user1.Contacts[userid2]
	if foundInContacts {
		// found userid2 in userid1's contacts
		if add == true {
			// update
			user1.Contacts[userid2] = contact
		} else {
			delete(user1.Contacts, userid2)
		}
	} else {
		// could not find userid1 in userid1's connections
		if add == true {
			user1.Contacts[userid2] = contact
		} else {
			log.Printf("user %s already not in user %s contacts", userid2, userid1)
			return
		}
	}
	err = db.C("user").UpdateId(userid1, user1)
	if err != nil {
		log.Printf("Error updating user's contacts: ", err.Error())
		return
	}
	return
}

func CreateGroup(group *Group) (err error) {
	err = db.C("userGroup").Insert(group)
	// TODO: send notifications to group members if any
	return
}

func GetGroup(groupid string) (group Group, err error) {
	err = db.C("userGroup").Find(bson.M{"_id": groupid}).One(&group)
	if err != nil {
		log.Printf("Group %s doesn't exist : %s\n", groupid, err.Error())
	}
	return
}

func GetGroupsByUser(userid string) (groups []Group, err error) {
	_, err = GetUser(userid, false)
	if err != nil {
		return
	}
	err = db.C("userGroup").Find(bson.M{"users": userid}).All(&groups)
	if err != nil {
		log.Println("Error finding groups by user: ", err.Error())
	}
	return
}

func GetPublicGroups(keyword string) (groups []Group, err error) {
	var q interface{}
	if keyword != "" {
		q = bson.M{"public": true, "name": bson.M{"$regex": keyword}}
	} else {
		q = bson.M{"public": true}
	}
	err = db.C("userGroup").Find(q).All(&groups)
	if err != nil {
		log.Println("Error getting public groups: ", err.Error())
	}
	return
}

func CreateEvent(e *Event) (err error) {
	e.Modified = time.Now()
	err = db.C("event").Insert(e)
	if err != nil {
		log.Println("Error creating new event: ", err.Error())
		return
	}
	log.Println("Created event ", e.Id)
	// send notifications to all
	cm := ControlMessage{Type: "SyncEvents"}
	for _, u := range e.Invitees {
		SendMessageOnWebSocket(u, cm)
	}
	return
}

func GetEvent(eid string) (e Event, err error) {
	err = db.C("event").Find(bson.M{"_id": eid}).One(&e)
	if err != nil {
		log.Println("Error getting event: ", err.Error())
		return
	}
	return
}

func UpdateEvent(e *Event) (err error) {
	// TODO: check Modified against db version to ensure no conflict
	// if different, then both have changed, and we must reject
	e.Modified = time.Now()
	err = db.C("event").UpdateId(e.Id, e)
	if err != nil {
		log.Println("Error updating event: ", err.Error())
	} else {
		log.Println("Updated event ", e.Id)
		// send notifications to everyone, including owner
		cm := ControlMessage{Type: "SyncEvents"}
		for _, u := range e.Invitees {
			SendMessageOnWebSocket(u, cm)
		}
		SendMessageOnWebSocket(e.Owner, cm)
	}
	return
}

func UpdateEventAcceptance(eid string, userid string, yesNoMaybe string) (err error) {
	// TODO: make this block concurrency-safe per event
	e, err := GetEvent(eid)
	if err != nil {
		return
	}
	if e.InviteeStatus == nil {
		e.InviteeStatus = make(map[string]string)
	}
	e.InviteeStatus[userid] = yesNoMaybe
	// update overall event status
	numYes := 0
	for _, v := range e.InviteeStatus {
		if v == "yes" {
			numYes++
		}
	}
	if numYes >= e.Quorum {
		e.State = "Scheduled"
	} else {
		e.State = "Pending"
	}
	e.Modified = time.Now()
	err = db.C("event").UpdateId(e.Id, e)
	if err != nil {
		log.Println("Error updating event: ", err.Error())
	} else {
		log.Println("Updated event ", e.Id)
	}
	// send notifications to everyone, including owner, to resync
	cm := ControlMessage{Type: "SyncEvents"}
	for _, u := range e.Invitees {
		SendMessageOnWebSocket(u, cm)
	}
	SendMessageOnWebSocket(e.Owner, cm)
	return
}

func DeleteEvent(eid string) (err error) {
	err = db.C("event").RemoveId(eid)
	if err != nil {
		log.Println("Error deleting event: ", err.Error())
	} else {
		log.Println("Deleted event ", eid)
	}
	return
}

// DeletePastEvents deletes events in the db that are past
// Past events cannot be modified, so there's no reason to keep them on server
// Messages can be sent on past events however. If client has event, message will show up
func DeletePastEvents() {
	log.Println("Starting past event deletor routine...")
        var oneWeek = time.Hour * 24 * 7
	for _ = range time.Tick(time.Hour) {
		var timeNow = time.Now()
		// find events that have a start-time one week before now
		var events []Event
		query := bson.M{"startTime": bson.M{"$lt": timeNow.Add(-oneWeek)}}
		err := db.C("event").Find(query).All(&events)
		if err != nil {
			log.Println("Error querying past events: ", err.Error())
		}
		for _, e := range events {
			// is event timeless?
			// is event currently happening?
			if e.StartTime.IsZero() == false && e.EndTime.Before(timeNow) {
				// no
				DeleteEvent(e.Id)
			}
		}
	}
	log.Println("Finished past event deletor routine")
}

// SendMessageOnEvent sends a message (aka eventatom) to all receivers.
// If receiver is online, sends via websocket, else caches in server for later delivery
func SendMessageOnEvent(a *EventAtom) (err error) {
	// send message to all receivers on websocket
	for i, r := range a.Receivers {
		if r == a.Sender {
			continue
		}
		sent := false
		if IsUserOnline(r) {
			log.Println("Sending message on websocket to online user ", r)
			cm := ControlMessage{Type: "EventMsg", Data: []EventAtom{*a}}
			err := SendMessageOnWebSocket(r, cm)
			if err == nil {
				sent = true
			}
		}
		// save message for all users that we didn't send it to
		// TODO: use NSQ for message queue scaling !!
		if sent == false && a.State == "TxtMsg" {
			t1 := a.Id
			t2 := a.Receivers
			a.Id = a.Id + "_" + strconv.Itoa(i)
			a.Receivers = []string{r}
			err = db.C("atom").Insert(a)
			a.Id = t1
			a.Receivers = t2
			if err != nil {
				log.Println("Error creating new atom: ", err.Error())
			}
			// deliver a push notification to receiver
			// this notification contains no data, just a 'Send-to-Sync' ping
			SendPushNotification(r, a)
		}
	}
	return
}

func GetEventsModifiedSince(userId string, sinceTime time.Time) (events []Event, err error) {
	query := bson.M{"$or": bsonMArray{{"invitees": userId}, {"owner": userId}}}
	query["modified"] = bson.M{"$gte": sinceTime}

	// find events that have been updated since given time that user is part of
	err = db.C("event").Find(query).All(&events)
	if err != nil {
		log.Println("Error querying events: ", err.Error())
	}
	return
}

func GetCachedMessages(userId string) (atoms []EventAtom, err error) {
	err = db.C("atom").Find(bson.M{"receivers": userId}).All(&atoms)
	if err != nil {
		log.Println("Error querying atoms: ", err.Error())
	}
	_, err = db.C("atom").RemoveAll(bson.M{"receivers": userId})
	if err != nil {
		log.Println("Error deleting cached atoms: ", err.Error())
	}
	return
}

func SendPushNotification(userid string, a *EventAtom) {
	// GCM Messaging API key
	sender := &gcm.Sender{ApiKey: "AIzaSyAd25kBjZtolclNyA1z5otAMMe3eI_5wR8"}
	token, ok := userPushTokenMap[userid]
	if ok == false {
		log.Println("User " + userid + " not registered for push")
		return
	}
	targets := []string{token}
	message := a.Sender + ": "
	if len(a.Body) <= 40 {
		message += a.Body
	} else {
		message += a.Body[:40]
	}
	dataLoad := map[string]interface{}{"SendToSync": "1", "title": "BeeWaggle", "message": message, "atom": a, "msgcnt": 1}
	msg := gcm.NewMessage(dataLoad, targets...)
	msg.TimeToLive = 10
	msg.CollapseKey = "amnotify"
	log.Println("Sending GCM push notification: ", msg)
	_, err := sender.Send(msg, 1)
	if err != nil {
		log.Println("Error sending push notification: ", err.Error())
	}
	return
}
