package main

import (
	"fmt"
	"testing"
	"time"
)

func init() {
	InitSqlDb("root:root@tcp(localhost:3306)")
}

func TestResetDb(t *testing.T) {
	ResetDb()
}

func TestInsertFlow(t *testing.T) {
	timeNow := time.Now()
	timeOneDayBefore := timeNow.AddDate(0, 0, -1)
	a0 := Atom{CrudFlag: "c", FlowId: "abc", AtomId: "0", State: "Ask", Sender: "brijesh", Data: map[string]string{"Price": "500"}, Modified: timeOneDayBefore}
	f := Flow{CrudFlag: "c",
		FlowId:   "abc",
		PortId:   "0",
		State:    "Answer",
		Users:    []string{"brijesh", "prateek"},
		Data:     map[string]string{"Price": "500"},
		Atoms:    []Atom{a0},
		Modified: timeNow,
	}
	flowDelta := []Flow{f}
	if err := ApplyFlowDelta("brijesh", flowDelta); err != nil {
		t.Error("ApplyFlowDelta failed", err.Error())
	}
	fmt.Println("Inserted flow : ", f)
}

func TestUpdateFlow(t *testing.T) {
	timeNow := time.Now()
	a1 := Atom{CrudFlag: "c", FlowId: "abc", AtomId: "1", State: "Answer", Sender: "prateek", Modified: timeNow}
	f := Flow{CrudFlag: "u",
		FlowId:   "abc",
		PortId:   "0",
		State:    "Answer",
		Users:    []string{"brijesh", "prateek"},
		Data:     map[string]string{"Price": "500"},
		Atoms:    []Atom{a1},
		Modified: timeNow,
	}
	flowDelta := []Flow{f}
	if err := ApplyFlowDelta("brijesh", flowDelta); err != nil {
		t.Error("ApplyFlowDelta failed", err.Error())
		return
	}
	fmt.Println("Updated flow with new atom : ", f)
	a1 = Atom{CrudFlag: "u", FlowId: "abc", AtomId: "1", State: "Answer", Sender: "prateek", Body: "updated now", Modified: time.Now()}
	f.Atoms = []Atom{a1}
	flowDelta = []Flow{f}
	if err := ApplyFlowDelta("brijesh", flowDelta); err != nil {
		t.Error("ApplyFlowDelta failed", err.Error())
		return
	}
	fmt.Println("Updated flow with updated atom : ", f)
}

func TestGetFlow(t *testing.T) {
	f, err := GetFlow("brijesh", "abc")
	if err != nil {
		t.Error("Error getting flow", err.Error())
		return
	}
	if len(f.Atoms) != 2 {
		t.Error("Expected 2 atoms back, got ", len(f.Atoms))
		return
	}
	if f.Atoms[1].Body != "updated now" {
		t.Error("Update didn't work fine")
		return
	}
	fmt.Println("Got flow : ", f)
}

func TestGetFlowDelta(t *testing.T) {
	flows, err := GetFlowDeltaSince("brijesh", time.Now().AddDate(0, 0, -5))
	if err != nil {
		t.Error("Error getting flow delta : ", err.Error())
		return
	}
	fmt.Println("Got flow delta: --")
	fmt.Println(flows)
}

/*
func TestDeleteFlow(t *testing.T) {
	f := Flow{CrudFlag: "d", FlowId: "abc"}
	flowDelta := []Flow{f}
	if err := ApplyFlowDelta("brijesh", flowDelta); err != nil {
		t.Error("ApplyFlowDelta failed", err.Error())
	}
	f, err := GetFlow("abc")
	if err == nil {
		t.Error("Flow NOT deleted")
	}
	fmt.Println("Deleted flow")
}

func TestPerf(t *testing.T) {
	timeNow := time.Now()
	for i := 0; i < 1000; i++ {
		fid := fmt.Sprintf("perf%d", i)
		a0 := Atom{CrudFlag: "c", FlowId: fid, AtomId: 0, State: "Ask", Sender: "brijesh", Data: map[string]string{"Price": "500"}, Modified: timeNow}
		f := Flow{CrudFlag: "c",
			FlowId:   fid,
			PortId:   0,
			State:    "Answer",
			Users:    []string{"brijesh", "prateek"},
			Data:     map[string]string{"Price": "500"},
			Atoms:    []Atom{a0},
			Modified: timeNow,
		}
		flowDelta := []Flow{f}
		if err := ApplyFlowDelta("brijesh", flowDelta); err != nil {
			t.Error("ApplyFlowDelta failed", err.Error())
		}
	}
}
*/
