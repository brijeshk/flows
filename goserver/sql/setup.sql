drop database if exists flowdb;
create database flowdb;
use flowdb;

create table flows
(
    flowid varchar(36) NOT NULL,
    userid varchar(20) NOT NULL,
    users varchar(200) NOT NULL,
    port varchar(36) NOT NULL,
    state varchar(20),
    data varchar(2048),
    modified datetime NOT NULL,
    place varchar(200) NOT NULL,
    time varchar(100) NOT NULL,
    constraint pk_userid_flowid PRIMARY KEY (userid, flowid)
);

create table atoms
(
    flowid varchar(36) NOT NULL,
    atomid varchar(16) NOT NULL,
    userid varchar(20) NOT NULL,
    sender varchar(20) NOT NULL,
    state varchar(20),
    data varchar(2048),
    body varchar(2048),
    flags varchar(5),
    modified datetime NOT NULL,
    constraint pk_userid_flowid_atomid PRIMARY KEY (userid, flowid, atomid)
);
