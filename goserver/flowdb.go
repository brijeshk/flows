package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"strings"
	"time"
)

// sqldb is a global variable containing the connection to the db.
var sqldb *sql.DB = nil

// InitSqlDb opens a database connection and stores it in the global sqldb variable.
func InitSqlDb(endpoint string) (err error) {
	sqldb, err = sql.Open("mysql", endpoint+"/flowdb?parseTime=true")
	if err != nil {
		log.Println("Error opening sql db: ", err.Error())
		panic(err.Error())
	}

	err = sqldb.Ping()
	if err != nil {
		log.Println("Error connecting to sql db: ", err.Error())
		panic(err.Error())
	}

	log.Println("Connected to sql db")
	return
}

// CloseSqlDb closes an existing database connection.
func CloseSqlDb() {
	if sqldb != nil {
		sqldb.Close()
		sqldb = nil
		log.Println("Closed sql db")
	}
}

// ResetDb erases ALL flows and atoms from the database. Use only for testing!!
func ResetDb() {
	sqldb.Exec("delete from flows")
	sqldb.Exec("delete from atoms")
	log.Println("** Deleted all flows and atoms in db! **")
}

// ApplyFlowDelta takes in a list of flow delta items (with create/update/delete flags)
// and applies it to the sql database for the given user.
func ApplyFlowDelta(userid string, flowDelta []Flow) (err error) {
	for _, fd := range flowDelta {
		switch fd.CrudFlag {
		case "c":
			err = insertFlow(userid, fd)
		case "d":
			err = deleteFlow(userid, fd.FlowId)
		case "u":
			err = updateFlow(userid, fd)
		default:
			log.Println("Unknown crud flag in flow delta: ", fd.CrudFlag)
		}
	}
	return
}

// GetFlowDeltaSince retrieves a list of flow delta items representing
// changes since a given timestamp for a given user.
// This also deletes all flows and atoms with timestamps before the requested timestamp,
// since the request for timestamp X is effectively an ack that says all messages before X were received by the client.
func GetFlowDeltaSince(userid string, sinceTime time.Time) (flows []Flow, err error) {
	_, err = sqldb.Exec("delete from flows where userid = ?  and modified <= ?", userid, sinceTime)
	if err != nil {
		log.Printf("Error deleting flows till timestamp %s : %s\n", sinceTime, err.Error())
	}
	_, err = sqldb.Exec("delete from atoms where userid = ?  and modified <= ?", userid, sinceTime)
	if err != nil {
		log.Printf("Error deleting atoms till timestamp %s : %s\n", sinceTime, err.Error())
	}
	rows, err := sqldb.Query("select * from flows where userid = ? and modified > ? order by modified desc", userid, sinceTime)
	if err != nil {
		log.Println("Error retrieving flow delta for user "+userid+" : ", err.Error())
		return
	}
	defer rows.Close()
	flows, err = scanFlowsFromRows(rows)
	return
}

// GetFlow retrieves a given flow by flowid.
func GetFlow(userId string, flowId string) (f Flow, err error) {
	rows, err := sqldb.Query("select * from flows where userid = ? and flowid = ?", userId, flowId)
	if err != nil {
		log.Printf("Error retrieving flow %s for user %s : %s\n", flowId, userId, err.Error())
		return
	}
	defer rows.Close()
	flows, err := scanFlowsFromRows(rows)
	if err != nil {
		return
	}
	if len(flows) != 1 {
		log.Printf("Found %d flows for flowid %s\n", len(flows), flowId)
		err = errors.New("Couldn't find unique row for flowid " + flowId)
		return
	}
	f = flows[0]
	return
}

func scanFlowsFromRows(rows *sql.Rows) (flows []Flow, err error) {
	for rows.Next() {
		var f Flow
		var users string
		var data sql.NullString
		var state sql.NullString
		var userid string
		err = rows.Scan(&f.FlowId, &userid, &users, &f.PortId, &state, &data, &f.Modified, &f.Place, &f.Time)
		if err != nil {
			log.Println("Error scanning flow : ", err.Error())
			return
		}
		f.Users = strings.Split(users, ",")
		if state.Valid {
			f.State = state.String
		}
		if data.Valid {
			byteData := []byte(data.String)
			json.Unmarshal(byteData, &f.Data)
		}
		// get constituent atoms
		f.Atoms, err = getAtoms(userid, f.FlowId)
		if err != nil {
			log.Println("Error getting atoms for flow : "+f.FlowId, err.Error())
			return
		}
		// TODO: identify correct flag
		f.CrudFlag = "c"
		flows = append(flows, f)
	}
	return
}

// insertFlow inserts a new flow into the db.
func insertFlow(userid string, f Flow) (err error) {
	// update flow attributes
	var strData string
	if f.Data != nil {
		bytesData, _ := json.Marshal(f.Data)
		strData = string(bytesData)
	}
	_, err = sqldb.Exec("insert into flows (flowid, userid, port, users, state, data, modified, place, time) values(?, ?, ?, ?, ?, ?, ?, ?, ?)",
		f.FlowId, userid, f.PortId, strings.Join(f.Users, ","), f.State, strData, f.Modified, f.Place, f.Time)
	if err != nil {
		log.Println("Error inserting flow: ", err.Error())
		return
	}
	// insert atoms into atoms table
	err = applyAtomDelta(userid, f.FlowId, f.Atoms)
	return
}

// updateFlow updates an existing flow in db, by changing its attributes and creating/deleting/updating constituent atoms.
func updateFlow(userid string, fd Flow) (err error) {
	var strData string
	if fd.Data != nil {
		bytesData, _ := json.Marshal(fd.Data)
		strData = string(bytesData)
	}
	_, err = GetFlow(userid, fd.FlowId)
	if err != nil {
		// flow doesn't exist, insert it
		err = insertFlow(userid, fd)
		if err != nil {
			log.Println("Error saving flow: ", err.Error())
			return
		}
	} else {
		// update flow attributes
		_, err = sqldb.Exec("update flows set users=?, state=?, port=?, data=?, modified=?, place=?, time=? where flowid=? and userid=?",
			strings.Join(fd.Users, ","), fd.State, fd.PortId, strData, fd.Modified, fd.Place, fd.Time, fd.FlowId, userid)
		if err != nil {
			log.Println("Error updating flow : ", err.Error())
			return
		}
		// update atoms table
		err = applyAtomDelta(userid, fd.FlowId, fd.Atoms)
	}
	return
}

// UpdateFlowFlags updates an existing flow's atoms' flags in db.
// flags signify unread/tagged status for individual atoms.
func UpdateFlowFlags(userid string, fd Flow) (err error) {
	for _, ad := range fd.Atoms {
		if err = updateAtomFlags(userid, fd.FlowId, ad); err != nil {
			return
		}
	}
	return
}

// applyAtomDelta takes in a list of atom delta items, and updates the atoms table.
func applyAtomDelta(userid string, flowId string, atomDelta []Atom) (err error) {
	for _, ad := range atomDelta {
		switch ad.CrudFlag {
		case "c":
			err = insertAtom(userid, flowId, ad)
		case "d":
			err = deleteAtom(userid, flowId, ad.AtomId)
		case "u":
			err = updateAtom(userid, flowId, ad)
		default:
			log.Println("Unknown crud flag in atom delta: ", ad.CrudFlag)
		}
		if err != nil {
			log.Println("Error in applying atom delta: ", err.Error())
			return
		}
	}
	return
}

// deleteFlow deletes a given flow and its constituent atoms.
func deleteFlow(userid string, flowId string) (err error) {
	_, err = sqldb.Exec("delete from flows where userid=? and flowid=?", userid, flowId)
	if err != nil {
		log.Println("Error deleting flow: ", err.Error())
		return
	}
	// delete constituent atoms
	err = deleteAtoms(userid, flowId)
	return
}

// getAtoms gets the list of atoms by userid and flowid.
func getAtoms(userid string, flowId string) (atoms []Atom, err error) {
	rows, err := sqldb.Query("select * from atoms where userid = ? and flowid = ? order by modified", userid, flowId)
	if err != nil {
		log.Println("Error retrieving atoms for flow "+flowId+": ", err.Error())
		return
	}
	defer rows.Close()
	for rows.Next() {
		var data sql.NullString
		var state sql.NullString
		var a Atom
		err = rows.Scan(&a.FlowId, &a.AtomId, &userid, &a.Sender, &state, &data, &a.Body, &a.Flags, &a.Modified)
		if err != nil {
			log.Println("Error scanning atom", err.Error())
			// keep going
			err = nil
		}
		if state.Valid {
			a.State = state.String
		}
		if data.Valid {
			byteData := []byte(data.String)
			json.Unmarshal(byteData, &a.Data)
		}
		// TODO: identify correct crudFlag
		a.CrudFlag = "c"
		atoms = append(atoms, a)
	}
	return
}

// insertAtom inserts one atom row into the atoms table.
func insertAtom(userid string, flowId string, a Atom) (err error) {
	var strData string
	if a.Data != nil {
		bytesData, _ := json.Marshal(a.Data)
		strData = string(bytesData)
	}
	// set unread flag for anyone other than sender
	if a.Sender != userid {
		a.Flags = "n"
	} else {
		a.Flags = ""
	}
	_, err = sqldb.Exec("insert into atoms (flowid, atomid, userid, sender, state, data, body, flags, modified) values(?, ?, ?, ?, ?, ?, ?, ?, ?)",
		a.FlowId, a.AtomId, userid, a.Sender, a.State, strData, a.Body, a.Flags, a.Modified)
	if err != nil {
		log.Println("Error inserting atom: ", err.Error())
		// perhaps client and server are out of sync, let's try to update atom if it already exists
		// err = updateAtom(userid, flowId, a)
		return
	}
	return
}

// updateAtom updates an atom row in the atoms table in place.
func updateAtom(userid string, flowId string, a Atom) (err error) {
	var strData string
	if a.Data != nil {
		bytesData, _ := json.Marshal(a.Data)
		strData = string(bytesData)
	}
	_, err = sqldb.Exec("update atoms set sender=?, state=?, data=?, body=?, flags=?, modified=? where userid=? and flowid=? and atomid=?",
		a.Sender, a.State, strData, a.Body, a.Flags, a.Modified, userid, flowId, a.AtomId)
	if err != nil {
		log.Println("Error updating atom: ", err.Error())
		// perhaps client and server are out of sync, let's try to insert atom if it doesn't exist
		// err = insertAtom(userid, flowId, a)
		return
	}
	return
}

// updateAtomFlags updates an atom's unread/tagged flag in the atoms table.
// This is a convenient faster function to update just an atom's flags and not all attributes.
func updateAtomFlags(userid string, flowId string, a Atom) (err error) {
	_, err = sqldb.Exec("update atoms set flags=? where userid=? and flowid=? and atomid=?",
		a.Flags, userid, flowId, a.AtomId)
	if err != nil {
		log.Println("Error updating atom flags: ", err.Error())
		return
	}
	return
}

// deleteAtom deletes a single atom given by flowId and atomId.
func deleteAtom(userid string, flowId string, atomId string) (err error) {
	_, err = sqldb.Exec("delete from atoms where userid=? and flowId=? and atomId=?", userid, flowId, atomId)
	if err != nil {
		log.Println("Error deleting atom: ", err.Error())
		return
	}
	return
}

// deleteAtoms deletes all atoms associated with a given flow.
func deleteAtoms(userid string, flowId string) (err error) {
	_, err = sqldb.Exec("delete from atoms where userid=? and flowId=?", userid, flowId)
	if err != nil {
		log.Println("Error deleting atoms: ", err.Error())
		return
	}
	return
}
