package main

import (
	"encoding/base64"
	"errors"
	"net/http"
	"strings"
)

func parseRequest(r *http.Request) (scheme, credentials string, err error) {
	h, ok := r.Header["Authorization"]
	if !ok || len(h) == 0 {
		return "", "", errors.New("The authorization header is not set.")
	}
	return parse(h[0])
}

func parse(value string) (scheme, credentials string, err error) {
	parts := strings.SplitN(value, " ", 2)
	if len(parts) == 2 {
		return parts[0], parts[1], nil
	}
	return "", "", errors.New("The authorization header is malformed.")
}

func newBasic(credentials string) (*Basic, error) {
	if b, err := base64.StdEncoding.DecodeString(credentials); err == nil {
		parts := strings.Split(string(b), ":")
		if len(parts) == 2 {
			return &Basic{
				Username: parts[0],
				Password: parts[1],
			}, nil
		}
	}
	return nil, errors.New("The basic authentication header is malformed.")
}

type Basic struct {
	Username string
	Password string
}

// Extracts "Authorization" header from a request
func GetCredentialsFromBasicAuthHeader(r *http.Request) (*Basic, error) {
	scheme, credentials, err := parseRequest(r)
	if err == nil {
		if scheme == "Basic" {
			return newBasic(credentials)
		} else {
			err = errors.New("The basic authentication header is invalid.")
		}
	}
	return nil, err
}
