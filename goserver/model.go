package main

import "time"

type Contact struct {
	AcceptPorts bool     `bson:"acceptPorts" json:"acceptPorts"`
	Tags        []string `bson:"tags" json:"tags"`
}

type User struct {
	Id        string             `bson:"_id" json:"id" binding:"required"`
	Name      string             `bson:"name" json:"name"`
	Password  string             `bson:"password" json:"password"`
	Token     string             `bson:"token" json:"token"`
	Email     string             `bson:"email" json:"email"`
	Phone     string             `bson:"phone" json:"phone"`
	Bot       bool               `bson:"bot" json:"bot"`
	Shortcuts map[string]string  `bson:"shortcuts" json:"shortcuts"`
	Contacts  map[string]Contact `bson:"contacts" json:"contacts"`
	Transport string             `bson:"message_transport_type" json:"message_transport_type"`
}

const HelperUser = "JohnnyMeton"

// list of base data types
// "[type]" signifies list of type
// "{key:type}" signifies map type
// e.g. "[{Name:string, Price:price, Options:[string]}]" signifies a complex type
const (
	NoData   = "null"
	String   = "string"
	Integer  = "int"
	Quantity = "quantity"
	Price    = "price"
	Location = "location"
	Time     = "time"
	Date     = "date"
	Image    = "image"
	URL      = "url"
)

// list of actor strings
const (
	Originator = "originator"
	Owner      = "owner"
	Toggle     = "toggle"
)

// list of input actions
const (
	Select1 = "select1"
	SelectN = "selectN"
)

type DataInfo struct {
	Type        string      `bson:"type" json:"type"`
	Description string      `bson:"desc" json:"desc"`
	Value       interface{} `bson:"value" json:"value"`
	Source      string      `bson:"source" json:"source"`
	InputAction string      `bson:"inputAction" json:"inputAction"`
}

type CommandInfo struct {
	Transitions []string            `bson:"transitions" json:"transitions"`
	Description string              `bson:"desc" json:"description"`
	Actor       string              `bson:"actor" json:"actor"`
	Data        map[string]DataInfo `bson:"data" json:"data"`
	Hint        string              `bson:"hint" json:"hint"`
}

type ChoiceInfo struct {
	Values   []string `bson:"values" json:"values"`
	Commands []string `bson:"commands" json:"commands"`
}

type Port struct {
	Id              string                 `bson:"_id" json:"id"`
	Name            string                 `bson:"name" json:"name"`
	Description     string                 `bson:"desc" json:"description"`
	Status          string                 `bson:"status" json:"status"`
	Commands        map[string]CommandInfo `bson:"commands" json:"commands"`
	Creator         string                 `bson:"creator" json:"creator"`
	Choices         ChoiceInfo             `bson:"choices" json:"choices"`
	ExposedByUsers  []string               `bson:"exposedBy" json:"exposedBy"`
	VisibleToUsers  []string               `bson:"visibleTo" json:"visibleTo"`
	VisibleToDegree int                    `bson:"visibleToDegree" json:"visibleToDegree"`
	CanStartWithSay bool                   `bson:"canStartWithSay" json:"canStartWithSay"`
	CollapseFlows   bool                   `bson:"collapseFlows" json:"collapseFlows"`
	Questions       bool                   `bson:"questions" json:"questions"`
	Tasks           bool                   `bson:"tasks" json:"tasks"`
	Appointments    bool                   `bson:"appointments" json:"appointments"`
	Offers          bool                   `bson:"offers" json:"offers"`
	Balances        bool                   `bson:"balances" json:"balances"`
	Locate          bool                   `bson:"locate" json:"locate"`
	Factory         bool                   `bson:"factory" json:"factory"`
	Disabled        bool                   `bson:"disabled" json:"disabled"`
	Settings        map[string]DataInfo    `bson:"settings" json:"settings"`
	Bot             bool                   `bson:"bot" json:"bot"`
}

type PortTemplate struct {
	Id              string                 `bson:"_id" json:"id"`
	Name            string                 `bson:"name" json:"name"`
	Info            interface{}            `bson:"info" json:"info"`
	Description     string                 `bson:"desc" json:"description"`
	Commands        map[string]CommandInfo `bson:"commands" json:"commands"`
	Creator         string                 `bson:"creator" json:"creator"`
	VisibleToUsers  []string               `bson:"visibleTo" json:"visibleTo"`
	CanStartWithSay bool                   `bson:"canStartWithSay" json:"canStartWithSay"`
	Settings        map[string]DataInfo    `bson:"settings" json:"settings"`
}

type Group struct {
	Id       string   `bson:"_id" json:"id"`
	Name     string   `bson:"name" json:"name"`
	Creator  string   `bson:"creator" json:"creator"`
	Users    []string `bson:"users" json:"users"`
	IsPublic bool     `bson:"public" json:"public"`
	Ports    []string `bson:"ports" json:"ports"`
}

type Flow struct {
	FlowId   string      `json:"fid"`
	PortId   string      `json:"pid"`
	CrudFlag string      `json:"crud"`
	Flags    string      `json:"flags"`
	Users    []string    `json:"users"`
	State    string      `json:"state"`
	Data     interface{} `json:"data"`
	Atoms    []Atom      `json:"atoms"`
	Modified time.Time   `json:"modified"`
	Place    string      `json:"place"`
	Time     string      `json:"time"`
}

const (
	Pending   = "pending"
	Scheduled = "scheduled"
)

type ControlMessage struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

type Event struct {
	Id            string            `bson:"_id" json:"id"`
	EventType     string            `bson:"etype" json:"etype"`
	Name          string            `bson:"name" json:"name"`
	Flags         string            `bson:"flags" json:"flags"`
	Owner         string            `bson:"owner" json:"owner"`
	Invitees      []string          `bson:"invitees" json:"invitees"`
	InviteeStatus map[string]string `bson:"inviteeStatus" json:"inviteeStatus"`
	State         string            `bson:"state" json:"state"`
	Data          interface{}       `bson:"data" json:"data"`
	Modified      time.Time         `bson:"modified" json:"modified"`
	Place         string            `bson:"place" json:"place"`
	StartTime     time.Time         `bson:"startTime" json:"startTime"`
	EndTime       time.Time         `bson:"endTime" json:"endTime"`
	Quorum        int               `bson:"quorum" json:"quorum"`
}

type EventAtom struct {
	EventId   string    `bson:"eid" json:"eid"`
	Id        string    `bson:"_id" json:"id"`
	Flags     string    `bson:"flags" json:"flags"`
	Sender    string    `bson:"sender" json:"sender"`
	Receivers []string  `bson:"receivers" json:"receivers"`
	State     string    `bson:"state" json:"state"`
	Body      string    `bson:"body" json:"body"`
	Modified  time.Time `bson:"modified" json:"modified"`
}

type Atom struct {
	FlowId   string      `json:"fid"`
	AtomId   string      `json:"aid"`
	CrudFlag string      `json:"crud"`
	Flags    string      `json:"flags"`
	Sender   string      `json:"sender"`
	State    string      `json:"state"`
	Data     interface{} `json:"data"`
	Body     string      `json:"body"`
	Modified time.Time   `json:"modified"`
}
