// This file holds functions that bring up popup dialogs for gates/contacts.

//
// Shows popup dialog to edit, create, delete a gate.
// If created or edited, returns updated gate object.
//
function editGate(gate) {
    var name = gate ? gate.name : '';
    var desc = gate ? gate.description : '';
    var status = gate ? gate.status : '';
    var inc1 = (gate && gate.commands && gate.commands["Ask"]) ? 'checked' : '';
    var inc2 = (gate && gate.commands && gate.commands["Request"]) ? 'checked' : '';
    var title = gate ? 'Edit Gate' : 'New Gate';
    
    var buttons = {};
    buttons['success'] = {
                label: "Save",
                className: "btn-success",
                callback: function () {
                    inc1 = $("#inc1").is(':checked');
                    inc2 = $("#inc2").is(':checked');
                    var commands = {};
                    if (inc1) commands = $.extend(commands, _.findWhere(g_ports, {id:1}).commands);
                    if (inc2) commands = $.extend(commands, _.findWhere(g_ports, {id:2}).commands);
                    if (! gate) {
                        // create new
                        gate = { 'name' : $('#gateName').val(), 
                                 'description' : $('#gateDesc').val(),
                                 'status' : $('#gateStatus').val(),
                                 'creator' : g_loggedInUser,
                                 'canStartWithSay' : true, 
                                 'commands': commands,
                                 'visibleTo': []
                               };
                        var e = createPort(gate);                       
                        if (e) {
                            showStatusMessage('Error creating new gate: ' + e, 2);
                        }
                        else {
                            showStatusMessage('Gate ' + gate.name + ' created.', 0);
                        }
                    }
                    else {
                        gate.name = $('#gateName').val();
                        gate.commands = commands;
                        gate.description = $('#gateDesc').val();
                        gate.status = $('#gateStatus').val();
                        var e = updatePort(gate);
                        if (e) {
                            showStatusMessage('Error updating gate: ' + e, 2);
                        }
                        else {
                            showStatusMessage('Gate ' + gate.name + ' updated.', 0);
                        }
                    }
                }
    };
        
    if (gate) { 
        buttons['delete'] = {
                    label: "Delete",
                    className: "btn-danger",
                    callback: function () {
                        var e = deletePort(gate.id);
                        if (e) {
                            showStatusMessage('Error deleting gate: ' + e, 2);
                        }
                        else {
                            showStatusMessage('Gate ' + gate.name + ' deleted.', 0);
                        }
                    }               
        };      
    }
        
    bootbox.dialog({
        title: title,
        message: '<div>Name: <input id="gateName" name="name" type="text" style="width:300px" placeholder="Gate Name" value="' 
                 + name + '" class="form-control"></div><br>' +
                 '<div>Description: <input id="gateDesc" name="desc" type="text" style="width:500px" placeholder="Gate Description" value="' 
                 + desc + '" class="form-control"></div><br>' +
                 '<div>Status: <input id="gateStatus" name="status" type="text" style="width:500px" placeholder="Current Status" value="' 
                 + status + '" class="form-control"></div>' +
                 '<div class="checkbox"><label><input type="checkbox" id="inc1" ' + inc1 + '>Include Ask/Assign states</label></div>' +
                 '<div class="checkbox"><label><input type="checkbox" id="inc2" ' + inc2 + '>Include Appointment states</label></div>',
        buttons: buttons
    });
	
    return gate;
}

//
// Shows popup dialog to edit, create, delete a contact.
// If created or edited, returns updated contact object.
//
function editContact(me, cid, contact) {
    var name = cid ? cid : '';
    var ap = (contact && contact.acceptPorts) ? 'checked' : '';
    var title = contact ? 'Edit Contact' : 'New Contact';
    
    var buttons = {};
    buttons['success'] = {
                label: "Save",
                className: "btn-success",
                callback: function () {
                    ap = $("#ap").is(':checked');
                    cid = $('#contactName').val();
                    if (! contact) {
                        // create new
                        contact = { 
                            'acceptPorts' : ap
                        };
                        var e = updateContact(me.id, cid, contact);                       
                        if (e) {
                            showStatusMessage('Error adding new contact: ' + e, 2);
                        }
                        else {
							me.contacts[cid] = contact;
                            showStatusMessage('Contact ' + cid + ' added.', 0);
                        }
                    }
                    else {
                        contact.acceptPorts = ap;
                        var e = updateContact(me.id, cid, contact);
                        if (e) {
                            showStatusMessage('Error updating contact: ' + e, 2);
                        }
                        else {
							me.contacts[cid] = contact;
                            showStatusMessage('Contact ' + cid + ' updated.', 0);
                        }
                    }
                }
    };
        
    if (contact) { 
        buttons['delete'] = {
                    label: "Delete",
                    className: "btn-danger",
                    callback: function () {
                        var e = removeContact(me.id, cid, contact);
                        if (e) {
                            showStatusMessage('Error deleting contact: ' + e, 2);
                        }
                        else {
							delete me.contacts[cid];
                            showStatusMessage('Contact ' + name + ' deleted.', 0);                            
                        }
                    }               
        };      
    }
        
    bootbox.dialog({
        title: title,
        message: '<div>Name: <input id="contactName" name="name" type="text" style="width:300px" placeholder="Contact Name" value="' 
                 + name + '" class="form-control"></div><br>' +
                 '<div class="checkbox"><label><input type="checkbox" id="ap" ' + ap + '>Accept gates from this contact</label></div>',
        buttons: buttons
    });
	
	return contact;
}

//
// shows a status alert message that dismisses itself after 5 seconds
//
function showStatusMessage(msg, level) {
    var alertClass = "alert-info";
    if (level == 1) {
        alertClass = "alert-warning";
    }
    else if (level == 2) {
        alertClass = "alert-danger";
    }
    $("#statusMsg").addClass(alertClass);
    $("#statusMsg").html(msg);
    $("#statusMsg").show();
    $("#statusMsg").fadeTo(5000, 500).slideUp(500, function(){
        $("#statusMsg").hide();
        $("#statusMsg").removeClass(alertClass);
    });
}
