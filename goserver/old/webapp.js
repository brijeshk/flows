// This file contains the main application UI view code. 

//
// globals
//
var g_userProfile = null;
var g_contactProfiles = null;
var g_loggedInUser = null;
var g_selectedFlow = null;
var g_displayedFlows = null;
var g_curPort = null;
var g_filterAction = null;
var g_lastSyncTime = "0";

//
// login handler
// after login, retrieves contacts/ports/flows
//
function doLogin() {
    var u = $("#user").val();
    if (u == "") {
        return false;
    }
    var p = $("#password").val();
    if (loginUser(u, p) == false) {
        showStatusMessage("Invalid credentials - check username and/or password.", 2);
        return false;
    }

    // setup auth header for all further requests
    /*
    $.ajaxSetup({
        headers: {
            'Authorization': "Basic " + btoa(u + ":" + p)
        }
    });*/

    g_loggedInUser = u;
    $("#loginBar").hide();
    $("#menubar").show();
    $("#footerBox").show();
    $("#sidebar-wrapper").show();

    // get and cache all contacts and ports visible to logged in user
    g_userProfile = getUser(g_loggedInUser);
    portManager.refreshVisiblePorts(g_loggedInUser);
    g_contactProfiles = _.map(g_userProfile.contacts, function(v, c) { return getUser(c); });

    // get and display flows
    getFlowsFromServer();
    showFlowsTable(g_displayedFlows);    

    // open websocket connection to server
    openWebSocketToServer(g_loggedInUser, mergeFlowDelta);
    return false;
}

//
// logout handler, closes websocket
//
function doLogout() {
    // reset globals
    g_loggedInUser = null;
    g_userProfile = null;
    portManager.visiblePorts = null;
    g_selectedFlow = null;
    g_displayedFlows = null;
    g_curPort = null;
    g_filterAction = null;
    g_lastSyncTime = "0";

    $("#menubar").hide();
    $("#footerBox").hide();
    $("#loginBar").show();
    $("#sidebar-wrapper").hide();
    $("#flowsTable").html("");

    closeWebSocketToServer();

    // reset global auth header
    /*
    $.ajaxSetup({
        headers: {}
    });*/

    return false;
}

//
// mergeFlowDelta merges a flow delta package into an existing cached set of flows.
// this is used when receiving incremental notifications via the websocket.
//
function mergeFlowDelta(deltaFlows) {
    g_lastSyncTime = ISODateString(new Date()); // TODO: small time gap here, need to plug
    var insertIndex = 0;
    _.each(deltaFlows, function(df) {
        var bf = _.findWhere(g_displayedFlows, {fid: df.fid});
        if (bf) { 
            // found base flow, merge the delta in
            console.log("merging flow " + df.fid);
            bf.state = df.state;
            bf.data = df.data;
            _.each(df.atoms, function(a) {
                a.flags = 'n';
                if (a.crud == 'u') {
                    var atomToUpdate = _.findWhere(bf.atoms, { aid: a.aid });
                    if (atomToUpdate) {
                        atomToUpdate.state = a.state;
                        atomToUpdate.data = a.data;
                        atomToUpdate.modified = a.modified;
                        atomToUpdate.body = a.body;
                    }
                    else {
                        console.log('No matching atom found to update');
                    }
                }
                else if (a.crud == 'd') {
                    var atomToDelete = _.findWhere(bf.atoms, { aid: a.aid });
                    if (atomToDelete) {
                        bf.atoms.splice(_.indexOf(bf, atomToDelete), 1);
                    }
                    else {
                        console.log('No matching atom found to delete');
                    }
                }
                else {
                    bf.atoms.push(a);
                }
            });
            bf.updatedFlag = true;
            bf.modified = df.modified;
            // move flow to front of array
            g_displayedFlows.splice(_.indexOf(g_displayedFlows, bf), 1);
            g_displayedFlows.splice(insertIndex, 0, bf);
        }
        else {
            // new flow
            console.log("adding new flow " + df.fid);
            df.updatedFlag = true;
            g_displayedFlows.unshift(df);  // add to front of array
        }
        insertIndex++;
    });

    if (g_selectedFlow == null) {
        showFlowsTable(g_displayedFlows);
    }
    else {
        showFlowDetails();
    }
    return;
}

//
// refreshes filter button states
//
function refreshFilterStates() {
    $('#unreadButton').css('color', 'gray');
    $('#todoButton').css('color', 'gray');
    $('#waitingButton').css('color', 'gray');
    if (g_filterAction) {
        $('#' + g_filterAction + 'Button').css('color', 'white');
    }
}

//
// showFlowsTable displays a list of flows in the flows table.
//
function showFlowsTable(flows) {
    // parse the filter text to see if any filter is applied
    var meta = parseMetaLine($("#filterText").val());
    var filterUser = (meta[0].length > 0) ? meta[0][0] : null;
    var filterPort = meta[1] ? portManager.getPortFromName(meta[1]) : null;
    var todoCount = 0, waitingCount = 0, unreadCount = 0;

    refreshFilterStates();

    var tableHtml = '<div class="list-group" style="padding:2px">';
    for (var i in flows) {
        flow = flows[i];
        port = portManager.getPortFromId(flow.pid);
        var portname = port ? port.name : "Unknown"; 
        var usersButMe = [];
        _.each(flow.users, function(u) {
            if (u != g_loggedInUser || flow.users.length == 1) {
                usersButMe.push(u);
            }
        });

        // color flow differently if it's waiting on action
        var rowClass = '';
        var curAction = null;
        if (port) {
            var pa = portManager.isPendingAction(flow, g_loggedInUser);
            if (pa == 1) {
                // my todos are colored red
                rowClass = ' list-group-item-danger';
                curAction = 'todo';
                todoCount++;
            }
            else if (pa == -1) {
                // others todos are colored orange
                rowClass = ' list-group-item-warning';
                curAction = 'waiting';
                waitingCount++;
            }

            // skip flows that don't satisfy filter
            if ((filterPort != null && filterPort.id != port.id) ||
                (filterUser != null && $.inArray(filterUser, usersButMe)==-1)) {
                continue;
            }
        }

        if (_.findWhere(flow.atoms, {flags:'n'})) {
            unreadCount++;
            flow.updatedFlag = true;
        }

        // is there an action filter in effect?
        if (g_filterAction == 'unread' && ! flow.updatedFlag) {
            continue;
        }
        if (g_filterAction != null && g_filterAction != 'unread' && curAction != g_filterAction) {
            continue;
        }

        var modified = new Date(flow.modified);
        var timeNow = new Date();
        var modStr = (modified.getMonth()+1) + '/' + modified.getDate();
        var today = (timeNow.getMonth()+1) + '/' + timeNow.getDate();
        if (today == modStr) {
            modStr = modified.toTimeString().substr(0,5);
        }
        var msg = flow.atoms[flow.atoms.length-1].body.substr(0,60);
        msg = $("<p>").html(msg).text(); // remove html markups
        var tmp = '<a href="#" id="{0}" style="padding:4px" class="list-group-item{1}">' + 
                  '<div class="container">' + 
                     '<div class="row">' + 
                         '<div class="col-md-4 list-group-item-heading" style="height:14px">{2}</div>' + 
                         '<div class="col-md-6 list-group-item-text" style="font-size:11px;height:14px"><i>{3}</i></div>' +
                         '<div class="col-md-2 hidden-sm list-group-item-text" style="font-size:10px;height:14px;text-align:right">{4}</div>' +
                    '</div>' + 
                  '</div></a>';
        tableHtml += tmp.format(flow.fid, 
                                rowClass, 
                                usersButMe.join(', ') + ' on ' + portname + ' (' + flow.state + ')',
                                msg,
                                modStr);
    }
    tableHtml += "</div>";
    $("#flowsTable").html(tableHtml);

    // update count badges
    todoCount > 0 ? $("#todoBadge").html(todoCount) : $("#todoBadge").html("");
    waitingCount > 0 ? $("#waitingBadge").html(waitingCount) : $("#waitingBadge").html("");
    unreadCount > 0 ? $("#unreadBadge").html(unreadCount) : $("#unreadBadge").html("");

    // reset selection
    g_selectedFlow = null;
    g_curPort = null;
}

/*
function selectAndShowFlow(fid) {
    for (var i in g_displayedFlows) {
        if (g_displayedFlows[i].fid == fid) {
            g_selectedFlow = g_displayedFlows[i];
            break;
        }
    }
    showFlowDetails();
}*/

function filterFlows(f) {
	if (f) {		
		g_filterAction = (g_filterAction == f) ? null : f;
	}
    showFlowsTable(g_displayedFlows);
    return false;
}

//
// filters flows by action and gate, triggered from summary list links
//
function filterFromSummary(pname, action) {
    if (action) g_filterAction = action.toLowerCase();
    if (g_filterAction == 'total') g_filterAction = null;
    $("#filterText").val('.' + pname);
    showFlowsTable(g_displayedFlows);
}

//
// groups flows by gate, and shows unread/todo/waiting/total counts per gate
//
function showGateWiseSummary(flows) {
    // compute summary counts
    var gateCounts = {};
    var g_selectedFlow = null;
    _.each(flows, function(f) {
        if (! gateCounts[f.pid]) gateCounts[f.pid] = [0,0,0,0,''];
        var c = gateCounts[f.pid];
        c[3]++;
        if (f.updatedFlag) c[0]++;
        var pa = portManager.isPendingAction(f, g_loggedInUser);
        if (pa == 1) c[1]++;
        else if (pa == -1) c[2]++;

        // preview of last unread or todo message for each flow in gate
        if (f.updatedFlag || pa == 1) {
            var p = gateCounts[f.pid][4];
            if (p != '') p += '; &nbsp;&nbsp;&nbsp;';
            var a = f.atoms[f.atoms.length-1];
            p += getActionStringFromAtom(a, g_loggedInUser);
            if (a.body) p += ': ' + a.body.substring(0,30);
            gateCounts[f.pid][4] = p;
        }
    }); 

    // generate table
    var tableHtml = '<table class="table">';
    tableHtml += '<tr class="active">' + 
                      '<th style="width:12%">Gate</th>' + 
                      '<th style="width:16%">Counts</th>' + 
                      '<th>Last Activity</th>' + 
                  '</tr>';
    var labels = ['Unread', 'ToDo', 'Waiting']; 
    var colors = ['lightsteelblue', 'lightpink', 'gold', '']; 
    _.each(gateCounts, function(c, pid) {
        var p = portManager.getPortFromId(parseInt(pid));
        var pname = (p) ? p.name : 'Unknown';
        var counts = [];
        for (i=0; i<3; i++) {
            if (c[i] > 0) {
                var tmp = '<a href="#" onclick="filterFromSummary(\'{0}\',\'{2}\'); return false;">' +
                    '<span class="badge" style="background-color:{3}">{1}</span> {2}' + 
                    '</a>';
                counts.push(tmp.format(pname, c[i], labels[i], colors[i]));
            }
        }
        cntsCol = counts.join(' , ');
        tableHtml += '<tr style="height:45px"><td style="vertical-align:middle"><a href="#" onclick="filterFromSummary(\'' + pname + '\',null); return false;">' + pname + '</a></td>' + 
                     '<td style="vertical-align:middle">' + cntsCol + '</td><td style="text-align:left;vertical-align:top;font-size:10px">' + c[4] + '</td></tr>';
    });
    tableHtml += '</table>';
    $("#flowsTable").html(tableHtml);
    $("#filterText").val('');
    g_filterAction = null;
    refreshFilterStates();
    return false;
}

// 
// groups flows by user, and shows unread/total counts by user
//
function showUserWiseSummary(flows) {
}


//
// deletes current selected flow
// 
function deleteFlow() {
    if (g_selectedFlow) {
        // post delta to remove from server
        g_selectedFlow.crud = 'd';
        postFlowDelta(g_loggedInUser, [g_loggedInUser], [g_selectedFlow]);
        // remove from displayed list
        g_displayedFlows.splice(g_displayedFlows.indexOf(g_selectedFlow), 1);
        console.log("deleted flow");
        showFlowsTable(g_displayedFlows);
    }
    else {
        // main table, delete selected flows
        $("#flowsTable").find(".list-group-item").each(function() {
            if ($(this).hasClass('list-group-item-info')) {
                var flow = null;
                var fid = $(this).attr('id');
                for (var i in g_displayedFlows) {
                    if (g_displayedFlows[i].fid == fid) {
                        flow = g_displayedFlows[i];
                        break;
                    }
                }
                if (flow) { 
                    // post delta to remove from server
                    flow.crud = 'd';
                    postFlowDelta(g_loggedInUser, [g_loggedInUser], [flow]);
                    // remove from displayed list
                    g_displayedFlows.splice(g_displayedFlows.indexOf(flow), 1);
                }
            }
        });
        showFlowsTable(g_displayedFlows);
    }
}

//
// getFlowsFromServer gets all the flows for logged in user from the server
// TODO: only get the last N days worth of flows
//
function getFlowsFromServer() {
    if (g_lastSyncTime == "0") {
        g_displayedFlows = getFlows(g_loggedInUser, g_lastSyncTime);
        g_lastSyncTime = ISODateString(new Date()); // TODO: time gap here, need to plug
    }
    else {
        var flowDelta = getFlows(g_loggedInUser, g_lastSyncTime);
        mergeFlowDelta(flowDelta);
    }
    return false;
}

//
// showFlowDetails shows all messages of a single flow
//
function showFlowDetails() {    
    if (g_selectedFlow == null) {
        console.log("How can g_selectedFlow be null?");
        return;
    }
    
    // get port name and users involved in flow
    g_curPort = portManager.getPortFromId(g_selectedFlow.pid);
    var portName = g_curPort ? g_curPort.name : "Unknown";
    var usersButMe = [];
    _.each(g_selectedFlow.users, function(u) {
        if (u != g_loggedInUser || g_selectedFlow.users.length == 1) {
            usersButMe.push(u);
        }
    });

    // heading for flow
    var detailHtml = '<h5>On gate ' + portName + ' with ' + usersButMe.join(', ') + '. At ' + g_selectedFlow.state + '.</h5><br>';

    // table of atoms
    //detailHtml += '<ul class="list-group" style="padding:4px;border-bottom:0px">';
    
    var flagsUpdated = false;
    for (var i in g_selectedFlow.atoms) {
        var a = g_selectedFlow.atoms[i];
        // who did what?
        var s = portManager.getActionStringFromAtom(a, g_loggedInUser);
        var c = (a.sender == g_loggedInUser) ? 'lavender' : '#D3F8D3'; 
        // any data in the atom?
        var d = '';
        if (a.data) {
            for (k in a.data) {
                var item = a.data[k];
                if (item['type'][0] == '[') {
                    // list type
                    d += item['value'].join('\n');
                }
                else {
                    d += k + ' : ' + item.value;
                }
                d += '\n';
            }
        }
        // any body?
        if (a.body) {
            d += a.body.replace('\n','<br>');
        }
    	// unset unread for messages i sent
    	if (a.flags == 'n' && a.sender == g_loggedInUser) a.flags = '';

        var tmp = '<div class="well well-sm" style="background:{2};margin-left:10px;margin-right:10px">' + 
                    '<div class="container-fluid">' +
                       '<div class="row">' + 
                         '<div class="col-xs-10" style="white-space:pre-wrap;font-size:13px;">{0}</div>' +
                         '<div class="col-xs-2" style="font-size:13px;text-align:right">{1}</div>' + 
                       '</div>' + 
                     '</div>' + 
                  '</div>';
        detailHtml += tmp.format(d, s, c);

    	// unset
    	if (a.flags == 'n') {
            a.flags = '';
            flagsUpdated = true;
        }
    }
    detailHtml += '</ul>';

    // unset unread flags for flow and atoms
    g_selectedFlow.updatedFlag = false;  
    if (flagsUpdated) {
        updateFlowFlags(g_loggedInUser, g_selectedFlow);
    }

    $("#flowsTable").html(detailHtml);
    
    // prepopulate reply edit boxes, we know the users and port 
    if ($("#msgBox").val() == "") {
        //var text = '@' + usersButMe.join(",@") + '.' + portName + ':';
        $("#msgBox").val(':');
        //showStateButtons();
        //$("#msgBox").focus();      
    }
    return false;
}

//
// lists contacts of logged in user in contacts button dropdown.
// retrieves status of who's online as well.
//
function showContacts() {
    var onlineContacts = getOnlineContacts(g_loggedInUser);
    var dropList = '<li style="font-size:13px"><a href="#" id="addContactMenu">Add Contact...</a></li>';
    _.each(g_userProfile.contacts, function(v, c) {
        dropList += '<li style="font-size:13px"><a href="#" id="c_' + c + '">' + c + ((_.indexOf(onlineContacts, c)>=0) ? ' (online)' : '') + '</a></li>';
    });
    $("#contactsList").html(dropList);
    _.each(g_userProfile.contacts, function(c, cid) {
		$('#c_' + cid).click(function() { editContact(g_userProfile, cid, c); });
	});
    $("#addContactMenu").click(function() { editContact(g_userProfile, null, null); return false; });
    $('.dropdown-toggle').dropdown();
    return true;
}

//
// lists gates of logged in user in Gates button dropdown.
//
function showGatesDropDown() {
    var ports = getPortsExposedByUser(g_loggedInUser);
    var dropList = '<li style="font-size:13px"><a href="#" id="newGateButton">New Gate...</a></li>';
    dropList += '<li style="font-size:13px"><a href="#" id="discoverGatesButton">Discover...</a></li>';
    _.each(ports, function(p) {
        dropList += '<li style="font-size:13px"><a href="#" id="p_' + p.id + '">' + p.name + '</a></li>';
    });
    $("#gateList").html(dropList);
    $("#discoverGatesButton").click(showGatesTable);
    $("#newGateButton").click(function() { 
		var p = editGate(null); 
		if (p) portManager.visiblePorts.push(p); 
	});

    _.each(ports, function(p) {
        $('#p_' + p.id).click(function() { editGate(p); });
    }); 
    $('.dropdown-toggle').dropdown();
    return true;
}

//
// shows available state buttons
//
function showStateButtons() {
    var curState = null;
    if (g_selectedFlow != null && g_curPort && g_selectedFlow.pid == g_curPort.id) {
        curState = g_selectedFlow.state;
    }
    var keys = portManager.getValidTransitionStates(g_curPort, curState);
    // add state buttons dynamically
    var html = '';
    _.each(keys, function(k) {
        var b = '<li><a href="#" id="state_' + k + '">' + k + '</a></li>';
        html += b;                  
    });
    $("#stateList").html(html);
    // setup handlers
    _.each(keys, function(k) {
        $("#state_" + k).click(function() { $("#msgBox").val($("#msgBox").val().split(':')[0] + ":" + k); });
    });
    // setup any data hints
    if (keys.length == 1) {
        var cmd = keys[0];
        if (g_curPort && g_curPort.commands[cmd] && g_curPort.commands[cmd].data) {
            var keys = Object.keys(g_curPort.commands[cmd].data);
            $("#msgBox").val($("#msgBox").val() + cmd + ' ' + keys.join(', '));
        }
    }
    $("#msgBox").focus();
}

// 
// parses the contents of the msg edit box or filter text box.
// extracts @user, #port, :state if present.
// returns parsed result in an array: [users, port, state, body]
//
function parseMetaLine(s) {
    // initialize with defaults
    var result = [[], null, 'Say', '']

    // if no meta line detected, treat the whole string as msg body
    if (s == "" || (s[0] != '@' && s[0] != '.' && s[0] != ':')) {
        result[3] = s
        return result
    }
    
    var m = s.match(/@[^,\.\s]+/g);
    if (m != null) {
        _.each(m, function(x) {
            result[0].push(x.substring(1));
        });
    }
    m = s.match(/\.[^:\s]+/);
    if (m != null) {
        result[1] = m[0].substring(1);
    }
    m = s.match(/:\S+/);
    if (m != null) {
        result[2] = m[0].substring(1);
    }
    m = s.match(/\s\S.*/);
    if (m != null) {
        result[3] = m[0].substring(1);
    }
    return result;
}

//
// sendMessage sends the message typed in the msg edit box
//
function sendMessage() {
    var meta = parseMetaLine($("#msgBox").val());
    //console.log("parseMetaLine returns: " + meta);
    var toUsers = meta[0];
    if (toUsers.length == 0 && g_selectedFlow != null) {
        // get users from selected flow
        toUsers = _.reject(g_selectedFlow.users, function(u) { return u == g_loggedInUser; }); 
    }
    if (toUsers.length == 0) {
        showStatusMessage('No users specified. Add one or more users using @.', 2);
        return false;
    }
    var state = meta[2];
    var body = meta[3];
    var port = null;
    if (meta[1]) {
        port = portManager.getPortFromName(meta[1]);
    } 
    if (g_selectedFlow != null) {
        var oldPort = portManager.getPortFromId(g_selectedFlow.pid);
        if (port && port.id != oldPort.id) {
            // port switch, ask for confirmation
            var ok = confirm("You are trying to switch the gate on this thread. This would override any previous state. Continue?");
            if (!ok) {
                return;
            }
            // switched port new state overrides existing state in this case
            g_selectedFlow.state = state;
        }
        else {
            port = oldPort;
        }
    }
    if (! port) {
        showStatusMessage('No gate specified. Defaulting to Chat:Say. You can address a user\'s specific gate using ".".', 1);
        port = portManager.getPortFromId(0); // use the Chat port
    } 
    
    //++ parse body into data map if state accepts data
    var data = null;
    if (port.commands[state] && port.commands[state].data) {
        var keys = Object.keys(port.commands[state].data);
        if (keys.length > 0) {
            data = {};
            var values = body.split(',');
            if (body == '' || values.length < keys.length) {
                showStatusMessage('Enter data input in the following format: ' + keys.join(', '), 2);
                return;
            }
            for (var i in keys) {           
                var k = keys[i];
                data[k] = { 'value': values[i].trim(), 'type': port.commands[state].data[k].type };
            }
            body = '';
        }
    }
    //-- parse body
    
    //++ make up atom and flow 
    var date = new Date();
    var aid = g_loggedInUser.substring(0,3) + Math.round(date/1000);
    var timeNow = ISODateString(date);
    var atom = { 
        'crud' : 'c',
        'state': state, 
        'sender': g_loggedInUser, 
        'body': body,
        'data' : data,
        'modified' : timeNow
    };
    var users = toUsers.concat(g_loggedInUser);
    var flow = { 
        'users' : users, 
        'pid' : port.id,
        'state' : state,
        'data': null,
        'atoms' : [atom],
        'modified' : timeNow
    };
    if (g_selectedFlow == null) {  // new flow
        flow.fid = generateUUID();
        flow['crud'] = 'c';
    }
    else {  // reply on existing flow
        flow.fid = g_selectedFlow.fid;
        // Say and Remind are "soft" states. They don't overwrite the existing state on the flow.
        if (flow.state == 'Say' || flow.state == 'Remind') {
           flow.state = g_selectedFlow.state;
        }
        else {
           g_selectedFlow.state = flow.state;
        }
        flow['crud'] = 'u';
        g_selectedFlow.atoms.push(atom);
        g_selectedFlow.pid = flow.pid;
        g_selectedFlow.modified = flow.modified;
    }
    atom.flags = 'n';
    atom.aid = aid;
    atom.fid = flow.fid;
    //-- make up atom and flow
    
    // all set, send message
    console.log('Sending flow payload:');
    console.log(JSON.stringify(flow, null, 4));
    var flows = [];
    flows.push(flow);
    postFlowDelta(g_loggedInUser, users, flows);
    g_lastSyncTime = ISODateString(new Date()); // TODO: small time gap here, need to plug

    // clear edit box
    $("#msgBox").val("");
    $("#stateList").html("");

    // refresh view
    if (g_selectedFlow == null) {
        g_displayedFlows.unshift(flow);
        g_selectedFlow = flow;
    }
    else {
        // bubble the existing flow to the top
        g_displayedFlows.splice(_.indexOf(g_displayedFlows, g_selectedFlow), 1);
        g_displayedFlows.splice(0, 0, g_selectedFlow);
    }
    showFlowDetails();
}

//
// setup message edit box autocomplete triggers for user, gate, state
//
function setupAutoCompleteTriggers() {
    $("#msgBox").atwho({
        at : ".", 
        limit: 20,
        suffix: ':',
        start_with_space: false,
        callbacks : {
            remote_filter: function(q, callback) {
                var options = [];
                var meta = parseMetaLine($("#msgBox").val());

                // if port already selected, then don't complete anymore.
                // TODO: fix hack
                if (g_curPort && meta[0] != 'metonbot') return; 

                var users = meta[0];
                // does selected user accept my gates?
                // TODO: checking only first target user now
                var c = _.findWhere(g_contactProfiles, {id:users[0]});
                if (c) {
                    // find me in that user's contacts and check the acceptPorts flag
                    var meAsContact = c.contacts[g_loggedInUser];
                    if (meAsContact && meAsContact.acceptPorts) {
                        console.log("adding my ports because contact accepts them.");
                        users.push(g_loggedInUser); 
                    }
                }                
                for (var i in portManager.visiblePorts) {
                    var p = portManager.visiblePorts[i];
                    // is this port exposed by selected user?
                    var y = false;
                    if ($.inArray(p.creator, users) >= 0) {
                        y = true;
                    }
                    else if ((p.exposedBy != null) && !(/bot$/i.test(users[0]))) {
                        if ($.inArray(users[0], p.exposedBy)>=0 || $.inArray("all", p.exposedBy)>=0) {
                            y = true;
                        }
                    }
                    if (y == true && p.name.toLowerCase().indexOf(q.toLowerCase()) == 0) {
                        options.push({"id":i, "name":p.name});
                    }
                }
                callback(options);
            },
            before_insert: function(value, elem) {
                g_curPort = portManager.getPortFromName(value.substring(1));
                showStateButtons();
                return value;
            }
        }
    }).atwho({
        at : "@", 
        limit: 20,
        suffix: '',
        start_with_space: false,
        callbacks : {
            remote_filter: function(q, callback) {
                var options = [];
                if (g_userProfile.contacts == null) {
                    callback(options);
                    return;
                }
                var i = 0;
                _.each(g_userProfile.contacts, function(v,c) {
                    if (c.toLowerCase().indexOf(q.toLowerCase()) == 0) {
                        options.push({"id":i, "name":c});
                        i++;
                    }
                });
                callback(options);
            },
            before_insert: function(value, elem) {
                return value + '.';
            }
        }
    })
    .atwho({
        at : ":", 
        suffix: ' ',
        limit: 10,
        start_with_space: false,
        callbacks : {
            remote_filter: function(q, callback) {
                var options = [];
                if (g_curPort == null) {
                    callback(options);
                    return;
                }
                var curState = null;
                if (g_selectedFlow != null) {
                    curState = g_selectedFlow.state;
                }
                var keys = portManager.getValidTransitionStates(g_curPort, curState);               
                for (var i in keys) {
                    if (keys[i].toLowerCase().indexOf(q) == 0) {
                        options.push({"id":i, "name":keys[i]});
                    }
                }
                callback(options);
            },
            before_insert: function(value, elem) {
                var cmd = value.substring(1);
                if (g_curPort.commands[cmd] && g_curPort.commands[cmd].data) {
                    var keys = Object.keys(g_curPort.commands[cmd].data);
                    value += ' ' + keys.join(', ');
                }
                return value;
            }
        }
    }); 

    $("#filterText").atwho({
        at : ".", 
        limit: 20,
        suffix: '',
        start_with_space: false,
        callbacks : {
            remote_filter: function(q, callback) {
                var options = [];
                for (var i in portManager.visiblePorts) {
                    var p = portManager.visiblePorts[i];
                    if (p.name.toLowerCase().indexOf(q) == 0) {
                        options.push({"id":i, "name":p.name});
                    }
                }
                callback(options);
            },
            before_insert: function(value, elem) {
                return value + ' ';
            }
        }
    }).atwho({
        at : "@", 
        limit: 20,
        suffix: '',
        start_with_space: false,
        callbacks : {
            remote_filter: function(q, callback) {
                var options = [];
                if (g_userProfile.contacts == null) {
                    callback(options);
                    return;
                }
                var i = 0;
                _.each(g_userProfile.contacts, function(v,c) {
                    if (c.toLowerCase().indexOf(q) == 0) {
                        options.push({"id":i, "name":c});
                        i++;
                    }
                });
                callback(options);
            },
            before_insert: function(value, elem) {
                return value + ' ';
            }
        }
    });
}

//
// searches for a user by keyword and displays results in main area
//
/*
function searchForUsers() {
    $("#searchUsersButton").click(function() {
        var text = $("#searchUsersText").val();
        console.log("searching for " + text);
        return false;
    });
    $("#flowsTable").hide();
    $("#searchUsers").show();
    return true;
}
*/

//
// shows visible gates and their info
//
function showGatesTable() {
    var t = '<table class="table table-condensed"><tr class="active"><th>Id</th><th>Name</th><th>Creator</th><th>Description</th><th>Status</th><th>Extensions</th></tr>';
    _.each(portManager.visiblePorts, function(p) {
        var extstr = '';
        if (p.extensionPorts) {
            extstr = (_.map(p.extensionPorts, function(e) { return portManager.getPortFromId(e).name; })).join(', '); 
        }
        t += '<tr><td>' + p.id + '</td>' +
             '<td>' + p.name + '</td>' +
             '<td>' + p.creator + '</td>' +
             '<td>' + p.description + '</td>' + 
             '<td>' + p.status + '</td>' +            
             '<td>' + extstr + '</td></tr>';
    });
    t += '</table>';

    $("#flowsTable").html(t);
}


//
// entry method to setup the event handlers
//
function main() {
    // set all ajax calls to be synchronous, this aids control flow.
    // we can change this later if we want UI to be more responsive
    $.ajaxSetup({async: false});

    // add a string format util function to all strings
    String.prototype.format = function() {
        var formatted = this;
        for (var i = 0; i < arguments.length; i++) {
            var regexp = new RegExp('\\{'+i+'\\}', 'gi');
            formatted = formatted.replace(regexp, arguments[i]);
        }
        return formatted;
    };

    $.getScript("util.js");
    $.getScript("api.js");
    $.getScript("ports.js");
    $.getScript("dialogs.js");

    // setup button handlers
    $("#login").click(doLogin);
    $("#logoutButton").click(doLogout);
    $("#todoButton").click(function() { return filterFlows('todo'); });
    $("#waitingButton").click(function() { return filterFlows('waiting'); });
    $("#unreadButton").click(function() { return filterFlows('unread'); });
    $("#filterButton").click(function() { return filterFlows(null); });
    $("#newFlowButton").click(function() { $("#msgBox").focus(); return false; });
    $("#deleteFlowButton").click(deleteFlow);
    $("#sendButton").click(function() { sendMessage(); return false; });
    $('#filterText').keypress(function(e) {
        if (e.which == '13') {
            filterFlowsKeyword();
            return false;
        }
    });
    $('#msgBox').keypress(function(e) {
        if (e.keyCode == 13 && e.shiftKey) {
            sendMessage();
            return false;
        }
    });
    $("#homeButton").click(function() { 
        $("#msgBox").val(""); 
        $("#stateList").html("");
        $("#gatesTable").hide();
        $("#flowsTable").show();
        showFlowsTable(g_displayedFlows); 
        return false; 
    });
    $("#refreshButton").click(function() { 
        $("#msgBox").val(""); 
        $("#stateList").html("");
        getFlowsFromServer();
        showFlowsTable(g_displayedFlows); 
        return false; 
    });
    $("#contactsButton").click(showContacts);
    $("#gatesButton").click(showGatesDropDown);

    // setup auto-complete handler in the msg edit and filter boxes
    setupAutoCompleteTriggers();

    // hide action buttons till user logs in
    $("#menubar").hide();
    $("#statusMsg").hide();
    $("#footerBox").hide();
    $("#gatesTable").hide();
    $("#sidebar-wrapper").hide();

    // setup flow table row selection toggler (on right-click)
    $("#flowsTable").on('contextmenu', '.list-group-item', function(event) {
        if ($(this).hasClass('list-group-item-info')) {  // unselect flow
            $(this).removeClass('list-group-item-info');
        }
        else {  // select flow
            $(this).removeClass('list-group-item-danger').removeClass('list-group-item-warning').addClass('list-group-item-info');
        }
        return false;
    });

    // setup row click handler to show flow details   
    $("#flowsTable").on('click', '.list-group-item', function(event) {
        var fid = $(this).attr('id');
        if (! fid) {
            g_selectedFlow = null;
            return;
        }
        for (var i in g_displayedFlows) {
            if (g_displayedFlows[i].fid == fid) {
                g_selectedFlow = g_displayedFlows[i];
                break;
            }
        }
        showFlowDetails();
    });

    // focus input in login text box
    $("#user").focus();

    // ready for action
}
