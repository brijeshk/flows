package main

import (
	"fmt"
	"testing"
	"time"
)

func init() {
	InitDbSession()
}

func TestComputeDegreeOfSeparation(t *testing.T) {
	d, err := ComputeDegreeBetweenUsers("brijesh", "brijesh")
	if err != nil {
		t.Error("Error from ComputeDegree: ", err.Error())
		return
	}
	if d != 0 {
		t.Error("Identity test failed. Got degree: ", d)
		return
	}
	fmt.Println("Identity test passed")
	d, err = ComputeDegreeBetweenUsers("brijesh", "prateek")
	if err != nil {
		t.Error("Error from ComputeDegree: ", err.Error())
		return
	}
	if d != 1 {
		t.Error("Degree 1 direct contact test failed. Got degree: ", d)
		return
	}
	fmt.Println("Degree 1 direct contact test passed")

	d, err = ComputeDegreeBetweenUsers("brijesh", "manasi")
	if err != nil {
		t.Error("Error from ComputeDegree: ", err.Error())
		return
	}
	if d != 2 {
		t.Error("Degree 2 contact test failed. Got degree: ", d)
		return
	}
	fmt.Println("Degree 2 test passed")

	d, err = ComputeDegreeBetweenUsers("manasi", "brijesh")
	if err != nil {
		t.Error("Error from ComputeDegree: ", err.Error())
		return
	}
	if d != 2 {
		t.Error("Degree 2 reverse contact test failed. Got degree: ", d)
		return
	}
	fmt.Println("Degree 2 reverse test passed")

	d, err = ComputeDegreeBetweenUsers("nitesh", "manasi")
	if err != nil {
		t.Error("Error from ComputeDegree: ", err.Error())
		return
	}
	if d != 3 {
		t.Error("Degree 3 contact test failed. Got degree: ", d)
		return
	}
	fmt.Println("Degree 3 test passed")

	d, err = ComputeDegreeBetweenUsers("prateek", "amitabh")
	if err != nil {
		t.Error("Error from ComputeDegree: ", err.Error())
		return
	}
	if d != -1 {
		t.Error("Stranger test failed. Got degree: ", d)
		return
	}
	fmt.Println("Stranger test passed")
}

func TestDiscoverPorts(t *testing.T) {
	for i := 0; i < 3; i++ {
		ports, _, err := DiscoverPorts("brijesh", "", i)
		if err != nil {
			t.Error("DiscoverPorts failed for degree ", i, ": ", err.Error())
		}
		var names []string
		for _, p := range ports {
			names = append(names, p.Name)
		}
		fmt.Printf("ports visible to brijesh at degree %d : %v\n", i, names)
	}
}

func TestEventCRUD(t *testing.T) {
	timeOneDayBefore := time.Now().AddDate(0, 0, -1)
	a0 := EventAtom{EventId: "abc", Id: "1", State: "", Sender: "brijesh", Receivers: []string{"mangesh", "prateek"}, Body: "lets go guys", Modified: timeOneDayBefore}
	e := Event{
		Id:        "abc",
		EventType: "Lunch",
		Owner:     "brijesh",
		State:     "Pending",
		Invitees:  []string{"brijesh", "prateek", "mangesh"},
	}
	if err := CreateEvent(&e); err != nil {
		t.Error("CreateEvent failed: ", err.Error())
	}
	fmt.Println("Inserted event : ", e.Id)

	event, err := GetEvent("abc")
	if err != nil {
		t.Error("GetEvent failed: ", err.Error())
	}
	fmt.Println("Retrieved event : ", event)

	e.State = "Scheduled"
	if err := UpdateEvent(&e); err != nil {
		t.Error("UpdateEvent failed: ", err.Error())
	}
	fmt.Println("Updated event : ", e.Id)

	if err := SendMessageOnEvent(&a0); err != nil {
		t.Error("SendMessageOnEvent failed: ", err.Error())
	}
	fmt.Println("Sent atoms on event : ", e.Id)

	events, err := GetEventsModifiedSince("prateek", timeOneDayBefore)
	if err != nil {
		t.Error("GetEventsModifiedSince failed: ", err.Error())
	}
	for _, ev := range events {
		fmt.Println("Got EventModifiedSince: ", ev)
	}

	atoms, err := GetCachedMessages("prateek")
	if err != nil {
		t.Error("GetCachedMessages failed: ", err.Error())
	}
	if len(atoms) != 1 {
		t.Error("GetCachedMessages returned invalid num of messages")
	}

	atoms, err = GetCachedMessages("mangesh")
	if err != nil {
		t.Error("GetCachedMessages failed: ", err.Error())
	}
	if len(atoms) != 1 {
		t.Error("GetCachedMessages returned invalid num of messages")
	}

	if err := DeleteEvent(e.Id); err != nil {
		t.Error("DeleteEvent failed: ", err.Error())
	}
	fmt.Println("Deleted event : ", e.Id)
}
