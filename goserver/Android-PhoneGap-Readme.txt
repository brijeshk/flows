To make an PhoneGap-based android apk:
-------------------------------------

- zip up the www folder:
zip -r www.zip www

- login to build.phonegap.com (use brijeshsk@gmail.com, brinkley)

- click Update code, upload the zip file

- this should build an apk on the cloud 

- if you see a blue "apk" button, build succeeded, click to download apk

- if you see a red error, examine contents and fix error

- save the apk to downloads/AutoMeton.apk, or login to build.phonegap.com
  from device and install app directly.


To debug a PhoneGap-based app running on device:
-----------------------------------------------

- download GapDebug (Mac or Windows only). this also needs Chrome
  browser.

- enable USB debugging (follow Help in GapDebug) on device

- connect device vis USB

- open AutoMeton app (it's already enabled for debug)

- you can then access the javascript console logs from the app in the
  GapDebug Chrome browser window.
