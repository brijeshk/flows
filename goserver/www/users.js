
var userManager = {
	curUser: null,
	curContacts: null,
	onlineContacts: null,
    curSettings: null,
    db: null,

    init: function() {
        userManager.db = new PouchDB('users');
    },

    //
    // logs in user and saves credentials in local db.
    //
	login: function(u, p, saveCreds) {
	    if (loginUser(u, p) == false) {
            // delete stored credentials
            console.log("login failed. deleting saved creds");
            userManager.db.get(u).then(function(d) {
                userManager.db.remove(d);
            });
	        return false;
	    }
          
        if (saveCreds) {
            // default settings
            userManager.curSettings = { 
                '_id' : u,
                'password' : p,
                'lastSync' : '0',
                'storeOnServer' : false,
            };

            userManager.db.put(userManager.curSettings, function cb(e, r) {
                if (e) console.log('Pouchdb error: ' + e.message);
            });

            userManager.curSettings.firstLogin = true;
        }

	    // get and cache all contacts of logged in user
	    userManager.curUser = getUser(u);
	    userManager.curContacts = _.map(userManager.curUser.contacts, function(v, c) { return getUser(c); });
	    return true;
	},

    //
    // logs out user and removes saved credentials from local db.
    // calls callback if passed. 
    //
	logout: function(cb) {
		var u = userManager.curUser.id;
		userManager.curUser = null;
		userManager.curContacts = null;
        userManager.curSettings = null;
        userManager.db.get(u).then(function(d) {
            userManager.db.remove(d);
            if (cb) cb();
        });
	},

    //
    // refreshes online status of current user's contacts
    //
    refreshOnlineContacts: function() {
        userManager.onlineContacts = getOnlineContacts(userManager.curUser.id);
    },

    //
    // updates last sync time in user record in local db
    //
    updateLastSyncTime: function(t) {
		var u = userManager.curUser.id;
        userManager.db.get(u).then(function(d) {
            d.lastSync = t;
            userManager.db.put(d);
        });
    },

    //
    // updates user settings in user record in local db
    //
    updateSettings: function(s) {
        userManager.curSettings = s;
		var u = userManager.curUser.id;
        userManager.db.get(u).then(function(d) {
            d.storeOnServer = s.storeOnServer;
            userManager.db.put(d);
        });
        // TODO: update user record on server
    },

    //
    // finds saved credentials for user if any
    // and calls callback cb if exactly one set of credentials found
    //
    useStoredCredentials: function(cb) {
        userManager.db.allDocs({include_docs:true}).then(function(r) {
            if (r.total_rows == 1) {
                var d = r.rows[0].doc;
                userManager.curSettings = d;
                if (cb) cb(d);
            }
            else if (r.total_rows == 0) {
                console.log('found no stored credentials.');
                if (cb) cb(null);
            }
            else {
                console.log('found multiple stored credentials.');
                if (cb) cb(null);
            }
        });
    },

    //
    // debug function to print user records to console
    //
    printLocalDb: function() {
        userManager.db.allDocs({include_docs:true}).then(function(r) {
            _.each(r.rows, function(row) {
                console.log(row.doc);
            });
        });
    },

    //
    // register new user
    //
    register: function(u, p, n, ph, e) {
        var user = {
            'id': u, 
            'password': p,
            'name': n, 
            'phone': ph,
            'email': e
        };
        return registerUser(user);
    },

    //
    // get contact object for given user id
    //
    getContactUserFromId: function(cid) {
        return _.findWhere(userManager.curContacts, { id: cid});
    },

    // 
    // search for AutoMeton users by their phone number
    // used when bulk importing contacts at registration time
    //
    searchForUsers: function(phoneNumbers) {
        return findUsersByPhoneNumber(phoneNumbers);
    },

    //
    // updates contact details 
    //
	updateContact: function(cid, c) {
		var e = updateContact(userManager.curUser.id, cid, c);
		if (e) return e;
		if (! userManager.curUser.contacts[cid]) {
			userManager.curContacts.push(getUser(cid));
		}
		userManager.curUser.contacts[cid] = c;
		return null;
	},
	
    // 
    // removes a contact
    //
	removeContact: function(cid) {
		var e = removeContact(userManager.curUser.id, cid, { 'acceptPorts' : false });
		if (e) return e;
		delete userManager.curUser.contacts[cid];
		_.each(userManager.curContacts, function(c, i) {
			if (c.id == cid) {
				userManager.curContacts.splice(i, 1);
			}
		});
		return null;
	},
}
