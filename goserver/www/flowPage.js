// Functions to control the flow detail page.

function setupTagAutoComplete() {
    //(g_curPort.choices.values && g_curPort.choices.values.length > 0) ? $('#tagsButton').show() : $('#tagsButton').hide();
    (flowManager.selectedFlow && flowManager.selectedFlow.data) ? $('#cardButton').show() : $('#cardButton').hide();
    (g_curPort && g_curPort.bot) ? $('#adduser').hide() : $('#adduser').show();

    (g_curPort && g_curPort.appointments) ? $('#where').show() : $('#where').hide();
	(g_curPort && g_curPort.appointments) ? $('#when').show() : $('#when').hide();
	
    $('#msgBox').atwho({
        at : "#",
        limit: 10,
        suffix: '',
        start_with_space: false,
        callbacks : {
            remote_filter: function(q, callback) {
                var options = [];
                var i = 0;
                _.each(g_curPort.choices.values, function(c) {
                    if (c.toLowerCase().indexOf(q.toLowerCase()) == 0) {
                        options.push({"id":i, "name":c});
                        i++;
                    }
                });
                callback(options);
            },
            before_insert: function(value, elem) {
                return value + '';
            }
        }
    });
}

function setupTimeTranslate() {
    $('#when').change(function() {
        var t = $(this).val();
        if (t != '') {
            var parsedTime = chrono.parse($(this).val());
            if (! parsedTime || ! parsedTime[0]) {
                showPopupMsg("I don't understand the time! Please enter it differently.");
                return;
            }
            $('#when').val(parsedTime[0].start.date().toLocaleString());
        }
    });
}

//
// initializes flow detail page with new flow.
//
function showNewFlowDetails(users) {
    $('#msgBox').data('users', users);
    $('#msgBox').data('gate', g_curPort.name);
    if (users == userManager.curUser.id) users = 'myself';
    $('#flowTitle').html(g_curPort.name + '<span style="font-size:13px"> (' + users + ')<span>');
    $('#where').val('');
    $('#when').val('');
    $('#flowMessages').html("");	
    showStateButtons();
    setupTagAutoComplete();
    $('#msgBox').focus();
    return false;
}

//
// show stored data
//
function showData() {
    if (! flowManager.selectedFlow || ! flowManager.selectedFlow.data) {
        return;
    }

    if ($('#popupTags')) $('#popupTags').remove();

    var $toast = $('<div>').attr({'data-role':'popup','id':'popupTags','data-dismissible':'true'}).appendTo('#flowDetailPage');
    $('<div>').attr({'data-role':'header','id':'popup-header'}).css({'background-color':'#CD5C5C'}).append('<h3>Thread Card</h3>').appendTo('#popupTags');

    var d = '';
    var item = flowManager.selectedFlow.data;
    if (item.type[0] == '[') {
        // list type
        d = item.value.join('<br>');
    }
    else if (item.type == 'location') {
        d = 'Location: <a href="geo:' + item.value.replace(';',',') + '">Open Map</a>';
    }
    else {
        d = item.value;
    }
    $('<p style="text-align:center">' + d + '</p>').attr({'data-theme':'a'}).appendTo('#popupTags');


    $toast.css({
        opacity: 0.95, 
        position: 'fixed',
        padding: '3px',
        'text-align': 'center',
        width: '250px',
        'max-height': '300px',
        left:  $(window).width() / 2 - 125,
        top: '20%',
        'font-size':13,
    });

    $('#flowDetailPage').trigger('create');
    var popup = setInterval(function () {
        $("#popupTags").popup("open", {
            overlyaTheme: "a"
        }).on("popupafterclose", function () {
            $(this).remove();
        });
        clearInterval(popup);
    }, 1);
}

//
// shows topic hashtags
//
function showTopicTags() {
    if ($('#popupTags')) $('#popupTags').remove();

    if (! g_curPort.choices.values || g_curPort.choices.values.length == 0) {
        showPopupMsg('There are no hashtags available for this topic.<br>Hashtags are created by the port owner to signify sub-topics, choices etc.');
        return;
    }

    var $toast = $('<div>').attr({'data-role':'popup','id':'popupTags','data-dismissible':'true'}).appendTo('#flowDetailPage');
    $('<div>').attr({'data-role':'header','id':'popup-header'}).css({'background-color':'#CD5C5C'}).append('<h3>Hashtags</h3>').appendTo('#popupTags');
    $('<ul>').attr({'data-role':'listview','id':'mylist'}).appendTo('#popupTags');
    _.each(g_curPort.choices.values, function(tag,i) {
        $('<li style="text-align:center" id="tag_' + i + '">').attr({'data-theme':'a'}).append('#' + tag).appendTo('#mylist');
    });

    $toast.css({
        opacity: 0.95, 
        position: 'fixed',
        padding: '3px',
        'text-align': 'center',
        width: '250px',
        'max-height': '300px',
        left:  $(window).width() / 2 - 125,
        top: '20%',
        'font-size':13,
        'overflow-y': 'scroll'
    });

    $('#flowDetailPage').trigger('create');
    var popup = setInterval(function () {
        $("#popupTags").popup("open", {
            overlyaTheme: "a"
        }).on("popupafterclose", function () {
            $(this).remove();
        });
        clearInterval(popup);
    }, 1);

    _.each(g_curPort.choices.values, function(tag,i) {
        $('#tag_' + i).click(function() {
            $('#msgBox').val($('#msgBox').val() + ' #' + tag + ' ');
            $('#msgBox').focus();
            $('#popupTags').remove();
        });
    });

}

function showSwitchTopics() {
    if ($('#popupTags')) $('#popupTags').remove();

    var users = $('#msgBox').data('users').split(',');
    var ports = getPortsExposedByUser(users[0], userManager.curUser.id);
    _.each(getPortsExposedByUser(userManager.curUser.id, users[0]), function(p) {
        if (p.creator == userManager.curUser.id && p.factory == false) ports.push(p);
    });

    var $toast = $('<div>').attr({'data-role':'popup','id':'popupTags','data-dismissible':'true'}).appendTo('#flowDetailPage');
    $('<div>').attr({'data-role':'header','id':'popup-header'}).css({'background-color':'#CD5C5C'}).append('<h3>Switch Port</h3>').appendTo('#popupTags');
    $('<ul>').attr({'data-role':'listview','id':'mylist'}).appendTo('#popupTags');
    _.each(ports, function(p) {
        $('<li style="text-align:center">').append(p.name).appendTo('#mylist');
    });
    $toast.css({
        opacity: 0.95, 
        position: 'fixed',
        padding: '3px',
        'text-align': 'center',
        width: '200px',
        'max-height': '300px',
        left:  $(window).width() / 2 - 100,
        top: '20%',
        'font-size':13,
        'overflow-y': 'scroll'
    });

    $('#flowDetailPage').trigger('create');
    var popup = setInterval(function () {
        $("#popupTags").popup("open", {
            overlyaTheme: "a"
        }).on("popupafterclose", function () {
            $(this).remove();
        });
        clearInterval(popup);
    }, 1);
 
    $('#mylist').on('click', 'li', function(e) {
        var pname = $(this).html();
        g_curPort = portManager.getPortFromName(pname);
        if (flowManager.selectedFlow != null) {
            flowManager.selectedFlow.pid = g_curPort.id;
            flowManager.selectedFlow.state = null;
            showFlowDetails();
        }
        $('#popupTags').remove();
    });
}

function showAddContacts() {
    if ($('#popupTags')) $('#popupTags').remove();
    var $toast = $('<div>').attr({'data-role':'popup','id':'popupTags','data-dismissible':'true'}).appendTo('#flowDetailPage');
    $('<div>').attr({'data-role':'header','id':'popup-header'}).css({'background-color':'#CD5C5C'}).append('<h3>Add User</h3>').appendTo('#popupTags');
    $('<ul>').attr({'data-role':'listview','id':'mylist'}).appendTo('#popupTags');
    _.each(userManager.curContacts, function(c,i) {
        if (! c.shortcuts || _.isEmpty(c.shortcuts)) {
            $('<li style="text-align:center">').append(c.id).appendTo('#mylist');
        }
    });
    $toast.css({
        opacity: 0.95, 
        position: 'fixed',
        padding: '3px',
        'text-align': 'center',
        width: '200px',
        'max-height': '300px',
        left:  $(window).width() / 2 - 100,
        top: '20%',
        'font-size':13,
        'overflow-y': 'scroll'
    });

    $('#flowDetailPage').trigger('create');
    var popup = setInterval(function () {
        $("#popupTags").popup("open", {
            overlyaTheme: "a"
        }).on("popupafterclose", function () {
            $(this).remove();
        });
        clearInterval(popup);
    }, 1);
  
    $('#mylist').on('click', 'li', function(e) {
        var u = $(this).html();
        $('#msgBox').val($('#msgBox').val() + ' @' + u + ': ');
        $('#msgBox').focus();
        $('#popupTags').remove();
    });
}

function showAskBots() {
    if ($('#popupTags')) $('#popupTags').remove();
    var $toast = $('<div>').attr({'data-role':'popup','id':'popupTags','data-dismissible':'true'}).appendTo('#flowDetailPage');
    $('<div>').attr({'data-role':'header','id':'popup-header'}).css({'background-color':'#CD5C5C'}).append('<h3>Ask Bot</h3>').appendTo('#popupTags');
    $('<ul>').attr({'data-role':'listview','id':'mylist'}).appendTo('#popupTags');
    _.each(userManager.curContacts, function(c,i) {
        if (c.shortcuts && ! _.isEmpty(c.shortcuts)) {
            _.each(c.shortcuts, function(v,k) {
                $('<li style="text-align:center">').append(k).appendTo('#mylist');
            });
        }
    });
    $toast.css({
        opacity: 0.95, 
        position: 'fixed',
        padding: '3px',
        'text-align': 'center',
        width: '200px',
        'max-height': '300px',
        left:  $(window).width() / 2 - 100,
        top: '20%',
        'font-size':13,
        'overflow-y': 'scroll'
    });

    $('#flowDetailPage').trigger('create');
    var popup = setInterval(function () {
        $("#popupTags").popup("open", {
            overlyaTheme: "a"
        }).on("popupafterclose", function () {
            $(this).remove();
        });
        clearInterval(popup);
    }, 1);
  
    $('#mylist').on('click', 'li', function(e) {
        var u = $(this).html();
        $('#msgBox').val($('#msgBox').val() + ' @' + u + ': ');
        $('#msgBox').focus();
        $('#popupTags').remove();
        var shortcut = null;
        _.each(userManager.curContacts, function(c) {
            if (c.shortcuts && c.shortcuts[u]) shortcut = c.shortcuts[u];
        });
        if (shortcut) {
            var t = shortcut.split('.');
            var botuser = t[0];
            var botport = portManager.getPortFromName(t[1]);
            var botcmd = t[2];
            var hint = portManager.getDataInputHint(botport, botcmd);
            if (hint) showPopupMsg('Text me like this: ' + hint, 10000);
        }
    });
}

//
// shows all messages of a single flow.
// this is used as the compose message page for an existing flow.
//
function showFlowDetails() {   
    if (flowManager.selectedFlow == null) {
        console.log("no selected flow");
        return false;
    }

    // get port name and users involved in flow
    g_curPort = portManager.getPortFromId(flowManager.selectedFlow.pid);
    var portName = g_curPort ? g_curPort.name : "Unknown";
    var usersButMe = [];
    _.each(flowManager.selectedFlow.users, function(u) {
        if (u != userManager.curUser.id || flowManager.selectedFlow.users.length == 1) {
            usersButMe.push(u);
        }
    });
    if (usersButMe.length == 0) usersButMe = [userManager.curUser.id];

    // set heading for flow
    var users = usersButMe.join(',');
    $('#flowTitle').html(g_curPort.name + '<span style="font-size:13px"> (' + users + ')<span>');
    $('#msgBox').data('users', usersButMe.join(','));
    $('#msgBox').data('gate', portName);
	
    $('#where').val(flowManager.selectedFlow.place);
    $('#when').val(flowManager.selectedFlow.time);
    
 	//add an extra class to div#flowMessages
	$('#flowMessages').addClass("dottedLine");
		
    // generate flow message bubbles
    var detailHtml = '';
    var flagsUpdated = false;
    var timeNow = new Date();
    var today = (timeNow.getMonth()+1) + '/' + timeNow.getDate();

    for (var i in flowManager.selectedFlow.atoms) {
      var a = flowManager.selectedFlow.atoms[i];

      var name = a.sender;

      // who did what?
      var s = portManager.getActionStringFromAtom(a, userManager.curUser.id);
      // who did what (userAvatar)?
      var su = portManager.getActionStringFromAtomForUserAvatar(a, userManager.curUser.id);
      var x = su.charAt(3);
      // and when?
      var modified = new Date(a.modified);
      var t = (modified.getMonth()+1) + '/' + modified.getDate();
      if (today == t) {
        t = modified.toTimeString().substr(0,5);
      }

      var c = (a.sender == userManager.curUser.id) ? '#989898' : '#d3d8dc'; 

      // any data in the atom?
      var d = '';
      var map = null;
      if (a.data) {
        for (k in a.data) {
          var item = a.data[k];
          if (item['type'][0] == '[') {
            // list type
            d += item['value'].join('<br>');
          }
          else if (item.type == 'location' && a.sender != userManager.curUser.id) {
            // create map insets for location responses i receive (not the ones i send)
            //map = '<div id="locmap" data-loc="{0}" style="height:200px;width:200px;align:right"></div>'.format(item.value);
            d += 'Location: <a href="geo:' + item.value.replace(';',',') + '">Open Map</a>';
          }
          else {
            d += k + ' : ' + item.value;
          }
          d += '<br>';
        }
      }

      // what exactly did they say?
      if (a.body) {
        d += a.body.replace(/([#@]\S+)/, '<span style="color:#CD5C5C">$1</span>').replace('\n','<br>');
      }
      // unset unread for messages i sent
      // if (a.flags == 'n' && a.sender == userManager.curUser.id) a.flags = '';

      // bubbles or table format        
      var bubbleType = (a.sender == userManager.curUser.id) ? 'bubbleMe' : 'bubbleThem';
      var tmp = '<div class="usrAvatar" style="background-color:'+ getUserColor(x) +'; color:'+ getTextColor(getUserColor(x)) +'">{3}</div><div style="font-size:12px;text-align:center" class="' + bubbleType + '"><p style="margin:0px">{2}</p><span class="time">{1}</span></div><p><p>';
      detailHtml += tmp.format(s, t, d, su);

      if (map) detailHtml += map;

      // reset unread flags
      if (a.flags == 'n') {
        a.flags = '';
        flagsUpdated = true;
      }
    }

    // unset unread flags for flow and atoms
    if (flagsUpdated) {
        flowManager.saveFlowsToLocalDb([flowManager.selectedFlow]);
    }

    var fdiv = $('#flowMessages');
    fdiv.html(detailHtml);
    $(document).scrollTop($(document).height());

    $('#flowMessages').on('contextmenu', '.bubbleleft', addDataToCard);
    $('#flowMessages').on('contextmenu', '.bubbleright', addDataToCard);

    // show state hints 
    showStateButtons();

    setupTagAutoComplete();

    return false;
}

function addDataToCard() {
    var rx = /<p>(.*)<\/p>/g;
    var m = rx.exec($(this).html());
    if (m && m.length >= 2) {
        var c = m[1];
        //if (flowManager.selectedFlow.data) flowManager.selectedFlow.data += '<br>';
        flowManager.selectedFlow.data = { 'type' : 'string', 'value' : c };
        flowManager.saveFlowsToLocalDb([flowManager.selectedFlow]);
        showFlowDetails();
        showPopupMsg('Message starred');
    }
    return false;
}

/* EMBEDDED MAP WIDGET no longer needed, we will just open native Maps app with geo location
//
// initializes any map widgets on the flow detail page
//
function showMapWidgets() {
    console.log("initializing any mapdiv elements");
    var mapdiv = $('#locmap');
    if (!mapdiv) {
        console.log('No mapdiv element found');
        return;
    }
    var loc = mapdiv.data('loc');
    if (!loc) {
        console.log('No location data in mapdiv element!');
        return;
    }
    if (!loc || typeof loc != "string") return;

    var ll = loc.split(';');
    if (ll.length != 2) return;
    var latlng = new google.maps.LatLng(ll[0], ll[1]); 
    var mapOptions = {
        center: latlng,
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('locmap'), mapOptions);
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: "I'm here!"
    });
}*/

//
// shows available state buttons
//
function showStateButtons() {
    if (! g_curPort) return;

    var curState = null, lastUser = null;
    if (flowManager.selectedFlow != null && flowManager.selectedFlow.pid == g_curPort.id) {
        curState = flowManager.selectedFlow.state;
        if (flowManager.selectedFlow.users.length > 1) {
            // find last user that set an actionable state
            var a = portManager.findLastAtomWithState(flowManager.selectedFlow, flowManager.selectedFlow.state);
            if (a) lastUser = a.sender;
        }
    }

    var commands = portManager.getCommandGraph(g_curPort);

    // find available state transitions for current user
    var keys = portManager.getValidTransitionStates(g_curPort, curState, userManager.curUser.id, lastUser);
	if(keys.length == 4){
		var width = "10%";
	}else if(keys.length == 3){
		var width = "20%";
	}else if(keys.length == 2){
		var width = "40%";
	}
	
    if (keys.length == 0) return;

    // add state buttons dynamically
    /*var html = '<div id="stateList" data-role="navbar" style="background-color: #3a4348; background-image:url(css/images/dotted-line.png);"><ul>';
    _.each(keys, function(k) {
        //var ellipsis = (commands[k] && commands[k].data) ? "..." : "";
        var b = '<li><a href="#" class="ui-btn ui-btn-corner-all" style="color:#fff !important; border-color:rgba(0,0,0,0); opacity:1;background:none !important" id="s_' + k + '"><span class="'+ btn_class +'" style="background-color: #3a4348">' + k + ' <i class="fa fa-chevron-right" style="color:#d44035"]></i></span></a></li>';
        html += b;                  
    });
    html += '</ul></div>';*/
	var ccount = 1;
	var html = '<div id="stateList" data-role="navbar" style="background-color: #3a4348; text-align:center">';
    _.each(keys, function(k) {
        //var ellipsis = (commands[k] && commands[k].data) ? "..." : "";
        var b = '<a href="#" class="ui-btn ui-btn-corner-all" style="color:#fff !important; border-color:rgba(0,0,0,0); opacity:1;background:none !important" id="s_' + k + '"><span style="background-color: #3a4348">' + k + ' <i class="fa fa-chevron-right" style="color:#d44035"]></i></span></a>';
        html += b;  
		if(ccount == keys.length){
			
		}
		else{
			html += "<a style='background-color:#3B4349; border-color:#3B4349; background-image:url(css/images/dotted-line.png); width:"+ width +"; opacity:0.4'>&nbsp;</a>";
		}
		ccount++;
    });
    html += '</div>';
    $('#stateList').remove();
    $('#msgBoxFooter').prepend($(html));
    $('#stateList').navbar();

    // setup handlers
    _.each(keys, function(k) {
        $("#s_" + k).click(function() { 
            $('#msgBox').data('state', k);

            if (g_curPort && commands[k] && commands[k].data && Object.keys(commands[k].data).length>0) {
                //showDataInputDialog(k, commands[k].data);
                var data = portManager.parseDataInput(g_curPort, k, $('#msgBox').val());
                if (! data) return;
                $('#msgBox').data('data', data);
            }

            // special handling for appointments
            // if user accepts, add to user's calendar
            if (k == 'Accept') {
                flowManager.addEventToCalendar(flowManager.selectedFlow);
            }

            // check for message content
            if ($('#msgBox').val() == '' && ! _.contains(['Schedule','Accept','Reject','Remind','Complete','Respond','Locate','Settle'], k)) {
                showPopupMsg('Say something, yo!');
                return false;
            }

            // send the message
            doSendMessage();
            return false;
        });
    });
}

//
// extracts bot reference in message body, and if found, sends message to bot with input data
// 
function sendBotMessage(text) {
    var rx = /@(.*):(.*)/g;
    var m = rx.exec(text);
    if (m && m.length > 2) {
        var trigger = m[1];
        var shortcut = null;
        _.each(userManager.curContacts, function(c) {
            if (c.shortcuts && c.shortcuts[m[1]]) shortcut = c.shortcuts[m[1]];
        });
        if (shortcut) {
            var t = shortcut.split('.');
            var botuser = t[0];
            var botport = portManager.getPortFromName(t[1]);
            var botcmd = t[2];
            var botdata = portManager.parseDataInput(botport, botcmd, m[2]);
            if (botdata) {
                flowManager.sendBotMessage(userManager.curUser.id, botuser, botport, botcmd, botdata, '');
            }
        }
    }
}

//
// sends the message typed in the msg edit box
//
function doSendMessage() {
    console.log('sending message');

    var u = $('#msgBox').data('users').split(',');
    var p = $('#msgBox').data('gate');
    var s = $('#msgBox').data('state');
    var d = $('#msgBox').data('data');
    var m = $('#msgBox').val();
	var where = $('#where').val();
	var when = $('#when').val();
    var pid = g_curPort.id;

    // find and add other non-bot users referenced in message
    var rx = /@([^:]*):/g;
    while (o = rx.exec(m)) {
        if (o[1].indexOf('Bot') < 0) u.push(o[1]);
    }

    if (! s) s = 'Say';
    if (g_curPort.canStartWithSay == false && s == 'Say' && flowManager.selectedFlow == null) {
        showPopupMsg('You must select one of the available commands to start a thread on this topic.');
        return false;
    }
    flowManager.sendMessage(userManager.curUser.id, u, pid, s, d, m, where, when);

    // find bot references in message, and invoke such bots if any
    sendBotMessage(m); 

    // clear edit box
    $("#msgBox").val("");
    $("#stateList").html("");

    // refresh view
    showFlowDetails();
    return false;
}

function getUserColor(userFirstChar){
	colors = ['#CB99C9', '#DEA5A4', '#966FD6', '#77DD77', '#779ECB', '#C23B22', '#CFCFC4', '#B19CD9', '#03C03C', '#FFB347', '#836953', '#FF6961', '#B39EB5', '#FFD1DC', '#F49AC2', '#AEC6CF', '#FDFD96'];
	
	if(userFirstChar == "I"){
		var bg_color = "#E3E3E3";
	}else{
		var hash = 0;
		if(userFirstChar.length == 0) return (hash % 17);
		for(i = 0; i < userFirstChar.length; i++){
			chr = userFirstChar.charCodeAt(i);
			hash = hash+chr;
		}
		hash = (hash % 17);
	
		var bg_color = colors[hash];
	}
	
	
	return bg_color;
}

function getTextColor(hexcolor){
	var c = hexcolor.substring(1);      // strip #
	var rgb = parseInt(c, 16);   // convert rrggbb to decimal
	var r = (rgb >> 16) & 0xff;  // extract red
	var g = (rgb >>  8) & 0xff;  // extract green
	var b = (rgb >>  0) & 0xff;  // extract blue

	var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

	if (luma < 40) {
		// pick a different colour
		var textcolor = "#ffffff";
	}else{
        // color is dark
		var textcolor = "#000000";
    }
	return textcolor;
}
