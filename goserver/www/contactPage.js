// functions to manage Contact Detail page.

//
// populates fields in contact details dialog
//
function showContactDetails(id) {
    if (id) {
        var c = userManager.curUser.contacts[id];
        if (! c) {
            showPopupMsg("Contact " + id + " doesn't exist in AutoMeton.");
            return;
        }
        $("#contactName").val(id);
        $("#contactAcceptPorts").prop('checked', c.acceptPorts).checkboxradio("refresh");
    }
    else {
        $("#contactName").val('');
        $("#contactAcceptPorts").prop('checked', false).checkboxradio("refresh");
    }
}

//
// saves contact details
//
function doSaveContact() {
    var c = {};
    var cid = $("#contactName").val();
    c.acceptPorts = $("#contactAcceptPorts").is(':checked');
    var e = userManager.updateContact(cid, c);
    if (e) {
        showPopupMsg('Error adding new contact: ' + e);
        return false;
    }
	else {
		showContacts();
	}
}

//
// deletes contact
//
function doDeleteContact(cid) {
    var e = userManager.removeContact(cid);
    if (e) {
        showPopupMsg('Error deleting contact: ' + e);
        return false;
    }
	else {
		showContacts();
	}	
}
