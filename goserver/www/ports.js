// portManager is an object to manage port-related functions.

var portManager = {
    visiblePorts : null,

    systemCommands : { "Decline" : { "actor" : "toggle" }, 
                       "Complete" : { "actor" : "toggle" }, 
                       "Schedule" : { "transitions" : [ "Accept", "Decline" ], 
                                      "data" : { "Date" : { "source" : "user", "type" : "date" }, 
                                                 "About" : { "source" : "user", "type" : "string" }, 
                                                 "At" : { "source" : "user", "type" : "time" }, 
                                                 "Duration" : { "source" : "user", "type" : "int", "desc" : "Duration (minutes)" } },
                                      "hint" : "at {At} on {Date} for {Duration} regarding {About}" }, 
                       "Accept" : { "actor" : "toggle" }, 
                       "Ask" : { "transitions" : [ "Answer", "Cancel" ] }, 
                       "Answer" : { "actor" : "toggle" }, 
                       "Cancel" : {}, 
                       "Assign" : { "transitions" : [ "Complete", "Cancel" ] },
                       "Locate" : { "transitions" : [ "Respond" ] }, 
                       "Respond" : { "data" : { "Speed" : { "type" : "string" }, 
                                                "Location" : { "type" : "location" } }, 
                                     "actor" : "toggle" },
                       "Offer" : { "transitions" : [ "Accept", "Decline", "Cancel" ],
                                   "data" : { "Price" : { "source" : "user", "type" : "price" } } },
                       "Update" : { "data" : { "Value" : { "source": "user", "type":"int" }}, "hint" : "{Value}" },
                       "Settle" : {},
    },

    systemPorts : { "Social" : ["Say", "Short", "Busy", "Later", "Snap"],
                    "Questions" : ["Ask", "Answer", "Cancel"],
                    "Todos" : ["Assign", "Complete", "Cancel"],
                    "Offers" : ["Offer", "Accept", "Decline", "Cancel"],
                    "Appointments" : ["Schedule", "Accept", "Decline", "Cancel"],
                    "Balances" : ["Update", "Settle"]
    },

	reset: function() {
		portManager.visiblePorts = null;
	},

	//
	// find cached port by id
	//
	getPortFromId: function(pid) {
		var port = _.findWhere(portManager.visiblePorts, {id: pid});
		return port ? port : null;
	},

	//
	// find cached port by name
	//
	getPortFromName: function(pname) {
		var port = _.findWhere(portManager.visiblePorts, {name: pname});
		return port ? port : null;
	},

    getMyPorts: function() {
        return _.where(portManager.visiblePorts, {creator:userManager.curUser.id});
    },

    getUserPorts: function(uid) {
        return _.where(portManager.visiblePorts, {creator:uid});
    },

    getPortsVisibleFromUserToUser: function(uid1, uid2) {
        var ports = [];
        _.each(portManager.getUserPorts(uid1), function(p) {
            if (p.factory == false && 
                (_.contains(p.visibleTo, 'world') || _.contains(p.visibleTo, uid2))) {
		ports.push(p);
            }
        });
        return ports;
    },
	
	//
	// get ports visible to given user and store in cache var
	//
	refreshVisiblePorts: function(userid) {
		portManager.visiblePorts = getPortsVisibleToUser(userid);
		return portManager.visiblePorts;
	},

    getCommandGraph: function(port) {
        var commands = _.clone(port.commands);
        if (port.questions) {
            commands["Ask"] = portManager.systemCommands["Ask"];
            commands["Answer"] = portManager.systemCommands["Answer"];
            commands["Cancel"] = portManager.systemCommands["Cancel"];
        }
        if (port.tasks) {
            commands["Assign"] = portManager.systemCommands["Assign"];
            commands["Complete"] = portManager.systemCommands["Complete"];
            commands["Cancel"] = portManager.systemCommands["Cancel"];
        }
        if (port.locate) {
            commands["Locate"] = portManager.systemCommands["Locate"];
            commands["Respond"] = portManager.systemCommands["Respond"];
        }
        if (port.offers) {
            commands["Offer"] = portManager.systemCommands["Offer"];
            commands["Accept"] = portManager.systemCommands["Accept"];
            commands["Decline"] = portManager.systemCommands["Decline"];
            commands["Cancel"] = portManager.systemCommands["Cancel"];
        }
        if (port.balances) {
            commands["Update"] = portManager.systemCommands["Update"];
            commands["Settle"] = portManager.systemCommands["Settle"];
        }
        /*
        // add choice menu data if configured
        _.each(port.choices.commands, function(c) {
            var d = { 'Choose' : { 'inputAction' : 'select1', 
                                   'value' : port.choices.values, 
                                   'source' : 'user',
                                   'type' : 'string',
                                   'desc' : 'Choose'
                                 }
            };
            // TODO: merge existing data with choices
            if (c != 'Schedule') commands[c].data = d;
        });*/

        return commands;
    },
	
	//
	// get valid state transitions in a given port with a current state
	//
	getValidTransitionStates: function(port, curState, curUser, lastUser) {
		// for invalid ports, like deleted ones, just support basic 'Say'
		if (port == null) return ['Say']; 
		var validStates = [];

        var commands = portManager.getCommandGraph(port);

        if (port.bot == false && 
            port.name != 'Tracker' &&
            curState != null && 
            ! portManager.isSinkState(port, curState)) {
			// get valid transitions from current state
			if (commands[curState] != null) {
                var tr = commands[curState].transitions;
                if (tr) {
                    _.each(tr, function(t) {
                        var c = commands[t];
                        if (c && c.actor && c.actor == 'toggle' && curUser == lastUser) return;
                        validStates.push(t);
                    });
                }
			}
		    validStates.push('Say');
			if (curState != 'Say') {
				// all non Say states can have a Remind followup
				validStates.push('Remind');
			}
		}
        else {
            if (port.canStartWithSay) {
                validStates.push('Say');
            }

            // no current state or last state, or special port like tracker or bot port 
            // startStates = allStates minus transitionStates
            var allStates = Object.keys(commands);
            var transitionStates = [];
            $.map(allStates, function(s) { 
                var t = commands[s].transitions;
                if (t != null) $.merge(transitionStates, t);
            });
            $.map(allStates, function(s) { 
                if ($.inArray(s, transitionStates) < 0) 
                    validStates.push(s);
            });
        }

		return validStates;
	},
	
	//
	// returns true if state has no outgoing transitions, false otherwise.
	//
	isSinkState: function(port, state) {
        var commands = portManager.getCommandGraph(port);
		return !(commands[state] && commands[state].transitions &&
		         commands[state].transitions.length > 0); 
	},

    //
    // returns 1 if pending action on me, -1 if pending action on someone else, 0 if no action pending
    //
    isPendingAction: function(flow, me) {
        var port = portManager.getPortFromId(flow.pid);
        if (! port) return 0;
        if (! portManager.isSinkState(port, flow.state)) {
            var a = portManager.findLastAtomWithState(flow, flow.state);
            // if last actionable state was assigned by me to myself, or by someone else to me, then pending action on me
            if (a && (flow.users.length == 1 || a.sender != me)) {
                return 1;
            }
            else {
                return -1;
            }
        }
        return 0;
    },
		
	//
	// make displayable action string from atom
	// e.g. sender:brijesh state:Say becomes "brijesh says" or "I say"
	//
	getActionStringFromAtom: function(a, me) {
		if (a.sender == me) {
			s = 'I ' + a.state.toLowerCase();
		}
		else {
			s = a.sender + ' ' + a.state.toLowerCase() + 's';
		}
		return s;
	},
	
	getActionStringFromAtomForUserAvatar: function(a, me) {
		if (a.sender == me) {
			s = '<p>I<br/><span style="font-size:10px">' + a.state.toLowerCase() +'</span></p>';
		}
		else {
			var str = a.sender;
			var sub = str.substring(0, 2);
			s = '<p>'+ sub + '<br/><span style="font-size:10px">' + a.state.toLowerCase() + 's</span></p>';
		}
		return s;
	},

    //
    // returns a summary string indicating flow status, like:
    // "I ask userX on General" or
    // "userX assigns me on MyTopic" etc.
    //
    getSummaryStringFromFlow: function(f, me) {
        var port = portManager.getPortFromId(f.pid);
        var portname = port ? port.name : 'Unknown';
        var usersButMe = _.reject(f.users, function(u) { return (u == me); } ); 
        var ustr = (usersButMe.length > 0) ? usersButMe.join(',') : 'myself'; 
        var a = portManager.findLastAtomWithState(f, f.state);
        var s = '';
        if (a.sender == me) {
            s = 'I {0} {1} on {2}'.format(a.state.toLowerCase(), ustr, portname);
        }
        else {
            s = '{0} {1} me on {2}'.format(ustr, a.state.toLowerCase(), portname);
        }
        return s;
    },

    //
    // finds most recent atom with a given state
    //
    findLastAtomWithState: function(flow, state) {
        for (var i=flow.atoms.length-1; i>=0; i--) {
            if (state == flow.atoms[i].state) return flow.atoms[i];
        }
        return null;
    },

    //
    // extracts data values for a given port command from a text string and returns data map.
    // if valid values cannot be extracted, returns null
    //
    parseDataInput: function(port, cmd, text) {
        var commands = portManager.getCommandGraph(port);
	    if (commands[cmd] && commands[cmd].data) {
	        var keys = Object.keys(commands[cmd].data);
	        if (keys.length > 0) {
                if (commands[cmd].hint) {
                    var tokenMap = extractTokensFromText(commands[cmd].hint, text, keys);
                    if (tokenMap) {
                        console.log('tokenMap: ' + JSON.stringify(tokenMap));
                        var data = {};
                        for (var i in keys) {           
                            var k = keys[i];
                            var v = tokenMap[k].trim();
                            data[k] = { 'value': v, 'type': commands[cmd].data[k].type };
                        }
                        return data;
                    }
                }

                // try simple list
                var dataValues = text.split(',');
                if (text != '' && dataValues.length >= keys.length) {
                    var data = {};
                    for (var i in keys) {           
                        var k = keys[i];
                        var v = dataValues[i].trim();
                        data[k] = { 'value': v, 'type': commands[cmd].data[k].type };
                    }
                    return data;
                }

                // no luck
                showPopupMsg('Text me as: ' + portManager.getDataInputHint(port, cmd), 10000);
	        }
	    }
        return null;
    },

    //
    // returns a data input hint string specified by port cmd
    //
    getDataInputHint: function(port, cmd) {
        var commands = portManager.getCommandGraph(port);
	    if (commands[cmd] && commands[cmd].data) {
	        var keys = Object.keys(commands[cmd].data);
            return commands[cmd].hint || keys.join(', ');
        }
        return null;
    },
}
