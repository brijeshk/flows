// This file contains the main application UI view code. 

// conventions:
// functions named doSomething are event handlers for button clicks.
// functions named showSomething update some page content.

//
// globals
//
var g_curPort = null;
var g_filterAction = null;
var g_sortFunction = null;
var g_sortOrder = -1; //ascending is 1, descending -1
var g_appPaused = false;
var g_curTab = null;
var g_timer = null;

function log(msg) {
    if (console && console.log) console.log("app: "+msg);
}

//
// is phonegap application or browser app?
//
function isDeviceApp() {
    if (window._cordovaNative) {
        console.log('Running as mobile app');
        return true;
    }
    else {
        console.log('Running as browser app');
        return false;
    }
}

//
// login handler
//
function doLogin() {
	$(".faLoaderSpinner").addClass("opened");
    u = $('#loginUser').val();
    p = $('#loginPasswd').val();
    loginAndSync(u, p, true);
    return false;
}

//
// login then retrieve contacts/ports/flows
//
function loginAndSync(u, p, saveCreds) {
    if (! userManager.login(u, p, saveCreds)) {
        $(".faLoaderSpinner").removeClass("opened");
        showPopupMsg('Invalid credentials. Please check username and password.');
        $.mobile.changePage("#loginPage"); 
        return false;
    }
    portManager.refreshVisiblePorts(userManager.curUser.id);
    flowManager.initLocalDb(userManager.curUser.id);
    flowManager.getFlowsFromLocalDb(function() {
        $(".faLoaderSpinner").removeClass("opened");
        $.mobile.changePage("#mainPage");
        showFlows();
        flowManager.getFlowsFromServer(userManager.curUser.id);
    });
    openWebSocketToServer(userManager.curUser.id, listenToSocketNotifications);
}

//
// registers new user
//
function doRegister() {
    $.mobile.changePage("#registerPage");
}

//
// reads contacts from device, finds registered AutoMeton users by phone-numbers,
// and adds them to logged-in user's AutoMeton contact list.
//
function importContacts() {
    function onSuccess(contacts) {
        showPopupMsg('found ' + contacts.length + ' contacts');
        var phoneNumbers = [];
        _.each(contacts, function(c) {
            if (c.phoneNumbers) {
                _.each(c.phoneNumbers, function(p) {
                    phoneNumbers.push(p.value);
                });
            }
        });

        if (phoneNumbers.length == 0) {
            return;
        }

        var users = userManager.searchForUsers(phoneNumbers);
        if (users) {
            var c = { acceptPorts : false };
            _.each(users, function(u) {
                userManager.updateContact(u.id, c);
            });
            showPopupMsg('Imported ' + users.length + ' contacts registered in AutoMeton.');
        }
        else {
            showPopupMsg('Found no contacts registered in AutoMeton.');
        }
    };

    function onError(contactError) {
        console.log('Error reading contacts: ' + contactError);
    };

    showPopupMsg('Importing contacts from phonebook...');
    var options      = new ContactFindOptions();
    options.multiple = true;
    var fields       = ["displayName", "name", "phoneNumbers"];
    navigator.contacts.find(fields, onSuccess, onError, options);
}

//
// logout handler, closes websocket
//
function doLogout() {
    // reset globals
    g_curPort = null;
    g_filterAction = null;
    portManager.reset();
    flowManager.reset();
    closeWebSocketToServer();
    userManager.logout(function() { $.mobile.changePage("#loginPage") } );
}

//
// handles websocket notifications of new messages
//
function listenToSocketNotifications(deltaFlows) {
    if (isDeviceApp()) {
        deltaFlows = sendAutomaticResponses(deltaFlows);
        if (deltaFlows.length == 0) return;
    }
    flowManager.mergeFlowDelta(deltaFlows);
    flowManager.selectedFlow ? showFlowDetails() : showFlows();

    // if app is in background, then fire local notification
    var a = deltaFlows[0].atoms[deltaFlows[0].atoms.length - 1];
    if (isDeviceApp() && g_appPaused == true) {
        cordova.plugins.notification.local.schedule({
            id: 1,
            title: "Autometon",
            text: a.sender + ":" + a.body.substring(0,50)
        });
    }
    return;
}

//
// handles dynamic data pulls, like location tracking.
// returns list of filtered flows that were not processed.
//
function sendAutomaticResponses(flows) {
    _.each(flows, function(f) {
        _.each(f.atoms, function(a) {
            if (a.state == 'Locate') {
                console.log("Got location request");
                var func = function(loc) {
                    if (! loc.coords.speed) loc.coords.speed = 'Unknown';
                    var d = { 'location' : { 'value' : loc.coords.latitude + ';' + loc.coords.longitude, 'type' : 'location' },
                              'speed' : { 'value' : loc.coords.speed, 'type': 'string' }
                    };
                    var date = new Date();
                    var aid = userManager.curUser.id.substring(0,3) + Math.round(date/1000);
                    var timeNow = ISODateString(date);
                    var atom = { 
                        'crud' : 'c',
                        'state': 'Respond', 
                        'sender': userManager.curUser.id, 
                        'body': '',
                        'data' : d,
                        'modified' : timeNow
                    };
                    f.atoms = [atom];
                    f.modified = timeNow;
                    f.crud = 'u';
                    f.state = atom.state;
                    f.modified = atom.timeNow;
                    postFlowDelta(userManager.curUser.id, f.users, [f]);
                };
                getCurrentLocation(func);
                f.processed = true;
            }
        });
    });
    var filteredFlows = [];
    _.each(flows, function(f) {
        if (! f.processed) filteredFlows.push(f);
    });
    return filteredFlows;
}

//
// refreshes filter button states
//
function refreshFilterPanel() {
    // get unique set of port names used in current flows
    var portnames = [];
    _.each(flowManager.cachedFlows, function(f) {
        var p = portManager.getPortFromId(f.pid);
        portnames.push(p ? p.name : 'Unknown');
    });
    portnames = _.uniq(portnames);
    var ports = [];
    _.each(portnames, function(n) { 
        ports.push({ 'name' : n });
    });
                
    var template = $('#filterListTmpl').html();
    var html = Mustache.to_html(template, { 'ports' : ports });
    $('#filterList').html(html);
    $('#f_unread').css('color', 'white');
    $('#f_todo').css('color', 'white');
    $('#f_waiting').css('color', 'white');
    if (g_filterAction) {
        $('#f_' + g_filterAction).css('color', '#B85353');
    }
    $('#filterList').listview('refresh');
    $('#filterPanel').trigger("updatelayout");
}

//
// debug function
//
function printLocalDb() {
    userManager.printLocalDb();
    flowManager.printLocalDb();
}

//
// periodic connection check
//
function doPeriodicConnectionCheck() {
    console.log('connection check');
    if (userManager.curUser && ! checkWebSocketToServer()) {
        showPopupMsg("Websocket connection lost. Re-connecting...");
        openWebSocketToServer(userManager.curUser.id, listenToSocketNotifications);
    }
}

//
// displays a list of flows in the flows table
//
function showFlows() {
    var todoCount = 0, waitingCount = 0, unreadCount = 0;

    refreshFilterPanel();

    var data = [];
    for (var i in flowManager.cachedFlows) {
        flow = flowManager.cachedFlows[i];
        port = portManager.getPortFromId(flow.pid);
        var portname = port ? port.name : "Unknown"; 
        var usersButMe = [];
        _.each(flow.users, function(u) {
            if (u != userManager.curUser.id || flow.users.length == 1) {
                usersButMe.push(u);
            }
        });

        // color flow differently if it's waiting on action
        var rowClass = '';
        var curAction = null;
        if (port) {
            var pa = portManager.isPendingAction(flow, userManager.curUser.id);
            if (pa == 1) {
                curAction = 'todo';
                todoCount++;
            }
            else if (pa == -1) {
                curAction = 'waiting';
                waitingCount++;
            }
        }

        var unreadAtoms = _.where(flow.atoms, {flags:'n'});
        if (unreadAtoms.length > 0) {
            unreadCount++;
        }

        // is there a filter in effect?
        if (g_filterAction == 'todo' || g_filterAction == 'waiting') {
            if (curAction != g_filterAction) continue;
        }
        else if (g_filterAction == 'unread' && unreadAtoms.length == 0) {
            continue;
        }
        else if (g_filterAction != null && g_filterAction != portname && usersButMe.indexOf(g_filterAction) < 0) {
            continue;
        }

        var modified = new Date(flow.modified);
        var timeNow = new Date();
        var modStr = (modified.getMonth()+1) + '/' + modified.getDate();
        var today = (timeNow.getMonth()+1) + '/' + timeNow.getDate();
        if (today == modStr) {
            modStr = modified.toTimeString().substr(0,5);
        }
        var a = flow.atoms[flow.atoms.length-1];
        var msg = a.body;
        if (msg == '' && a && a.data) {
            var items = [];
            for (k in a.data) {
                var item = a.data[k];
                if (item['type'][0] == '[') {
                    items.push(item['value'].join(','));
                }
                else if (item.type != 'location') {
                    items.push(item.value);
                }
            }
            msg = items.join(', ');
        }

        var style = 'border-left:3px solid';
        switch (curAction) {
            // colored strip on right border
            case 'todo': style += ' #ff0000'; break;
            case 'waiting': style += ' #ffff00'; break;
            //default: style += ' #f5f5f5'; break;
            default: style += ' #3b4349'; break;
        }

        var heading = (usersButMe.length == 0) ? 'me' : usersButMe.join(', ');
        msg = $("<p>").html(msg).text().substr(0, 60); // remove html markups

        var item = { 'fid' : flow.fid, 
                     'heading' : heading, 
                     'preview' : msg,
                     'gate' : portname,
                     'state' : a.state,
                     'style' : style,
                     'time': modStr,
                     'modified': modified,
                     'count': unreadAtoms.length + '/' + flow.atoms.length
                   };
        data.push(item);
    }

    if (g_sortFunction != null) {
        data = g_sortFunction(data);        
    }
        
    var template = $('#flowListTmpl').html();
    var html = Mustache.to_html(template, { 'flows' : data });
    $('#mainList').html(html);
    $('#mainList').listview('refresh');

    // reset selection
    flowManager.selectedFlow = null;
    g_curPort = null;

    // update count badges
    todoCount > 0 ? $("#todoBadge").html(todoCount) : $("#todoBadge").html("");
    waitingCount > 0 ? $("#waitingBadge").html(waitingCount) : $("#waitingBadge").html("");
    unreadCount > 0 ? $("#unreadBadge").html(unreadCount) : $("#unreadBadge").html("");
}

//
// filters flows based on current filters set, and updates flows list
//
function filterFlows(f) {
	g_filterAction = (f != '') ? f : null;
    closePanel();
    showFlows();    
}
 
function closePanel() {
    $('#filterPanel').panel('close');
}

// setup sort functions
function setSortFunction(name) {
    g_sortFunction = name;
    g_sortOrder = g_sortOrder*-1;
    closePanel();
    showFlows();  
}

// sort flows and refresh list
function sortByGate(rows) {
    var res=  _.sortBy(rows, function(row) {         
        return row.gate; 
     });
    if (g_sortOrder <0) {
        res = res.reverse();
    }
    return res;
}
                 
// sort flows and refresh list
function sortByState(rows) {
    var res=  _.sortBy(rows, function(row) {         
        return row.gate; 
     });
     if (g_sortOrder <0) {
        res = res.reverse();
    }
    return res;
}

// sort flows  and refresh list
function sortByDate(rows) {
    var res=  _.sortBy(rows, function(row) { 
        return row.modified;
     });
     if (g_sortOrder <0) {
        res = res.reverse();
    }
   return res;
}

// sort flows  and refresh list
function sortByContact(rows) {
    var res=  _.sortBy(rows, function(row) {         
        return row.heading;
     });
     if (g_sortOrder <0) {
        res = res.reverse();
    }
   return res;
}

//
// deletes current selected flow
// 
function deleteFlow(id) {                
    console.log('deleting flow ' + id);
    for (var i in flowManager.cachedFlows) {
        if (flowManager.cachedFlows[i].fid == id) {
            flowManager.selectedFlow = flowManager.cachedFlows[i];
            break;
        }
    }
    flowManager.deleteFlows(userManager.curUser.id, [flowManager.selectedFlow]);
    flowManager.selectedFlow = null;
    showFlows();    
}

//
// lists contacts of logged-in user
// retrieves and shows status of who's online as well.
//
function showContacts() {
    g_curTab = '#contactsButton';
    var template = $('#contactListTmpl').html();
    var data = { 'contacts' : userManager.curContacts,
                 'me' : userManager.curUser.id,
                 'idmin' : function() { return this.id.substring(0,2); },
				         'bgcolor' : function() { return getUserColor(this.id.substring(0,1)) },
				         'txtcolor' : function() { return getTextColor( getUserColor(this.id.substring(0,1))) }
               };
    var html = Mustache.to_html(template, data);
    $('#mainList').html(html);
    $('#mainList').listview('refresh');
}

//
// lists gates of logged-in user
//
function showMyGates() {
    g_curTab = '#gatesButton';
    var ports = portManager.getUserPorts(userManager.curUser.id);
    var template = $('#myGateListTmpl').html();
    var html = Mustache.to_html(template, { 'ports' : ports, 
                                            'uid' : function() { return userManager.curUser.id; }
    });
    $('#mainList').html(html);
    $('#mainList').listview('refresh');
}

//
// lists gates of contact user
//
function showContactGates(uid) {
    // all ports exposed by contact user to me
    var ports = portManager.getUserPorts(uid);
    // plus, all ports i expose to the contact
    var myports = portManager.getPortsVisibleFromUserToUser(userManager.curUser.id, uid);
    var template = $('#gateListTmpl').html();
    var html = Mustache.to_html(template, { 'ports' : ports, 'myports' : myports, 'uid' : function() { return uid; } });
    $('#mainList').html(html);
    $('#mainList').listview('refresh');
}

//
// import contacts from phone-book
//
function doImportContacts() {
    if (isDeviceApp()) {
        importContacts();
    }
    else {
        showPopupMsg('Only supported on mobile devices!');
    }
}

//
// function to perform init actions after JQM has initialized app fully
//
function onDeviceReady() {
    console.log("device ready");
    document.addEventListener("pause", onAppPause, false);
    document.addEventListener("resume", onAppResume, false);
}

//
// handles app pause/backgrounding event
//
function onAppPause() {
    console.log("app paused");
    g_appPaused = true;
    /*if (g_timer) {
        clearInterval(g_timer);
        g_timer = null;
    }*/
}

//
// handles app resume/foregrounding event
//
function onAppResume() {
    console.log("app resumed");
    g_appPaused = false;
    doPeriodicConnectionCheck();
    /*if (! g_timer) {
        g_timer = setInterval(doPeriodicConnectionCheck, 60000);
    }*/
}

//
// finds a given flow by fid (flowid), or most recent by pid (portid) + uid (userid)
//
function findFlowById(fid, pid, uid) {
    var f = null;
    if (flowManager.cachedFlows.length == 0) return f;
 
    for (var i=0; i<flowManager.cachedFlows.length; i++) {
        if (flowManager.cachedFlows[i].fid == fid ||
            (flowManager.cachedFlows[i].pid == pid && flowManager.cachedFlows[i].users.indexOf(uid)>=0)) { 
            f = flowManager.cachedFlows[i];
            break;
        }
    }
    return f;
}

//
// finds a given flow by fid (flowid) or pid (portid) + uid (userid)
//
function findCommonFlow(fid, pid, uid) {
    if (fid && !pid && !uid) return fid;

    var p = portManager.getPortFromId(pid);
    if (! p) return null;
    if (p.collapseFlows || p.bot) {
        // for these cases, always use a single flow per to-user
        for (var i in flowManager.cachedFlows) {
            if (flowManager.cachedFlows[i].pid == pidint && flowManager.cachedFlows[i].users.indexOf(uid)>=0) {
                fid = flowManager.cachedFlows[i].fid;
                break;
            }
        }
    }
    return fid;
}

// 
// shows Discover input popup
//
function doDiscover() {
    g_curTab = '#discoverButton';
    $.mobile.changePage('#discoverInputPage');
}

// 
// shows Settings dialog
//
function doSettings() {
    $.mobile.changePage('#settingsPage');
}

// 
// shows Help dialog
//
function showHelp() {
    $.mobile.changePage('#helpAboutAutoMeton');
}
// 
// forgot password handler
//
function doForgotPassword() {
    var uid = $('#loginUser').val();
    if (!uid || uid == '') {
        showPopupMsg('Please enter your userid first.');
        return;
    }
    var err = sendForgotPasswordRequest(uid);
    if (err) {
        showPopupMsg('Error processing password reset request: ' + err);
    }
    else {
        showPopupMsg('Please check your email for instructions to reset your password.');
    }
}

//
// entry method to setup the event handlers
//
function main() {
    // set all ajax calls to be synchronous, this aids control flow.
    $.ajaxSetup({async: false});

    // add a string format util function to all strings
    String.prototype.format = function() {
        var formatted = this;
        for (var i = 0; i < arguments.length; i++) {
            var regexp = new RegExp('\\{'+i+'\\}', 'gi');
            formatted = formatted.replace(regexp, arguments[i]);
        }
        return formatted;
    };

    $.getScript("util.js");
    $.getScript("api.js");
    $.getScript("ports.js");
    $.getScript("flows.js");
    $.getScript("users.js");
    $.getScript("flowPage.js");
    $.getScript("contactPage.js");
    $.getScript("gatePage.js");
    $.getScript("settingsPage.js");
    $.getScript("dataInputPage.js");
    $.getScript("discoverPage.js");

    if (isDeviceApp()) {
        // device app, connect to autometon.com
        // phonegap app takes care of cross-domain requests
        setRemoteServer("https://autometon.com");
    }
    else {
        // browser app
        setRemoteServer("..");
        setRemoteServer("https://autometon.com");
    }

    userManager.init();


    // 
    // --------------------ACTION HANDLERS---------------------
    //

    // setup button handlers
    $('#loginButton').click(doLogin);
    $('#registerButton').click(doRegister);
    $("#logoutButton").click(doLogout);
    $("#sendButton").click(doSendMessage);
    $("#tagsButton").click(showTopicTags);
    $("#contactsButton").click(showContacts);
    $("#contactSaveButton").click(doSaveContact);
    $("#gatesButton").click(showMyGates);
    $("#gateDisableButton").click(doDisableEnableGate);
    $("#settingsSaveButton").click(saveSettings);
    $("#helpButton").click(showHelp);
    $("#registerRegisterButton").click(registerRegister);
    $("#forgotPasswordButton").click(doForgotPassword);
    $("#dataSaveButton").click(saveDataInputDialog);
    $("#gateSaveButton").click(doSaveGate);
    $('#switchtopic').click(function() {
        $('#flowActionsMenu').one("popupafterclose", showSwitchTopics).popup("close");
    });
    $('#adduser').click(function() {
        $('#flowActionsMenu').one("popupafterclose", showAddContacts).popup("close");
    });
    $('#askbot').click(function() {
        $('#flowActionsMenu').one("popupafterclose", showAskBots).popup("close");
    });
    setupTimeTranslate();
    $('#cardButton').click(showData);
       
    $('#filterText').keypress(function(e) {
        if (e.which == '13') {
            filterFlowsKeyword();
            return false;
        }
    });
    $('#msgBox').keypress(function(e) {
        if (e.keyCode == 13 && e.shiftKey) {
            doSendMessage();
            return false;
        }
    });
    $("#flowsButton").click(function() { 
        g_curTab = '#flowsButton';
        showFlows(); 
        return;
    });

    // register pause and resume handlers
    document.addEventListener("deviceready", onDeviceReady, false);

    
    // test actions
    $('#popupMenu').on('click', 'li', function() {
        $('#popupMenu').popup('close');
    });
    $("#purgeDbButton").click(flowManager.purgeLocalDb);
    $("#syncDbButton").click(function() { 
        flowManager.getFlowsFromServer(userManager.curUser.id);
        portManager.refreshVisiblePorts(userManager.curUser.id);
        flowManager.getFlowsFromLocalDb(showFlows);
    });
    $("#logDbButton").click(printLocalDb);
    $("#settingsButton").click(doSettings);
    $("#discoverButton").click(doDiscover);
    $("#discoverSearchButton").click(doDiscoverSearch);
	$('#backButton').click(function(){
		$('#flowMessages').removeClass("dottedLine");
	})

    //
    // -------------------GESTURES--------------------
    // 

    if ($.mobile.support.touch) {
        $.event.special.swipe.horizontalDistanceThreshold = window.devicePixelRatio >= 2 ? 15 : 30;
        $.event.special.swipe.verticalDistanceThreshold = window.devicePixelRatio >= 2 ? 15 : 30; 

        // setup swipe gesture for delete flow on touch devices
        /*$('#mainList').on('swipeleft', '>li', function(e) {
            var id = $(this).find('a').data('fid');
            if (id) {
                deleteFlow(id);
            }
            return false;
        });*/

        // filter panel open and close
        $(document).on( "swipeleft swiperight", "#mainPage", function( e ) {
            if (e.type == "swiperight") {
                $("#filterPanel").panel("open");
            }
            else if (e.type == "swipeleft") {
                $("#filterPanel").panel("close");
            }
        });

        $(document).on("swipeleft swiperight", "#flowDetailPage", function(e) {
            if (e.type == "swiperight") {
                showTopicTags();
            }
            else if (e.type == "swipeleft") {
                showAddContacts();
            }
        });
    }

    // support long press and right click to select multiple items
    var selectItem = function(item) {
        var sel = item.hasClass('selected');
        if (sel) {
            item.css('background-color', '');
            item.removeClass('selected');
        }
        else {
            item.css('background-color', '#8B0000');
            item.addClass('selected');
        }
        return false;
    };

    var deleteSelectedItems = function(e) {
        $('#mainList').find(".selected").each(function() {
            var id = $(this).data('fid');
            if (id) deleteFlow(id);
            var id = $(this).data('pid');
            if (id) doDeleteGate(id);
            var id = $(this).data('cid');
            if (id) doDeleteContact(id);
        });
        $('#trashButton').hide();
    }

    var showContextMenu = function(e) {
        selectItem($(this));
        $('#trashButton').show();
        return false;
    }

    // long press options
    $('#trashButton').click(deleteSelectedItems);
    $('#mainList').on('contextmenu', 'a', showContextMenu);

    // disable click on flow page so scroll works fine
    //$('#flowContent').click(function() { return false; });


    //
    // -------------------PAGE TRANSITIONS---------------------
    //
    
    // perf optimizations
    $.mobile.defaultPageTransition   = 'none';
    $.mobile.defaultDialogTransition = 'none';

    /* dialogs with transparent background 
    $(function() {
        $('div[data-role="dialog"]').on('pagebeforeshow', function(e, ui) {
            ui.prevPage.addClass("ui-dialog-background");
        });

        $('div[data-role="dialog"]').on('pagehide', function(e, ui) {
            $(".ui-dialog-background").removeClass("ui-dialog-background");
        });
    });*/

    // support for passing params between pages
    $(document).bind("pagebeforechange", function( event, data ) {
        $.mobile.pageData = (data && data.options && data.options.pageData)
            ? data.options.pageData
            : null;
    });

    // main page
    $('#mainPage').on("pagecreate", function(e) {
        // setup auto-login with saved credentials
        userManager.useStoredCredentials(function(creds) { 
            if (creds) {
                loginAndSync(creds._id, creds.password, false);
                flowManager.lastSyncTime = creds.lastSync;
            } 
            else {
                $.mobile.changePage('#loginPage');
            }
        });
    });

    // hack to refresh flows when main page is shown
    $('#mainPage').on("pagebeforeshow", function(e) {
        if (g_curTab) {
            $(g_curTab).addClass('ui-btn-active');
            if (g_curTab == '#flowsButton') showFlows();
        }
    });
    /*
    $('#mainPage').on("pagebeforehide", function(e) {
        _.each(['#flowsButton', '#contactsButton', '#gatesButton'], function(b) {
            if ($(b).hasClass('ui-btn-active')) { 
               console.log('setting cur tab to ' + b);
               g_curTab = b; 
            }
        });
    });*/
    g_curTab = '#flowsButton';

    // contact page
    $('#contactPage').on("pagebeforeshow", function(e) {
        var id = ($.mobile.pageData) ? $.mobile.pageData.id : null;
        showContactDetails(id);
    });

    // gate page 
    $('#gatePage').on("pagebeforeshow", function(e) {
        var id = ($.mobile.pageData) ? $.mobile.pageData.id : null;
        showGateDetails(id);
    });

    // settings page 
    $('#settingsPage').on("pagebeforeshow", function(e) {
        showSettings();
    });

    // flow detail page show
    $('#flowDetailPage').on("pagebeforeshow", function(e) {
        console.log("page show for flow details page");
        if (! $.mobile.pageData) {
            console.log("no url params for flow page!");
            showFlowDetails();
            return;
        }
        var fid = $.mobile.pageData.fid;
        var pid = $.mobile.pageData.pid;
        var uid = $.mobile.pageData.uid;
        var newflow = $.mobile.pageData.newflow;
        if (newflow && newflow == '1') {
            // user initiates a new flow
            flowManager.selectedFlow = null;
        }
        else {
            // user initiates on existing flow
            flowManager.selectedFlow = findFlowById(fid, pid, uid);
        }
        
        if (pid) {
            g_curPort = portManager.getPortFromId(pid);
            if (! g_curPort) {
                console.log("invalid port id: " + pid);
                return false;
            }
        }

        flowManager.selectedFlow ? showFlowDetails() : showNewFlowDetails(uid);
    });

    $('#flowDetailPage').on("pagebeforehide", function(e) {
        console.log("flow page being hidden");
        $('#msgBox').val('');
        $('#msgBox').removeData(['data', 'state', 'gate', 'users']);
    });

    $('#flowDetailPage').on("pageshow", function(e){
        // scroll to bottom
        $(document).scrollTop($(document).height());
    });

    //
    // setup periodic websocket connection check - once every min
    //
    //g_timer = setInterval(doPeriodicConnectionCheck, 60000);

    // ready for action
    $('#loginUser').focus();
}
