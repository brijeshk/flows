// Flow manager manages flow related functions.

var flowManager = {
	cachedFlows : [],
	selectedFlow : null,
	lastSyncTime : "0",
    flowsDirty : false,
    db : null,

    reset: function() {
        flowManager.cachedFlows = [];
        flowManager.selectedFlow = null;
        flowManager.lastSyncTime = "0";
        flowManager.db = null;
        flowManager.flowsDirty = false;
    },

	//
	// mergeFlowDelta merges a flow delta package into an existing cached set of flows.
	// It is used when receiving incremental message notifications via the websocket.
	//
    mergeFlowDelta: function(deltaFlows) {
        flowManager.lastSyncTime = ISODateString(new Date()); // TODO: small time gap here, need to plug
        var insertIndex = 0;
        var flowsToSave = [];
        _.each(deltaFlows, function(df) {
            var bf = _.findWhere(flowManager.cachedFlows, {fid: df.fid});
            if (bf) { 
                // found base flow, merge the delta in
                console.log("merging flow " + df.fid);
                bf.state = df.state;
                bf.data = df.data;
                _.each(df.atoms, function(a) {
                    a.flags = 'n';
                    if (a.crud == 'u' || a.crud == 'c') {
                        var atomToUpdate = _.findWhere(bf.atoms, { aid: a.aid });
                        if (atomToUpdate) {
                            atomToUpdate.state = a.state;
                            atomToUpdate.data = a.data;
                            atomToUpdate.modified = a.modified;
                            atomToUpdate.body = a.body;
                        }
                        else {
                            bf.atoms.push(a);
                        }
                    }
                    else if (a.crud == 'd') {
                        var atomToDelete = _.findWhere(bf.atoms, { aid: a.aid });
                        if (atomToDelete) {
                            bf.atoms.splice(_.indexOf(bf, atomToDelete), 1);
                        }
                        else {
                            console.log('No matching atom found to delete');
                        }
                    }
                });
                bf.updatedFlag = true;
                bf.modified = df.modified;
                bf.users = df.users;
                // move flow to front of array
                flowManager.cachedFlows.splice(_.indexOf(flowManager.cachedFlows, bf), 1);
                flowManager.cachedFlows.splice(insertIndex, 0, bf);

                // modify local reminder if place or time has changed
                if (bf.place != df.place || bf.time != df.time) {
                    bf.place = df.place;
                    bf.time = df.time;
                    var pname = portManager.getPortFromId(df.pid).name;
                    flowManager.setupLocalReminder(df.fid, pname, df.place, df.time, true);
                }

                flowsToSave.push(bf);
            }
            else {
                // new flow
                console.log("adding new flow " + df.fid);
                df.updatedFlag = true;
                flowManager.cachedFlows.unshift(df);  // add to front of array
                flowsToSave.push(df);
                if (df.place != '' || df.time != '') {
                    var pname = portManager.getPortFromId(df.pid).name;
                    flowManager.setupLocalReminder(df.fid, pname, df.place, df.time, false);
                }
            }
            insertIndex++;
        });

        flowManager.saveFlowsToLocalDb(flowsToSave);
        userManager.updateLastSyncTime(flowManager.lastSyncTime);
    },

    //
    // getFlowsFromServer gets all the modified flows for logged-in user from the server 
    // since the last sync time
    //
    getFlowsFromServer : function(user) {
        console.log("syncing flows from server since : " + flowManager.lastSyncTime);
        var flows = getFlows(user, flowManager.lastSyncTime);
        flowManager.mergeFlowDelta(flows); 
    },

    //
    // inits client database (per user)
    //
    initLocalDb: function(userid) {
        flowManager.db = new PouchDB('flows-' + userid);
        flowManager.cachedFlows = [];
    },

    //
    // saves a list of synced flows to client db
    //
    saveFlowsToLocalDb: function(flows) {
        console.log("saving " + flows.length + " flows to local db");
        for (i in flows) {
            var f = flows[i];
            if (! f._id) f._id = f.fid; 
            flowManager.db.put(f, function cb(e, r) {
                if (e) {
                    console.log('Pouchdb error: ' + e.message);
                }
                else {
                    f['_rev'] = r.rev;
                }
            });
        };
        flowManager.flowsDirty = true;
    },

    //
    // gets flows from client database
    //
    getFlowsFromLocalDb: function(cb) {
        console.log("reading flows from local db");
        flowManager.cachedFlows = [];
        flowManager.db.allDocs({include_docs: true}, function(err, doc) {
            if (doc && doc.rows) {
                _.each(doc.rows, function(d) {
                    flowManager.cachedFlows.push(d.doc);
                });
                flowManager.cachedFlows = _.sortBy(flowManager.cachedFlows, 'modified').reverse();
                if (cb) cb();
            }
        });
    },

    //
    // deletes flow from client database
    //
    deleteFlowFromLocalDb: function(f) {
        console.log("deleting flow from local db");
        flowManager.db.remove(f);
    },

    //
    // util method to print out contents of local db
    //
    printLocalDb: function() {
        console.log("local flow db contents:");
        flowManager.db.allDocs({include_docs: true}, function(err, doc) {
            _.each(doc.rows, function(d) {
                console.log(JSON.stringify(d.doc));
            });
        });
    },

    //
    // purges client database
    //
    purgeLocalDb: function() {
        console.log("purging local db!");
        flowManager.db.destroy().then(function() { console.log('local flows db purged'); }).catch(function (e) { console.log(e); });
        userManager.db.destroy();
        flowManager.cachedFlows = [];
        flowManager.selectedFlow = null;
        flowManager.lastSyncTime = '0';
        userManager.updateLastSyncTime(flowManager.lastSyncTime);
    },

    //
    // deletes a set of flows from the server
    //
    deleteFlows: function(user, flows) {
        console.log("deleting flows on server");
        _.each(flows, function(f) {
            // post delta to remove from server
            f.crud = 'd';
            postFlowDelta(user, [user], [f]);
            // remove from cached list
            flowManager.cachedFlows.splice(_.indexOf(flowManager.cachedFlows, f), 1);
            // delete from local db
            flowManager.deleteFlowFromLocalDb(f);
        });
    },

	//
	// sends a flow message to a list of users, storing them in each of their server accounts.
	// updates the sender's server store as well.
	// server handles notifications to online users.
	//
	sendMessage: function(fromUser, toUsers, pid, state, data, body, where, when) {
	    if (toUsers.length == 0 && flowManager.selectedFlow != null) {
	        // get users from selected flow
	        toUsers = _.reject(flowManager.selectedFlow.users, function(u) { return u == fromUser; }); 
	    }
	    if (toUsers.length == 0) {
	        return false;
	    }
        
	    var port = portManager.getPortFromId(pid);
	    if (flowManager.selectedFlow != null) {
	        var oldPort = portManager.getPortFromId(flowManager.selectedFlow.pid);
	        if (port.id != oldPort.id) {
                console.log('sending non port message');
	            //showPopupMsg("Switching topics on this thread");
	            // switched port new state overrides existing state in this case
	            //flowManager.selectedFlow.state = state;
	        }
	        else {
	            port = oldPort;
	        }
	    }
	    if (! port) {
            showPopupMsg('Cannot identify a valid port.');
            return;
	    } 
	   
	    //++ make up atom and flow 
	    var date = new Date();
	    var aid = fromUser.substring(0,3) + Math.round(date/1000);
	    var timeNow = ISODateString(date);
	    var atom = { 
	        'crud' : 'c',
	        'state': state, 
	        'sender': fromUser, 
	        'body': body,
	        'data' : data,
	        'modified' : timeNow
	    };
        var users = toUsers;
        if (_.indexOf(toUsers, fromUser) < 0) {
            users.push(fromUser);
        }
	    var flow = { 
	        'users' : users, 
	        'pid' : port.id,
	        'state' : state,
	        'data': null,
	        'atoms' : [atom],
	        'modified' : timeNow,
			'place' : where,
			'time' : when
	    };
	    if (flowManager.selectedFlow == null) {  // new flow
	        flow.fid = generateUUID();
	        flow['crud'] = 'c';
            if (state == 'Update') flow.data = { 'type':'number', 'value':parseInt(atom.data.Value.value) };
            if (state == 'Settle') flow.data = { 'type' : 'number', 'value' : 0 };
            if (flow.place != '' || flow.time != '') {
                flowManager.setupLocalReminder(flow.fid, port.name, flow.place, flow.time, false);
            }			
	    }
	    else {  // reply on existing flow
	        flow.fid = flowManager.selectedFlow.fid;
	        // Say and Remind are "soft" states. They don't overwrite the existing state on the flow.
	        if (flow.state == 'Say' || flow.state == 'Remind') {
	           flow.state = flowManager.selectedFlow.state;
	        }
	        else {
	           flowManager.selectedFlow.state = flow.state;
	        }
            if (state == 'Update') flowManager.selectedFlow.data.value += parseInt(atom.data.Value.value);
            if (state == 'Settle') flowManager.selectedFlow.data.value = 0;
            flow.data = flowManager.selectedFlow.data;
            flowManager.selectedFlow.users = flow.users;
	        flow['crud'] = 'u';
	        flowManager.selectedFlow.atoms.push(atom);
	        flowManager.selectedFlow.modified = flow.modified;
          if (flow.place != flowManager.selectedFlow.place || flow.time != flowManager.selectedFlow.time) {
              flowManager.setupLocalReminder(flow.fid, port.name, flow.place, flow.time, true);
          }
			flowManager.selectedFlow.place = flow.place;
			flowManager.selectedFlow.time = flow.time;
	    }
	    atom.flags = 'n';
	    atom.aid = aid;
	    atom.fid = flow.fid;
	    //-- make up atom and flow
	    
	    // all set, send message
	    console.log('Sending flow payload:');
	    console.log(JSON.stringify(flow, null, 4));
	    var flows = [];
	    flows.push(flow);
	    postFlowDelta(fromUser, users, flows);
	    flowManager.lastSyncTime = ISODateString(new Date()); // TODO: small time gap here, need to plug	

	    if (flowManager.selectedFlow == null) {
	    	// add new flow to top
	        flowManager.cachedFlows.unshift(flow);
	        flowManager.selectedFlow = flow;
	    }
	    else {
	        // bubble the existing flow to the top
	        flowManager.cachedFlows.splice(_.indexOf(flowManager.cachedFlows, flowManager.selectedFlow), 1);
	        flowManager.cachedFlows.splice(0, 0, flowManager.selectedFlow);
	    }
        // update flow in local db
        flowManager.saveFlowsToLocalDb([flowManager.selectedFlow]);		
	},

    //
    // sends a one-off bot message to bot
    //
    sendBotMessage: function(fromUser, botUser, port, state, data, body) {
	    var date = new Date();
	    var aid = fromUser.substring(0,3) + Math.round(date/1000);
	    var timeNow = ISODateString(date);
	    var atom = { 
	        'crud' : 'c',
	        'state': state, 
	        'sender': fromUser, 
	        'body': body,
	        'data' : data,
	        'modified' : timeNow
	    };
	    var flow = { 
            'fid' : flowManager.selectedFlow.fid,
	        'users' : flowManager.selectedFlow.users, 
	        'pid' : port.id,
	        'state' : state,
	        'data': null,
	        'atoms' : [atom],
	        'modified' : timeNow,
            'crud' : 'u'
	    };
	    atom.flags = 'n';
	    atom.aid = aid;
	    atom.fid = flow.fid;
	    console.log('Sending bot payload:');
	    console.log(JSON.stringify(flow, null, 4));
	    postFlowDelta(fromUser, [botUser], [flow]);
    },

    //
    // adds an event to local device calendar.
    //
    addEventToCalendar: function(flow) {
        if (! window.plugins || ! window.plugins.calendar) {
            console.log('no calendar plugin found');
            return;
        }
        console.log('creating calendar event');
        var a = portManager.findLastAtomWithState(flow, 'Schedule');
        if (!a || !a.data) {
            console.log('No meeting data found in flow!');
            return;
        }
        var x = a.data['Date'].value.split('-');
        var st = a.data['At'].value.split(':');
        var dur = parseInt(a.data['Duration'].value);
        var sdate = new Date(x[0], x[1]-1, x[2], st[0], st[1], 0, 0, 0); 
        var edate = new Date(x[0], x[1]-1, x[2], st[0], parseInt(st[1]) + dur, 0, 0, 0); 
        var title = a.data['About'].value;
        var success = function(m) { showPopupMsg('Added calendar event'); };
        var error = function(m) { showPopupMsg('Error adding calendar event: ' + m); };
        window.plugins.calendar.createEvent(title, '', '', sdate, edate, success, error);
    },
	
    //
    // setup local reminder
    //
    setupLocalReminder: function(flowId, about, place, time, isUpdate) {
        if (isDeviceApp()) {
            console.log('setting up local reminder');
            var tt = chrono.parse($(this).val());
            if (! tt || ! tt[0]) {
				console.log('Cannot parse time string using chrono');
				return;
			}			
            if (isUpdate) {
                cordova.plugins.notification.local.update({
                    id: flowId,
                    title: "Autometon",
                    text: about + ' at ' + place,
                    at: tt[0].start.date()
                });
            }
            else {
                cordova.plugins.notification.local.schedule({
                    id: flowId,
                    title: "Autometon",
                    text: about + ' at ' + place,
                    at: tt[0].start.date()
                });
            }
            showPopupMsg('Added reminder for : ' + time, 4000);
        }
    },
}
