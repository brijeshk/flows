
function showSettings() {
    var s = userManager.curSettings;
    $('#myUserId').val(userManager.curUser.id);
    $('#myPassword').val(s.password);
    $('#myPhoneNumber').val(userManager.curUser.phone);
    $("#myPhoneNumber").prop('disabled', true);
    $('#settingsRegisterButton').hide();
    $('#settingsSaveButton').show();
}

function saveSettings() {
    var s = { 
        'password': $('#myPassword').val(),
        'storeOnServer' : false 
    };
    userManager.updateSettings(s);
}

function registerRegister() {
    console.log("registering new user");
    var u = $('#registerUserId').val();
    var ph = $('#registerPhoneNumber').val();
    var p = $('#registerPassword').val();
    //var p = (isDeviceApp()) ? device.uuid : 'NoDeviceId';
    var e = userManager.register(u, p, '', ph, '');
    if (e) {
        showPopupMsg('Unable to register new user: ' + e);
        return false;
    }
    if (isDeviceApp()) {
        importContacts();
    }
    // auto-login
    $('#loginUser').val(u);
    $('#loginPasswd').val(p);
    return doLogin();
}
