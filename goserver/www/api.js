// This file contains functions that communicate with the server API.

var g_serverAddr = "..";
var g_authHeader = null;
var g_websocketConnection = null;

//
// checks if there is an online connection
//
function isOnline() {
    if ('onLine' in navigator) {
       return navigator.onLine;
    }
    else {
        // default to true optimistically
        return true;
    }
}

//
// sets remote server address depending on app context
//
function setRemoteServer(addr) {
    g_serverAddr = addr;
}

function authHeader(u, p) {
    return { "Authorization": "Basic " + btoa(u + ":" + p) };
}

//
// Sends user credentials to server to authenticate.
// Returns a boolean indicating status.
//
function loginUser(userid, password) {
    console.log("api: logging in user " + userid);
    g_authHeader = authHeader(userid, password); 
    var authenticated = false;
    $.ajax({
        type: "GET",
        url: g_serverAddr + "/users/login",
        //username: userid,
        //password: password,
        headers: g_authHeader,
        async: false,
        success: function(data) {
            authenticated = true;
        },
        error: function(request, error) {
            console.log("Error from GET /users/login: " + request.responseText);
        }
    });
    return authenticated;
}

//
// Registers a new user with server.
// Returns an error string (null if successful).
//
function registerUser(user) {
    console.log("api: registering user " + user.id);
    var e = null;
    $.ajax({
        type: "POST",
        url: g_serverAddr + "/users",
        contentType: 'application/json',
        data: JSON.stringify(user),
        //headers: authHeader('admin', 'admin'), // TODO: need authorization to create user
        async: false,
        success: function(data) {
            e = null;
        },
        error: function(request, error) {
            console.log("Error from POST /users: " + request.responseText);
            e = request.responseText;
        }
    });
    return e;
}

//
// Gets the public user profile of a given user from server.
// If user doesn't exist, returns null.
//
function getUser(userid) {
    console.log("api: get user " + userid);
    var user = null;
    $.getJSON(g_serverAddr + '/users/id/' + userid)
        .done(function(data) { user = data; })
        .fail(function(jqxhr, textStatus, error) { console.log("Error getting user profile: " + textStatus + ", " + error); });
    return user;
}

//
// given a list of phone numbers, finds users registered by these numbers
//
function findUsersByPhoneNumber(phoneNumbers) {
    console.log("api: finding users by phone numbers");
    var users = null;
    $.ajax({
        type: "PUT",
        url: g_serverAddr + "/searchusers",
        data: JSON.stringify(phoneNumbers),
        contentType: 'application/json',
        success: function(data) {
            users = data;
        },
        error: function(request, error) {
            console.log("Error from PUT /searchusers: " + request.responseText);
            users = null;
        }
    });
    return users;
}

//
// sends password reset request for userid
//
function sendForgotPasswordRequest(userid) {
    console.log('api: sending forgot password request');
    var err = null;
    $.ajax({
        type: "POST",
        url: g_serverAddr + "/users/gentoken/" + userid,
        success: function(data) {
        },
        error: function(request, error) {
            console.log("Error from POST /users/gentoken: " + request.responseText);
            err = request.responseText;
        }
    });
    return err;
}

//
// Adds a user to a given user's contacts.
// Returns null if successful or error string if not. 
// 
function updateContact(forUserId, contactId, contact) {
    console.log("api: updating contact " + contactId);
    var r = null;
    $.ajax({
        type: "PUT",
        url: g_serverAddr + "/contacts/" + forUserId + "/" + contactId,
        data: JSON.stringify(contact),
        contentType: 'application/json',
        headers: g_authHeader,
        success: function(data) {
            console.log("added contact successfully");
        },
        error: function(request, error) {
            console.log("Error from PUT /contacts: " + request.responseText);
            r = request.responseText;
        }
    });
    return r;
}

//
// Removes a user from a given user's contacts.
// Returns null if successful or error string if not. 
// 
function removeContact(forUserId, contactId, contact) {
    console.log("api: removing contact " + contactId);
    var r = null;
    $.ajax({
        type: "DELETE",
        url: g_serverAddr + "/contacts/" + forUserId + "/" + contactId,
        contentType: 'application/json',
        data: JSON.stringify(contact),
        headers: g_authHeader,
        success: function(data) {
            console.log("removed contact successfully");
        },
        error: function(request, error) {
            console.log("Error from DELETE /contacts: " + request.responseText);
            r = request.responseText;
        }
    });
    return r;
}

//
// Retrieves ports visible to user (exposed by other users).
// Visible ports = Default ports + public ports of other users + specific ports exposed to user
//
function getPortsVisibleToUser(userid) {
    console.log("api: getting ports visible to user");
    var ports = [];
    $.getJSON(g_serverAddr + '/ports/visible/' + userid + '?publicPorts=true')
        .done(function(data) { ports = data; })
        .fail(function(jqxhr, textStatus, error) { console.log("Error getting visible ports: " + textStatus + ", " + error); });
    return ports;
}

//
// Retrieves ports exposed by user to me.
//
function getPortsExposedByUser(userid, me) {
    console.log("api: getting ports exposed by user");
    var ports = [];
    $.getJSON(g_serverAddr + '/ports/user/' + userid + '/' + me)
        .done(function(data) { ports = data; })
        .fail(function(jqxhr, textStatus, error) { console.log("Error getting exposed ports: " + textStatus + ", " + error); });
    return ports;
}

function discoverPorts(userid, term, distance) {
    console.log("api: discovering ports");
    var data = null;
    var q = term ? ('?q=' + term) : '';
    $.getJSON(g_serverAddr + '/ports/discover/' + userid + '/' + distance + q)
        .done(function(d) { data = d; })
        .fail(function(jqxhr, textStatus, error) { console.log("Error getting discovered ports: " + textStatus + ", " + error); });
    return data;
}

//
// Creates a new port on the server.
//
function createPort(port) {
    console.log("api: creating port");
    var e = null;
    $.ajax({
        type: "POST",
        url: g_serverAddr + "/ports",
        contentType: 'application/json',
        data: JSON.stringify(port),
        headers: g_authHeader,
        success: function(data) {
            console.log("created port successfully");
        },
        error: function(request, error) {
            console.log("Error from POST /ports: " + request.responseText);
            e = request.responseText;
        }
    }); 
    return e;
}

//
// Updates an existing port on the server.
//
function updatePort(port) {
    console.log("api: updating port");
    var e = null;
    $.ajax({
        type: "PUT",
        url: g_serverAddr + "/ports/" + port.id,
        contentType: 'application/json',
        data: JSON.stringify(port),
        headers: g_authHeader,
        success: function(data) {
            console.log("updated port successfully");
        },
        error: function(request, error) {
            console.log("Error from PUT /ports: " + request.responseText);
            e = request.responseText;
        }
    }); 
    return e;
}

//
// Updates an existing port on the server.
//
function deletePort(pid) {
    console.log("api: deleting port");
    var e = null;
    $.ajax({
        type: "DELETE",
        url: g_serverAddr + "/ports/" + pid,
        contentType: 'application/json',
        headers: g_authHeader,
        success: function(data) {
            console.log("deleted port successfully");
        },
        error: function(request, error) {
            console.log("Error from DELETE /ports: " + request.responseText);
            e = request.responseText;
        }
    }); 
    return e;
}

//
// Gets all the flows belonging to userid since a given timestamp.
//
function getFlows(userid, sinceTime) {
    console.log("api: getting flows");
    var flows = [];
    $.ajax({
        type: "GET",
        url: g_serverAddr + "/flowDelta/" + userid + "/" + sinceTime,
        async: false,
        headers: g_authHeader,
        success: function(data) {
            flows = data;
        },
        error: function(request, error) {
            console.log("Error from GET /flowDelta: " + request.responseText);
        }
    });
    return flows;
}

//
// Posts a flow delta set to the server for the given list of userid accounts.
//
function postFlowDelta(fromUser, userids, flows) {
    console.log("api: sending flow message");
    _.each(userids, function(u) {
        if (u == fromUser) return;
        $.ajax({
            type: "POST",
            url: g_serverAddr + "/flowDelta/" + u,
            data: JSON.stringify(flows),
            async: false,
            contentType: 'application/json',
            headers: g_authHeader,
            success: function(data) {
            },
            error: function(request, error) {
                console.log("Error from POST /flowDelta: " + request.responseText);
            }
        });
    });
}

//
// Updates flow and atom flags (unread/tagged) on server.
//
function updateFlowFlags(forUser, flow) {
    console.log("api: updating flow flags");
    var minFlow = { fid : flow.fid,
		    atoms : _.map(flow.atoms, function(a) { return {aid:a.aid,flags:a.flags}; })
		  };
    $.ajax({
        type: "POST",
        url: g_serverAddr + "/flowFlags/" + forUser,
        data: JSON.stringify(minFlow),
        async: false,
        headers: g_authHeader,
        contentType: 'application/json',
        success: function(data) {
            console.log("updated flags successfully");
        },
        error: function(request, error) {
            console.log("Error from POST /flowFlags: " + request.responseText);
        }
    });
}

//
// Gets online contacts for a given user.
// 
function getOnlineContacts(userid) {
    console.log("api: getting online contacts");
    var contacts = [];
    $.ajax({
        type: "GET",
        url: g_serverAddr + "/onlinecontacts/" + userid,
        async: false,
        headers: g_authHeader,
        success: function(data) {
            contacts = data;
        },
        error: function(request, error) {
            console.log("Error from GET /onlineContacts: " + request.responseText);
        }
    });
    return contacts;
}

//
// Opens a websocket connection to server, to receive flow messages sent by others.
// 
function openWebSocketToServer(userid, receiveCallback) {
    if (! "WebSocket" in window) {
        alert("WebSockets not supported by your browser. No notifications, sorry!");
        return;
    }

    var loc = window.location, new_uri;
    if (loc.protocol === "https:") {
        url = "wss://";
    } else {
        url = "ws://";
    }
    if (g_serverAddr == "..") {
        url += loc.host;
    }
    else {
        url += "autometon.com:5000";
    }
    url += "/websocket/" + userid;
    console.log("connecting to websocket url: " + url);

    g_websocketConnection = new WebSocket(url);
    g_websocketConnection.onopen = function() {
        console.log("websocket connection opened");
    };

    g_websocketConnection.onmessage = function (evt) { 
        var received_msg = evt.data;
        console.log("received websocket msg: " + received_msg);
        var flowDelta = eval(evt.data);
        receiveCallback(flowDelta);
    };

    g_websocketConnection.onclose = function() { 
        g_websocketConnection = null;
        console.log("websocket connection closed"); 
    };
}

//
// Checks health of current websocket connection, returns boolean
// 
function checkWebSocketToServer() {
    if (g_websocketConnection) {
        return (g_websocketConnection.readyState == WebSocket.OPENING || g_websocketConnection.readyState == WebSocket.OPEN);
    }
    else {
        return false;
    }
}

//
// Closes websocket connection to server.
//
function closeWebSocketToServer() {
    if (g_websocketConnection) {
        g_websocketConnection.close();
        g_websocketConnection = null;
        console.log("websocket closed");
    }
}
