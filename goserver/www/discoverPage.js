//
// Handles discover related functionality
// 

//
// reads discovery search parameters from search dialog,
// searches on server, and displays results in main list
//
function doDiscoverSearch() {
    var term = $("#discoverTerm").val();
    var degree = $("#discoverDegree").val();
    var loc = $("#discoverLocation").val();
    var radius = $("#discoverRadius").val();

    // TODO: factor in location
    var data = discoverPorts(userManager.curUser.id, term, degree);
    if (data == null) {
       showPopupMsg('Error retrieving discover results from server');
       return;
    } 
    var template = $('#discoverGateListTmpl').html();
    _.each(data.ports, function(p, i) { 
        p.distance = data.distances[i];
    });
    var html = Mustache.to_html(template, { 
        'ports' : data.ports,
        'getDistance': function() { 
            if (this.distance == -1) return '';  
            if (this.distance == 1) return 'Direct';
            if (this.distance >= 2) return this.distance + ' hops'; 
        }
    });
    $('#mainList').html(html);
    $('#mainList').listview('refresh');
}

