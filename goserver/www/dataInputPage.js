// Handles data input items for a gate command.
// Shows a form dialog based on data input fields, and saves the values.

//
// gets current location using GeoLocation plugin.
// calls func callback with location if specified
//
function getCurrentLocation(func) {
    var onSuccess = function(position) {
        console.log('Latitude: '          + position.coords.latitude          + '\n' +
              'Longitude: '         + position.coords.longitude         + '\n' +
              'Altitude: '          + position.coords.altitude          + '\n' +
              'Accuracy: '          + position.coords.accuracy          + '\n' +
              'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
              'Heading: '           + position.coords.heading           + '\n' +
              'Speed: '             + position.coords.speed             + '\n' +
              'Timestamp: '         + position.timestamp                + '\n');
        if (func) func(position);
    };

    function onError(error) {
        console.log('Error getting geolocation. Code: ' + error.code + ', ' + 'Message: ' + error.message);
    }

    if (navigator.geolocation) {
        showPopupMsg('Getting current location...');
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }
    return;
}

//
// shows a dynamic form dialog with form input widgets to input data for a particular cmd
//
function showDataInputDialog(cmd, data) {
    var html = '';
    var needSomeUserInput = false;
    _.each(data, function(v, k) {
        if (v.source != 'user') return; // skip items that don't need user input
        needSomeUserInput = true;
        var k2 = k.replace(' ', '_');
        html += '<div class="ui-field-contain"><label for="data_{0}">{0}</label>'.format(k2);
        if (v.type == 'string') {
            if (v.value && v.inputAction == 'select1') {
                html += '<select id="data_{0}">'.format(k2);
                _.each(v.value, function(option) {
                    html += '<option value="{0}">{0}</option>'.format(option);
                });
                html += '</select>';
            }
            else {
                html += '<input type="text" id="data_{0}">'.format(k2);
            }
        }
        else if (v.type == 'location') {
            var f = function(loc) {
                $('#data_' + k2).val(loc.coords.latitude + ';' + loc.coords.longitude);
            }
            getCurrentLocation(f);
            html += '<input type="text" id="data_{0}">'.format(k2);
        }
        else if (v.type == 'date') {
            html += '<input type="text" data-role="datebox" data-options=\'{"mode":"calbox"}\' id="data_{0}">'.format(k2);
        }
        else if (v.type == 'time') {
            html += '<input type="text" data-role="datebox" data-options=\'{"mode":"timebox"}\' id="data_{0}">'.format(k2);
        }
        else if (v.type == 'int' || v.type == 'price' || v.type == '' || v.type == 'quantity') {
            html += '<input type="number" pattern="[0-9]*" id="data_{0}">'.format(k2);
        }
        html += '</div>';
    });
    if (! needSomeUserInput) return; // no user input needed

    $('#dataFields').html(html);
    $('#dataTitle').html(cmd);
    $('#dataFields').trigger('create');
    $('#dataPage').trigger('create');
    $.mobile.changePage("#dataPage");
}

//
// saves items in data dialog
//
function saveDataInputDialog() {
    var k = $('#msgBox').data('state');
    var dataValues = [];
    var commands = portManager.getCommandGraph(g_curPort);
    if (g_curPort && commands[k] && commands[k].data) {
        _.each(commands[k].data, function(v, k) {
            dataValues.push($('#data_' + k.replace(' ','_')).val());
        });
        $('#msgBox').data('data', dataValues);
        doSendMessage();
    }
}
