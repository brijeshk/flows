// functions to manage the Gate Details page.
/*
function setupContactAutoComplete() {
    $('#gateVisibleTo').atwho({
        at : "@",
        limit: 10,
        suffix: '',
        start_with_space: false,
        callbacks : {
            remote_filter: function(q, callback) {
                var options = [];
                var allOptions = ['world'];
                _.each(userManager.curUser.contacts, function(v,c) { allOptions.push(c); });
                var i = 0;
                _.each(allOptions, function(c) {
                    if (c.toLowerCase().indexOf(q.toLowerCase()) == 0) {
                        options.push({"id":i, "name":c});
                        i++;
                    }
                });
                
                callback(options);
            },
            before_insert: function(value, elem) {
                return value + ',';
            }
        }
    });
}*/

//
// populates fields in gate details dialog
//
function showGateDetails(id) {
    var visible = [];
    visible.push('<option value="world">World</option>');
    $.each(userManager.curUser.contacts, function(c) {
        visible.push('<option value="'+ c +'">'+ c +'</option>');
    });
    $('#gateVisibleTo').html(visible.join(''));

    if (id && id != '') { 
        g_curPort = portManager.getPortFromId(id);
        if (! g_curPort) {
            console.log('Error getting port for id: ' + id);
            return;
        }
        $("#gateName").val(g_curPort.name);
        $("#gateDesc").val(g_curPort.description);       
        $("#gateChoices").val(g_curPort.choices.values ? g_curPort.choices.values.join(',') : '');

        var actions = [];
        if (g_curPort.questions) actions.push('questions');
        if (g_curPort.tasks) actions.push('tasks');
        if (g_curPort.appointments) actions.push('appointments');
        if (g_curPort.offers) actions.push('offers');
        if (g_curPort.locate) actions.push('location');
        if (g_curPort.balances) actions.push('balances');		
        $('#gateActions').val(actions);	

        $('#gateVisibleTo').val(g_curPort.visibleTo);

        $("#gateDisableButton").html(g_curPort.disabled ? "Enable" : "Disable");
        $("#gateDisableButton").show();
        $("#gateDeleteButton").show();
    }
    else {
        g_curPort = null;
        $("#gateName").val('');
        $("#gateDesc").val('');
        $("#gateChoices").val('');
        $("#gateVisibleTo").val([]);
        $('#gateActions').val([]);
        $("#gateDisableButton").hide();
        $("#gateDeleteButton").hide();
    }
    $('#gateActions').selectmenu('refresh');	
    $('#gateVisibleTo').selectmenu('refresh');
    //setupContactAutoComplete();
}

//
// saves gate details
//
function doSaveGate() {
    if (! g_curPort) {
        g_curPort = { 'creator' : userManager.curUser.id,
            'canStartWithSay' : true,
            'commands' : {},
            'visibleTo' : []
        };
    }
    g_curPort.name = $("#gateName").val();
    g_curPort.description = $("#gateDesc").val();
    g_curPort.visibleTo = $("#gateVisibleTo").val();

    var actions = $('#gateActions').val();
    g_curPort.questions = _.contains(actions, 'questions');
    g_curPort.tasks = _.contains(actions, 'tasks');
    g_curPort.appointments = _.contains(actions, 'appointments');
    g_curPort.offers = _.contains(actions, 'offers');
    g_curPort.balances = _.contains(actions, 'balances');
    g_curPort.locate = _.contains(actions, 'location');
    g_curPort.canStartWithSay = true;

    var c = $('#gateChoices').val();
    if (c) {
        var cmds = [];
        if (g_curPort.questions) cmds.push('Ask');
        if (g_curPort.tasks) cmds.push('Assign');
        if (g_curPort.appointments) cmds.push('Schedule');
        g_curPort.choices = { 'commands' : cmds, 'values' : c.split(',') };
    }

    var e = g_curPort.id ? updatePort(g_curPort) : createPort(g_curPort);
    if (e) {
        showPopupMsg(e);
        return false;
    }
    if (! g_curPort.id) {
        portManager.refreshVisiblePorts(userManager.curUser.id);
    }
    showMyGates();
}

//
// disables or enables gate
//
function doDisableEnableGate() {
    if (! g_curPort) return;
    g_curPort.disabled = ! g_curPort.disabled;
    doSaveGate();
}

//
// deletes gate
//
function doDeleteGate(id) {
    var e = deletePort(id);
    if (e) {
        showPopupMsg(e);
        return false;
    }
    else {
        portManager.refreshVisiblePorts(userManager.curUser.id);
        showMyGates();
    }
}
