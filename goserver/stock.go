package main

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

// GetStockQuoteBySymbol returns the full name and current quote of a stock symbol by querying the Yahoo Finance API.
// Result is returned as a string map {"name":X, "price":Y}.
func (s *SystemService) GetStockQuoteBySymbol(symbol string) (map[string]string, error) {
	if symbol == "" {
		return nil, nil
	}

	resp, err := http.Get("http://download.finance.yahoo.com/d/quotes.csv?s=" + symbol + "&f=nl1")
	if err != nil {
		log.Println("Error getting quote from web: ", err.Error())
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	fields := strings.Split(strings.TrimSpace(string(body)), ",")
	if len(fields) < 2 {
		log.Println("Web response not in CSV format: ", body)
		err = errors.New("Invalid response from API")
		return nil, err
	}

	quote := make(map[string]string)
	quote["name"] = strings.Replace(fields[0], "\"", "", -1)
	quote["price"] = fields[1]
	return quote, nil
}
