#!/bin/bash
# get users
curl http://localhost:5000/users
# get ports visible to
curl http://localhost:5000/ports/visible/brijesh
# get ports exposed by
curl http://localhost:5000/ports/user/brijesh
# create user
curl -H "Content-Type:application/json" -d '{"id":"userA", "password": "userA"}' http://localhost:5000/users
curl http://localhost:5000/users/id/userA
# create port
curl -H "Content-Type:application/json" -d '{"id":501, "name":"testPort", "creator":"brijesh", "commands": { "Ask": { "transitions" : ["Answer"] }, "Tell": { "transitions": [] }}}' http://localhost:5000/ports
# create group
curl -H "Content-Type:application/json" -d '{"name":"testGroup", "creator":"brijesh", "users":["brijesh","ramya","manasi","nikhil"]}' http://localhost:5000/groups
curl http://localhost:5000/groups/id/0
curl http://localhost:5000/groups/user/brijesh
# get flows
curl http://localhost:5000/flowDelta/brijesh/2014-11-01T22:08:41+00:00
# discover ports
curl http://localhost:5000/ports/discover/brijesh/2
