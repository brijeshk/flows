package main

import (
	"log"
)

func MakeDefaultPorts() []Port {
	generalPort := Port{Tasks: true,
		Appointments:    true,
		Questions:       true,
		Name:            "General",
		Description:     "Miscellaneous topics",
		Factory:         true,
		VisibleToUsers:  []string{"world"},
		CanStartWithSay: true,
		CollapseFlows:   false,
	}
	ports := []Port{generalPort}
	return ports
}

// system service is an empty type to hold different system services
type SystemService struct{}

func (port *Port) FireCommand(command string, input string) (dataResult interface{}, err error) {
	return dataResult, err

	/*
		// if there's no processor, then nothing to return
		cmdInfo, ok := port.Commands[command]
		if ok == false {
			log.Printf("Command %s doesn't have any fire actions in port %s\n", command, port.Name)
			dataResult = nil
			return
		}

		// if cmd has data, just return it
		// just return it
		if cmdInfo.OutputData != nil {
			dataResult = cmdInfo.OutputData
			return
		}

		if cmdInfo.Function == "" {
			dataResult = nil
			return
		}

		defer func() {
			if r := recover(); r != nil {
				log.Println("Recovered from panic from fire function: ", r)
				dataResult = nil
			}
		}()

		// reflect on and find the processor function
		var ss SystemService
		processorFunction := reflect.ValueOf(&ss).MethodByName(cmdInfo.Function)
		if processorFunction.IsValid() == false {
			err = errors.New("Cannot find processor function " + cmdInfo.Function)
			log.Println(err.Error())
			return
		}

		// execute it
		log.Printf("Executing fire function: %s in port %s\n", cmdInfo.Function, port.Name)
		reflectValues := processorFunction.Call([]reflect.Value{reflect.ValueOf(input)})
		dataResult = reflectValues[0].Interface()
		err = nil
		return
	*/
}

// ComputeDegreeBetweenUsers computes the degree of connectedness between 2 users.
// Returns 0 if user1 = user2.
// Returns 1 if both are in each others' contacts.
// Returns 2 if there's a third user that has both of them in his/her contacts.
// And so on.
// Returns -1 if there exists no path between them.
func ComputeDegreeBetweenUsers(user1 string, user2 string) (int, error) {
	// keep an in-memory map of intermediate distances
	distances := make(map[string]int)
	d, err := computeDegreeRecursive(user1, user2, distances)
	return d, err
}

func makeDistKey(u1 string, u2 string) string {
	if u2 < u1 {
		return u2 + ":" + u1
	} else {
		return u1 + ":" + u2
	}
}

// computeDegreeRecursive recursively computes distance between 2 users,
// using a DFS strategy, storing intermediate distances computed in a distance map
func computeDegreeRecursive(user1 string, user2 string, distances map[string]int) (int, error) {
	//log.Println("computing degree between " + user1 + " and " + user2)
	//log.Println("distance map : ", distances)

	// identity distance?
	if user1 == user2 {
		distances[makeDistKey(user1, user2)] = 0
		return 0, nil
	}

	u1, err := GetUser(user1, false)
	if err != nil {
		return 0, err
	}
	u2, err := GetUser(user2, false)
	if err != nil {
		return 0, err
	}

	// direct distance? both users must have each other in their contacts
	_, u2IsContactOfu1 := u1.Contacts[user2]
	_, u1IsContactOfu2 := u2.Contacts[user1]
	if u2IsContactOfu1 && u1IsContactOfu2 {
		distances[makeDistKey(user1, user2)] = 1
		return 1, nil
	}

	// no direct contact, compute distances from user1's contacts to user2 inductively

	minDegree := -1
	distances[makeDistKey(user1, user2)] = minDegree

	for c := range u1.Contacts {
		d := -1
		var ok bool
		// if distance already computes, read from map, don't recompute
		if d, ok = distances[makeDistKey(c, user2)]; !ok {
			// nope, so compute now
			var err error
			d, err = computeDegreeRecursive(c, user2, distances)
			if err != nil {
				log.Println("Error from recursive call to compute degree: ", err.Error())
				return 0, err
			}
		}
		if d >= 0 {
			// log.Printf("minDegree from %s to %s is currently %d. Degree between %s and %s is %d\n", user1, user2, minDegree, c, user2, d)
			if minDegree == -1 || (d+1) < minDegree {
				minDegree = d + 1
			}
		}
	}

	distances[makeDistKey(user1, user2)] = minDegree
	return minDegree, nil
}

// DiscoverPorts returns the list of ports that's visible to given userid (me):
// -- filtered by optional keyword substring match in port name
// -- filtered by a max degree of separation between me and exposing user.
//
// If degree=0, returns private ports exposed directly to me.
// If degree=1, returns degree 0 ports + public ports exposed by immediate contacts.
// If degree=2, returns degree 1 ports + public ports exposed by contacts of contacts.
// If degree=-1, returns degree infinity ports, i.e. all private ports exposed to me + public ports of the world.
func DiscoverPorts(userid string, keyword string, maxDegreeOfSeparation int) ([]Port, []int, error) {
	// get all private and public ports visible to userid first
	infinityPorts, err := GetPortsVisibleToUser(userid, keyword, true)
	if err != nil {
		log.Println("Error getting infinity ports visible to user", err.Error())
		return nil, nil, err
	}

	var filteredPorts []Port
	var distances []int

	for _, p := range infinityPorts {
		// filter out factory ports, disabled ports, bot ports, ports exposed by asking user
		if p.Factory == true || p.Creator == userid || p.Disabled == true {
			continue
		}

		/*if maxDegreeOfSeparation == -1 {
			filteredPorts = append(filteredPorts, p)
			continue
		}*/

		if stringArrayContains(p.VisibleToUsers, userid) {
			filteredPorts = append(filteredPorts, p)
			distances = append(distances, 1)
			continue
		}

		degree, err := ComputeDegreeBetweenUsers(userid, p.Creator)
		if err != nil {
			log.Println("Error computing degree between users: ", err.Error())
			return nil, nil, err
		}
		if maxDegreeOfSeparation == -1 || (degree >= 0 && degree <= maxDegreeOfSeparation) {
			filteredPorts = append(filteredPorts, p)
			distances = append(distances, degree)
		}
	}

	return filteredPorts, distances, nil
}
