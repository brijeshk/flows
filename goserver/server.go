package main

import (
	"errors"
	"flag"
	"log"
	_ "net/http/pprof"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	//	"github.com/tommy351/gin-cors"
)

func HandleGetUserById(c *gin.Context) {
	userid := c.Params.ByName("uid")
	user, err := GetUser(userid, true)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	// mask password
	user.Password = ""
	c.JSON(200, user)
}

func HandleSearchForUsers(c *gin.Context) {
	var keywords []string
	if c.BindJSON(&keywords) == nil {
		users, err := FindUsersByKeywords(keywords)
		if err != nil {
			c.String(404, err.Error())
			return
		}
		c.JSON(200, users)
	}
}

func HandleUpdateUserPushToken(c *gin.Context) {
	type tokenData struct {
		Token string `json:"token"`
	}
	var token tokenData
	userid := c.Params.ByName("uid")
	if c.BindJSON(&token) == nil {
		err := UpdateUserPushToken(userid, token.Token)
		if err != nil {
			c.String(404, err.Error())
			return
		}
		c.String(200, "Success")
	}
}

func HandleGetUsers(c *gin.Context) {
	var err error
	var users []User
	users, err = GetUsers()
	if err != nil {
		c.String(404, err.Error())
		return
	}
	c.JSON(200, users)
}

func AuthorizeUser(c *gin.Context) {
	userPass, err := GetCredentialsFromBasicAuthHeader(c.Request)
	if err != nil {
		c.Writer.Header().Set("WWW-Authenticate", "Basic realm=\"Authorization Required\"")
		c.AbortWithStatus(401)
		return
	}
	b := AuthorizeUserInDb(userPass.Username, userPass.Password)
	if b == false {
		c.AbortWithStatus(401)
		return
	}
}

func HandleLoginUser(c *gin.Context) {
	c.String(200, "Success")
}

func HandleGetPortById(c *gin.Context) {
	portid := c.Params.ByName("pid")
	port, err := GetPort(portid)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	c.JSON(200, port)
}

func HandleGetGroupById(c *gin.Context) {
	groupid := c.Params.ByName("gid")
	group, err := GetGroup(groupid)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	c.JSON(200, group)
}

func HandleGetPublicGroups(c *gin.Context) {
	keyword := c.Params.ByName("q")
	groups, err := GetPublicGroups(keyword)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	c.JSON(200, groups)
}

func HandleGetGroupsByUser(c *gin.Context) {
	userid := c.Params.ByName("uid")
	groups, err := GetGroupsByUser(userid)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	c.JSON(200, groups)
}

func HandleGetPortsExposedByUser(c *gin.Context) {
	byUserId := c.Params.ByName("byUser")
	ports, err := GetPortsExposedByUserToUser(byUserId, byUserId)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	c.JSON(200, ports)
}

func HandleGetPortsExposedByUserToUser(c *gin.Context) {
	byUserId := c.Params.ByName("byUser")
	toUserId := c.Params.ByName("toUser")
	ports, err := GetPortsExposedByUserToUser(byUserId, toUserId)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	c.JSON(200, ports)
}

func HandleGetDefaultPorts(c *gin.Context) {
	ports, err := GetDefaultPorts()
	if err != nil {
		c.String(500, err.Error())
		return
	}
	c.JSON(200, ports)
}

func HandleGetPortsVisibleToUser(c *gin.Context) {
	userid := c.Params.ByName("user")
	publicPorts, _ := strconv.ParseBool(c.Request.FormValue("publicPorts"))
	ports, err := GetPortsVisibleToUser(userid, "", publicPorts)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	if ports != nil {
		c.JSON(200, ports)
	} else {
		c.JSON(200, []int{})
	}
}

func HandleGetPortTemplates(c *gin.Context) {
	templates, err := GetPortTemplates()
	if err != nil {
		c.String(500, err.Error())
		return
	}
	c.JSON(200, templates)
}

func HandleDiscoverPorts(c *gin.Context) {
	userid := c.Params.ByName("uid")
	maxDegree, err := strconv.Atoi(c.Params.ByName("maxDegree"))
	if err != nil {
		c.String(404, err.Error())
		return
	}
	keyword := c.Request.FormValue("q")
	ports, distances, err := DiscoverPorts(userid, keyword, maxDegree)
	if err != nil {
		log.Printf("Error from DiscoverPorts for [%s, %s, %d] : %s\n", userid, keyword, maxDegree, err.Error())
		c.String(500, err.Error())
		return
	}
	r := map[string]interface{}{
		"ports":     ports,
		"distances": distances,
	}
	c.JSON(200, r)
}

func HandleHello(c *gin.Context) {
	hw := map[string]string{"Hello": "World"}
	c.JSON(200, hw)
}

func HandleCreatePort(c *gin.Context) {
	var port Port
	err := c.BindJSON(&port)
	if err == nil {
		if err := CreatePort(&port); err != nil {
			c.String(404, err.Error())
			return
		}
		c.JSON(200, port)
	} else {
		log.Printf("Error binding port json: ", err.Error())
	}
}

func HandleUpdatePort(c *gin.Context) {
	var port Port
	if c.BindJSON(&port) == nil {
		pid := c.Params.ByName("pid")
		if pid != port.Id {
			err := errors.New("portid in PUT url doesn't match id inside port json")
			c.String(404, err.Error())
			return
		}
		up, _ := GetCredentialsFromBasicAuthHeader(c.Request)
		if up.Username != port.Creator {
			err := errors.New("User " + up.Username + " not authorized to modify this port")
			c.String(403, err.Error())
		}
		if err := UpdatePort(&port); err != nil {
			c.String(404, err.Error())
			return
		}
		c.String(200, "Success")
	}
}

func HandleDeletePort(c *gin.Context) {
	pid := c.Params.ByName("pid")
	if err := DeletePort(pid); err != nil {
		c.String(404, err.Error())
		return
	}
	c.String(200, "Success")
}

func HandleCreateEvent(c *gin.Context) {
	var e Event
	err := c.BindJSON(&e)
	if err == nil {
		if err := CreateEvent(&e); err != nil {
			c.String(404, err.Error())
			return
		}
		c.JSON(200, "Success")
	} else {
		log.Printf("Error parsing event json: ", err.Error())
	}
}

func HandleUpdateEvent(c *gin.Context) {
	var e Event
	err := c.BindJSON(&e)
	if err == nil {
		eid := c.Params.ByName("eid")
		if eid != e.Id {
			err := errors.New("eventid in PUT url doesn't match id inside event json")
			c.String(404, err.Error())
			return
		}
		up, _ := GetCredentialsFromBasicAuthHeader(c.Request)
		if up.Username != e.Owner {
			err := errors.New("User " + up.Username + " not authorized to modify this event")
			c.String(403, err.Error())
		}
		if err := UpdateEvent(&e); err != nil {
			c.String(404, err.Error())
			return
		}
		c.String(200, "Success")
	} else {
		log.Printf("Error parsing event json: ", err.Error())
	}
}

func HandleUpdateEventInvite(c *gin.Context) {
	eid := c.Params.ByName("eid")
	userid := c.Params.ByName("userid")
	yesNoMaybe := c.Params.ByName("status")
	if err := UpdateEventAcceptance(eid, userid, yesNoMaybe); err != nil {
		c.String(404, err.Error())
		return
	}
	c.String(200, "Success")
}

func HandleDeleteEvent(c *gin.Context) {
	eid := c.Params.ByName("eid")
	if err := DeleteEvent(eid); err != nil {
		c.String(404, err.Error())
		return
	}
	c.String(200, "Success")
}

func HandleGetEvent(c *gin.Context) {
	eid := c.Params.ByName("eid")
	e, err := GetEvent(eid)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	c.JSON(200, e)
}

func HandleGetEventDeltaSince(c *gin.Context) {
	var sinceTime time.Time
	userid := c.Params.ByName("userid")
	sinceTimeStr := c.Params.ByName("sinceTime")
	if sinceTimeStr != "0" && sinceTimeStr != "" {
		var err error
		sinceTime, err = time.Parse(time.RFC3339, sinceTimeStr)
		if err != nil {
			c.String(404, "Invalid sinceTime specified")
			return
		}
	} else {
		log.Println("sinceTime requested = 0; getting all events")
	}
	events, err := GetEventsModifiedSince(userid, sinceTime)
	if err != nil {
		c.String(500, err.Error())
		return
	}
	if events == nil {
		// empty result
		c.JSON(200, []Event{})
	} else {
		c.JSON(200, events)
	}
}

func HandleSendMessageOnEvent(c *gin.Context) {
	var atom EventAtom
	if c.BindJSON(&atom) == nil {
		err := SendMessageOnEvent(&atom)
		if err != nil {
			c.String(500, err.Error())
			return
		}
	} else {
		c.String(404, "Invalid atom JSON posted")
	}
}

func HandleGetCachedMessages(c *gin.Context) {
	userid := c.Params.ByName("userid")
	atoms, err := GetCachedMessages(userid)
	if err != nil {
		c.String(500, err.Error())
		return
	}
	c.JSON(200, atoms)
}

func HandleCreateUser(c *gin.Context) {
	var user User
	if c.BindJSON(&user) == nil {
		if err := CreateUser(&user); err != nil {
			c.String(404, err.Error())
			return
		}
		c.JSON(200, user)
	} else {
		c.String(404, "Invalid user JSON posted")
	}
}

func HandleUpdateUser(c *gin.Context) {
	var user User
	if c.BindJSON(&user) == nil {
		if err := UpdateUser(&user); err != nil {
			c.String(404, err.Error())
			return
		}
		c.String(200, "Success")
	}
}

func HandleCreateGroup(c *gin.Context) {
	var group Group
	if c.BindJSON(&group) == nil {
		if err := CreateGroup(&group); err != nil {
			c.String(404, err.Error())
			return
		}
		c.JSON(200, group)
	}
}

func HandleAddUserConnection(c *gin.Context) {
	user1 := c.Params.ByName("user1")
	user2 := c.Params.ByName("user2")
	var contact Contact
	if c.BindJSON(&contact) == nil {
		if err := AddOrRemoveUserConnection(user1, user2, contact, true); err != nil {
			c.String(404, err.Error())
			return
		}
		c.String(200, "Success")
	}
}

func HandleRemoveUserConnection(c *gin.Context) {
	user1 := c.Params.ByName("user1")
	user2 := c.Params.ByName("user2")
	var contact Contact
	if c.BindJSON(&contact) == nil {
		if err := AddOrRemoveUserConnection(user1, user2, contact, false); err != nil {
			c.String(404, err.Error())
			return
		}
		c.String(200, "Success")
	}
}

func HandleFirePortCommand(c *gin.Context) {
	portid := c.Params.ByName("pid")
	command := c.Request.FormValue("cmd")
	if command == "" {
		c.String(404, "No command specified")
		return
	}
	input := c.Request.FormValue("data")
	port, err := GetPort(portid)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	data, err := port.FireCommand(command, input)
	if err != nil {
		c.String(500, err.Error())
		return
	}
	c.JSON(200, data)
}

func HandleApplyFlowDelta(c *gin.Context) {
	userid := c.Params.ByName("userid")
	//notify, _ := strconv.ParseBool(c.Request.FormValue("notify"))
	var flows []Flow
	if c.BindJSON(&flows) != nil {
		c.String(404, "Invalid flow delta content posted")
		return
	}

	// if user is online, try sending message on websocket
	sent := false
	if IsUserOnline(userid) {
		log.Println("Sending message on websocket to online user ", userid)
		err := SendMessageOnWebSocket(userid, flows)
		if err == nil {
			sent = true
		}
	}

	// if user is not online or message was not delivered, cache in server db
	if !sent {
		log.Println("User not online, saving message to server db")
		err := ApplyFlowDelta(userid, flows)
		if err != nil {
			c.String(500, err.Error())
			return
		}
	}
	c.String(200, "Success")
}

func HandleApplyFlowFlags(c *gin.Context) {
	userid := c.Params.ByName("userid")
	var flow Flow
	if c.BindJSON(&flow) == nil {
		if err := UpdateFlowFlags(userid, flow); err != nil {
			c.String(500, err.Error())
			return
		}
	} else {
		c.String(404, "Invalid flow flag content posted")
		return
	}
	c.String(200, "Success")
}

func HandleGetFlowDeltaSince(c *gin.Context) {
	var sinceTime time.Time
	userid := c.Params.ByName("userid")
	sinceTimeStr := c.Params.ByName("sinceTime")
	if sinceTimeStr != "0" && sinceTimeStr != "" {
		var err error
		sinceTime, err = time.Parse(time.RFC3339, sinceTimeStr)
		if err != nil {
			c.String(404, "Invalid sinceTime specified")
			return
		}
	} else {
		log.Println("sinceTime requested = 0; getting all flows")
	}
	flows, err := GetFlowDeltaSince(userid, sinceTime)
	if err != nil {
		c.String(500, err.Error())
		return
	}
	if flows == nil {
		// empty result
		c.JSON(200, []Flow{})
	} else {
		c.JSON(200, flows)
	}
}

func HandleGetOnlineContacts(c *gin.Context) {
	userid := c.Params.ByName("uid")
	u, err := GetUser(userid, true)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	var onlineContacts []string
	for c := range u.Contacts {
		if IsUserOnline(c) {
			onlineContacts = append(onlineContacts, c)
		}
	}
	c.JSON(200, onlineContacts)
}

func HandleGenerateUserToken(c *gin.Context) {
	userid := c.Params.ByName("uid")
	err := GenerateUserToken(userid)
	if err != nil {
		c.String(404, err.Error())
		return
	}
	c.String(200, "Success")
}

func HandleResetPassword(c *gin.Context) {
	userid := c.Params.ByName("uid")
	token := c.Request.FormValue("token")
	err := ResetUserPassword(userid, token)
	if err != nil {
		c.String(401, err.Error())
		return
	}
	c.String(200, "Your password has been reset successfully!")
}

func HandlePrintServerStats(c *gin.Context) {
	PrintWebSocketStats(true)
	c.String(200, "Success")
}

func MyCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}
		c.Next()
	}
}

func main() {
	var useRds bool
	flag.BoolVar(&useRds, "useRDS", false, "Use RDS for MySQL flows db")
	flag.Parse()

	/*
	// init flow database connection
	sqldb := "root:root@tcp(localhost:3306)"
	if useRds == true {
		sqldb = "autometon:autometon@tcp(mysql1.c8ruoozgtc0u.us-west-2.rds.amazonaws.com:3306)"
	}
	InitSqlDb(sqldb)
	defer CloseSqlDb()
	*/

	// init port database connection
	InitDbSession()
	defer CloseDbSession()

	// init websocket handler, setup global connection map etc.
	initWebSocketsHandler()

	r := gin.Default()

	// handle websocket connections
	r.GET("/websocket/:uid", HandleWebSocketConnection)

	// allow requests originating from localhost domain for testing
	/*
		w := r.Group("/",
			cors.Middleware(cors.Options{
				//AllowOrigins:     "*",
				AllowHeaders:     []string{"Origin", "Authorization", "authorization", "Content-Type"},
				AllowMethods:     []string{"GET", "PUT", "POST", "DELETE", "OPTIONS"},
				AllowCredentials: true,
			}))
		w.OPTIONS("/", HandleHello)
	*/

	w := r.Group("/")
	w.Use(MyCORSMiddleware())

	// CORS works only if we handle each OPTIONS request and send success
	w.OPTIONS("/", HandleHello)
	w.OPTIONS("/users", HandleHello)
	w.OPTIONS("/users/login", HandleHello)
	w.OPTIONS("/pushtoken/:uid", HandleHello)
	w.OPTIONS("/onlinecontacts/:uid", HandleHello)
	w.OPTIONS("/flowDelta/:userid/:sinceTime", HandleHello)
	w.OPTIONS("/flowDelta/:userid", HandleHello)
	w.OPTIONS("/ports", HandleHello)
	w.OPTIONS("/eventDelta/:userid/:sinceTime", HandleHello)
	w.OPTIONS("/eventDelta/:userid", HandleHello)
	w.OPTIONS("/events", HandleHello)
	w.OPTIONS("/events/:eid", HandleHello)
	w.OPTIONS("/eventInvite/:eid/:userid/:status", HandleHello)
	w.OPTIONS("/eventAtoms", HandleHello)

	w.GET("/users/login", AuthorizeUser, HandleLoginUser)
	w.GET("/users/id/:uid", HandleGetUserById)
	w.PUT("/searchusers", HandleSearchForUsers)
	w.PUT("/pushtoken/:uid", HandleUpdateUserPushToken)
	w.GET("/users", HandleGetUsers)
	w.GET("/ports/id/:pid", HandleGetPortById)
	w.GET("/ports/default", HandleGetDefaultPorts)
	w.GET("/ports/user/:byUser", HandleGetPortsExposedByUser)
	w.GET("/ports/user/:byUser/:toUser", HandleGetPortsExposedByUserToUser)
	w.GET("/ports/visible/:user", HandleGetPortsVisibleToUser)
	w.GET("/ports/id/:pid/fire_cmd", HandleFirePortCommand)
	w.GET("/ports/templates", HandleGetPortTemplates)
	w.GET("/ports/discover/:uid/:maxDegree", HandleDiscoverPorts)
	w.POST("/ports", HandleCreatePort)
	w.PUT("/ports/:pid", AuthorizeUser, HandleUpdatePort)
	w.DELETE("/ports/:pid", HandleDeletePort)
	w.POST("/users", HandleCreateUser)
	w.PUT("/users/:uid", HandleUpdateUser)
	w.POST("/users/gentoken/:uid", HandleGenerateUserToken)
	w.GET("/resetpass/:uid", HandleResetPassword)
	w.GET("/hello", HandleHello)
	w.POST("/groups", HandleCreateGroup)
	w.GET("/groups/id/:gid", HandleGetGroupById)
	w.GET("/groups/public", HandleGetPublicGroups)
	w.GET("/groups/user/:uid", HandleGetGroupsByUser)
	w.GET("/onlinecontacts/:uid", HandleGetOnlineContacts)
	w.PUT("/contacts/:user1/:user2", HandleAddUserConnection)
	w.DELETE("/contacts/:user1/:user2", HandleRemoveUserConnection)

	w.POST("/flowDelta/:userid", HandleApplyFlowDelta)
	w.POST("/flowFlags/:userid", HandleApplyFlowFlags)
	w.GET("/flowDelta/:userid/:sinceTime", HandleGetFlowDeltaSince)

	w.POST("/events", AuthorizeUser, HandleCreateEvent)
	w.PUT("/events/:eid", AuthorizeUser, HandleUpdateEvent)
	w.PUT("/eventInvite/:eid/:userid/:status", AuthorizeUser, HandleUpdateEventInvite)
	w.DELETE("/events/:eid", AuthorizeUser, HandleDeleteEvent)
	w.GET("/events/:eid", AuthorizeUser, HandleGetEvent)
	w.GET("/eventDelta/:userid/:sinceTime", AuthorizeUser, HandleGetEventDeltaSince)
	w.POST("/eventAtoms", AuthorizeUser, HandleSendMessageOnEvent)
	w.GET("/eventAtoms/:userid", AuthorizeUser, HandleGetCachedMessages)

	w.GET("/stats", HandlePrintServerStats)

	w.Static("/app", "www")
	w.Static("/dl", "dl")

	// kick off goroutine to delete past events at a 1 minute interval
	go DeletePastEvents()

	log.Println("Starting web server at port 5000...")
	r.Run(":5000")
}
