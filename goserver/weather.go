package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// GetWeatherByZip returns the current weather condition of a given US zipcode or international city name by querying the OpenWeather API.
// Result is returned as a string.
func (s *SystemService) GetWeatherByZip(zipOrCity string) (string, error) {
	if zipOrCity == "" {
		return "", nil
	}

	resp, err := http.Get("http://api.openweathermap.org/data/2.5/weather?q=" + zipOrCity)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	var arbit map[string]interface{}
	err = json.Unmarshal([]byte(body), &arbit)
	aw := arbit["weather"].([]interface{})
	w0 := aw[0].(map[string]interface{})
	desc := w0["description"].(string)
	return desc, nil
}
