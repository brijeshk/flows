if [ $1 = 'reset' ]; then
    echo "Resetting records in flows db"
    mongo flows --eval "db.dropDatabase()"
fi
mongoimport -h 127.0.0.1:27017 -d flows -c user users.json
mongoimport -h 127.0.0.1:27017 -d flows -c userGroup groups.json
./create_users.sh
./yaml2json.sh ports.yaml > tmp1.json
mongoimport -h 127.0.0.1:27017 -d flows -c port tmp1.json --jsonArray
./yaml2json.sh events.yaml > tmp2.json
mongoimport -h 127.0.0.1:27017 -d flows -c event tmp2.json --jsonArray
rm tmp*.json
